# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

import io
import hashlib
import os

from django.conf import settings

class FileManager():
    
    
    def write_reproduction_file(self, relations, variables):
        self.stream = io.StringIO()
        self._print_reproduction_headers(variables)
        self._print_relations(relations)
        self.stream.seek(0)
        h = hashlib.sha1(self.stream.read().encode('utf8'))
        with open(os.path.join(settings.MEDIA_ROOT,'tmp',h.hexdigest()+'.csv'), 'wb') as csvfile:
            self.stream.seek(0)
            csvfile.write(self.stream.read().encode('utf8'))
            csvfile.close()
        return h
    
    def write_individual_file(self, relations, variables):
        self.stream = io.StringIO()
        self._print_individual_headers(variables)
        self._print_individuals(relations)
        self.stream.seek(0)
        h = hashlib.sha1(self.stream.read().encode('utf8'))
        with open(os.path.join(settings.MEDIA_ROOT,'tmp',h.hexdigest()+'.csv'), 'wb') as csvfile:
            self.stream.seek(0)
            csvfile.write(self.stream.read().encode('utf8'))
            csvfile.close()
        return h
    
    def _print_reproduction_headers(self, variables):
        self.stream.write("project\tsown_year\tharvested_year\tid_seed_lot_sown\tintra_selection_name\tetiquette\tsplit\tquantity_sown\tquantity_harvested\tblock\tX\tY")
        for v in variables :
            self.stream.write("\t{0}\t{0}$date".format(v.name))
        self.stream.write("\n")
    
    def _print_individual_headers(self, variables):
        self.stream.write("project\tsown_year\tharvested_year\tid_seed_lot_sown\tintra_selection_name\tetiquette\tsplit\tquantity_sown\tquantity_harvested\tblock\tX\tY\tindividuals\tcorrelation_group")
        for v in variables :
            self.stream.write("\t{0}\t{0}$date".format(v.name))
        self.stream.write("\n")
    
    def _print_relations(self, relations) :
        for r in relations:
            selection_name = r.selection.selection_name if r.selection else "" 
            quantity_sown = r.seed_lot_son.quantity_ini if r.seed_lot_son.quantity_ini else ""
            self.stream.write("{0}\t{1}\t{2}\t{3}\t{4}\t\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\n".format(','.join([p.project_name for p in r.project.all()]),
                                                                        r.reproduction.start_date,
                                                                        r.reproduction.end_date,
                                                                        r.seed_lot_father.name,
                                                                        selection_name,
                                                                        r.split,
                                                                        r.quantity,
                                                                        quantity_sown,
                                                                        r.block,
                                                                        r.X,
                                                                        r.Y
                                                                        ))
            
    def _print_individuals(self, relations) :
        for r in relations:
            for i in r.get_phenotyped_individual() :
                selection_name = r.selection.selection_name if r.selection else "" 
                quantity_sown = r.seed_lot_son.quantity_ini if r.seed_lot_son.quantity_ini else ""
                self.stream.write("{0}\t{1}\t{2}\t{3}\t{4}\t\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t\n".format(','.join([p.project_name for p in r.project.all()]),
                                                                            r.reproduction.start_date,
                                                                            r.reproduction.end_date,
                                                                            r.seed_lot_father.name,
                                                                            selection_name,
                                                                            r.split,
                                                                            r.quantity,
                                                                            quantity_sown,
                                                                            r.block,
                                                                            r.X,
                                                                            r.Y,
                                                                            i,
                                                                            ))

# class FileManager():
#     
#     def __init__(self, radio, datafile, methodfile):
#         self.radio = radio
#         self.methodfile = methodfile
#         self.datafile = datafile
#         self.error_messages= {"error_row":[], "error_nomenclature":[],"error_split":[],"error_empty":[],'sowing_block_error':[],
#                       "error_son_germplasm":[],"idgermplasm_error":[],"mixture_empty":[],"variable_error":[],"method_error":[],
#                       "selct_error":[],"person_error":[],"seed_lot_error":[],"error_block":[],"error_project":[],
#                       "error_sown_year":[],"error_pheno_variable":[],"pheno_grp_error":[],"error_message":[],"split_string":[],
#                       "error_file_list":[]
#                       }
#         
#         self.warning_messages = {"warning_diffusion_quantity":[],"warnings_seed":[],"female_warnings_seed":[],"xy_missing":[],
#                         "male_warnings_seed":[],"son_warnings_seed":[],"warnings_relation":[],"male_relation":[],
#                         "female_relation":[],"warnings_not_quantity":[],"warnings_germ":[],"created_person":[],"warnings_var":[],"reproduction_type":[]}
#         
#         self.variable_method_dict = {} #dictionnaire qui contient les variables déclarée dans le fichier methodes
#         self.correlation_groups = {} #dictionnaire qui contient les groupes de correlation (uniquement pour les données individuelles)
#         
#     def read_and_save_data(self):
#         csvmethodfile = csv.DictReader(self.methodfile, delimiter='\t', quotechar='"')
#         csvdatafile = csv.DictReader(self.datafile, delimiter='\t', quotechar='"')
#         
#         self._read_method_file(csvmethodfile)
#         
#         germplasm_type = Germplasm_type.objects.values_list('germplasm_type')
#         self.menu_list = ''
#         for n in  germplasm_type:
#             menu = '<option value="%s">%s</option>' % (n[0], n[0])
#             self.menu_list += menu
#         
#         if self.radio == CROSS:
#             self._compute_cross(csvdatafile)
#         elif self.radio == DIFFUSION:
#             self._compute_diffusion(csvdatafile)
#         elif self.radio == MIXTURE:
#             self._compute_mixture(csvdatafile)
#         elif self.radio == REPRODUCTION:
#             self._compute_reproduction(csvdatafile)
#         elif self.radio == SELECTION:
#             self._compute_selection(csvdatafile)
#         elif self.radio == PHENOTYPIC:
#             self._compute_individual(csvdatafile)
# 
#     
#     def get_error_messages(self):
#         return self.error_messages
#     
#     def get_warning_messages(self):
#         return self.warning_messages
#     
#     def _read_method_file(self,csvmethodfile):
#         line = 0
#         for row in csvmethodfile:
#             line += 1
#             #On vérifie que le fichier contient bien les colonnes voulues.
#             if METHOD_NAME not in csvmethodfile.fieldnames or VARIABLE not in csvmethodfile.fieldnames or CORRELATION_GROUP not in csvmethodfile.fieldnames:
#                 self.error_messages["error_message"].append(_('The method file is not in the good format')+'<br>')
#                 #return render(request,'myuser/error.html', {'error_message': error_messages["error_message"], 'radio':radio, 'file_name':file_name})
#             else:
#                 try:
#                     method_obj = Method.objects.get(method_name=row[METHOD_NAME])
#                 except Method.DoesNotExist:
#                     self.error_messages["method_error"].append(_('line {line}: the method {method} does not exist')+'<br> '.format(line=line, method=row[METHOD_NAME]))
#                 else:
#                     #Si le tag '%' est utilisé alors on utilise une variable avec deux methodes on split sur '%'
#                     if '%' in row[VARIABLE]:
#                         varname = row[VARIABLE].split('%')[1]
#                     else:
#                         varname = row[VARIABLE]
#                     (variable_obj, create) = Variable.objects.get_or_create(type=row[TYPE],
#                                                                                         name=varname,
#                                                                                         season='?',
#                                                                                         )
#                     if create == True:
#                         self.warning_messages["warnings_var"].append(variable_warnings(line,variable_obj,method_obj))
#                     self.variable_method_dict[variable_obj] = method_obj
#                     
#                     if self.radio == PHENOTYPIC:
#                         groups = row[CORRELATION_GROUP].split(',')
#                         for g in groups:
#                             if g not in self.correlation_groups.keys():
#                                 self.correlation_groups[g] = []
#                             self.correlation_groups[g].append(row[VARIABLE])
# 
#     def _compute_cross(self,csvdatafile):
#         project = None
#         line = 0
#         mandatory_fields = [PROJECT, SOWN_YEAR, HARVESTED_YEAR, QUANTITY_HARVESTED, CROSS_YEAR, CROSS_GERMPLASM, NUMBER_CROSSES,
#                                 KERNEL_NUMBER_F1, MALE_SEED_LOT, MALE_ETIQUETTE, MALE_SPLIT, MALE_QUANTITY, MALE_BLOCK, MALE_X, MALE_Y,
#                                 FEMALE_SEED_LOT, FEMALE_ETIQUETTE, FEMALE_SPLIT, FEMALE_QUANTITY, FEMALE_BLOCK, FEMALE_X, FEMALE_Y]
#         female_seedlot_dict = {}
#         male_seedlot_dict = {}
#         if self._is_valid_format(csvdatafile, mandatory_fields, CROSS_YEAR):
#             #ici on vérifie la bonne concordance entre les variables du fichier methode et du fichier de données
#             variable_list = self._get_variable_list(copy.copy(csvdatafile.fieldnames), mandatory_fields)
#             self._check_variable_methods(variable_list)
#             for row in csvdatafile:
#                 line += 1
#                 try:
#                     project = self._check_project(row[PROJECT], project, line)
#                 except NameError:
#                     project = self._check_project(row[PROJECT], None, line)
#                 #on check que les coordonnées ne sont pas vides
#                 if row[MALE_X] == '' or row[MALE_Y] == '' :
#                     self.warning_messages['xy_missing'].append(_("line {line}: X and/or Y is missing for the cross concerning male seed lot {seedlot}")+"<br />".format(line=line, seedlot=row[MALE_SEED_LOT]))
#                 if row[FEMALE_X] == '' or row[FEMALE_Y] == '' :
#                     self.warning_messages['xy_missing'].append(_("line {line}: X and/or Y is missing for the cross concerning female seed lot {seedlot}")+"<br />".format(line=line, seedlot=row[FEMALE_SEED_LOT]))
#                 
#                 
#                 #### DEBUT DES CHECKS CROSS ###
#                 if not self._check_cross_date(row[SOWN_YEAR], row[HARVESTED_YEAR], row[CROSS_YEAR], line) : continue        
#                 
#                 try:
#                     int(row[FEMALE_BLOCK])
#                 except ValueError:   
#                     self.error_messages["error_block"].append(_('line {line}: block information for female seed lot is missing or is not an integer value')+'<br />'.format(line=line))
#                 
#                 try:
#                     int(row[MALE_BLOCK])
#                 except ValueError: 
#                     self.error_messages["error_block"].append(_('line {line}: block information for male seed lot is missing or is not an integer value')+'<br />'.format(line=line))
# 
#                 if row[FEMALE_SEED_LOT].split('_')[1] != row[MALE_SEED_LOT].split('_')[1]:
#                     self.error_messages["error_message"].append(_('line {line}: {male} (male) creation place is different from {female} (female) place')+'<br>'.format(line=line, male=row[MALE_SEED_LOT], female=row[FEMALE_SEED_LOT]))
# 
# 
#                 if row[FEMALE_SPLIT] != '':
#                     if row[FEMALE_SPLIT] != '0' and row[FEMALE_SPLIT] != '1' :
#                         self.error_messages["error_split"].append(_('line {line}: {split} is not a valid split value it should be 0 or 1')+'<br>'.format(line=line, split=row[FEMALE_SPLIT]))
#                 if row[MALE_SPLIT] !='':
#                     if row[MALE_SPLIT] != '0' and row[MALE_SPLIT] != '1' :
#                         self.error_messages["error_split"].append(_('line {line}: {split} is not a valid split value it should be 0 or 1')+'<br>'.format(line=line, split=row[MALE_SPLIT]))
# 
#                 #### FIN DES CHECKS CROSS ###
#                 seed_lot_female = self._get_or_create_seedlot(row[FEMALE_SEED_LOT], female_seedlot_dict, project, row[FEMALE_QUANTITY], line, 'female')
#                 seed_lot_male = self._get_or_create_seedlot(row[MALE_SEED_LOT], male_seedlot_dict, project, row[MALE_QUANTITY], line, 'male')
#            
#                 if row[KERNEL_NUMBER_F1] == '0':
#                     row_germplasm = self._create_digit_gpnotcrossed()
#                 else:
#                     row_germplasm = row[CROSS_GERMPLASM]
#                         
#                         
#                 if row[KERNEL_NUMBER_F1] == '0':
#                     son_name = row_germplasm 
#                 else:
#                     son_name = row_germplasm + "_" + row[MALE_SEED_LOT].split('_')[1] + "_" + row[CROSS_YEAR]
#                 
#                 crossed_seedlot = self._create_seed_lot(son_name, row[QUANTITY_HARVESTED], line, 'son')
#                 print(project) 
#                 crossed_seedlot.project.add(project)
#                             
#                 """ Creation de l'évenement de croisement """
#                             
#                 cross_dict = {
#                             'date':row[CROSS_YEAR],
#                             'kernel_number':row[KERNEL_NUMBER_F1],
#                             'cross_number':row[NUMBER_CROSSES],
#                             
#                             'male_split':row[MALE_SPLIT],
#                             'male_block':row[MALE_BLOCK],
#                             'male_X':row[MALE_X],
#                             'male_Y':row[MALE_Y],
#                             'male_quantity':row[MALE_QUANTITY],
#                             
#                             'female_split':row[FEMALE_SPLIT],
#                             'female_block':row[FEMALE_BLOCK],
#                             'female_X':row[FEMALE_X],
#                             'female_Y':row[FEMALE_Y],
#                             'female_quantity':row[FEMALE_QUANTITY]
#                                             }
#                             
#                 cross_event = Reproduction.objects.create_cross(cross_dict, seed_lot_female, seed_lot_male, crossed_seedlot)
#                 self._insert_data(row, cross_event, [seed_lot_male, seed_lot_female], crossed_seedlot, line)
#     
#     def _compute_diffusion(self, csvdatafile):
#         line = 0
#         project = None
#         mandatory_fields = [PROJECT, LOCATION, ID_SEED_LOT,ETIQUETTE, EVENT_YEAR, SPLIT, QUANTITY]
#         diffusion_list = [] #une liste de tuple(SL, LOC) pour éviter d'éventuels doublons dans un fichier
#         seed_lot_diffused = {} #pour les lot de graines sortis du chapeau (c'est le même qui est réutilisé)
#         
#         if self._is_valid_format(csvdatafile, mandatory_fields, LOCATION):
#             #ici on vérifie la bonne concordance entre les variables du fichier methode et du fichier de données
#             variable_list = self._get_variable_list(copy.copy(csvdatafile.fieldnames), mandatory_fields)
#             self._check_variable_methods(variable_list)
#             
#             for row in csvdatafile:
#                 line += 1
#                 project = self._check_project(row[PROJECT], project, line)
# 
#                 #on vérifie que les dates sont au bon format
#                 
#                 if not self._check_date_validity(row[EVENT_YEAR], EVENT_YEAR, line) : continue
#                 if row[LOCATION] == "":
#                     self.error_messages["person_error"].append(_("line {0}: You need to fill the location cell to send the seed lot").format(line))
#                 #on vérifie que la diffusion n'a pas déjà été créée dans ce fichier    
#                 if (row[ID_SEED_LOT], row[LOCATION]) not in diffusion_list :
#                     diffusion_list.append((row[ID_SEED_LOT], row[LOCATION]))
#                     #le nom du lot de graine contient 4 éléments dans ce cas on suppose qu'il existe déjà en base
#                     
#                     seed_lot_sent = self._get_or_create_seedlot(row[ID_SEED_LOT], seed_lot_diffused, project, None, line, 'father')
#                     seed_lot_received = None
#                     if seed_lot_sent and project:
#                         seed_lot_sent.project.add(project)
#                         #on créé le lot de graine reçu
#                         son = seed_lot_sent.name.split('_')[0] + '_' + row[LOCATION] + '_' + row[EVENT_YEAR]
#                         seed_lot_received = self._create_seed_lot(son, row[QUANTITY], line, 'son')
#                         seed_lot_received.project.add(project)
#                     
#                     if seed_lot_received:
#                         #puis on créé un évenement de diffusion     
#                         diffusion_dict = {'split' : row[SPLIT],
#                                           'date' : row[EVENT_YEAR],
#                                           'quantity':row[QUANTITY]
#                                         }
#                         (diffusion_event, create) = Diffusion.objects.create_diffusion(seed_lot_sent, seed_lot_received, diffusion_dict)
#                         if row[QUANTITY] == '':
#                             self.warning_messages["warning_diffusion_quantity"].append(_('line')+' {line}: {diffusion} <br>'.format(line=line, diffusion=diffusion_event))
#                         if create == True:
#                             self.warning_messages["warnings_relation"].append(_('line')+' {line}: {diffusion}<br>'.format(line=line, diffusion=diffusion_event))
#     
#                         self._insert_data(row, diffusion_event, seed_lot_sent, seed_lot_received, line)
#     
#     def _compute_mixture(self, csvdatafile):
#         line = 0
#         project = None
#         mandatory_fields = [PROJECT, ID_SEED_LOT,ETIQUETTE, SPLIT, QUANTITY, GERMPLASM, EVENT_YEAR]
#         mixt_dict = {}
#         created_seed_lot = {}
#            
#         if self._is_valid_format(csvdatafile, mandatory_fields, GERMPLASM):
#             variable_list = self._get_variable_list(copy.copy(csvdatafile.fieldnames), mandatory_fields)
#             self._check_variable_methods(variable_list)
#             
#             for row in csvdatafile:
#                 line += 1
#                 project = self._check_project(row[PROJECT], project, line)
#                 if not self._check_date_validity(row[EVENT_YEAR], EVENT_YEAR, line): continue
#                 if not self._check_date_logic(row[ID_SEED_LOT].split('_')[2], row[EVENT_YEAR], line, "Creation year", EVENT_YEAR): continue
#                 
#                 try:
#                     if int(row[SPLIT]) != 0 and int(row[SPLIT]) != 1:
#                         self.error_messages["split_string"].append(_('line {line}: The split value must be equal to 0 or 1, not {split}')+' <br>'.format(line=line, split=row[SPLIT]))
#                 except ValueError:
#                     self.error_messages["split_string"].append(_('line {line}: The split value must be equal to 0 or 1, not {split}')+' <br>'.format(line=line, split=row[SPLIT]))
# 
#                 if row[GERMPLASM] not in mixt_dict.keys() :
#                     mixt_dict[row[GERMPLASM]] = {}
#                     mixt_dict[row[GERMPLASM]]['lot_to_mix'] = []
#                     
#                 #we check that the seed lot we are mixing is on the same farm than others mixed seed lot
#                 if row[GERMPLASM] in mixt_dict.keys():
#                     for seedlot in mixt_dict[row[GERMPLASM]]['lot_to_mix']:
#                         if row[ID_SEED_LOT].split('_')[1] != seedlot.location.short_name:
#                             self.error_messages["error_message"].append(_('line {line}: The seed lot {seedlot} is not mixed ({germplasm}) on the same farm than other seed lots, make a diffusion before')+'<br>'.format(line=line, seedlot=row[ID_SEED_LOT], germplasm=row[GERMPLASM]))
# 
#                 seed_lot_to_mix = self._get_or_create_seedlot(row[ID_SEED_LOT], created_seed_lot, project, None, line, 'father')
#                 if seed_lot_to_mix in mixt_dict[row[GERMPLASM]]['lot_to_mix']:
#                     self.error_messages["error_message"].append(_('line {line}: the seed lot {seedlot} is present twice for the {germplasm} mixture')+'<br>'.format(line=line, seedlot=row[ID_SEED_LOT], germplasm=row[GERMPLASM]))
#                 else :
#                     mixt_dict[row[GERMPLASM]]['lot_to_mix'].append(seed_lot_to_mix)
# 
#                 if seed_lot_to_mix:
#                     if row[GERMPLASM] in mixt_dict.keys() :
#                         seed_lot_mixed = mixt_dict[row[GERMPLASM]]['seedlot']
#                     else :
#                         mixed_name = row[GERMPLASM] + "_" + row[ID_SEED_LOT].split('_')[1] + "_" + row[EVENT_YEAR]
#                         seed_lot_mixed = self._create_seed_lot(mixed_name, row[QUANTITY], line, 'son')
#                         seed_lot_mixed.project.add(project)
#                         mixt_dict[row[GERMPLASM]] = {}
#                         mixt_dict[row[GERMPLASM]]['data_dict'] = {}
#                         mixt_dict[row[GERMPLASM]]['seedlot'] = seed_lot_mixed
# 
#                     
#                     """Mixture creation"""
#                     mixture_dict = {
#                                     'quantity':row[QUANTITY],
#                                     'split':row[SPLIT]
#                                     }
# 
#                     if row[GERMPLASM] not in mixt_dict.keys():
#                         mixture_event = Mixture.objects.create(date=row[EVENT_YEAR])
#                         mixt_dict[row[GERMPLASM]]['event'] = mixture_event
#                     else :
#                         mixture_event = mixt_dict[row[GERMPLASM]]['event']
#                     mixture_event.append_relation(seed_lot_to_mix, seed_lot_mixed, mixture_event, mixture_dict)
#                     for variable in variable_list:
#                         if variable.name in row.keys() and row[variable.name] != '':
#                             mixt_dict[row[GERMPLASM]]['data_dict'][variable.name] = row[variable.name]
#                             
#             for germplasm in mixt_dict.keys() :
#                 self._insert_data(mixt_dict[germplasm]['data_dict'], mixt_dict[germplasm]['event'], mixt_dict[germplasm]['lot_to_mix'], mixt_dict[germplasm]['seedlot'], None)
#                 mixture = mixt_dict[germplasm]['event']
#                 quantity_ini = 0
#                 for relation in mixture.relation_set.all():
#                     try:
#                         quantity_ini += int(relation.quantity)
#                     except:
#                         pass
#                 seedlot = mixt_dict[germplasm]['seedlot']
#                 seedlot.quantity_ini = quantity_ini
#                 seedlot.save()
#     
#     def _compute_reproduction(self, csvdatafile):
#         line = 0
#         project = None
#         mandatory_fields = [PROJECT, SOWN_YEAR, HARVESTED_YEAR, ID_SEED_LOT_SOWN, ETIQUETTE, SPLIT, QUANTITY_SOWN, QUANTITY_HARVESTED, BLOCK, X, Y]
#         sowed_lots = {}
#         if self._is_valid_format(csvdatafile, mandatory_fields, QUANTITY_SOWN):
#             variable_list = self._get_variable_list(copy.copy(csvdatafile.fieldnames), mandatory_fields)
#             self._check_variable_methods(variable_list)
#             
#             for row in csvdatafile:
#                 line += 1
#                 #CHECK REPRODUCTION
#                 project = self._check_project(row[PROJECT], project, line)
#                 if row[ID_SEED_LOT_SOWN] =='':
#                     self.error_messages["error_row"].append(_('line %s: seed lot cell is empty')%line)
#                     continue
#                 
#                 if row[X] == '' or row[Y] == '' :
#                     self.warning_messages['xy_missing'].append(_("line %s: X and/or Y is missing for the reproduction concerning")+" %s<br />"%(line,row[ID_SEED_LOT_SOWN]))
#                 
#                 if not self._check_date_validity(row[SOWN_YEAR], SOWN_YEAR, line): continue
#                 if not self._check_date_validity(row[HARVESTED_YEAR], HARVESTED_YEAR, line): continue
#                 if not self._check_date_logic(row[SOWN_YEAR], row[HARVESTED_YEAR], line, SOWN_YEAR, HARVESTED_YEAR): continue
#                 try:
#                     int(row[BLOCK])
#                 except ValueError:   
#                     self.error_messages["error_block"].append(_('line %s: block information is missing or is not an integer value')+'<br />'%line)
#                 #FIN CHECK REPRODUCTION
#                 
#                 event_date = "{0}-{1}".format(row[SOWN_YEAR],row[HARVESTED_YEAR])
#                 sowed_seedlot = self._get_or_create_seedlot(row[ID_SEED_LOT_SOWN], sowed_lots, project, None, line, 'father')
#                 if sowed_seedlot:
#                     current_relation = self._find_reproduction(sowed_seedlot, row[X], row[Y], event_date, line)
#                     harvested_seedlot = None
#                     reproduction_event, harvested_seedlot = self._update_or_create_reproduction(current_relation, row, sowed_seedlot, line)
#                     self._insert_data(row, reproduction_event, sowed_seedlot, harvested_seedlot, line)
#     
#     def _compute_selection(self, csvdatafile):
#         line = 0
#         project = None
#         mandatory_fields = [PROJECT, SOWN_YEAR, HARVESTED_YEAR, ID_SEED_LOT_SOWN, ETIQUETTE, BLOCK, X, Y, SELECTION_PERSON, SELECTION_QUANTITY_INI, SELECTION_NAME]           
#         sowed_lots = {}
#         if self._is_valid_format(csvdatafile, mandatory_fields, SELECTION_NAME):
#             variable_list = self._get_variable_list(copy.copy(csvdatafile.fieldnames), mandatory_fields)
#             self._check_variable_methods(variable_list)
#             
#             for row in csvdatafile:
#                 line += 1
#                 #CHECK SELECTION
#                 project = self._check_project(row[PROJECT], project, line)
#                 if row[X] == '' or row[Y] == '' :
#                     self.warning_messages['xy_missing'].append(_("line %s: X and/or Y is missing for the reproduction concerning")+" %s<br />"%(line,row[ID_SEED_LOT_SOWN]))
#                     if row[SELECTION_NAME] == '':
#                         self.error_messages["error_message"].append(_("line {0}: the field 'selection_name' can't be empty")+".<br>".format(line))
#                 if not self._check_date_validity(row[SOWN_YEAR], SOWN_YEAR, line): continue
#                 if not self._check_date_validity(row[HARVESTED_YEAR], HARVESTED_YEAR, line): continue
#                 if not self._check_date_logic(row[SOWN_YEAR], row[HARVESTED_YEAR], line, SOWN_YEAR, HARVESTED_YEAR): continue
#                 try:
#                     int(row[BLOCK])
#                 except ValueError:   
#                     self.error_messages["error_block"].append(_('line %s: block information is missing or is not an integer value')+'<br />'%line)
#                 if row[ID_SEED_LOT_SOWN] =='':
#                     self.error_messages["error_row"].append(_('line %s: seed lot cell is empty')%line)
#                     continue
#                 
#                 #self._check_selectionname_used()
#                 #FIN CHECK SELECTION
#                 
#                 event_date = row[SOWN_YEAR]+"-"+row[HARVESTED_YEAR]
#                 sowed_seedlot = self._get_or_create_seedlot(row[ID_SEED_LOT_SOWN], sowed_lots, project, None, line, 'father')
#                 current_relation = self._find_reproduction(sowed_seedlot, row[X], row[Y], event_date, line)
#                 reproduction_event, harvested_seedlot = self._update_or_create_reproduction(current_relation, row, sowed_seedlot, line)
#                 
#                 selected_seedlot = None
#                 if row[HARVESTED_YEAR] != '':
#                     selected_name = sowed_seedlot.germplasm.idgermplasm+'#'+row[SELECTION_NAME]+'_'+ sowed_seedlot.location.short_name + '_' + row[HARVESTED_YEAR]
#                     selected_seedlot = self._create_seed_lot(selected_name, row[SELECTION_QUANTITY_INI], line, 'son')
#                 selection_dict = {'selection_name':row[SELECTION_NAME],
#                                   'date':event_date,
#                                   'block':row[BLOCK],
#                                   'X':row[X],
#                                   'Y':row[Y],
#                                   'person':row[SELECTION_PERSON]
#                                   }
#                 
#                 selection_event = Selection.objects.create_selection(reproduction_event, sowed_seedlot, selected_seedlot, selection_dict)
#                 self._insert_data(row, selection_event, sowed_seedlot, selected_seedlot, line)
# 
#     def _compute_individual(self, csvdatafile):
#         line = 0
#         project = None
#         mandatory_fields = [PROJECT, SOWN_YEAR, HARVESTED_YEAR, ID_SEED_LOT_SOWN, SOWN_ETIQUETTE, ID_SEED_LOT_HARVESTED, HARVESTED_ETIQUETTE, BLOCK, X, Y, INDIVIDUALS,CORRELATION_GROUP]          
#         sowed_lots = {}
#         if self._is_valid_format(csvdatafile, mandatory_fields, INDIVIDUALS):
#             variable_list = self._get_variable_list(copy.copy(csvdatafile.fieldnames), mandatory_fields)
#             self._check_variable_methods(variable_list)
#             for row in csvdatafile:
#                 line += 1
#                 #CHECK INDIVIDUAL
#                 project = self._check_project(row[PROJECT], project, line)
#                 if row[X] == '' or row[Y] == '' :
#                     self.warning_messages['xy_missing'].append(_("line %s: X and/or Y is missing for the reproduction concerning")+" %s<br />"%(line,row[ID_SEED_LOT_SOWN]))
#             
#                 if not self._check_date_validity(row[SOWN_YEAR], SOWN_YEAR, line): continue
#                 if not self._check_date_validity(row[HARVESTED_YEAR], HARVESTED_YEAR, line): continue
#                 if not self._check_date_logic(row[SOWN_YEAR], row[HARVESTED_YEAR], line, SOWN_YEAR, HARVESTED_YEAR): continue
#                 try:
#                     int(row[BLOCK])
#                 except ValueError:   
#                     self.error_messages["error_block"].append(_('line %s: block information is missing or is not an integer value')+'<br />'%line)
#                 if row[ID_SEED_LOT_SOWN] =='':
#                     self.error_messages["error_row"].append(_('line %s: seed lot cell is empty')%line)
#                     continue
#                 #FIN CHECK INDIVIDUAL
#                 event_date = "{0}-{1}".format(row[SOWN_YEAR],row[HARVESTED_YEAR])
#                 sowed_seedlot = self._get_or_create_seedlot(row[ID_SEED_LOT_SOWN], sowed_lots, project, None, line, 'father')
#                 current_relation = self._find_reproduction(sowed_seedlot, row[X], row[Y], event_date, line)
#                 reproduction_event, harvested_seedlot = self._update_or_create_reproduction(current_relation, row, sowed_seedlot, line)
#                 self._insert_data(row, reproduction_event, sowed_seedlot, harvested_seedlot, line)
# 
#     def _is_valid_format(self, csvdatafile, mandatory_fields, checker_field):
#         if checker_field not in csvdatafile.fieldnames:
#             putative_format = 'unknown'
#             for checker in FORMAT_VALIDATOR.keys():
#                 if checker in csvdatafile.fieldnames:
#                     putative_format = FORMAT_VALIDATOR[checker]
#             self.error_messages["error_message"].append(_("You selected the {0} button but the file you submitted seems to be a {1} file").format(self.radio, putative_format))
#             return False
#         else:
#             validity = True
#             for field in mandatory_fields :
#                 if field not in csvdatafile.fieldnames:
#                     validity = False
#                     self.error_messages["error_message"].append(_('%s is missing in the file')+'.<br>' % field)
#             return validity
#         
#     def _check_project(self, project_name, current_project, line):
#         if project_name == '' : 
#             self.error_messages["error_project"].append(_("line %s: Project cell is empty")+"<br />"%line)
#             return None
#         if not current_project or current_project.project_name != project_name :
#             project, created = Project.objects.get_or_create(project_name=project_name,defaults={'start_date':datetime.date.today()})
#             return project
#         else :
#             return current_project
#     
#     def _get_variable_list(self, filefields, fields):
#         variable_list = filefields
#         for l in range(len(fields)) :
#             if fields[l] in variable_list:
#                 variable_list.remove(fields[l])
#             
#         return Variable.objects.filter(name__in=variable_list)
#     
#     def _check_variable_methods(self, variable_list):
#         for variable in variable_list:
#             if not self.variable_method_dict.has_key(variable):
#                 self.error_messages["variable_error"].append(_('The variable {0} has been found in the data file but not in the method file')+'<br>'.format(variable.name))
#         
#         for variable in self.variable_method_dict.keys():
#             if variable not in variable_list:
#                 self.error_messages["variable_error"].append(_('The variable {0} has been found in the method file but not in the data file')+'<br>'.format(variable.name))
# 
#     def _check_date_validity(self, event_date, field_name, line):
#         try:
#             intdate = int(event_date)
#             if intdate < datetime.date.today().year-150 or intdate > datetime.date.today().year+150:
#                 self.error_messages["error_sown_year"].append(_("line {0}: '{2}' in column '{1}' is an aberrant value")+"<br>".format(line, field_name, event_date))
#                 return False
#             return True
#         except ValueError:
#             self.error_messages["error_sown_year"].append(_("line {0}: '{2}' in column '{1}' is not a valid value")+"<br>".format(line, field_name, event_date))
#             return False
#             
#     def _check_date_logic(self, before, after, line, column_before, column_after):
#         try :
#             if int(before) > int(after):
#                 self.error_messages["error_message"].append(_('line {line}: {col1} ({year1}) can not be greater than or equal to {col2} ({year2})')+'.<br>'.format(line=line, col1=column_before, year1=before, col2=column_after, year2=after))
#                 return False
#             return True
#         except :
#             return False
#         
#     """Porté dans PersonManager"""
#     def _get_person_from_seedlotname(self, seedlotname, line):
#         try:
#             return Person.objects.get(short_name=seedlotname.split('_')[1])
#         except Person.DoesNotExist:
#             self.error_messages["person_error"].append(_('line')+' {line}: {person} <br>'.format(line=line, person=seedlotname.split('_')[1]))
#             return None
#         except IndexError :
#             self.error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot')+'<br>'.format(line=line, seedlot=seedlotname))
#             return None
#         except:
#             return None
#     
#     """Ajouté comme method static de Seed_lot"""
#     def _build_seedlot_name(self, name):
#         """does the seed lot with digit exist?no:error,yes use it"""
#         
#         """if len(seedlotname.split('_')) == 4:
#             seedlot = Seed_lot.objects.get(name=seedlotname)
#             return seedlotname"""
#             
#         if len(name.split('_')) == 3:        
#             result = Seed_lot.objects.filter(name__startswith=name).values_list('name',flat=True).order_by('-name')
#         elif len(name.split('_')) == 1:
#             result = Germplasm.objects.filter(idgermplasm__startswith=name).values_list('idgermplasm',flat=True).order_by('-idgermplasm')
#         if result:
#             last_digit = ""
#             last_digit = result[0].split('_')[-1]
#             last_digit =  int(last_digit)+1
#             return "%s_%.4d"%(name,last_digit)
#         else:
#             last_digit = 1
#             result = name
#             return "%s_%.4d"%(result,last_digit)
#     
#     """ Porté comme method static de Seed_lot"""
#     def _find_date_from_name(self,seedlotname):
#         date = seedlotname.split('_')[2]
#         return date
#     
#     """Porté comme method static de Germplasm"""
#     def _is_valid_idgermplasm(self, idgermplasm):
#         char_list=['#',' ','_',',',';']
#         for char in char_list:
#             try:
#                 idgermplasm.index(char)
#             except:
#                 pass
#             else :
#                 return False
#         return True
#     
#     
#     """Porté dans GermplasmManager"""
#     def _create_germplasm(self, seedlotname, person_obj):
#         
#         germplasm_full = seedlotname.split('_')[0]
#         germplasm = germplasm_full.split('#')[0]
#         if not self._is_valid_idgermplasm(germplasm) :
#             raise NameError(_("The germplasm name %s is not a valid name (should not contain space ( ), diez (#), coma (,), semicolon (;) or underscore (_))")%germplasm)
#         
#         (germplasm_object, created) = Germplasm.objects.get_or_create(idgermplasm = germplasm,
#                                                           defaults={'germplasm_short_name':'',
#                                                                     'person' : person_obj                                                                                                 
#                                                                     }) 
#         
#         return (germplasm_object, created)
#     
#     """Porté dans GermplasmManager"""
#     def _get_or_create_germplasm(self, seedlotname, person, line):
#         try :
#             (germplasm_obj, created) = self._create_germplasm(seedlotname, person)
#             return germplasm_obj
#         except NameError as ne:
#             self.error_messages["idgermplasm_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=ne.message))
#         if created == True:
#             self.warning_messages["warnings_germ"].append(germplasm_warnings(germplasm_obj, _('father/son'), line, self.menu_list))
#     
#     """Porté dans SeedlotManager"""
#     def _create_seed_lot(self, seedlotname, quantity_ini, line, label_creation):
#         father_person = self._get_person_from_seedlotname(seedlotname, line)
#         germplasm_object = self._get_or_create_germplasm(seedlotname, father_person, line)
#         date = self._find_date_from_name(seedlotname)
#         
#         build_name = self._build_seedlot_name(seedlotname)
#         seed_lot_obj= Seed_lot.objects.create(name=build_name,
#                                                          person=father_person,
#                                                          germplasm=germplasm_object,
#                                                          quantity_ini=quantity_ini,
#                                                          date = int(date))
# 
#         self.warning_messages["warnings_seed"].append(seed_lot_warnings(seed_lot_obj, label_creation , None, line))
#         self.warning_messages["warnings_not_quantity"].append(_('line')+' {line}: ({type}) {seedlot}<br>'.format(line=line, type=label_creation, seedlot=seed_lot_obj))
#         return seed_lot_obj
# 
#         
#     def _insert_data(self, dataline, event, father_seedlot, son_seedlot, line):
#         for variable in self.variable_method_dict.keys():
#             if variable.name in dataline.keys() and dataline[variable.name].strip() != "":
#                 
#                 if INDIVIDUALS not in dataline.keys() :
#                     data = Rawdata.objects.create(variable=variable,
#                                                                method=self.variable_method_dict[variable],
#                                                                raw_data=dataline[variable.name].strip())
#                 else :
#                     allgroups = list(map(str.strip, dataline[CORRELATION_GROUP].split(',')))
#                     group = None
#                     for g in allgroups:
#                         if variable.name in self.correlation_groups[g]:
#                             if not group:
#                                 group = g
#                             else:
#                                 self.error_messages["error_pheno_variable"].append(_("line {0}: raw data for variable {1} coud not belong to several groups {2} and {3}")+"<br>".format(line, variable.name, group, g ))
#                     data = Rawdata.objects.create(variable=variable,
#                                                                method=self.variable_method_dict[variable],
#                                                                raw_data=dataline[variable.name].strip(),
#                                                                individual=int(dataline[INDIVIDUALS]),
#                                                                group=group)
#                 
#                 if str(variable.type) == 'T1':
#                     for relation in event.relation_set.all() :
#                         data.relation.add(relation)
#                 elif str(variable.type) == 'T2':
#                     male_relations = event.relation_set.filter(is_male='M')
#                     for relation in male_relations :
#                         data.relation.add(relation)
#                 elif str(variable.type) == 'T3':
#                     female_relations = event.relation_set.filter(is_male='F')
#                     for relation in female_relations:
#                         data.relation.add(relation)
#                 elif str(variable.type) == 'T4':
#                     selection_relations = event.relation_set.filter(seed_lot_father=father_seedlot, seed_lot_son=son_seedlot)
#                     for relation in selection_relations:
#                         data.relation.add(relation)
#                 elif str(variable.type) == 'T5':
#                     mixture_relations = event.relation_set.filter(seed_lot_father=father_seedlot, seed_lot_son=son_seedlot)
#                     for relation in mixture_relations:
#                         data.relation.add(relation)
#                 elif str(variable.type) == 'T6':
#                     vrac_relations = event.relation_set.filter(selection__isnull=True)
#                     for relation in vrac_relations:
#                         data.relation.add(relation)
#                 elif str(variable.type) == 'T7':
#                     if type(father_seedlot) == list:
#                         for seedlot in father_seedlot:
#                             data.seed_lot.add(seedlot)
#                     else : 
#                         data.seed_lot.add(father_seedlot)
#                 elif str(variable.type) == 'T8':
#                     if type(son_seedlot) == list:
#                         for seedlot in son_seedlot:
#                             data.seed_lot.add(seedlot)
#                     else : 
#                         data.seed_lot.add(son_seedlot)
#                 elif str(variable.type) == 'T9':
#                     data.seed_lot.add(father_seedlot[0])
#                 elif str(variable.type) == 'T10':
#                     data.seed_lot.add(father_seedlot[1])
#                 elif str(variable.type) == 'T11':
#                     pass
# 
#     def _check_cross_date(self, sown_year, harvested_year, cross_year, line):
#         
#         check_date = []
#         #on vérifie que les dates sont au bon format (seulement si le test précédent ne passe pas
#         check_date.append(self._check_date_validity(sown_year, SOWN_YEAR, line))
#         check_date.append(self._check_date_validity(harvested_year, HARVESTED_YEAR, line))
#         check_date.append(self._check_date_validity(cross_year, CROSS_YEAR, line))
#         
#         check_date.append(self._check_date_logic(sown_year, harvested_year, line, SOWN_YEAR, HARVESTED_YEAR))
#         check_date.append(self._check_date_logic(sown_year, cross_year, line, SOWN_YEAR, CROSS_YEAR))
#         
#         return all(check_date)
#     
#     def _create_digit_gpnotcrossed(self):
#         not_crossed_germplasm = Germplasm.objects.filter(idgermplasm__startswith='NOT-CROSSED').order_by('-idgermplasm')
#         if not_crossed_germplasm:
#             last_digit = ""
#             last_digit = not_crossed_germplasm[0].idgermplasm.split('_')[-1][-4:]
#             last_digit =  int(last_digit)+1
#         else:
#             last_digit = 1
#         return "NOT-CROSSED%.4d"%last_digit
#     
#     def _find_reproduction(self, sowed_seedlot, x, y, date, line):
#         reproduction = None
#         try:
#             reproduction = Relation.objects.get(seed_lot_father=sowed_seedlot, X=x, Y=y, selection__isnull=True, reproduction__date=date)
#         except Relation.DoesNotExist:
#             pass
#         except Relation.MultipleObjectsReturned:
#             self.error_messages["error_block"].append(_("line {0}: Several reproductions have been found at the same coordinates ({1},{2}) on {3} farm").format(line,x,y,sowed_seedlot.location))
#         except Exception as e:
#             print(e)
#         return reproduction
#     
#     def _update_or_create_reproduction(self, current_relation, row, sowed_seedlot, line):
#         harvested_seedlot = None
#         event_date = row[SOWN_YEAR]+'-'+row[HARVESTED_YEAR]
#         if not current_relation:
#             if row[HARVESTED_YEAR] != '':
#                 harvested_name = sowed_seedlot.name.split('_')[0]+'_'+ sowed_seedlot.location.short_name + '_' + row[HARVESTED_YEAR]
#                 harvested_seedlot = self._create_seed_lot(harvested_name, row[QUANTITY_HARVESTED], line, 'son')
#             reproduction_dict = {'block': row[BLOCK],
#                                 'date' : event_date,
#                                 'X' : row[X],
#                                 'Y' : row[Y]
#                                       }
#             reproduction_event = Reproduction.objects.create_reproduction(sowed_seedlot, harvested_seedlot, reproduction_dict)              
#         else :
#             if row[HARVESTED_YEAR] != '' and current_relation.seed_lot_son == None:
#                 harvested_name = sowed_seedlot.name.split('_')[0]+'_'+ sowed_seedlot.location.short_name + '_' + row[HARVESTED_YEAR]
#                 harvested_seedlot = self._create_seed_lot(harvested_name, row[QUANTITY_HARVESTED], line)
#                 current_relation.seed_lot_son = harvested_seedlot
#                 current_relation.save()
#             reproduction_event = current_relation.reproduction
#         return reproduction_event, harvested_seedlot
