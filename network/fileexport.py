# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from entities.models import Seedlot, Species
from entities.forms import SimpleRequest
from actors.models import Location, Project
from network.models import Relation
from network.forms import LocationForm, MixtureNameForm
from myuser.cross_file_constants import DIFFUSION, REPRODUCTION, MIXTURE, CROSS, SELECTION, PHENOTYPIC, RUNING_REPRO
from network.factory import PrepareFileFactory

def _file_generator(post_data, FormObjectMoreInfo, file_type):
    """
    This view enable to generate a CSV file for a set of seed lots
     
    @param request: The request object send by Http
    @type request: HttpRequest object
    
    @param template: the template file used for the response
    @type request: string
    
    
     
    @return: A HttpResponse with corresponding template
    """

    template_data = {}
    if file_type == DIFFUSION :
        locationform = LocationForm()
        template_data['locationform'] = locationform
    elif file_type == MIXTURE :
        mixture_form = MixtureNameForm()
        template_data['mixtureform'] = mixture_form
    else :
        template_data['showiconlist'] = True
    if "finallist" in post_data :
        if FormObjectMoreInfo:
            moreinfo = FormObjectMoreInfo(data=post_data)
            if moreinfo.is_valid():
                stream = _stream_builder(post_data, file_type)
                return stream
            else :
                final_list = post_data.getlist("finallist")
                form = SimpleRequest(data=post_data)
                query, current_list = _compute_query_file_generator(form, post_data, file_type)
                if file_type == CROSS and len(final_list) == 1:
                    final_list = final_list[0].split('&')
                template_data.update({'form':form,
                                      'seedlots':query,
                                      'current_list':final_list,
                                      'moreinfo':moreinfo,
                                      'event_type':file_type})
                return template_data
        else :
            stream = _stream_builder(post_data, file_type)
            return stream
    else :
        if FormObjectMoreInfo :
            moreinfo = FormObjectMoreInfo()
        else :
            moreinfo = None
        form = SimpleRequest(data=post_data)
        query, current_list = _compute_query_file_generator(form, post_data, file_type)
        template_data.update({'form':form,
                        'seedlots':query,
                        'moreinfo':moreinfo,
                        'current_list':current_list,
                        'event_type':file_type})
        return template_data

def _stream_builder(post_data, event_type):

    factory = PrepareFileFactory(event_type, post_data)
    factory.write_headers()
    factory.write_data()
    return factory.get_stream()

def _compute_query_file_generator(form, post_data, filetype):
    query = None
    current_list = None
    if form.is_valid() :
        date = None
        location = None
        projects = None
        query = None
        species = None
        
        if "location" in post_data and post_data["location"] :
            location = Location.objects.get(id=int(post_data["location"]))
        if "projects" in post_data and post_data["projects"] :
            projects = Project.objects.get(id=int(post_data["projects"]))
        if "year" in post_data and post_data["year"] :
            date = int(post_data["year"])
        if "species" in post_data and post_data["species"] :
            species = Species.objects.get(id=int(post_data["species"]))
        
        if "current_list" in post_data :
            current_list = post_data.getlist("current_list")
        
        if filetype in [REPRODUCTION, CROSS, DIFFUSION, MIXTURE] :
            query = Seedlot.objects.simple_query(location, projects, date, species)
        elif filetype in [SELECTION, RUNING_REPRO, PHENOTYPIC]:
            query = Relation.objects.simple_query(filetype, location, projects, date, species)
    return query, current_list