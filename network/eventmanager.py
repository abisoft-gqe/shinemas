# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""
from __future__ import unicode_literals
from django.apps import apps


class EventManager():
    
    @staticmethod
    def create_diffusion_event(seedlot, seed_lot_received, project, location, event_year, quantity, split, quantity_ini, datalist):
        Seedlot = apps.get_model('entities','seedlot')
        Diffusion = apps.get_model('network','diffusion')
        seed_lot_sent = Seedlot.objects.get_or_create_seedlot(seedlot, quantity_ini, 0, None)

        if not seed_lot_received :
            selection_name = seed_lot_sent.get_selection_name()
            #create seedlot received
            if selection_name :
                future_name = seed_lot_sent.germplasm.idgermplasm + '#' + selection_name + '_' + location.short_name + '_' + event_year
            else :
                future_name = seed_lot_sent.germplasm.idgermplasm + '_' + location.short_name + '_' + event_year
            seed_lot_received = Seedlot.objects.create_seed_lot(future_name, quantity, 0, None)

        if seed_lot_received:
            #creating diffusion event 
            diffusion_dict = {'split' : split,
                              'date' : event_year,
                              'quantity':quantity,
                            }
            diffusion_relation = Diffusion.objects.create_diffusion(seed_lot_sent, seed_lot_received, diffusion_dict)
            for relation in diffusion_relation.relation_set.all():
                relation.project.add(project)
        
            EventManager.linkdata(diffusion_relation, datalist)
        
        return diffusion_relation
    
    @staticmethod
    def create_mixture_event():
        pass
    
    @staticmethod
    def create_cross_event(parents, cross_seedlot, cross_dict, child, eventdata ):
        Seedlot = apps.get_model('entities', 'seed_lot')
        Reproduction = apps.get_model('network','reproduction')
        
        male_seedlot = Seedlot.objects.get_or_create_seedlot(parents['male_seedlot'], parents['quantity_ini_male'], 0, None)
        female_seedlot = Seedlot.objects.get_or_create_seedlot(parents['female_seedlot'], parents['quantity_ini_female'], 0, None)
        
        if not cross_seedlot and cross_dict['end_date'] != "" :
            future_name = child['idgermplasm'] + '_' + male_seedlot.location.short_name + '_' + cross_dict['end_date']
            cross_seedlot = Seedlot.objects.create_seed_lot(future_name, child['harvested_quantity'], 2, child['species'])
        
        if cross_seedlot :
            cross = Reproduction.objects.create_cross(cross_dict, female_seedlot, male_seedlot, cross_seedlot)
            for relation in cross.relation_set.all():
                relation.project.add(cross_dict['project'])
            EventManager.linkdata(cross, eventdata)
        return cross
    
    @staticmethod
    def create_multiplication_event(seedlot, harvested_seedlot, project, quantity_ini, sowing_year, harvesting_year, \
                                    quantity, split, x_coord, y_coord, block, harvested_quantity, datalist):
        Seedlot = apps.get_model('entities','seedlot')
        Reproduction = apps.get_model('network','reproduction')
        seed_lot_sowed = Seedlot.objects.get_or_create_seedlot(seedlot, quantity_ini, 0, None)
        
        #if harvesting year is filled and harvesting seedlot doesn't exist we create one
        if not harvested_seedlot and harvesting_year != "" :
            future_name = seed_lot_sowed.germplasm.idgermplasm + '_' + seed_lot_sowed.location.short_name + '_' + harvesting_year
            harvested_seedlot = Seedlot.objects.create_seed_lot(future_name, harvested_quantity, 0, None)
        
        reproduction_dict = {'split' : split,
                                 'start_date' : int(sowing_year),
                                 'quantity':quantity,
                                 'block':block,
                                 'X':x_coord,
                                 'Y':y_coord                          
                            }
        if harvested_seedlot :
            #creating multiplication event 
            reproduction_dict['end_date'] = int(harvesting_year)
        else :
            #creating multiplication event 
            reproduction_dict['end_date'] = None
            
        reproduction_relation = Reproduction.objects.create_reproduction(seed_lot_sowed, harvested_seedlot, reproduction_dict)
        for relation in reproduction_relation.relation_set.all():
            relation.project.add(project)
    
        EventManager.linkdata(reproduction_relation,datalist)
        
        return reproduction_relation
    
    @staticmethod
    def create_selection_event(relation, selection_name, selection_person, selection_year, quantity, project, datalist):
        Seedlot = apps.get_model('entities','seedlot')
        Selection = apps.get_model('network','selection')
        
        selected_seed_lot_name = "{0}#{1}_{2}_{3}".format(relation.seed_lot_father.germplasm.idgermplasm, 
                                                          selection_name, 
                                                          relation.seed_lot_father.location.short_name, 
                                                          selection_year)
        selected_seed_lot = Seedlot.objects.create_seed_lot(selected_seed_lot_name, quantity, 0, None)
        
        selection_dict = {'person':selection_person,
                          'selection_name':selection_name,
                          'date':selection_year,
                          'block':relation.block,
                          'X':relation.X,
                          'Y':relation.Y
        
        }
        selection_relation, created = Selection.objects.create_selection(relation.reproduction, relation.seed_lot_father, selected_seed_lot, selection_dict)
        selection_relation.project.add(project)
        EventManager.linkdata(selection_relation.selection, datalist)
        
        return selection_relation
    
    @staticmethod
    def linkdata(event, datalist):
        for data in datalist:
            for relation in event.relation_set.all() :
                if str(data.variable.type) == 'T1' or (str(data.variable.type) == 'T2' and relation.is_male == 'M') or \
                (str(data.variable.type) == 'T3' and relation.is_male == 'F') or str(data.variable.type) == 'T4' or \
                (str(data.variable.type) == 'T6' and relation.reproduction_id != None and relation.selection_id == None) \
                or str(data.variable.type) == 'T11' :
                    data.relation.add(relation)
                elif str(data.variable.type) == 'T8':
                    data.seedlot.add(relation.seed_lot_son)
                elif str(data.variable.type) == 'T7' or (str(data.variable.type) == 'T9' and relation.is_male == 'M') or \
                    (str(data.variable.type) == 'T10' and relation.is_male == 'F'):
                    data.seedlot.add(relation.seed_lot_father)
   
    @staticmethod
    def search_events_todelete(starting_event, current_list):
        Relation = apps.get_model('network','relation')
        for r in starting_event.relation_set.all() :
            childs = Relation.objects.filter(seed_lot_father = r.seed_lot_son)
            for c in childs :
                if c.diffusion != None and c.diffusion not in current_list :
                    current_list.append(c.diffusion)
                    EventManager.search_events_todelete(c.diffusion, current_list)
                elif c.mixture != None and c.mixture not in current_list :
                    current_list.append(c.mixture)
                elif c.reproduction != None and c.is_male == 'X' and c.reproduction not in current_list :
                    current_list.append(c.reproduction)
                    EventManager.search_events_todelete(c.reproduction, current_list)
                elif c.reproduction != None and (c.is_male == 'M' or c.is_male == 'F') and c.reproduction not in current_list :
                    current_list.append(c.reproduction)
    