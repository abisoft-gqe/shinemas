# Generated by Django 3.2 on 2023-12-15 09:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('network', '0016_auto_20231214_1641'),
    ]

    operations = [
        migrations.AddConstraint(
            model_name='relation',
            constraint=models.UniqueConstraint(condition=models.Q(('is_male', 'X'), ('reproduction__isnull', False), ('selection__isnull', True)), fields=('seed_lot_father', 'block', 'X', 'Y'), name='unique_reproduction'),
        ),
        migrations.AddConstraint(
            model_name='relation',
            constraint=models.CheckConstraint(check=models.Q(('reproduction__isnull', False), ('mixture__isnull', False), ('diffusion__isnull', False), _connector='OR'), name='events_notnull_together'),
        ),
    ]
