# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-05-09 12:26
from __future__ import unicode_literals

from django.db import migrations, models

reproduction_dates = {}
mixture_dates = {}

def save_dates(apps, schema_editor):
    Reproduction = apps.get_model("network", "Reproduction")
    for r in Reproduction.objects.all():
        reproduction_dates[r.id] = r.date

    Mixture = apps.get_model("network", "Mixture")
    for m in Mixture.objects.all():
        mixture_dates[m.id] = m.date

def migrate_date(apps, schema_editor):
    Reproduction = apps.get_model("network", "Reproduction")
    for rid in reproduction_dates.keys() :
        r = Reproduction.objects.get(id=rid)
        datestr = reproduction_dates[rid]
        try :
            start_date = datestr.split('-')[0]
            end_date = datestr.split('-')[1]
            if start_date != '':
                r.start_date = int(start_date)
                r.save()
            if end_date != '':
                r.end_date = int(end_date)
                r.save()
        except:
            continue
        
    Mixture = apps.get_model("network", "Mixture")
    for mid in mixture_dates.keys() :
        m = Mixture.objects.get(id=mid)
        datestr = mixture_dates[mid]
        try :
            end_date = datestr.split('-')[1]
        except:
            try :
                m.date = int(datestr)
                m.save()
            except :
                continue
        else :
            if end_date != '':
                m.date = int(end_date)
                m.save()

class Migration(migrations.Migration):

    dependencies = [
        ('network', '0003_auto_20160504_1140'),
    ]

    operations = [
        migrations.RunPython(save_dates),
        migrations.RemoveField(
            model_name='reproduction',
            name='date',
        ),
        migrations.AddField(
            model_name='reproduction',
            name='end_date',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='reproduction',
            name='start_date',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='diffusion',
            name='date',
            field=models.IntegerField(null=True),
        ),
        migrations.RemoveField(
            model_name='mixture',
            name='date',
        ),
        migrations.AddField(
            model_name='mixture',
            name='date',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='selection',
            name='date',
            field=models.IntegerField(null=True),
        ),
        migrations.RunPython(migrate_date)
    ]
