# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-08-10 09:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('network', '0004_auto_20160509_1226'),
    ]

    operations = [
        migrations.AlterField(
            model_name='relation',
            name='is_male',
            field=models.CharField(blank=True, choices=[('M', 'Male'), ('F', 'Female'), ('X', 'Unknown')], max_length=2, null=True),
        ),
        migrations.AlterField(
            model_name='reproduction',
            name='realised',
            field=models.CharField(blank=True, choices=[('Y', 'Yes'), ('N', 'No'), ('X', 'Unknown')], max_length=2, null=True),
        ),
    ]
