# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-04-26 10:21
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('actors', '0001_initial'),
        ('entities', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Diffusion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.CharField(blank=True, max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Mixture',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.CharField(blank=True, max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Relation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField(blank=True, null=True)),
                ('is_male', models.CharField(blank=True, choices=[('M', 'Male'), ('F', 'Female'), ('X', 'Unknown')], max_length=2, null=True)),
                ('split', models.CharField(blank=True, max_length=1, null=True)),
                ('block', models.IntegerField(blank=True, max_length=10, null=True)),
                ('X', models.CharField(blank=True, max_length=10, null=True)),
                ('Y', models.CharField(blank=True, max_length=10, null=True)),
                ('diffusion', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='network.Diffusion')),
                ('mixture', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='network.Mixture')),
            ],
            options={
                'ordering': ('seed_lot_father',),
            },
        ),
        migrations.CreateModel(
            name='Reproduction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.CharField(blank=True, max_length=100, null=True)),
                ('kernel_number', models.IntegerField(blank=True, max_length=100, null=True)),
                ('cross_number', models.IntegerField(blank=True, max_length=100, null=True)),
                ('description', models.CharField(blank=True, max_length=200, null=True)),
                ('realised', models.CharField(blank=True, choices=[('Y', 'Yes'), ('N', 'No'), ('X', 'Unknown')], max_length=2, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Reproduction_method',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('reproduction_methode_name', models.CharField(blank=True, max_length=100, null=True)),
                ('description', models.CharField(blank=True, max_length=500, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Selection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('selection_name', models.CharField(blank=True, max_length=100, null=True)),
                ('date', models.CharField(blank=True, max_length=100, null=True)),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='actors.Person')),
            ],
        ),
        migrations.AddField(
            model_name='reproduction',
            name='reproduction_method',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='network.Reproduction_method'),
        ),
        migrations.AddField(
            model_name='relation',
            name='reproduction',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='network.Reproduction'),
        ),
        migrations.AddField(
            model_name='relation',
            name='seed_lot_father',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='lot_parent', to='entities.Seed_lot'),
        ),
        migrations.AddField(
            model_name='relation',
            name='seed_lot_son',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='lot_fils', to='entities.Seed_lot'),
        ),
        migrations.AddField(
            model_name='relation',
            name='selection',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='network.Selection'),
        ),
    ]
