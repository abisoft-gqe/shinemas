import networkx as nx
from bokeh.embed import components
from bokeh.io import show, output_file
from bokeh.plotting import figure, from_networkx, show
from bokeh.models import Circle, MultiLine, HoverTool, GraphRenderer, Ellipse, StaticLayoutProvider, TapTool, OpenURL, ColumnDataSource, NodesAndLinkedEdges, Legend, \
                        LegendItem
from bokeh.models.renderers import GlyphRenderer
from bokeh.events import MouseEnter
from bokeh.palettes import linear_palette
from colorcet import glasbey_dark, glasbey_light

from django.db.models import Q
from django.utils.translation import gettext_lazy as _

from entities.models import Seedlot

class NetworkGraph():
    
    EDGE_COLORS = {
            "Diffusion": '#ccffdd',
            "Reproduction": '#cce0ff',
            "Selection": '#ffff99',
            "Mixture": '#ffcccc',
            "Cross": '#ffffcc',
            "Merge": '#e6ccff',
            "Unknown event": "black"
            }
    
    def __init__(self, relation, seedlot, germplasm_obj, base_url):
        G = self._build_graph(relation, seedlot)
        plot = self._init_plot(germplasm_obj)
        self._build_renderer(G, plot, base_url)
    
    def _build_graph(self, relation, seedlot):
        
        qs = Seedlot.objects.select_related('location').filter(Q(relations_as_parent__in = relation)|Q(relations_as_child__in = relation)).values("location__short_name", "date").distinct()
        location_liste = list(set([p['location__short_name'] for p in qs]))
        
        # Création du graph
        G = nx.Graph()
        
        # Ajout des nœuds (seedlots) en fonction de l'emplacement (location) et de la date
        num_colors = len(location_liste)
        node_colors = linear_palette(glasbey_light, num_colors)
        for s in seedlot:
            G.add_node(s.id, **{'label':s.name, 'color':node_colors[s.location.id % num_colors],'id':s.id})
        
        # Ajout des arêtes (relations) 
        
        
        for r in relation:
            edge_relation = str(r.get_relation_type())
            G.add_edge(r.seed_lot_father.id, r.seed_lot_son.id, color=self.EDGE_COLORS[edge_relation], type=edge_relation)

        return G
    
    def _init_plot(self, germplasm_obj):
        # Création du rendu du graph avec Bokeh
        plot = figure(title=_("Seedlot network for {0}").format(germplasm_obj.name), x_range=(-1, 1), y_range=(-1, 1), height = 600, width = 1000)
        plot.axis.visible = False
        plot.xgrid.grid_line_color = None
        plot.ygrid.grid_line_color = None
        
        return plot
        
    def _build_renderer(self, G, plot, base_url):
        graph_renderer = GraphRenderer()
        graph_layout = nx.spring_layout(G)
        # Mise en place des positions des nœuds
        edge_indexes = {}
        i=0
        for e in zip(G.edges.data('color'), G.edges.data('type')) :
            if e[1][2] not in edge_indexes.keys() :
                edge_indexes[e[1][2]] = i
            i+=1
        
        tips = '''
        Name: @label
        '''

        #Ajout des noeuds
        hover = HoverTool(tooltips=tips)
        graph_renderer.node_renderer.data_source.data = dict(index=list(G.nodes()), label=[l[1] for l in G.nodes.data('label')], color=[l[1] for l in G.nodes.data('color')], id=[l[1] for l in G.nodes.data('id')])
        graph_renderer.node_renderer.glyph = Circle(radius=0.01, fill_color="color")
        graph_renderer.node_renderer.hover_glyph = Circle(radius=0.5, fill_color="yellow")
        #graph_renderer.node_renderer.selection_glyph = Circle(radius=0.5, fill_color="yellow")

        graph_renderer.edge_renderer.data_source.data = dict(start=[e[0] for e in G.edges()], end=[e[1] for e in G.edges()], color=[e[2] for e in G.edges.data('color')])
        graph_renderer.edge_renderer.glyph = MultiLine(line_color="color", line_alpha=0.8, line_width=3)
        graph_renderer.edge_renderer.hover_glyph = MultiLine(line_color="yellow", line_width=3)
        #graph_renderer.edge_renderer.data_source.data["color"] = edge_line_color
        
        
        #---------------------------------------- Affichage
        #affichage des noeuds et des liens
        graph_renderer.layout_provider = StaticLayoutProvider(graph_layout=graph_layout)
        graph_renderer.selection_policy = NodesAndLinkedEdges()
        graph_renderer.inspection_policy = NodesAndLinkedEdges()
        
        # Ajout des rendus au plot
        plot.renderers.append(graph_renderer)
        plot.add_tools(hover)
        
        # Ajout du TapTool avec le CircleRenderer
        seedloturl = "{0}/entities/seedlot/@id".format(base_url)
        taptool = TapTool(renderers=[graph_renderer])
        taptool.callback = OpenURL(url=seedloturl)
        plot.add_tools(taptool)
        
        #========Legend=========
        legend_items = [LegendItem(label="Seedlot (color = location)", renderers=[graph_renderer.node_renderer], index=0),]
        for relation_type, index in edge_indexes.items() :
            legend_items.append(LegendItem(label=relation_type, renderers=[graph_renderer.edge_renderer], index=index))
        legend = Legend(title= "Network Legend", location="top_left", items=legend_items)
        plot.add_layout(legend)
        
        # Affichage du graph
        self.script, self.div = components(plot)
        
    def get_components(self):
        return self.script, self.div
    
        