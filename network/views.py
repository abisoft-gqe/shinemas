# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Melanie Polart-Donat, Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""
import collections
import csv
import io
import copy
import datetime
import json

import networkx as nx
from bokeh.embed import components
from bokeh.io import show, output_file
from bokeh.plotting import figure, from_networkx, show
from bokeh.models import Circle, MultiLine, HoverTool, GraphRenderer, Ellipse, StaticLayoutProvider, TapTool, OpenURL, ColumnDataSource, NodesAndLinkedEdges
from bokeh.models.renderers import GlyphRenderer
from bokeh.palettes import linear_palette
from colorcet import glasbey_dark, b_linear_grey_0_100_c0

from django.http import Http404
from django.http import HttpResponseBadRequest
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, reverse
from django.contrib.auth.decorators import user_passes_test, login_required
from django.forms import modelformset_factory, formset_factory
from django.forms.utils import ErrorList
from django.core.exceptions import ValidationError
from django.db import transaction
from django.db.models import F, Q
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.contrib import messages

from entities.models import Seedlot, GermplasmType, Species, Germplasm
from actors.models import Person, Project
from network.models import Mixture, Relation, ReproductionMethod, Reproduction, Selection
from network.forms import ProjectForm, ReproductionMoreInfoForm, ExtendedMoreInfoForm, SelectionTest, IndividualForm, DeleteFromFile, SearchMixtureRelation, SearchDiffusionRelation, \
    SearchCrossRelation, SearchReproductionRelation, SearchSelectionRelation
from network.forms_newevents import NewDiffusionForm, NewReproductionForm, NewSelectionForm,\
    NewCrossForm
from network.filemanager import FileManager
from network.eventmanager import EventManager
from network.fileexport import _file_generator
from network.utils import is_not_already_merged, some_errors, get_input_seedlot, get_output_seedlot, convert_date, form_date
from myuser.cross_file_constants import DIFFUSION, REPRODUCTION, MIXTURE, CROSS, SELECTION, PHENOTYPIC, RUNING_REPRO, STORAGE, \
                                        SEEDLOT, GERMPLASM
from eppdata.models import Rawdata, Variable
from eppdata.forms import AddDataForm
from images.models import ImageLink
from myuser.utils import recode
from entities.forms import SimpleRequest, GermplasmSearchForm
from network.exceptions import MergeRepsException
from network.network_plot import NetworkGraph
from weather.models import WeatherServiceTokens

if settings.WEATHER:
    from weather.forms import QueryRelation
    from weather.models import WeatherStation
    from weather.request import meteorologic_request
    from weather import webservice
    from actors.models import Location
    #from geopy.distance import vincenty


@login_required
@user_passes_test(lambda u: u.is_superuser)
def merge_reps(request):
    """
    This view is called by JQuery function. It merges repetitions and return a 
    String giving the state of the merge action (success or error with message)
    
    TODO: tester si la method is_ajax
    
    @param request: The request object send by Http
    @type request: HttpRequest object
    """
    if request.method == "POST":
        id_project = request.POST["project"]
        
        try :
            project = Project.objects.get(id=int(id_project))
        except :
            return HttpResponseBadRequest(_("Invalid project"))
        else :
            #dans request.POST onva recupérer tous les seed lot checkés
            sonlist = Seedlot.get_list_from_request(request.POST)
            
    
            if len(sonlist) < 2:
                #On ne peut pas merger un seul seed lot ou 0 --> erreur
                return HttpResponseBadRequest(_("You need to merge at least 2 seed lots"))
            else :
                #pour merger les reps --> on ne créé pas de nouveau GP
                #on créé un nouveau SL, et N relation avec ce seed lot ainsi qu'un évenement de mixture
                
                #D'abord on créé un evenement de mixture
                #date = "%s-%s"%(int(sonlist[0].split('_')[2])-1,sonlist[0].split('_')[2])
                date = int(sonlist[0].split('_')[2])
                 
                try:
                    mixed_seed_lot = Mixture.objects.merge_reps(date, sonlist, project)
                except MergeRepsException as e:
                    return HttpResponseBadRequest(e.message)

            return HttpResponse("<br /><i>{0} : {1}</i>".format(_("Selected seed lots have been merged in a new seed lot"), mixed_seed_lot.name))
    else :
        raise Http404

@login_required
@user_passes_test(lambda u: u.is_superuser)
def reproduction_method_choice(request):
    """
    This view is called by a JQuery function. It affect a reproduction method to a set of reproductions. 
    It returns a String object corresponding to the state of this action (success or error with message)
        
    @param request: The request object send by Http
    @type request: HttpRequest object
    """
    if request.method == "POST" :
        for key in request.POST:
            if key.startswith('reproduction') :
                reproduction_id = int(key.split('_')[-1].strip())
                method_id = int(request.POST[key])
                method_obj = None
                try :
                    method_obj = ReproductionMethod.objects.get(id=method_id)
                except :
                    return HttpResponseBadRequest(_("The method you selected (%s) doesn't exist")%method_id)
                
                try :
                    reproduction_obj = Reproduction.objects.get(id=int(reproduction_id))
                except Exception as e :
                    return HttpResponseBadRequest(str(e))
                
                reproduction_obj.reproduction_method = method_obj
                try :
                    reproduction_obj.save()
                except :
                    return HttpResponseBadRequest(_("The following object ({0}) couldn't be saved in database").format(reproduction_obj))    
        return HttpResponse("<i><b>{0}</b></i>".format(_("The selected methods have been affected to the corresponding reproductions")))
    else :
        raise Http404

@login_required
@user_passes_test(lambda u: u.is_superuser)
def generate_cross_file(request):
    """
    This view enable to generate a CSV file (cross file) for a set of seed lots
    
    @param request: The request object send by Http
    @type request: HttpRequest object
    
    @return: A HttpResponse with corresponding template
    """
    template = 'network/generate_cross_file.html'
    if request.method=="POST":
        post_processing = _file_generator(request.POST, ReproductionMoreInfoForm, CROSS)
        if type(post_processing) is dict :
            return render(request, template, post_processing)
        elif type(post_processing) is str :
            response = HttpResponse(post_processing, content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename="'+str(CROSS)+'.csv"'
            return response
    else :
        form = SimpleRequest()
        return render(request, template, {'form':form, 'event_type':CROSS})

@login_required
@user_passes_test(lambda u: u.is_superuser)
def generate_reproduction_file(request):
    """
    This view enable to generate a CSV file (reproduction file) for a set of seed lots
    It calls the generic private function _file_generator
    
    @param request: The request object send by Http
    @type request: HttpRequest object
    
    @return: A HttpResponse with corresponding template
    """
    template = 'network/generate_reproduction_file.html'
    if request.method=="POST":
        post_processing = _file_generator(request.POST, ReproductionMoreInfoForm, REPRODUCTION)
        if type(post_processing) is dict :
            return render(request, template, post_processing)
        elif type(post_processing) is str :
            response = HttpResponse(post_processing, content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename="'+str(REPRODUCTION)+'.csv"'
            return response
    else :
        form = SimpleRequest()
        return render(request, template, {'form':form, 'event_type':REPRODUCTION})

@login_required
@user_passes_test(lambda u: u.is_superuser)
def generate_runingrepro_file(request):
    """
    This view enable to generate a CSV file (reproduction file) for a set of seed lots
    It calls the generic private function _file_generator
    
    @param request: The request object send by Http
    @type request: HttpRequest object
    
    @return: A HttpResponse with corresponding template
    """
    template = 'network/generate_runingrepro_file.html'
    if request.method=="POST":
        post_processing = _file_generator(request.POST, None, RUNING_REPRO)
        if type(post_processing) is dict :
            return render(request, template, post_processing)
        elif type(post_processing) is str :
            response = HttpResponse(post_processing, content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename="'+str(RUNING_REPRO)+'.csv"'
            return response
    else :
        form = SimpleRequest()
        return render(request, template, {'form':form, 'event_type':RUNING_REPRO})

@login_required
@user_passes_test(lambda u: u.is_superuser)
def generate_diffusion_file(request):
    """
    This view enable to generate a CSV file (diffusion file) for a set of seed lots
    It calls the generic private function _file_generator
     
    @param request: The request object send by Http
    @type request: HttpRequest object
     
    @return: A HttpResponse with corresponding template
    """
    template = 'network/generate_diffusion_file.html'
    if request.method=="POST":
        post_processing = _file_generator(request.POST, ExtendedMoreInfoForm, DIFFUSION)
        if type(post_processing) is dict :
            return render(request, template, post_processing)
        elif type(post_processing) is str :
            response = HttpResponse(post_processing, content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename="'+str(DIFFUSION)+'.csv"'
            return response
    else :
        form = SimpleRequest()
        return render(request, template, {'form':form, 'event_type':DIFFUSION})

@login_required
@user_passes_test(lambda u: u.is_superuser)
def generate_mixture_file(request):
    """
    This view enable to generate a CSV file (mixture file) for a set of seed lots
    It calls the generic private function _file_generator
     
    @param request: The request object send by Http
    @type request: HttpRequest object
     
    @return: A HttpResponse with corresponding template
    """
    template = 'network/generate_mixture_file.html'
    if request.method=="POST":
        post_processing = _file_generator(request.POST, ExtendedMoreInfoForm, MIXTURE)
        if type(post_processing) is dict :
            return render(request, template, post_processing)
        elif type(post_processing) is str :
            response = HttpResponse(post_processing, content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename="'+str(MIXTURE)+'.csv"'
            return response
    else :
        form = SimpleRequest()
        return render(request, template, {'form':form, 'event_type':MIXTURE})

@login_required
@user_passes_test(lambda u: u.is_superuser)
def generate_selection_file(request):
    """
    This view enable to generate a CSV file (selection file) for a set of multiplication
    It calls the generic private function _file_generator
    
    @param request: The request object send by Http
    @type request: HttpRequest object
    
    @return: A HttpResponse with corresponding template
    """
    template = 'network/generate_selection_file.html'
    if request.method=="POST":
        post_processing = _file_generator(request.POST, None, SELECTION)
        if type(post_processing) is dict :
            return render(request, template, post_processing)
        elif type(post_processing) is str :
            response = HttpResponse(post_processing, content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename="'+str(SELECTION)+'.csv"'
            return response
    else :
        form = SimpleRequest()
        return render(request, template, {'form':form, 'event_type':SELECTION})

@login_required
@user_passes_test(lambda u: u.is_superuser)
def generate_individual_file(request):
    """
    This view enable to generate a CSV file (individuals file) for a set of multiplication
    It calls the generic private function _file_generator
    
    @param request: The request object send by Http
    @type request: HttpRequest object
    
    @return: A HttpResponse with corresponding template
    """
    template = 'network/generate_individual_file.html'
    if request.method=="POST":
        post_processing = _file_generator(request.POST, IndividualForm, PHENOTYPIC)
        if type(post_processing) is dict :
            return render(request, template, post_processing)
        elif type(post_processing) is str :
            response = HttpResponse(post_processing, content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename="'+str(PHENOTYPIC)+'.csv"'
            return response
    else :
        form = SimpleRequest()
        return render(request, template, {'form':form, 'event_type':PHENOTYPIC})

@login_required
@user_passes_test(lambda u: u.is_superuser)
def export_empty_file(request, event_type):
    stream = ""
    if event_type == SELECTION:
        stream = "project\tsown_year\tharvested_year\tid_seed_lot_sown\tetiquette\tblock\tX\tY\tselection_person\tselection_quantity_ini\tselection_name"
    elif event_type == RUNING_REPRO or event_type == REPRODUCTION:
        stream = "project\tsown_year\tharvested_year\tid_seed_lot_sown\tintra_selection_name\tetiquette\tsplit\tquantity_sown\tquantity_harvested\tblock\tX\tY"
    elif event_type == PHENOTYPIC:
        stream = "project\tsown_year\tharvested_year\tid_seed_lot_sown\tintra_selection_name\tetiquette\tsplit\tquantity_sown\tquantity_harvested\tblock\tX\tY\tindividuals\tcorrelation_group"
    elif event_type == DIFFUSION :
        stream = "project\tlocation\tid_seed_lot\tetiquette\tevent_year\tsplit\tquantity"
    elif event_type == MIXTURE :
        stream = "project\tid_seed_lot\tetiquette\tsplit\tquantity\tgermplasm\tevent_year"
    elif  event_type == SEEDLOT :
        stream = ",".join(Seedlot.SEEDLOT_MANDATORY_FILE)
    elif  event_type == GERMPLASM :
        stream = ",".join(Germplasm.GERMPLASM_MANDATORY_FILE)
    elif event_type == CROSS:
        stream = "project\tsown_year\tharvested_year\tquantity_harvested\tcross_year\tcross_germplasm\tnumber_crosses\tkernel_number_F1\tmale_seed_lot\tmale_etiquette\tmale_split\tmale_quantity\tmale_block\tmale_X\tmale_Y\tfemale_seed_lot\tfemale_etiquette\tfemale_split\tfemale_quantity\tfemale_block\tfemale_X\tfemale_Y"
    elif event_type == STORAGE :
        stream = "seedlot\tlevel1\tlevel2\tlevel3\tlevel4"
    response = HttpResponse(stream, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="'+str(event_type)+'.csv"'
    return response

@login_required
@user_passes_test(lambda u: u.is_superuser)
def creatediffusion(request):

    if request.method == "GET":
        AddDataFormset = modelformset_factory(Rawdata, AddDataForm)
        return render(request,'network/newdiffusion.html',{'form':NewDiffusionForm(),'dataform':AddDataFormset(queryset=Rawdata.objects.none())})
    elif request.method == "POST":
        form = NewDiffusionForm(request.POST)
        AddDataFormset = modelformset_factory(Rawdata, AddDataForm)
        moredataformset = AddDataFormset(request.POST)

        if form.is_valid() and moredataformset.is_valid():
            seedlot_id = request.POST['seedlot']
            germplasm_id = request.POST['germplasm']
            location_id = request.POST['location']
            sl_year = request.POST['year']
            quantity_ini = request.POST['quantity_ini']
            
            received_id = request.POST['received_seedlot']
            project_id = request.POST['project']
            event_year = request.POST['event_year']
            receiver = request.POST['where']
            quantity = request.POST['quantity']
            splited = request.POST['splited']
            
            if quantity_ini == '' : quantity_ini = None
            if quantity == '' : quantity = None
            
            seedlot = get_input_seedlot(seedlot_id, location_id, germplasm_id, sl_year)
            received_seedlot = get_output_seedlot(received_id)

            if received_seedlot :
                location = received_seedlot.location
            else :
                location = Location.objects.get(id=receiver)
            
            project = Project.objects.get(id=project_id)

            try :
                with transaction.atomic():
                    eventdata = moredataformset.save(commit=False)
                    for data in eventdata :
                        data.last_modification_user = request.user
                        data.save()
                    diffusion = EventManager.create_diffusion_event(seedlot, received_seedlot, project, location, event_year, quantity, splited, quantity_ini, eventdata)
                    AddDataFormset = modelformset_factory(Rawdata, AddDataForm)
                    messages.add_message(request, messages.SUCCESS, "Insertion OK, a new diffusion has been created : {0}".format(diffusion))

                    return render(request,'network/newdiffusion.html',{'form':NewDiffusionForm(),'dataform':AddDataFormset(queryset=Rawdata.objects.none()),#'successmsg':successmessage, 
                                                                       'diffusion':diffusion})
            except ValidationError as e :
                errors = form._errors.setdefault(_("diffusion errors"), ErrorList())
                errors.append(e.message)
                return render(request,'network/newdiffusion.html',{'form':form,'dataform':moredataformset})
        else :
            return render(request,'network/newdiffusion.html',{'form':form,'dataform':moredataformset})
    else :
        return Http404

@login_required
@user_passes_test(lambda u: u.is_superuser)
def createreproduction(request):
    if request.method == "GET":
        AddDataFormset = modelformset_factory(Rawdata, AddDataForm)
        return render(request,'network/newreproduction.html',{'form':NewReproductionForm(),'dataform':AddDataFormset(queryset=Rawdata.objects.none())})
    elif request.method == "POST":
        form = NewReproductionForm(request.POST)
        AddDataFormset = modelformset_factory(Rawdata, AddDataForm)
        moredataformset = AddDataFormset(request.POST)
        if form.is_valid() and moredataformset.is_valid():
            print(request.POST)
            sown_seedlot_id = request.POST['seedlot']
            germplasm_id = request.POST['germplasm']
            location_id = request.POST['location']
            sl_year = request.POST['year']
            quantity_ini = request.POST['quantity_ini']
    
            project_id = request.POST['project']
            quantity = request.POST['quantity']
            splited = request.POST['splited']
            
            harvested_id = request.POST['harvested_seedlot']
            x_coord = request.POST['x']
            y_coord = request.POST['y']
            block = request.POST['block']
            harvested_quantity = request.POST['harvested_quantity']
            sowing_year = request.POST['sowing_year']
            harvesting_year = request.POST['harvesting_year']
            
            if quantity_ini == '' : quantity_ini = None
            if quantity == '' : quantity = None
            if harvested_quantity == '' : harvested_quantity = None
            
            seedlot = get_input_seedlot(sown_seedlot_id, location_id, germplasm_id, sl_year)
            harvested_seedlot = get_output_seedlot(harvested_id)
            project = Project.objects.get(id=project_id)
            
            try :
                with transaction.atomic():
                    eventdata = moredataformset.save(commit=False)
                    for data in eventdata :
                        data.last_modification_user = request.user
                        data.save()
                    reproduction = EventManager.create_multiplication_event(seedlot, harvested_seedlot, project, quantity_ini, sowing_year, \
                                                             harvesting_year, quantity, splited, x_coord, y_coord, block, harvested_quantity, eventdata)
                    AddDataFormset = modelformset_factory(Rawdata, AddDataForm)
                    messages.add_message(request, messages.SUCCESS, "Insertion OK, a new reproduction has been created : {0}".format(reproduction))
                    return render(request,'network/newreproduction.html',{'form':NewReproductionForm(),'dataform':AddDataFormset(queryset=Rawdata.objects.none())})
            except ValidationError as e :
                errors = form._errors.setdefault("multiplication errors", ErrorList())
                errors.append(e.message)
                return render(request,'network/newreproduction.html',{'form':form,'dataform':moredataformset})
        else :
            return render(request,'network/newreproduction.html',{'form':form,'dataform':moredataformset})
    else :
        return Http404

@login_required
@user_passes_test(lambda u: u.is_superuser)
def createselection(request):
    if request.method == "GET":
        AddDataFormset = modelformset_factory(Rawdata, AddDataForm)
        return render(request,'network/newselection.html',{'form':NewSelectionForm(),'dataform':AddDataFormset(queryset=Rawdata.objects.none())})
    elif request.method == "POST":
        form = NewSelectionForm(request.POST)
        AddDataFormset = modelformset_factory(Rawdata, AddDataForm)
        moredataformset = AddDataFormset(request.POST)
        if form.is_valid() and moredataformset.is_valid():
            relation_id = request.POST['relation']
            selection_year = request.POST['event_year']
            selection_person_id = request.POST['person']
            selection_name = request.POST['selection_name']
            quantity = request.POST['quantity_ini']
            project_id = request.POST['project']
            
            if quantity == '' : quantity = None
            
            relation = Relation.objects.get(id=relation_id)
            selection_person = Person.objects.get(id=selection_person_id)
            project = Project.objects.get(id=project_id)
            try :
                with transaction.atomic():
                    eventdata = moredataformset.save(commit=False)
                    for data in eventdata :
                        data.last_modification_user = request.user
                        data.save()
                    selection = EventManager.create_selection_event(relation, selection_name, selection_person, selection_year, quantity, project, eventdata)
                    AddDataFormset = modelformset_factory(Rawdata, AddDataForm)
                    messages.add_message(request, messages.SUCCESS, "Insertion OK, a new selection has been created : {0}".format(selection))
                    return render(request,'network/newselection.html',{'form':NewSelectionForm(),'dataform':AddDataFormset(queryset=Rawdata.objects.none())})
            except ValidationError as e :
                errors = form._errors.setdefault("selection errors", ErrorList())
                errors.append(e.message)
                return render(request,'network/newselection.html',{'form':form,'dataform':moredataformset})
        else :
            return render(request,'network/newselection.html',{'form':form,'dataform':moredataformset})

@login_required
@user_passes_test(lambda u: u.is_superuser)
def createcross(request):
    if request.method == "GET":
        AddDataFormset = modelformset_factory(Rawdata, AddDataForm)
        return render(request,'network/newcross.html',{'form':NewCrossForm(),'dataform':AddDataFormset(queryset=Rawdata.objects.none())})
    elif request.method == "POST":
        form = NewCrossForm(request.POST)
        AddDataFormset = modelformset_factory(Rawdata, AddDataForm)
        moredataformset = AddDataFormset(request.POST)
        if form.is_valid() and moredataformset.is_valid():
            """Get data from the request.POST"""
            male_seedlot_id = request.POST['seedlot_male']
            germplasm_id_male = request.POST['germplasm_male']
            location_id_male = request.POST['location_male']
            sl_year_male = request.POST['year_male']
            quantity_ini_male = request.POST['quantity_ini_male']
            male_seedlot = get_input_seedlot(male_seedlot_id, location_id_male, germplasm_id_male, sl_year_male)
            
            female_seedlot_id = request.POST['seedlot_female']
            germplasm_id_female = request.POST['germplasm_female']
            location_id_female = request.POST['location_female']
            sl_year_female = request.POST['year_female']
            quantity_ini_female = request.POST['quantity_ini_female']
            female_seedlot = get_input_seedlot(female_seedlot_id, location_id_female, germplasm_id_female, sl_year_female)
            
            harvested_id = request.POST['harvested_seedlot']
            cross_seedlot = get_output_seedlot(harvested_id)
            
            quantity_male = request.POST['quantity_male']
            splited_male = request.POST['splited_male']
            x_coord_male = request.POST['x_male']
            y_coord_male = request.POST['y_male']
            block_male = request.POST['block_male']
            
            quantity_female = request.POST['quantity_female']
            splited_female = request.POST['splited_female']
            x_coord_female = request.POST['x_female']
            y_coord_female = request.POST['y_female']
            block_female = request.POST['block_female']
            
            start_date = request.POST['sowing_year']
            end_date = request.POST['harvesting_year']
            project_id = request.POST['project']
            project = Project.objects.get(id=project_id)
            idgermplasm = request.POST['idgermplasm']
            cross_number = request.POST['cross_number']
            kernel_number = request.POST['kernel_number_f1']
            harvested_quantity = request.POST['harvested_quantity']
            
            if quantity_male == '' : quantity_male = None
            if quantity_female == '' : quantity_female = None
            if harvested_quantity == '' : harvested_quantity = None
            if quantity_ini_female == '' : quantity_ini_female = None
            if quantity_ini_male == '' : quantity_ini_male = None
            
            species_id = request.POST['species']
            if species_id :
                species = Species.objects.get(id=species_id)
            else :
                species = None
            
            try :
                with transaction.atomic():
                    eventdata = moredataformset.save(commit=False)
                    for data in eventdata :
                        data.last_modification_user = request.user
                        data.save()
                        
                    cross_dict = {
                            'kernel_number':kernel_number,
                            'cross_number':cross_number,
                            'female_quantity':quantity_female,
                            'male_quantity':quantity_male,
                            'female_split':splited_female,
                            'female_block':block_female,
                            'female_X':x_coord_female,
                            'female_Y':y_coord_female,
                            'male_split':splited_male,
                            'male_block':block_male,
                            'male_X':x_coord_male,
                            'male_Y':y_coord_male,
                            'end_date':end_date,
                            'start_date':start_date,
                            'project': project
                        }
                    
                    parents = {'male_seedlot':male_seedlot,
                               'quantity_ini_male': quantity_ini_male,
                               'female_seedlot':female_seedlot,
                               'quantity_ini_female':quantity_ini_female}
                    
                    child = {'idgermplasm':idgermplasm,
                             'harvested_quantity':harvested_quantity,
                             'species':species
                             }
                    cross= EventManager.create_cross_event(parents, cross_seedlot, cross_dict, child, eventdata)
                    AddDataFormset = modelformset_factory(Rawdata, AddDataForm)
                    messages.add_message(request, messages.SUCCESS, "Insertion OK, a new cross has been created : {0}".format(cross))
                    return render(request,'network/newcross.html',{'form':NewCrossForm(),'dataform':AddDataFormset(queryset=Rawdata.objects.none())})
            except ValidationError as e :
                errors = form._errors.setdefault("cross errors", ErrorList())
                errors.append(e.message)
                return render(request,'network/newcross.html',{'form':form,'dataform':moredataformset})
        else :
            return render(request,'network/newcross.html',{'form':form,'dataform':moredataformset})

@login_required
@user_passes_test(lambda u: u.is_superuser)
def createmixture(request):
    pass

@login_required
@user_passes_test(lambda u: u.is_superuser)
def selection_table(request):
    """
    This view enable to generate a table with selection name already used for each germplasm
    
    @param request: The request object send by Http
    @type request: HttpRequest object
    
    @return: A HttpResponse with corresponding template
    """
    if request.method=="GET":
        selectionform = SelectionTest()
        
        ordered_dict = collections.OrderedDict()
        selection_set = Selection.objects.prefetch_related('relation_set', 'relation_set__seed_lot_son__germplasm').all().order_by('relation__seed_lot_son__germplasm__idgermplasm', 'selection_name')
        for selection in selection_set:
            relations = selection.relation_set.all()
            for relation in relations:
                germplasm = relation.seed_lot_son.germplasm
                if germplasm not in ordered_dict.keys():
                    ordered_dict[germplasm] = []
                ordered_dict[germplasm].append(selection.selection_name)
        
        return render(request,'network/selection_table.html',{'selections':ordered_dict, 'testform':selectionform})
    else :
        raise Http404

@login_required
@user_passes_test(lambda u: u.is_superuser)
def merge_repetition(request):
    """
    This view enable to generate a list with repetition that haven't been already merged.
    Thus it generates a table in which you can click on 'Merge' button and call a JQuery function that merge the selected seed lot
    
    @param request: The request object send by Http
    @type request: HttpRequest object
    
    @return: A HttpResponse with corresponding template
    """
    if request.method=="GET":
        #We get all Relations implied in reps
        # queryset = Relation.objects.select_related('seed_lot_father__germplasm', 'seed_lot_son__germplasm').filter(reproduction__isnull=False, is_male = 'X').extra(
        #                                                                                                 tables=['"network_relation" AS "r2", "network_reproduction" AS "rp1", "network_reproduction" AS "rp2"'],
        #                                                                                                 where=['network_relation.seed_lot_father_id = r2.seed_lot_father_id AND network_relation.id <> r2.id AND network_relation.seed_lot_son_id <> r2.seed_lot_son_id AND \
        #                                                                                                 r2.reproduction_id IS NOT NULL AND network_relation.reproduction_id = rp1.id AND r2.reproduction_id = rp2.id AND \
        #                                                                                                 rp1.start_date = rp2.start_date AND rp1.end_date = rp2.end_date']
        #                                                                                           ).order_by('-seed_lot_father__date').distinct()
        
        #prefetch_relation = Prefetch('seed_lot_father__relations_as_parent',queryset=Relation.objects.prefetch_related('project').select_related('reproduction','seed_lot_son','seed_lot_father','seed_lot_son__germplasm__species','seed_lot_son__location').filter(reproduction__isnull=False, is_male = 'X', seed_lot_son__relations_as_parent__mixture_id__isnull=True), to_attr='sister_relation')
        queryset = Relation.objects.select_related('seed_lot_father', 'seed_lot_son', 'reproduction').filter(reproduction__isnull=False, is_male = 'X', reproduction__start_date=F('seed_lot_father__relations_as_parent__reproduction__start_date'), reproduction__end_date=F('seed_lot_father__relations_as_parent__reproduction__end_date')).exclude(id=F('seed_lot_father__relations_as_parent__id')).exclude(seed_lot_son__relations_as_parent__mixture_id__isnull=True, seed_lot_son__relations_as_parent__seed_lot_son__germplasm=F('seed_lot_son__germplasm')).order_by('-reproduction__start_date').distinct()
        
        #We change the query set in dict with key = seed_lot_father, value = a list of the relations
        repetiton_dict = collections.OrderedDict()
        for relation in queryset:
            #if is_not_already_merged(relation.seed_lot_son):
            if (relation.seed_lot_father, relation.reproduction.start_date) not in repetiton_dict.keys():
                repetiton_dict[(relation.seed_lot_father, relation.reproduction.start_date)] = []
            repetiton_dict[(relation.seed_lot_father, relation.reproduction.start_date)].append(relation)
        
        dict_to_return = copy.copy(repetiton_dict)
        for father in repetiton_dict.keys():
            if len(repetiton_dict[father]) <= 1 :
                dict_to_return.pop(father, None)
        formset = formset_factory(ProjectForm, extra=len(dict_to_return))
        data = zip(list(dict_to_return.items()), formset())

        return render(request,'network/merge_reps.html',{'data':data})
        
    return Http404

@login_required
def testselection(request):
    
    if request.is_ajax():
        idgermplasm = request.POST['germplasm']
        selection_name = request.POST['selection_name']
        qs = Selection.objects.filter(selection_name = selection_name, relation__seed_lot_son__germplasm__idgermplasm=idgermplasm)
        if qs.exists() :
            return HttpResponse("<b><i>"+_("The selection name '{0}' is already used for {1}").format(selection_name, idgermplasm)+"</i></b>")
        else :
            return HttpResponse("<b><i>"+_("The selection name '{0}' is not used for {1}").format(selection_name, idgermplasm)+"</i></b>")
    else :
        raise Http404

@login_required
def relation_profil(request, relation_id):
    try :
        #We get variables' informations linked to individuals
        relation = Relation.objects.prefetch_related('rawdatas','rawdatas__variable','rawdatas__method').get(id=relation_id)
        
        ind_variables = Variable.objects.filter(rawdata__relation=relation, type='T11').distinct()
        table_tr_info = []
        individual_results = []
        genre = ['Cross','Reproduction','Selection']
        #print("var" , ind_variables[0])
        """
            iteration on all rawdatas and build an ordered dict
        """
        for ind in relation.get_phenotyped_individual():
            data_set = relation.rawdatas.filter(individual=ind)
            table_tr_info = [ind]
            for v in ind_variables:
                variabledata = data_set.filter(variable=v)
                if variabledata:
                    #datastring = u""
                    #for d in variabledata:
                        #datastring = u"{0}  ({1}, {2})<br />{3}".format(d.raw_data, d.date, d.method, datastring)
                    #table_tr_info.append(datastring)
                    table_tr_info.append(variabledata)
                    #print("var" , variabledata)
                else:
                    table_tr_info.append('-')
            individual_results.append(table_tr_info)
            #print("table",individual_results[0])

        #We get global variables' informations (all except individual data)
        global_results = []
        glob_variables = Variable.objects.exclude(type__in=['T7', 'T8', 'T9', 'T10', 'T11'])
        data_set = relation.rawdatas.all()

        for v in glob_variables:
            print("global",v)
            variabledata = data_set.filter(variable=v)
            if variabledata:
                for d in variabledata:
                    global_results.append([d.variable,d,d.date,d.method]) 
                    print("var", d) 
                    print("res", d.variable,d,d.date,d.method)      
        entities_queryset = ImageLink.objects.filter(entity_name=str(relation))
        images = []
        for entity in entities_queryset:
            images.append(entity.image)
        
        
        if settings.WEATHER :
            father = relation.seed_lot_father.name.split("_")
            if relation.seed_lot_son :
                son = relation.seed_lot_son.name.split("_")
                end = son[2] + "-08-01"
            else :
                son = None
                end = str(int(father[2])+1) + "-08-01"
            srt = father[2] + "-10-01"
            
            p = request.GET.get("period")
            location = Location.objects.get(short_name=father[1])
            loc = (location.latitude,location.longitude)
            request.session['0'] = loc
            form = QueryRelation(initial={'start_date':srt,'end_date':end,'period':'d'})
            
            if request.method == "POST":
                
                try :
                    WebClass = getattr(webservice,settings.WEB_SERVICE)
                    service = WebClass()
                    if service.METHODE == "OAUTH" :
                        try :
                            token_object = WeatherServiceTokens.objects.get(user=request.user, service = service.SERVICE_NAME)
                            token_data = json.loads(token_object.information)
                            if token_data["expires_at"] < datetime.datetime.now().timestamp()*1000 :
                                print("token expired trying to get a new one")
                                return HttpResponseRedirect(reverse('weatheroauth'))
                            else :
                                service.set_token(token_data["access_token"])
                        except Exception as e:
                            print(e)
                            return HttpResponseRedirect(reverse('weatheroauth'))
                except :
                    messages.error(request, "Unavailable server or connection problem.")
                
                error_messages = {"error_date":[],"error_select_date":[]}
                 
                #if form.is_valid():
                dsrt = request.POST['start_date']
                date_srt = convert_date(dsrt)
                dend = request.POST['end_date']
                date_end = convert_date(dend)
                period = request.POST['period']
                meteo_variables = request.POST.getlist('var')
                stations = []
                if request.POST['station1']: 
                    stations.append(request.POST['station1'])
                if request.POST['station2']: 
                    stations.append(request.POST['station2'])
                if request.POST['station3']: 
                    stations.append(request.POST['station3'])
                
                # form = QueryRelation(initial={'start_date':dsrt,
                #                               'end_date':dend,
                #                               'period':period,
                #                               'var':meteo_variables,
                #                               'station1':stations[0],
                #                               'station2':stations[1],
                #                               'station3':stations[2]})
                
                form = QueryRelation(data=request.POST)
                
                
                t=[]
                i=0
                for e in ind_variables :
                    
                    t.append(e)
                    t.append(e.name+"$methode")
                    t.append(e.name+"$date")
                
                ind_variables=t
                if(form_date(dsrt) and form_date(dend)):
                    
                    # research meteorologic information
                    #(var_fin,var_null,vdescript,vunit,ndescript,station,data_fin) = meteorologic_request(ws,dsrt,date_srt,dend,date_end,period,vars,loc,station1,station2,station3)
                    meteo_data, variables, stations_obj = service.get_data(request.POST['start_date'], 
                             request.POST['end_date'],
                             request.POST['period'],
                             stations,
                             meteo_variables)
                        
                    return render(request, "network/relation_profil.html", {'relation':relation,
                                                                    'images':images,
                                                                    'ind_variables': ind_variables,
                                                                    'ind_results': individual_results,
                                                                    'glob_variables': glob_variables,
                                                                    'glob_results': global_results,
                                                                    'genre':genre,
                                                                    'form':form,
                                                                    'data':meteo_data.to_html(classes=['alternate',]),
                                                                    'variables': variables,
                                                                    'stations':stations_obj,
                                                                    'location':location})
                else :
                    messages.error(request,"The date is in a wrong format. It must be in the form yyyy-mm-dd.")
                    return render(request, "network/relation_profil.html", {'relation':relation,
                                                                'images':images,
                                                                'ind_variables': ind_variables,
                                                                'ind_results': individual_results,
                                                                'glob_variables': glob_variables,
                                                                'glob_results': global_results,
                                                                'genre':genre,
                                                                'form':form})
            else :
                t=[]
                i=0
                for e in ind_variables :
                    
                    t.append(e)
                    t.append(e.name+"$methode")
                    t.append(e.name+"$date")
                    
                ind_variables=t
                return render(request, "network/relation_profil.html", {'relation':relation,
                                                                'images':images,
                                                                'ind_variables': ind_variables,
                                                                'ind_results': individual_results,
                                                                'glob_variables': glob_variables,
                                                                'glob_results': global_results,
                                                                'genre':genre,
                                                                'form':form})
        else :
            t=[]
            i=0
            for e in ind_variables :
                
                t.append(e)
                t.append(e.name+"$methode")
                t.append(e.name+"$date")
                
            ind_variables=t
            return render(request, "network/relation_profil.html", {'relation':relation,
                                                                'images':images,
                                                                'ind_variables': ind_variables,
                                                                'ind_results': individual_results,
                                                                'glob_variables': glob_variables,
                                                                'glob_results': global_results,
                                                                'genre':genre})
    except ObjectDoesNotExist:
        raise HttpResponseBadRequest(_("No relation with id {0}".format(relation_id)))
        
    

@user_passes_test(lambda u: u.is_superuser)
@login_required
def uploadfile(request):

    if request.method == "POST":
        radio = request.POST['submit_type']
        file_name = None
        method_file = None
        germplasm_type = GermplasmType.objects.values_list('germplasm_type')
        rep_manager = {}
        try:
            file_name = request.FILES['file']
        except:
            pass
        
        try:
            method_file = request.FILES['second_file']
        except:
            pass
        
        filemanager = FileManager(radio, file_name, method_file)
        filemanager.read_and_save_data()
        error_messages = filemanager.get_error_messages()
        warning_messages = filemanager.get_warning_messages()
        if some_errors(error_messages):
            transaction.rollback()
            return render(request, 'network/error.html', {'error_message': error_messages["error_message"], 'person_error':error_messages["person_error"], 'sowing_block_error': error_messages["sowing_block_error"],
                                                                'error_son_germplasm':error_messages["error_son_germplasm"],'idgermplasm_error':error_messages["idgermplasm_error"], 'selct_error':error_messages["selct_error"],
                                                                'seed_lot_error':error_messages["seed_lot_error"], 'error_split':error_messages["error_split"],
                                                                'error_nomenclature':error_messages["error_nomenclature"], 'error_empty':error_messages["error_empty"],
                                                                'mixture_empty':error_messages["mixture_empty"], 'variable_error':error_messages["variable_error"],'error_project':error_messages["error_project"],
                                                                'pheno_grp_error':error_messages["pheno_grp_error"],'error_pheno_variable':error_messages["error_pheno_variable"],
                                                                'error_row':error_messages["error_row"],'split_string':error_messages["split_string"],'error_sown_year':error_messages["error_sown_year"],
                                                                'error_block':error_messages["error_block"],
                                                                'radio':radio, 'file_name':file_name})
            
        else:
            transaction.commit()
            return render(request, 'network/upload.html', {'file':file_name, 'warnings_var':warning_messages["warnings_var"],
                                                         'warnings_germ':warning_messages["warnings_germ"],'warning_diffusion_quantity':warning_messages["warning_diffusion_quantity"],
                                                         'warnings_seed':warning_messages["warnings_seed"], 'female_warnings_seed':warning_messages["female_warnings_seed"],
                                                         'male_warnings_seed':warning_messages["male_warnings_seed"], 'son_warnings_seed':warning_messages["son_warnings_seed"],
                                                         'warnings_relation':warning_messages["warnings_relation"], 'warnings_not_quantity':warning_messages["warnings_not_quantity"],
                                                         'created_person':warning_messages["created_person"],'male_relation':warning_messages["male_relation"],'xy_missing':warning_messages['xy_missing'],
                                                         'female_relation':warning_messages["female_relation"], 'germplasm_type':germplasm_type,'radio':radio, 'file_name':file_name,'reps':rep_manager})
    else :
        return render(request, 'network/upload.html', {})

@user_passes_test(lambda u: u.is_superuser)
@login_required
def delete_reproduction_fromfile(request):
    
    if request.method == "GET" :
        return render(request, 'network/delete_from_file.html',{'form':DeleteFromFile()})
    elif request.method == "POST" :
        form = DeleteFromFile(request.POST, request.FILES)
        if "delete_from_file" in request.POST :
            if form.is_valid() :
                reproduction_set = set()
                doesnotexist = set()
                delete_cascade = request.POST.get("delete_cascade")
                django_inmemoryfile = form.cleaned_data.get('delete_file')
                
                #TODO : use csv file reader from Mélanie
                stream_init = django_inmemoryfile.read()
                stream = recode(stream_init) #we call the recode function to avoid encoding errors             
                csvfile = io.StringIO(stream)
                dialect = csv.Sniffer().sniff(csvfile.read())
                csvfile.seek(0,0)
                csvstream = csv.DictReader(csvfile, dialect = dialect) 
                
                for row in csvstream :
                    try :
                        r = Reproduction.objects.get_from_file_row(row)
                    except Reproduction.DoesNotExist :
                        doesnotexist.add(_("There is no reproduction corresponding to {0} sowed in {1} and harvested in {5} at this coordinates block={2} X={3} Y={4} \
                                                                and selected with name \"{6}\"").format(row['id_seed_lot_sown'],
                                                                                                                        row['sown_year'],
                                                                                                                        row['block'], 
                                                                                                                        row['X'],
                                                                                                                        row['Y'],
                                                                                                                        row['harvested_year'],
                                                                                                                        row['intra_selection_name']))
                        continue
                    reproduction_set.add(r)
            return render(request, 'network/delete_from_file.html',{'reproduction_set': reproduction_set, 'doesnotexist':doesnotexist,'form': form, 'cascade': delete_cascade})
        elif "back_submit" in request.POST :
            return render(request, 'network/delete_from_file.html',{'form':form})
        elif "confirm_submit" in request.POST :
            reproductions = request.POST.getlist("reproduction_to_delete")
            delete_cascade =  request.POST["delete_cascade"]
            message_list = Reproduction.objects.delete_reproductions_set(reproductions, delete_cascade)
            for message in message_list :
                messages.add_message(request, messages.INFO, message)
            return render(request, 'network/delete_from_file.html',{})

def search_relation_by_type(request, type_relation):
    
    if request.method == "GET" :
        if type_relation == "mixture" :
            return render(request,'network/search_relation_by_type.html',{'form':SearchMixtureRelation()})
        elif type_relation == "diffusion" :
            return render(request,'network/search_relation_by_type.html',{'form':SearchDiffusionRelation()})
        elif type_relation == "cross" :
            return render(request,'network/search_relation_by_type.html',{'form':SearchCrossRelation()})
        elif type_relation == "reproduction" :
            return render(request,'network/search_relation_by_type.html',{'form':SearchReproductionRelation()})
        elif type_relation == "selection" :
            return render(request,'network/search_relation_by_type.html',{'form':SearchSelectionRelation()})
        else :
            return Http404()
    elif request.method == "POST" :
        if type_relation == "mixture" :
            relation_id = request.POST['mixture_relation']
            return HttpResponseRedirect(reverse('relation_profil',args=(relation_id,) ))
        elif type_relation == "diffusion" :
            relation_id = request.POST['diffusion_relation']
            return HttpResponseRedirect(reverse('relation_profil',args=(relation_id,) ))
        elif type_relation == "cross" :
            relation_id = request.POST['cross_relation']
            return HttpResponseRedirect(reverse('relation_profil',args=(relation_id,) ))
        elif type_relation == "reproduction" :
            relation_id = request.POST['reproduction_relation']
            return HttpResponseRedirect(reverse('relation_profil',args=(relation_id,) ))
        elif type_relation == "selection" :
            relation_id = request.POST['selection_relation']
            return HttpResponseRedirect(reverse('relation_profil',args=(relation_id,) ))
        else :
            return Http404()
        
def network_explorer(request):
    
    if request.method == 'GET':
        form_germplasm = GermplasmSearchForm()
        return render(request,'network/network_explorer.html',{'form_germplasm':form_germplasm})
    
    elif request.method == "POST":
        form_germplasm = GermplasmSearchForm(request.POST)
        
        if form_germplasm.is_valid():
            germplasm_id = request.POST.get('germplasm')

            try:
                germplasm_obj = Germplasm.objects.get(id=int(germplasm_id))
                
                """ Move this in a disctinct class"""
                relation = Relation.objects.filter(seed_lot_father__germplasm__name = germplasm_obj)
                seedlot = Seedlot.objects.filter(relations_as_parent__in = relation).union(Seedlot.objects.filter(relations_as_child__in = relation))
                base_url = "{0}://{1}".format(request.scheme, request.get_host())
                ng = NetworkGraph(relation, seedlot, germplasm_obj, base_url)
                script, div = ng.get_components()
            except ObjectDoesNotExist:
                return HttpResponseBadRequest(_("No Germplasm with id %s")%germplasm_id)
            else:
                return render(request, 'network/network_explorer.html',{
                                                                        'germplasm':germplasm_obj,
                                                                         'script':script,
                                                                         'reseau':div,
                                                                         'form_germplasm':form_germplasm
                                                                         })
        else:
            return render(request, 'network/network_explorer.html', {'form_germplasm':form_germplasm})    
