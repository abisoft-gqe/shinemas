# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.db import models
from django.db.models import Q
from django.apps import apps
from django.utils.translation import gettext_lazy as _

from myuser.cross_file_constants import RUNING_REPRO, PHENOTYPIC
from network.exceptions import MergeRepsException

class RelationManager(models.Manager):
    
    def advanced_query(self, query, variables_ids, params):
        Relation = apps.get_model('network','relation')
        relations = Relation.objects.filter(seed_lot_son__in=query).exclude(reproduction=None) #.exclude(is_male='M').exclude(is_male='F')
        if params['date_start'] :
            if bool(params['include_no_date']) :
                relations = relations.filter(Q(rawdatas__date__gte=params['date_start'])|Q(rawdatas__date__isnull=True))
            else :
                relations = relations.filter(rawdatas__date__gte=params['date_start'])
        
        if params['date_end'] :
            if bool(params['include_no_date']) :
                relations = relations.filter(Q(rawdatas__date__lte=params['date_end'])|Q(rawdatas__date__isnull=True))
            else :
                relations = relations.filter(rawdatas__date__lte=params['date_end'])
            
        if variables_ids :
            relations = relations.filter(rawdatas__variable__id__in = variables_ids)
        
        relations = relations.distinct()
        return relations
    
    def simple_query(self, filetype, location, project, date, species):
        if filetype in [RUNING_REPRO, PHENOTYPIC] :
            query = self.filter(reproduction__isnull = False, is_male='X')
        else :
            query = self.filter(selection__isnull = True, reproduction__isnull = False, is_male='X')
        if location :
            query = query.filter(seed_lot_father__location=location)
        if project :
            query = query.filter(project=project)
        if date :
            query = query.filter(reproduction__start_date = date)
        if species :
            query = query.filter(seed_lot_father__germplasm__species=species)
        return query
    

class DiffusionManager(models.Manager):
    
    def create_diffusion(self,seed_lot_obj,diff_seed_lot_obj,diffusion_dict):
        diffusion_relation = self.create(date = diffusion_dict['date'],)
        
        Relation = apps.get_model('network','Relation')
        relation = Relation.objects.create(
                           seed_lot_father = seed_lot_obj,
                           seed_lot_son = diff_seed_lot_obj,
                           diffusion = diffusion_relation,
                           is_male = 'X',
                           split = diffusion_dict['split']
                           )
        if 'quantity' in diffusion_dict.keys() and diffusion_dict['quantity'] != '':
            relation.quantity = diffusion_dict['quantity']
            relation.save()
        return diffusion_relation

class ReproductionManager(models.Manager):
    
    def create_cross(self,cross_reproduction_dict, female_obj, male_obj, son_obj):
                                                                        
        """ reproduction"""
        if cross_reproduction_dict['cross_number']==0:
            is_realised='N'
        elif cross_reproduction_dict['cross_number']== '':
            is_realised='X'
        else:
            is_realised='Y'

    
        if 'kernel_number' in cross_reproduction_dict.keys() and cross_reproduction_dict['kernel_number'] != '' \
            and 'cross_number' in cross_reproduction_dict.keys() and cross_reproduction_dict['cross_number'] != '' :
            reproduction_relation = self.create(
                    kernel_number = int(cross_reproduction_dict['kernel_number']),
                    cross_number = int(cross_reproduction_dict['cross_number']),
                    start_date = cross_reproduction_dict['start_date'],
                    end_date = cross_reproduction_dict['end_date'],
                    realised = is_realised
                    )
        else :
            reproduction_relation = self.create(
                    start_date = cross_reproduction_dict['start_date'],
                    end_date = cross_reproduction_dict['end_date'],
                    realised = is_realised
                    )
        female_quantity = None
        male_quantity = None
        if 'female_quantity' in cross_reproduction_dict.keys() and cross_reproduction_dict['female_quantity'] != '':
            female_quantity = int(cross_reproduction_dict['female_quantity'])
        elif 'male_quantity' in cross_reproduction_dict.keys() and cross_reproduction_dict['male_quantity'] != '':
            male_quantity = int(cross_reproduction_dict['male_quantity'])
            
        Relation = apps.get_model('network','Relation')
        Relation.objects.get_or_create(
                                          seed_lot_father = female_obj,
                                          seed_lot_son = son_obj,
                                          defaults={
                                          'reproduction' : reproduction_relation,
                                          'split' : cross_reproduction_dict['female_split'],
                                          'quantity': female_quantity,
                                          'is_male': 'F',
                                          #'block': int(cross_reproduction_dict['female_block']),
                                          'block': cross_reproduction_dict['female_block'],
                                          'X': cross_reproduction_dict['female_X'],
                                          'Y': cross_reproduction_dict['female_Y']
                                          }
                                          )
        
        Relation.objects.get_or_create(
                                          seed_lot_father = male_obj,
                                          seed_lot_son = son_obj,
                                          defaults={
                                          'reproduction' : reproduction_relation,
                                          'split' : cross_reproduction_dict['male_split'],
                                          'quantity': male_quantity,
                                          'is_male': 'M',
                                          #'block': int(cross_reproduction_dict['male_block']),
                                          'block': cross_reproduction_dict['male_block'],
                                          'X': cross_reproduction_dict['male_X'],
                                          'Y': cross_reproduction_dict['male_Y']
                                            }
                                          )
            
        return  reproduction_relation

    def create_reproduction(self, sowed_seedlot, harvested_seedlot, reproduction_dict):
        male='X'
        reproduction = self.create(description='reproduction',
                                   start_date = reproduction_dict['start_date'],
                                   end_date = reproduction_dict['end_date'])
        
        Relation = apps.get_model('network','Relation')
        Relation.objects.create(seed_lot_father = sowed_seedlot,
                                           seed_lot_son = harvested_seedlot,
                                           reproduction = reproduction,
                                           is_male = male,
                                           block= reproduction_dict['block'],
                                           X=reproduction_dict['X'],
                                           Y=reproduction_dict['Y'],
                                           quantity=reproduction_dict['quantity']
                                           )
        return reproduction

    def get_from_file_row(self, row):
        query_params = {
                'start_date':row['sown_year'],
                'relation__seed_lot_father__name':row['id_seed_lot_sown'],
                'relation__block':row['block'],
                'relation__X':row['X'],
                'relation__Y':row['Y'],
                    }
        
        if row['intra_selection_name'] == '' :
            query_params['relation__selection__isnull'] = True
        else :
            query_params['relation__selection__selection_name'] = row['intra_selection_name']
        if row['harvested_year'] == '' :
            query_params['end_date__isnull'] = True
        else :
            query_params['end_date'] = row['harvested_year']

        return self.get(**query_params)

    def delete_reproductions_set(self, reproductions, delete_cascade):
        messages = []
        for reproduction_id in reproductions :
            r = self.get(id=reproduction_id)
            if delete_cascade == "False" :
                messages.append(_("Reproduction {0} has been deleted successfully").format(str(r)))
                r.delete()
            elif delete_cascade == "True" :
                messages.append(_("Reproduction {0} has been deleted in cascade successfully").format(str(r)))
                r.delete_cascade()
            else :
                messages.append(("Error deleting {0}").format(r))
        return messages

class MixtureManager(models.Manager):
    def append_relation(self, seed_lot_to_mix, seed_lot_mixed, mixture_event, mixture_dict):
        X = 'X'
    
        """il faudrait creer dans la table mixture"""
        if 'split' in mixture_dict.keys() and mixture_dict['split'] != '':
            defs = {
                                                      'mixture' : mixture_event,
                                                      'quantity' : mixture_dict['mixture_quantity'],
                                                      'is_male' : X,
                                                      'split' : mixture_dict['split']
                                                      }
        else :
            defs = {
                                                  'mixture' : mixture_event,
                                                  'quantity' : mixture_dict['mixture_quantity'],
                                                  'is_male' : X,
                                                  }
        Relation = apps.get_model('network','Relation')
        Relation.objects.get_or_create(seed_lot_father = seed_lot_to_mix,
                                            seed_lot_son = seed_lot_mixed,
                                            defaults=defs)
    
    def merge_reps(self, date, sonlist, project):
        Seedlot = apps.get_model('entities', 'seedlot')
        Relation = apps.get_model('network','relation')
        #create appropriate exception to return the bad request messages 
        mixture = self.create(date=date)
        
        #Ensuite on créé le Seedlot résultat de la mixture
        
        #Raise an exception with message and captur it in the view to return the HttpResponseBadRequest
        try :
            sl = Seedlot.objects.get(name = sonlist[0])
        except :
            raise MergeRepsException(_("The seed lot {0} doesn't exist").format(sl.name))
        try :
            gp = sl.germplasm
        except :
            raise MergeRepsException(_("It seems the germplasm for this seed lot doesn't exist"))
        
        try :
            location = sl.location
        except :
            raise MergeRepsException(_("It seems the location for this seed lot doesn't exist"))
        
        #construction du nom
        new_sl_pattern = gp.idgermplasm+'_'+location.short_name+'_'+str(sl.date)
        
        
        mixed_seedlot = Seedlot.objects.create_seed_lot(new_sl_pattern, 0, 0, gp.species)
        
#         seedlot_dict = { 'date' : int(sonlist[0].split('_')[2]),
#                         'quantity_ini' : 0
#                         }
#         mixed_seedlot, created = create_seed_lot(seedlot_dict, location, gp, new_sl_name)
        #mixed_seedlot.project.add(project)
        q = 0
        for seed_lot in sonlist :
            sl = Seedlot.objects.get(name=seed_lot)
            
            quantity = None
            if sl.quantity_ini != '' and sl.quantity_ini != None :
                quantity = sl.quantity_ini
            r = Relation.objects.create(mixture = mixture,
                                    seed_lot_father = sl,
                                    seed_lot_son = mixed_seedlot,
                                    quantity = quantity,
                                    split= '0')
            #sl.project.add(project)
            r.project.add(project)
            if sl.quantity_ini :
                q += int(sl.quantity_ini)
        if q != 0 :
            mixed_seedlot.quantity_ini = q
            mixed_seedlot.save()
        mixed_seedlot.generation = sl.generation
        mixed_seedlot.onfarm_generation = sl.onfarm_generation
        mixed_seedlot.confidence = sl.confidence
        mixed_seedlot.onfarm_confidence = sl.onfarm_confidence
        mixed_seedlot.save()
        
        return mixed_seedlot

class SelectionManager(models.Manager):
    def create_selection(self, reproduction, sowed_seedlot, selected_seedlot, selection_dict):
        
        Person = apps.get_model('actors','Person')
        Relation = apps.get_model('network','Relation')
        
        if type(selected_seedlot) is Person :
            person = selection_dict['person']
        else :
            try:
                
                person = Person.objects.get(short_name = selection_dict['person'])
            except:
                person = None

        selection_relation = self.create(person = person,
                                        selection_name = selection_dict['selection_name'],
                                        date = selection_dict['date'])
        
        (relation,create) = Relation.objects.get_or_create(
                                               reproduction = reproduction,
                                               seed_lot_father = sowed_seedlot,
                                               seed_lot_son = selected_seedlot,
                                               selection = selection_relation,
                                               block  = selection_dict['block'],
                                               X=selection_dict['X'],
                                               Y=selection_dict['Y'],
                                               defaults={
                                               'is_male' : 'X',}
                                               )
        return (relation,create)
