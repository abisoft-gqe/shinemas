# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django.conf.urls import url
from django.template.response import TemplateResponse
from django.http.response import Http404
from django import forms

from import_export.admin import ImportExportModelAdmin
from network.models import Diffusion, ReproductionMethod, Relation, Reproduction, Selection, Mixture
from network.eventmanager import EventManager
from network.adminfilters import CrossOrReproductionFilter, DiffusionChildLocationFilter, DiffusionGermplasmFilter, DiffusionParentLocationFilter, \
MixtureChildGermplasmFilter, MixtureParentGermplasmFilter, SelectionGermplasmFilter, LocationFilter
from network.forms import RelationAdminForm

class EventAdmin():
    
    def event_response(self, request, obj):
        if request.method == "POST" :
            obj.delete_cascade()
            return self.response_delete(request, obj, obj.id)
        elif request.method == "GET" :
            to_delete = [obj,]
            EventManager.search_events_todelete(obj, to_delete)
            context = dict(
               self.admin_site.each_context(request),
               objects_list = to_delete,
               object_name = obj.__class__.__name__,
               original_object = obj
            )
            return TemplateResponse(request, "admin/network/deletecascade_confirmation.html", context)
        else :
            return Http404()
        
class MixtureAdmin(admin.ModelAdmin, EventAdmin):
    readonly_fields = ("date",)
    list_filter = ('date', MixtureParentGermplasmFilter, MixtureChildGermplasmFilter, LocationFilter)
    
    def get_urls(self):
        urls = super(MixtureAdmin, self).get_urls()
        my_urls = [
            url(r'^(?P<id_mixture>\d+)/deletecascade/$', self.admin_site.admin_view(self.cascadedelete), name="network_mixture_deletecascade_url"),
        ]
        return my_urls + urls

    def get_queryset(self, request):
        qs = super(MixtureAdmin, self).get_queryset(request)
        return qs.prefetch_related('relation_set__seed_lot_father', 'relation_set__seed_lot_son').all()

    def cascadedelete(self, request, id_mixture):
        m = Mixture.objects.get(id=id_mixture)
        return self.event_response(request, m)

class DiffusionAdmin(admin.ModelAdmin, EventAdmin):
    readonly_fields = ("date",)
    list_filter = ('date', DiffusionParentLocationFilter, DiffusionChildLocationFilter, DiffusionGermplasmFilter)
    
    def get_queryset(self, request):
        qs = super(DiffusionAdmin, self).get_queryset(request)
        return qs.prefetch_related('relation_set__seed_lot_father', 'relation_set__seed_lot_son').all()
    
    def get_urls(self):
        urls = super(DiffusionAdmin, self).get_urls()
        my_urls = [
            url(r'^(?P<id_diffusion>\d+)/deletecascade/$', self.admin_site.admin_view(self.cascadedelete), name="network_diffusion_deletecascade_url"),
        ]
        return my_urls + urls

    def cascadedelete(self, request, id_diffusion):
        d = Diffusion.objects.get(id=id_diffusion)
        return self.event_response(request, d)

class ReproductionMethodAdmin(admin.ModelAdmin):
    Fieldset = [
        (_('Reproduction method'), {'fields': ['reproduction_methode_name']}),
        (_('Description'), {'fields': ['description']}),
                      
        ]

class ReproductionAdmin(admin.ModelAdmin, EventAdmin):
    list_filter = ("start_date", "end_date", CrossOrReproductionFilter, LocationFilter, DiffusionGermplasmFilter)
    readonly_fields = ("start_date","end_date")
    
    def get_queryset(self, request):
        qs = super(ReproductionAdmin, self).get_queryset(request)
        return qs.prefetch_related('relation_set__seed_lot_father', 'relation_set__seed_lot_son').all()
    
    def get_urls(self):
        urls = super(ReproductionAdmin, self).get_urls()
        my_urls = [
            url(r'^(?P<id_reproduction>\d+)/deletecascade/$', self.admin_site.admin_view(self.cascadedelete), name="network_reproduction_deletecascade_url"),
        ]
        return my_urls + urls

    def cascadedelete(self, request, id_reproduction):
        r = Reproduction.objects.get(id=id_reproduction)
        return self.event_response(request, r)

class SelectionAdmin(admin.ModelAdmin, EventAdmin):
    list_filter = ("person","date", SelectionGermplasmFilter)
    readonly_fields = ("selection_name", "date")
    
    def get_queryset(self, request):
        qs = super(SelectionAdmin, self).get_queryset(request)
        return qs.prefetch_related('relation_set__seed_lot_father', 'relation_set__seed_lot_son').all()
    
    def get_urls(self):
        urls = super(SelectionAdmin, self).get_urls()
        my_urls = [
            url(r'^(?P<id_selection>\d+)/deletecascade/$', self.admin_site.admin_view(self.cascadedelete), name="network_selection_deletecascade_url"),
        ]
        return my_urls + urls

    def cascadedelete(self, request, id_selection):
        s = Selection.objects.get(id=id_selection)
        return self.event_response(request, s)
     
class RelationAdmin(admin.ModelAdmin):
    list_filter = ("seed_lot_father__location","seed_lot_son__location","seed_lot_father__germplasm","seed_lot_son__germplasm")
    form = RelationAdminForm
    
    def get_queryset(self, request):
        qs = super(RelationAdmin, self).get_queryset(request)
        return qs.select_related('seed_lot_father', 'seed_lot_son').all()

    """def get_form(self, request, obj, **kwargs):
        form = super(RelationAdmin,self).get_form(request, obj, **kwargs)
        form.base_fields['mixture'] = forms.ModelChoiceField(queryset=Mixture.objects.prefetch_related('relation_set__seed_lot_father', 'relation_set__seed_lot_son').all())
        form.base_fields['diffusion'] = forms.ModelChoiceField(queryset=Diffusion.objects.prefetch_related('relation_set__seed_lot_father', 'relation_set__seed_lot_son').all())
        form.base_fields['reproduction'] = forms.ModelChoiceField(queryset=Reproduction.objects.prefetch_related('relation_set__seed_lot_father', 'relation_set__seed_lot_son').all())
        form.base_fields['selection'] = forms.ModelChoiceField(queryset=Selection.objects.prefetch_related('relation_set__seed_lot_father', 'relation_set__seed_lot_son').all())
        return form"""

admin.site.register(Mixture, MixtureAdmin)
admin.site.register(Diffusion,DiffusionAdmin)
admin.site.register(ReproductionMethod,ReproductionMethodAdmin)
admin.site.register(Reproduction,ReproductionAdmin)
admin.site.register(Selection, SelectionAdmin)
admin.site.register(Relation,RelationAdmin)
