# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

import datetime

from django import forms
from django.utils.translation import gettext_lazy as _
from django.db.models import Q

from dal import autocomplete

from actors.models import Project, Location
from eppdata.models import Variable
from network.models import Relation

class ProjectForm(forms.Form):
    project = forms.ModelChoiceField(label=_("Projects"),queryset=Project.objects.all().order_by('project_name'),required=True)

class MoreInfoForm(forms.Form):
    """Generic Form used for file generation forms (additional data)"""
    project = forms.ModelChoiceField(label=_("Projects"),queryset=Project.objects.all().order_by('project_name'),required=True)

class ReproductionMoreInfoForm(MoreInfoForm):
    """This form is used in the generate_reproduction_file view.
    It add informations to seed lot list that will be sowed and allowed
    to complete informations that will be filled in the export file """
    tuple_vals = []
    for v in range(datetime.date.today().year - 10, datetime.date.today().year + 10) :
        tuple_vals.append((v,v))
    splited_vals = [(0,_("No")), (1,_("Yes")), (2,_("Unknown"))]
    
    sowed_year = forms.ChoiceField(label=_("Sowing year"),required=True,choices=tuple_vals, initial=datetime.date.today().year)
    harvested_year = forms.ChoiceField(label=_("Harvesting year"),required=True,choices=tuple_vals, initial=datetime.date.today().year+1)
    splited = forms.ChoiceField(label=_("Splited"),required=True,choices=splited_vals)

class ExtendedMoreInfoForm(MoreInfoForm):
    """This form is used in the generate_diffusion_file view and generate_mixture_view.
    It add informations to seed lot list that will be sowed and allowed
    to complete informations that will be filled in the export file """
    tuple_vals = []
    for v in range(datetime.date.today().year - 10, datetime.date.today().year + 10) :
        tuple_vals.append((v,v))
    splited_vals = [(0,_("No")), (1,_("Yes")), (2,_("Unknown"))]
    
    event_year = forms.ChoiceField(label=_("Event year"),required=True,choices=tuple_vals, initial=datetime.date.today().year)
    splited = forms.ChoiceField(label=_("Splited"),required=True,choices=splited_vals)

class LocationForm(forms.Form):
    """ A simple form class to get Person list as a Choice Field"""
    sendto = forms.ModelChoiceField(label=_("Send to "),queryset=Location.objects.all().order_by('short_name'),required=True)

class MixtureNameForm(forms.Form):
    """ A simple form class to get a textField for the new mixture name"""
    mixture_name = forms.CharField(label=_("Mixture name"),max_length=20, required=True)

class SelectionTest(forms.Form):
    germplasm = forms.CharField(label=_("Germplasm name"),max_length=150, required=True, widget=forms.TextInput(attrs={'size': 14, 'id':'autocomplete'}))
    selection_name = forms.CharField(label=_("Selection name"),max_length=8, required=True, widget=forms.TextInput(attrs={'size': 6, }))

class IndividualForm(forms.Form):
    individual = forms.IntegerField(label= _("Number of individuals"), required=True)

class RelationAdminForm(forms.ModelForm):
    class Meta:
        model = Relation
        fields = ('__all__')
        widgets = {
                'seed_lot_son': autocomplete.ModelSelect2(url='seedlot-autocomplete'),
                'seed_lot_father': autocomplete.ModelSelect2(url='seedlot-autocomplete'),
                'mixture': autocomplete.ModelSelect2(url='mixtureadmin-autocomplete'),
                'selection': autocomplete.ModelSelect2(url='selectionadmin-autocomplete'),
                'reproduction': autocomplete.ModelSelect2(url='reproductionadmin-autocomplete'),
                'diffusion': autocomplete.ModelSelect2(url='diffusionadmin-autocomplete'),
            }

class DeleteFromFile(forms.Form):
    delete_file = forms.FileField(label=_("Delete from file"))
    delete_cascade = forms.BooleanField(label=_("Delete in cascade"),required=False)
    
class SearchMixtureRelation(forms.Form):
    mixture_relation = forms.ModelChoiceField(
        queryset=Relation.objects.filter(mixture__isnull = False),
        widget=autocomplete.ModelSelect2(url='mixture-autocomplete', attrs={'style': 'width:350px', 'onchange': 'this.closest(\'form\').submit();'})
    )

class SearchDiffusionRelation(forms.Form):
    diffusion_relation = forms.ModelChoiceField(
        queryset=Relation.objects.filter(diffusion__isnull = False),
        widget=autocomplete.ModelSelect2(url='diffusion-autocomplete', attrs={'style': 'width:350px', 'onchange': 'this.closest(\'form\').submit();'})
    )

class SearchCrossRelation(forms.Form):
    cross_relation = forms.ModelChoiceField(
        queryset=Relation.objects.filter(Q(is_male='M')|Q(is_male='F'), reproduction__isnull = False, selection__isnull=True),
        widget=autocomplete.ModelSelect2(url='cross-autocomplete', attrs={'style': 'width:350px', 'onchange': 'this.closest(\'form\').submit();'})
    )

class SearchReproductionRelation(forms.Form):
    reproduction_relation = forms.ModelChoiceField(
        queryset=Relation.objects.filter(reproduction__isnull = False, selection__isnull=True, is_male='X'),
        widget=autocomplete.ModelSelect2(url='reproduction-autocomplete', attrs={'style': 'width:350px', 'onchange': 'this.closest(\'form\').submit();'})
    )

class SearchSelectionRelation(forms.Form):
    selection_relation = forms.ModelChoiceField(
        queryset=Relation.objects.filter(selection__isnull=False),
        widget=autocomplete.ModelSelect2(url='selection-autocomplete', attrs={'style': 'width:350px', 'onchange': 'this.closest(\'form\').submit();'})
    )
