# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from actors.models import Location
from entities.models import Germplasm


class DiffusionParentLocationFilter(admin.SimpleListFilter):
    title = _("sender")
    parameter_name = "sender"
    def lookups(self, request, model_admin):
        return Location.objects.order_by('short_name').values_list('short_name','short_name')
    
    def queryset(self, request, queryset):
        if self.value() :
            return queryset.filter(relation__seed_lot_father__location__short_name=self.value()).distinct()
        
class DiffusionChildLocationFilter(admin.SimpleListFilter):
    title = _("receiver")
    parameter_name = "receiver"
    def lookups(self, request, model_admin):
        return Location.objects.order_by('short_name').values_list('short_name','short_name')
    
    def queryset(self, request, queryset):
        if self.value() :
            return queryset.filter(relation__seed_lot_son__location__short_name=self.value()).distinct()

class DiffusionGermplasmFilter(admin.SimpleListFilter):
    title = _("germplasm")
    parameter_name = "germplasm"
    def lookups(self, request, model_admin):
        return Germplasm.objects.values_list('idgermplasm','idgermplasm')
    
    def queryset(self, request, queryset):
        if self.value() :
            return queryset.filter(relation__seed_lot_father__germplasm__idgermplasm=self.value()).distinct()

class MixtureParentGermplasmFilter(DiffusionGermplasmFilter):
    title = _("germplasm used")
    parameter_name = "gpused"

class MixtureChildGermplasmFilter(DiffusionGermplasmFilter):
    title = _("mixed germplasm")
    parameter_name = "mixgp"
    
    def queryset(self, request, queryset):
        if self.value() :
            return queryset.filter(relation__seed_lot_son__germplasm__idgermplasm=self.value()).distinct()

class SelectionGermplasmFilter(DiffusionGermplasmFilter):
    title = _("selected germplasm")
    parameter_name = "selectedgp"

class CrossOrReproductionFilter(admin.SimpleListFilter):
    title = _("type")
    parameter_name = "crossorrepro"
    def lookups(self, request, model_admin):
        repro_type = (
                      ('cross','Cross'),
                      ('multiplication','Multiplication')
                      )
        return repro_type
    
    def queryset(self, request, queryset):
        if self.value() == 'cross' :
            return queryset.filter(relation__is_male = 'M').distinct()
        if self.value() == 'multiplication' :
            return queryset.filter(relation__is_male = 'X').distinct()

class InputGermplasmFilter(MixtureParentGermplasmFilter):
    title = _("parent germplasm")
    parameter_name = "parentgp"

class OutputGermplasmFilter(MixtureChildGermplasmFilter):
    title = _("child germplasm")
    parameter_name = "childgp"

class LocationFilter(DiffusionParentLocationFilter):
    title = _("location")
    parameter_name = "location"

