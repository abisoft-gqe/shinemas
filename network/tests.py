# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.test import TestCase

from network.filemanager import FileManager

class FileManagerTest(TestCase):
    filemanager = None
    
    def setUp(self):
        self.filemanager = FileManager(None, None, None)

    def test_methods_nodb(self):
        self.assertEqual(self.filemanager._is_valid_idgermplasm("Un#Germplasm"), False)
        self.assertEqual(self.filemanager._is_valid_idgermplasm("Un Germplasm"), False)
        self.assertEqual(self.filemanager._is_valid_idgermplasm("Un_Germplasm"), False)
        self.assertEqual(self.filemanager._is_valid_idgermplasm("Un;Germplasm"), False)
        self.assertEqual(self.filemanager._is_valid_idgermplasm("Un,Germplasm"), False)
        self.assertEqual(self.filemanager._is_valid_idgermplasm("UnGermplasm"), True)
        
        self.assertEqual(self.filemanager._find_date_from_name("Germplasm_BER_2008_0001"), "2008")
        self.assertEqual(self.filemanager._find_date_from_name("Germplasm_BER_2007"), "2007")
        self.assertNotEqual(self.filemanager._find_date_from_name("Germplasm_BER_2008_0001"), "2009")
        
        self.assertEqual(self.filemanager._check_date_logic("2008", "2009", 25, "Before", "After"), True)
        self.assertEqual(self.filemanager._check_date_logic("2010", "2009", 25, "Before", "After"), False)
        self.assertEqual(self.filemanager._check_date_logic("2009", "2009", 25, "Before", "After"), True)
        self.assertEqual(self.filemanager._check_date_logic("Pas une date", "2009", 25, "Before", "After"), False)
        
        self.assertEqual(self.filemanager._check_date_validity("2008", "Year", 25), True)
        self.assertEqual(self.filemanager._check_date_validity("8", "Year", 25), False)
        self.assertEqual(self.filemanager._check_date_validity("Pas une date", "Year", 25), False)
