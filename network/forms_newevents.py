# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

import datetime

from django import forms
from django.utils.translation import gettext_lazy as _
from dal import autocomplete

from actors.models import Project, Person, Location
from entities.models import Seedlot, Germplasm, Species
from network.models import ReproductionMethod, Relation

class NewGenericForm(forms.Form):
    tuple_vals = [("","---------")]
    for v in range(datetime.date.today().year - 10, datetime.date.today().year + 10) :
        tuple_vals.append((v,v))
    splited_vals = [(0,_("No")), (1,_("Yes")), (2,_("Unknown"))]
    
    
    
    seedlot = forms.ModelChoiceField(label=_("Seed lot"),
                                     queryset=Seedlot.objects.all().order_by('name'),
                                     required=False,
                                     widget=autocomplete.ModelSelect2(url='seedlot-autocomplete', attrs={'style': 'width:300px'})
                                     )
    germplasm = forms.ModelChoiceField(
        label=_("Germplasm name"),
        queryset=Germplasm.objects.all(),
        widget=autocomplete.ModelSelect2(url='germplasm-autocomplete', attrs={'style': 'width:100px'}),
        required=False
    )
    location = forms.ModelChoiceField(label=_("Location"), required=False,queryset=Location.objects.all().order_by('short_name'))
    year = forms.ChoiceField(label=_("Seed lot year"),required=False,choices=tuple_vals)
    quantity_ini = forms.FloatField(label=_("Initial quantity"), required=False)

    project = forms.ModelChoiceField(label=_("Project"),queryset=Project.objects.all().order_by('project_name'))
    splited = forms.ChoiceField(label=_("Splited"),required=True,choices=splited_vals)

class NewDiffusionForm(NewGenericForm):
    
    received_seedlot = forms.ModelChoiceField(label=_("Seed lot"),
                                              queryset=Seedlot.objects.all().order_by('name'),
                                              required=False,
                                              widget=autocomplete.ModelSelect2(url='seedlot-autocomplete', attrs={'style': 'width:300px'})
                                              )
    event_year = forms.ChoiceField(label=_("Event year"), required=False, choices=NewGenericForm.tuple_vals, initial=datetime.date.today().year)
    where = forms.ModelChoiceField(label=_("Receiver"), required=False,queryset=Location.objects.all().order_by('short_name'))
    
    quantity = forms.FloatField(label=_("Quantity"), required=False)
    
    def clean(self):
        cleaned_data = super(NewDiffusionForm, self).clean()
        
        germplasm_field = cleaned_data.get("germplasm")
        location_field = cleaned_data.get("location")
        year_field = cleaned_data.get("year")
        seedlot_field = cleaned_data.get("seedlot")
        
        received_seedlot_field = cleaned_data.get("received_seedlot")
        where_field = cleaned_data.get("where")        
        event_year_field = cleaned_data.get("event_year")
        if seedlot_field is None and (germplasm_field is None or location_field is None or year_field == "") :
            raise forms.ValidationError(_("Seed lot diffused error : You must select a seed lot or fill information to create a new one"))
        
        if seedlot_field is not None and (germplasm_field != None or location_field is not None or year_field != "") :
            raise forms.ValidationError(_("Seed lot diffused error : You can't select an existing seed lot and fill information to create a new one"))
        
        if received_seedlot_field is None and (where_field is None or event_year_field == "") :
            raise forms.ValidationError(_("Seed lot received error : You must select a seed lot or fill information to create a new one"))     
        
        
        
        return cleaned_data

class NewReproductionForm(NewGenericForm):
    
    harvested_seedlot = forms.ModelChoiceField(label=_("Seed lot"),
                                               queryset=Seedlot.objects.all().order_by('name'),
                                               required=False,
                                               widget=autocomplete.ModelSelect2(url='seedlot-autocomplete', attrs={'style': 'width:300px'})
                                               )
    x = forms.CharField(label="X ",required=False,max_length=15, widget=forms.TextInput(attrs={'size': 6,}))
    y = forms.CharField(label="Y ",required=False,max_length=15, widget=forms.TextInput(attrs={'size': 6,}))
    block  = forms.CharField(label=_("Block"),max_length=15, widget=forms.TextInput(attrs={'size': 6,}), initial=1)
    sowing_year = forms.ChoiceField(label=_("Sowing year"),choices=NewGenericForm.tuple_vals, initial=datetime.date.today().year)
    harvesting_year = forms.ChoiceField(label=_("Harvesting year"),required=False,choices=NewGenericForm.tuple_vals, initial=datetime.date.today().year+1)
    harvested_quantity = forms.FloatField(label=_("Harvested quantity"), required=False)
    reproduction_method = forms.ModelChoiceField(label=_("Reproduction method"),queryset=ReproductionMethod.objects.all().order_by('reproduction_methode_name'), required=False)
    quantity = forms.FloatField(label=_("Sown quantity"), required=False)

    def clean(self):
        cleaned_data = super(NewReproductionForm, self).clean()
        
        germplasm_field = cleaned_data.get("germplasm")
        location_field = cleaned_data.get("location")
        year_field = cleaned_data.get("year")
        seedlot_sown = cleaned_data.get("seedlot")
        
        received_seedlot_field = cleaned_data.get("harvested_seedlot")      
        sowing_year_field = cleaned_data.get("sowing_year")
        harvesting_year_field = cleaned_data.get("harvesting_year")
        
        if seedlot_sown is None and (germplasm_field is None or location_field is None or year_field == "") :
            raise forms.ValidationError(_("Seed lot sown error : You must select a seed lot or fill information to create a new one"))
        
        if seedlot_sown is not None and (germplasm_field is not None or location_field is not None or year_field != "") :
            raise forms.ValidationError(_("Seed lot sown error : You can't select an existing seed lot and fill information to create a new one"))
        
        if received_seedlot_field is None and sowing_year_field == "" :
            raise forms.ValidationError(_("Seed lot harvested error : You must select a seed lot that has been harvested or fill sowing year information"))

        if received_seedlot_field and str(received_seedlot_field.date) != harvesting_year_field :
            raise forms.ValidationError(_("Harvesting year error : The creation date of the harvested seed lot must be the same as the harvesting year"))
        
        return cleaned_data

class NewSelectionForm(forms.Form):
    tuple_vals = []
    for v in range(datetime.date.today().year - 10, datetime.date.today().year + 10) :
        tuple_vals.append((v,v))
    splited_vals = [(0,_("No")), (1,_("Yes")), (2,_("Unknown"))]
    
    relation = forms.ModelChoiceField(
        queryset=Relation.objects.all(),
        widget=autocomplete.ModelSelect2(url='reproduction-autocomplete', attrs={'style': 'width:350px'})
    )
    event_year = forms.ChoiceField(label=_("Event year"), choices=tuple_vals, initial=datetime.date.today().year)
    selection_name = forms.CharField(label=_("Selection name"))
    person = forms.ModelChoiceField(label=_("Breeder"), required=False, queryset=Person.objects.all())
    quantity_ini = forms.FloatField(label=_("Initial quantity"), required=False)
    project = forms.ModelChoiceField(label=_("Project"),queryset=Project.objects.all().order_by('project_name'))

class NewCrossForm(forms.Form):
    tuple_vals = []
    for v in range(datetime.date.today().year - 10, datetime.date.today().year + 10) :
        tuple_vals.append((v,v))
    splited_vals = [(0,_("No")), (1,_("Yes")), (2,_("Unknown"))]
    
    seedlot_male = forms.ModelChoiceField(
        label=_("Male seed lot"),
        queryset=Seedlot.objects.all(),
        widget=autocomplete.ModelSelect2(url='seedlot-autocomplete', attrs={'style': 'width:300px'}),
        required=False
    )
    germplasm_male = forms.ModelChoiceField(
        label=_("Germplasm name"),
        queryset=Germplasm.objects.all(),
        widget=autocomplete.ModelSelect2(url='germplasm-autocomplete', attrs={'style': 'width:100px'}),
        required=False
    )
    location_male = forms.ModelChoiceField(label=_("Location"), required=False,queryset=Location.objects.all().order_by('short_name'))
    year_male = forms.ChoiceField(label=_("Seed lot year"),required=False,choices=tuple_vals)
    quantity_ini_male = forms.FloatField(label=_("Initial quantity"), required=False)
    x_male = forms.CharField(label="X ",required=False,max_length=15, widget=forms.TextInput(attrs={'size': 6,}))
    y_male = forms.CharField(label="Y ",required=False,max_length=15, widget=forms.TextInput(attrs={'size': 6,}))
    block_male  = forms.CharField(label=_("Block"),max_length=15, widget=forms.TextInput(attrs={'size': 6,}), initial=1)
    splited_male = forms.ChoiceField(label=_("Splited"),required=True,choices=splited_vals)
    quantity_male = forms.FloatField(label=_("Sown quantity"), required=False)
    
    seedlot_female = forms.ModelChoiceField(
        label=_("Female seed lot"),
        queryset=Seedlot.objects.all(),
        widget=autocomplete.ModelSelect2(url='seedlot-autocomplete', attrs={'style': 'width:300px'}),
        required=False
    )
    germplasm_female = forms.ModelChoiceField(
        label=_("Germplasm name"),
        queryset=Germplasm.objects.all(),
        widget=autocomplete.ModelSelect2(url='germplasm-autocomplete', attrs={'style': 'width:100px'}),
        required=False
    )
    location_female = forms.ModelChoiceField(label=_("Location"), required=False,queryset=Location.objects.all().order_by('short_name'))
    year_female = forms.ChoiceField(label=_("Seed lot year"),required=False,choices=tuple_vals)
    quantity_ini_female = forms.FloatField(label=_("Initial quantity"), required=False)
    x_female = forms.CharField(label="X ",required=False,max_length=15, widget=forms.TextInput(attrs={'size': 6,}))
    y_female = forms.CharField(label="Y ",required=False,max_length=15, widget=forms.TextInput(attrs={'size': 6,}))
    block_female  = forms.CharField(label=_("Block"),max_length=15, widget=forms.TextInput(attrs={'size': 6,}), initial=1)
    splited_female = forms.ChoiceField(label=_("Splited"),required=True,choices=splited_vals)
    quantity_female = forms.FloatField(label=_("Sown quantity"), required=False)
    
    harvested_seedlot = forms.ModelChoiceField(
        label=_("Harvested seed lot"),
        queryset=Seedlot.objects.all(),
        widget=autocomplete.ModelSelect2(url='seedlot-autocomplete', attrs={'style': 'width:300px'}),
        required=False
    )
    sowing_year = forms.ChoiceField(label=_("Sowing year"), choices=NewGenericForm.tuple_vals, initial=datetime.date.today().year)
    harvesting_year = forms.ChoiceField(label=_("Harvesting year"), required=False, choices=NewGenericForm.tuple_vals, initial=datetime.date.today().year+1)
    harvested_quantity = forms.FloatField(label=_("Harvested quantity"), required=False)
    project = forms.ModelChoiceField(label=_("Project"), queryset=Project.objects.all().order_by('project_name'))
    idgermplasm = forms.CharField(label=_("Germplasm name"), max_length=100, required=False)
    cross_number = forms.CharField(label=_("Cross number"), max_length=5, required=False)
    kernel_number_f1 = forms.CharField(label=_("Kernel number F1"), max_length=5, required=False)
    species = forms.ModelChoiceField(label=_("Species"), queryset=Species.objects.all(), required=False)
    
#     def clean(self):
#         cleaned_data = super(NewCrossForm, self).clean()
#         
#         seedlot_male = cleaned_data['seedlot_male']
    
class NewMixtureForm(forms.Form):
    tuple_vals = []
    for v in range(datetime.date.today().year - 10, datetime.date.today().year + 10) :
        tuple_vals.append((v,v))
    
    seed_lot = forms.ModelMultipleChoiceField(label='Seed lot',required=False,
                                  queryset=Seedlot.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='seedlot-autocomplete'))
    
    germplasm = forms.ModelChoiceField(
        label=_("Germplasm name"),
        queryset=Germplasm.objects.all(),
        widget=autocomplete.ModelSelect2(url='germplasm-autocomplete', attrs={'style': 'width:100px'}),
        required=False
    )
    location = forms.ModelChoiceField(label=_("Location"), required=False,queryset=Location.objects.all().order_by('short_name'))
    year = forms.ChoiceField(label=_("Seed lot year"),required=False,choices=tuple_vals)
    mixture_name = forms.CharField(label=_("Mixture name"), max_length=100, required=False)
    mixed_seed_lot = forms.ModelChoiceField(label='Mixed seed lot',required=False,
                                  queryset=Seedlot.objects.all(),
                                  widget=autocomplete.ModelSelect2(url='seedlot-autocomplete'))
