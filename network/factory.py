from io import StringIO

from myuser.cross_file_constants import DIFFUSION, SELECTION, MIXTURE, REPRODUCTION, RUNING_REPRO, PHENOTYPIC, CROSS
from network.models import Relation
from actors.models import Project

HEADERS = {
        SELECTION:["project","sown_year","harvested_year","id_seed_lot_sown","etiquette","block","X","Y","selection_person","selection_quantity_ini","selection_name"],
        DIFFUSION:["project","location","id_seed_lot","etiquette","event_year","split","quantity"],
        MIXTURE:["project","id_seed_lot","etiquette","split","quantity","germplasm","event_year"],
        REPRODUCTION:["project","sown_year","harvested_year","id_seed_lot_sown","intra_selection_name","etiquette","split","quantity_sown","quantity_harvested","block","X","Y"],
        RUNING_REPRO:["project","sown_year","harvested_year","id_seed_lot_sown","intra_selection_name","etiquette","split","quantity_sown","quantity_harvested","block","X","Y"],
        PHENOTYPIC:["project","sown_year","harvested_year","id_seed_lot_sown","intra_selection_name","etiquette","block","X","Y","individuals","correlation_group"],
        CROSS:["project","sown_year","harvested_year","quantity_harvested","cross_year","cross_germplasm","number_crosses","kernel_number_F1","male_seed_lot","male_etiquette","male_split","male_quantity","male_block","male_X","male_Y","female_seed_lot","female_etiquette","female_split","female_quantity","female_block","female_X","female_Y"]
    }

class PrepareFileFactory :
    filetype = None
    data = None
    output = None
    def __init__(self, filetype, data):
        self.filetype = filetype
        self.data = data
        self.output = StringIO()
        
    def write_headers(self):
        self.output.write('\t'.join(HEADERS[self.filetype]))
        self.output.write('\n')
        
    def write_data(self):
        final_list = self.data.getlist("finallist")
        if self.filetype == SELECTION :
            self._write_selection_stream(final_list)
        elif self.filetype == DIFFUSION :
            self._write_diffusion_stream(final_list)
        elif self.filetype == REPRODUCTION :
            self._write_reproduction_stream(final_list)
        elif self.filetype == MIXTURE :
            self._write_mixture_stream(final_list)
        elif self.filetype == RUNING_REPRO :
            self._write_runingrepro_stream(final_list)
        elif self.filetype == PHENOTYPIC :
            self._write_pheno_stream(final_list)
        elif self.filetype == CROSS :
            self._write_cross_stream(final_list)
    
    def _write_selection_stream(self, final_list):
        for selection in final_list:
            sowed_sl, block, x, y = self._get_selection_infos(selection)
            relation = Relation.objects.get(seed_lot_father__name=sowed_sl,block=block,X=x,Y=y, selection = None)
            project_list = ','.join(map(lambda x: x['project_name'],relation.project.all().values('project_name')))
            self.output.write("{0}\t{1}\t{2}\t{3}\t\t{4}\t{5}\t{6}\t\t\t\n"
                              .format(project_list, self.xstr(relation.reproduction.start_date), self.xstr(relation.reproduction.end_date), sowed_sl, block, (x or ''), (y or '')))
    
    def _get_selection_infos(self, selection):
        sowed_sl, coords = selection[:-1].split(' (')
        block, x, y = coords.split(', ')
        if x == 'None' : x = None
        if y == 'None' : y = None
        return sowed_sl, block, x, y
    
    def _write_diffusion_stream(self, final_list):
        id_project = int(self.data["project"])
        project = Project.objects.get(id=id_project)
        splited = int(self.data["splited"])
        event_year = int(self.data["event_year"])
        for diffusion in final_list :
            seedlot, location = diffusion.split('-->') 
            self.output.write("{0}\t{1}\t{2}\t\t{3}\t{4}\t\n".format(project.project_name,location,seedlot, self.xstr(event_year),self.xstr(splited)))
    
    def _write_reproduction_stream(self, final_list):
        id_project = int(self.data["project"])
        project = Project.objects.get(id=id_project)
        splited = int(self.data["splited"])
        sowed_year = int(self.data["sowed_year"])
        harvested_year = int(self.data["harvested_year"])
        for seedlot in final_list :
            self.output.write("{0}\t{1}\t{2}\t{3}\t\t\t{4}\t\t\t1\t\t\n"
                              .format(project.project_name, self.xstr(sowed_year), self.xstr(harvested_year), seedlot, self.xstr(splited)))
    
    
    def _write_mixture_stream(self, final_list) :
        id_project = int(self.data["project"])
        project = Project.objects.get(id=id_project)
        splited = int(self.data["splited"])
        event_year = int(self.data["event_year"])
        for mixture in final_list :
            seedlot, mixture_name = mixture.split('-->')
            self.output.write("{0}\t{1}\t\t{2}\t\t{3}\t{4}\n".format(project.project_name, seedlot, self.xstr(splited), mixture_name, self.xstr(event_year)))
    
    def _write_runingrepro_stream(self, final_list) :
        for repro in final_list :
            block, x, y, relation = self._get_runningrepro_infos(repro[:-1])
            sowed_year, harvested_year = relation.reproduction.start_date, relation.reproduction.end_date
            split, quantity = "", ""
            if relation.split != None:
                split = self.xstr(relation.split)
            if relation.quantity != None:
                quantity = self.xstr(relation.quantity)
            project_list = ','.join(map(lambda x: x['project_name'],relation.project.all().values('project_name')))
            self.output.write("{0}\t{1}\t{2}\t{3}\t{4}\t\t{5}\t{6}\t\t{7}\t{8}\t{9}\n" 
                              .format(project_list, self.xstr(sowed_year), self.xstr(harvested_year), relation.seed_lot_father.name, 
                                      (relation.selection.selection_name if relation.selection else ""), split, quantity, block, (x or ''), (y or '')))
    
    def _get_runningrepro_infos(self, repro_infos):
        sowed_sl, coords = repro_infos.split(' (')
        block, x, y, selection = coords.split(', ')
        if x == 'None' : x = None
        if y == 'None' : y = None
        if selection != '-' :
            relation = Relation.objects.get(seed_lot_father__name=sowed_sl,block=block, X=x, Y=y, selection__selection_name = selection, is_male='X')
        else :
            print(Relation.objects.filter(seed_lot_father__name=sowed_sl, block=block, X=x, Y=y, selection__isnull = True, is_male='X'))
            relation = Relation.objects.get(seed_lot_father__name=sowed_sl, block=block, X=x, Y=y, selection__isnull = True, is_male='X')
        return block, x, y, relation
    
    def _write_pheno_stream(self, final_list) :
        
        nb_ind = int(self.data["individual"])
        for repro in final_list :
            block, x, y, relation = self._get_runningrepro_infos(repro[:-1])
            sowed_year, harvested_year = relation.reproduction.start_date, relation.reproduction.end_date
            split, quantity = "", ""
            if relation.split != None:
                split = self.xstr(relation.split)
            if relation.quantity != None:
                quantity = self.xstr(relation.quantity)
            for i in range(nb_ind):
                project_list = ','.join(map(lambda x: x['project_name'],relation.project.all().values('project_name')))
                self.output.write("{0}\t{1}\t{2}\t{3}\t{4}\t\t{5}\t{6}\t{7}\t{8}\n"
                                  .format(project_list, self.xstr(sowed_year), self.xstr(harvested_year), relation.seed_lot_father.name, 
                                          (relation.selection.selection_name if relation.selection else ""), block, (x or ''), (y or ''), self.xstr(i+1)))
        
    def _write_cross_stream(self, final_list) :
        id_project = int(self.data["project"])
        project = Project.objects.get(id=id_project)
        splited = int(self.data["splited"])
        crosses = final_list[0].split('&')[:-1]
        sowed_year = int(self.data["sowed_year"])
        harvested_year = int(self.data["harvested_year"])
        for cross in crosses:
            male, female = cross.split(';')
            self.output.write("{0}\t{1}\t{2}\t\t\t\t\t\t{3}\t\t{4}\t\t\t\t\t{5}\t\t{6}\t\t\t\t\n"
                              .format(project.project_name, self.xstr(sowed_year), self.xstr(harvested_year), male, self.xstr(splited), female, self.xstr(splited)))
    def get_stream(self):
        return self.output.getvalue()
    
    def xstr(self, s):
        if s is None :
            return ''
        return str(s)