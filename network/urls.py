# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.urls import path
from network.views import uploadfile, merge_reps, reproduction_method_choice, generate_cross_file, generate_reproduction_file, generate_runingrepro_file, generate_diffusion_file,\
 generate_mixture_file, generate_selection_file, generate_individual_file, creatediffusion, createreproduction, selection_table, merge_repetition, testselection, relation_profil, \
 createselection, createcross, export_empty_file, delete_reproduction_fromfile, search_relation_by_type, network_explorer

urlpatterns = [
                       path('upload/', uploadfile, name='uploadfile'),
                       path('mergereps/', merge_reps, name='mergereps'),
                       path('chooserepromethod/', reproduction_method_choice, name='reproduction_method_choice'),
                       
                       path('delete_reproduction_fromfile/', delete_reproduction_fromfile, name='delete_reproduction_fromfile'),
                       
                       path('exportcrossfile/', generate_cross_file, name="generatecross"),
                       path('exportreproductionfile/', generate_reproduction_file, name="generatereproduction"),
                       path('exportruningreprofile/', generate_runingrepro_file, name="generateruningrepro"),
                       path('exportdiffusionfile/', generate_diffusion_file, name="generatediffusion"),
                       path('exportmixturefile/', generate_mixture_file, name="generatemixture"),
                       path('exportselectionfile/', generate_selection_file, name="generateselection"),
                       path('exportindividualfile/', generate_individual_file, name="generateindividual"),
                       path('exportemptyfile/<str:event_type>/', export_empty_file, name='export_empty_file'),
                       
                       path('newdiffusion/', creatediffusion, name="creatediffusion"),
                       path('newreproduction/', createreproduction, name="createreproduction"),
                       path('newselection/', createselection, name="createselection"),
                       path('newcross/', createcross, name="createcross"),
                       
                       path('selectiontable/', selection_table, name='selectiontable'),
                       path('merge/', merge_repetition, name='merge'),
                       path('testselection/', testselection, name='testselection'),
                       path('relation/<int:relation_id>/', relation_profil, name='relation_profil'),
                       path('searchrelation/<str:type_relation>/', search_relation_by_type, name='search_relation_by_type'),
                       
                       path('network-explorer/', network_explorer, name='networkexplorer')
                       ]
