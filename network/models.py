# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""
from __future__ import unicode_literals

from django.db import models
from django.db.models import Q, UniqueConstraint, CheckConstraint
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.apps import apps

from network.managers import DiffusionManager, ReproductionManager, MixtureManager, SelectionManager, RelationManager
from network.eventmanager import EventManager

CROSS_REALISED = (
          (u'Y', _('Yes')),
          (u'N', _('No')),
          (u'X', _('Unknown'))
          )

FATHER_SEED_LOT_GENDER = (
              (u'M', _('Male')),
              (u'F', _('Female')),
              (u'X', _('Unknown')),
              )

class GenericEvent():
    
    def delete_cascade(self):
        
        todelete = [self,]
        EventManager.search_events_todelete(self, todelete)
        todelete.reverse()
        for d in todelete :
            d.delete()
    
    def delete_properly(self):
        Relation = apps.get_model('network','relation')
        to_delete = []
        for r in self.relation_set.all():
            if r.seed_lot_son and not Relation.objects.filter(seed_lot_father = r.seed_lot_son) :
                to_delete.append(r.seed_lot_son)
            if r.seed_lot_father and not Relation.objects.filter(Q(seed_lot_father = r.seed_lot_father)|Q(seed_lot_son = r.seed_lot_father)).exclude(id__in=self.relation_set.values_list('id')) :
                to_delete.append(r.seed_lot_father)
            for rd in r.rawdatas.all() :
                if rd.relation.all().count() == 1 :
                    to_delete.append(rd)
        for obj in to_delete :
            obj.delete()

class Diffusion (models.Model, GenericEvent):
    date = models.IntegerField(null=True)
    
    objects = DiffusionManager()

    def __str__(self):
        s = "["
        if self.relation_set.all() :
            s += "{0}".format(self.relation_set.all().first())
            for r in self.relation_set.all()[1:] :
                s += ", {0}".format(r)
        s += "]"
        return "{0} {1}".format(self.date, s)
    
    def delete(self,*args, **kwargs):
        self.delete_properly()
        super(Diffusion, self).delete(*args, **kwargs)

class ReproductionMethod (models.Model):
    reproduction_methode_name = models.CharField(max_length=100,unique=True)
    description = models.CharField(max_length=500,blank=True,null=True)
    def __unicode__(self):
        return u"%s"%self.reproduction_methode_name
    def __str__(self):
        return self.reproduction_methode_name

class Reproduction (models.Model, GenericEvent):
    reproduction_method = models.ForeignKey('ReproductionMethod', blank=True, null=True, on_delete=models.SET_NULL)
    start_date = models.IntegerField(null=True, blank=True)
    end_date = models.IntegerField(null=True, blank=True)
    kernel_number = models.IntegerField(blank=True, null=True)
    cross_number = models.IntegerField(blank=True, null=True)
    description = models.CharField(max_length=200, blank=True, null=True)
    realised =  models.CharField(max_length=2, choices=CROSS_REALISED,blank=True,null=True)    
    objects = ReproductionManager()
    
    def __str__(self):
        s = "["
        if self.relation_set.all() :
            s += "{0}".format(self.relation_set.all().first())
            for r in self.relation_set.all()[1:] :
                s += ", {0}".format(r)
        s += ']'
        return "{0}-{1} {2}".format(self.start_date, self.end_date, s)
    
    def delete(self,*args, **kwargs):
        self.delete_properly()
        super(Reproduction, self).delete(*args, **kwargs)

class Mixture (models.Model, GenericEvent):
    date = models.IntegerField(null=True)
    objects = MixtureManager()
    
    def __str__(self):
        s = "["
        if self.relation_set.all() :
            s += "{0}".format(self.relation_set.all().first())
            for r in self.relation_set.all()[1:] :
                s += ", {0}".format(r)
        s += ']'
        return "{0} {1}".format(self.date,s)
    
    def delete(self,*args, **kwargs):
        self.delete_properly()
        super(Mixture, self).delete(*args, **kwargs)

class Selection (models.Model, GenericEvent):
    person=models.ForeignKey('actors.Person', blank=True, null=True, on_delete=models.SET_NULL)
    selection_name = models.CharField(max_length=100)
    date = models.IntegerField(null=True)
    
    objects = SelectionManager()

    def __str__(self):
        s = "["
        if self.relation_set.all() :
            s += "{0}".format(self.relation_set.all().first())
            for r in self.relation_set.all()[1:] :
                s += ", {0}".format(r)
        s += ']'
        return "{0} {1}".format(self.date, s)
    
    def delete(self,*args, **kwargs):
        self.delete_properly()
        super(Selection, self).delete(*args, **kwargs)

class Relation (models.Model):
    
    mixture = models.ForeignKey('Mixture', blank=True, null=True, on_delete=models.SET_NULL)
    reproduction = models.ForeignKey('Reproduction',blank=True,null=True, on_delete=models.SET_NULL)
    diffusion = models.ForeignKey('Diffusion',blank=True,null=True, on_delete=models.SET_NULL)
    selection = models.ForeignKey('Selection',blank=True,null=True, on_delete=models.SET_NULL)
    seed_lot_father = models.ForeignKey('entities.Seedlot', related_name='relations_as_parent', on_delete=models.CASCADE)
    seed_lot_son = models.ForeignKey('entities.Seedlot',blank=True,null=True, related_name = 'relations_as_child', on_delete=models.SET_NULL)

    quantity = models.FloatField(blank=True,null=True)
    is_male =  models.CharField(max_length=2, choices=FATHER_SEED_LOT_GENDER,blank=True,null=True)    
    split = models.CharField(max_length=1,blank=True,null=True)
    block = models.CharField(max_length=10, blank=True, null=True)
    X = models.CharField(max_length=10, blank=True,null=True)
    Y = models.CharField(max_length=10, blank=True,null=True)
    project = models.ManyToManyField('actors.Project')
    
    objects = RelationManager()
    
    class Meta:
        constraints = [
            UniqueConstraint(
                fields = ['seed_lot_father', 'block', 'X', 'Y'],
                condition = Q(reproduction__isnull=False, selection__isnull=True, is_male='X'),
                name = 'unique_reproduction'
                ),
            CheckConstraint(
                check = Q(reproduction__isnull=False)|Q(mixture__isnull=False)|Q(diffusion__isnull=False),
                name = 'events_notnull_together'
                )
            ]
    
    def __str__(self):
        if self.seed_lot_son:
            return "{0} --> {1}".format(self.seed_lot_father, self.seed_lot_son)
        else:
            return "{0} --> {1}".format(self.seed_lot_father, 'Child unknown')
    
    def get_relation_type(self):
        if self.mixture != None :
            if self.mixture.relation_set.values('seed_lot_father__germplasm').distinct().count() > 1 :
                return _("Mixture")
            else :
                return _("Merge")
        elif self.reproduction != None and self.selection == None and self.is_male == 'X':
            return _("Reproduction")
        elif self.selection != None :
            return _("Selection")
        elif self.diffusion != None :
            return _("Diffusion")
        elif self.reproduction != None and self.selection == None and (self.is_male == 'M' or self.is_male == 'F'):
            return _("Cross")
        else :
            return _("Unknown event")
    
    def compute_child_generation(self):
        father = self.seed_lot_father
        child = self.seed_lot_son
        if child :
            if self.diffusion_id != None :
                child.generation = father.generation
                child.confidence = father.confidence
                child.onfarm_confidence = True
                child.onfarm_generation = 0
            elif self.mixture_id != None and father.germplasm == child.germplasm:
                child.generation = father.generation+1
                child.confidence = False
                child.onfarm_generation = father.onfarm_generation+1
                child.onfarm_confidence = False
            elif self.mixture_id != None or (self.reproduction_id != None and (self.is_male == "F" or self.is_male == "M")) :
                child.generation = 0
                child.confidence = True
                child.onfarm_confidence = True
                child.onfarm_generation = 0
            elif self.reproduction_id != None and (self.is_male == "X" or self.selection_id != None):
                child.generation = father.generation+1
                child.confidence = father.confidence
                child.onfarm_generation = father.onfarm_generation+1
                child.onfarm_confidence = father.onfarm_confidence
            child.save()    
    
    def compute_descedants_generation(self,seedlot):
        qs = Relation.objects.filter(seed_lot_father = seedlot)
        for r in qs :
            r.compute_child_generation()
            if r.diffusion_id != None or (self.reproduction_id != None and self.is_male == "X") or (self.reproduction_id != None and self.selection_id != None) :
                r.compute_descedants_generation(r.seed_lot_son)
    
    def get_phenotyped_individual(self):
        return list(map((lambda x: x['individual']), self.rawdatas.exclude(individual__isnull=True).values('individual').order_by('individual').distinct()))

    def get_is_male_as_string(self):
        for t in FATHER_SEED_LOT_GENDER :
            if self.is_male == t[0]:
                return t[1]

    def clean(self):
        """
            The clean method is overriden. The new implementation considers the type of the relation.
        """
        self.clean_fields()
        if self.diffusion_id != None :
            self.diffusion.clean_fields()
            if self.seed_lot_son and self.seed_lot_father.germplasm != self.seed_lot_son.germplasm :
                raise ValidationError(_("You can't create a diffusion event between two seed lot of different germplasm"))
            
            if self.seed_lot_son and self.seed_lot_son.date != int(self.diffusion.date) :
                raise ValidationError(_("The event year of the diffusion must be the same of the creation date of the received seed lot"))
            
            if self.seed_lot_father.date and int(self.diffusion.date) < self.seed_lot_father.date :
                raise ValidationError(_("You can't diffuse a seed lot before its creation date"))
            
            if self.seed_lot_son and self.seed_lot_father.location == self.seed_lot_son.location :
                raise ValidationError(_("A seed lot can't be diffused in its own location"))
        
        elif self.reproduction_id != None and self.selection_id == None :
            self.reproduction.clean_fields()
            
            if self.is_male == 'X' :
                event_type = _("multiplication")            
            elif self.is_male == 'F' or self.is_male == 'M' :
                event_type = _("cross")
                
            if self.seed_lot_son and self.seed_lot_father.germplasm != self.seed_lot_son.germplasm and self.is_male == 'X' :
                    raise ValidationError(_("You can't create a {0} relation between two seed lot of different germplasm").format(event_type))
            
            if self.seed_lot_son and self.seed_lot_father.location != self.seed_lot_son.location :
                raise ValidationError(_("You can't create a {0} event between two seed lot that do not belong to the same farm").format(event_type))
            
            if self.reproduction.end_date and self.reproduction.start_date > self.reproduction.end_date :
                raise ValidationError(_("You can't sow a seed lot after it has been harvested"))
            
            if self.seed_lot_father.date and self.seed_lot_father.date > self.reproduction.start_date :
                raise ValidationError(_("You can't sow a seed lot before its creation date"))
            
            if self.seed_lot_son and self.seed_lot_son.date and self.seed_lot_son.date != self.reproduction.end_date :
                raise ValidationError(_("The creation date of the harvested seed lot must be the as the harvesting date"))
            
            current_relation = None
            if self.X and self.Y :
                current_relation = Relation.objects.filter(seed_lot_father__location=self.seed_lot_father.location, seed_lot_father__germplasm__species=self.seed_lot_father.germplasm.species,
                                                       reproduction__start_date=self.reproduction.start_date, block=self.block, X=self.X, Y=self.Y, selection=None)
            if current_relation and self.id is None:
                raise ValidationError(_("You can't sow this seed lot here, a reproduction is already growing here"))
        elif self.selection_id != None :
            self.selection.clean_fields()
            if self.selection.selection_name.strip() == '':
                raise ValidationError(_("Selection name can't be empty or whitespace"))
            if Selection.objects.filter(selection_name = self.selection.selection_name, relation__seed_lot_son__germplasm = self.seed_lot_father.germplasm) :
                raise ValidationError(_("The selection name is already used for this germplasm"))
            if self.selection.date > self.reproduction.end_date :
                raise ValidationError(_("You can't do an intra seed lot selection after harvesting year"))

        if self.seed_lot_son and self.seed_lot_father.date and self.seed_lot_father.date > self.seed_lot_son.date :
            raise ValidationError(_("A seed lot son can't be older than a seed lot father"))
        
        relation_creation = Relation.objects.filter(seed_lot_son = self.seed_lot_son, seed_lot_son__isnull = False)
        for r in relation_creation :
            if r.reproduction_id != self.reproduction_id or r.mixture_id != self.mixture_id or r.selection_id != self.selection_id or r.diffusion_id != self.diffusion_id :
                raise ValidationError(_("This seed lot has already be created by a previous event"))
        
    def save(self,*args, **kwargs):
        self.compute_child_generation()
        self.compute_descedants_generation(self.seed_lot_son)
        self.clean()
        super(Relation,self).save(*args, **kwargs)
