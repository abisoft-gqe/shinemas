# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from network.models import Relation
from entities.models import Seedlot, Germplasm
from actors.models import Location


def is_not_already_merged(seed_lot) :
    if Relation.objects.filter(seed_lot_father = seed_lot, mixture__isnull = False, seed_lot_son__germplasm = seed_lot.germplasm) :
        return False
    else :
        return True

def some_errors(error_messages) :
    for type_message in error_messages.keys():
        if len(error_messages[type_message]) > 0 :
            return True
    return False

def get_input_seedlot(seedlot_id, location_id, germplasm_id, sl_year):
    if seedlot_id :
        seedlot = Seedlot.objects.get(id=seedlot_id)
    else :
        try :
            idgermplasm = Germplasm.objects.get(id=germplasm_id)
        except :
            raise
        location = Location.objects.get(id=location_id)
        seedlot = u"{0}_{1}_{2}".format(idgermplasm, location.short_name, sl_year)
    return seedlot

def get_output_seedlot(output_id):
    if output_id :
        output_id_seedlot = Seedlot.objects.get(id=output_id)
    else :
        output_id_seedlot = None

    return output_id_seedlot

def convert_date(date_in):
    if len(date_in)>=10 :
        date_out = date_in[8:10] + '/' + date_in[5:7] + '/' + date_in[0:4]
    else :
        date_out = ""
    return date_out

def form_date(date):
    if len(date)==10:
        if(date[4]=='-' and date[7]=='-'):
            y = int(date[0:4])
            m = int(date[5:7])
            d = int(date[8:10])
            if(y<2080 and y>1800 and m<=12 and m>=1 and d<=31 and d>=1):
                return True
    return False
             

# def get_data_list(variables_id, values, methods_id, dates):
#     eventdata = []
#     for i in range(0,len(variables_id)):
#         if values[i] is not None and values[i] != "":
#             variable = Variable.objects.get(id = variables_id[i])
#             method = Method.objects.get(id = methods_id[i])
#             
#             eventdata.append((variable, values[i], method, dates[i]))
#     return eventdata

def xstr(s):
    if s is None :
        return ''
    return str(s)
