# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView

from rest_framework.schemas import get_schema_view
from rest_framework.permissions import AllowAny
import debug_toolbar
from common import views


admin.autodiscover()

urlpatterns = [
    
    path('admin/', admin.site.urls),
    path('actors/', include('actors.urls')),
    path('network/', include('network.urls')),
    path('api/', include('wsR.urls')),
    #url('^analysis/', include('analysis.urls')),
    path('entities/', include('entities.urls')),
    path('weather/', include('weather.urls')),
    path('images/', include('images.urls')),
    path('data/', include('eppdata.urls')),
    path('generic/', include('common.urls')),
    path('', include('myuser.urls')),
    path('authors/', views.authors , name='authors'),
    path('next/', views.nextfeatures , name='next'),
    path('setlang/', views.change_langage, name='set_language'),
    path('files/', include('csv_io.urls')),
    path('__debug__/', include(debug_toolbar.urls)),
    
    path('openapi/', get_schema_view(
        title="SHiNeMaS' API",
        permission_classes=[AllowAny],
        version="2.0"
    ), name='openapi-schema'),
    path('api/swagger-ui/', TemplateView.as_view(
        template_name='swagger-ui.html',
        extra_context={'schema_url':'openapi-schema'}
    ), name='swagger-ui'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
