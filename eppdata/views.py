# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""
import datetime
import json
import hashlib
import os
import copy

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponse, Http404
from django.conf import settings
from django.core import serializers

from eppdata.forms import DeleteDataset, EditRawdata
from eppdata.models import Rawdata, Variable
from remote_forms.forms import RemoteForm
from network.filemanager import FileManager

@login_required
@user_passes_test(lambda u: u.is_superuser)
def delete_reproduction_data(request):
    if request.method == "GET" :
        return render(request, 'eppdata/delete_reproduction_data.html', {'form' : DeleteDataset()})
    elif request.method == "POST" :
        if "delete_submit" in request.POST :
            location_ids = request.POST.getlist("location")
            germplasm_ids = request.POST.getlist("germplasm")
            variable_ids  = request.POST.getlist("variable")
            sowing_date =  int(request.POST["sowing_year"])
            start_date = datetime.datetime.strptime(request.POST["start_date"],"%Y-%m-%d")
            end_date =datetime.datetime.strptime( request.POST["end_date"],"%Y-%m-%d")
            if "include_data_without_date" in request.POST :
                include_data_without_date = True
            else :
                include_data_without_date = False
            
            result_set = Rawdata.objects.raw_data_to_delete(location_ids, germplasm_ids, variable_ids, sowing_date, start_date, end_date, include_data_without_date)        
            
            return render(request, 'eppdata/delete_reproduction_data.html', {'raw_data':result_set,'form': DeleteDataset(data=request.POST)})
        
        if "back_submit" in request.POST :
            return render(request, 'eppdata/delete_reproduction_data.html', {'form' : DeleteDataset(data=request.POST)})
        elif "confirm_submit" in request.POST :
            f = FileManager()
            
            raw_data_ids = request.POST.getlist('raw_data')
            raw_data_to_delete = Rawdata.objects.filter(id__in=raw_data_ids)
            result_set = Rawdata.get_dict_with_relation_as_key(raw_data_to_delete)
            
            reproduction_hash = f.write_reproduction_file(result_set.keys(), Variable.objects.filter(rawdata__in=raw_data_to_delete).distinct().exclude(type='T11')  )
            individual_hash = f.write_individual_file(result_set.keys(), Variable.objects.filter(rawdata__in=raw_data_to_delete).distinct().filter(type='T11')  )
            for rel in result_set.keys() :
                result_set[rel] = len(result_set[rel])
            raw_data_to_delete.delete()
            return render(request,'eppdata/delete_data_report.html',{'deletion_report':result_set, 'reproduction_file':reproduction_hash.hexdigest(), 'individual_file':individual_hash.hexdigest()})
        else :
            return render(request, 'eppdata/delete_reproduction_data.html', {'form' : DeleteDataset()})

@login_required
def get_csv_file(request, hashstr=None):
    if request.is_ajax() :
        stream = request.POST["csvstring"]
        h = hashlib.sha1(stream.encode('utf8'))
        with open(os.path.join(settings.TEMP_DIR,h.hexdigest()+'.csv'), 'wb') as csvfile:
            csvfile.write(stream.encode('utf8'))
            csvfile.close()
        return HttpResponse(json.dumps({'hash': h.hexdigest()}))
    elif request.method == "GET" and hash != ""  :
        filepath = os.path.join(settings.TEMP_DIR,hashstr+'.csv')
        with open(filepath, 'rb') as csvfile:
            response = HttpResponse(csvfile.read(), content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename="'+hashstr+'.csv"'
            os.remove(filepath)
            return response
    else :
        return redirect('home')

@login_required
@user_passes_test(lambda u: u.is_superuser)
def get_rawdataedition(request, rawdata_id):
    if request.is_ajax() :
        try :
            rd = Rawdata.objects.get(id=rawdata_id)
        except :
            return Http404()
        if request.method == "GET" :
            form = EditRawdata(instance=rd)
            remote_form = RemoteForm(form)
            return HttpResponse(json.dumps({'form': json.dumps(remote_form.as_dict())}))
        elif request.method == "POST" :
            form = EditRawdata(copy.copy(request.POST), instance=rd)
            if form.is_valid(): 
                updated_rawdata = form.save(commit=False)
                updated_rawdata.last_modification_user = request.user
                updated_rawdata.save()
                data = serializers.serialize("json", [updated_rawdata, updated_rawdata.method])
                return HttpResponse(json.dumps({'data':data}))
            else :
                remote_form = RemoteForm(form)
                remote_form_dict = remote_form.as_dict()
                return HttpResponse(json.dumps({'form': json.dumps(remote_form_dict)}))
        else :
            return redirect('home')
    else :
        return redirect('home')