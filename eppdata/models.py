# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.db import models
from django.utils.html import format_html
from django.urls import reverse
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.contrib import messages

#from django.apps import apps

from eppdata.managers import RawdataManager, VariableManager
# Je permet la sélection de différents choix pour les différentes saisons.


SEASON_CHOICE=(
                  (u'A', u'Autumn'),
                  (u'W', u'Winter'),
                  (u'Su', u'Summer'),
                  (u'Sp', u'Spring')
                  )
TYPE_CHOICE=(
             (u'T1', u'Type1'),
             (u'T2', u'Type2'),
             (u'T3',u'Type3'),
             (u'T4',u'Type4'),
             (u'T5',u'Type5'),
             (u'T6',u'Type6'),
             (u'T7',u'Type7'),
             (u'T8',u'Type8'),
             (u'T9',u'Type9'),
             (u'T10',u'Type10'),
             (u'T11',u'Type11'),
             (u'h',u'Weather hourly'),
             (u'd',u'Weather daily'),
             #(u'dix',u'Weather decadal'),
            # (u'm',u'Weather monthly'),
             #(u'y',u'Weather annual')
             )

class Method (models.Model):

    person = models.ForeignKey('actors.Person', blank=True, null=True, on_delete=models.SET_NULL)
    method_description = models.TextField(blank=True, null=True)
    method_name= models.CharField(max_length=100,blank=True,null=True,unique=True)
    unit = models.CharField(max_length=100,blank=True,null=True)
    quali_quanti_notes = models.CharField(max_length=100,blank=True,null=True)
    ind_global = models.CharField (max_length=100,blank=True,null=True)
    
    def __unicode__(self):
        return self.method_name
    def __str__(self):
        return self.method_name

class Variable (models.Model):
    name = models.CharField(max_length=100,blank=True,null=True)
    name_descriptor = models.CharField(max_length=100,blank=True,null=True)
    type = models.CharField(max_length=10, choices=TYPE_CHOICE,blank=True,null=True)
    season  =  models.CharField(max_length=10, choices=SEASON_CHOICE,blank=True,null=True)
    unit = models.CharField(max_length=100,blank=True,null=True)
    source = models.CharField(max_length=20, default="Custom")
    
    objects = VariableManager()
    
    def __str__(self):
        if self.type in ['h','d'] :
            if self.name_descriptor != '':
                return self.name_descriptor
            else :
                return self.name
        else :
            return u"{0} ({1})".format(self.name, self.type)

    def save(self,*args, **kwargs):
        self.clean()
        super(Variable,self).save(*args, **kwargs)
    
    def clean(self):
        if not Variable.is_valid_variable_name(self.name) :
            raise ValidationError(_("The variable name {0} is not a valid name (should not contain space ( ), dollar '$' or percentage '%')").format(self.name))
    
    class Meta :
        unique_together = ('name', 'type')
        indexes = [
            models.Index(fields=['name','type'])
                ]
     
    @staticmethod
    def is_valid_variable_name(variable_name):
        char_list=['$',' ','%']
        for char in char_list:
            try:
                variable_name.index(char)
            except:
                pass
            else :
                return False
        return True
    
    def get_methods(self):
        return Method.objects.filter(rawdata__variable=self).distinct()
    
    def associated_methods(self):
        s = ""
        methods = self.get_methods()
        if methods :
            s += "<a href=\"{1}\">{0}</a>".format(methods[0].method_name, reverse('admin:eppdata_method_change', args=[methods[0].id]))
            if methods.count() > 1 :
                for m in methods[1:] :
                    s += ", <a href=\"{1}\">{0}</a>".format(m.method_name, reverse('admin:eppdata_method_change', args=[m.id]))
                    
        return format_html(s)

class Rawdata (models.Model):
    seedlot = models.ManyToManyField('entities.Seedlot')
    relation = models.ManyToManyField('network.Relation', related_name='rawdatas')
    germplasm = models.ManyToManyField('entities.Germplasm')
    station = models.ManyToManyField('weather.WeatherStation')
    method = models.ForeignKey('Method', on_delete=models.CASCADE)
    variable = models.ForeignKey('Variable', on_delete=models.CASCADE)
    raw_data = models.TextField(blank=True,null=True)
    group = models.CharField (max_length=100,blank=True,null=True)
    comment = models.TextField (max_length=200,blank=True,null=True)
    individual = models.IntegerField(blank=True,null=True)
    date = models.DateField(blank=True,null=True)
    submit_date = models.DateField(auto_now_add=True)
    last_modification_date = models.DateField(auto_now=True)
    last_modification_user = models.ForeignKey('auth.User', blank=True, null=True, on_delete=models.SET_NULL)
    
    objects = RawdataManager()
    
    def __str__(self):
        return self.raw_data
    
    @staticmethod
    def get_dict_with_relation_as_key(raw_data_qs):
        result_set = {}
        for rd in raw_data_qs :
            if rd.relation.all()[0] in result_set.keys() :
                result_set[rd.relation.all()[0]].append(rd)
            else :
                result_set[rd.relation.all()[0]] = [rd,]

        return result_set
    
    @staticmethod
    def check_variables_file(vars_and_methods) :
        variables = []
        errors = []
        for v in vars_and_methods:
            if '$' in v :
                continue

            try :
                variables.append(Variable.objects.get(name=v))
            except Exception as e:
                errors.append(Exception("Variable {0} doesn't exists".format(v)))
            
            c = vars_and_methods.count("{0}$method".format(v))
            if c == 0 :
                errors.append(Exception("There is no method for variable {0} you should have a header {0}$method".format(v)))
            elif c > 1 :
                errors.append(Exception("The header {0}$method have been found multiple time, you must have only one".format(v)))
            
        
        if errors :
            raise Exception(*errors)
        else :
            return variables
        
