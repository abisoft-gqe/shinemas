# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.urls import path
from eppdata.views import delete_reproduction_data, get_csv_file, get_rawdataedition

urlpatterns = [              
    path('reproduction/deletedata/', delete_reproduction_data, name='delete_reproduction_data'),
    path('getcsvfile/', get_csv_file, name='put_csv_file'),
    path('getcsvfile/<str:hashstr>/', get_csv_file, name='get_csv_file'),
    path('getrawdataform/<int:rawdata_id>/', get_rawdataedition, name='getrawdataform')
    ]