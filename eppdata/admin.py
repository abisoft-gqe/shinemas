# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from import_export.admin import ImportExportModelAdmin
from import_export import resources, fields, widgets

from eppdata.models import Method, Variable
from actors.models import Person

class VariableResource(resources.ModelResource):
    class Meta:
        model = Variable
        exclude = ('id',)
        import_id_fields = ('name','type')

class MethodResource(resources.ModelResource):
    person = fields.Field(column_name='person', attribute='person', widget=widgets.ForeignKeyWidget(Person, field='short_name'))
    class Meta:
        model = Method
        exclude = ('id',)
        import_id_fields = ('method_name',)

class MethodAdmin (ImportExportModelAdmin):
    list_display = ('method_name','person',)
    Fieldset = [
        
        (_('Person'), {'fields': ['person']}),
        (_('Meyhod'), {'fields': ['method_description']}),
        (_('Method name'), {'fields': ['method_name']}), 
        (_('Unit'), {'fields': ['unit']}), 
        (_('Qualitative or Quantitive'), {'fields': ['quali_quanti_notes']}), 
        (_('Individual'), {'fields': ['ind_global']}),      
        ]
    resource_class = MethodResource
    
    
class VariableAdmin (ImportExportModelAdmin):
    Fieldset = [
        
        (_('Name'), {'fields': ['name']}),
        (_('Description Name'), {'fields': ['name_descriptor']}),
        (_('Type'), {'fields': ['type']}),
        (_('Season'), {'fields': ['season']}),
        (_('Unit'), {'fields': ['unit']}),
        (_('Source'), {'fields': ['source']}),
        ]
    list_display = ('name', 'name_descriptor', 'type', 'associated_methods')
    readonly_fields = ("source",)
    resource_class = VariableResource
    
admin.site.register(Method,MethodAdmin)
admin.site.register(Variable,VariableAdmin)



