# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.db import models
from django.apps import apps
from django.db.models import Q
from django.contrib import messages

class RawdataManager(models.Manager):
    
    def prepare_rawdata(self, file_line, variable):
        Rawdata = apps.get_model('eppdata','rawdata')
        Method = apps.get_model('eppdata','method')
        
        data = file_line[variable.name]
        method_field = "{0}$method".format(variable.name)
        if file_line[method_field] :
            try :
                method = Method.objects.get(method_name=file_line[method_field])
            except Exception as e:
                raise Exception("Method {0} doesn't exists".format(file_line[method_field]))
        else :
            raise Exception("Method can't be empty for variable {0}".format(variable.name))

        return Rawdata(
                method = method,
                variable = variable,
                raw_data = data,
            )
            
    def filter_with_range_date(self, params, queryset):
        if params['date_start'] :
            if params['include_no_date'] :
                queryset = queryset.filter(Q(date__gte=params['date_start'])|Q(date__isnull = True))
            else :
                queryset = queryset.filter(date__gte=params['date_start'])
        if params['date_end'] :
            if params['include_no_date'] :
                queryset = queryset.filter(Q(date__lte=params['date_end'])|Q(date__isnull = True))
            else :
                queryset = queryset.filter(date__lte=params['date_end'])
        return queryset

    def raw_data_to_delete(self, location_ids, germplasm_ids, variable_ids, sowing_date, start_date, end_date, include_data_without_date):
        Rawdata = apps.get_model('eppdata','rawdata')
        
        queryset = self.all()
        if location_ids :
            queryset = queryset.filter(relation__seed_lot_father__location__id__in=location_ids)
        if germplasm_ids :
            queryset = queryset.filter(relation__seed_lot_father__germplasm__id__in=germplasm_ids)
        if variable_ids :
            queryset = queryset.filter(variable__id__in=variable_ids)
        
        queryset = queryset.filter(relation__reproduction__start_date=sowing_date)
        if include_data_without_date :
            queryset = queryset.exclude(date__gte=end_date, date__lte=start_date)
        else :
            queryset = queryset.filter(date__lte=end_date, date__gte=start_date)
                
        return Rawdata.get_dict_with_relation_as_key(queryset)
        
class VariableManager(models.Manager):
    
    def get_individual_variables_from_ids(self, variables_ids):
        Variable = apps.get_model('eppdata','variable')
        if variables_ids:
            return Variable.objects.filter(id__in = variables_ids, type = 'T11')
        else :
            return Variable.objects.filter(type = 'T11')
    
    def get_seedlot_variables_from_ids(self, variables_ids) :
        Variable = apps.get_model('eppdata','variable')
        if variables_ids:
            return Variable.objects.filter(id__in = variables_ids).filter(type__in= ['T7','T8','T9','T10'])
        else :
            return Variable.objects.filter(type__in= ['T7','T8','T9','T10'])
        
    def get_global_variables_from_ids(self, variables_ids):
        Variable = apps.get_model('eppdata','variable')
        if variables_ids:
            return Variable.objects.filter(id__in = variables_ids).exclude(type__in= ['T7','T8','T9','T10','T11'])
        else :
            return Variable.objects.exclude(type__in= ['T7','T8','T9','T10','T11'])