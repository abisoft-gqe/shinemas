# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-06-02 15:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eppdata', '0030_auto_20170529_1431'),
    ]

    operations = [
        migrations.AlterField(
            model_name='variable',
            name='source',
            field=models.CharField(default='Custom', max_length=20),
        ),
        migrations.AlterField(
            model_name='variable',
            name='type',
            field=models.CharField(blank=True, choices=[('T1', 'Type1'), ('T2', 'Type2'), ('T3', 'Type3'), ('T4', 'Type4'), ('T5', 'Type5'), ('T6', 'Type6'), ('T7', 'Type7'), ('T8', 'Type8'), ('T9', 'Type9'), ('T10', 'Type10'), ('T11', 'Type11'), ('h', 'Weather hourly'), ('d', 'Weather daily')], max_length=10, null=True),
        ),
    ]
