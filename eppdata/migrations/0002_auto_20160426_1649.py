# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-04-26 16:49
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    atomic=False
    dependencies = [
        ('network', '0002_auto_20160426_1645'),
        ('entities', '0002_auto_20160426_1047'),
        ('eppdata', '0001_initial'),
    ]

    operations = [
#         migrations.CreateModel(
#             name='Raw_data',
#             fields=[
#                 ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
#                 ('raw_data', models.TextField(blank=True, null=True)),
#                 ('group', models.CharField(blank=True, max_length=100, null=True)),
#                 ('description', models.CharField(blank=True, max_length=100, null=True)),
#                 ('individual', models.IntegerField(blank=True, null=True)),
#                 ('date', models.DateField(blank=True, null=True)),
#             ],
#         ),
        migrations.RenameModel(
            old_name='Env_pra_phe_raw_data',
            new_name='Raw_data',
        ),
        migrations.RenameModel(
            old_name='Env_pra_phe_method',
            new_name='Method',
        ),
        migrations.RenameModel(
            old_name='Env_pra_phe_variable',
            new_name='Variable',
        ),
#         migrations.RemoveField(
#             model_name='env_pra_phe_raw_data',
#             name='method',
#         ),
#         migrations.RemoveField(
#             model_name='env_pra_phe_raw_data',
#             name='relation',
#         ),
#         migrations.RemoveField(
#             model_name='env_pra_phe_raw_data',
#             name='seed_lot',
#         ),
#         migrations.RemoveField(
#             model_name='env_pra_phe_raw_data',
#             name='variable',
#         ),
        migrations.RemoveField(
            model_name='env_pra_phe_value',
            name='envpraphemethod',
        ),
        migrations.RemoveField(
            model_name='env_pra_phe_value',
            name='rawdata',
        ),
#         migrations.DeleteModel(
#             name='Env_pra_phe_raw_data',
#         ),
        migrations.DeleteModel(
            name='Env_pra_phe_value',
        ),
#         migrations.AddField(
#             model_name='raw_data',
#             name='method',
#             field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='eppdata.Method'),
#         ),
#         migrations.AddField(
#             model_name='raw_data',
#             name='relation',
#             field=models.ManyToManyField(to='network.Relation'),
#         ),
#         migrations.AddField(
#             model_name='raw_data',
#             name='seed_lot',
#             field=models.ManyToManyField(to='entities.Seed_lot'),
#         ),
#         migrations.AddField(
#             model_name='raw_data',
#             name='variable',
#             field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='eppdata.Variable'),
#         ),
    ]
