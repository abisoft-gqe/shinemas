# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-05-23 15:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('actors', '0008_auto_20170509_1633'),
        ('eppdata', '0028_auto_20170504_1434'),
    ]

    operations = [
        migrations.AddField(
            model_name='rawdata',
            name='location',
            field=models.ManyToManyField(to='actors.Location'),
        ),
        migrations.AlterField(
            model_name='variable',
            name='source',
            field=models.CharField(default='Personal', max_length=20),
        ),
    ]
