# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-03-15 17:42
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eppdata', '0004_auto_20170214_1615'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='method',
            name='classe',
        ),
        migrations.DeleteModel(
            name='Classe',
        ),
    ]
