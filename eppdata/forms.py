# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django import forms
from django.utils.translation import gettext_lazy as _
from dal import autocomplete
from dal import autocomplete
import datetime

from eppdata.models import Rawdata, Variable
from entities.models import Germplasm
from actors.models import Location

class AddDataForm(forms.ModelForm):
    class Meta:
        model = Rawdata
        fields = ('variable', 'raw_data', 'method', 'date')
        widgets={'raw_data': forms.TextInput(),
                 'date':forms.DateInput(attrs = {'class':'datepicker'})}

    def clean(self):
        cleaned_data = super(AddDataForm, self).clean()
        
        cleaned_variable = cleaned_data.get("variable")
        cleaned_value = cleaned_data.get("raw_data")
        cleaned_method = cleaned_data.get("method")
        if not (cleaned_variable == None and cleaned_value == "" and cleaned_method == None) and \
               not (cleaned_variable != None and cleaned_value != "" and cleaned_method != None) :
            raise forms.ValidationError(_("Variable, value and method must be filed together or empty together"))

        return cleaned_data
    
class DeleteDataset(forms.Form):
    
    tuple_vals = []
    for v in range(datetime.date.today().year - 10, datetime.date.today().year + 10) :
        tuple_vals.append((v,v))
    
    location =  forms.ModelMultipleChoiceField(label=_("Locations"),
                                       queryset=Location.objects.all().order_by('short_name'),
                                       required=False,
                                       widget=autocomplete.ModelSelect2Multiple(url='location-autocomplete', attrs={'style': 'width:150px'}))
    germplasm =  forms.ModelMultipleChoiceField(label=_("Germplasms"),
                                       queryset=Germplasm.objects.all().order_by('idgermplasm'),
                                       required=False,
                                       widget=autocomplete.ModelSelect2Multiple(url='germplasm-autocomplete', attrs={'style': 'width:150px'}))
    variable = forms.ModelMultipleChoiceField(label=_("Variables"),
                                       queryset=Variable.objects.all().order_by('name'),
                                       required=False,
                                       widget=autocomplete.ModelSelect2Multiple(url='variable-autocomplete', attrs={'style': 'width:150px'}))
    sowing_year = forms.ChoiceField(label=_("Sowing year"), required=True, choices=tuple_vals, initial=datetime.date.today().year)
    start_date = forms.CharField(label=_("Start date"), widget=forms.DateInput(attrs = {'class':'datepicker'}))
    end_date = forms.CharField(label=_("End date"), widget=forms.DateInput(attrs = {'class':'datepicker'}))
    include_data_without_date = forms.BooleanField(label=_("Include data without date"), required=False)


class EditRawdata(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        """
          Mandatory for remote_forms module, due to the integration of 
          ModelChoiceIteratorValue for ModelChoiceField in Django 3.1. 
          Replace ModelChoiceIteratorValue by the simple value of the object,
          as it was before, so that the remote_form module can serialize 
          data to JSON format.
          See: https://code.djangoproject.com/ticket/32013
        """
        super().__init__(*args, **kwargs)
        for field_name in ["variable", "method"]:
            field = self.fields[field_name]
            
            new_choices = []
            for k, v in field.choices:
                if type(k) is str:
                    new_choices.append((k, v))
                else:
                    new_choices.append((k.value, v))
            
            field.choices = new_choices
    class Meta:
        model = Rawdata
        fields = ['variable', 'method', 'raw_data', 'date','comment']
        widgets = {
                   'variable': forms.TextInput(attrs={'disabled': True}),
        }
