# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira

    This file is part of SHiNeMaS
    
    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SHiNeMaS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""
    
from django.conf import settings
from django import http
from django.utils import translation

from common.forms import GlobalSearchForm

def rooturl(request):
    return {'ROOT_URL':settings.ROOT_URL, 'MEDIA_URL': settings.MEDIA_URL}

def current_language(request):
    if request.COOKIES.get(settings.LANGUAGE_COOKIE_NAME) :
        return {'LANG': request.COOKIES.get(settings.LANGUAGE_COOKIE_NAME)}
    else :
        translation.activate('en')
        request.session[translation.LANGUAGE_SESSION_KEY] = 'en'
        http.HttpResponseRedirect('/').set_cookie(settings.LANGUAGE_COOKIE_NAME, 'en')
        return {'LANG': 'en'}

def global_search_fom(request):
    return {'global_search':GlobalSearchForm()}
