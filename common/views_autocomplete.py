# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from dal import autocomplete

from network.models import Relation, Mixture, Selection, Reproduction, Diffusion
from entities.models import Germplasm, Seedlot, GermplasmType
from eppdata.models import Variable
from actors.models import Location, StorageDevice

from django.db.models import Q
from django.conf import settings
from cProfile import label

if settings.WEATHER :
    from weather.models import WeatherStation
    from geopy.distance import geodesic

class DiffusionAdminAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Diffusion.objects.none()
    
        qs = Diffusion.objects.prefetch_related('relation_set__seed_lot_father','relation_set__seed_lot_son').all()
        if self.q:
            qs = qs.filter(Q(relation_set__seed_lot_father__name__icontains =self.q)|Q(relation_set__seed_lot_son__name__icontains =self.q))
        return qs

class MixtureAdminAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Mixture.objects.none()
    
        qs = Mixture.objects.prefetch_related('relation_set__seed_lot_father','relation_set__seed_lot_son').all()
        if self.q:
            qs = qs.filter(Q(relation_set__seed_lot_father__name__icontains =self.q)|Q(relation_set__seed_lot_son__name__icontains =self.q))
        return qs

class SelectionAdminAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Selection.objects.none()
    
        qs = Selection.objects.prefetch_related('relation_set__seed_lot_father','relation_set__seed_lot_son').all()
        if self.q:
            qs = qs.filter(Q(relation_set__seed_lot_father__name__icontains =self.q)|Q(relation_set__seed_lot_son__name__icontains =self.q))
        return qs

class ReproductionAdminAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Reproduction.objects.none()
    
        qs = Reproduction.objects.prefetch_related('relation_set__seed_lot_father','relation_set__seed_lot_son').all()
        if self.q:
            qs = qs.filter(Q(relation__seed_lot_father__name__icontains =self.q)|Q(relation__seed_lot_son__name__icontains =self.q))
        return qs

class MixtureAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Mixture.objects.none()
        qs = Mixture.objects.prefetch_related('relation_set__seed_lot_father','relation_set__seed_lot_son').all()
        if self.q:
            qs = qs.filter(Q(relation__seed_lot_father__name__icontains =self.q)|Q(relation__seed_lot_son__name__icontains =self.q))
        return qs
    
class DiffusionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Relation.objects.none()
    
        qs = Relation.objects.select_related('seed_lot_father','seed_lot_son').filter(diffusion__isnull = False)
        if self.q:
            qs = qs.filter(Q(seed_lot_father__name__icontains =self.q)|Q(seed_lot_son__name__icontains =self.q))
        return qs

class CrossAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Relation.objects.none()
    
        qs = Relation.objects.select_related('seed_lot_father','seed_lot_son').filter(Q(is_male='M')|Q(is_male='F'), reproduction__isnull = False, selection__isnull=True)
        if self.q:
            qs = qs.filter(Q(seed_lot_father__name__icontains =self.q)|Q(seed_lot_son__name__icontains =self.q))
        return qs

class ReproductionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Relation.objects.none()
    
        qs = Relation.objects.select_related('seed_lot_father','seed_lot_son').filter(reproduction__isnull = False, selection__isnull=True, is_male='X')
        if self.q:
            qs = qs.filter(Q(seed_lot_father__name__icontains =self.q)|Q(seed_lot_son__name__icontains =self.q))
        return qs

class SelectionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Relation.objects.none()
    
        qs = Relation.objects.select_related('seed_lot_father','seed_lot_son').filter(selection__isnull=False)
        if self.q:
            qs = qs.filter(Q(seed_lot_father__name__icontains =self.q)|Q(seed_lot_son__name__icontains =self.q))
        return qs

class RelationAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Relation.objects.none()

        qs = Relation.objects.select_related('seed_lot_father','seed_lot_son').all()
        if self.q:
            qs = qs.filter(Q(seed_lot_father__name__icontains =self.q)|Q(seed_lot_son__name__icontains =self.q))
        return qs

class VariableAutocomplete(autocomplete.Select2QuerySetView):
    
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Variable.objects.none()

        qs = Variable.objects.all()
        
        period = self.forwarded.get('period', None)
        
        if period:
            qs = qs.filter(type=period)
             
        if self.q:
            if period :
                qs = qs.filter(name_descriptor__icontains = self.q)
            else :
                qs = qs.filter(name__icontains = self.q)
        return qs
    
class WeatherStationAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return WeatherStation.objects.none()

        period = self.forwarded.get('period', None)
        loc = self.forwarded.get('location', None)
        
        qs = WeatherStation.objects.all()
        
        if loc != None :
            if loc != '':
                location = Location.objects.get(id = loc)
                coord = (location.latitude,location.longitude)
            else :
                return WeatherStation.objects.none()
        else :
            coord = self.request.session['0']
        
        if period == 'h':
            qs = qs.filter(~Q(source = 'Custom'))
        
        stations = WeatherStation.objects.all()
        stations_final = []
        for s in stations :
            st = (s.latitude, s.longitude)
            d = geodesic(coord,st).kilometers
            if d < settings.DISTANCE:
                s.distance = d
                stations_final.append(s)
        stations_final.sort(key=lambda x: x.distance)
        stations_final.sort(key=lambda x: x.status, reverse=True)

        return stations_final
        

    
    def get_result_label(self, result):
        if result.depth == 1:
            return result.name
        elif result.depth == 2:
            return "{0} > {1}".format(result.storage_device.name, result.name)
        elif result.depth == 3:
            return "{0} > {1} > {2}".format(result.storage_device.storage_device.name, result.storage_device.name, result.name)
        elif result.depth == 4:
            return "{0} > {1} > {2} > {3}".format(result.storage_device.storage_device.storage_device.name, result.storage_device.storage_device.name, result.storage_device.name, result.name)

class GlobalSearchAutocomplete(autocomplete.Select2QuerySetSequenceView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return autocomplete.QuerySetSequence()

        qs_germplasm = Germplasm.objects.all()
        qs_seed_lot = Seedlot.objects.all()
        qs_relation = Relation.objects.select_related('seed_lot_father','seed_lot_son').all()
        if self.q:
            qs_germplasm = qs_germplasm.filter(name__icontains = self.q)
            qs_seed_lot = qs_seed_lot.filter(name__icontains = self.q)
            qs_relation = qs_relation.filter(Q(seed_lot_father__name__icontains =self.q)|Q(seed_lot_son__name__icontains =self.q))
        return autocomplete.QuerySetSequence(
            qs_germplasm,
            qs_seed_lot,
            qs_relation
        )
