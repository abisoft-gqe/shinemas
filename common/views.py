# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira

    This file is part of SHiNeMaS
    
    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SHiNeMaS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""


from django.shortcuts import render
from django.utils import translation
from django import http
from django.conf import settings
from django.urls import reverse
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import redirect

from common.forms import GlobalSearchForm
from network.models import Relation
from entities.models import Germplasm, Seedlot

def authors(request):
    return render(request,'about/authors.html', {})

def nextfeatures(request):
    return render(request,'about/next.html', {})

def change_langage(request):
    if request.method == "POST" :
        user_language = request.POST['language']
        translation.activate(user_language)
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language
        response = http.HttpResponseRedirect(request.META['HTTP_REFERER'])
        response.set_cookie(settings.LANGUAGE_COOKIE_NAME, user_language)
        return response
    else :
        return http.HttpResponseRedirect(reverse('home'))

def global_search(request):
    if request.method=="POST":
        form = GlobalSearchForm(data=request.POST)
        if form.is_valid() :
            content_type_id = request.POST['search'].split('-')[0]
            content_type = ContentType.objects.get(id=content_type_id)
            object_id = request.POST['search'].split('-')[1]
            if content_type.model_class() == Relation :
                return redirect('relation_profil',relation_id=object_id)
            elif content_type.model_class() == Germplasm :
                return redirect('germplasm_profil',germplasm_id=object_id)
            elif content_type.model_class() == Seedlot:
                return redirect('seedlot_profil',seedlot_id=object_id)
            else :
                return http.HttpResponseRedirect(reverse('home'))
    else :
        return http.HttpResponseRedirect(reverse('home'))
        