from django import forms
from django.utils.translation import gettext_lazy as _

import dal_queryset_sequence
from dal import autocomplete
import dal_select2_queryset_sequence

from entities.models import Germplasm, Seedlot
from network.models import Relation

LANGUAGES = (
                        ('fr', '<img style="float:right;" src="/static/images/fr.png">'),
                        ('en','<img style="float:right;" src="/static/images/gb.png">')
                        )

class LanguageForm(forms.Form):
    language = forms.ChoiceField(choices=LANGUAGES)
    
class GlobalSearchForm(forms.Form):
    search = dal_queryset_sequence.fields.QuerySetSequenceModelField(label=_("Search in database"),
                                       queryset=autocomplete.QuerySetSequence(
                                                                                Germplasm.objects.all(),
                                                                                Seedlot.objects.all(),
                                                                                Relation.objects.all()
                                                                                ),
                                       required=False,
                                       widget=dal_select2_queryset_sequence.widgets.QuerySetSequenceSelect2(url='search-autocomplete', attrs={'style': 'width:100px'}))