# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.urls import path
from common.views_autocomplete import RelationAutocomplete, VariableAutocomplete, GlobalSearchAutocomplete, \
                                MixtureAutocomplete, DiffusionAutocomplete, CrossAutocomplete, ReproductionAutocomplete, SelectionAutocomplete, WeatherStationAutocomplete, \
                                MixtureAdminAutocomplete, DiffusionAdminAutocomplete, ReproductionAdminAutocomplete, SelectionAdminAutocomplete
from common.views import global_search

urlpatterns = [
                       path('mixtureadmin-autocomplete/', MixtureAdminAutocomplete.as_view(), name='mixtureadmin-autocomplete'),
                       path('diffusionadmin-autocomplete/', DiffusionAdminAutocomplete.as_view(), name='diffusionadmin-autocomplete'),
                       path('reproductionadmin-autocomplete/', ReproductionAdminAutocomplete.as_view(), name='reproductionadmin-autocomplete'),
                       path('selectionadmin-autocomplete/', SelectionAdminAutocomplete.as_view(), name='selectionadmin-autocomplete'),
                       path('mixture-autocomplete/', MixtureAutocomplete.as_view(), name='mixture-autocomplete'),
                       path('diffusion-autocomplete/', DiffusionAutocomplete.as_view(), name='diffusion-autocomplete'),
                       path('cross-autocomplete/', CrossAutocomplete.as_view(), name='cross-autocomplete'),
                       path('reproduction-autocomplete/', ReproductionAutocomplete.as_view(), name='reproduction-autocomplete'),
                       path('selection-autocomplete/', SelectionAutocomplete.as_view(), name='selection-autocomplete'),
                       path('relation-autocomplete/', RelationAutocomplete.as_view(), name='relation-autocomplete'),
                       path('variable-autocomplete/', VariableAutocomplete.as_view(), name='variable-autocomplete'),
                       
                       path('search-autocomplete/', GlobalSearchAutocomplete.as_view(), name='search-autocomplete'),
                       path('weatherstation-autocomplete/', WeatherStationAutocomplete.as_view(), name='weatherstation-autocomplete'),
                       path('globalsearch/', global_search, name='globalsearch'),
                       

                       ]
