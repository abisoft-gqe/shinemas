# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
    
    
    Define administration interface configuration\n
    For each ModelAdmin class :\n
    1. fieldsets enable form configuration design\n
    2. list_display is the list of field to display in the whole table\n
    3. search_fields is a list of field you can use to search data\n
    4. list_filter is a list of fields you can use to filter data\n
    
    For more information, read the `django admin documentation <https://docs.djangoproject.com/en/1.6/ref/contrib/admin/>`_\n
"""

from django.utils.translation import gettext_lazy as _
from django.contrib import admin

from import_export.admin import ImportExportModelAdmin
from import_export import resources, fields, widgets

from actors.models import Person, Location, Project
#admin.site.unregister(Site)
class LocationResource(resources.ModelResource):
    class Meta:
        model = Location
        exclude = ('id',)
        import_id_fields = ('short_name',)

class PersonResource(resources.ModelResource):
    location = fields.Field(column_name='location', attribute='location', widget=widgets.ForeignKeyWidget(Location, field='short_name'))
    
    class Meta:
        model = Person
        exclude = ('id',)
        import_id_fields = ('short_name',)

class PersonAdmin(ImportExportModelAdmin):
    """
        The ModelAdmin class for
        :py:class:`actors.models.Person`
        model
    """
    fieldsets = [
        
        (_('Person informations'), {'fields': ['first_name', 'last_name', 'short_name'] }),
        (_('Contact informations'), {'classes': ['collapse',],
								'fields': ['email','phone1', 'phone2', 'fax', 'location'] })
        

        ]
    list_display = ('short_name','first_name','last_name')
    search_fields = ['last_name',]
    ordering = ('short_name',)
    resource_class = PersonResource


class LocationAdmin (ImportExportModelAdmin):
    """
        The ModelAdmin class for
        :py:class:`actors.models.Location`
        model
    """
    list_display = ('location_name', 'short_name', 'location_type', 'address', 'post_code', 'longitude', 'latitude', 'altitude')
    search_fields = ['location_name',]
    ordering = ('short_name',)
    resource_class = LocationResource

class ProjectAdmin (admin.ModelAdmin):
    """
        The ModelAdmin class for
        :py:class:`actors.models.Project`
        model
    """
    list_display = ('project_name','start_date','end_date')

admin.site.register(Person, PersonAdmin)
admin.site.register(Location,LocationAdmin)
admin.site.register(Project,ProjectAdmin)