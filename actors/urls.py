# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""


from django.urls import path
from actors.views import getFile, storagedevice, tree_location, editstoragedevice, deletestoragedevice, search_location_devices, store_seedlots, storagecontents
from actors.views_autocomplete import LocationAutocomplete, DeviceAutocomplete, PersonAutocomplete

urlpatterns = [
            path('get-file/<str:file_name>/', getFile, name='getfile'), #à déplacre
            path('create/storagedevice/<int:object_id>/<str:content_type>/', storagedevice, name='storagedevice'),
            path('edit/storagedevice/<int:object_id>/', editstoragedevice, name='editstoragedevice'),
            path('delete/storagedevice/<int:object_id>/', deletestoragedevice, name='deletestoragedevice'),
            path('storagedevice/', search_location_devices, name='storagedevices'),
            path('storagedevice/<int:object_id>/', tree_location, name='locationstorages'),
            path('storagedevice/<int:object_id>/contents/', storagecontents, name='storagecontents'),
            path('storeseedlots/', store_seedlots, name='storeseedlots'),
            path('storagedevices/autocomplete/', DeviceAutocomplete.as_view(), name='device-autocomplete'),
            path('locations/autocomplete/', LocationAutocomplete.as_view(), name='location-autocomplete'),
            path('people/autocomplete/', PersonAutocomplete.as_view(), name='person-autocomplete')
]
