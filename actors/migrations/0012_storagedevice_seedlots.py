# Generated by Django 2.2 on 2021-11-17 17:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0020_delete_auditfileseedlot'),
        ('actors', '0011_auto_20210603_1458'),
    ]

    operations = [
        migrations.AddField(
            model_name='storagedevice',
            name='seedlots',
            field=models.ManyToManyField(to='entities.Seedlot'),
        ),
    ]
