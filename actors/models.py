# -*- coding: utf-8 -*-
"""LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE

    This package describe the infrastructure of the database.
    Those models are not really related to data but describes the actors of this database
    
    :data `GENDER_CHOICES`: Tuple that defines gender choice for :py:class:`Person` model
"""

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation

from actors.managers import LocationManager, StorageDeviceManager

GENDER_CHOICES = (
                  (u'M', _(u'Male')),
                  (u'F', _(u'Female')),
                  )

class Location (models.Model):
    """
        The Location model give information about farm localization
        
    
        :var CharField location_name: The name of this Location
        :var CharField location_type: The type of this Location
        :var CharField address: Address of this Location
        :var CharField post_code: The post code of this Location
        :var CharField country: The country of this Location
        :var CharField longitude: The longitude of this Location, GPS coordinate are used for climatic data
        :var CharField latitude: The latitude of this Location, GPS coordinate are used for climatic data
        :var CharField altitude: The altitude of this Location, GPS coordinate are used for climatic data
    """
    location_name= models.CharField(_("Name"),max_length=100,blank=True,null=True)
    location_type= models.CharField(_("Type"),max_length=100,blank=True,null=True)
    address = models.CharField(_("Address"),max_length=200,blank=True,null=True)
    post_code = models.CharField(_("Post Code"),max_length=200,blank=True,null=True)
    country= models.CharField(_("Country"),max_length=100,blank=True,null=True)
    longitude=models.FloatField(_("Longitude"),max_length=50,blank=True,null=True)
    latitude=models.FloatField(_("Latitude"),max_length=50,blank=True,null=True)
    altitude=models.FloatField(_("Altitude"),max_length=50,blank=True,null=True)
    short_name = models.CharField(_("Short Name"),max_length=15,unique=True)
    
    objects = LocationManager()
    devices = GenericRelation('StorageDevice', related_query_name='location')
    def __unicode__(self):
        return u"%s"%self.short_name
    def __str__(self):
        return "%s"%self.short_name

class Person (models.Model):
    """
    The Person model contains the description of the network actors. It is different of database users who have access to the database.
    
    :var CharField first_name: The first name of this Person
    :var CharField last_name: The last name of this Person
    :var CharField short_name: It is a short description of the complet name of a Person, it is used in the barcode of seed lot (their database name)
    :var DateField birth_date: Birth date of the Person
    :var CharField gender: Gender of the Person
    :var CharField email: E-mail of the Person
    :var CharField phone1: The first phone number of the Person
    :var CharField phone2: The second phone number of the Person
    :var CharField fax: The fax of The Person
    :var ForeignKey location: The :py:class:`Location` corresponding to this Person (FK Location)
    """

    first_name = models.CharField(_("First name"),max_length=100,blank=True,null=True)
    last_name = models.CharField(_("Last name"),max_length=100,blank=True,null=True)
    short_name = models.CharField(_("Short name"), max_length=50,unique=True) 
    email = models.CharField(_("E-mail"),max_length=75,blank=True,null=True)
    phone1 = models.CharField(_("Phone 1"),max_length=200,blank=True,null=True)
    phone2 = models.CharField(_("Phone 2"),max_length=200,blank=True,null=True)
    fax = models.CharField(max_length=200,blank=True,null=True)
    location = models.ForeignKey('Location', blank=True, null=True, on_delete=models.SET_NULL)

    def __unicode__(self):
        return u"%s"%self.short_name
      
    def __str__(self):
        return "%s"%self.short_name

class Project (models.Model):
    """
    :var ManyToMany person: the :py:class:`Person` that belong to this Project
    :var CharField project_name: The name of the Project
    :var DateField start_date: The starting date of this Project
    :var DateField end_date: The ending date of this Project
    """
    person = models.ManyToManyField(Person, blank=True)
    project_name = models.CharField(_("Name"),max_length=50,unique=True)
    start_date = models.DateField(_("Start date"))
    end_date = models.DateField(_("End date"),blank=True,null=True)
    
    def __unicode__(self):
        return u"%s"%self.project_name
    def __str__(self):
        return "%s"%self.project_name
    
class StorageDevice (models.Model):
    """
    The StorageDevice give information about the location of seed lot on the farm
    :var Charfield name: The name of the storage device
    :var CharField comment: A comment can be associated with the storage device
    :var DecimalField depth: The storage depth
    GenericForeignKey storage_device: allows you to location if the storage device is level1 and to the previous level storage device if the storage device is higher than 1
    """
    name = models.CharField(_("Name"), max_length=100)
    comment = models.TextField(_("Comment"), max_length=250, blank=True, null=True)
    depth = models.PositiveIntegerField(_("Level"), validators=[
            MaxValueValidator(4),
            MinValueValidator(1)
        ])
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    storage_device = GenericForeignKey('content_type', 'object_id')
    devices = GenericRelation('StorageDevice')
    pdevice = GenericRelation('StorageDevice', related_query_name='parent_device')
    objects = StorageDeviceManager()
    
    class Meta:
        unique_together = ['name', 'content_type', 'object_id']
        ordering = ordering = ['name']
    
    def __str__(self):
        return "{0}".format(self.name)

    def complete_name(self):
        if self.depth == 1:
            return self.name
        elif self.depth == 2:
            return "{0} > {1}".format(self.storage_device.name, self.name)
        elif self.depth == 3:
            return "{0} > {1} > {2}".format(self.storage_device.storage_device.name, self.storage_device.name, self.name)
        elif self.depth == 4:
            return "{0} > {1} > {2} > {3}".format(self.storage_device.storage_device.storage_device.name, self.storage_device.storage_device.name, self.storage_device.name, self.name)


    def get_from_location(self):
        parent = self.storage_device
        while type(parent) is not Location:
            parent = parent.get_from_location()
        return parent
    
    def store_seedlot(self, seedlot):
        seedlot.storage_device = self
        seedlot.save()
