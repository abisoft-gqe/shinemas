# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

#NETTOYAGE
import os

from django.conf import settings
from django.core.files import File
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.contenttypes.models import ContentType
from django.contrib import messages
from django.db import DatabaseError, transaction
from django.core.exceptions import NON_FIELD_ERRORS, ValidationError
from django.urls import reverse
from django.template import RequestContext
from django.http import HttpResponse, Http404, HttpResponseBadRequest, HttpResponseRedirect
from django.contrib.auth.decorators import user_passes_test, login_required

from actors.models import Person, StorageDevice, Location
from actors.exceptions import StorageException
from entities.models import Seedlot
from csv_io.models import AuditFile
from csv_io.factory import CSVFactoryForTempFiles
from csv_io.exceptions import AlreadySubmitedFile
from actors.forms import CreateStorageDevice, LocationSearch, StorageSearch



# def index(request):
#     latest_person_list = Person.objects.all()
#     return render(request, 'actors/index.html', {'latest_person_list': latest_person_list})
#
#
#
# def detail(request, person_id):
#     p = get_object_or_404(Person, pk=person_id)
#     return render(request, 'actors/detail.html', {'person': p})

    
@user_passes_test(lambda u: u.is_superuser)
@login_required     
def getFile(request,file_name):
    """username=request.user
    user = User.objects.get(username=username)
    files=AuditFileSeedLot.objects.filter(user=username)
    print("audit",AuditFileSeedLot.objects.all())
    """
    path=os.path.join(settings.MEDIA_ROOT,file_name)
    f = open(path, 'r')
    fileContent = File(f)
    response = HttpResponse(fileContent, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename='+str(file_name)+''
        
    return response
    

@user_passes_test(lambda u: u.is_superuser)
@login_required 
def storagedevice(request, object_id, content_type):
    parent = None
    try:  
        if content_type == 'location':
            parent = Location.objects.get(id = object_id)
        elif content_type == 'storagedevice':
            parent = StorageDevice.objects.get(id = object_id)
    except:
        raise Http404()
    if content_type in ['location', 'storagedevice']: 
        if request.method == "GET":
            form = CreateStorageDevice()
            return render(request, 'actors/create_storagedevice.html', {'form':form, 'parent':parent})
        
        elif request.method == "POST":
            ctlocation = ContentType.objects.get(model='location')
            ctstoragedevice = ContentType.objects.get(model='storagedevice')
            form = CreateStorageDevice(data = request.POST)
            if form.is_valid():
                if content_type == 'location':
                    try:
                        storagedevice = StorageDevice(depth=1, content_type=ctlocation, object_id = object_id, name = request.POST['name'], comment = request.POST['comment'])
                        storagedevice.save()  
                    except DatabaseError:
                        messages.error(request, 'Storage Device with this name already exist.')   
                elif content_type == 'storagedevice':  
                    storagedevice = StorageDevice(depth=parent.depth+1, content_type=ctstoragedevice, object_id = object_id, name = request.POST['name'], comment = request.POST['comment'])    
                    try :
                        storagedevice.full_clean()
                    except ValidationError as e:
                        messages.error(request, e.message_dict)
                    else :
                        try:
                            storagedevice.save()  
                        except DatabaseError as e:
                            messages.error(request, e) 
                
                return redirect('locationstorages', object_id=storagedevice.get_from_location().id) 
                        
            else:
                return render(request, 'actors/create_storagedevice.html', {'form': form, 'parent': parent})      
            
    else :
        raise Http404()  

@login_required 
def search_location_devices(request):
    location_form = LocationSearch()
    if request.method == 'GET':
        return render(request, 'actors/tree_storagedevice.html', {'form': location_form})
    elif request.method == "POST":
        return redirect('locationstorages', object_id=request.POST["location"])

@user_passes_test(lambda u: u.is_superuser)
@login_required 
def tree_location(request, object_id):
    location_form = LocationSearch()
    location = Location.objects.get(id=object_id)
    return render(request, 'actors/tree_storagedevice.html', {'form': location_form, 'location':location})

@user_passes_test(lambda u: u.is_superuser)
@login_required 
def storagecontents(request, object_id):
    loc = Location.objects.prefetch_related('devices').get(id=object_id)
    devices = loc.devices.all()
    return render(request, "actors/storage_contents.html", {'location':loc, 'devices':devices})

@user_passes_test(lambda u: u.is_superuser)
@login_required 
def editstoragedevice(request, object_id):
    if request.method == 'GET':
        try:
            storagedevice = StorageDevice.objects.get(id = object_id)
            form = CreateStorageDevice(instance = storagedevice)
        except DatabaseError:
            messages.error(request, 'Storage Device with this name already exist.')  
        return render(request, 'actors/edit_storagedevice.html', {'form':form, 'from_location':storagedevice.get_from_location()})   
    
    
    elif request.method == 'POST':
        try:
            storagedevice = StorageDevice.objects.get(id = object_id)
            
        except DatabaseError:
            messages.error(request, 'Storage Device with this name already exist.') 
        form = CreateStorageDevice(instance = storagedevice, data=request.POST)
        if form.is_valid():
            storage_device = form.save()
            return redirect('locationstorages', object_id=storage_device.get_from_location().id) 

@user_passes_test(lambda u: u.is_superuser)
@login_required 
def deletestoragedevice(request, object_id):
    storagedevice = StorageDevice.objects.get(id = object_id)
    from_location = storagedevice.get_from_location()
    storagedevice.delete()
    messages.success(request, "Storage device have been deleted")
    return redirect('locationstorages', object_id=from_location.id)

@user_passes_test(lambda u: u.is_superuser)
@transaction.non_atomic_requests
@login_required 
def store_seedlots(request):
    if request.method == "GET":
        return render(request,"actors/uploadStorage.html",{})
    elif request.method == "POST":
        file_handler = request.FILES['file']
        filefactory = CSVFactoryForTempFiles(file_handler)
        filefactory.find_dialect()
        filefactory.read_file()
        filedict = filefactory.get_file()
        hashprint = filefactory.getHash()
        
        try :
            AuditFile.objects.check_submited_files(hashprint, request.user)
        except AlreadySubmitedFile as e:
            messages.add_message(request, messages.ERROR, str(e))
            return render(request, 'actors/uploadStorage.html', {})
        
        errors = False
        storages = {}
        with transaction.atomic():
            i=1     
            for line in filedict:
                seedlot=None
                try :
                    seedlot = Seedlot.objects.select_related('location').get(name=line["seedlot"])
                except Seedlot.DoesNotExist :
                    msg = "Line {0} : Seedlot {1} doesn't exists".format(i, line["seedlot"])
                    messages.add_message(request, messages.ERROR, msg)
                    errors = True
                else :
                    if not seedlot.location.short_name in storages.keys() :
                        storages[seedlot.location.short_name] = StorageDevice.objects.visit_storages(seedlot.location)
                    try :
                        seedlot.store_from_file(line, storages)
                    except StorageException as e:
                        msg = "Line {0} : {1}".format(i, str(e))
                        messages.add_message(request, messages.ERROR, msg)
                        errors = True
                i += 1
        if errors :
            transaction.rollback()
            return render(request,"actors/uploadStorage.html",{})
        AuditFile.objects.audit_file(file_handler, request.user, hashprint)
        transaction.commit()
        
        messages.add_message(request, messages.SUCCESS,"File submited with success")
        return render(request,"actors/uploadStorage.html",{})
    else :
        return render(request,"actors/uploadStorage.html",{})

