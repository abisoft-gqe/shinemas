from dal import autocomplete

from actors.models import Location, Person, StorageDevice


class LocationAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Location.objects.none()

        qs = Location.objects.all().order_by('short_name')
        if self.q:
            qs = qs.filter(short_name__icontains = self.q)
        return qs
    
class PersonAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Person.objects.none()

        qs = Person.objects.all().order_by('short_name')
        if self.q:
            qs = qs.filter(short_name__icontains = self.q)
        return qs

class DeviceAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return StorageDevice.objects.none()

        qs = StorageDevice.objects.all()
        if self.q:
            qs1 = qs.filter(depth=1, name__icontains = self.q)
            qs2 = qs.filter(Q(parent_device__name__icontains = self.q)|Q(name__icontains = self.q), depth=2)
            qs3 = qs.filter(Q(parent_device__parent_device__name__icontains = self.q)|Q(parent_device__name__icontains = self.q)|Q(name__icontains = self.q), depth=3)
            qs4 = qs.filter(Q(parent_device__parent_device__parent_device__name__icontains = self.q)|Q(parent_device__parent_device__name__icontains = self.q)|Q(parent_device__name__icontains = self.q)|Q(name__icontains = self.q),depth=4)
            qs = qs1.union(qs2,qs3,qs4)
        return qs