from django import forms
from django.db.models import Q
from django.utils.translation import gettext_lazy as _

from dal import autocomplete, forward

from actors.models import StorageDevice, Location

class CreateStorageDevice(forms.ModelForm):
    class Meta:
        model = StorageDevice
        fields = ['name', 'comment']

class LocationSearch(forms.Form):
    location = forms.ModelChoiceField(label=_("Search for Location "), required=False,
                            queryset=Location.objects.all().order_by('short_name'),
                            widget=autocomplete.ModelSelect2(url='location-autocomplete', attrs={'style': 'width:100px'}),
                                      )
    
class StorageSearch(forms.Form):
    
    storages = forms.ModelChoiceField(label=_("Search for Storage "), required=False,
                            queryset=StorageDevice.objects.all(),
                            widget=autocomplete.ModelSelect2(url='device-autocomplete', attrs={'style': 'width:100px'})
                                      )
    def __init__(self, *args, **kwargs):
        self.location = kwargs.pop('location', None)
        super(StorageSearch, self).__init__(*args, **kwargs)
        if location:
            ##pb on ne peut pas faire un simple filter, il faut faire une requete recursive
            #--> un solution peut être Q(depth=1, storage_device_id=id)|Q(depth=2, storage_device__storage_device_id=..)
            self.fields['storages'].queryset = StorageDevice.objects.filter(Q(depth=1, location__id=location.id)| \
                                                                            Q(depth=2, parent_device__location__id=location.id)| \
                                                                            Q(depth=3, parent_device__parent_device__location__id=location.id)| \
                                                                            Q(depth=4, parent_device__parent_device__parent_device__location__id=location.id))
            print(self.fields['storages'].queryset)

