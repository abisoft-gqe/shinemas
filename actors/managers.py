# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.apps import apps
from django.db import models

class LocationManager(models.Manager):
    """
        Manager to get Location object from a string seed lot name
        
    """
    def get_location_from_seedlotname(self, seedlotname) :
        Location = apps.get_model('actors','location')
        try :
            return Location.objects.get(short_name=seedlotname.split('_')[1])
        except :
            return None

class StorageDeviceManager(models.Manager):
    
    def visit_storages(self, location):
        storages = {}
        for sd1 in location.devices.all() :
            storages[sd1.name]={}
            storages["{0}-object".format(sd1.name)]=sd1
            for sd2 in sd1.devices.all() :
                storages[sd1.name][sd2.name]={}
                storages[sd1.name]["{0}-object".format(sd2.name)]=sd2
                
                for sd3 in sd2.devices.all() :
                    storages[sd1.name][sd2.name][sd3.name]={}
                    storages[sd1.name][sd2.name]["{0}-object".format(sd3.name)]=sd3
                    
                    for sd4 in sd3.devices.all() :
                        storages[sd1.name][sd2.name][sd3.name]["{0}-object".format(sd4.name)]=sd4
        return storages