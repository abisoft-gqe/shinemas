from rest_framework import serializers
from .models import Location, Project

class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ['short_name','location_name','country']
        
class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ['project_name','start_date','end_date']