# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""
import datetime

from django.test import TestCase
from django.apps import apps
from django.contrib.contenttypes.models import ContentType

class URLsTest(TestCase):
    """ 
        urls to test 
        
        getfile GET
        
        editstoragedevice GET/POST
        locationstorages GET
        storagecontents GET
        storeseedlots GET/POST
    """
    def setUp(self):
        User = apps.get_model('auth','user')
        User.objects.create_superuser(username = "admin",
                             password = "admin")
        self.client.post('/login/', {'username': 'admin', 'password':'admin'})
        
        Location = apps.get_model('actors','location')
        self.location1 = Location.objects.create(
                location_name = "Ferme du Moulon",
                location_type = "Station exp",
                address = "ferme du moulon",
                post_code = "91190",
                country = "France",
                longitude = "34.7",
                latitude = "33.2",
                altitude = "111",
                short_name = "MLN")
        
        Person = apps.get_model('actors','person')
        self.person1 = Person.objects.create(
                first_name = "Yannick",
                last_name = "De Oliveira",
                short_name = "YDO",
                email = "yannick.de-oliveira@inra.fr",
                phone1 = "01.69.33.23.76",
                phone2 = "01.69.33.33.33",
                fax = "01.69.33.20.20",
                location = self.location1,
                              )
        
        StorageDevice = apps.get_model('actors','storagedevice')
        self.storage1 = StorageDevice.objects.create(name="Main storage",
                                                     depth=1,
                                                     content_type=ContentType.objects.get(app_label='actors',model='location'),
                                                     object_id=self.location1.id,
                                                     )
        
        
    
    def test_url_404(self):
        response = self.client.get('/actors/do/not/exists/')
        self.assertEqual(response.status_code, 404)
        
    def test_get_storagedevice(self):
        response = self.client.get('/actors/create/storagedevice/{0}/{1}/'.format(self.location1.id,'location'))
        self.assertEqual(response.status_code, 200)
    
    def test_get_storagedevice2(self):
        response = self.client.get('/actors/create/storagedevice/{0}/{1}/'.format(self.storage1.id,'storagedevice'))
        self.assertEqual(response.status_code, 200)
    
    def test_post_storagedevice(self):
        data = {'name': 'storage level 2',
                'comment':''}
        response = self.client.post('/actors/create/storagedevice/{0}/{1}/'.format(self.location1.id,'location'), data)
        self.assertEqual(response.status_code, 302)
    
    def test_post_storagedevice2(self):
        data = {'name': 'storage level 3',
                'comment':'Storage device of level 3'}
        response = self.client.post('/actors/create/storagedevice/{0}/{1}/'.format(self.storage1.id,'storagedevice'), data)
        self.assertEqual(response.status_code, 302)
        
    def test_post_unvalid_storagedevice(self):
        data = {'name': '',
                'comment':'Storage device of level 3 but with no name'}
        response = self.client.post('/actors/create/storagedevice/{0}/{1}/'.format(self.storage1.id,'storagedevice'), data)
        self.assertEqual(response.status_code, 200)
    
    def test_get_storagedevices(self):
        response = self.client.get('/actors/storagedevice/')
        self.assertEqual(response.status_code, 200)
        
    def test_post_storagedevices(self):
        data = {'location':self.location1.id}
        response = self.client.post('/actors/storagedevice/', data)
        self.assertEqual(response.status_code, 302)
    
    def test_get_editstoragedevice(self):
        response = self.client.get('/actors/edit/storagedevice/{}/'.format(self.storage1.id))
        self.assertEqual(response.status_code, 200)
    
    def test_post_editstoragedevice(self):
        data = {'id':self.storage1.id,
                'name':'Storage level 1',
                'comment': ''}
        response = self.client.post('/actors/edit/storagedevice/{}/'.format(self.storage1.id), data)
        self.assertEqual(response.status_code, 302)
    
    def test_get_show_tree_of_devices(self):
        response = self.client.get('/actors/storagedevice/{}/'.format(self.location1.id))
        self.assertEqual(response.status_code, 200)
    
    def test_get_show_content(self):
        response = self.client.get('/actors/storagedevice/{}/contents/'.format(self.location1.id))
        self.assertEqual(response.status_code, 200)
    
    def test_get_storelots(self):
        response = self.client.get('/actors/storeseedlots/')
        self.assertEqual(response.status_code, 200)
    
    def test_post_storelots(self):
        pass
    
    def test_getfile(self):
        pass
    
    def test_get_location_autocomplete(self):
        response = self.client.get('/actors/locations/autocomplete/')
        self.assertEqual(response.status_code, 200)
    
    def test_get_person_autocomplete(self):
        response = self.client.get('/actors/people/autocomplete/')
        self.assertEqual(response.status_code, 200)
    
    def test_get_storagedevice_autocomplete(self):
        response = self.client.get('/actors/storagedevices/autocomplete/')
        self.assertEqual(response.status_code, 200)
    
    def test_get_deletestoragedevice(self):
        response = self.client.get('/actors/delete/storagedevice/{}/'.format(self.storage1.id))
        self.assertEqual(response.status_code, 302)
