from django.apps import AppConfig


class RemoteFormsConfig(AppConfig):
    name = 'remote_forms'
