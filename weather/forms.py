# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014, Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""
from dal import autocomplete

from django import forms
from django.utils.translation import ugettext as _

from actors.models import Location
from weather.models import WeatherStation
from eppdata.models import SEASON_CHOICE, Variable
from django.forms.models import ModelChoiceField

class UpdateWebService(forms.Form):
    
    webservice = forms.ChoiceField(
        widget = forms.RadioSelect,
        #choices = WEB_SERVICE,
    )
    
class UploadLocation(forms.Form):
    
    file = forms.FileField(label=_("Data file"), required=True)
    file_method = forms.FileField(label=_("Methods file"),required=True)
    station = forms.ModelChoiceField(label=_("Station"),queryset=WeatherStation.objects.filter(source='Custom').order_by('station_name'))


TYPE_CHOICE = (('h',_('Hourly')),
             ('d',_('Daily')),
             #('dix',_('Decadal')),
             #('m',_('Monthly')),
             #('y',_('Annual'))
             )

class QueryRelation(forms.Form):
    
    start_date = forms.CharField(widget=forms.DateInput(attrs = {'class':'datepicker', 'autocomplete': 'off'}),required=True)
    end_date = forms.CharField(widget=forms.DateInput(attrs = {'class':'datepicker', 'autocomplete': 'off'}),required=False)
    period = forms.ChoiceField(choices=TYPE_CHOICE,required=True)
    var = forms.ModelMultipleChoiceField(queryset=Variable.objects.all().order_by('name_descriptor'),
                                         required=True,
                                         widget=autocomplete.ModelSelect2Multiple(url='variable-autocomplete', forward=('period',), attrs={'style': 'width:410px'})
                                         )

    station1 = forms.ModelChoiceField(queryset=WeatherStation.objects.all().order_by('station_name'),required=True,
                                             widget=autocomplete.ModelSelect2(url='weatherstation-autocomplete', forward=('period',), attrs={'style': 'width:410px'})
                                             )
    station2 = forms.ModelChoiceField(queryset=WeatherStation.objects.all().order_by('station_name'),
                                      required=False,
                                      widget=autocomplete.ModelSelect2(url='weatherstation-autocomplete', forward=('period',), attrs={'style': 'width:410px'})
                                      )
    station3 = forms.ModelChoiceField(queryset=WeatherStation.objects.all().order_by('station_name'),
                                      required=False,
                                      widget=autocomplete.ModelSelect2(url='weatherstation-autocomplete', forward=('period',), attrs={'style': 'width:410px'})
                                      )
    
    def __init__(self, *args, **kwargs):
        super(QueryRelation, self).__init__(*args, **kwargs)
        
    
class MeteoQuery(QueryRelation):
    
    location = forms.ModelChoiceField(queryset=Location.objects.all().order_by('short_name'),required=True)
    
    station1 = forms.ModelChoiceField(queryset=WeatherStation.objects.all().order_by('station_name'),required=True,
                                             widget=autocomplete.ModelSelect2(url='weatherstation-autocomplete', forward=('period','location'), attrs={'style': 'width:410px'})
                                             )
    station2 = forms.ModelChoiceField(queryset=WeatherStation.objects.all().order_by('station_name'),
                                      required=False,
                                      widget=autocomplete.ModelSelect2(url='weatherstation-autocomplete', forward=('period','location'), attrs={'style': 'width:410px'})
                                      )
    station3 = forms.ModelChoiceField(queryset=WeatherStation.objects.all().order_by('station_name'),
                                      required=False,
                                      widget=autocomplete.ModelSelect2(url='weatherstation-autocomplete', forward=('period','location'), attrs={'style': 'width:410px'})
                                      )

