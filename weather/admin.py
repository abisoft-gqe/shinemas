# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.contrib import admin

from django.utils.translation import ugettext as _

from weather.models import WeatherStation, WeatherServiceTokens

from import_export.admin import ImportExportModelAdmin
from import_export import resources, fields, widgets

class WeatherStationResource(resources.ModelResource):
    class Meta:
        model = WeatherStation
        exclude = ('id',)
        import_id_fields = ('station_number','source')
        
class WeatherStationAdmin(ImportExportModelAdmin): 
    list_display = ('station_name','city','source')
    list_filter = ('source','status')
    search_fields = ('station_name',)
    readonly_fields = ("source",)
    resource_class = WeatherStationResource

admin.site.register(WeatherStation, WeatherStationAdmin)
admin.site.register(WeatherServiceTokens)
