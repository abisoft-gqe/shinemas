# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext as _

class WeatherStation(models.Model):
    """
        The WeatherStation model give informations about the weather station used to obtain the weather data
        
    
        :var CharField station_name: The name used for identify the Station
        :var CharField station_number: The number used for identify the Station in the WebService 
        :var CharField city: Address of this Station
        :var CharField country: The country of this Station
        :var CharField longitude: The longitude of this Station, GPS coordinate are used for climatic data
        :var CharField latitude: The latitude of this Station, GPS coordinate are used for climatic data
        :var CharField altitude: The altitude of this Station, GPS coordinate are used for climatic data
        :var DateField start_date: The starting date of this Station
        :var DateField end_date: The ending date of this Station
        :var CharField status: If station is open or close
        :var CharField source: The web service to which this Station belongs
        :var DateField update_date: The updated date used to know when the web service has been updated
    """
    
    STATUS_CHOICES = (
                      ('open','open'),
                      ('close','close')
                      )
    
    station_name = models.CharField(_("Name"),max_length=100)
    station_number = models.CharField(_("Number"),max_length=100)
    city = models.CharField(_("City"),max_length=100,blank=True,null=True)
    country = models.CharField(_("Country"),max_length=100,blank=True,null=True)
    longitude = models.FloatField(_("Longitude"),max_length=50)
    latitude = models.FloatField(_("Latitude"),max_length=50)
    altitude = models.FloatField(_("Altitude"),max_length=50,blank=True,null=True)
    start_date = models.DateField(_("Start date"))
    end_date = models.DateField(_("End date"),blank=True,null=True)
    status = models.CharField(_("Status"), max_length=5, choices=STATUS_CHOICES)
    source = models.CharField(_("Source"), default="Custom", max_length=100)
    update_date = models.DateField(_("Update date"),blank=True,null=True)
    
    def __unicode__(self):
        return u"%s"%self.station_name
    def __str__(self):
        #return "%s"%self.station_name
        end = self.end_date        
        if end == None :
            end = "..."
        return u"{0} ( {1} - {2} )".format(self.station_name, self.start_date, end)
    
    class Meta:
        ordering = ['-status','-start_date']
    
    def save(self,*args, **kwargs):
        super(WeatherStation,self).save(*args, **kwargs)
        
     
class WeatherServiceTokens(models.Model):
    service = models.CharField(_("Service"), max_length=100)
    user = models.ForeignKey('auth.User', on_delete = models.CASCADE)
    information = models.TextField(_("Token information"))
    
    