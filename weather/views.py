# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""
import json
import datetime
import traceback

from django.shortcuts import render
from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.conf import settings

from eppdata.models import Variable, Rawdata, Method
from actors.models import Location
from weather.forms import UploadLocation, MeteoQuery
from weather import webservice
from weather.utils import open_csv
from weather.models import WeatherServiceTokens

@login_required
@user_passes_test(lambda u: u.is_superuser)
def update_station(request):
    """
    This view enable to update the stations of a web service.

    @param request: The request object send by Http
    @type request: HttpRequest object

    @return: A HttpResponse with corresponding template
    """
    if request.method == "GET":
        if settings.WEB_SERVICE == ():
            messages.error(request, 'You are not connected to a web service.')
            return HttpResponseRedirect(reverse('admin:weather_weatherstation_changelist'))
        else :
            try :
                WebClass = getattr(webservice,settings.WEB_SERVICE)
                ws = WebClass()
                #ws.authentication()
                (create, edit) = ws.update_station()
                mess = "The update was completed. " + str(create) + " stations were created, " + str(edit) + " stations have been modified."
                messages.success(request, mess)
                return HttpResponseRedirect(reverse('admin:weather_weatherstation_changelist'))
            except Exception as e:
                print(traceback.format_exc())
                messages.error(request, 'Server unavailable.')
                return HttpResponseRedirect(reverse('admin:weather_weatherstation_changelist'))

@login_required
@user_passes_test(lambda u: u.is_superuser)  
def update_variable(request):
    """
    This view enable to update the variable of a web service.

    @param request: The request object send by Http
    @type request: HttpRequest object

    @return: A HttpResponse with corresponding template
    """
    if request.method == "GET":
        if settings.WEB_SERVICE == ():
            messages.error(request, 'You are not connected to a web service.')
            return HttpResponseRedirect(reverse('admin:weather_weatherstation_changelist'))
        else :
            try :
                WebClass = getattr(webservice,settings.WEB_SERVICE)
                ws = WebClass()
                #ws.authentication()
                add = ws.update_variable()
                mess = "The update was completed. " + str(add) + " variables were created."
                messages.success(request, mess)
                return HttpResponseRedirect(reverse('admin:eppdata_variable_changelist'))
            except:
                messages.error(request, 'Server unavailable.')
                return HttpResponseRedirect(reverse('admin:eppdata_variable_changelist'))

@login_required
@user_passes_test(lambda u: u.is_superuser) 
def upload_location(request):  
    """
    This view enable to upload meteorologic data.
    """ 
    if request.method == "GET":
        form = UploadLocation()
        return render(request, 'weather/upload_location.html', {'form': form})
    if request.method == "POST" and request.FILES['file'] :
        form = UploadLocation()
        location = request.POST['station']
        file = request.FILES['file']
        file_meth = request.FILES['file_method']
        tag = 'success'
        mess = []
        
        #try :
            
            # read method file 
        csvfile =  open_csv(file_meth)
        
        var = {}
        var_meth = {}
        
        for line in csvfile :
            print(line)
            try :
                var[line['variable']] = Variable.objects.get(name=line['variable'],type='d')
            except:
                mes = line['variable'] + ' doesn\'t exist in Variable Weather Daily'
                if mes not in mess :
                    mess.append(mes)
                tag = "error"
            try :
                var_meth[line['variable']] = Method.objects.get(method_name=line['method_name'])
            except:
                mes = line['method_name'] + ' doesn\'t exist'
                if mes not in mess :
                    mess.append(mes)
                tag = "error"
        
        # read data file
        csvfile =  open_csv(file)    
        
        if tag != "error" :
            for line in csvfile :
                for v in var:
                    try:
                        rd =  Rawdata.objects.create(method=var_meth[v], variable=var[v],raw_data=line[v],date=line['DATE'])
                        rd.station.add(location)
                    except:
                        pass
            messages.success(request, 'Loaded data successfully')
        """except :
            tag = "error"
            messages.error(request, 'Wrong file format')"""
        
        for m in mess : 
            messages.error(request,m)
        
        return render(request, 'weather/upload_location.html', {'form': form,'tag':tag})
    return render(request, 'weather/upload_location.html', {'tag':tag})

def weather_oauth(request):
    
    WeatherClass = getattr(webservice, settings.WEB_SERVICE)
    n = WeatherClass()
    if 'code' in request.GET :
        try :
            r = n.get_token(request)
        except :
            messages.error(request, "Impossible to get token.")
        else :
            messages.success(request, "Authentication to {0} service succeed".format(n.SERVICE_NAME))
            url_redirect = request.session.get('meteo-redirect')
            del request.session['meteo-redirect']
            return HttpResponseRedirect(url_redirect)
    else :
        try :
            request.session['meteo-redirect'] = request.META.get('HTTP_REFERER')
            r = n.authentication(request.build_absolute_uri())
            return HttpResponseRedirect(r)
        except Exception as e:
            print("Exception {0}".format(e))
            messages.error(request, "Authentication failed.")
    
    return render(request, 'weather/weather_oauth.html')

@login_required
def weather_query(request):

    #print(settings.code_verifier)
    WebClass = getattr(webservice, settings.WEB_SERVICE)
    service = WebClass()
    if request.method == "GET" :
        form = MeteoQuery()
        return render(request, 'weather/weather_query.html', {'form':form})

  
        
    
    if request.method == "POST" :
        
        if service.METHODE == "OAUTH" :
            try :
                token_object = WeatherServiceTokens.objects.get(user=request.user, service = service.SERVICE_NAME)
                token_data = json.loads(token_object.information)
                if token_data["expires_at"] < datetime.datetime.now().timestamp()*1000 :
                    ##here we try get a new token
                    print("token expired")
                    token_information = service.refresh_token(token_data["refresh_token"])
                    token_object.information = token_information
                    token_object.save()
                    token_data = json.loads(token_information)
                    service.set_token(token_data["access_token"])
                else :
                    service.set_token(token_data["access_token"])
            except Exception as e:
                print(e)
                return HttpResponseRedirect(reverse('weatheroauth'))
        form = MeteoQuery(data=request.POST)
        if form.is_valid() :

            meteo_variables = request.POST.getlist('var')

            stations = []
            if request.POST['station1']: 
                stations.append(request.POST['station1'])
            if request.POST['station2']: 
                stations.append(request.POST['station2'])
            if request.POST['station3']: 
                stations.append(request.POST['station3'])
            
            location = Location.objects.get(id=int(request.POST['location']))
            
            meteo_data, variables, stations_obj = service.get_data(request.POST['start_date'], 
                             request.POST['end_date'],
                             request.POST['period'],
                             stations,
                             meteo_variables)
            

            return render(request, "weather/weather_query.html", {'form':form, 'data':meteo_data.to_html(classes=['alternate',]), 'variables': variables, 'stations':stations_obj, 'location':location})
        return render(request, "weather/weather_query.html", {'form':form})
