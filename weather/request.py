# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from geopy.distance import geodesic
from collections import OrderedDict

from weather.models import WeatherStation
from eppdata.models import Variable, Rawdata, Method
from weather.forms import UploadLocation, MeteoQuery
from weather.utils import convertDate
from weather import webservice


def meteorologic_request(ws,dsrt,date_srt,dend,date_end,period,vars,loc,station1,station2,station3):
    
    st = [] # stock id of web service station
    st_personal = [] # stock id of web service personal station
    station = [] # stock a list for each station with station info 
    pers = -1
    s = WeatherStation.objects.get(id=station1)
    # Where station 1 is a personal station
    if s.source == 'Custom':
        st_personal.append(s.station_number)
        pers = 1
    else :
        st.append(s.station_number)
    gps = (s.latitude,s.longitude)
    d = round(geodesic(loc,gps).kilometers)
    station.append([s.station_number,s.station_name,s.status,str(d)])
    try:
        sa = WeatherStation.objects.get(id=station2)
        # Where station 2 is a personal station
        if sa.source == 'Custom':
            st_personal.append(sa.station_number)
            pers = 0
        else :
            st.append(sa.station_number)
        gps = (sa.latitude,sa.longitude)
        d = round(geodesic(loc,gps).kilometers)
        station.append([sa.station_number,sa.station_name,sa.status,str(d)])
    except:
        pass
    try:
        sa = WeatherStation.objects.get(id=station3)
        # Where station 3 is a personal station
        if sa.source == 'Custom':
            st_personal.append(sa.station_number)
            pers = 0
        else :
            st.append(sa.station_number)
        gps = (sa.latitude,sa.longitude)
        d = round(geodesic(loc,gps).kilometers,2)
        station.append([sa.station_number,sa.station_name,sa.status,str(d)])
    except:
        pass
    vname = [] # List of variable names related to web services
    vname_pers = [] # List of names of personal varaibles
    vdescript = [] # List of name_descriptor for each variable
    vunit = [] # List of units for each variable
    ndescript = [] # List of name_descriptor for variables not found
    for var in vars :
        try :
            v = Variable.objects.get(id=var)
            if v.source != 'Custom':
                vname.append(v.name)
            else :
                vname_pers.append(v.name)
        except:
            pass
            
    # Searches first in the database and then with web services if requested
    if pers == 1 :
        (res,var_pers,var_null_pers,vpers_null) = webservice.get_data_personal_station(st_personal, vars, dsrt, dend)
        try :
            (data, var_res, var_null) = ws.get_data(date_srt,date_end,period,st,var_null_pers)
        except:
            data = None
            var_res = []
            var_null = vname
        var_null = var_null + vpers_null
    # Searches first with web services and then in the databases if requested
    else :
        try :
            (data, var_res, var_null) = ws.get_data(date_srt,date_end,period,st,vname)
        except:
            data = None
            var_res = []
            var_null = vname
        if pers == 0 :
            var_null = var_null + vname_pers
            v_list = []
            for v in var_null:
                variable = Variable.objects.get(name=v, type=period)
                v_list.append(variable.id)
            (res,var_pers,var_null,vpers_null) = webservice.get_data_personal_station(st_personal, v_list, dsrt, dend)
            var_null = var_null + vpers_null
        else :
            var_pers = []
            res = {}
            var_null = var_null + vname_pers
            
    for var in var_res :
        try :
            v = Variable.objects.filter(name=var[0], type=period)
            vdescript.append(v[0].name_descriptor)
            vunit.append(v[0].unit)
        except:
            pass
    for var in var_pers :
        try :
            v = Variable.objects.filter(name=var[0], type=period)
            vdescript.append(v[0].name_descriptor)
            vunit.append(v[0].unit)
        except:
            pass
    for var in var_null :
        try :
            v = Variable.objects.filter(name=var, type=period)
            ndescript.append(v[0].name_descriptor)
        except:
            pass
              
    # Sharing of information on the database and web services
    data_fin = []
    if data == None or data == [] :
        res = OrderedDict(sorted(res.items(), key=lambda t: t[0]))
        for da,val in res.items():
            tmp = [da]
            for c,v in val.items():
                tmp.append(v)
            data_fin.append(tmp)
    else : 
        for d in data:
            tmp = d.copy()
            for vtmp in var_pers :
                try :
                    tmp.append(res[d[0]][vtmp[0]])
                except:
                    tmp.append("None")
            data_fin.append(tmp)
    var_fin = var_res + var_pers
    return (var_fin,var_null,vdescript,vunit,ndescript,station,data_fin)
