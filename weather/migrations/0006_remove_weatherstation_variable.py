# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2017-04-11 16:25
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('weather', '0005_weatherstation_update_date'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='weatherstation',
            name='variable',
        ),
    ]
