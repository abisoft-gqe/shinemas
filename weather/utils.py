# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""
import datetime
from dateutil import parser
import io
import csv
from myuser.utils import recode

# convert a date in format datetime in string YYYY/MM/DD
def convertDate(date):
    try:
        d = str(date.day)
        if len(d) == 1 :
            d = "0" +  d
        m = str(date.month)
        if len(m) == 1 :
            m = "0" +  m
        new_date = str(date.year) + '-' + m + '-' + d
    except :
        new_date = ""
    return new_date

# Returns a list with 'days' (int) days between the 1st element and the 2nd, then the 3rd is the next day after the 2nd and there are 'days' days again
# Between the 3rd and 4th, ... and so on
def time_date(start,end,day):
    j1 = int(start[0:2])
    m1 = int(start[3:5])
    a1 = int(start[6:10])
    j2 = int(end[0:2])
    m2 = int(end[3:5])
    a2 = int(end[6:10])
    d0 = datetime.date(a1, m1, j1)
    d1 = datetime.date(a2, m2, j2)
    delta = d1 - d0
    days = delta.days
    date = []
    date.append(d0)
    
    while days>day :
        tmp = datetime.timedelta(days=day)
        d0 = d0 + tmp
        date.append(d0)
        tmp = datetime.timedelta(days=1)
        d0 = d0 + tmp
        date.append(d0)
        delta = d1 - d0
        days = delta.days
    
    date.append(d1)
    date_fin = []
    
    for d in date :
        j = str(d.day)
        if len(j)<2 :
            j = "0" + j
        m = str(d.month)
        if len(m)<2 :
            m = "0" + m
        da = j + '/' + m + '/' + str(d.year)
        date_fin.append(da)
    
    return date_fin


# Converts any date format to YYYY/MM/DD
def date_day(date):  
    new_date = parser.parse(date)
    date_day = new_date + datetime.timedelta(days=1)
    new = convertDate(date_day)
    return new
    
def open_csv(file):
    stream_init = file.read()
    stream = recode(stream_init) #we call recode to avoid any encoding error
    f = io.StringIO(stream)
    dialect = csv.Sniffer().sniff(f.read()) # doc sniffer : https://docs.python.org/3.4/library/csv.html#dialects-and-formatting-parameters
    f = io.StringIO(stream)
    csvfile = csv.DictReader(f, dialect = dialect)
    return csvfile