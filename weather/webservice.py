# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from abc import ABCMeta, abstractmethod
import datetime
import requests
import urllib3
import pandas

from django.utils.dateparse import parse_date
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

from weather.models import WeatherStation, WeatherServiceTokens
from eppdata.models import Variable
from weather.utils import convertDate


 
class Weather:
    __metaclass__ = ABCMeta
  
    METHODE = None
    @abstractmethod
    def authentication(self):
        """Authenticates to web service if necessary"""
        pass
    
    
    @abstractmethod
    def get_token(self):
        """Authenticates to web service if necessary"""
        pass
      
    @abstractmethod
    def update_station(self):
        """
        Updating weather stations from web services.
        Returns a list of two items:
        - the number of stations created
        - the number of stations modified
        """
        return
      
    @abstractmethod
    def update_variable(self):
        """
        Updating weather variables from web services.
        Returns the number of variables created.
        """
        return
      
    @abstractmethod
    def get_data(self,start_date,end_date,period,station,var):
        """
        Updating weather data from web services.
        Need :
        - data start date
        - end date of data
        - data interval
        - List of weather station (id)
        - list of weather variables
        Returns argument :
        - list contains List(date,value_var1, value_var2, ...)
        - List contains List(name_var,station_number) with the station_number of station where data is derived
        - List contains name of Variable Where there is no data in the results
        """
        return

class Climatik(Weather):
    
    key = None
    token = None
    
    URL = 'https://agroclim.inrae.fr/climatik/rs'
    SERVICE_NAME = 'Climatik'
    METHODE = 'OAUTH'
    MENTION = 'Données issues de la plateforme INRAE CLIMATIK'
    
    def authentication(self, redirect):
        """TODO : 
        - créer le code_verifier et code_challenge à la volée
        """
        print(redirect)
        requests.packages.urllib3.disable_warnings()
        requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += ':HIGH:!DH:!aNULL'
        try:
            requests.packages.urllib3.contrib.pyopenssl.util.ssl_.DEFAULT_CIPHERS += ':HIGH:!DH:!aNULL'
        except AttributeError:
            # no pyopenssl support used / needed / available
            pass
        
        #put serveur in variable
        url = self.URL+'/oauth/authorize?client_id={0}&code_challenge={1}&code_challenge_method=s256&redirect_uri={2}&response_type=code&scope=data:read&state=retour'.format(settings.CLIENT_ID, settings.CODE_CHALLENGE, redirect) 
        #url = 'https://intranet-preproduction.inra.fr/climatik_v2/rs/oauth/authorize?client_id={0}&code_challenge={1}&code_challenge_method=s256&redirect_uri=http://127.0.0.1:8000/weather/weatheroauth/&response_type=code&scope=data:read&state=retour'.format(settings.client_id, settings.code_challenge) 
        return url
    
    def set_token(self, token_str):
        self.token = token_str
    
    def get_token(self, request):  
    
        code = request.GET['code']
        params = {
            'client_id':settings.CLIENT_ID,
            'code_verifier':settings.CODE_VERIFIER,
            'code' : code,
            'grant_type' :'CODE'
        }
        
        response = requests.post(self.URL+'/oauth/token', data=params, verify=False)
        print(response.text)
        try :
            wstokens = WeatherServiceTokens.objects.get(service = self.SERVICE_NAME,
                                            user = request.user
                                            )
            wstokens.information = response.text
            wstokens.save()
        except ObjectDoesNotExist:
            WeatherServiceTokens.objects.create(
                                            service = self.SERVICE_NAME,
                                            user = request.user,
                                            information = response.text
                                            )
        return True
    
    def refresh_token(self, refresh_token):  
    
        params = {
            'client_id':settings.CLIENT_ID,
            'code' : refresh_token,
            'grant_type' :'refresh_token'
        }
        
        response = requests.post(self.URL+'/oauth/token', data=params, verify=False)
        print(response.text)
        return response.text
              
    def update_station(self):
          
        edit = 0
        create = 0
        """wsdlFile = 'https://intranet.inra.fr/climatik_v2/ws/xfire/AlternateWebService?wsdl'
        server = zeep.Client(wsdlFile)
        stations = server.service.getStations(self.key)"""
        
        response = requests.get(self.URL+"/stations")
        response.json()["data"]
        today = datetime.date.today()
        date_now = str(today.year) + "-" + str(today.month) + "-" + str(today.day)
          
        for station in response.json()["data"] :
            dat =  station['startDate']
            try :
                wb = WeatherStation.objects.get(station_number=station['id'],start_date=dat)
            except Exception:
                wb = None
                              
            # creation of station
            if wb == None: 
                create = create + 1
                name = station['city']+" - "+station['network']+" - Climatik"

                if station['endDate']== None :
                    wb =  WeatherStation.objects.create(station_name=name,station_number=station['id'],city=station['city'],
                                                        country='FRANCE',longitude=station['longitude'],latitude=station['latitude'],
                                                        altitude=station['altitude'],start_date=station['startDate'],
                                                        status='open',source="Climatik",update_date=date_now)
                else :
                    wb =  WeatherStation.objects.create(station_name=name,station_number=station['id'],city=station['city'],
                                                        country='FRANCE',longitude=station['longitude'],latitude=station['latitude'],
                                                        altitude=station['altitude'],start_date=station['startDate'],end_date=station['endDate'],
                                                        status='close',source="Climatik",update_date=date_now)
                              
            # station already exist in the databases  
            else :
                upd_date = convertDate(station['changeDate'])
                upd_date_old = str(wb.update_date)
                  
                if (station['endDate']!= None) and (wb.end_date == ""):
                    edit = edit + 1
                    wb.end_date = parse_date(station['endDate'])
                    wb.status = "close"
                    wb.save()
                                  
                # If there have been updates, we update all the elements that may have changed
                if (upd_date != "") and (upd_date > upd_date_old):
                    edit = edit + 1
                    wb.station_name = station['city'] + " - Climatik"
                    wb.city = station['city']
                    wb.longitude = station['longitude']
                    wb.latitude = station['latitude']
                    wb.altitude = station['altitude']
                    wb.save()
          
        new_date = parse_date(date_now)
        WeatherStation.objects.filter(source="Climatik").update(update_date=new_date)
          
        return (create,edit)
      
    def update_variable(self):
          
        var_add = 0
        """wsdlFile = 'https://intranet.inra.fr/climatik_v2/ws/xfire/AlternateWebService?wsdl'
        server = zeep.Client(wsdlFile)
        variables = server.service.getVariables(self.key)"""
        
        response = requests.get(self.URL+"/variables")
        
              
        for v in response.json()["data"]:
            bool = False
            if v['timescale'] == 'DAILY':
                t = 'd'
                bool = True
            elif v['timescale'] == 'HOURLY':
                t = 'h'
                bool = True
            if bool :
                try:
                    var = Variable.objects.get(name=v['abbreviation'],type=t)
                except:
                    var = None
                if var == None :
                    var_add = var_add + 1
                    var = Variable.objects.create(name=v['abbreviation'],name_descriptor=v['name'],type=t,unit=v['unit'],source="Climatik")
        return var_add
      
    def get_data(self, start_date, end_date, period, stations, meteo_vars):
        
        
        params = {'Authorization': 'Bearer ' + self.token}
        get_parameters = ""
        variables = []
        stations_obj = []
        
        if period == 'h' :
            get_parameters += 'timescale=HOURLY&'
        else :
            get_parameters += "timescale=DAILY&"
        for v in meteo_vars:
            var_object = Variable.objects.get(id=v)
            get_parameters += "variable={0}&".format(var_object.name)
            variables.append(var_object)
        get_parameters += "start={0}&".format(start_date)
        get_parameters += "end={0}".format(end_date)

        if period == 'd':
            sdate = datetime.datetime.strptime(start_date, '%Y-%m-%d')
            edate = datetime.datetime.strptime(end_date, '%Y-%m-%d')
            indexes = [sdate+datetime.timedelta(days=d) for d in range(0,(edate-sdate).days+1)]
        elif period == 'h' :
            sdate = datetime.datetime.strptime(start_date, '%Y-%m-%d')
            edate = datetime.datetime.strptime(end_date, '%Y-%m-%d')
            indexes = [sdate+datetime.timedelta(seconds=h*3600) for h in range(0,((edate-sdate).days+1)*24)]
        
        df = pandas.DataFrame(pandas.NA, index=indexes, columns=variables)
        var_from_station = dict(zip(variables, [None]*len(variables)))
        for s in stations:
            stations_obj.append(WeatherStation.objects.get(id=s))
            final_parameters = get_parameters+"&station={0}".format(int(stations_obj[-1].station_number))

        
            url = self.URL+'/data?{url_params}'.format(url_params=final_parameters)
            response = requests.get(url, headers=params)
            meteo_data = response.json()["data"]
        #meteo_variables = response.json()["variables"]
        #meteo_stations = response.json()["stations"]
            
            for data in meteo_data :
                data_date = datetime.datetime.strptime(data['date'],'%Y-%m-%dT%H:%M')
                for varname in variables :
                    #print(data_date, varname)
                    if df[varname][data_date] is pandas.NA and data['values'][variables.index(varname)] is not None :
                        if df[varname].isna().all() == True:
                            var_from_station[varname] = data['station']
                            df[varname][data_date] = data['values'][variables.index(varname)]
                        elif var_from_station[varname] == data['station']:
                            df[varname][data_date] = data['values'][variables.index(varname)]
        return (df, var_from_station, stations_obj)

