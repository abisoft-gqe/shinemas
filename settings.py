# -*- coding: utf-8 -*-
""" 
    LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (RÃ©seau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Eva Dechaux, Melanie Polart-Donat

    This file is part of SHiNeMaS
    
    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SHiNeMaS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

import os

from config import *

INSTALLED_APPS = (
    'actors.apps.ActorsConfig',
    'weather.apps.WeatherConfig',
    'entities.apps.EntitiesConfig',
    'network.apps.NetworkConfig',
    'eppdata.apps.EppdataConfig',
    'wsR.apps.WsrConfig',
    #'analysis.apps.AnalysisConfig',
    'common.apps.CommonConfig', 
    'myuser.apps.MyuserConfig',
    'images.apps.ImagesConfig',
    'csv_io.apps.CsvIoConfig',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'dal',
    'dal_select2',
    'dal_queryset_sequence',
    #optionnal moduls
    'debug_toolbar',
    #'django_extensions',
    #'django_file_form',
    #'django_file_form.ajaxuploader',
    #'django_bootstrap3_form',
    #'django_pony_forms',
    'rest_framework',
    'import_export',
    'rest_framework.authtoken',

)

DEFAULT_AUTO_FIELD='django.db.models.AutoField'
IMPORT_EXPORT_USE_TRANSACTIONS = True

DEBUG = True
#TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('', ''),
)

MANAGERS = ADMINS


SITE_ID = 1

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
#
#LANGUAGE_CODE = 'fr-fr'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
#ADMIN_MEDIA_PREFIX = '/media_admin/'

# List of callables that know how to import templates from various sources.
# TEMPLATE_LOADERS = (
#     #'django.template.loaders.filesystem.load_template_source',
#     #'django.template.loaders.app_directories.load_template_source',
# #     'django.template.loaders.eggs.load_template_source',
#     'django.template.loaders.filesystem.Loader',
#     'django.template.loaders.app_directories.Loader',
# )

DEFAULT_CHARSET = 'utf-8'

# MIDDLEWARE_CLASSES = (
#     'django.middleware.common.CommonMiddleware',
#     'django.contrib.sessions.middleware.SessionMiddleware',
#     'django.contrib.auth.middleware.AuthenticationMiddleware',
#     'django.contrib.messages.middleware.MessageMiddleware',
#     'debug_toolbar.middleware.DebugToolbarMiddleware',
#     'images.middleware.EmptyTempUploadFile'
# )

MIDDLEWARE = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',         
    'images.middleware.EmptyTempUploadFile'                                    
)


REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated', ),
}
AUTH_TOKEN_LIFE = 60 #days

ROOT_URLCONF = DJANGO_ROOT_SETTING+'urls'

LOGIN_URL = ROOT_URL+'/login/'
LOGIN_REDIRECT_URL = ROOT_URL+'/'

PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# LOCALE_PATHS = [
#     os.path.join(PROJECT_PATH, "locale"),
# ]

DATE_INPUT_FORMATS = ['%Y-%m-%d', '%d/%m/%Y']

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [PROJECT_PATH+"/templates",
                 PROJECT_PATH+"/templates/myuser/"],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.static',
                DJANGO_ROOT_SETTING+'common.processor.rooturl',
                DJANGO_ROOT_SETTING+'common.processor.current_language',
                DJANGO_ROOT_SETTING+'common.processor.global_search_fom'
            ],
            'debug':True
        },
    },
]


#Verify if it is used
MEDIA_ROOT_ANALYSIS = MEDIA_ROOT+"analysis/"
MEDIA_ROOT_TEMPLATES = MEDIA_ROOT+"templates/"



FILE_UPLOAD_HANDLERS = (
                        "django.core.files.uploadhandler.MemoryFileUploadHandler",
                        "django.core.files.uploadhandler.TemporaryFileUploadHandler",
                         )


STATIC_ROOT = os.path.join(PROJECT_PATH, "static")
STATIC_URL = ROOT_URL+'/static/'

MEDIA_URL = ROOT_URL+'/media/'

STATICFILES_DIRS = (
                    os.path.join(PROJECT_PATH, "staticfiles"),   
                    os.path.join(PROJECT_PATH, "_docs/"),
)

FILE_FORM_UPLOAD_DIR = MEDIA_ROOT

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

if DJANGO_ROOT_SETTING != '':
    SESSION_COOKIE_NAME = DJANGO_ROOT_SETTING[:-1]


PROFILE_LOG_BASE = "/var/log/shinemas/"

#LANGUAGE_CODE = 'en-us'
LANGUAGE_COOKIE_NAME = "shinemas_language"
# LANGUAGES = [
#     ('en', 'English'),
#     ('fr', 'French'),
# ]



INTERNAL_IPS = [
    '127.0.0.1',
]

TEMP_DIR = MEDIA_ROOT + "/temp_uploads/"
IMAGE_PATH = MEDIA_ROOT + "/images/"

