# Generated by Django 2.2 on 2022-04-01 11:18

from django.db import migrations, models

def convertquantity(apps, schema_editor):
    Seedlot = apps.get_model('entities','seedlot')
    bulk_seedlot = []
    for s in Seedlot.objects.all():
        try :
            s.initial_quantity = float(s.quantity_ini)
        except :
            pass
        bulk_seedlot.append(s)
    Seedlot.objects.bulk_update(bulk_seedlot, ['initial_quantity'])

class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0022_auto_20220401_1117'),
    ]

    operations = [
        migrations.RunPython(convertquantity)
    ]
