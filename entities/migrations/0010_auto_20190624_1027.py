# Generated by Django 2.0 on 2019-06-24 10:27

from django.db import migrations


class Migration(migrations.Migration):
    atomic=False
    dependencies = [
        ('eppdata', '0008_auto_20190624_1027'),
        ('actors', '0007_auto_20190128_1751'),
        ('network', '0009_auto_20190624_1027'),
        ('entities', '0009_auto_20190128_1751'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Germplasm_type',
            new_name='GermplasmType',
        ),
        migrations.RenameModel(
            old_name='Seed_lot',
            new_name='Seedlot',
        ),
        migrations.RenameModel(
            old_name='Update_quantity',
            new_name='UpdateQuantity',
        ),
    ]
