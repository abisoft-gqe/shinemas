# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-05-17 16:57
from __future__ import unicode_literals

from django.db import migrations, models

def populate_generations(apps, schema_editor):
    
    Seedlot = apps.get_model("entities", "seed_lot")
    first_seedlot = Seedlot.objects.filter(relations_as_child__isnull=True)
    
    for sl in first_seedlot:
        fill_generation(apps, schema_editor, sl)

def fill_generation(apps, schema_editor, father):
    Relation = apps.get_model("network", "Relation")
    qs = Relation.objects.filter(seed_lot_father=father)
    for r in qs :
        sl = r.seed_lot_son
        if r.diffusion_id != None :
            sl.generation = father.generation
            sl.confidence = father.confidence
            sl.onfarm_confidence = True
            sl.onfarm_generation = 0
            sl.save()
        elif r.mixture_id != None :
            sl.generation = 0
            sl.confidence = True
            sl.onfarm_confidence = True
            sl.onfarm_generation = 0
            sl.save()
        elif r.reproduction_id != None and r.is_male == "X" :
            sl.generation = father.generation+1
            sl.confidence = father.confidence
            sl.onfarm_generation = father.onfarm_generation+1
            sl.onfarm_confidence = father.onfarm_confidence
            sl.save()
        elif r.reproduction_id != None and r.selection_id != None :
            sl.generation = father.generation+1
            sl.confidence = father.confidence
            sl.onfarm_generation = father.onfarm_generation+1
            sl.onfarm_confidence = father.onfarm_confidence
            sl.save()
        elif r.reproduction_id != None and (r.is_male == "F" or r.is_male == "M") :
            sl.generation = 0
            sl.confidence = True
            sl.onfarm_generation = 0
            sl.onfarm_confidence = True
            sl.save()
        fill_generation(apps, schema_editor, r.seed_lot_son)

class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0002_auto_20160426_1047'),
    ]

    operations = [
        migrations.AddField(
            model_name='seed_lot',
            name='comments',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='seed_lot',
            name='confidence',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='seed_lot',
            name='generation',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='seed_lot',
            name='onfarm_confidence',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='seed_lot',
            name='onfarm_generation',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.RunPython(populate_generations)
    ]
