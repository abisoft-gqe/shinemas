# Generated by Django 2.0 on 2020-10-19 15:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0013_auto_20201012_0954'),
    ]

    operations = [
        migrations.CreateModel(
            name='AuditFileSeedLot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file_url', models.URLField()),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='Update date')),
            ],
        ),
    ]
