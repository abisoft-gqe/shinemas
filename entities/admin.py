# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""
from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from entities.models import Seedlot, Germplasm, GermplasmType, Species
from actors.models import Person

from import_export.admin import ImportExportModelAdmin
from import_export import resources, fields, widgets

"""
Define administration interface configuration
fieldsets enable form configuration design
list_display is the list of field to display in the whole table
search_fields is a list of field you can use to search data
list_filter is a list of fields you can use to filter data
"""

class GermplasmResource(resources.ModelResource):
    person = fields.Field(column_name='person', attribute='person', widget=widgets.ForeignKeyWidget(Person, field='short_name'))
    germplasm_type = fields.Field(column_name='germplasm_type', attribute='germplasm_type', widget=widgets.ForeignKeyWidget(GermplasmType, field='germplasm_type'))
    species = fields.Field(column_name='species', attribute='species', widget=widgets.ForeignKeyWidget(Species, field='species'))
    
    class Meta:
        model = Germplasm
        fields = ('person','idgermplasm','germplasm_type','species')
        import_id_fields = ('idgermplasm','species')

class SeedlotAdmin(admin.ModelAdmin): 
    list_display = ('name','location','date','germplasm')
    list_filter = ('germplasm', 'location', 'date')
    search_fields = ('name',)
    readonly_fields = ('name','germplasm','date','location') 
    fieldsets = (
                 (_('Seed Lot'),{'fields':['name','location','germplasm','date','quantity_ini','generation', 'confidence', 'onfarm_generation', 'onfarm_confidence']}),
                 )

    def has_add_permission(self,request):
        return False
    
class GermplasmAdmin(ImportExportModelAdmin):
    list_display = ('name', 'idgermplasm','germplasm_type','person','species')
    list_filter = ('germplasm_type',)
    search_fields = ('idgermplasm', 'name')
    actions = ['changetoCross','changetoMixture']
    resource_class = GermplasmResource
    
    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields + ('idgermplasm',)
        return self.readonly_fields
    
    def changetoCross(self, request, queryset):
        gptype = GermplasmType.objects.get(germplasm_type='Cross')
        queryset.update(germplasm_type=gptype)
    changetoCross.short_description = _('Change selected Germplasm to Cross')
    
    def changetoMixture(self, request, queryset):
        gptype = GermplasmType.objects.get(germplasm_type='Mixture')
        queryset.update(germplasm_type=gptype)
    changetoMixture.short_description = _('Change selected Germplasm to Mixture')
    
class GermplasmTypeAdmin(admin.ModelAdmin):
    list_display = ('germplasm_type',)

admin.site.register(Seedlot, SeedlotAdmin)
admin.site.register(Germplasm, GermplasmAdmin)
admin.site.register(GermplasmType, GermplasmTypeAdmin)
admin.site.register(Species)

