
# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""



### Fonctionnalite lier une date a une mesure ###
#ces filtres permettent de faire les requetes sur l'interface advanced_query 


from django import template
from eppdata.models import Rawdata, Variable, Method
from entities.models import Seedlot
from django.template.defaultfilters import stringfilter
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings

register = template.Library()
    

@register.filter
def add_var(variable, relation):
    try:
        result = Rawdata.objects.get(relation=relation,variable=variable,individual=None)
    except ValueError:
        result = ' '
        return result
    except ObjectDoesNotExist:
        result = ' '
        return result
    else:
        return result


@register.filter
def add_var_ind(_name_ind, _rel):
    _name, _ind = _name_ind
    result = ' '
    try:
        result = Rawdata.objects.get(individual=_ind,relation=_rel,variable=Variable.objects.get(name=_name))
    except ValueError:
        result = ' '
    except ObjectDoesNotExist:
        result = ' ' 
    return result
   

# filtre permettant d'ajouter un 2eme parametre a un filtre quelconque
@register.filter
def one_more(_var1, _var2):
    return _var1, _var2


@register.filter
def add_sl_ind(_namevar_2, _namesl):
    _namevar, _ind = _namevar_2
    try:
        result = Rawdata.objects.get(seed_lot=Seedlot.objects.get(name=_namesl),variable=Variable.objects.get(name=_namevar),individual=_ind)
    except ObjectDoesNotExist:
        result = ' '
    return result

@register.filter
def abs(_number):
    if _number < 0 :
        result = -1 * _number
    else :
        result = _number
    return result

@register.filter    
def add_weather(_boo):
    return settings.WEATHER

@register.filter
def index(List, i):
    return str(List[int(i)])

@register.simple_tag
def index2(List,i,j):
    return str(List[int(i)][int(j)])
