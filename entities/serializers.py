from rest_framework import serializers
from .models import Germplasm, GermplasmType, Species

class GermplasmSerializer(serializers.ModelSerializer):
    germplasm_type = serializers.CharField(source='germplasm_type.germplasm_type',allow_null=True)
    species = serializers.CharField(source='species.species', allow_null=True)
    
    class Meta:
        model = Germplasm
        fields = ['idgermplasm','germplasm_type','species']
        
class GermplasmTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = GermplasmType
        fields = '__all__'

class SpeciesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Species
        fields = '__all__'