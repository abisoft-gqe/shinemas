# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""
import datetime

from django import forms
from django.utils.translation import gettext_lazy as _
from dal import autocomplete

from entities.models import Germplasm, Species, Seedlot, GermplasmType
from actors.models import Location, Project, Person
from eppdata.models import Variable
from django.conf import settings
from geopy import location
from _datetime import date



CLASSIC = 'classic'
GENERATION = 'generation'
RELATION = 'relation'
MEASURES = 'measures'

RADIO_REQUESTS = ((CLASSIC,'Classic'),
                  (GENERATION,'Generations'),
                  #(RELATION,'Relations'),
                  (MEASURES,'Measures'))

RELATIONS = ((0,'-------'),
             (1,_('Reproduction')),
             (2,_('Diffusion')),
             (3,_('Mixture')),
             (4,_('Selection')),
             (5,_('Cross')))

def get_dates():
    # date de creation
    values = Seedlot.objects.order_by('date').values('date').distinct()
    tuple_vals = [("",'---------')]
    for v in values:
        if v["date"]!=None:
            tuple_vals.append((v['date'],v['date']))
    
    return tuple_vals

def catalogue_getdates():
    # date de creation
    y = datetime.date.today().year
    date_list = ((str(y+x), y+x) for x in range(-4,15))
    return date_list

class HorizontalRadioSelect(forms.RadioSelect):
    template_name = 'horizontal_select.html'

class SimpleRequest(forms.Form):
    year = forms.ChoiceField(label=_("Creation year"),required=False,choices=get_dates)
    location = forms.ModelChoiceField(label=_("Location"),queryset=Location.objects.all().order_by('short_name'),required=False)
    projects = forms.ModelChoiceField(label=_("Projects"),queryset=Project.objects.all().order_by('project_name'),required=False)
    species = forms.ModelChoiceField(label=_("Species"),queryset=Species.objects.all().order_by('species'),required=False)

class ClassicRequest(forms.Form):
    
    year = forms.ChoiceField(label=_("Creation year"),required=False,choices=get_dates)
    germplasm = forms.ModelChoiceField(label=_("Germplasm"),queryset=Germplasm.objects.all().order_by('idgermplasm','species'),required=False)
    germplasm_exclude = forms.BooleanField(label=_("Not"), widget=forms.CheckboxInput(), required=False)
    
    # affiche le nom du germplasm et son espece dans la liste deroulante (attention aux accents...)
    def __init__(self, *args, **kwargs):
        super(ClassicRequest, self).__init__(*args, **kwargs)
        # without the next line label_from_instance does NOT work
        self.fields['germplasm'].queryset = Germplasm.objects.select_related('species').all()
        self.fields['germplasm'].label_from_instance = lambda obj: "%s (%s)" % (obj.idgermplasm, obj.species)   

    location = forms.ModelChoiceField(label=_("Location"),queryset=Location.objects.all().order_by('short_name'),required=False)
    location_exclude = forms.BooleanField(label=_("Not"), widget=forms.CheckboxInput(), required=False)
    projects = forms.ModelChoiceField(label=_("Projects"),queryset=Project.objects.all().order_by('project_name'),required=False)
    projects_exclude = forms.BooleanField(label=_("Not"), widget=forms.CheckboxInput(), required=False)
    relation = forms.ChoiceField(label=_("Relation type"),choices=RELATIONS,required=False)
    relation_exclude = forms.BooleanField(label=_("Not"), widget=forms.CheckboxInput(), required=False)
    
    images = forms.BooleanField(label=_("Only seed lots with images"), widget=forms.CheckboxInput(), required=False)
    
    start_date = forms.CharField(label=_("Start date"), widget=forms.DateInput(attrs = {'class':'datepicker'}),required=False)
    end_date = forms.CharField(label=_("End date"), widget=forms.DateInput(attrs = {'class':'datepicker'}),required=False)
    variables = forms.ModelMultipleChoiceField(label=_("Variables"),
                                       queryset=Variable.objects.all().order_by('name'),
                                       required=False,
                                       widget=autocomplete.ModelSelect2Multiple(url='variable-autocomplete', attrs={'style': 'width:300px'}))
    mode = forms.ChoiceField(widget=HorizontalRadioSelect(),choices = RADIO_REQUESTS, required=True)
    no_date = forms.BooleanField(label=_("Include data without date"), widget=forms.CheckboxInput(), required=False)
    
    def clean(self):
        cleaned_data = super(ClassicRequest, self).clean()
        good_start_date = False
        good_end_date = False
        
        if cleaned_data["start_date"] != "" :
            try :
                datetime.datetime.strptime(cleaned_data["start_date"], '%Y-%m-%d')
                good_start_date = True
            except :
                self.add_error('start_date', _("The start date is not in a good format ('YYYY-mm-dd') : {0}".format(cleaned_data["start_date"])))
        
        if cleaned_data["end_date"] != "" :
            try :
                datetime.datetime.strptime(cleaned_data["end_date"], '%Y-%m-%d')
                good_end_date = True
            except :
                self.add_error('end_date', _("The end date is not in a good format ('YYYY-mm-dd') : {0}".format(cleaned_data["end_date"])))
        
        if good_start_date and good_end_date and datetime.datetime.strptime(cleaned_data["start_date"], '%Y-%m-%d') > datetime.datetime.strptime(cleaned_data["end_date"], '%Y-%m-%d') :
            self.add_error('start_date', _("The start date must be earlier than the end date"))
        
class SubmitSpecies(forms.Form): 
    species = forms.ModelChoiceField(label=_("Species"),queryset=Species.objects.all().order_by('species'),required=False)
    
class CatalogueForm(forms.Form): 
    catYear = forms.ChoiceField(initial= datetime.date.today().year,label=_("publication_year"),required=False,choices=catalogue_getdates)
     
      
class CatRequest(forms.Form):
    germplasm = forms.ModelChoiceField(initial= "All", label=_("Germplasm"),queryset=Germplasm.objects.filter(person = None).order_by('idgermplasm'),required=False)
    variables = forms.ModelChoiceField(label=_("Variables"),queryset=Variable.objects.filter(type = 'T12').order_by('name'),required=False)

class NewQuantityForm(forms.Form):
    newquantity = forms.FloatField(label= _("New quantity"), required=True)
    comment = forms.CharField(label=_("Comment"), required=False)
        
class NewSeedlotForm(forms.Form):
    germplasm = forms.ModelChoiceField(
        label=_("Germplasm name"),
        queryset=Germplasm.objects.all(),
        widget=autocomplete.ModelSelect2(url='germplasm-autocomplete', attrs={'style': 'width:100px'}),
        required=False
    )
    location = forms.ModelChoiceField(label=_("Location"), required=False,
                            queryset=Location.objects.all().order_by('short_name'),
                            widget=autocomplete.ModelSelect2(url='location-autocomplete', attrs={'style': 'width:100px'}),
                                      )

    date = forms.CharField(label=_("Year"),max_length=5, required=False)
    quantity_ini = forms.FloatField(label=_("Initial quantity"), required=False)

class GermplasmForm(forms.ModelForm):
    class Meta:
        model = Germplasm
        fields = ['name', 'idgermplasm', 'germplasm_type', 'person', 'species']
        widgets = { 'name': forms.TextInput(attrs={'size': 50}),
                   }
    
    germplasm_type = forms.ModelChoiceField(queryset=GermplasmType.objects.all(),
                                           widget=autocomplete.ModelSelect2(url='germplasmtype-autocomplete', attrs={'style': 'width:100px'}),
                                           required=False)
    species = forms.ModelChoiceField(queryset=Species.objects.all(),
                                           widget=autocomplete.ModelSelect2(url='species-autocomplete', attrs={'style': 'width:100px'}),
                                           required=False)
    person = forms.ModelChoiceField(queryset=Person.objects.all(),
                                           widget=autocomplete.ModelSelect2(url='person-autocomplete', attrs={'style': 'width:100px'}),
                                           required=False)
        
class GermplasmUpdateForm(GermplasmForm):
    
    idgermplasm = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly','size': 50}))
    
    
class GermplasmCreateForm(GermplasmForm):
    
    idgermplasm = forms.CharField(widget=forms.TextInput(attrs={'size': 50}), required=False)

class GermplasmSearchForm(forms.Form):
    germplasm = forms.ModelChoiceField(queryset=Germplasm.objects.all(), 
                                               widget=autocomplete.ModelSelect2(),
                                               required=False)


