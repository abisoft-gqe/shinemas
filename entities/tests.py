# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.test import TestCase
from django.apps import apps

from actors.models import Location
from entities.models import Seedlot, Germplasm, GermplasmType, Species, UpdateQuantity
from entities import managers

"""query ,
    updatequantity 200 post and get mais est ce que c'est bon
    path('uploaded_data_bis/', submit_germplasm_type, name='submit_gp_type'),  
    path('getgermplasmlist/', getgermplasmlist, name='getgermplasmlist'),
    sspath('catalogue_query/', catalogue_query, name='catalogue_query')
    """

class TestSeedLot(TestCase):
    def setUp(self):
        User = apps.get_model('auth','user')
        Location = apps.get_model('actors','Location')
        Species = apps.get_model('entities','Species')
        Germplasm = apps.get_model('entities','Germplasm')
        Seedlot = apps.get_model('entities','Seedlot')
        
        user = User.objects.create_superuser("admin", "admin", "admin")
        #self.c = Client()
        self.client.post('/login/', {'username': 'admin', 'password':'admin'})
        
        l =Location.objects.create(location_name='PEM')
        l.save()
        #sLocation.objects.create(location_name='JOD2').save()
        
        s = Species.objects.create(species='nesrine')
        s.save()
        
        
        
        
        g =Germplasm.objects.create(idgermplasm='Yellow',species = s)
        g.save()
        
        
        g =Germplasm.objects.create(idgermplasm='Red',species = s)
        g.save()
        
        
        
        
        dict_seedlot={}
        dict_seedlot["germplasm"]= "1"
        dict_seedlot["species"]= "nesrine"
        dict_seedlot["location" ]="1"
        dict_seedlot["year"]="2020"
        dict_seedlot["quantity_ini"]="1000"
        
        
        s=Seedlot.objects.create_new_seed(dict_seedlot,"form")
        s.save()
        #Seedlot.objects.create(name='seed1',location= l , germplasm = g).save()

        #Germplasm.objects.create(idgermplasm='Blue').save()

    def testCreateNewSeedLotSucces(self):
        
        
        dict_seedlot={}
        dict_seedlot["germplasm"]= "1"
        dict_seedlot["species"]= "nesrine"
        dict_seedlot["location" ]="1"
        dict_seedlot["year"]="2020"
        dict_seedlot["quantity_ini"]="1000"
        
        
        s=Seedlot.objects.create_new_seed(dict_seedlot,"form")
        #print(s)
        self.assertIsInstance(s, Seedlot)
        
    """def testCreateNewSeedLotSucces(self):    SPECIES"""
    def testCreateNewSeedLotError(self):        
        
       
        dict_seedlot2={}
        dict_seedlot2["germplasm"]= "ggg"
        dict_seedlot2["location" ]="ms"
        dict_seedlot2["year"]="2020"
        dict_seedlot2["quantity_ini"]="1000"
       
        with self.assertRaisesMessage(Exception, 'germplasm and species didn\'t match') and self.assertRaisesMessage(Exception, 'germplasm and species didn\'t match') :
            Seedlot.objects.create_new_seed(dict_seedlot2,"form")

    def test_query(self):
        r =self.client.post('/entities/query',{'year': '2020', 'mode':'CLASSIC'})
        self.assertEqual(r.status_code,301)#301python manage.py  test entities.tests.TestSeedLot.test_upload_new_Seedlot



    def test_seedlot_profil(self):
        
        s = Seedlot.objects.get(name = 'Yellow__2020_0001')
        '''print(Seedlot.objects.all().values())
        print(s.id)'''
        r =self.client.get('/entities/seedlot/{0}/'.format(s.id))
        self.assertEqual(r.status_code,200)#302
        
    def test_Germplasm_profil(self):
        
        s = Germplasm.objects.get(idgermplasm = 'Yellow')
        #print(Germplasm.objects.all().values())
        print(s.id)
        r =self.client.get('/entities/germplasm/{0}/'.format(s.id))
        self.assertEqual(r.status_code,200)#302
    
    def test_Species_profil(self):
        
        s = Species.objects.get(species = 'nesrine')
        r =self.client.get('/entities/species/{0}/'.format(s.id))
        self.assertEqual(r.status_code,200)#301




        
    def test_uploadOneSeedLot(self):
    
        dict_seedlot={}
        dict_seedlot["germplasm"]= "1"
        dict_seedlot["species"]= "nesrine"
        dict_seedlot["location" ]="1"
        dict_seedlot["date"]="2020"
        dict_seedlot["quantity_ini"]="1000"
        r =self.client.post('/entities/seedlot/create/',dict_seedlot)
        print(Seedlot.objects.all().values())
        self.assertEqual(Seedlot.objects.count(),2)#302
    
    def test_update_quantity(self):
        
        s = Seedlot.objects.get(name = 'Yellow__2020_0001')
        '''print(Seedlot.objects.all().values())
        print(s.id)'''
        r =self.client.get('/entities/updatequantity/{0}/'.format(s.id))
        self.assertEqual(r.status_code,200)
        
        
        d ={'newquantity':'1','comment':'gggg'}
        r =self.client.post('/entities/updatequantity/1/',data =d)
        self.assertEqual(r.status_code,200)
        
        
        
    def test_uploaded_data_bis(self):
        
        d={'germplasm_type_Yellow_nesrine':'germplasm_type_1_nesrine'}
        #print("sssss",d['key'].startsWith('g'))
        r =self.client.post('/entities/uploaded_data_bis/',d)
        self.assertEqual(r.status_code,200)
        
        
        
