# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.apps import apps
from django.db import models, IntegrityError
from django.db.models import Q
from django.core.exceptions import ValidationError
from django.db import transaction

""" Managers """
class SeedlotManager(models.Manager):
    """
        Manager for the ModelClass 
        :py:class:`Seedlot`
    """
    def get_or_create_seedlot(self, seedlotname, quantity_ini, accept_gp_creation, species):
        """
            According to the *seedlotname* object, if seedlotname is a string this method do :\n
            1. A query in database if len = 4\n
            2. Create a new Seed lot if len == 3\n
            3. Else return None\n
            \n
            If *seedlotname* is a Seedlot object then it return itself
            
            :param seedlotname: The seedlotname can be a string or a Seedlot object
            :type seedlotname: str or Seedlot
            :param int quantity_ini: The initial quantity for this seed lot
            :return: A seed lot (created or not)
            :rtype: Seedlot
        """
        Seedlot = apps.get_model('entities','seedlot')
        
        if type(seedlotname) is str :
            if len(seedlotname.split('_')) == 4 : 
                try:
                    seed_lot = Seedlot.objects.get(name=seedlotname)
                except Seedlot.DoesNotExist:
                    seed_lot = None
            elif len(seedlotname.split('_')) == 3 :
                #si le nom du lot de graine contient 3 élément et n'a pas déjà été créé alors on le créé
                seed_lot = self.create_seed_lot(seedlotname, quantity_ini, accept_gp_creation, species)
            else :
                #sinon il y a un problème
                seed_lot = None
        elif type(seedlotname) is Seedlot:
            seed_lot = seedlotname
        return seed_lot
    
    def create_seed_lot(self, seedlotname, quantity_ini, accept_gp_creation, species):
        """
            this method create a new
            :py:class:`Seedlot`
            according to the string given in params of type *germplasm_person_year*\n
            This method calls the 
            :py:meth:`Seedlot.build_seedlot_name`
            method and give the real name for this seed lot
            
            :param str seedlotname: The string containing information to create the seed lot
            :param int quantity_ini: The initial quantity for this seed lot
            :return: A new Seedlot object
            :rtype: Seedlot
        """
        Location = apps.get_model('actors', 'location')
        Germplasm = apps.get_model('entities', 'germplasm')
        Seedlot = apps.get_model('entities', 'seedlot')
        father_location = Location.objects.get_location_from_seedlotname(seedlotname)
        if accept_gp_creation == 2 :
            germplasm_object = Germplasm.objects.create_germplasm_from_seedlotname(seedlotname, None, species)
        elif accept_gp_creation == 0 :
            print(seedlotname)
            try :
                germplasm_object = Germplasm.objects.get_from_seedlotname(seedlotname)
            except :
                raise ValidationError(_("You're not allowed to create a new germplasm by this way, please create a new one before"))
        date = Seedlot.get_date_from_name(seedlotname)
        
        build_name = Seedlot.build_seedlot_name(seedlotname)
        print(build_name)
        seed_lot_obj= Seedlot.objects.create(name=build_name,
                                              location=father_location,
                                              germplasm=germplasm_object,
                                              quantity_ini=quantity_ini,
                                              date = int(date))

        return seed_lot_obj
    
    
    
    from django.db.models import Q

    def create_new_seed(self, data, mode):
        """
          Function used to create seedlot from a file uploaded by the user.
          :param dict file_dict: file on dict format returned by the CSVFactory
        """
        Location = apps.get_model('actors', 'location')
        Germplasm = apps.get_model('entities', 'germplasm')
        Seedlot = apps.get_model('entities', 'seedlot')

        
        if mode=="form":
            
            try:
                loc=Location.objects.get(id=data["location"]  )
            except Exception as e:
                raise Exception("Location dosen't exists")  
            else :
                try :
                    germ=Germplasm.objects.get(id=data["germplasm"])
                except Exception as e:
                    raise Exception("germplasm and species didn't match")

        else :
            
            try :
                loc=Location.objects.get(short_name=data["location"] ) 
            except Exception as e:
                raise Exception("Location {0} dosen't exists".format(data["location"]))
            else :
                try :
                    germ=Germplasm.objects.get(idgermplasm=data["germplasm"],species__species=data['species'])
                except Exception as e:
                    raise Exception("germplasm and species didn't match")
    
   
               
        name=str(germ.idgermplasm)+"_"+str(loc.short_name)+"_"+data["year"]
        full_name=Seedlot.build_seedlot_name(name)
        try :
            quantity = float(data["quantity_ini"].replace(',','.'))
        except Exception as e:
            quantity = None
        
        seedlot=Seedlot(name=full_name,
                        germplasm=germ,
                        location=loc,
                        date=data["year"],
                        quantity_ini=quantity
                        )                            
        return seedlot

    def advanced_query(self, filters, excludes):
        query = self.__filter_projects__(filters["projects"], excludes["projects_exclude"])
        query = self.__filter_location__(query, filters["location"], excludes["location_exclude"])
        query = self.__filter_year__(query, filters["year"], excludes["year_exclude"])
        query = self.__filter_germplasm__(query, filters["germplasm"], excludes["germplasm_exclude"])
        query = self.__filter_relation__(query, filters["relationtype"], excludes["relationtype_exclude"])
        query.select_related('germplasm', 'person').prefetch_related('relations_as_parent', 'relations_as_child')
        return query.distinct()
    
    def __filter_projects__(self, projects, exclude):
        if projects and not exclude:
            query = self.filter(Q(relations_as_parent__project=projects)|Q(relations_as_child__project=projects))
        elif projects and exclude :
            query = self.exclude(Q(relations_as_parent__project=projects)|Q(relations_as_child__project=projects))
        else :
            query = self.all()
        return query
    
    def __filter_location__(self, query, location, exclude):
        Location = apps.get_model("actors","location")
        if location and not exclude :
            query = query.filter(location=Location.objects.get(id=int(location)))
        elif location and exclude :
            query = query.exclude(location=Location.objects.get(id=int(location)))
        return query
    
    def __filter_year__(self, query, year, exclude):
        if year and not exclude :
            query = query.filter(date=year)
        elif year and exclude :
            query = query.exclude(date=year)
        return query
    
    def __filter_germplasm__(self, query, germplasm, exclude):
        Germplasm = apps.get_model("entities","germplasm")
        if germplasm and not exclude:
            query = query.filter(germplasm=Germplasm.objects.get(id=int(germplasm)))
        elif germplasm and exclude:
            query = query.exclude(germplasm=Germplasm.objects.get(id=int(germplasm)))
        return query
    
    def __filter_relation__(self, query, relationtype, exclude):
        Relation = apps.get_model("network", "relation")
        if relationtype and int(relationtype) > 0 and relationtype == 1 and not exclude :
            query = self.filter(id__in=Relation.objects.filter(seed_lot_son__in=query,reproduction__isnull=False,selection__isnull=True, is_male = 'X').values_list('seed_lot_son',flat=True)).order_by('name')
        elif relationtype and int(relationtype) > 0 and relationtype == 2 and not exclude :
            query = self.filter(id__in=Relation.objects.filter(seed_lot_son__in=query,diffusion__isnull=False).values_list('seed_lot_son',flat=True)).order_by('name')
        elif relationtype and int(relationtype) > 0 and relationtype == 3 and not exclude :
            query = self.filter(id__in=Relation.objects.filter(seed_lot_son__in=query,mixture__isnull=False).values_list('seed_lot_son',flat=True)).order_by('name')
        elif relationtype and int(relationtype) > 0 and relationtype == 4 and not exclude :
            query = self.filter(id__in=Relation.objects.filter(seed_lot_son__in=query,selection__isnull=False).values_list('seed_lot_son',flat=True)).order_by('name')
        elif relationtype and int(relationtype) > 0 and relationtype == 5 and not exclude :
            query = self.filter(id__in=Relation.objects.filter(seed_lot_son__in=query,reproduction__isnull=False,selection__isnull=True).filter( Q(is_male = 'M')|Q(is_male='F')).values_list('seed_lot_son',flat=True)).order_by('name')
        elif relationtype and int(relationtype) > 0 and relationtype == 1 and exclude :
            query = self.exclude(id__in=Relation.objects.filter(seed_lot_son__in=query,reproduction__isnull=False,selection__isnull=True).values_list('seed_lot_son',flat=True)).order_by('name')
        elif relationtype and int(relationtype) > 0 and relationtype == 2 and exclude :
            query = self.exclude(id__in=Relation.objects.filter(seed_lot_son__in=query,diffusion__isnull=False).values_list('seed_lot_son',flat=True)).order_by('name')
        elif relationtype and int(relationtype) > 0 and relationtype == 3 and exclude :
            query = self.exclude(id__in=Relation.objects.filter(seed_lot_son__in=query,mixture__isnull=False).values_list('seed_lot_son',flat=True)).order_by('name')
        elif relationtype and int(relationtype) > 0 and relationtype == 4 and exclude :
            query = self.exclude(id__in=Relation.objects.filter(seed_lot_son__in=query,selection__isnull=False).values_list('seed_lot_son',flat=True)).order_by('name')
        elif relationtype and int(relationtype) > 0 and relationtype == 5 and exclude :
            query = self.exclude(id__in=Relation.objects.filter(seed_lot_son__in=query,reproduction__isnull=False,selection__isnull=True).filter( Q(is_male = 'M')|Q(is_male='F')).values_list('seed_lot_son',flat=True)).order_by('name')
            
        return query

    def simple_query(self, location, project, date, species):
        query = self.all()
        if location :
            query = query.filter(location=location)
        if project :
            query = query.filter(Q(relations_as_child__project = project)|Q(relations_as_parent__project = project))
        if date :
            query = query.filter(date=date)
        if species:
            query = query.filter(germplasm__species = species)
        query = query.distinct()
        return query
    

class GermplasmManager(models.Manager):
    """
        Manager for the ModelClass 
        :py:class:`Germplasm`
    """
    
    def create_germplasm(self, data, mode):
        #mode is used to check if germplasm is created through a file or a form
        Species = apps.get_model('entities','species')
        Person = apps.get_model('actors','person')
        GermplasmType = apps.get_model('entities','germplasmtype')
        Germplasm = apps.get_model('entities','germplasm')
        
        if data["species"] != "" :
            try :
                data['species'] = Species.objects.get(species=data['species'])
            except Exception as e:
                raise Exception("The species with name {0} doesn't exists".format(data["species"]))
        else :
            raise Exception("The species column can't be empty")
        
        if data["person"] != "" :
            try :
                data["person"] = Person.objects.get(short_name = data["person"])
            except Exception as e:
                raise Exception("The person with short name {0} doesn't exists".format(data["person"]))
        else :
            data["person"] = None
            
        if data["type"] != "" :
            try :
                data["type"] = GermplasmType.objects.get(germplasm_type = data["type"])
            except Exception as e:
                raise Exception("The germplasm type with name {0} doesn't exists".format(data["type"]))
        else :
            data["type"] = None
            
        if data["name"] == "" :
            raise Exception("The name column can't be empty")
        
        if data["idgermplasm"] == "" :
            data["idgermplasm"] = data["name"].replace(' ','-').replace('\'', '-')
        
        try :
            Germplasm.objects.get(name = data["name"], species = data["species"])
        except Exception as e:
            pass
        else : 
            raise Exception("The germplasm with name {0} already exists for {1}".format(data["name"], data["species"]))
        
        try :
            Germplasm.objects.get(idgermplasm = data["idgermplasm"], species = data["species"])
        except Exception as e:
            pass
        else : 
            raise Exception("The germplasm with idgermplasm {0} already exists for {1}".format(data["idgermplasm"], data["species"]))
        
        return Germplasm(name = data["name"],
                         idgermplasm = data["idgermplasm"],
                         germplasm_type = data["type"],
                         person = data["person"],
                         species = data["species"]
                        )
    
    """ This method should be deprecated, it must become impossible to create a new germplasm from seedlot name"""
    def create_germplasm_from_seedlotname(self, seedlotname, person_obj, species):
        Germplasm = apps.get_model('entities','germplasm')

        germplasm_full = seedlotname.split('_')[0]
        germplasm = germplasm_full.split('#')[0]
        if not Germplasm.is_valid_idgermplasm(germplasm) :
            raise NameError(_("The germplasm name %s is not a valid name (should not contain space ( ), diez (#), coma (,), semicolon (;) or underscore (_))")%germplasm)
        
        try :
            germplasm_object = Germplasm.objects.create(idgermplasm = germplasm,
                                                    person = person_obj,
                                                    species = species)
        except IntegrityError:
            raise ValidationError(_("The germplasm {0} already exists for species {1}").format(germplasm, species.species))
        
        return germplasm_object
    
    def get_from_seedlotname(self, seedlotname) :
        Germplasm = apps.get_model('entities','germplasm')
        
        germplasm_full = seedlotname.split('_')[0]
        germplasm = germplasm_full.split('#')[0]
        return Germplasm.objects.get(idgermplasm=germplasm)
