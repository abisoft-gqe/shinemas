# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""
from __future__ import unicode_literals
import re

from django.db import models
from django.conf.urls.static import static
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.apps import apps
from django.utils import timezone
from django.contrib.contenttypes.fields import GenericRelation

from entities.utils import get_origindiffusion  
from entities.managers import GermplasmManager, SeedlotManager
from actors.exceptions import StorageException

"""
    Entites application contains models related to Genetic content
    A seed lot is linked to a germplasm which is linked himself to a species
"""
            
""" Models """
class Species(models.Model):
    species = models.CharField(max_length=100,unique=True) 
    latin_name = models.CharField(max_length=100,blank=True,null=True)
    description = models.TextField(max_length=200,blank=True,null=True) 
    image = models.ImageField(upload_to='images', blank=True, null=True)
    
    class Meta :
        ordering = ('species',) 
        verbose_name_plural='species'
        
    def __unicode__(self):
        return u"%s"%self.species
    
    def __str__(self):
        return self.species
    
    @staticmethod
    def is_valid_species_name(species_name):
        char_list=['#',' ','_',',',';']
        for char in char_list:
            try:
                species_name.index(char)
            except:
                pass
            else :
                return False
        return True
    
    def save(self,*args, **kwargs):
        self.clean()
        super(Species,self).save(*args, **kwargs)
    
    
    def clean(self):
        if not Species.is_valid_species_name(self.species) :
            raise ValidationError(_("The species name {0} is not a valid name (should not contain space ( ), diez (#), coma (,), semicolon (;) or underscore (_))").format(self.species))

class GermplasmType(models.Model):
    germplasm_type = models.CharField(max_length=200)
    description = models.CharField(max_length=200,blank=True,null=True)
    def __unicode__(self):
        return u"%s"%self.germplasm_type
    def __str__(self):
        return self.germplasm_type

class Germplasm (models.Model):
    GERMPLASM_MANDATORY_FILE = ["name","idgermplasm","type","species","person"]
    
    
    person = models.ForeignKey('actors.Person', blank=True, null=True, on_delete=models.SET_NULL)    
    idgermplasm = models.CharField(max_length=100,unique=False)
    name = models.CharField(max_length=100,unique=False)
    germplasm_type = models.ForeignKey('GermplasmType',blank=True,null=True,on_delete=models.SET_NULL)
    species = models.ForeignKey('Species',blank=True,null=True,on_delete=models.SET_NULL)

    objects = GermplasmManager()

#     def __unicode__(self):
#         return unicode(self.idgermplasm)
    
    def __str__(self):
        return self.name
    
    def get_genealogy(self):
        from network.models import Relation
        
        relations = Relation.objects.filter(seed_lot_son__germplasm=self).exclude(seed_lot_father__germplasm = self)
        parents = []
        for origin in relations :
            parents.append(origin.seed_lot_father.germplasm)
        
        return parents
    
    @staticmethod
    def is_valid_idgermplasm(idgermplasm):
        char_list=['#',' ','_',',',';']
        for char in char_list:
            try:
                idgermplasm.index(char)
            except:
                pass
            else :
                return False
        return True
    
    def clean(self):
        if not Germplasm.is_valid_idgermplasm(self.idgermplasm) :
            raise ValidationError(_("The idgermplasm  '{0}' is not a valid name (should not contain space ( ), diez (#), coma (,), semicolon (;) or underscore (_))").format(self.idgermplasm))
        if self.idgermplasm == "":
            self.idgermplasm = self.name.replace(' ','-').replace('\'','-')
            
    def save(self,*args, **kwargs):
        self.clean()
        super(Germplasm,self).save(*args, **kwargs)
    
    class Meta :
        ordering = ('idgermplasm',)
        constraints = [models.UniqueConstraint(name='unique_name',fields=['name','species']),
                       models.UniqueConstraint(name='unique_idgermplasm',fields=['idgermplasm', 'species'])
            ]

class Seedlot (models.Model):
    
    SEEDLOT_MANDATORY_FILE = ["germplasm","species","location","year","quantity_ini"]
    
    name = models.CharField(max_length=100, unique=True)
    location = models.ForeignKey('actors.Location', on_delete=models.CASCADE)
    germplasm = models.ForeignKey('Germplasm', on_delete=models.CASCADE)
    quantity_ini = models.FloatField(blank=True,null=True)
    date = models.IntegerField(blank=True, null=True)
    storage_device = models.ForeignKey('actors.StorageDevice', blank=True, null=True, on_delete=models.SET_NULL)
    
    generation = models.IntegerField(default=0)
    onfarm_generation = models.IntegerField(default=0)
    confidence = models.BooleanField(default=False)
    onfarm_confidence = models.BooleanField(default=False)
    comments = models.TextField(null=True)
    
    origin_relations = None
    fathers = None
    
    
    objects = SeedlotManager()
    
    class Meta :
        ordering = ('name',)
        
#         indexes = [
#                    models.Index(fields='germplasm'),
#                    models.Index(fields='location')
#                    ] 

    def __str__(self):
        return self.name
    
    def delete(self,*args, **kwargs):
        to_delete = []
        for rd in self.rawdata_set.all() :
            if rd.seedlot.all().count() == 1 :
                to_delete.append(rd)
        for rd in to_delete :
            rd.delete()
        super(Seedlot, self).delete(*args, **kwargs)
    
    def parents(self):
        Relation = apps.get_model("network", "Relation")  
        relations = Relation.objects.filter(seed_lot_son=self)
        self.fathers = []
        for rel in relations :
            self.fathers.append(rel.seed_lot_father)
        return self.fathers
    
    def get_selection_name(self):
        if '#' in self.name :
            try :
                return self.name.split('_')[0].split('#')[1]
            except :
                return None
        else :
            return None
        
    
    def first_parents(self):
        Relation = apps.get_model("network", "Relation") 
        relations = Relation.objects.filter(seed_lot_son=self)
        if relations :
            if len(relations) > 1 : #c'est qu'il s'agit d'une mixture ou d'un cross
                parents = []
                for r in relations :
                    if r.seed_lot_father.germplasm == r.seed_lot_son.germplasm: #cas des merges
                        return parents
                    parents.append("{0} [{1}]".format(r.seed_lot_father, r.is_male))
                return parents
            else :
                return relations[0].seed_lot_father.first_parents()
        else :
            return []
    
    def still_available(self):
        first_relations = []
        quantity = self.get_remaining_quantity()
        quantity_updates = self.updatequantity_set.all()
        
        for qu in quantity_updates :
            first_relations.extend(list(qu.relations_before.all()))
        
        last_relations = self.relations_as_parent.exclude(id__in=[f.id for f in first_relations])
        for r in last_relations :
            if r.split != '' and int(r.split) == 0:
                return False
        try :
            if quantity <= 0 :
                return False
        except :
            #if quantity is unkknown but split has never been 0 we return True
            return True
        else :
            #quantity > 0
            return True

    is_available = property(still_available)
    
    def get_remaining_quantity(self):
        Relation = apps.get_model('network','Relation')
        if self.quantity_ini != None:
            remaining_quantity = float(self.quantity_ini)
        else:
            remaining_quantity = _('Unknown quantity')

        last_relations = Relation.objects.filter(seed_lot_father=self).exclude(selection__isnull=False)
        quantity_update = UpdateQuantity.objects.filter(seed_lot=self).order_by('date')

        for up in quantity_update :
            last_relations = last_relations.exclude(id__in=up.relations_before.all())
            remaining_quantity = up.quantity

        for r in last_relations:
            if r.quantity != None and r.quantity != "":
                try :
                    remaining_quantity -= float(r.quantity)
                except :
                    remaining_quantity = "Unknown"
                    break
            else :
                remaining_quantity = "Unknown"
                break

        return remaining_quantity

    def parents_name(self):
        fathers_name = []
        if self.fathers is not None:
            for fat in self.fathers :
                fathers_name.append(fat.name)
            if fathers_name :
                return fathers_name
            else :
                return [str(_("No fathers for this Seed lot")),]
        else :
            self.parents()
            return self.parents_name()


    def get_relation_creation(self):        
        self.origin_relations = self.relations_as_child.all()

    def get_relation_name_creation(self):
        if self.origin_relations is not None:
            if self.origin_relations:
                all_relation_type = [r.get_relation_type() for r in self.origin_relations]
                if len(set(all_relation_type)) == 1:
                    return self.origin_relations[0].get_relation_type()
                else :
                    return _("Several origin for this seed lot")
            else :
                return _("No previous relation")

        else :
            self.get_relation_creation()
            return self.get_relation_name_creation()

    def grand_parents_name(self):
        grand_parents_name = []
        for p in self.fathers:
            grand_parents_name.extend(p.parents_name())
        return grand_parents_name

    def get_origindiffusion(self):
        d = get_origindiffusion(self)
        return d
    

    def projects_involved_in(self):
        projects = []

        for r in self.relations_as_parent.all():
            projects.extend(list(r.project.all()))
        for r in self.relations_as_child.all():
            projects.extend(list(r.project.all()))

        return set(projects)
    
    @staticmethod
    def get_date_from_name(seedlotname):
        try :
            date = seedlotname.split('_')[2]
            return date
        except :
            return None
    
    @staticmethod
    def build_seedlot_name(name):
        """does the seed lot with digit exist?no:error,yes use it"""
            
        if len(name.split('_')) == 3:        
            result = Seedlot.objects.filter(name__startswith=name).values_list('name',flat=True).order_by('-name')
        elif len(name.split('_')) == 1:
            result = Germplasm.objects.filter(idgermplasm__startswith=name).values_list('idgermplasm',flat=True).order_by('-idgermplasm')
        if result:
            last_digit = ""
            last_digit = result[0].split('_')[-1]
            last_digit =  int(last_digit)+1
            return "%s_%.4d"%(name,last_digit)
        else:
            last_digit = 1
            result = name
            return "%s_%.4d"%(result,last_digit)
        
    @staticmethod
    def get_list_from_request(data):
        seedlot_names_list = []
        for son in data.keys():
            if son != "father" and re.match(".+?\_\w{3,10}\_\d+\_\d{4}",son) :
                seedlot_names_list.append(son)
        return seedlot_names_list
    
    def store_from_file(self, storage_line, storages):
        StorageDevice = apps.get_model('actors','storagedevice')
        if storage_line["level4"] != "":
            storagedevice = None
            try :
                # storagedevice = StorageDevice.objects.get(depth=4, name=storage_line["level4"], 
                #                           parent_device__depth=3, parent_device__name=storage_line["level3"],
                #                           parent_device__parent_device__depth=2, parent_device__parent_device__name=storage_line["level2"],
                #                           parent_device__parent_device__parent_device__depth=1, parent_device__parent_device__parent_device__name=storage_line["level1"], 
                #                           parent_device__parent_device__parent_device__location=self.location)
                storagedevice = storages[self.location.short_name][storage_line["level1"]][storage_line["level2"]][storage_line["level3"]][storage_line["level4"]+'-object']
            except StorageDevice.DoesNotExist as e:
                raise StorageException("There is no storage device named {0} > {1} > {2} > {3} at {4}".format(storage_line["level1"], storage_line["level2"], storage_line["level3"], storage_line["level4"],self.location))
            except KeyError as e:
                raise StorageException("There is no storage device named {0} > {1} > {2} > {3} at {4}".format(storage_line["level1"], storage_line["level2"], storage_line["level3"], storage_line["level4"],self.location))
            else :
                self.storage_device = storagedevice
                self.save()
        elif storage_line["level3"] != "":
            storagedevice = None
            try :
                # storagedevice = StorageDevice.objects.get(depth=3, name=storage_line["level3"],
                #                           parent_device__depth=2, parent_device__name=storage_line["level2"],
                #                           parent_device__parent_device__depth=1, parent_device__parent_device__name=storage_line["level1"], 
                #                           parent_device__parent_device__location=self.location)
                storagedevice = storages[self.location.short_name][storage_line["level1"]][storage_line["level2"]][storage_line["level3"]+'-object']
            except StorageDevice.DoesNotExist as e:
                raise StorageException("There is no storage device named {0} > {1} > {2} at {3}".format(storage_line["level1"], storage_line["level2"], storage_line["level3"], self.location))
            except KeyError as e:
                raise StorageException("There is no storage device named {0} > {1} > {2} at {3}".format(storage_line["level1"], storage_line["level2"], storage_line["level3"], self.location))
            else :
                self.storage_device = storagedevice
                self.save()
        elif storage_line["level2"] != "":
            storagedevice = None
            try :
                # storagedevice = StorageDevice.objects.get(depth=2, name=storage_line["level2"],
                #                           parent_device__depth=1, parent_device__name=storage_line["level1"], 
                #                           parent_device__location=self.location)
                storagedevice = storages[self.location.short_name][storage_line["level1"]][storage_line["level2"]+'-object']
            except StorageDevice.DoesNotExist as e:
                raise StorageException("There is no storage device named {0} > {1} at {2}".format(storage_line["level1"], storage_line["level2"], self.location))
            except KeyError as e:
                raise StorageException("There is no storage device named {0} > {1} at {2}".format(storage_line["level1"], storage_line["level2"], self.location))
            else :
                self.storage_device = storagedevice
                self.save()
        elif storage_line["level1"] != "":
            storagedevice = None
            try :
                # storagedevice = StorageDevice.objects.get(
                #                         depth=1, name=storage_line["level1"], 
                #                         location=self.location)
                storagedevice = storages[self.location.short_name][storage_line["level1"]+'-object']
            except StorageDevice.DoesNotExist as e:
                raise StorageException("There is no storage device named {0} at {1}".format(storage_line["level1"], self.location))
            except KeyError as e:
                raise StorageException("There is no storage device named {0} at {1}".format(storage_line["level1"], self.location))
            else :
                self.storage_device = storagedevice
                self.save()
    
class UpdateQuantity(models.Model):
    seed_lot =  models.ForeignKey('Seedlot', on_delete=models.CASCADE)
    quantity = models.FloatField()
    comment = models.TextField(max_length=150,blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="Update date")
    relations_before = models.ManyToManyField('network.relation')
    
    def __unicode__(self):
        return u"%s"%self.quantity
    def __str__(self):
        return str(self.quantity)

    def save(self, *args, **kwargs):
        super(UpdateQuantity, self).save(*args, **kwargs)

    class Meta :
        ordering = ('seed_lot',)
        