# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""


def get_origindiffusion(seedlot):
    relations = seedlot.relations_as_child.all()
    if relations :
        if len(relations) > 1 : #c'est qu'il s'agit d'une mixture ou d'un cross
            for r in relations :
                if r.seed_lot_father.germplasm == r.seed_lot_son.germplasm:
                    return get_origindiffusion(r.seed_lot_father)
            return None
        elif relations[0].reproduction != None :
            return get_origindiffusion(relations[0].seed_lot_father)
        elif relations[0].diffusion != None :
            return relations[0]
        else :
            return None
    else :
        return None


def is_number(s):
    try:
        float(s.replace(',','.'))
        return True
    except ValueError:
        return False