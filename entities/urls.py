# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.urls import path

from entities.views import catalogue_query, upload_seedlots_file, create_seedlot, get_harvested_seedlots, submit_germplasm_type, seedlot_profil, germplasm_profil, \
                            getgermplasmlist, species_profil, update_quantity, upload_germplasm_file, GermplasmListView, GermplasmEditView, GermplasmCreateView

from entities.views_autocomplete import GermplasmTypeAutocomplete, GermplasmAutocomplete, SeedlotAutocomplete, SpeciesAutocomplete

from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [              
    path('query/', get_harvested_seedlots, name='harvestedlots'),
    path('uploaded_data_bis/', submit_germplasm_type, name='submit_gp_type'),  


    path('species/<int:species_id>/', species_profil, name='species_profil'),

    path('getgermplasmlist/', getgermplasmlist, name='getgermplasmlist'),
    
    path('updatequantity/<int:seedlot_id>/', update_quantity, name='update_quantity'),

    path('seedlot/upload/', upload_seedlots_file, name='upload_seedlot'),
    path('seedlot/create/', create_seedlot, name='create_one_seedlot'),
    path('seedlot/<int:seedlot_id>/', seedlot_profil, name='seedlot_profil'),
    
    path('catalogue_query/', catalogue_query, name='catalogue_query'),
    
    path('germplasm/upload/', upload_germplasm_file, name='upload_germplasm'),
    path('germplasm/list/', GermplasmListView.as_view(), name='germplasm-list'),
    path('germplasm/edit/<int:pk>/', GermplasmEditView.as_view(), name='germplasm-update'),
    path('germplasm/create/', GermplasmCreateView.as_view(), name='germplasm-create'),
    path('germplasm/<int:germplasm_id>/', germplasm_profil, name='germplasm_profil'),


    path('germplasm-autocomplete/', GermplasmAutocomplete.as_view(), name='germplasm-autocomplete'),
    path('germplasmtype-autocomplete/', GermplasmTypeAutocomplete.as_view(), name='germplasmtype-autocomplete'),
    path('seedlot-autocomplete/', SeedlotAutocomplete.as_view(), name='seedlot-autocomplete'),
    path('species-autocomplete/', SpeciesAutocomplete.as_view(), name='species-autocomplete'),



    ]




if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)