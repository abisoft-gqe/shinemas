# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Eva Dechaux, Melanie Polart-Donat

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

import json
import folium
from datetime import datetime
from collections import OrderedDict 

import networkx as nx
from bokeh.embed import components
from bokeh.io import show, output_file
from bokeh.plotting import figure, from_networkx, show
from bokeh.models import Circle, MultiLine, HoverTool, GraphRenderer, Ellipse, StaticLayoutProvider, TapTool, OpenURL, ColumnDataSource, NodesAndLinkedEdges, Legend, \
                        LegendItem
from bokeh.models.renderers import GlyphRenderer
from bokeh.events import MouseEnter
from bokeh.palettes import linear_palette
from colorcet import glasbey_dark, glasbey_light

from django.contrib import messages
from django import forms
from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponse, Http404, HttpResponseBadRequest
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import user_passes_test, login_required
from django.db.models import Q, Min
from django.utils.translation import gettext_lazy as _
from django.shortcuts import render
from django.apps import apps
from django.core.files.storage import FileSystemStorage
from django.shortcuts import render
from django.conf import settings
from django.core.files import File
from django.contrib.auth.models import User    
from django.db import transaction
from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView, CreateView
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.urls import reverse_lazy

from csv_io.factory import CSVFactoryForTempFiles
from csv_io.models import AuditFile
from csv_io.exceptions import AlreadySubmitedFile
from actors.models import Location, Person
from entities.models import Seedlot, Germplasm, GermplasmType, Species, UpdateQuantity

from entities.forms import CatRequest, ClassicRequest, CLASSIC, GENERATION, RELATION, MEASURES, NewQuantityForm, NewSeedlotForm, \
                    GermplasmUpdateForm, GermplasmCreateForm

from entities.factory import AdvancedQueryFactory
from network.models import Relation
from network.network_plot import NetworkGraph
from eppdata.models import Variable, Rawdata
from images.models import ImageLink


@login_required
def seedlot_profil(request,seedlot_id):
    try :
        seedlot_obj = Seedlot.objects.get(id=int(seedlot_id))
        remaining_quantity = seedlot_obj.get_remaining_quantity()
        relation_as_father = Relation.objects.filter(seed_lot_father=seedlot_obj)
        relation_as_son = Relation.objects.filter(seed_lot_son=seedlot_obj)
        relation_as_update_quantity = UpdateQuantity.objects.filter(seed_lot=seedlot_obj)        
        entities_queryset = ImageLink.objects.filter(entity_name=str(seedlot_obj))
        images = []
        for entity in entities_queryset:
            images.append(entity.image)

        return render(request, 'entities/seedlot_profil.html', {'seedlot':seedlot_obj,
                                                                'relation_as_father':relation_as_father,
                                                                'relation_as_son':relation_as_son,
                                                                'rquantity':remaining_quantity,
                                                                'images':images,
                                                                'relation_as_update_quantity':relation_as_update_quantity
                                                                })
    except ObjectDoesNotExist:
        raise Http404
    #except TypeError :
    #    return HttpResponseBadRequest(_("Quantity ini is not valid : %s")%seedlot_id)

class GermplasmListView(UserPassesTestMixin, LoginRequiredMixin, ListView):
    model = Germplasm
    paginate_by = 100

    def get_queryset(self, **kwargs):
        return Germplasm.objects.select_related('person','species','germplasm_type').all()
        
    
    def test_func(self):
        return self.request.user.is_superuser

class GermplasmEditView(UserPassesTestMixin, LoginRequiredMixin, UpdateView):
    model = Germplasm
    template_name_suffix = '_update_form'
    success_url = reverse_lazy('germplasm-list')
    form_class = GermplasmUpdateForm
    
    def test_func(self):
        return self.request.user.is_superuser
    
class GermplasmCreateView(UserPassesTestMixin, LoginRequiredMixin, CreateView):
    model = Germplasm
    template_name_suffix = '_create_form'
    success_url = reverse_lazy('germplasm-list')
    form_class = GermplasmCreateForm
    
    def test_func(self):
        return self.request.user.is_superuser

@login_required
def germplasm_profil(request,germplasm_id): 
    try:
        germplasm_obj = Germplasm.objects.get(id=int(germplasm_id))
        
        #seedlot = Seedlot.objects.filter(germplasm = germplasm_obj)
        
        fig = folium.Figure()
        map = folium.Map(
            width=1000,
            height=400,
            location=[47, 2],
            zoom_start=5,
        )
        map.add_to(fig)
        
        location_obj = Location.objects.filter(seedlot__germplasm  =  germplasm_obj).distinct()
                       
        for l in location_obj :
        
            latitude = l.latitude
            longitude = l.longitude
            
            if l.latitude and l.longitude:
                marker = folium.Marker([latitude, longitude], popup=l.short_name, tooltip=l.short_name)
                map.add_child(marker)
            
        fig.render() 
        
        """ Graph builder to move in distinct class"""
        
        
        base_url = "{0}://{1}".format(request.scheme, request.get_host())
        #recuperation des relations
        relation = Relation.objects.select_related('seed_lot_son','seed_lot_father','selection','diffusion','mixture','reproduction').filter(seed_lot_father__germplasm__name = germplasm_obj)
        seedlot = Seedlot.objects.select_related('location').filter(relations_as_parent__in = relation).union(Seedlot.objects.select_related('location').filter(relations_as_child__in = relation))
        ng = NetworkGraph(relation, seedlot, germplasm_obj, base_url)
        script, div = ng.get_components()  
               
    except ObjectDoesNotExist:
        raise HttpResponseBadRequest(_("No Germplasm with id %s")%germplasm_id)
    else:
        available_seedlots = []
        
        for sl in Seedlot.objects.prefetch_related('relations_as_parent').filter(germplasm=germplasm_obj) :
            if sl.is_available:
                available_seedlots.append(sl)
        
        """ annee de creation du germplasm dans la base """
        date_creation_germ = []
        try:  
            date_creation_germ = Seedlot.objects.filter(germplasm=int(germplasm_id)).values('date')
        except:
            pass
        if date_creation_germ == []:
            date_min = _('Unknown')
        else:               
            date_min = date_creation_germ.aggregate(Min('date'))
            date_min = date_min['date__min']

        entities_queryset = ImageLink.objects.filter(entity_name=str(germplasm_obj))
        images = []
        for entity in entities_queryset:
            images.append(entity.image)
        return render(request, 'entities/germplasm_profil.html',{'germplasm':germplasm_obj,
                                                                 'availablesl':available_seedlots,
                                                                 'date':date_min,
                                                                 'images':images,
                                                                 'map': fig,
                                                                 'script':script,
                                                                 'reseau':div,
                                                                 })                


@login_required
def species_profil(request, species_id):
    try :
        species_obj = Species.objects.get(id=int(species_id))
        
        germplasm = Germplasm.objects.filter(species = species_obj)
        germplasm_count = germplasm.count()
        
        
        seedlot_count = Seedlot.objects.filter(germplasm__species = species_obj).count()
        
        fig = folium.Figure()
        map = folium.Map(
            width=1000,
            height=400,
            location=[47, 2],
            zoom_start=5,
        )
        map.add_to(fig)
        
        location_obj = Location.objects.filter(seedlot__germplasm__species  =  species_obj).distinct()
        
        for l in location_obj:
            if l.longitude and l.latitude:
                marker = folium.Marker([l.latitude, l.longitude], popup=l.short_name, tooltip=l.short_name)
                map.add_child(marker)
                
        fig.render()
        
        return render(request, 'entities/species_profil.html',{'species':species_obj,
                                                               'germplasmCount':germplasm_count,
                                                               'seedlotCount':seedlot_count,
                                                               'map': fig})
    except ObjectDoesNotExist:
        raise Http404
  
@login_required
def result_catalogu(request, germ_list, var_list, data_list):
    #function qui récupère les raw_data de la base de donnée à partir des paramètres de recherche
    for r in germ_list:
        """print r"""
        data_germ = []
        qr = Rawdata.objects.filter(germplasm=Germplasm.objects.filter(idgermplasm = r))
        """print qr"""
        for s in var_list:
            qs = qr.filter(variable__in = Variable.objects.filter(name= s))
            if len(qs) != 0:
                """print qs"""
                vl = qs[0]
                data_germ.append(vl)
            else:
                data_germ.append("---")
            data_list.append(data_germ)
    return  data_list

@login_required
def catalogue_query(request):
    #Code aichatou : ne doit pas fonction s'il y a des erreurs (cf appel à somme_errors)
    #En fonction du nombre de  paramètres donnés par l'utilisateur, on lui affiche les raw_data retournés par la fonction result_catalog()
    lis = []
    error_messages = {"error_date":[],"error_select_date":[]}
           
    if request.method == "POST":

        error_messages = {"error_date":[],"error_select_date":[]}
    
        form = CatRequest(data = request.POST)
        """form.fields['catYear'].widget = forms.HiddenInput()"""
        if form.is_valid():
            if "liste_germ" in request.POST:
                germ_lis = request.POST.getlist("liste_germ")
                if "liste_var" in request.POST:
                    var_lis = request.POST.getlist("liste_var")
                    datalis = result_catalogu(request, germ_lis, var_lis,lis)
                else:
                    var_lis = Variable.objects.filter(type = 'T12')  
                    datalis = result_catalogu(request, germ_lis, var_lis, lis)
              
            else:
                germ_lis= Germplasm.objects.filter(person = None)
                if "liste_var" in request.POST:
                    var_lis = request.POST.getlist("liste_var")
                    datalis = result_catalogu(request,germ_lis, var_lis, lis )
                else:
                    germ_lis = Germplasm.objects.filter(person = None)
                    var_lis = Variable.objects.filter(type = 'T12') 
                    datalis = result_catalogu(request,germ_lis, var_lis,lis )
               
            len_germpls = list(range(len(germ_lis)))       
            return render(request, 'entities/catalogue_query.html', {'form': form, 'var': var_lis, 'data_list': datalis, 'germpls': germ_lis, 'len_germpls': len_germpls})
                     
                    
        else:
            return render(request, 'entities/catalogue_query.html', {'form': form})
    else :  
        form = CatRequest()
        return render(request, 'entities/catalogue_query.html', {'form': form})
                             
@login_required
def get_harvested_seedlots(request):

    if request.method == "POST":        
        mode = None
        
        include_no_date = bool(request.POST.get("no_date"))
        location = request.POST.get("location")
        year = request.POST.get("year")
        date_start = request.POST.get("start_date")
        date_end = request.POST.get("end_date")
        projects = request.POST.get("projects")
        germplasm = request.POST.get("germplasm")
        relationtype = int(request.POST.get("relation"))
        variables_ids = list(set(request.POST.getlist("variables")))
        location_exclude = bool(request.POST.get("location_exclude"))
        year_exclude = bool(request.POST.get("year_exclude"))
        projects_exclude = bool(request.POST.get("projects_exclude"))
        germplasm_exclude = bool(request.POST.get("germplasm_exclude"))
        relationtype_exclude = bool(request.POST.get("relation_exclude"))
        only_images = bool(request.POST.get("images"))
        
        form = ClassicRequest(data=request.POST)
        
        if form.is_valid() :
            
            mode = request.POST["mode"]
            query = None
            filters = {"location":location,
                       "year":year,
                       "date_start":date_start,
                       "date_end":date_end,
                       "projects":projects,
                       "germplasm":germplasm,
                       "relationtype":relationtype,
                       "variables_ids":variables_ids,
                       "only_images": only_images
                       }
            excludes = {
                        "location_exclude":location_exclude,
                        "year_exclude":year_exclude,
                        "projects_exclude":projects_exclude,
                        "germplasm_exclude":germplasm_exclude,
                        "relationtype_exclude":relationtype_exclude
                }
            query = Seedlot.objects.advanced_query(filters, excludes)

            """transfer this code in the advanced query"""
            if only_images:
                for q in query:
                    contains_img = ImageLink.objects.filter(entity_name=str(q))
                    if not contains_img:
                        query = query.exclude(id=q.id) 
                           
            if mode == CLASSIC :
                list_lots = AdvancedQueryFactory.format_classic_query(query)
                return render(request, 'entities/advanced_query.html', {'form': form,'list_lots':list_lots,'selected_mode':CLASSIC})
            
            elif mode == GENERATION :
                return render(request, 'entities/advanced_query.html', {'form': form,'list_lots':query,'selected_mode':GENERATION})
            
            elif mode == RELATION :
                relations = Relation.objects.filter(seed_lot_son__in=query).exclude(reproduction=None).exclude(is_male='M').exclude(is_male='F')        
                return render(request, 'entities/advanced_query.html', {'form': form,'list_lots':relations,'selected_mode':RELATION})
            
            elif mode == MEASURES :
                print(include_no_date)
                """We select first the relations in which some measures have been made betwwen the start_date and end_date"""
                params = {'include_no_date':include_no_date,
                          'date_start':date_start,
                          'date_end':date_end
                    }
                ##Mettre la requête dans un manager
                relations = Relation.objects.advanced_query(query, variables_ids, params)
                
                """Now we select individual data"""
                ind_variables = Variable.objects.get_individual_variables_from_ids(variables_ids)
#                 if variables_ids:
#                     ind_variables = Variable.objects.filter(id__in = variables_ids, type = 'T11')
#                 else :
#                     ind_variables = Variable.objects.filter(type = 'T11')
                individual_results = AdvancedQueryFactory.format_individual_data(relations, ind_variables, params)
                
                """Second, we select data related to seed lot"""
                ##a mettre dans AdvancedQueryFactory
                
                sl_variables = Variable.objects.get_seedlot_variables_from_ids(variables_ids)
                seed_lots_results = AdvancedQueryFactory.format_seedlot_data(query, sl_variables, params)
                
                
                """Last we select data related to relations (but not individual)"""
                ##a mettre dans AdvancedQueryFactory
                glob_variables = Variable.objects.get_global_variables_from_ids(variables_ids)
                global_results = AdvancedQueryFactory.format_global_data(relations, glob_variables, params)

                return render(request, 'entities/advanced_query.html', {'form': form,
                                                                        'ind_variables':ind_variables,
                                                                        'glob_variables':glob_variables,
                                                                        'sl_variables':sl_variables,
                                                                        'list_lots':query,
                                                                        'relations':relations,
                                                                        'mesu_sl':seed_lots_results,
                                                                        'mesu_ind':individual_results,
                                                                        'mesu_globales':global_results,
                                                                        'selected_mode':MEASURES})
        else:
            return render(request, 'entities/advanced_query.html', {'form': form})
    else :
        form = ClassicRequest()
        return render(request, 'entities/advanced_query.html', {'form': form})

@login_required
@user_passes_test(lambda u: u.is_superuser)
def submit_germplasm_type(request):
    if request.method == "POST" :
        #for i in germ_obj_list:
        for key in request.POST:
            if key.startswith('germplasm_type') :
                name = key.split('_')[2] #retourne le germplasme
                species = key.split('_')[-1] #retourne l'espèce
                species_id = Species.objects.get(species=species).id
                try:
                    g = Germplasm.objects.get(idgermplasm=name,species=species_id)
                except Germplasm.DoesNotExist:
                    return HttpResponseBadRequest('%s does not exist. <br>' % name)
                else:
                    t = GermplasmType.objects.get(germplasm_type=request.POST[key])
                    g.germplasm_type = t
                    g.save()
        return HttpResponse("<i><b>{0}</b></i>".format(_("Germplasm type has been defined for all new germplasm")))
    else :
        raise Http404

@login_required
def getgermplasmlist(request):
    if request.is_ajax():
        germplasm_list = []
        for gp_name in Germplasm.objects.values('idgermplasm'):
            germplasm_list.append(gp_name['idgermplasm'])
        return HttpResponse(json.dumps(germplasm_list), content_type="application/json")
    else :
        raise Http404



@login_required
@user_passes_test(lambda u: u.is_superuser)
def update_quantity(request,seedlot_id):
    """
    This view enable to update a seed lots quantity.

    @param request: The request object send by Http
    @type request: HttpRequest object

    @return: A HttpResponse with corresponding template
    """
    seedlot_obj = Seedlot.objects.get(id=int(seedlot_id))
    #quantity_actual = seedlot_obj.get_remaining_quantity()
    remaining_quantity = seedlot_obj.get_remaining_quantity()

    if request.method == "GET":
        quantityform = NewQuantityForm()

        return render(request, 'entities/update_quantity.html', {'seedlot':seedlot_obj,'rquantity':remaining_quantity,'form':quantityform})
    else :
        if request.method == "POST":
            quantity_new = request.POST['newquantity']
            comment = request.POST['comment']
            quantityform = NewQuantityForm(request.POST)
            if quantityform.is_valid():
#                 try :
#                     quantity_actual = float(quantity_actual)
#                 except :
#                     quantity_actual = 0
#                     
#                 try :
#                     quantity_new = float(quantity_new)
#                 except :
#                     quantity_new = 0
#                 quantity_add = quantity_new - quantity_actual
                UpdateQuantity = apps.get_model('entities', 'updatequantity')
                previous_update = UpdateQuantity.objects.filter(seed_lot=seedlot_obj)
                relations_already_sub = []
                for up in previous_update :
                    relations_already_sub.extend(up.relations_before.all())
                update = UpdateQuantity.objects.create(seed_lot=seedlot_obj, quantity=quantity_new, comment=comment)
                update.relations_before.set(Relation.objects.filter(seed_lot_father=seedlot_obj).exclude(id__in=[r.id for r in relations_already_sub]))
                remaining_quantity = seedlot_obj.get_remaining_quantity()
            return render(request, 'entities/update_quantity.html',{'seedlot': seedlot_obj, 'rquantity': remaining_quantity, 'form': quantityform})

        else :
            raise Http404
                
@user_passes_test(lambda u: u.is_superuser)
@login_required
def upload_seedlots_file(request):
    """
        function used to upload SeedLot from  file 
    """
    
    username=request.user
    
    seedlot=[]
    if request.method == "POST":

        file_name = request.FILES['file']
        #upload file
        
        myfactory = CSVFactoryForTempFiles(file_name)
        myfactory.find_dialect()
        myfactory.read_file()
        file_dict = myfactory.get_file()
        hashprint = myfactory.getHash()
        
        try :
            AuditFile.objects.check_submited_files(hashprint, username)
        except AlreadySubmitedFile as e:
            messages.add_message(request, messages.ERROR, str(e))
            return render(request, 'entities/uploadSeedLotFile.html', {})
        with transaction.atomic():        
            for line in file_dict:
                try:
                    seedlot.append(Seedlot.objects.create_new_seed(line.copy(),"file"))
                except Exception as e:
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["germplasm"])+", "+str(line["species"])+").")
            
            AuditFile.objects.audit_file(file_name, username, hashprint)
            
            
        #To cancel all the transactions if there is an error message
        err=False
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                err=True
                break
        if err==False:
            Seedlot.objects.bulk_create(seedlot)
            messages.add_message(request, messages.SUCCESS, "Insertion Succeded")
           
            
    return render(request, 'entities/uploadSeedLotFile.html', {})

@user_passes_test(lambda u: u.is_superuser)
@login_required
def upload_germplasm_file(request):
    
    username=request.user
    
    germplasm = []
    global_data = {}
    variables = []
    err = False
    if request.method == "POST":

        file_name = request.FILES['file']
        #upload file
        
        myfactory = CSVFactoryForTempFiles(file_name)
        myfactory.find_dialect()
        myfactory.read_file()
        file_dict = myfactory.get_file()
        hashprint = myfactory.getHash()
        
        
        try :
            AuditFile.objects.check_submited_files(hashprint, username)
        except AlreadySubmitedFile as e:
            err = True
            messages.add_message(request, messages.ERROR, str(e))
            return render(request, 'entities/upload_germplasm_file.html', {})
               
               
        vars_and_methods = myfactory.get_headers().copy()
        [vars_and_methods.remove(x) for x in Germplasm.GERMPLASM_MANDATORY_FILE]
        try :
            variables = Rawdata.check_variables_file(vars_and_methods)
        except Exception as exceptions:
            for e in exceptions.args:
                err = True
                messages.add_message(request, messages.ERROR, "headers line: {0}".format(str(e)))
            
        iline = 2
        for line in file_dict:
            try:
                germplasm.append(Germplasm.objects.create_germplasm(line.copy(), "file"))
            except Exception as e:
                err = True
                messages.add_message(request, messages.ERROR, "line {1}: {0}".format(str(e), iline))
            
            #read data here and put it in a dict with tuple gp name / species as key
            data_key = (line["name"], line["species"])
            global_data[data_key] = []
            for v in variables :
                if line[v.name]:
                    try :
                        global_data[data_key].append(Rawdata.objects.prepare_rawdata(line, v))
                    except Exception as e:
                        err = True
                        messages.add_message(request, messages.ERROR, "line {1}: {0}".format(str(e), iline))
            
            iline+=1

        if err==False:
            with transaction.atomic(): 
                germplasms_objects = Germplasm.objects.bulk_create(germplasm)
                for gp in germplasms_objects:
                    dataset = Rawdata.objects.bulk_create(global_data[(gp.name,gp.species.species)])
                    gp.rawdata_set.add(*dataset)
                messages.add_message(request, messages.SUCCESS, "Insertion Succeded")
                AuditFile.objects.audit_file(file_name, username, hashprint)

    return render(request, 'entities/upload_germplasm_file.html', {})
    


@user_passes_test(lambda u: u.is_superuser)
@login_required    
def create_seedlot(request):
    """function used to upload SeedLot from  text field 
    """
    #print(get_user(request))
    if request.method == "POST":
        form_seedlot = NewSeedlotForm(data=request.POST)

        line = {}
        #insert a data
        #if request.POST.get('submit'):
        if form_seedlot.is_valid() :
            line["germplasm"]= request.POST.get('germplasm')
            line["location" ]=request.POST.get('location')
            line["year"]=request.POST.get('date')
            line["quantity_ini"]=request.POST.get('quantity_ini')
            line["species"]=request.POST.get('species')
                   
                   
            try:
                s=Seedlot.objects.create_new_seed(line,"form")
                s.save()
            except Exception as e:                
                messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["germplasm"])+", "+str(line["location"])+").")
       
            #To cancel all the transactions if there is an error message
            err=False
            for msg in messages.get_messages(request):
                if msg.level == 40:
                    err=True
                    break
            if err==False:
                s.save()
                messages.add_message(request, messages.SUCCESS, "Insertion Succeded")
               
            
    return render(request, 'entities/uploadOneSeedLot.html', {'form':NewSeedlotForm()})

   

