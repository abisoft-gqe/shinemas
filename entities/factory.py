# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.db.models import Q

from eppdata.models import Rawdata

class AdvancedQueryFactory():
    
    @staticmethod
    def format_classic_query(queryset):
        list_lots = []
        for lot in queryset :
            selection_name = ""
            selection_person = ""
            if lot.get_relation_name_creation() == "Selection":
                selection = lot.origin_relations[0]
                selection_name = selection.selection.selection_name
                selection_person = selection.selection.person.short_name
                
            origin_seedlot_name = ""
            origin_date = ""
            origin_diffusion = lot.get_origindiffusion()
            
            if origin_diffusion :
                origin_seedlot_name = origin_diffusion.seed_lot_father.name
                origin_date = origin_diffusion.diffusion.date
            list_lots.append([
                              lot.id, 
                              lot.name,
                              lot.get_relation_name_creation(),
                              '; '.join(lot.parents_name()),
                              '; '.join([str(p.get_relation_name_creation()) for p in lot.parents()]),
                              '; '.join(lot.grand_parents_name()),
                              selection_name,
                              selection_person,
                              origin_seedlot_name,
                              origin_date
                              ])
        return list_lots
    
    @staticmethod
    def format_individual_data(relations, ind_variables, params):
        individual_results = []
        for r in relations :
            for ind in r.get_phenotyped_individual() :
                data_set = Rawdata.objects.filter_with_range_date(params, r.rawdatas.filter(individual=ind))
                table_tr_info = [r, ind, r.block, r.X, r.Y]
                for v in ind_variables :
                    variabledata = data_set.filter(variable = v)
                    if variabledata :
                        datastring = u""
                        for d in variabledata :
                            datastring = u"{0}  ({1}, {2})<br />{3}".format(d.raw_data, d.date, d.method, datastring)
                        table_tr_info.append(datastring)
                    else :
                        table_tr_info.append('-')
                individual_results.append(table_tr_info)
        return individual_results
    
    @staticmethod
    def format_seedlot_data(query, sl_variables, params):
        seed_lots_results = []
        for sl in query :
            data_set = Rawdata.objects.filter_with_range_date(params, sl.rawdata_set.all())
            table_tr_info = [sl,]
            for v in sl_variables :
                variabledata = data_set.filter(variable = v)
                if variabledata :
                    datastring = u""
                    for d in variabledata :
                        datastring = u"{0}  ({1}, {2})<br />{3}".format(d.raw_data, d.date, d.method, datastring)
                    table_tr_info.append(datastring)
                else :
                    table_tr_info.append('-')
            seed_lots_results.append(table_tr_info)
    
    @staticmethod
    def format_global_data(relations, glob_variables, params):
        global_results = []
        for r in relations :
            data_set = Rawdata.objects.filter_with_range_date(params, r.rawdatas.all())
            table_tr_info = [r, r.block, r.X, r.Y]
            for v in glob_variables :
                variabledata = data_set.filter(variable = v)
                if variabledata :
                    datastring = u""
                    for d in variabledata :
                        datastring = u"{0}  ({1}, {2})<br />{3}".format(d.raw_data, d.date, d.method, datastring)
                    table_tr_info.append(datastring)
                else :
                    table_tr_info.append('-')
            global_results.append(table_tr_info)
        return global_results