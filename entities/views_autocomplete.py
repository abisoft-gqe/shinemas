from dal import autocomplete

from entities.models import Germplasm, Seedlot, GermplasmType, Species

class GermplasmAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Germplasm.objects.none()

        qs = Germplasm.objects.all()
        if self.q:
            qs = qs.filter(name__icontains = self.q)
        return qs
    
    def get_result_label(self, result):
        label = "{0} ({1})".format(result, result.species)
        return label
    
class GermplasmTypeAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return GermplasmType.objects.none()

        qs = GermplasmType.objects.all()
        if self.q:
            qs = qs.filter(germplas_type__icontains = self.q)
        return qs

    

class SeedlotAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Seedlot.objects.none()

        qs = Seedlot.objects.all()
        if self.q:
            qs = qs.filter(name__icontains = self.q)
        return qs
    
class SpeciesAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Species.objects.none()

        qs = Species.objects.all()
        if self.q:
            qs = qs.filter(species__icontains = self.q)
        return qs
