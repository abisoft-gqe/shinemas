--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- Name: actors_location_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dbuser
--

SELECT pg_catalog.setval('actors_location_id_seq', 61, true);


--
-- Name: actors_person_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dbuser
--

SELECT pg_catalog.setval('actors_person_id_seq', 94, true);


--
-- Name: eppdata_classe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dbuser
--

SELECT pg_catalog.setval('eppdata_classe_id_seq', 5, true);


--
-- Name: eppdata_env_pra_phe_method_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dbuser
--

SELECT pg_catalog.setval('eppdata_env_pra_phe_method_id_seq', 148, true);


--
-- Data for Name: actors_location; Type: TABLE DATA; Schema: public; Owner: dbuser
--

COPY actors_location (id, location_name, location_type, address, post_code, country, longitude, latitude, altitude) FROM stdin;
1	\N	\N			France			
2	\N	\N	1 r Moulin 	51260	France	3.82	48.63	75
3	\N	\N	54, chemin du Murier	38410	France	5.82	45.15	578
4	\N	\N	18 rue d'Orville	21260	France	5.27	47.56	297
5	\N	\N	Les Chapeliers, Place 21 Juillet	26420	France	5.37	44.89	1061
6	\N	\N	La ferme Picbois, 8 ch des Egrivolays	38690	France	5.4	45.83	487
7	\N	\N	234 avenue du Brezet	63100	France	3.14	45.77	337
8	\N	\N	GAEC du Pont de l'Arche Le Haut Pont de l'Arche	49080	France	-0.6	47.71	24
9	\N	\N	Chassagne	16240	France	0.09	46	119
10	\N	\N	Mas de Mayan, 2025 ch du Mas de Mayan	30900	France	4.33	43.79	23
11	\N	\N			Hollande			
12	\N	\N			Italie			
13	\N	\N	Le Roc	47130	France	0.42	44.4	107
14	\N	\N	Rue des Eaux Claires	16230	France	0.21	45.84	102
15	\N	\N	ferme Carafray Kerbricon	56230	France	-2.44	47.73	73
16	\N	\N	La Pinellerie	49250	France	-0.23	47.46	20
17	\N	\N	920 chemin des Vals	30140	France	4.02	44	153
18	\N	\N	Ferme du Moulon	91190	France	2.16	48.71	161
19	\N	\N	Le Rocher	35330	France	-2.04	47.94	92
20	\N	\N	Ferme de la Bergerie	95710	France	1.71	49.43	123
21	\N	\N	Couderc Bas	47440	France	0.65	44.47	87
22	\N	\N	1086 Rte de Loex	74380	France	6.35	46.46	501
23	\N	\N	19 place saint jacques 	44190	France	-1.28	47.08	38
24	\N	\N	1 r Jean Michelez	91510	France	2.26	48.52	63
25	\N	\N		88700	France	6.66	48.37	286
26	\N	\N			Belgique			
27	\N	\N						
28	\N	\N	Cormier 	49570	France	-0.93	48.01	83
29	\N	\N	Rue des Eaux Claires	16230		0.21	45.84	102
30	\N	\N	Le Bourg 	16370	France	-0.25	45.79	18
31	\N	\N	?????	86210	France	0.65	46.67	130
32	\N	\N	Au Rythme des Saisons, Cauberotte	47600	France	0.37	44.21	72
33	\N	\N	Au Loriot	32550	France	0.49	43.76	239
34	\N	\N	Mas des Bergas	30320	France	4.5	44.2	68
35	\N	\N	Earle Adamakane, 37 Route des Gardians	40190	France	-0.29	43.99	102
36	\N	\N	Le Fournil la Font de Soule	24140	France	0.6	45.04	144
37	\N	\N	89 avenue de Prieux	44380	France	-2.24	47.67	11
38	\N	\N	Montorcier	5260	France	6.26	45.03	1171
39	\N	\N	Rue de Margny 38/3	6823	Belgique	5.49	49.87	195
40	\N	\N	Domaine Des Raux	63360	France	3.09	46.2	318
41	\N	\N	La Curade	81170	France	1.92	44.21	269
42	\N	\N	La Combe -Rioclar	4340	France	6.54	44.74	1218
43	\N	\N	Le Debes	82240	France	1.65	44.39	165
44	\N	\N	Le Bourg	47400	France	0.4	44.46	47
45	\N	\N	61 chemin du Fournet	38940	France	5.18	45.48	489
46	\N	\N	Ferme du Hayon	6769	Belgique	5.49	49.87	255
47	\N	\N		1555	Suisse	6.98	47.04	639
48	\N	\N	Boussac	47130	France	0.4	44.27	155
49	\N	\N	Les perrets	47260	France	0.44	44.47	99
50	\N	\N	Lieu dit Mayersbreit	67120	France	7.66	48.81	157
51	\N	\N	24 Rue George Sand	92500	France	2	49.26	70
52	\N	\N	2 rue du noyer rose	21220	France	5.03	47.18	221
53	\N	\N	3 avenue de la gare	47190	France	0.33	44.29	3
54	\N	\N	Kerna ùn Sohma, 5 place de la gare	68000	France	7.34	48.07	196
55	\N	\N	Ferme de Bobéhec	56250	France	-2.52	47.69	16
56	\N	\N	58 rue Raulin	69007	France	4.83	45.74	172
57	\N	\N	20 rue de la fôret	67330	France	7.39	48.82	241
58	\N	\N	14 rue des bergers	67150	France	7.66	48.42	155
59	\N	\N	5 rue des orfèvres	67000	France	7.74	48.58	152
60	\N	\N	Rue du midi	21440	France	4.74	47.44	508
61	\N	\N		2110	France	5.11	47.37	218
\.


--
-- Data for Name: actors_person; Type: TABLE DATA; Schema: public; Owner: dbuser
--

COPY actors_person (id, first_name, last_name, short_name, short_nickname, nick_name, born_year, gender, email, phone1, phone2, fax, location_id) FROM stdin;
1	Aurelie	Dimouro	AUD	\N	\N		\N				['fax']	1
2	Alain	Basson	ALB	\N	\N		\N		03 26 42 73 56		['fax']	2
3	Alain	Pommard	ALPO	\N	\N		\N		09 79 17 26 46		['fax']	3
4	Alain	Ronot	ALR	\N	\N		\N		03 80 75 70 78		['fax']	4
5	Philippe	Catinaud	ARC	\N	\N		\N				['fax']	1
6	Bastien	Moysan	BAM	\N	\N		\N				['fax']	1
7	Bernard	Ronot	BER	\N	\N	11/10/1932	\N		03 80 75 73 02 et 06 07 08 05 93	03 80 75 70 78 (Alain)	['fax']	4
8	Cecile	Raffetin	CER	\N	\N		\N	ceraf586@orange.fr	04 75 47 14 83		['fax']	5
9	Christian	Dalmasso	CHD	\N	\N		\N	chdalmasso@wanadoo.fr	04 74 92 25 29		['fax']	6
10	Jean	Koenig	CLM	\N	\N		\N	koenig@clermont.inra.fr	04 73 62 43 27		['fax']	7
11	Daniel	Cortial	DAC	\N	\N		\N				['fax']	1
12	Dominique	Bourdon	DOB	\N	\N		\N				['fax']	1
13	Florent	Mercier	FLM	\N	\N		\N	fl.m@laposte.net	02 41 77 15 89	06 01 77 85 80 	['fax']	8
14	Francois	Peloquin	FRP	\N	\N		\N	fpeloquin@wanadoo.fr	05 45 84 25 32		['fax']	9
15	Henri	Ferte	HEF	\N	\N		\N	agripacte@wanadoo.fr	04 66 38 23 28		['fax']	10
16			HGA	\N	\N		\N				['fax']	11
17			HZE	\N	\N		\N				['fax']	11
18			ICE	\N	\N		\N				['fax']	12
19			INE	\N	\N		\N				['fax']	12
20	Jean	Fouquet	JEF	\N	\N		\N				['fax']	1
21	Jean-Francois	Berthellot	JFB	\N	\N		\N	Jean-francois.berthellot@wanadoo.fr	05 53 88 11 84		['fax']	13
22	Joel	Payement	JOP	\N	\N		\N	joel.payement@wanadoo.fr	05 45 20 69 08		['fax']	14
23	Jean-Pierre	Bolognini	JPB	\N	\N		\N				['fax']	1
24	Julie	Bertrand	JUB	\N	\N		\N		06 60 80 02 37		['fax']	15
25	Maria	Irazoqui	MAI  	\N	\N		\N	xoria@laposte.net	02 41 66 22 89		['fax']	16
26	Michel	Rampazzi	MIR	\N	\N		\N		04 66 61 99 48		['fax']	17
27			MLN	\N	\N		\N				['fax']	18
28	Nicolas	Supiot	NIS	\N	\N		\N	supiotn@wanadoo.fr	02 99 34 50 85		['fax']	19
29	Olivier	Ranke	OLR	\N	\N		\N	olivier.ranke@wanadoo.fr	01 34 67 96 17	06 80 41 48 86	['fax']	20
30	Inconnu	Inconnu	P01	\N	\N		\N				['fax']	1
31	Inconnu	Inconnu	P02	\N	\N		\N				['fax']	1
32	Philippe	Catinaud	PHC	\N	\N		\N				['fax']	1
33	Philippe	Guichard	PHG	\N	\N		\N		05 47 99 00 05		['fax']	21
34	Raphael	Baltassat	RAB	\N	\N		\N	talzabar@yahoo.fr	04 50 39 20 45	06 84 97 48 33	['fax']	22
35	?	?	VER	\N	\N		\N				['fax']	1
36	Vincent	Chesneau	VIC	\N	\N		\N	Superpettiot@aol.com	06 14 88 16 19		['fax']	23
37	?	Fuette	FUE	\N	\N		\N				['fax']	1
38	Pierre	Delton	PID	\N	\N		\N		01 60 82 77 22		['fax']	24
39	Henri	Granier	HEG	\N	\N		\N	lamicroboulange@opain.com	03 29 66 02 62		['fax']	25
40	Mark			\N	\N		\N				['fax']	26
41	James	Restoux	JAR	\N	\N		\N				['fax']	1
42	inconnu1		I01	\N	\N		\N				['fax']	27
43	Paul	Ferte	PAF	\N	\N		\N	paul.ferte@no-log.org	04 66 38 23 28		['fax']	10
44	Florence	Payement	FLP	\N	\N		\N		05 45 20 69 08		['fax']	29
45	Cecile	Peloquin	CEP	\N	\N		\N		05 45 84 25 32		['fax']	9
46	Jean-Marie	Roustaud	JMR	\N	\N		\N		05 45 80 98 34 		['fax']	30
47	Isabelle	Goldringer	ISG	\N	\N		\N	isa@moulon.inra.fr			['fax']	1
48	Estelle	Serpolay	ESS	\N	\N		\N				['fax']	27
49	Stephanie		STE	\N	\N		\N				['fax']	1
50	Jean-Marie	Marchand	JMM	\N	\N		\N				['fax']	31
51	Yves	Cauderon	YVC	\N	\N		\N				['fax']	1
52			VEN	\N	\N		\N				['fax']	27
53		Radzikov	RDZ	\N	\N		\N				['fax']	27
54			VJT	\N	\N		\N				['fax']	1
55	Charles	Poilly	CHP	\N	\N		\N	poilly_charles@hotmail.com		06 63 60 53 73	['fax']	32
56	Jean-Jacques	Garbay	JJG	\N	\N		\N	garbay.jj@wanadoo.fr	05 62 61 89 65	06 75 70 60 44	['fax']	33
57	François	Caizergue	FRC	\N	\N		\N	fcaizergues@yahoo.fr		06 23 20 59 75	['fax']	34
58	Marie Paule	Hernandez	MPH	\N	\N		\N	marieher@wanadoo.fr	05 58 03 37 45	06 03 98 08 42	['fax']	35
59	Jean-Marie	Coulbeaut	JMC	\N	\N		\N	jmcoulbeaut@gmail.com	05 53 81 29 82		['fax']	36
60	Alain	Parise	ALP	\N	\N		\N	parisealain@wanadoo.fr		06 21 70 23 75	['fax']	37
61	André	Bochede	ANB	\N	\N		\N	andre.bochede@wanadoo.fr	04 92 55 97 78		['fax']	38
62	Catherine	Lagache	CAL	\N	\N		\N	ecranlh@hotmail.com			['fax']	39
63	Chantal et Jean Sebastien	Gascuel	JSG	\N	\N	27/08/1956	\N	js.gascuel@63.sideral.fr, chantal63@gmail.com	04 73 24 31 15		['fax']	40
64	Gilles	Mailhe	GIM	\N	\N		\N	gilles.mailhe@laposte.net		06 30 32 36 83	['fax']	42
65	Hervé	Cournède	HEC	\N	\N		\N	herve.cournede@orange.fr	05 63 30 31 48	06 62 99 22 20	['fax']	43
66	Jacques	Baboulene	JAB	\N	\N		\N	jbaboulene@wanadoo.fr	05 53 79 28 12 	06 85 27 78 79	['fax']	44
67	Ludovic	Durif-Varambon	LUD	\N	\N		\N	marie.gaillard@live.fr	04 76 36 19 44		['fax']	45
68	Marc	Vanoversholde	MAV	\N	\N		\N	ferme.hayon@collectifs.net	32/63 57 90 80 		['fax']	46
69	Olivier	Mayor	OLM	\N	\N		\N	omv67@hotmail.com	00 4 17 97 84 19 69	022 228 1535	['fax']	47
70	Patrick	de Kochko	PAD	\N	\N		\N	patrick@semencespaysannes.org	05 53 87 27 13	06 17 06 62 60	['fax']	48
71	Raphael	Lavoyer	RAL	\N	\N		\N	r.lavoyer@voila.fr	05 53 88 80 77 (journée) 05 53 93 20 74 (soir)	06 79 51 69 01	['fax']	49
72	Richard	Heckmann	RIH	\N	\N	10/01/1961	\N	rheckmann@estvideo.fr	03 88 49 10 53	06 83 02 93 15	['fax']	50
73	Romain	Wittrisch	ROW	\N	\N		\N	romain.wittrisch@hotmail.fr		06 15 16 81 88	['fax']	51
74	Eugène	Krempp	EUK	\N	\N		\N	eugene.k@free.fr	03 80 36 60 06		['fax']	52
75	Jean François	Marot	JFM	\N	\N		\N		05 53 84 91 94 (CETAB Aiguillon)	06 81 82 96 91	['fax']	53
76	Anne	Wanner	ANW	\N	\N		\N		03 89 27 10 61 (perso) 03 89 24 43 19 (taf)	06 60 83 68 70	['fax']	54
77	Christelle	Poulaud	CRP	\N	\N		\N	tripto@laposte.net	02 97 67 48 35		['fax']	55
78	Alexandre	Hyacinthe	ALH	\N	\N		\N	ardear.semences@wanadoo.fr	04 72 41 79 22		['fax']	56
79	Guy	Kasler	GUK	\N	\N		\N				['fax']	27
80	Salvatore	Cecarrelli	SAC	\N	\N		\N				['fax']	27
81	Cindy	Gialleni	CIG	\N	\N		\N			06 47 02 93 36	['fax']	27
82	Pierre	Salanabe	PIS	\N	\N		\N				['fax']	27
83	Thomas	Glassmann	THG	\N	\N		\N	thomas.glassmann@orange.fr	03 88 70 03 65		['fax']	57
84	Arsène	Bingert	ARB	\N	\N		\N		03 88 98 84 64		['fax']	58
85	Patricia	Jung	PAJ	\N	\N		\N	projects@terra-symbiosis.org			['fax']	59
86	Stéphanie	Parizot	STP	\N	\N		\N	tilia.earl@gmail.com	03 80 35 08 48	06 83 27 79 75	['fax']	60
87			AGO	\N	\N		\N				['fax']	27
88			BRE	\N	\N		\N				['fax']	61
89	Pierre	Rivière	PIR	\N	\N		\N			06 87 13 46 98	['fax']	27
90	Mathieu	Thomas	MAT	\N	\N		\N				['fax']	27
91	Julie	Dawson	JUD	\N	\N		\N				['fax']	27
92	Guillemette	Fruchaud	GUF	\N	\N		\N				['fax']	27
93	Robin	Gasnier	ROG	\N	\N		\N	cetab@laposte.net	05 53 84 91 94	06 52 15 89 02	['fax']	27
94	Cathy	Bourffatigue	CAB	\N	\N		\N			06 14 57 34 55	['fax']	27
\.


--
-- Data for Name: eppdata_classe; Type: TABLE DATA; Schema: public; Owner: dbuser
--

COPY eppdata_classe (id, classe_name, modality_nb, classe_description) FROM stdin;
1	0-1	5	{0,0.5,1,1.5,2}
2	0-2	2	{0,0.5,1,1.5,2}
3	1-3	3	{1,2,3}
4	1-4	4	{1,2,3,4}
5	1-5	5	{1,2,3,4,5}
\.


--
-- Data for Name: eppdata_env_pra_phe_method; Type: TABLE DATA; Schema: public; Owner: dbuser
--

COPY eppdata_env_pra_phe_method (id, person_id, classe_id, method_description, method_name, unit, quali_quanti_notes, ind_global) FROM stdin;
7	89	\N	Commentaire libre des paysans 	special_remarks_farmer			\N
8	89	\N	choix multipleﾠ: faible, moyen, fort	tallage	cm	quanti	\N
9	47	\N	mesure au champ à maturité de la hauteur moyenne de la population	plant_height_mean		quali	\N
10	47	\N	note de verse, 1 : bien debout, 2: se courbe, 3: versé	verse_note	cm	quanti	\N
11	47	\N	mesure moyenne du LLSD sur une pop	LLSD_bis		quali	\N
12	47	\N	densité de végétation: 1: claire, 2:moyenne, 3:forte	leaves-density		quali	\N
13	47	\N	couleur de la population: v: type vert, vj: type vert jaune, vb:type vert bleu	spring_color_bis		quali	\N
14	47	\N	largeur des feuilles dans la population: 1:fine, 2: moyenne, 3: large	leaves-width		quali	\N
16	47	\N	comment se comporte l'épi par rapport aux maladies? Pression des maladies : faible, moyen, forte	spike-disease		quali	\N
17	47	\N	homogénéité des barbes sur l'épi, 1 tous identiques, 2 quelques differentes, 3 melange	awns-homogeneity		quali	\N
18	47	\N	taille de l'épi, G:gros, F:fin	spike-size		quali	\N
19	47	\N	densité de l'épi : l: lache, m:milache, cc : mpact	spike-density		quali	\N
20	47	\N	caractérisation globale des épis de la pop : N:Normal,b: bou fin, gt: grosse tete, gc: gros cul	spike-characterisation			\N
21	47	\N	caractérisation globale des épis de la pop : m:mutique, a:aristé, pb:pointe barbue	awns_F_bis			\N
22	89	3	à quelle occasion a été fait la relation?	occasion		quali	\N
23	89	\N	Est on sur de l'information? 1:oui et 0: non	confidence			\N
24	89	\N	Quelle est la qualité de l'échantillon? 1: OK, 0: pas OK	quality			\N
25	89	\N	commentaires libre de l'équipe de recherche	special_remarks_MLN			\N
26	89	\N	date qualitative	qualitative_date			\N
27	89	\N	date quantitative	quantitative_date		quali	\N
28	91	\N	Nombre de plantes épiées pour un jour données sur la ligne. Une plante est épiée quand la moitié de l'épi est sorti de la gaine	heading_date_individual			\N
29	91	\N	Nombre deplante sur ligne	plantes			\N
30	91	\N	version sélectionnée ou pas, pratique pour les analyses ensuite	version			\N
31	91	\N	mesure au champ à maturité de la hauteur du brin maitre d'une talle de la base de la tige au sommet de l'épi (sans les barbes) au mm prés	plant_height	mm	quanti	\N
32	91	\N	mesure  au champ à maturité de la hauteur du brin maitre de la base de la tige é la base de l'épi	plant_height_2	mm	quanti	\N
33	91	\N	mesure  au champ à maturité de la hauteur du brin maitre de la base de la tige é la base de la feuille drapeau	plant_height_3	mm	quanti	\N
34	91	\N	Last Leaf Spike Distance : (plant_height_2) ﾖ (plant_height_3)	LLSD	mm	quanti	\N
35	91	2	mesure au Moulon en salle technique é maturité, choix multiple, 0= blanc, 2=rouge	color_M		quali	\N
36	91	2	mesure au Moulon en salle technique é maturité, choix multiple, 0= pas de barbes, 2= trés barbu	awns_M		quali	\N
37	91	\N	Longueur de l'épi au Moulon en salle technique au mm prés	spike_length_M	mm	quanti	\N
38	91	2	mesure au champ é maturité, choix multiple, 0= blanc, 2=rouge	color_F		quali	\N
39	91	2	mesure au champ é maturité, choix multiple, 0= pas de barbes, 2= trés barbu	awns_F		quali	\N
40	91	\N	Nombre d'épillet par épi au Moulon dans la salle technique	nbr_spikelets		quanti	\N
41	91	\N	Nombre d'épillet stériles par épi au Moulon dans la salle technique; compté é la base et au sommet de l'épi. On ajoutte des tiers d'épillets stérils (hypothése que un épillet = 3 grains)	nbr_sterile_spikelets_b		quanti	\N
42	91	\N	Nombre d'épillets manquants au Moulon dans la salle technique, permet de corriger le poids de l'épi	nbr_missing_spikelet		quanti	\N
43	91	\N	Nombre de grains comptés au Moulon dans la salle technique sur un épi	nbr_kernels_ind		quanti	\N
44	91	\N	Poids du grain dans un épi	grain_weight	g	quanti	\N
45	91	\N	Poids de l'épi en gramme (é 0.01 gramme prés) au Moulon dans la salle technique	spike_weight	g	quanti	\N
46	91	\N	GW_Spike/KN_Spike*1000	tkw_b		quanti	\N
47	91	\N	nbr_kernels_ind_corrected * nbr_kernels_ind / grain_weight	grain_weight_corrected			\N
48	91	\N	kernels per spikelet = nbr_kernels / (nbr_spikelets ﾖ nbr_sterile_spikelets_b ﾖ nbr_missing_spikelets)	nbr_kernels_spikelets_corrected			\N
49	91	\N	nbr_kernels + nbr_spikelets_corrected * nbr_missing_spikelets	nbr_kernels_ind_corrected			\N
50	91	\N	nbr_spikelets_corrected / spike_length_MLN	spikelets_density	mm-1		\N
51	91	\N	nbr_sterile_spikelets_b / nbr_kernels_ind_corrected	spikelets_sterility_proportion			\N
52	91	\N	Distance from the top of the spike till the last_node	last_node	mm		\N
53	91	\N	plant_height_2 - last_node	bottom_spike_to_first_node	mm	quanti	\N
54	91	\N	LLSD / bottom_spike_to_first_node	leaf_prop			\N
55	91	\N	je ne sais pas, demander é Julie!	position			\N
56	91	\N	Taux de proteine, mesures faites é Clermont Ferrand avec la machine NIRS d'agri Obtention (voir avec lui pour le nom de la machine)	protein	%	quanti	\N
57	91	\N	Dureté, mesures faites é Clermont Ferrand avec la machine NIRS d'agri Obtention (voir avec lui pour le nom de la machine)	hardness		quanti	\N
58	91	\N	Poids spécifique, mesures faites é Clermont Ferrand avec la machine NIRS d'agri Obtention (voir avec lui pour le nom de la machine)	specific_weight		quanti	\N
59	91	\N	W, la force boulangére, alvéogramme de chopin, mesures faites é Clermont Ferrand avec la machine NIRS d'agri Obtention (voir avec lui pour le nom de la machine)	W		quanti	\N
60	91	\N	Temps de mixture intégrale : trés corrélé au W, mesures faites é Clermont Ferrand avec la machine NIRS d'agri Obtention (voir avec lui pour le nom de la machine)	mti		quanti	\N
61	89	\N	précédent cultural	previous_culture			\N
63	89	\N	date de castration de la femelle lors des croisements	castration_date		quali	\N
64	89	\N	libre, nom du champ oé sont les essais, demandé avec la fiche semi	sowing_notice_field_name		notes	\N
62	89	\N	date quantitative, saison	qualitative_date_b		quali	
65	89	\N	libre, date de semi demandé avec la fiche semi, de la forme jj/mm/aaaa	sowing_notice_sowing_quantitative_date			\N
66	89	\N	date de semi déduite de quantitative_date, on met automne, hiver, printemps ou été	sowing_notice_sowing_qualitative_date			\N
67	89	\N	libre, pratiques de semi	sowing_notice_sowing_practices			\N
68	89	\N	libre, reconverti en m2	sowing_notice_micro_field_area	m2	quali	\N
69	89	\N	libre, surface du champ oé sont les essais	sowing_notice_field_area	libre	quali	\N
70	89	4	choix multipleé: pente <3%, pente 3 é 10%, pente > 10%, plateau	sowing_notice_topography		quali	\N
71	89	5	choix multiple: limons, argile, argilo-calcaire, sableux, autre	sowing_notice_soil_type_1		quali	\N
72	89	3	choix multipleé: séchant, intermédiaire, hydromorphe	sowing_notice_soil_type_2		quali	\N
73	89	1	choix multiple: oui, non	sowing_notice_battance		quali	\N
74	89	1	choix multiple: oui, non	sowing_notice_drainage		quali	\N
75	89	\N	pas demandé	sowing_notice_previous_culture_category		quali	\N
76	89	\N	libre	sowing_notice_field_info		notes	\N
77	89	1	choix multipleﾠ: oui, non	sowing_notice_ploughing_before_sowing		quali	\N
78	89	\N	libre	sowing_notice_sowing_condition		notes	\N
80	89	\N	densité de semis: libre	sowing_notice_density	libre	quali	\N
81	89	1	choix multipleﾠ: lente, rapide	sowing_notice_come_up_info_1		quali	\N
82	89	1	choix multipleé: réguliére, irréguliére	sowing_notice_come_up_info_2		quali	\N
83	89	\N	libre : précédent cultural	sowing_notice_previous_culture			\N
84	89	\N	date d'observation, format jj/mm/aaaa	observation_date			\N
85	89	5	choix multiple, de 1 (moche) é 5 (magnifique)	global		quanti	\N
86	91	\N	choix multiple, de 1 (moche) é 5 (magnifique), fait par Julie	globale_jud			\N
87	89	3	Information sur les précipitations, par rapport aux normales saisonniére choix multipleé: faible, normal, excés	rainfall		quali	\N
88	89	3	Information sur la température, par rapport aux normales saisonniére choix multipleé: froides, moyennes, chaudes	temperature		quali	\N
89	89	\N	libre, accident climatique particulier	climate_accident		notes	\N
90	89	\N	libre, remarques particuliéres sur le climat	climate_notes		notes	\N
91	89	\N	libre, accident dans les champ (gibier, etc)	field_accident		notes	\N
92	91	\N	date d'épiaison de la variété, de la forme jj/mm/aaaa	heading_date_global			\N
93	91	\N	note sur la précocité de 1: précoce é 5 tardif, de la forme jj/mm/aaaa	heading_note			\N
94	91	\N	note de biomasse de 1: nul é 5: super, note faite par le paysan	biomass			\N
95	91	\N	note de biomasse de 1: nul é 5: super, note faite par Julie	biomass_jud			\N
96	64	\N	poids des gerbes récoltées	poids_gerbes	kg		\N
97	91	\N	De 0 pas malade é 5 trés malade, note réalisée par Julie dans le champ	disease			\N
98	91	\N	note de verse, 0 droit et 5 couché, fait par Julie	verse_jud			\N
99	91	\N	note d'enherbement 0 nickel, 5 plein de mauvaises herbes, fait par Julie	enherbement_jud			\N
100	91	\N	choix multiple, de 1 (homogéne) é 5 (hétérogéne), fait par Julie	heterogeneite_jud		quanti	\N
101	91	\N	port de l'épi, de 0 droit é 2 trés courbé	crosses_jud			\N
102	91	\N		dens_epi_jud			\N
103	63	\N	estimation du rendement via les micro parcelle fait par JSG	rdt_micro_parcelle_jsg			\N
105	91	\N	(kernels_weight) / (nbr_kernels) * 1000	tkw		quanti	\N
106	91	\N	Nombre d'épillet stériles par épi au Moulon dans la salle technique; comptés é la base ou au sommet (hypothése que un épillet = 3 grains)	nbr_sterile_spikelets		quanti	\N
107	91	\N	Nombre d'épi mesurés pour calculer le poids de mille grains au Moulon dans la salle technique	nbr_spikes		quanti	\N
108	91	\N	Nombre de grains comptés au Moulon dans la salle technique pour un lot de graine	nbr_kernels_global		quanti	\N
109	89	\N	Date de fécondation lors des croisements	fecondation_date			\N
110	89	5	Note de sortie d'hiver choix multiple, de 1 (moche) é 5 (magnifique)	post_winter_reprise		quanti	\N
111	89	5	choix multipleé: 0 à 100, 100 à 200, 200 à 300, 300 à 400, <400	post_winter_density	pied/m�	quali	\N
112	89	5	choix multipleé: dressé, demi-dressé,intermédiaire, demi-étalé, étalé	post_winter_port_au_tallage		quali	\N
113	89	3	choix multipleﾠ: horizontale, de biais, verticale	post_winter_leaves_attitude		quali	\N
114	89	5	choix multiple, de 1 (nul) é 5 (top)	vigueur		quanti	\N
115	89	3	choix multipleé: vert clair, vert, vert foncé	spring_color		quali	\N
116	89	\N	libre, nombre et type d'adventices demandés	self_propagating		quali	\N
117	89	\N	On demande une observation avec le pourcentage de plante épiées aujourd'hui, qui ont épiées il y a 3 jours ou plus, avant hier, hier, qui vont épier demain, aprés demain, dans 3 jours ou plus. 3 passages sont possible, on demande la date du passage. Un épi est épié quand la moitié de l'épi sort de la gaine. Les résultats sont en pourcentage d'individus de la population	heading		quali	\N
118	89	5	choix multiple, de 1 (moche) é 5 (magnifique)	summer_biomass		quanti	\N
119	89	\N	pourcentage de plante crossées (crosse), intermediaire (crosses_intermediaire) et pas crossé (pas_crosses) dans la micro parcelle	crosses		quanti	\N
120	89	\N	pourcentage de plante blanche (white_color), entre blanc et rouge (color_intermediaire) et rouge (red_color) dans la micro parcelle	summer_color		quanti	\N
121	89	\N	choix multiple, de 1 (homogéne) é 5 (hétérogéne)	heterogeneite		quanti	\N
122	89	\N	pourcentage de plante barbues (awns), intermediaire (awns_intermediaire) ou non barbu (no_awns) dans la micro parcelle	awns_fiche		quanti	\N
123	89	\N	pourcentage de verse dans la micro parcelle é la récolte	summer_verse		quanti	\N
124	89	\N	poids des grains récoltés sur la micro parcelle	poids_battage			\N
125	89	\N	remraque associée é la récolte des grains sur la micro parcelle	remarques poids_battage			\N
126	89	\N	remarque sur la faéon de sélectionner	selection_description		notes	\N
127	91	2	mesure au champ é maturité, choix multiple, 0= pas de courbe, 2= trés courbé	curve_F		quali	\N
79	89	1	choix multipleé: volée, semoir	sowing_notice_sowing_practices_b		quali	
128	91	\N	(plant_height) ﾖ (plant_height_2)	spike_length_F	mm	quanti	\N
129	91	2	mesure au Moulon en salle technique é maturité, choix multiple, 0= pas de courbe, 2= trés courbé	curve_M		quali	\N
130	91	\N	spike_length_F ﾖ spike_length_M pour voir et corriger les grosses erreurs entre les mesures au champ et au Moulon	slf-slm		quanti	\N
131	91	\N	Nombre de grains qui reste dans le sac compté au Moulon dans la salle technique	bulk		quanti	\N
132	91	\N	Nombre d'épi qui ont été récolté pour l'échantillon au Moulon dans la salle technique	total_nbr_spikes		quanti	\N
133	91	\N	poids total de l'échantillon récolté, i.e. le stock	total_weight		quanti	\N
134	91	\N	Poids de touts les grains venant de tous les épis qui ont été mesurés au Moulon dans la salle technique é 0.1 gramme prés	measured_grain_weight	g	quanti	\N
135	63	\N	poids des grains récoltés sur la microparcelle extrapolé en rendement par JSG	rdt_micro_parcelle	kg	quanti	\N
136	92	\N	Nﾰ parcelle (cas particulier OLR) 	n_parcelle			\N
137	89	4	choix multipleé: pente <3%, pente 3 é 10%, pente > 10%, plateau, plaine	sowing_notice_topography_2		quali	\N
138	89	\N	espace entre les micro parcelle, unité libre	space_between_micro_field		quanti	\N
139	92	\N	nombre de plantes levées pour 2 métres linéaires ( 1m sur chacun des 2 rangs du milieu)	post_winter_density_a		quanti	\N
140	92	\N	nombre de plantes levées pour 1métre linéaire (densité du deuxiéme rang)	post_winter_density_b		quanti	\N
141	92	\N	nombre de plantes levées pour 1métre linéaire (densité du 3éme rang)	post_winter_density_c		quanti	\N
142	89	\N	date qui correspond à une pression de maladie : sain, malade, très malade	disease_a			\N
143	89	\N	date qui correspond à une proportion d'épis épiées dans une variétés: 0%; 0-30%; 30-60%; 60-100%; 100%	heading_date_global_a			\N
144	89	\N	récolte à la main ou à la machine	harvest_type		quali	\N
145	89	\N	libre	harvest_condition		notes	\N
146	89	\N	D'oé vient l'information, comment a t elle été récupérée?	sent_source			\N
147	89	\N	plan de semis : sowing_X correspond é la colonne, ce sont des lettres, A, B, C, ..., Z	plan_coordinates_X		quali	\N
148	89	\N	plan de semis : sowing_Y correspond é la ligne, ce sont des chiffres, 1, 2, 3, ..., 30	plan_coordinates_Y		quali	\N
104	91	\N	courbe des épi, de 0 droit é 2 totalement courbé (paralélle é l'épi)	crosses_b			
15	47	\N	comment se comporte la population par rapport aux maladies? Pression des maladies : faible, moyen, forte	disease_b		quali	
\.


--
-- PostgreSQL database dump complete
--

