#!/usr/bin/python
#encoding:utf-8


import os
import sys
import csv
ENVIRONMENT_VARIABLE = "DJANGO_SETTINGS_MODULE"
apppath = os.getcwd()+"/../../"

#sys.path.append(apppath)


from eppdata.models import Method, Classe
from actors.models import Person




methods = csv.reader(open(apppath+'../Data/Project Data/tableau_base_perenne/methods.csv', 'rb'), delimiter='\t', quotechar='"')

header = methods.next()

method_name_pos = header.index("method_name")
person_pos = header.index("person")
desc_pos = header.index("method_description")
classe_pos = header.index("classe")
unit_pos = header.index("unit")
notes_pos = header.index("quali_quanti_notes")

for m in methods:
    if m[method_name_pos] != '' and m[person_pos] != '' :
        p = Person.objects.get(short_name=m[person_pos])
        c = None
        if m[classe_pos] != '' :
            c = Classe.objects.get(classe_name=m[classe_pos])
        Method.objects.create(person=p,
                                          method_name=m[method_name_pos],
                                          classe=c,
                                          unit=m[unit_pos],
                                          quali_quanti_notes=m[notes_pos],
                                          method_description=m[desc_pos]
                                          )
