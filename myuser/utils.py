# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""


import copy
import datetime

from django.utils.translation import gettext_lazy as _

from entities.models import Seedlot, Germplasm
from network.models import Diffusion, Relation, Reproduction, ReproductionMethod, Selection
from eppdata.models import Rawdata  
from myuser.exceptions import NameErrorException  
   
  
def create_digit(row):
    """DEPRECATED : use the static method Seedlot.build_seedlot_name() instead"""
    """does the seed lot with digit exist?no:error,yes use it"""       
    myset = None
    if len(row.split('_')) == 4:
        #seedlot = Seedlot.objects.get(name=row)
        return row    
    if len(row.split('_')) == 3:        
        myset = Seedlot.objects.filter(name__startswith=row).values_list('name',flat=True).order_by('-name')
    elif len(row.split('_')) == 1:
        myset = Germplasm.objects.filter(idgermplasm__startswith=row).values_list('idgermplasm',flat=True).order_by('-idgermplasm')
    if myset:
        last_digit = ""
        last_digit = myset[0].split('_')[-1]
        last_digit =  int(last_digit)+1
        return "%s_%.4d"%(row,last_digit)
    else:
        last_digit = 1
        result = row
        return "%s_%.4d"%(result,last_digit)


def create_digit_gpnotcrossed():
    gpset = Germplasm.objects.filter(idgermplasm__startswith='NOT-CROSSED').order_by('-idgermplasm')
    if gpset:
        last_digit = ""
        last_digit = gpset[0].idgermplasm.split('_')[-1][-4:]
        last_digit =  int(last_digit)+1
        return "NOT-CROSSED%.4d"%last_digit
    else:
        last_digit = 1
        return "NOT-CROSSED%.4d"%last_digit


def check_errors(error_messages) :
    for type_message in error_messages.keys():
        if len(error_messages[type_message]) > 0 :
            #print(error_messages[type_message])
            raise Exception("There was some errors in the file")

    
def check_seedlot_name(row):
    if len(row.split('_')) == 3:
        last_digit = 1
        return "%s_%.4d"%(row,last_digit)
    elif len(row.split('_')) == 4:
        last_digit = int(row.rsplit('_',1)[-1])+1
        result = row.rsplit('_',1)[0]
        return "%s_%.4d"%(result,last_digit)
    
    
def create_germplasm(row, person_obj, species):    
    if is_valid_idgermplasm(row) :
        germplasm = row
    if len(germplasm.split('_')) == 3 or len(germplasm.split('_')) == 4 :
            germplasm_full = row.split('_')[0]
            germplasm = germplasm_full.split('#')[0] 
    
       
    (germplasm_concerned, created) = Germplasm.objects.get_or_create(idgermplasm = germplasm,
                                                                     species = species,
                                                      defaults={'person' : person_obj, 

                                                                                                                                        
                                                                }
                                                      ) 
    
    return (germplasm_concerned, created)


def create_germplasm_catalogue(row):    
    
    (germplasm_concerned, created) = Germplasm.objects.get_or_create(idgermplasm =row) 
    
    return (germplasm_concerned, created)

def is_valid_idgermplasm(row):
    if len(row.split('_')) == 3 or len(row.split('_')) == 4 :
            germplasm_full = row.split('_')[0]
            germplasm = germplasm_full.split('#')[0]
            row = germplasm.encode('utf-8') 
    
    char_list=['#',' ','_',',',';','"']
    for char in char_list:
        try:
            row.index(char)
        except:
            pass
        else :
            raise NameErrorException(_("The germplasm name %s is not a valid name (should not contain space ( ), diez (#), coma (,), semicolon (;) or underscore (_))")%row)
            return False
    return True


def create_seed_lot(seed_lot_fields_dict,
                    location,
                    germplasm_obj,
                    seed_lot_name):
     
    try:
        date = int(seed_lot_fields_dict['date'])
    except :
        if seed_lot_fields_dict['date'] is not None :
            raise Exception("{0} is an invalid date format".format(seed_lot_fields_dict['date']))
        else :
            date = None
        
    (seed_lot_created,create) = Seedlot.objects.get_or_create(name = seed_lot_name,
                                            defaults={
                                            'location':location,
                                            'germplasm':germplasm_obj,
                                            'quantity_ini':seed_lot_fields_dict['quantity_ini'],
                                            'date' : date ,}
                                            )
    return(seed_lot_created,create)

def update_sequence(self, cursor, table, column='id'):
    sql = "SELECT setval('%s_%s_seq', (SELECT MAX(id) FROM %s)+1)" % (table, column, table)
    cursor.execute(sql)

def create_seed_lot2(seed_lot_fields_dict,
                    person,
                    query,
                    obj,
                    seed_lot_name):
    
    (seed_lot_created,create) = Seedlot.objects.create(
                                            id=obj,
                                            name = seed_lot_name,
                                            person=person,
                                            germplasm_id=query,
                                            quantity_ini=None,
                                            date = None)
    return(seed_lot_created,create)                                      

def variable_warnings(line,variable_obj,method_obj):

    #warning =  '<tr><td>'+str(line).decode('utf-8')+'</td><td>'+variable_obj.name.decode('utf-8')+'</td><td>'+method_obj.method_name.decode('utf-8')+'</td></tr>'
    #warning =  "<tr><td>"+str(line)+"</td><td>"+variable_obj.name+"</td><td>"+method_obj.method_name+"</td></tr>"
    warning =  "<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>".format(str(line), variable_obj.name, method_obj.method_name)
    return warning # ligne précédente rajout de decode('utf-8') pour permettre upload du fichier 2007-2008_1a.reproduction.csv et pour champs year de interface de requete

    #warning =  "<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>".format(str(line), variable_obj.name, method_obj.method_name) -> python 3.0
    #return warning


def germplasm_warnings(obj,gender,line,menu_list,species):
    warning = '<tr><td>%s</td><td>%s</td><td>%s</td><td><select name="germplasm_type_%s_%s" id="germplasm_type"> %s</select></td></tr>'%(line,gender,obj.idgermplasm,obj.idgermplasm,species,menu_list)    
    #warning = '<tr><td>'+str(line)+'</td><td>'+gender+'</td><td>'+obj.idgermplasm+'</td><td><select name="germplasm_type_'+obj.idgermplasm+'"> '+menu_list+'</select></td></tr>'
    return warning

def seed_lot_warnings(obj, gender, weight,line):
    warnings = _('line {line}: ({gender}) {name} ({weight} g)').format(line=line, gender=gender, name=obj, weight=weight)+'.<br>'
    return warnings

def dateData(date):
    convertdate = datetime.datetime.strptime(date, '%Y-%m-%d')
    return convertdate

def create_cross(cross_reproduction_dict, female_obj, male_obj, son_obj):
    
    """reproduction_method table"""
    (method_reproduction,create) = ReproductionMethod.objects.get_or_create(reproduction_methode_name = 'cross')
                                                                    
    """ reproduction"""
    
    if cross_reproduction_dict['cross_number']==0:
        is_realised='N'
    elif cross_reproduction_dict['cross_number']== '':
        is_realised='X'
    else:
        is_realised='Y'

#     try :
#         datecross = str(int(cross_reproduction_dict['date'])-1)+"-"+cross_reproduction_dict['date']
#     except :
#         datecross = cross_reproduction_dict['date']

    if 'kernel_number' in cross_reproduction_dict.keys() and cross_reproduction_dict['kernel_number'] != '' \
        and 'cross_number' in cross_reproduction_dict.keys() and cross_reproduction_dict['cross_number'] != '' :
        reproduction_relation = Reproduction.objects.create(reproduction_method = method_reproduction,
                kernel_number = int(cross_reproduction_dict['kernel_number']),
                cross_number = int(cross_reproduction_dict['cross_number']),
                #date = datecross,
                start_date = int(cross_reproduction_dict['start_date']),
                end_date = int(cross_reproduction_dict['end_date']),
                realised = is_realised
                )
    else :
        reproduction_relation = Reproduction.objects.create(
                reproduction_method = method_reproduction,
                #date = datecross,                           
                start_date = int(cross_reproduction_dict['start_date']),
                end_date = int(cross_reproduction_dict['end_date']),
                realised = is_realised
                )

    (female_relation_cross, create) = Relation.objects.get_or_create(
                                      seed_lot_father = female_obj,
                                      seed_lot_son = son_obj,
                                      defaults={
                                      'reproduction' : reproduction_relation,
                                      'split' : cross_reproduction_dict['female_split'],
                                      'quantity': cross_reproduction_dict['female_quantity'],
                                      'is_male': 'F',
                                      #'block': int(cross_reproduction_dict['female_block']),
                                      'block': cross_reproduction_dict['female_block'],
                                      'X': cross_reproduction_dict['female_X'],
                                      'Y': cross_reproduction_dict['female_Y']
                                      }
                                      )
    
    (male_relation_cross, create) = Relation.objects.get_or_create(
                                      seed_lot_father = male_obj,
                                      seed_lot_son = son_obj,
                                      defaults={
                                      'reproduction' : reproduction_relation,
                                      'split' : cross_reproduction_dict['male_split'],
                                      'quantity': cross_reproduction_dict['male_quantity'],
                                      'is_male': 'M',
                                      'block': cross_reproduction_dict['male_block'],
                                      #'block': int(cross_reproduction_dict['male_block']),
                                      'X': cross_reproduction_dict['male_X'],
                                      'Y': cross_reproduction_dict['male_Y']
                                      }
                                      )
        
    return  (female_relation_cross, male_relation_cross, create)

def create_diffusion (seed_lot_obj, diff_seed_lot_obj, diffusion_dict):
    X='X'
    diffusion_relation = Diffusion.objects.create(
                                         date = int(diffusion_dict['date']),
                                         )
    if 'quantity' in diffusion_dict.keys() and diffusion_dict['quantity'] != '':
        defs = {
                                           'diffusion' : diffusion_relation,
                                           'is_male' : X,
                                           'split' : diffusion_dict['split'],
                                           'quantity': diffusion_dict['quantity']
                                           }
    else :
        defs = {
                                           'diffusion' : diffusion_relation,
                                           'is_male' : X,
                                           'split' : diffusion_dict['split'],
                                           }    

    (relation,create) = Relation.objects.get_or_create(
                                           seed_lot_father = seed_lot_obj,
                                           seed_lot_son = diff_seed_lot_obj,
                                           quantity=diffusion_dict['quantity'],
                                           defaults=defs)
    return (relation,create)

def check_variable_methods(file_list, variable_method_dict):
    #TODELETE
    error_file_list = []
#     print(file_list)
#     print(variable_method_dict)
    efl = 0
    for y in file_list:
        if y not in variable_method_dict.keys():
            efl += 1
            error_file_list.append('%s.%s<br>'%(efl, y))
    if error_file_list != [] :
        error_file_list.append('<b>Total %s</b><br><br>' % efl)
    
    return error_file_list


def check_variable_date(var_date_list,var_list):
    error_var_date_list=list(var_date_list) 
    for var_date in var_date_list:
        var_chaine=""
        i=2
        while var_date[i] != "$":
            var_chaine=var_chaine+var_date[i]
            i=i+1
        for var_date2 in var_list:
            if var_date2.find(var_chaine)!=-1: # on regarde si la variable de 'variable$date' existe dans la liste des variables
                error_var_date_list.remove(var_date) # si c'est le cas, on la supprime de la liste
                break # 
    return error_var_date_list # on retourne la liste des dates qui ne sont pas liées à une variable existante



def create_mixture (seed_lot_obj,mixture_dict,son_obj,mixture_relation):
    X = 'X'
    try :
        son_obj.quantity_ini = son_obj.quantity_ini+mixture_dict['mixture_quantity']
        son_obj.save()
    except :
        mixture_dict['mixture_quantity'] = None
        
    """il faudrait creer dans la table mixture"""
    if 'split' in mixture_dict.keys() and mixture_dict['split'] != '':
        defs = {
                                                  'mixture' : mixture_relation,
                                                  'quantity' : mixture_dict['mixture_quantity'],
                                                  'is_male' : X,
                                                  'split' : mixture_dict['split']
                                                  }
    else :
        defs = {
                                              'mixture' : mixture_relation,
                                              'quantity' : mixture_dict['mixture_quantity'],
                                              'is_male' : X,
                                              }
    (relation,create) = Relation.objects.get_or_create(
                                                  seed_lot_father = seed_lot_obj,
                                                  seed_lot_son = son_obj,
                                                  defaults=defs)
    return (relation,create)

def create_raw_data_catalogue(variable_concerned,method_concerned,row,date_raw_data, germpl, annee):
    data  = Raw_data.objects.create(variable = variable_concerned,
                                            method = method_concerned,
                                            raw_data = row,
                                            date = date_raw_data,
                                            an = annee
                                            )
    data.save()
    data.germplasm.add(germpl)
  
    return data

    
def create_raw_data(variable_concerned, method_concerned,row, date_raw_data, logged_user):
    data  = Rawdata.objects.create(variable = variable_concerned,
                                            method = method_concerned,
                                            raw_data = row,
                                            date = date_raw_data,
                                            last_modification_user = logged_user
                                            )
  
    return data


def create_reproduction(father_obj,son_obj,reproduction_dict):
    X='X'
    block_x = None
    block_y = None
    quantity_sown = None
    
    
    ##Ici gérer les messages d'erreur si X et Y ne sont pas des chiffres !
    if 'X' in reproduction_dict.keys() and reproduction_dict['X'] != '' and reproduction_dict['X'] != None :
        block_x = reproduction_dict['X']
    if 'Y' in reproduction_dict.keys() and reproduction_dict['Y'] != '' and reproduction_dict['Y'] != None :
        block_y = reproduction_dict['Y']
    
    try :
        end_date = int(reproduction_dict['end_date'])
    except :
        end_date = None
    print(end_date)
    reproduction_relation = Reproduction.objects.create(
                                                        description='reproduction',
                                                        start_date = int(reproduction_dict['start_date']),
                                                        end_date = end_date
                                                        )
    
    relation = Relation.objects.create(seed_lot_father = father_obj,
                                           seed_lot_son = son_obj,
                                           reproduction = reproduction_relation,
                                           is_male = X,
                                           #block = int(reproduction_dict['block']),
                                           block = reproduction_dict['block'],
                                           X = block_x,
                                           Y = block_y,
                                           quantity = reproduction_dict['quantity'],
                                           split = reproduction_dict['split']
                                           )

    return relation
    

def find_date(row):
    try :
        date = int(row.split('_')[2])
        return date
    except :
        if row.split('_')[2] == "XXXX" :
            return None
        else :
            raise Exception("Unvalid date format")
     
def create_selected(row_seed,selection_name,year):
      
    row = row_seed.split('_')[0]
    row = row.split('#')[0]
    row = row +'#'+ selection_name+'_'+row_seed.split('_')[1]+'_'+ year
    return row
        
def create_selection(father_obj,selected_obj,selection_dict,row_name,person_selection,reproduction_obj):
#    (selection_relation,create) = Selection.objects.get_or_create(person = person_selection,
#                                                                  selection_name = row_name,
#                                                                  date = reproduction_dict['date'])
    X='X'
    block_x = None
    block_y = None
    
    ##Ici gérer les messages d'erreur si X et Y ne sont pas des chiffres !
    if 'X' in selection_dict.keys() and selection_dict['X'] != '' and selection_dict['X'] != None :
        block_x = selection_dict['X']
    if 'Y' in selection_dict.keys() and selection_dict['Y'] != '' and selection_dict['Y'] != None :
        block_y = selection_dict['Y']
            
    selection_relation = Selection.objects.create(person = person_selection,
                                                  selection_name = row_name,
                                                  date = int(selection_dict['date']))
            
    (relation,create) = Relation.objects.get_or_create(
                                           reproduction = reproduction_obj,
                                           seed_lot_father = father_obj,
                                           seed_lot_son = selected_obj,
                                           selection = selection_relation,
                                           block=selection_dict['block'],
                                           #block=int(selection_dict['block']),
                                           quantity=selection_dict['quantity'],
                                           X=block_x,
                                           Y=block_y,
                                           defaults={
                                           'is_male' : 'X',}
                                           )
    return (relation,create)

def get_variable_list(filefields, fields):
    #TODELETE
    file_list = copy.copy(filefields)
    for l in range(len(fields)) :
        if fields[l] in file_list:
            file_list.remove(fields[l])
        if '' in file_list:
            file_list.remove('')
        
    #return list(map(recode,file_list))
    return file_list



def is_already_merged(list_to_merge):
    
    n_mixture = 0
    for tup in list_to_merge :

        r = Relation.objects.filter(seed_lot_father = tup[3]).exclude(mixture = None)
        if r :
            n_mixture += 1 
    
    if n_mixture > 1:
        return True
    else :
        return False

def recode(string):
    encoding = ['ascii', 'utf-8-sig', 'utf-8','iso-8859-1','utf-16', ]
    for e in encoding :
        try :
            return string.decode(e)
        except :
            pass
    raise UnicodeDecodeError("%s can't be decoded"%string)


def get_quantity(quantity_str):
    quantity = None
    msg = None
    if  quantity_str == '' :
        quantity = None
    else:
        try:   
            quantity = float(quantity_str.replace(',','.'))
        except ValueError:
            msg = _("QUANTITY_HARVESTED doesn't correspond to a numerical format.")
    return quantity, msg
    

