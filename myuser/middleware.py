# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.conf import settings
from django.http import HttpResponseRedirect
from django.contrib.auth import login

class RequireLoginMiddleware(object):
    """ Come from http://www.djangosnippets.org/snippets/136/

    Require Login middleware. If enabled, each Django-powered page will
    require authentication.

    If an anonymous user requests a page, he/she is redirected to the login
    page set by REQUIRE_LOGIN_PATH or /accounts/login/ by default.
    """
    def __init__(self):
        self.require_login_path = "%s%s"% (settings.ROOT_URL, getattr(settings, 'REQUIRE_LOGIN_PATH', '/login/'))

    def process_request(self, request):
        if request.user.is_anonymous() \
            and not request.path.startswith(settings.ROOT_URL+'/media/') \
            and not request.path.startswith(settings.ROOT_URL+'/admin/') \
            and not request.path.startswith(settings.ROOT_URL+'/media_admin/') \
            and not request.path.startswith(settings.ROOT_URL+'/favicon.ico') \
            and request.path != self.require_login_path:
            if request.POST:
                return login(request, request.user)
            elif request.GET:
                return HttpResponseRedirect('%s?next=%s' % (self.require_login_path, request.path))
    
    def process_exception(self, request, exception):
        print(exception)
