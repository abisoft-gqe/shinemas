# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""


from django.conf.urls import url
from django.urls import path
from django.views.generic import TemplateView
#from django.views.generic.simple import direct_to_template

from myuser.views import login_view, logout_view, get_token, uploaded_data_view, acceuil, uploaded_catalogue_view


urlpatterns = [
                path('', acceuil, name='home'),
                path('login/', login_view, name='login'),
                path('logout/',  logout_view , name='logout'), 
                path('token/',  get_token , name='token'), 
                path('upload/', uploaded_data_view, name='upload'),
                path('uploaded_data/', uploaded_data_view, name='uploaded_data'),
                path('download/', TemplateView.as_view(template_name='myuser/download.html'), name='download')

]
