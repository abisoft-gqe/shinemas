# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""


"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.contrib.auth.models import User
from django.test import Client

from myuser.utils import create_selected

class UserTest(TestCase):
    def setUp(self):
        User.objects.create(username='yannick',password='yannick')
    
    def test_authentication(self):
        c = Client()
        response = c.post('/login/', {'username': 'yannick', 'password': 'yannick'})
        self.assertEqual(response.status_code, 200)

class SimpleTest(TestCase):
    def test_create_selected(self):
        seedlot = "C21_MLN_2006"
        selection_name = "toto"
        year = "2007"
        
        self.assertEqual(create_selected(seedlot,selection_name,year),"C21#toto_MLN_2007")
        
    def test2(self):
        seedlot = "C21#S_MLN_2006"
        selection_name = "toto"
        year = "2007"
        self.assertEqual(create_selected(seedlot,selection_name,year),"C21#toto_MLN_2007")
        self.assertNotEqual(create_selected(seedlot,selection_name,year),"C21#S_MLN_2007")
