# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.utils.translation import gettext_lazy as _

DIFFUSION = "diffusion"
CROSS = "cross"
MIXTURE = "mixture"
REPRODUCTION = "reproduction"
SELECTION = "intra_varietal"
PERSON = _("person")
SEEDLOT = "seedlot"

STORAGE = "storage"
RUNING_REPRO = _("runing_reproduction")

BASIC_TYPES = [_('Cross'), _('Mixture')]

"""cross file"""
PROJECT = _("project")
SOWN_YEAR = _("sown_year")
HARVESTED_YEAR = _("harvested_year")
QUANTITY_HARVESTED = _("quantity_harvested")
CROSS_YEAR = _("cross_year")
CROSS_GERMPLASM = _("cross_germplasm")
NUMBER_CROSSES = _("number_crosses")
KERNEL_NUMBER_F1 = _("kernel_number_F1")

MALE_SEED_LOT = _("male_seed_lot")
MALE_ETIQUETTE = _("male_etiquette")
MALE_SPLIT = _("male_split")
MALE_QUANTITY = _("male_quantity")
MALE_BLOCK = _("male_block")
MALE_X = _("male_X")
MALE_Y = _("male_Y")


FEMALE_SEED_LOT = _("female_seed_lot")
FEMALE_ETIQUETTE = _("female_etiquette")
FEMALE_SPLIT = _("female_split")
FEMALE_QUANTITY = _("female_quantity")
FEMALE_BLOCK = _("female_block")
FEMALE_X = _("female_X")
FEMALE_Y = _("female_Y")







""" method file """

METHOD_NAME = _("method_name")
TYPE = _("type")
VARIABLE = _("variable")
GROUP = _("group")




"""diffusion file constants"""

ID_SEED_LOT = _("id_seed_lot")
QUANTITY = _("quantity")
LOCATION = _("location")
EVENT_YEAR = _("event_year")
SPLIT = _("split")


"""mixture file constants"""
ID_SEED_LOT = _("id_seed_lot")
QUANTITY = _("quantity")
GERMPLASM = _("germplasm")
EVENT_YEAR = _("event_year")
SPLIT = _("split")


"""reproduction file"""
MULTIPLICATION = _("multiplication")
PROJECT = _("project")
HARVESTED_YEAR = _("harvested_year")
ID_SEED_LOT_SOWN = _("id_seed_lot_sown")
INTRA_SELECTION_NAME = _("intra_selection_name")
ETIQUETTE = _("etiquette")
SPLIT = _("split")
QUANTITY_SOWN = _("quantity_sown")
QUANTITY_HARVESTED = _("quantity_harvested")
BLOCK = _("block")
X = _("X")
Y = _("Y")


"""selection file"""
VARIETAL_SELECTION = _("varietal_selection")
SELECTION_NAME = _("selection_name")
SELECTION_PERSON = _("selection_person")
SELECTION_QUANTITY_INI = _("selection_quantity_ini")
INTRA_VARIETAL = _("intra_varietal")

"""phenotypic file """
PHENOTYPIC = _("individual_data")
ID_SEED_LOT_SOWN = _("id_seed_lot_sown")
SOWN_ETIQUETTE = _("sown_etiquette")
ID_SEED_LOT_HARVESTED = _("id_seed_lot_harvested")
INTRA_SELECTION_NAME = _("intra_selection_name")
HARVESTED_ETIQUETTE = _("harvested_etiquette")
HARVESTED_YEAR = _("harvested_year")
INDIVIDUALS = _("individuals")
BLOCK = _("block")
X = _("X")
Y = _("Y")
GROUP = _("group")
CORRELATION_GROUP = _("correlation_group")

FORMAT_VALIDATOR = {
                    LOCATION: DIFFUSION,
                    CROSS_YEAR: CROSS,
                    GERMPLASM: MIXTURE,
                    QUANTITY_SOWN: REPRODUCTION,
                    SELECTION_NAME: SELECTION,
                    INDIVIDUALS: PHENOTYPIC
                    }