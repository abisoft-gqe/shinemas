# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""


from __future__ import unicode_literals, division
import csv
import datetime
import io
import traceback
import copy

from rest_framework.authtoken.models import Token

from django.conf import settings
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.db import transaction
from django.db.models import Count, Max
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.contrib.auth.decorators import user_passes_test, login_required
from django.utils.translation import gettext_lazy as _
from django.forms import ValidationError

from .cross_file_constants import METHOD_NAME, VARIABLE, TYPE, PHENOTYPIC, CORRELATION_GROUP
from .cross_file_constants import CROSS, PROJECT, SOWN_YEAR, HARVESTED_YEAR, QUANTITY_HARVESTED, CROSS_YEAR, CROSS_GERMPLASM, NUMBER_CROSSES, KERNEL_NUMBER_F1, MALE_SEED_LOT, MALE_ETIQUETTE, MALE_SPLIT, MALE_QUANTITY, MALE_BLOCK, MALE_X, MALE_Y
from .cross_file_constants import FEMALE_SEED_LOT, FEMALE_ETIQUETTE, FEMALE_SPLIT, FEMALE_QUANTITY, FEMALE_BLOCK, FEMALE_X, FEMALE_Y, ID_SEED_LOT_SOWN
from .cross_file_constants import LOCATION, ID_SEED_LOT, ETIQUETTE, EVENT_YEAR, SPLIT, QUANTITY, DIFFUSION, MIXTURE, GERMPLASM, REPRODUCTION, SELECTION_NAME, QUANTITY_SOWN, BLOCK, X, Y
from .cross_file_constants import SELECTION_PERSON, SELECTION_QUANTITY_INI, INTRA_VARIETAL, INDIVIDUALS, INTRA_SELECTION_NAME  # , ID_SEED_LOT_HARVESTED

from .utils import variable_warnings, check_variable_methods, create_germplasm, germplasm_warnings, create_seed_lot, seed_lot_warnings
from .utils import recode, get_variable_list, create_digit, find_date, create_digit_gpnotcrossed, create_raw_data
from .utils import create_cross, create_diffusion, create_mixture, create_reproduction, create_selection, is_already_merged, check_errors
from .utils import check_variable_date, get_quantity

from eppdata.models import Method, Variable
from entities.models import GermplasmType, Seedlot, Species
from network.models import ReproductionMethod, Mixture, Relation, Reproduction
from actors.models import Person, Project, Location
from entities.forms import SubmitSpecies, CatalogueForm
from myuser.exceptions import NameErrorException

@login_required
def acceuil(request):
    return render(request, 'myuser/acceuil.html', {"species" : Species.objects.all()})

 
def logout_view(request):
    logout(request)
    message = _("You are now logout")
    return render(request, 'myuser/login.html', {'message': message})  
    
    
    
def get_token(request):
    user = request.user
    if request.method == "GET" :

        if user is not None and user.is_active :
            try :
                token = Token.objects.get(user=user)
            except Token.DoesNotExist:
                return render(request, 'myuser/token.html', {})
            else :
                expiration_date = token.created + datetime.timedelta(days=settings.AUTH_TOKEN_LIFE)
                has_expired = datetime.datetime.now() > expiration_date

            #print('token : '+str(token))
            return render(request, 'myuser/token.html', {'token': token, 'expired':has_expired})
        else :
            return render(request, 'myuser/token.html', {})
    elif request.method == "POST" :
        try :
            token = Token.objects.get(user=user)
        except Token.DoesNotExist:
            token = Token.objects.create(user=user)
        else :
            token.delete()
            token = Token.objects.create(user=user)
        return render(request, 'myuser/token.html', {'token':token})
    return render(request, 'myuser/token.html', {})
    
    
    
def login_view(request):
    
    if request.method == "POST" :
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)    
        if user is not None:
            if user.is_active:
                login(request, user)
                message_log = _('You are now log in')
                if 'next' in request.session:
                    return HttpResponseRedirect(request.session['next'])  
                else :
                    return render(request, 'myuser/acceuil.html', {'message_log': message_log, 'user':user})  # rediriger ailleurs 
            else:
                message = _("You're account is not active")
                return render(request, 'myuser/acceuil.html', {'message': message})
        else:
            error_message = _("Your login or your password is incorrect")
            return render(request, 'myuser/login.html', {'message':error_message, 'user':user})
    else :
        if request.user.is_authenticated :
            return render(request, 'myuser/acceuil.html', {})
        else :
            if 'next' in request.GET:
                request.session['next'] = request.GET['next']
            return render(request, 'myuser/login.html')

@user_passes_test(lambda u: u.is_superuser)
def uploaded_catalogue_view(request):
    """Copier coller par Aichatou --> tout refaire"""
    form = CatalogueForm()
    return render(request, 'myuser/load_catalogue.html', {'form':form})
    
    
    """
    1/ We retrieve the uploaded file .
    2/ We read the uploaded file in a dictionnary named row
    """
#     germ_type_radio = []
#     line = 1
# 
#     form = CatalogueForm()
#     
#     # ## gestion des messages d'erreurs
#     error_messages = { "idgermplasm_error":[], "mixture_empty":[], "variable_error":[], "method_error":[],
#                        "person_error":[],  "error_block":[], "error_project":[],
#                       "error_message":[], "split_string":[],
#                      
#                       }
#     
#     warning_messages = { "warnings_species":[], "son_warnings_seed":[], "warnings_relation":[], "male_relation":[],
#                         "female_relation":[], "warnings_not_quantity":[], "warnings_germ":[], "created_person":[],
#                          "warnings_var":[], "reproduction_type":[]}
#     
#     
#     """annee = 0"""
#     is_submit = request.POST.get('submit', False);
#     if is_submit :
#                         
#             if form.is_valid():
#                 try: 
#                     annee = request.POST.get('catYear','')
#                 except:
#                     error_messages["error_message"].append(_('No year selected.') + '<br>')
#                 else:
#                     """an = int(annee)"""
#                     Raw_data.objects.get_or_create(an = annee)
# 
#                
#             try:
#                 file_name = request.FILES['file']
#             except:
#                 error_messages["error_message"].append(_('No file submitted.') + '<br>')
#                 file_name = _('No input file selected')
#                 return render(request, 'myuser/error_catalogue_load.html', {'error_message': error_messages["error_message"], 'form':form, 'file_name':file_name})
#             else:
#                     
#                     try:
#                         second_file_name = request.FILES['second_file']
#                         
#                     except:
#                         error_messages["error_message"].append(_("You selected the {type} button but the file you submitted doesn't correspond to a {format} file format.") + "<br>")
#                         return render(request, 'myuser/error_catalogue_load.html', {'error_message': error_messages["error_message"], 'form':form, 'file_name':file_name})
#                     else:
#                         second_file = csv.DictReader(second_file_name, delimiter=str("\t"), quotechar=str('"'))
#                         variable_method_dict = {}
#                         """print second_file.fieldnames"""
#                      
#                         
#                         for row in second_file:
#                             
#                             line = line + 1 
# 
#                            
#                             if METHOD_NAME not in second_file.fieldnames or VARIABLE not in second_file.fieldnames:
#                                 error_messages["error_message"].append(_('The second file is not a method file.') + '<br>')
#                                 return render(request, 'myuser/error_catalogue_load.html', {'error_message': error_messages["error_message"], 'form':form, 'file_name':file_name})
#                             else:
#                                 
#                                 """***"""
#                                 # Method.objects.get_or_create(method_name=row[METHOD_NAME]) #permet de créer les méthodes à la volée
#                                 """***"""
#                                 try:
#                                     method_obj = Method.objects.get(method_name=row[METHOD_NAME])
#                                 except Method.DoesNotExist:
#                                     error_messages["method_error"].append(_('line') + ' %s: %s<br> ' % (line, row[METHOD_NAME]))
#                                 else:
#                                     if '%' in row[VARIABLE]:
#                                         verif_var = row[VARIABLE].split('%')[0] 
#                                         if len(verif_var) == 1: 
#                                             var = row[VARIABLE].split('%')[1]
#                                         else:
#                                             var = row[VARIABLE]     
#                                     else:
#                                         var = row[VARIABLE]
#                                      
#                                         
#                                     if len(var) > 0:    
#                                         (variable_obj, create) = Variable.objects.get_or_create(type=row[TYPE],
#                                                                                                   name=var,
#                                                                                                   season='?',) 
#                                     else:
#                                         error_messages["variable_error"].append(_('line {line}: Variable name {variable} is not correct.') + '<br>'.format(line=line, variable=row[VARIABLE]))
#                                         
#                                         
#                                     if create == True:
#                                         warning_messages["warnings_var"].append(variable_warnings(line, variable_obj, method_obj))
#                                     variable_method_dict[row[VARIABLE]] = (variable_obj, method_obj)
#                                     
# 
#             """
#         2/ we are reading the first file in a dictionnary.
#             """
#             e = 0
#             menu_list = ''
#             reproduction_method_list = ''
#             weight = ''
# 
#             line = 1
#     
#     
#             file = csv.DictReader(file_name, delimiter=str('\t'), quotechar=str('"'))  
#             """for row in file:
#                  print row[0]    """
#                         
#             for i in BASIC_TYPES:
#                 Germplasm_type.objects.get_or_create(germplasm_type=i, description=i)
#             germplasm_type = Germplasm_type.objects.values_list('germplasm_type')
#         
#             for n in  germplasm_type:
#                 menu = '<option value="%s">%s</option>' % (n[0], n[0])
#                 menu_list+= menu
#             reproduction_method = Reproduction_method.objects.all()
#             for rm in reproduction_method: 
#                 #print rm
#                 reproduction_method_list+= "<option value='{0}'>{1}</option>".format(rm.id, rm.reproduction_methode_name)
#                 #print reproduction_method_list
#             try:   
#                 for row in file:
#                     break
#                 
#             except:
#                 error_messages["error_message"].append(_("File format doesn't seem correct. Input a CSV with a tab delimiter") + ".<br>")
#                 return render(request, 'myuser/error_catalogue_load.html', {'error_message': error_messages["error_message"], 'form':form, 'file_name':file_name})
#             else:
# 
#                 file = csv.DictReader(file_name, delimiter=str('\t'), quotechar=str('"'))  
# 
#                 file_list = file.fieldnames
# 
#             
#             var_list = []  # liste des variables devant figurer dans le fichier méthodes
#             var_date_list = []  # liste des variables du type 'variable$date'
#                     
#             for var in file_list:
#                 """print var"""
#                 if var.find('$') != -1:
#                     var_date_list.append(var) 
#                 else:
#                     var_list.append(var)
#     
#             error_file_list = check_variable_methods(var_list, variable_method_dict)
#                     
#             if error_file_list != [] :
#                 return render(request, 'myuser/error_catalogue_load.html', {'error_file_list': error_file_list, 'form':form, 'file_name':file_name})
#                                     
#                                     
#             """ we  will create the raw_data from Raw_data table"""
#             """print variable_method_dict.items()"""
#             for row in file:
#                 """print row['variete']"""
#                 (germplasm_obj, create) = create_germplasm_catalogue(row['variete']) 
#                 if create == True:
#                     warning_messages["warnings_germ"].append(germplasm_warnings(germplasm_obj, _('father/son'), line, menu_list, ''))  
#                 for i in variable_method_dict.items():
#                                                 if str(variable_method_dict.items()[e][0]) == '' :
#                                                     error_messages["variable_error"].append('%s. %s<br>' % (len(error_messages["variable_error"]) + 1, _("A column containing data has an empty name")))
#                                                 elif str(variable_method_dict.items()[e][0]) not in file.fieldnames:
#                                                     error_messages["variable_error"].append(_('{line}. {variable}  is present in the method file but is not present in the data file') + '<br>'.format(line=len(error_messages["variable_error"]) + 1, variable=str(variable_method_dict.items()[e][0]))) 
#                                                 elif str(variable_method_dict.items()[e][0]) in file.fieldnames:
#                                                     # print "->",row[variable_method_dict.items()[e][0]],"<-"
#                                                     
#                                                     if row[variable_method_dict.items()[e][0]].strip() != "":
#                                                         if variable_method_dict.items()[e][0] + '$date' in file_list:
#                                                             data = create_raw_data_catalogue(variable_method_dict.items()[e][1][0],
#                                                                     variable_method_dict.items()[e][1][1],
#                                                                     row[variable_method_dict.items()[e][0]],
#                                                                     row[str(variable_method_dict.items()[e][0]) + '$date'], germplasm_obj       
#                                                                     )
#                                                         else:
#                                                           """str(variable_method_dict.items()[e][0]) correspond à la variable"""
#                                                           """try:"""
#                                                     
#                                                           annee = request.POST.get('catYear','')
#                   
#                                                           ann = int(annee)
#                                                           data= Raw_data.objects.filter(variable = variable_method_dict.items()[e][1][0],
#                                                                                           method = variable_method_dict.items()[e][1][1],
#                                                                                           raw_data = row[variable_method_dict.items()[e][0]],
#                                                                                           date = None, germplasm = germplasm_obj, an = ann) 
#                                                           """print data"""
#                                                           if data:
#                                                                 
#                                                                 print 'exist'
#                                                           else:
#                                                                                           
#                                                                                           
#                                                             """except (Raw_data.DoesNotExist):
#                                                             error_messages["error_message"].append(_('No raw_data submitted.') + '<br>')"""
#                                                             data = create_raw_data_catalogue(variable_method_dict.items()[e][1][0],
#                                                             variable_method_dict.items()[e][1][1],
#                                                             row[variable_method_dict.items()[e][0]],
#                                                             None, germplasm_obj, ann)
#                                                             
#                                                             """else:
# 
#                                                             print type(data)"""
#                             
#                                     
#                                                 e = e + 1
#               e = 0
#               """print data.germplasm.all()"""
#               if error_messages["variable_error"]:
#                                                 transaction.rollback()
#                                                 return render(request, 'myuser/error_catalogue_load.html', {'variable_error':error_messages["variable_error"], 'form':form, 'file_name':file_name})
# 
#                                     
#                  
#                                         
#               if error_messages["method_error"]:
#                             return render(request, 'myuser/error_catalogue_load.html', {'method_error': error_messages["method_error"], 'form':form, 'file_name':file_name})
# 
# 
# 
# 
#              for row in file:
#             
#                 #print row.keys()
#                 # #on supprime tous les espaces de fin de ligne et on recode l'info (à voir si y a moyen de faire plus propre)
#                 for key in row.keys():
#                  if key == 'variete':
#                     print (key,row[key])
#                 
#                 line = line + 1
#                             
#              message_report = 'The submission has been successful !'
# 
#             
#              if some_errors(error_messages):
#                 transaction.rollback()
#                 
#                 return render(request, 'myuser/error_catalogue_load.html', {'form':form, 'error_message': error_messages["error_message"], 'person_error':error_messages["person_error"], 
#                                                                 'error_son_germplasm':error_messages["error_son_germplasm"], 'idgermplasm_error':error_messages["idgermplasm_error"], 
#                                                                 'seed_lot_error':error_messages["seed_lot_error"], 'error_split':error_messages["error_split"],
#                                                                 'error_nomenclature':error_messages["error_nomenclature"], 'error_empty':error_messages["error_empty"],
#                                                               'method_error':error_messages["method_error"], 'variable_error':error_messages["variable_error"], 'error_project':error_messages["error_project"],
#                                                                'error_row':error_messages["error_row"],'file_name':file_name,'warnings_var':warning_messages["warnings_var"],  'message_report':message_report})
#             
#              return render(request, 'myuser/load_catalogue.html', {'form':form, 'file':file, 'warnings_var':warning_messages["warnings_var"],
#                                                              'warnings_germ':warning_messages["warnings_germ"],
#                                                              'warnings_relation':warning_messages["warnings_relation"], 'warnings_not_quantity':warning_messages["warnings_not_quantity"],
#                                                              'created_person':warning_messages["created_person"], 
#                                                             'germplasm_type':germplasm_type, 'file_name':file_name, 'is_submit':is_submit,
#                                                              'message_report':message_report})   
# 
# 
# 
# 
#    
#     else :
#         return render(request, 'myuser/load_catalogue.html', {'form':form})


@user_passes_test(lambda u: u.is_superuser)
@transaction.non_atomic_requests
@login_required
def uploaded_data_view(request):
    """
    1/ We retrieve the uploaded file .
    2/ We read the uploaded file in a dictionnary named row
    """
    germ_type_radio = []
    line = 1
    form = SubmitSpecies()
    
    # ## gestion des messages d'erreurs
    error_messages = {"error_row":[], "error_nomenclature":[], "error_split":[], "error_empty":[], 'sowing_block_error':[],
                      "error_son_germplasm":[], "idgermplasm_error":[], "mixture_empty":[], "variable_error":[], "method_error":[],
                      "select_error":[], "person_error":[], "seed_lot_error":[], "error_block":[], "error_project":[],
                      "error_sown_year":[], "error_pheno_variable":[], "pheno_grp_error":[], "error_message":[], "split_string":[],
                      "error_quantity":[], "diffusion_error":[]
                      }
    
    warning_messages = {"warning_diffusion_quantity":[], "warnings_seed":[], "female_warnings_seed":[], "xy_missing":[],
                        "male_warnings_seed":[], "warnings_species":[], "son_warnings_seed":[], "warnings_relation":[], "male_relation":[],
                        "female_relation":[], "warnings_not_quantity":[], "warnings_germ":[], "created_person":[], "warnings_var":[], "reproduction_type":[]}
   
    if request.method == 'POST':                                                                                                                                            
        
        try :
            with transaction.atomic():
                """
                1/ the uploaded file is retrieved
                """
                radio = request.POST['submit_type']
                print(radio)
                germ_type_radio.append(radio)
                species_id = request.POST['species'] 
                try:
                    species = Species.objects.get(id=request.POST['species'])
                except:
                    error_messages["error_message"].append(_('No species submitted.') + '<br>')
                    return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'file_name':'Don\'t forget to submit a species !'})
                
                """We are testing  if the files are received or not."""
                radio_dict = {'cross':'cross methods',
                              'diffusion':'diffusion methods',
                              'reproduction': 'reproduction methods',
                              'mixture':'mixture methods',
                              'intra_varietal':'intra_varietal methods',
                              'individual_data':'penotypic methods',
                              'person':'person'
                                }
                dict_grp_pheno = {}
                if radio in radio_dict.keys():
                    
                    try:
                        file_name = request.FILES['file']
                    except:
                        error_messages["error_message"].append(_('No file submitted.') + '<br>')
                        file_name = _('No input file selected')
                        return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'file_name':file_name})
                    else:
                        if radio != 'person':
                            try:
                                with request.FILES['second_file'] as second_file_name :
                                    stream = recode(second_file_name.read())
                                    f = io.StringIO(stream)
                                    second_file = csv.DictReader(f, delimiter=str("\t"), quotechar=str('"'))
                                    variable_method_dict = {}
                                    for row in second_file:
                                        
                                        line = line + 1 
            
                                       
                                        if METHOD_NAME not in second_file.fieldnames or VARIABLE not in second_file.fieldnames:
                                            error_messages["error_message"].append(_('The second file is not a method file.') + '<br>')
                                            return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                                        else:
                                            try:
                                                method_obj = Method.objects.get(method_name=row[METHOD_NAME])
                                            except Method.DoesNotExist:
                                                error_messages["method_error"].append(_('line') + ' %s: %s<br> ' % (line, row[METHOD_NAME]))
                                            else:
                                                #var_name = recode(row[VARIABLE])
                                                var_name = row[VARIABLE]
                                                if '%' in var_name:
                                                    verif_var = var_name.split('%')[0] 
                                                    if len(verif_var) == 1: 
                                                        var = var_name.split('%')[1]
                                                    else:
                                                        var = var_name  
                                                else:
                                                    var = var_name
                                                 
                                                    
                                                try :
                                                    (variable_obj, create) = Variable.objects.get_or_create(type=row[TYPE],
                                                                                                              name=var
                                                                                                              ) 
                                                except ValidationError as e:
                                                    error_messages["variable_error"].append(str(e) + '<br>')
                                                    continue
                                                    
                                                if create == True:
                                                    warning_messages["warnings_var"].append(variable_warnings(line, variable_obj, method_obj))
                                                variable_method_dict[row[VARIABLE]] = (variable_obj, method_obj)
                                                
                                                
                                                
                                                if radio == PHENOTYPIC:
                                                    if CORRELATION_GROUP not in second_file.fieldnames:
                                                        error_messages["error_message"].append(_('The second file is not a {radio} method file.').format(radio=radio) + '<br>')
                                                        return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                                                    else:
                                                        for i in range(len(row[CORRELATION_GROUP].split(','))):
                                                            grp = list(map(lambda x: str(x).strip(), row[CORRELATION_GROUP].split(',')))[i]
                                                            if grp in dict_grp_pheno:
                                                                dict_grp_pheno[grp] = dict_grp_pheno[grp] + '*/(' + row[VARIABLE]
                                                            else:
                                                                dict_grp_pheno[grp] = row[VARIABLE]
                            except Exception as e:
                                traceback.print_exc()
                                #error_messages["error_message"].append(_("You selected the {type} button but the file you submitted doesn't correspond to a {format} file format.").format(type=radio, format=radio_dict[radio]) + "<br>")
                                error_messages["error_message"].append("{0} <br />".format(str(e)))
                                return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                            else :
                                if radio == PHENOTYPIC: 
                                    if CORRELATION_GROUP not in second_file.fieldnames:
                                        error_messages["error_message"].append(_('The second file is not a {type} method file').format(type=radio) + '. <br>')
                                        return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})                      
                                    else:
                                        for i in  dict_grp_pheno.keys():
                                            dict_grp_pheno[i] = dict_grp_pheno[i].split('*/(')                  
                                                
                                if error_messages["method_error"] or error_messages["variable_error"]:
                                    return render(request, 'myuser/error.html', {'variable_error': error_messages["variable_error"], 'method_error': error_messages["method_error"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
            
                
                """
                2/ we are reading the first file in a dictionnary.
                """
                e = 0
               
                male_build_dict = {}
                male_build_dict1 = {}
                male_build_dict2 = {}
                       
                female_build_dict = {}
                female_build_dict1 = {}
                female_build_dict2 = {}
                
                build_son_dict = {}
                build_dict = {}
                build_dict1 = {}
                       
                son_seed_lot_dict = {}
                seed_lot_dict = {}
                fields_dict = {}
                seed_lot_dict1 = {}
                son_seed_lot_fields_dict = {}
           
                son_germlasm = {}
                mixt_dict = {}
                mixt_dict2 = {}
                rep_manager = {}
                
                project = None
                
                diffusion_list = []
                seed_lot_diffused = {}
                phenotypic_list = []
                created_mixture = {}
                
                previous = 0
                melange = ''
                result_list = []
                result = 0
                menu_list = ''
                
                reproduction_method_list = ''
                
                weight = ''
        
                line = 1
                list_year = ['1900']
                
                while '3000' not in list_year:
                    an = int(list_year[-1]) + 1
                    list_year.append(str(an))
                
                stream = recode(file_name.read())
                f = io.StringIO(stream)
                csvfile = csv.DictReader(f, delimiter=str('\t'), quotechar=str('"'))      
                                
#                 for i in BASIC_TYPES:
#                     GermplasmType.objects.get_or_create(germplasm_type=i, description=i)
        
                germplasm_type = GermplasmType.objects.values_list('germplasm_type')
                
                for n in  germplasm_type:
                    menu = '<option value="%s">%s</option>' % (n[0], n[0])
                    menu_list += menu
                
                
                reproduction_method = ReproductionMethod.objects.all()
                for rm in reproduction_method:
                    reproduction_method_list += "<option value='{0}'>{1}</option>".format(rm.id, rm.reproduction_methode_name)
#                 try:   
#                     for row in csvfile:
#                         break
#                         
#                 except:
#                     error_messages["error_message"].append(_("File format doesn't seem correct. Input a CSV with a tab delimiter") + ".<br>")
#                     return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
#                 else:
                #csvfile = csv.DictReader(file_name, delimiter=str('\t'), quotechar=str('"'))   
                for row in csvfile:
                    #print(row)
                    # #on supprime tous les espaces de fin de ligne et on recode l'info (à voir si y a moyen de faire plus propre)
                    for key in row.keys():
                        row[key] = row[key].strip()
                        #row[key] = recode(row[key])
                    
                    line = line + 1
                    # UNUSED CODE
    #                 if radio == PERSON :
    #                     if "id_person" not in file.fieldnames:
    #                         error_messages["error_message"].append('You selected the %s button but the file you submitted doesn\'t correspond to a %s file format.'%(radio,radio))
    #                     elif "id_person" in file.fieldnames:
    #                         #à mettre dans des constantes
    #                         fields = ["zip_code","adress","country","longitude","latitude","altitude","short_name","first_name",
    #                                   "last_name","birth_date","email","phone1","phone2","fax"]
    #                         for l in range(len(fields)) :
    #                             if fields[l] not in file.fieldnames:
    #                                 error_messages["error_message"].append('%s is missing among file fieldnames.<br>' % fields[l])
    #                                 transaction.rollback()
    #                                 return render(request,'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species,'file_name':file_name})   
    #                         (location_obj,create) = Location.objects.get_or_create(post_code=row[u"zip_code"],
    #                                                                                address = row[u"adress"],
    #                                                                                country = row[u"country"],
    #                                                                                longitude = row[u"longitude"],
    #                                                                                latitude= row[u"latitude"],
    #                                                                                altitude = row[u"altitude"],
    #                                                                                )
    #                         (person_obj, created) = Person.objects.get_or_create(short_name=row[u"short_name"],
    #                                                                              defaults={
    #                                                                              'first_name':row[u"first_name"],
    #                                                                              'last_name':row[u"last_name"],
    #                                                                              'born_year':row[u"birth_date"],
    #                                                                              'email':row[u"email"],
    #                                                                              'phone1':row[u"phone1"],
    #                                                                              'phone2':row[u"phone2"],
    #                                                                              'fax':[u"fax"],
    #                                                                              'location': location_obj
    #                                                                              })
    #                                                                              
    #                                                                              
    #                         if created == True:
    #                             warning_messages["created_person"].append('line {line}: %s<br>' % (line, person_obj))
                    #print(csvfile.fieldnames)
                    if radio == CROSS :
                        if CROSS_YEAR not in csvfile.fieldnames:
                            error_messages["error_message"].append(_("You selected the {type} button but the file you submitted doesn't correspond to a {type} file format").format(type=radio))
                            return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                        
                        fields = [PROJECT, SOWN_YEAR, HARVESTED_YEAR, QUANTITY_HARVESTED, CROSS_YEAR, CROSS_GERMPLASM, NUMBER_CROSSES,
                                        KERNEL_NUMBER_F1, MALE_SEED_LOT, MALE_ETIQUETTE, MALE_SPLIT, MALE_QUANTITY, MALE_BLOCK, MALE_X, MALE_Y,
                                        FEMALE_SEED_LOT, FEMALE_ETIQUETTE, FEMALE_SPLIT, FEMALE_QUANTITY, FEMALE_BLOCK, FEMALE_X, FEMALE_Y]
        
                        # On vérifie que les champs obligatoires (fields) sont bien présent dans le fichier
                        for l in range(len(fields)) :
                            if fields[l] not in csvfile.fieldnames:
                                error_messages["error_message"].append(_('{field} is missing among file fieldnames').format(field=fields[l]) + '.<br>')
                                transaction.rollback()
                                return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})   
                        
                        if row[PROJECT] == '' : 
                            error_messages["error_project"].append(_("line {line}: No project found").format(line=line) + "<br />")
                            continue
                        if not project or project.project_name != row[PROJECT] :
                            project, project_created = Project.objects.get_or_create(project_name=row[PROJECT], defaults={'start_date':datetime.date.today()})
                        
                        
                        if row[MALE_X] == '' or row[MALE_Y] == '' :
                            warning_messages['xy_missing'].append(_("line {line}: X and/or Y is missing for the cross concerning male seed lot").format(line=line) + " {seedlot}<br />".format(seedlot=row[MALE_SEED_LOT]))
                        if row[FEMALE_X] == '' or row[FEMALE_Y] == '' :
                            warning_messages['xy_missing'].append(_("line {line}: X and/or Y is missing for the cross concerning female seed lot").format(line=line) + " {seedlot}<br />".format(seedlot=row[FEMALE_SEED_LOT]))
                        
                        """ file_list represente l'entete - colonnes obligatoire = variables (tous les champs non obligatoires)"""
                        file_list = get_variable_list(csvfile.fieldnames, fields)
                        
                        """ On vérifie que les éléments de file_list correspondent à ce qui se trouve dans le fichier methode """
                
                        var_list = []  # liste des variables devant figurer dans le fichier méthodes
                        var_date_list = []  # liste des variables du type 'variable$date'
                        
                        for var in file_list:
                            if var.find('$') != -1:
                                var_date_list.append(var) 
                            else:
                                if '%' in var :
                                    var_list.append(var.split('%')[1])
                                else :
                                    var_list.append(var)
        
                        error_file_list = check_variable_methods(var_list, variable_method_dict)
                        
                        if error_file_list != [] :
                            return render(request, 'myuser/error.html', {'error_file_list': error_file_list, 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                        
                        """ On vérifie que la date 'variable$date' est bien liée à une variable 'variable' existante """ 
                        error_var_date_list = check_variable_date(var_date_list, var_list)
                        
                        if error_var_date_list != [] :
                            return render(request, 'myuser/error.html', {'error_var_date_list': error_var_date_list, 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
        
                        #### DEBUT DES CHECKS CROSS ###
                        try:
                            int(row[SOWN_YEAR])
                            int(row[HARVESTED_YEAR])
                            int(row[CROSS_YEAR])
                       
                            if int(row[SOWN_YEAR]) > int(row[HARVESTED_YEAR]) :
                                error_messages["error_sown_year"].append(_('line {line}: sown year is higher than harvested year').format(line=line) + '<br>')
                        except ValueError:
                            dict_year_sh = {row[SOWN_YEAR]:SOWN_YEAR, row[HARVESTED_YEAR]:HARVESTED_YEAR, row[CROSS_YEAR]:CROSS_YEAR}    
                            for i in dict_year_sh.keys():
                                if i not in list_year:
                                    error_messages["error_sown_year"].append(_("line {line}: column '{col}', '{value}' is not a valid value").format(line=line, col=dict_year_sh[i], value=i) + '<br>')
                        else:  
        
                            try:
                                int(row[FEMALE_BLOCK])
                            except ValueError:   
                                error_messages["error_block"].append(_('line {line}: block information for female seed lot is missing or is not an integer value').format(line=line) + '<br />')
                            else:
                                try:
                                    int(row[MALE_BLOCK])
                                except ValueError: 
                                    error_messages["error_block"].append(_('line {line}: block information for male seed lot is missing or is not an integer value').format(line=line) + '<br />')
                                else:
                                    if int(row[HARVESTED_YEAR]) < int(row[SOWN_YEAR]):  # Année de semis = à l'année de récolte autorisée
                                        error_messages["error_message"].append(_('line {line}: {col} ({value}) can not be greater than or equal to {othercol} ({othervalue})').format(line=line, col=SOWN_YEAR, value=row[SOWN_YEAR], othercol=HARVESTED_YEAR, othervalue=row[HARVESTED_YEAR]) + '.<br>')
                                    elif int(row[CROSS_YEAR]) < int(row[SOWN_YEAR]) :
                                        error_messages["error_message"].append(_('line {line}: {col} ({value}) can not be greater than or equal to {othercol} ({othervalue})').format(line=line, col=SOWN_YEAR, value=row[SOWN_YEAR], othercol=CROSS_YEAR, othervalue=row[CROSS_YEAR]) + '.<br>')  
                                    elif int(row[FEMALE_SEED_LOT].split('_')[2]) < int(row[SOWN_YEAR]):
                                        error_messages["error_message"].append(_('line {line}: The creation date of the female seed lot {col} ({value}) can not be smaller than {othercol} ({othervalue})').format(line=line, col=row[FEMALE_SEED_LOT], value=row[FEMALE_SEED_LOT].split('_')[2], othercol=SOWN_YEAR, othervalue=row[SOWN_YEAR]) + '.<br>')
                                    elif row[FEMALE_SEED_LOT].split('_')[1] != row[MALE_SEED_LOT].split('_')[1]:
                                        error_messages["error_message"].append(_('line {line}: {male} (male) creation place is different from  {female} (female) place').format(line=line, male=row[MALE_SEED_LOT], female=row[FEMALE_SEED_LOT]) + '.<br>')
                                    elif len(row[FEMALE_SEED_LOT].split('_')) > 4 or len(row[FEMALE_SEED_LOT].split('_')) < 3:
                                        error_messages["error_nomenclature"].append(_('line {line}: {seedlot} is not a valid name for a seed lot').format(line=line, seedlot=row[FEMALE_SEED_LOT]) + '.<br>')
                                    elif len(row[MALE_SEED_LOT].split('_')) > 4 or len(row[MALE_SEED_LOT].split('_')) < 3:
                                        error_messages["error_nomenclature"].append(_('line {line}: {seedlot} is not a valid name for a seed lot').format(line=line, seedlot=row[MALE_SEED_LOT]) + '.<br> ')
                                    else:
                                        try:
                                            location_obj = Location.objects.get(short_name=row[FEMALE_SEED_LOT].split('_')[1])
                                        except Location.DoesNotExist:
                                            error_messages["person_error"].append(_('line') + ' {line}: {seedlot} <br>'.format(line=line, seedlot=row[FEMALE_SEED_LOT].split('_')[1]))
                                        except IndexError :
                                            error_messages["seed_lot_error"].append(_('line {line}: {seedlot} is not a valid name for a seed lot').format(line=line, seedlot=row[ID_SEED_LOT_SOWN]) + '<br>')
                                        else:
                                            #### FIN DES CHECKS CROSS ###
                                            """ female """
                                            """ Germplasm creation in germplasm table """
                                            
                                            """Check female germplasm name"""        
                                            try :
                                                (female_germplasm_obj, create) = create_germplasm(row[FEMALE_SEED_LOT], None, species)
                                            except NameErrorException as ne:
                                                error_messages["idgermplasm_error"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ne)))
                                                continue
                                            
                                            if create == True:
                                                warning_messages["warnings_germ"].append(germplasm_warnings(female_germplasm_obj, 'female', line, menu_list, species))    
                                                
                                            if row[FEMALE_SEED_LOT] in female_build_dict :
                                                female_seed_lot_obj = female_build_dict[row[FEMALE_SEED_LOT]]
                                            else:
                                                if len(row[FEMALE_SEED_LOT].split('_')) == 3:
                                                    if row[FEMALE_SEED_LOT] in female_build_dict1:
                                                        female_digit = female_build_dict1[row[FEMALE_SEED_LOT]]
                                                    else:
                                                        # le digit du Seedlot est incrémenté lorsque qu'il est créé pour une espèce différente (germplasm)
                                                        female_digit = create_digit(row[FEMALE_SEED_LOT])
                                                        try :
                                                            date = find_date(row[FEMALE_SEED_LOT])
                                                        except :
                                                            error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot').format(line=line, name=row[FEMALE_SEED_LOT]) + '<br>')
                                                            continue                                 
                                                        #date = find_date(row[FEMALE_SEED_LOT]) 
                                                        
                                                        """ si la date de creation est inconnue """
                                                        """if date == "XXXX":
                                                            date = None
                                                        else:
                                                            date = date"""
                                                        
                                                        female_seed_lot_fields_dict = {'date':date,
                                                                                       'location':location_obj,
                                                                                       'germplasm':female_germplasm_obj,
                                                                                       'quantity_ini':None,
                                                                                       }
                                                        try :
                                                            (female_seed_lot_obj, create) = create_seed_lot(female_seed_lot_fields_dict,
                                                                                                  location_obj,
                                                                                                  female_germplasm_obj,
                                                                                                  female_digit,
                                                                                                  )
                                                        except Exception as e :
                                                            error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                                        # female_seed_lot_obj.project.add(project)
                                                        
                                                        if create == True:
                                                            warning_messages["female_warnings_seed"].append(seed_lot_warnings(female_seed_lot_obj, 'female', row[FEMALE_QUANTITY], line))    
                                                            warning_messages["warnings_not_quantity"].append(_('line {line}: (female) {seedlot}').format(line=line, seedlot=female_seed_lot_obj) + '.<br>')
                                                        female_build_dict1[row[FEMALE_SEED_LOT]] = female_digit                                                             
                                                elif len(row[FEMALE_SEED_LOT].split('_')) == 4:
                                                    if row[FEMALE_SEED_LOT] in female_build_dict2.keys():
                                                        female_seed_lot_obj = female_build_dict2[row[FEMALE_SEED_LOT]]
                                                    else:
                                                        try:
                                                            female_seed_lot_obj = Seedlot.objects.get(name=row[FEMALE_SEED_LOT])
                                                        except Seedlot.DoesNotExist:
                                                            error_messages["seed_lot_error"].append(_("line {line}: {seedlot} doesn't exist").format(line=line, seedlot=row[FEMALE_SEED_LOT]) + '<br>')
                                                            continue
                                                        else:
                                                            female_build_dict2[row[FEMALE_SEED_LOT]] = female_seed_lot_obj
                                                female_build_dict[row[FEMALE_SEED_LOT]] = female_seed_lot_obj
                                        
                                            """ male """
                                            """Check male germplasm name"""
                                            
                                            try :
                                                (male_germplasm_obj, create) = create_germplasm(row[MALE_SEED_LOT], None, species)
                                            except NameErrorException as ne:
                                                error_messages["idgermplasm_error"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ne)))
                                                continue
        
                                            if create == True:
                                                warning_messages["warnings_germ"].append(germplasm_warnings(male_germplasm_obj, 'male', line, menu_list, species))
                                                
                                            if row[MALE_SEED_LOT] in male_build_dict :
                                                male_seed_lot_obj = male_build_dict[row[MALE_SEED_LOT]]
                                            else:
                                                if len(row[MALE_SEED_LOT].split('_')) == 3:
                                                    # date is the date of creation of the seed lot
                                                    if row[MALE_SEED_LOT] in male_build_dict1:
                                                        
                                                        male_digit = male_build_dict1[row[MALE_SEED_LOT]]
                                                    else:
                                                        if row[MALE_SEED_LOT] in female_build_dict1:
                                                            male_digit = female_build_dict1[row[MALE_SEED_LOT]]
                                                        else:    
                                                            male_digit = create_digit(row[MALE_SEED_LOT])
                                                            try :
                                                                date = find_date(row[MALE_SEED_LOT])
                                                            except :
                                                                error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot').format(line=line, name=row[MALE_SEED_LOT]) + '<br>')
                                                                continue  
                                                            
                                                            #date = find_date(row[MALE_SEED_LOT]) 
                                                        
                                                        """ si la date de creation est inconnue """
                                                        """if date == "XXXX":
                                                            date = None
                                                        else:
                                                            date = date"""
                                                        
                                                        male_seed_lot_fields_dict = {'date':date,
                                                                                       'location':location_obj,
                                                                                       'germplasm':male_germplasm_obj,
                                                                                       'quantity_ini':None}
                                                        
                                                        try :
                                                            (male_seed_lot_obj, create) = create_seed_lot(male_seed_lot_fields_dict,
                                                                                                  location_obj,
                                                                                                  male_germplasm_obj,
                                                                                                  male_digit)
                                                        except Exception as e :
                                                            error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                                        
                                                        # male_seed_lot_obj.project.add(project)
                                                        
                                                        if create == True:
                                                            warning_messages["male_warnings_seed"].append(seed_lot_warnings(male_seed_lot_obj, 'male', row[MALE_QUANTITY], line))
                                                            warning_messages["warnings_not_quantity"].append(_('line {line}: (male) {seedlot}').format(line=line, seedlot=male_seed_lot_obj) + '.<br>')
                                                        male_build_dict1[row[MALE_SEED_LOT]] = male_digit
                                                        
                                                elif len(row[MALE_SEED_LOT].split('_')) == 4:
                                                    if row[MALE_SEED_LOT] in male_build_dict2.keys():
                                                        male_seed_lot_obj = male_build_dict2[row[MALE_SEED_LOT]]
                                                    else:
                                                        try:
                                                            male_seed_lot_obj = Seedlot.objects.get(name=row[MALE_SEED_LOT])
                                                        except Seedlot.DoesNotExist:
                                                            error_messages["seed_lot_error"].append(_("line {line}: {seedlot} doesn't exist").format(line=line, seedlot=row[MALE_SEED_LOT]) + '<br>')
                                                            continue
                                                        else:
                                                            male_build_dict2[row[MALE_SEED_LOT]] = male_seed_lot_obj
                                                male_build_dict[row[MALE_SEED_LOT]] = male_seed_lot_obj
                                            
                                                
                                            if row[KERNEL_NUMBER_F1] == '0':
                                                row_germplasm = create_digit_gpnotcrossed()
                                            else:
                                                row_germplasm = row[CROSS_GERMPLASM]
                                                
                                            """ SON """
                                            """Check son germplasm name"""    
                                                
                                            try :
                                                (son_germplasm_obj, created) = create_germplasm(row_germplasm, None, species)
                                            except NameErrorException as ne:
                                                error_messages["idgermplasm_error"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ne)))
                                                continue
                                            
                                            # print('error_messages["idgermplasm_error"]')
                                            # print(error_messages["idgermplasm_error"])
                                            
                                            if created == False:
                                                error_messages["error_son_germplasm"].append('line {line}: {germplasm}<br>'.format(line=line, germplasm=son_germplasm_obj))
                                            elif created == True:
                                                warning_messages["warnings_germ"].append(germplasm_warnings(son_germplasm_obj, 'son', line, menu_list, species))
                                                if row[KERNEL_NUMBER_F1] == '0':
                                                    son_name = row_germplasm 
                                                else:
                                                    son_name = row_germplasm + "_" + row[MALE_SEED_LOT].split('_')[1] + "_" + row[CROSS_YEAR]
                                                    
                                                if row_germplasm in build_son_dict.keys():
                                                    son_seed_lot_obj = build_son_dict[row_germplasm]
                                                else:
                                                    if row[KERNEL_NUMBER_F1] == '0':
                                                        son_digit = row_germplasm
                                                    else: 
                                                        son_digit = create_digit(son_name)
                                                    
                                                    """ Vérifie que la quantité récoltée est au format numéric"""  
                                                    weight, msg = get_quantity(row[QUANTITY_HARVESTED])
                                                    if msg :
                                                        error_messages["error_message"].append(msg)
                                                        continue
                                            
                                                    if weight == 0:
                                                        son_digit = 'NOT_HARVESTED_' + son_digit
                                                    
                                                    son_seed_lot_fields_dict = {'date':row[CROSS_YEAR],
                                                                                'quantity_ini':weight,
                                                                                }
                                                    try :
                                                        (son_seed_lot_obj, create) = create_seed_lot(son_seed_lot_fields_dict,
                                                                                       location_obj,
                                                                                       son_germplasm_obj,
                                                                                       son_digit)
                                                    except Exception as e :
                                                        error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                                    # son_seed_lot_obj.project.add(project)
                                                
                                                    if create == True:
                                                        warning_messages["son_warnings_seed"].append(seed_lot_warnings(son_seed_lot_obj, 'son', son_seed_lot_obj.quantity_ini, line))
                                                    if row[QUANTITY_HARVESTED] == '':
                                                        warning_messages["warnings_not_quantity"].append(_("line {line}: (son) {seedlot}.").format(line=line, seedlot=son_seed_lot_obj) + '<br>')
                                                    build_son_dict[row_germplasm] = son_seed_lot_obj
                                                
                                                if row[FEMALE_SPLIT] != '':
                                                    if row[FEMALE_SPLIT] != '0' and row[FEMALE_SPLIT] != '1' :
                                                        error_messages["error_split"].append(_('line {line}: {split} is not a valid split value it should be 0 or 1').format(line=line, split=row[FEMALE_SPLIT]) + '<br>')
                                                if row[MALE_SPLIT] != '':
                                                    if row[MALE_SPLIT] != '0' and row[MALE_SPLIT] != '1' :
                                                        error_messages["error_split"].append(_('line {line}: {split} is not a valid split value it should be 0 or 1').format(line=line, split=row[FEMALE_SPLIT]) + '<br>')
                                                
                                                """ we create the cross """
                                                if  row[QUANTITY_HARVESTED] == '' :
                                                    weight = None
                                                else:
                                                    try:   
                                                        weight = float(row[QUANTITY_HARVESTED].replace(',','.'))
                                                    except ValueError:
                                                        error_messages["error_message"].append(_("Row[QUANTITY_HARVESTED] doesn't correspond to a numerical format."))
                                                        
                                                    if float(row[QUANTITY_HARVESTED].replace(',','.')) == 0:
                                                        son_digit = 'NOT_HARVESTED_' + son_digit
                                                
                                                male_quantity, msg = get_quantity(row[MALE_QUANTITY])
                                                if msg :
                                                    error_messages["error_message"].append(msg)
                                                female_quantity, msg = get_quantity(row[FEMALE_QUANTITY])
                                                if msg :
                                                    error_messages["error_message"].append(msg)
                                                    
                                                dict = {
                                                            'start_date': row[SOWN_YEAR],
                                                            'end_date': row[HARVESTED_YEAR],
                                                            'date':row[CROSS_YEAR],
                                                            'kernel_number':row[KERNEL_NUMBER_F1],
                                                            'cross_number':row[NUMBER_CROSSES],
                                                            
                                                            'male_split':row[MALE_SPLIT],
                                                            'male_block':row[MALE_BLOCK],
                                                            'male_X':row[MALE_X],
                                                            'male_Y':row[MALE_Y],
                                                            'male_quantity':male_quantity,
                                                            
                                                            'female_split':row[FEMALE_SPLIT],
                                                            'female_block':row[FEMALE_BLOCK],
                                                            'female_X':row[FEMALE_X],
                                                            'female_Y':row[FEMALE_Y],
                                                            'female_quantity':female_quantity
                                                                            }
                                                
                                                (female_cross_created, male_cross_created, created) = create_cross(dict, female_seed_lot_obj, male_seed_lot_obj, son_seed_lot_obj)
                                                
                                                if created == True:
                                                    warning_messages["warnings_relation"].append(_('line {line}: cross between {female} (female) & {male} (male) --> {seedlot} (son)').format(line=line, female=female_seed_lot_obj, male=male_seed_lot_obj, seedlot=son_seed_lot_obj) + '.<br>')
                                                
                                                
                                                female_cross_created.project.add(project)
                                                male_cross_created.project.add(project)
                                                """ we  will create the raw_data from Rawdata table"""
                                                for i in variable_method_dict.items():
                                                    if str(list(variable_method_dict.items())[e][0]) == '' :
                                                        error_messages["variable_error"].append('%s. %s<br>' % (len(error_messages["variable_error"]) + 1, _("A column containing data has an empty name")))
                                                    elif str(list(variable_method_dict.items())[e][0]) not in csvfile.fieldnames:
                                                        error_messages["variable_error"].append(_('{line}. {variable}  is present in the method file but is not present in the data file').format(line=len(error_messages["variable_error"]) + 1, variable=str(list(variable_method_dict.items())[e][0])) + '<br>') 
                                                    elif str(list(variable_method_dict.items())[e][0]) in csvfile.fieldnames:
                                                        # print("->",row[variable_method_dict.items()[e][0]],"<-")
                                                        if row[list(variable_method_dict.items())[e][0]].strip() != "":
                                                            data = None
                                                            if list(variable_method_dict.items())[e][0] + '$date' in file_list and row[str(list(variable_method_dict.items())[e][0]) + '$date'] != '':
                                                                try :
                                                                    with transaction.atomic() :
                                                                        data = create_raw_data(list(variable_method_dict.items())[e][1][0],
                                                                                list(variable_method_dict.items())[e][1][1],
                                                                                row[list(variable_method_dict.items())[e][0]],
                                                                                row[str(list(variable_method_dict.items())[e][0]) + '$date'],
                                                                                request.user
                                                                                )
                                                                except ValidationError as ve:
                                                                    error_messages["error_pheno_variable"].append("line {line} : {message}".format(line=line, message=str(ve) % {'value': row[str(list(variable_method_dict.items())[e][0])+ '$date']}) + "<br />")
                                                            else:
                                                                """str(list(variable_method_dict.items())[e][0]) correspond à la variable"""
                                                                try:
                                                                    with transaction.atomic() :
                                                                        data = create_raw_data(list(variable_method_dict.items())[e][1][0],
                                                                                list(variable_method_dict.items())[e][1][1],
                                                                                row[list(variable_method_dict.items())[e][0]],
                                                                                None,
                                                                                request.user)
                                                                except ValidationError as ve:
                                                                    error_messages["error_pheno_variable"].append("line {line} : {message}".format(line=line, message=str(ve) % {'value': row[str(list(variable_method_dict.items())[e][0])+ '$date']}) + "<br />")
                                                            if data :
                                                                if str(data.variable.type) == 'T1':
                                                                    data.relation.add(male_cross_created)
                                                                    data.relation.add(female_cross_created)
                                                                elif str(data.variable.type) == 'T2':
                                                                    data.relation.add(male_cross_created)
                                                                elif str(data.variable.type) == 'T3':
                                                                    data.relation.add(female_cross_created)
                                                                elif str(data.variable.type) == 'T7':
                                                                    data.seedlot.add(male_seed_lot_obj)
                                                                    data.seedlot.add(female_seed_lot_obj)
                                                                elif str(data.variable.type) == 'T8':
                                                                    data.seedlot.add(son_seed_lot_obj)   
                                                                elif str(data.variable.type) == 'T9':
                                                                    data.seedlot.add(male_seed_lot_obj)
                                                                elif str(data.variable.type) == 'T10':
                                                                    data.seedlot.add(female_seed_lot_obj)
                                                                else :
                                                                    error_messages["error_pheno_variable"].append(_("line {line} : Type {type} can't be used with {event} event".format(line=line, type=data.variable.type, event=CROSS))+"<br />")
                                                    e = e + 1
                                                e = 0
                                                if error_messages["variable_error"]:
                                                    transaction.rollback()
                                                    return render(request, 'myuser/error.html', {'variable_error':error_messages["variable_error"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})                                                      
                    elif radio == DIFFUSION:
                        if LOCATION not in csvfile.fieldnames:
                            error_messages["error_message"].append(_("You selected the {type} button but the file you submitted doesn't correspond to a {type} file format").format(type=radio))
                            return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                        else:
                            # liste des champs obligatoires pour une diffusion (à modifier si on modifie un fichier type)
                            fields = [PROJECT, LOCATION, ID_SEED_LOT, ETIQUETTE, EVENT_YEAR, SPLIT, QUANTITY]
                            for l in range(len(fields)) :
                                if fields[l] not in csvfile.fieldnames:
                                    error_messages["error_message"].append(_('{field} is missing among file fieldnames.').format(field=fields[l]) + '<br>')
                                    transaction.rollback()
                                    return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})   
                            if row[PROJECT] == '' : 
                                error_messages["error_project"].append(_("line {line}: No project found").format(line=line) + "<br />")
                                continue
                            if not project or project.project_name != row[PROJECT] :
                                project, project_created = Project.objects.get_or_create(project_name=row[PROJECT], defaults={'start_date':datetime.date.today()})
                            
                            """on initalise l, copy permet de creer deux espaces différents dans le disque dure pour csvfile.fieldnames et file_list.
                            à la fin de cette boucle for file list represente l'entete - colones obligatoire = variables"""
                            file_list = get_variable_list(csvfile.fieldnames, fields)
                            """ On vérifie que les éléments de file_list correspondent à ce qui se trouve dans le fichier methode """
                
                            var_list = []  # liste des variables devant figurer dans le fichier méthodes
                            var_date_list = []  # liste des variables du type 'variable$date'
                            
                            for var in file_list:
                                if var.find('$') != -1:
                                    var_date_list.append(var) 
                                else:
                                    var_list.append(var)
            
                            error_file_list = check_variable_methods(var_list, variable_method_dict)
                            
                            if error_file_list != [] :
                                return render(request, 'myuser/error.html', {'error_file_list': error_file_list, 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                            
                            """ On vérifie que la date 'variable$date' est bien liée à une variable 'variable' existante """ 
                            error_var_date_list = check_variable_date(var_date_list, var_list)
                            
                            if error_var_date_list != [] :
                                return render(request, 'myuser/error.html', {'error_var_date_list': error_var_date_list, 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
        
                            """vérification que event_year est bien une date comprise entre 1900 et 3000"""
                            #### DEBUT DES CHECKS DIFFUSION ###
                            try:
                                int(row[EVENT_YEAR])
                            except ValueError:
                                dict_year_sh = {row[EVENT_YEAR]:EVENT_YEAR}    
                                for i in dict_year_sh.keys():
                                    if i not in list_year:
                                        error_messages["error_sown_year"].append(_("line {line}: column '{col}', '{value}' is not a valid value").format(line=line, col=dict_year_sh[i], value=i) + "<br>")
                            else:                           
                                if (row[ID_SEED_LOT], row[LOCATION]) not in diffusion_list :
                                    diffusion_list.append((row[ID_SEED_LOT], row[LOCATION]))
                                    try:
                                        father_location = Location.objects.get(short_name=row[ID_SEED_LOT].split('_')[1])
                                    except Location.DoesNotExist:
                                        error_messages["person_error"].append(_('line') + ' {line}: {person} <br>'.format(line=line, person=row[ID_SEED_LOT].split('_')[1]))
                                    except IndexError :
                                        error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot').format(line=line, name=row[ID_SEED_LOT]) + '<br>')
                                    else:
                                        #### FIN DES CHECKS DIFFUSION ###
                                        seed_lot_obj = None
                                        try :
                                            (germplasm_obj, created) = create_germplasm(row[ID_SEED_LOT], None, species)
                                        except NameErrorException as ne:
                                                error_messages["idgermplasm_error"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ne)))
                                                continue
                                        if created == True:
                                            warning_messages["warnings_germ"].append(germplasm_warnings(germplasm_obj, _('father/son'), line, menu_list, species))                                
                                        if len(row[ID_SEED_LOT].split('_')) == 4: 
                                            try:
                                                seed_lot_obj = Seedlot.objects.get(name=row[ID_SEED_LOT])
                                            except Seedlot.DoesNotExist:
                                                error_messages["seed_lot_error"].append(_("line {line}: {seedlot} doesn't exist").format(line=line, seedlot=row[ID_SEED_LOT]) + "<br>")
                                                continue
                                        elif len(row[ID_SEED_LOT].split('_')) == 3 and row[ID_SEED_LOT] not in seed_lot_diffused.keys():           
                                            try :
                                                date = find_date(row[ID_SEED_LOT])
                                            except :
                                                error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot').format(line=line, name=row[ID_SEED_LOT]) + '<br>')
                                                continue
                                            """ si la date de DIFFUSION est inconnue """
#                                             if date == "XXXX":
#                                                 date = None
#                                             else:
#                                                 date = date
#                                             
                                             
                                            seed_lot_table_dict = {'quantity_ini': None,
                                                                   'date': date
                                                                   }
                                            seed_lot_digit = create_digit(row[ID_SEED_LOT])
                                            try :
                                                (seed_lot_obj, create) = create_seed_lot(seed_lot_table_dict, father_location, germplasm_obj, seed_lot_digit)
                                            except Exception as e :
                                                error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                                continue
                                            seed_lot_diffused[row[ID_SEED_LOT]] = seed_lot_obj
                                            weight, msg = get_quantity(row[QUANTITY])
                                            if create == True:  
                                                warning_messages["warnings_seed"].append(seed_lot_warnings(seed_lot_obj, _('father'), weight, line))
                                                warning_messages["warnings_not_quantity"].append(_('line {line}: (father) {seedlot}').format(line=line, seedlot=seed_lot_obj) + '<br>')
                                        elif row[ID_SEED_LOT] in seed_lot_diffused.keys() :
                                            seed_lot_obj = seed_lot_diffused[row[ID_SEED_LOT]]
                                            
                                            """ seed lot diffused """
                                            
                                        # seed_lot_obj.project.add(project)
                                        if seed_lot_obj:
                                            try:
                                                son_location = Location.objects.get(short_name=row[LOCATION])
                                            except Location.DoesNotExist:
                                                error_messages["person_error"].append(_('line') + ' {line}: {location} <br>'.format(line=line, location=row[LOCATION]))
                                                continue
                                            except IndexError :
                                                error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot').format(line=line, name=row[ID_SEED_LOT_SOWN]) + '<br>')
                                            else:    
                                                son = row[ID_SEED_LOT].split('_')[0] + '_' + row[LOCATION] + '_' + row[EVENT_YEAR]
                                                son_digit = create_digit(son)
                                                male_build_dict[son] = son_digit
                    
                                                date = row[EVENT_YEAR] 
                                                quantity, msg = get_quantity(row[QUANTITY])
                                                if msg :
                                                    error_messages["error_message"].append(msg)
                                                    continue
                                                diff_seed_lot_table_dict = {
                                                                'quantity_ini': quantity,
                                                               'date': date
                                                               }
                                                try :
                                                    (diff_seed_lot_obj, create) = create_seed_lot(diff_seed_lot_table_dict, son_location, germplasm_obj, son_digit)
                                                except Exception as e :
                                                    error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                                # diff_seed_lot_obj.project.add(project)
                                                if create == True:
                                                    warning_messages["son_warnings_seed"].append(seed_lot_warnings(diff_seed_lot_obj, _('son'), row[QUANTITY], line))
                                                if row[QUANTITY] != '':
                                                    if float(row[QUANTITY].replace(',','.')) == 0:
                                                        warning_messages["warnings_not_quantity"].append(_('line {line}: (son) {seedlot}').format(line=line, seedlot=diff_seed_lot_obj) + '<br>')
                                                """ diffusion creation."""
                                                
                                                diffusion_dict = {'split' : row[SPLIT],
                                                                  'date' : date,
                                                                  'quantity': quantity
                                                                }
                                                    # (diffusion_created, create) = create_diffusion2(seed_lot_obj, diff_seed_lot_obj, diffusion_dict)
                                                try :
                                                    (diffusion_created, create) = create_diffusion(seed_lot_obj, diff_seed_lot_obj, diffusion_dict)
                                                except ValidationError as ve:
                                                    error_messages['diffusion_error'].append(_("line")+" {line} : {message}".format(line=line,message=str(ve)))
                                                    continue
                                                if quantity == None:
                                                    warning_messages["warning_diffusion_quantity"].append(_('line') + ' {line}: {diffusion} <br>'.format(line=line, diffusion=diffusion_created))
                                                diffusion_created.project.add(project)
                                                if create == True:
                                                    warning_messages["warnings_relation"].append(_('line') + ' {line}: {message}<br>'.format(line=line, message=diffusion_created))
        
                                                """ we  will create the raw_data from Rawdata table"""
                                                for i in variable_method_dict.items():
                                                    if str(list(variable_method_dict.items())[e][0]) == '' :
                                                        error_messages["variable_error"].append('%s. %s<br>' % (len(error_messages["variable_error"]) + 1, _("A column containing data has an empty name")))
                                                    elif str(list(variable_method_dict.items())[e][0]) not in csvfile.fieldnames:
                                                        error_messages["variable_error"].append(_('{line}. {method} is present in the method file but is not present in the data file').format(line=len(error_messages["variable_error"]) + 1, method=str(list(variable_method_dict.items())[e][0])) + '<br>') 
                                                    else: 
                                                        if row[list(variable_method_dict.items())[e][0]].strip() != "":
                                                            """str(list(variable_method_dict.items())[e][0]) correspond à la variable"""
                                                            data = None
                                                            if list(variable_method_dict.items())[e][0] + '$date' in file_list and row[str(list(variable_method_dict.items())[e][0]) + '$date'] != '':
                                                                try :
                                                                    with transaction.atomic() :
                                                                        data = create_raw_data(list(variable_method_dict.items())[e][1][0],
                                                                                                    list(variable_method_dict.items())[e][1][1],
                                                                                                    row[list(variable_method_dict.items())[e][0]],
                                                                                                    row[str(list(variable_method_dict.items())[e][0]) + '$date'] ,
                                                                                                    request.user    
                                                                                                    )
                                                                except ValidationError as ve:
                                                                    error_messages["error_pheno_variable"].append("line {line} : {message}".format(line=line, message=str(ve) % {'value': row[str(list(variable_method_dict.items())[e][0])+ '$date']}) + "<br />")
                                                            else:
                                                                """str(list(variable_method_dict.items())[e][0]) correspond à la variable"""
                                                                try :
                                                                    with transaction.atomic() :
                                                                        data = create_raw_data(list(variable_method_dict.items())[e][1][0],
                                                                                                    list(variable_method_dict.items())[e][1][1],
                                                                                                    row[list(variable_method_dict.items())[e][0]],
                                                                                                    None,
                                                                                                    request.user
                                                                                                    )
                                                                except ValidationError as ve:
                                                                    error_messages["error_pheno_variable"].append("line {line} : {message}".format(line=line, message=str(ve) % {'value': row[str(list(variable_method_dict.items())[e][0])+ '$date']}) + "<br />")
                                                            if data :
                                                                if str(data.variable.type) == 'T1':
                                                                    data.relation.add(diffusion_created)
                                                                elif str(data.variable.type) == 'T7':
                                                                    data.seedlot.add(seed_lot_obj)
                                                                elif str(data.variable.type) == 'T8':
                                                                    data.seedlot.add(diff_seed_lot_obj)
                                                                else :
                                                                    error_messages["error_pheno_variable"].append(_("line {line} : Type {type} can't be used with {event} event".format(line=line, type=data.variable.type, event=DIFFUSION))+"<br />")
                                                    e = e + 1
                                                e = 0
                                                if error_messages["variable_error"]:
                                                    transaction.rollback()
                                                    return render(request, 'myuser/error.html', {'variable_error':error_messages["variable_error"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name}) 
                    elif radio == MIXTURE :
                        #print(csvfile.fieldnames)
                        if GERMPLASM not in csvfile.fieldnames:
                            error_messages["error_message"].append(_("You selected the {type} button but the file you submitted doesn't correspond to a {type} file format").format(type=radio))
                            return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                        
                        fields = [PROJECT, ID_SEED_LOT, ETIQUETTE, SPLIT, QUANTITY, GERMPLASM, EVENT_YEAR]                        
                        for l in range(len(fields)) :
                            if fields[l] not in csvfile.fieldnames:
                                error_messages["error_message"].append(_("{field} field is missing among file fieldnames").format(field=fields[l]) + ".<br>")
                                transaction.rollback()
                                return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})   
                        
                        if row[PROJECT] == '' : 
                            error_messages["error_project"].append(_("line {line}: No project found").format(line=line) + "<br />")
                            continue
                        if not project or project.project_name != row[PROJECT] :
                            project, project_created = Project.objects.get_or_create(project_name=row[PROJECT], defaults={'start_date':datetime.date.today()})
                        """on initalise l, copy permet de creer deux espaces différents dans le disque dure pour csvfile.fieldnames et file_list.
                        à la fin de cette boucle for file list represente l'entete - colones obligatoire = variables"""
                        file_list = get_variable_list(csvfile.fieldnames, fields)
                        
                        """ On vérifie que les éléments de file_list correspondent à ce qui se trouve dans le fichier methode """
                
                        var_list = []  # liste des variables devant figurer dans le fichier méthodes
                        var_date_list = []  # liste des variables du type 'variable$date'
                        
                        for var in file_list:
                            if var.find('$') != -1:
                                var_date_list.append(var) 
                            else:
                                var_list.append(var)
        
                        error_file_list = check_variable_methods(var_list, variable_method_dict)
                        
                        if error_file_list != [] :
                            return render(request, 'myuser/error.html', {'error_file_list': error_file_list, 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                        
                        """ On vérifie que la date 'variable$date' est bien liée à une variable 'variable' existante """ 
                        error_var_date_list = check_variable_date(var_date_list, var_list)
                        
                        if error_var_date_list != [] :
                            return render(request, 'myuser/error.html', {'error_var_date_list': error_var_date_list, 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
        
                        if GERMPLASM not in csvfile.fieldnames:
                            error_messages["error_message"].append(_("You selected the {type} button but the file you submitted doesn't correspond to a {type} file format").format(type=radio))
                            return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                        
                        else:
                            """ vérification que event_year est bien une date comprise entre 1900 et 3000 """      
                            try:
                                int(row[EVENT_YEAR])     
                            except ValueError:
                                dict_year_sh = {row[EVENT_YEAR]:EVENT_YEAR}    
                                for i in dict_year_sh.keys():
                                    if i not in list_year:
                                        error_messages["error_sown_year"].append(_("line {line}: column '{col}', '{value}' is not a valid value").format(line=line, col=dict_year_sh[i], value=i) + "<br>")
                            else:
                                try :
                                    int(row[ID_SEED_LOT].split('_')[2])
                                except :
                                    pass
                                else :                                   
                                    if int(row[ID_SEED_LOT].split('_')[2]) > int(row[EVENT_YEAR]):
                                        error_messages["error_message"].append(_('line {line}: The creation date of {seedlot} cannot be superior than the event year(Mixture: {year})').format(line=line, seedlot=row[ID_SEED_LOT], year=row[EVENT_YEAR]) + '.<br>')
                                try:
                                    father_location = Location.objects.get(short_name=row[ID_SEED_LOT].split('_')[1])
                                except Location.DoesNotExist:
                                    error_messages["person_error"].append(_('line {line}:  {person}').format(line=line, person=row[ID_SEED_LOT].split('_')[1]) + ' <br>')
                                except IndexError :
                                    error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot').format(line=line, name=row[ID_SEED_LOT_SOWN]) + '<br>')
                                else:
                                    
                                    """ Germplasm creation """  
                                    
                                    try :
                                        (germplasm_obj, created) = create_germplasm(row[GERMPLASM], None, species)
                                    except NameErrorException as ne:
                                        error_messages["idgermplasm_error"].append("line {line}: {msg}<br />".format(line=line, msg=str(ne)))
                                        continue
                                    if created == True:
                                        warning_messages["warnings_germ"].append(germplasm_warnings(germplasm_obj, _('Son'), line, menu_list, species))
                                    elif created == False and row[GERMPLASM] not in mixt_dict.keys() :
                                        error_messages["idgermplasm_error"].append("line {line}: Germplasm {gp} already exists, you can't create it by a mixture<br />".format(line=line, gp=germplasm_obj))
                                        continue
                                    # seed_lot_dict contient les seed lots utilisés associés au champ ID_SEED_LOT du fichier
                                    if row[ID_SEED_LOT] in seed_lot_dict.keys():
                                        # Gestion du cas seed lot sans digit réutilisé dans le même fichier
                                        seed_lot_obj = seed_lot_dict[row[ID_SEED_LOT]]
                                    else: 
                                        # le seed lot n'a pas encore été utilisé
                                        try:
                                            # on check quel digit doit porter le nouveau seed lot si pas de digit ou simplement s'il existe dans le cas où il porte un digit
                                            seed_lot_digit = create_digit(row[ID_SEED_LOT])
                                        except ObjectDoesNotExist:
                                            error_messages["seed_lot_error"].append(_("line {line}: {seedlot} doesn't exist").format(line=line, seedlot=row[ID_SEED_LOT]) + "<br>")
                                            continue
                                        else:
                                            (germplasm_father, created) = create_germplasm(row[ID_SEED_LOT], None, species)
                                            if created == True:
                                                warning_messages["warnings_germ"].append(germplasm_warnings(germplasm_father, _('Father'), line, menu_list, species))
                                            try :
                                                date = find_date(row[ID_SEED_LOT])
                                            except :
                                                error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot').format(line=line, name=row[ID_SEED_LOT]) + '<br>')
                                                continue
                                            """we have to create the seed lot in Seedlot table (include germplasm_id and person_id)"""
                                            seed_lot_fields_dict = {'date':date,
                                                                    'quantity_ini':None,
                                                                   }
                                            try :
                                                (seed_lot_obj, created) = create_seed_lot(seed_lot_fields_dict,
                                                                                      father_location,
                                                                                      germplasm_father,
                                                                                      seed_lot_digit
                                                                                      )
                                            except Exception as e :
                                                error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                            # seed_lot_obj.project.add(project)
                                            if created == True:
                                                warning_messages["warnings_seed"].append(seed_lot_warnings(seed_lot_obj, _('father'), seed_lot_obj.quantity_ini, line))
                                                if len(row[ID_SEED_LOT].split('_')) == 3:
                                                    warning_messages["warnings_not_quantity"].append(_("line {line}: (father) {seedlot}").format(line=line, seedlot=seed_lot_obj) + ".<br>")
                                            seed_lot_dict[row[ID_SEED_LOT]] = seed_lot_obj
                                    
                                    
                                    # we check that the seed lot we are mixing is on the same farm than others mixed seed lot
                                    if row[GERMPLASM] in mixt_dict.keys():
                                        for i in range(len(mixt_dict[row[GERMPLASM]])):
                                            if row[ID_SEED_LOT].split('_')[1] not in mixt_dict[row[GERMPLASM]][i]:
                                                error_messages["error_message"].append(_("line {line}: The seed lot {seedlot} (mixture: {germplasm}) is not from {person}, make a diffusion").format(line=line, seedlot=row[ID_SEED_LOT], germplasm=row[GERMPLASM], person=mixt_dict[row[GERMPLASM]][i].split('_')[1]) + ".<br>")
                                                continue
                                        
                                        
                                    """ seed lot son (the mixed one) construction """
                                    
                                    """Gestion des quantités : A REVOIR ENTIEREMENT IL Y A MOYEN DE FAIRE PLUS SIMPLE !!!"""
                                    if row[QUANTITY] != '':
                                        if row[GERMPLASM] != melange:
                                            melange = row[GERMPLASM]
                                            previous = 0
                                            if result != 0:
                                                weight = result
                                            result_list = []
                                        
                                        result = float(row[QUANTITY]) + float(previous)
                                        previous = result
                                        result_list.append(result)
                                        """ A la dernière ligne du fichier je modifie la quantity_ini des seed lots fils:
                                            le SL fils de chaque mélange aura comme quantity_ini la somme des quantity_ini des fathers. """
                                        """ à vérifier ? """
        #                                if line == int(line_list[-1]):
        #                                    weight = result
        #                                    seed_lot_dict1[son_seed_lot_obj] = result
        #                                    for i in seed_lot_dict1.items():
        #                                        seed_lot_dict1.items()[sl][0].quantity_ini = seed_lot_dict1.items()[sl][1]
        #                                        seed_lot_dict1.items()[sl][0].save()
        #                                        warning_messages["warnings_seed"].append(seed_lot_warnings(son_seed_lot_obj, 'son', str(seed_lot_dict1.items()[sl][1]), line))
                                    
                                        
                                    """ Gestion des quantités : A REVOIR ENTIEREMENT IL Y A MOYEN DE FAIRE PLUS SIMPLE !!! """
                                    
                                    # We check if the mixed germplasm exist or not, else we create it
                                    # mixt_dict is used to keep in memory seed lot mixed in this new germplasm
                                    if row[GERMPLASM] not in mixt_dict.keys():
                                        try :
                                            (son_germplasm_obj, create) = create_germplasm(row[GERMPLASM], None, species)
                                        except NameErrorException as ne:
                                            error_messages["idgermplasm_error"].append("line {line}: %s<br />" % (line, str(ne)))
                                            continue
                                        mixt_dict[row[GERMPLASM]] = [row[ID_SEED_LOT]]
                                        if row[GERMPLASM] not in son_germlasm.keys() :
                                            son_germlasm[row[GERMPLASM]] = son_germplasm_obj
                                        if create == True:
                                            warning_messages["warnings_germ"].append(germplasm_warnings(son_germplasm_obj, _('son'), line, menu_list, species))
                                    else:
                                        if row[ID_SEED_LOT] in mixt_dict[row[GERMPLASM]]:
                                            error_messages["error_message"].append(_("line {line}: the seed lot {seedlot} is present twice for the {germplasm} mixture").format(line=line, seedlot=row[ID_SEED_LOT], germplasm=row[GERMPLASM]) + ".<br>")
                                            return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                                        else:
                                            mixt_dict[row[GERMPLASM]].append(row[ID_SEED_LOT])
                                    
                                    # we create the mixed seed lot
                                    # as a mixture is managed on several lines, we use son_seed_lot_dict to keep in memory the seed lot we created
                                    if row[GERMPLASM] in son_seed_lot_dict.keys():
                                        son_digit = son_seed_lot_dict[row[GERMPLASM]]
                                        weight, msg = get_quantity(row[QUANTITY])
                                        if msg :
                                            error_messages["error_message"].append(msg)
                                            continue
                                        mixture_dict = {
                                                        'mixture_quantity':weight,
                                                        'split':row[SPLIT]
                                                        }
                                    else:
                                        """ seed lot construction """
                                        son = row[GERMPLASM] + '_' + str(father_location) + '_' + row[EVENT_YEAR]
                                        son_digit = create_digit(son)
                                        son_seed_lot_dict[row[GERMPLASM]] = son_digit
                                        
                                        
                                        """ we create the seed lot in Seedlot table """
                                        
                                        # date_mixture = find_date(row[ID_SEED_LOT])
                                        date_mixture = row[EVENT_YEAR]
                                        seed_lot_fields_dict = {
                                                                'date':date_mixture,
                                                                'quantity_ini':result,
                                                                }
                                        try :
                                            (son_seed_lot_obj, son_create) = create_seed_lot(seed_lot_fields_dict, father_location, son_germlasm[row[GERMPLASM]], son_digit)
                                        except Exception as e :
                                                error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                        # son_seed_lot_obj.project.add(project)
                                        
                                        if son_create:
                                            warning_messages["warnings_not_quantity"].append(_('line {line}: (son) {seedlot}').format(line=line, seedlot=seed_lot_obj) + '.<br>')
                                        
                                        weight, msg = get_quantity(row[QUANTITY])
                                        if msg :
                                            error_messages["error_message"].append(msg)
                                            continue
                                        if  weight != None:      
                                            seed_lot_dict1[son_seed_lot_obj] = result

                                            
                                        """ Mixture creation """
                                        
                                        mixture_dict = {
                                                        'mixture_quantity':weight,
                                                        'split':row[SPLIT]
                                                        }
            
                                        if row[GERMPLASM] not in created_mixture.keys():
                                            mixture_relation = Mixture.objects.create(date=int(date_mixture))
                                            created_mixture[row[GERMPLASM]] = mixture_relation
                                    
                                    try:
                                        int(row[SPLIT])
                                    except ValueError:
                                        if row[SPLIT] != '':
                                            error_messages["split_string"].append(_('line {line}: The split value must be equal to 0 or 1, not {split}').format(line=line, split=row[SPLIT]) + ' <br>')
                                        if row[SPLIT] == '':
                                            (mixture, created) = create_mixture(seed_lot_obj, mixture_dict, son_seed_lot_obj, created_mixture[row[GERMPLASM]])
                                    else:
                                        if int(row[SPLIT]) != 0 and int(row[SPLIT]) != 1:
                                            error_messages["error_message"].append(_('line {line}: The split value must be equal to 0 or 1').format(line=line) + '.<br>')
                                        else:    
                                            (mixture, created) = create_mixture(seed_lot_obj, mixture_dict, son_seed_lot_obj, created_mixture[row[GERMPLASM]])
                                    if created == True:
                                        warning_messages["warnings_relation"].append(_("line {line}: {mixture} ({weight})").format(line=line, mixture=mixture, weight=row[QUANTITY]) + ".<br>")
                                        mixture.project.add(project)
                                    """ we  will create the raw_data from Rawdata table"""
                                    
                                    for i in range(len(variable_method_dict.items())):
                                        if str(list(variable_method_dict.items())[e][0]) == '' :
                                            error_messages["variable_error"].append('%s. %s<br>' % (len(error_messages["variable_error"]) + 1, _("A column containing data has an empty name")))
                                        elif str(list(variable_method_dict.items())[e][0]) not in csvfile.fieldnames:
                                            error_messages["variable_error"].append(_('{line}. {variable}  is present in the method file but is not present in the data file').format(line=len(error_messages["variable_error"]) + 1, variable=str(list(variable_method_dict.items())[e][0])) + '<br>') 
#                                         if error_messages["variable_error"]:
#                                             transaction.rollback()
#                                             return render(request, 'myuser/error.html', {'variable_error':error_messages["variable_error"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name}) 
                                    else:
                                        for i in variable_method_dict.items():
                                            #print(row[variable_method_dict.items()[e][0]])
                                            if row[list(variable_method_dict.items())[e][0]].strip() != "":                                                    
                                                try :
                                                    data = create_raw_data(list(variable_method_dict.items())[e][1][0],
                                                                            list(variable_method_dict.items())[e][1][1],
                                                                            row[str(list(variable_method_dict.items())[e][0])],
                                                                            None,
                                                                            request.user
                                                                            )
                                                except ValidationError as ve:
                                                    error_messages["error_pheno_variable"].append("line {line} : {message}".format(line=line, message=str(ve) % {'value': row[str(list(variable_method_dict.items())[e][0])+ '$date']}) + "<br />")
                                                    continue
                                                if list(variable_method_dict.items())[e][1][0].type == 'T7':
                                                    data.seedlot.add(seed_lot_obj)
                                                elif list(variable_method_dict.items())[e][1][0].type == 'T8':
                                                    data.seedlot.add(son_seed_lot_obj)
                                                elif list(variable_method_dict.items())[e][1][0].type == 'T5':
                                                    data.relation.add(mixture)
                                                elif list(variable_method_dict.items())[e][1][0].type == 'T1':
                                                    for r in created_mixture[row[GERMPLASM]].relation_set.all() :
                                                        data.relation.add(r)
                                                else :
                                                    error_messages["error_pheno_variable"].append(_("line {line} : Type {type} can't be used with {event} event".format(line=line, type=data.variable.type, event=MIXTURE))+"<br />")
                                                
                                                if row[GERMPLASM] not in mixt_dict2 :
                                                    mixt_dict2[row[GERMPLASM]] = {}
                                                if list(variable_method_dict.items())[e][0] not in mixt_dict2[row[GERMPLASM]].keys() :
                                                    mixt_dict2[row[GERMPLASM]][list(variable_method_dict.items())[e][0]] = data
                                            else :
                                                if list(variable_method_dict.items())[e][1][0].type == 'T1' and row[GERMPLASM] in mixt_dict2.keys and \
                                                    list(variable_method_dict.items())[e][0] in mixt_dict2[row[GERMPLASM]].keys() :
                                                        data.relation.add(mixture)
                                            e = e + 1
                                        e = 0  
                                
                    elif radio == REPRODUCTION:
                        if QUANTITY_SOWN not in csvfile.fieldnames:
                            error_messages["error_message"].append(_("You selected the {type} button but the file you submitted doesn't correspond to a {type} file format").format(type=radio))
                            return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name}) 
                        elif row[ID_SEED_LOT_SOWN] == '':
                            error_messages["error_row"].append(_('line {line}: seed lot cell is empty').format(line=line))
                            continue
                        else :
                            # fields = [PROJECT, SOWN_YEAR, HARVESTED_YEAR, ID_SEED_LOT_SOWN, ETIQUETTE, SPLIT, QUANTITY_SOWN, QUANTITY_HARVESTED, BLOCK, X, Y]
                            fields = [PROJECT, SOWN_YEAR, HARVESTED_YEAR, ID_SEED_LOT_SOWN, INTRA_SELECTION_NAME, ETIQUETTE, SPLIT, QUANTITY_SOWN, QUANTITY_HARVESTED, BLOCK, X, Y]
                            for l in range(len(fields)) :
                                if fields[l] not in csvfile.fieldnames:
                                    error_messages["error_message"].append(_("{field} field is missing among file fieldnames.").format(field=fields[l]) + "<br>")
                                    transaction.rollback()
                                    return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})   
                            
                            if row[PROJECT] == '' : 
                                error_messages["error_project"].append(_("line {line}: No project found").format(line=line) + "<br />")
                                continue
                            if not project or project.project_name != row[PROJECT] :
                                project, project_created = Project.objects.get_or_create(project_name=row[PROJECT], defaults={'start_date':datetime.date.today()})
                            
                            if row[X] == '' or row[Y] == '' :
                                warning_messages['xy_missing'].append(_("line {line}: X and/or Y is missing for the reproduction concerning {seedlot}").format(line=line, seedlot=row[ID_SEED_LOT_SOWN]) + "<br />")
                            
                            """ on initalise l, copy permet de creer deux espaces différents dans le disque dure pour csvfile.fieldnames et file_list.
                            à la fin de cette boucle for file list represente l'entete - colones obligatoire = variables """
                            file_list = get_variable_list(csvfile.fieldnames, fields)
                            #print("file_list", file_list)
                            
                            """ On vérifie que les éléments de file_list correspondent à ce qui se trouve dans le fichier methode """
                
                            var_list = []  # liste des variables devant figurer dans le fichier méthodes
                            var_date_list = []  # liste des variables du type 'variable$date'
    
                            for var in file_list:
                                if var.find('$') != -1:
                                    var_date_list.append(var) 
                                else:
                                    var_list.append(var)
            
                            error_file_list = check_variable_methods(var_list, variable_method_dict)
                            
                            
                        
                            if error_file_list != [] :
                                return render(request, 'myuser/error.html', {'error_file_list': error_file_list, 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                            
                            """ On vérifie que la date 'variable$date' est bien liée à une variable 'variable' existante """ 
                            error_var_date_list = check_variable_date(var_date_list, var_list)
                            
                            if error_var_date_list != [] :
                                return render(request, 'myuser/error.html', {'error_var_date_list': error_var_date_list, 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
        
                            """ Test quantity_sown numeric """
                            """if row[QUANTITY_SOWN]!="" or row[QUANTITY_SOWN]!=None:
                                try: 
                                    int(row[QUANTITY_SOWN])
                                except ValueError:
                                    error_messages["error_message"].append('line {line}: quantity sown is not an integer value<br />'%line)    
                            """
                            
                            """ emplacement des SL """
                            
                            blockX = None
                            blockY = None
                            if row[X] != "" and row[X] != None:
                                blockX = row[X]
                            if row[Y] != "" and row[Y] != None:
                                blockY = row[Y]
                            
                            """ traitement des fichiers si harvested_year n'est pas renseigné, i.e. prairies fourragères """
                            
                            if row[HARVESTED_YEAR] == '' :
                                try:
                                    int(row[SOWN_YEAR])
                                except ValueError:
                                    dict_year_sh = {row[SOWN_YEAR]:SOWN_YEAR, row[HARVESTED_YEAR]:HARVESTED_YEAR}    
                                    for i in dict_year_sh.keys():
                                        if i not in list_year:
                                            error_messages["error_sown_year"].append(_("line {line}: column '{col}', '{value}' is not a valid value").format(line=line, col=dict_year_sh[i], value=i) + "<br>")
                                else:              
#                                     try:
#                                         int(row[BLOCK])
#                                     except ValueError:   
#                                         error_messages["error_block"].append(_('line {line}: block information is missing or is not an integer value').format(line=line) + '<br />')
                                    if row[BLOCK] == '':
                                        error_messages["error_block"].append(_('line {line}: block information is missing or is not an integer value').format(line=line) + '<br />')
                                    else:
                                        try:                        
                                            """ Sometimes the following queries don't return objects that's why we put them in try statement."""                       
                                            """ we want to get the person_id in order to create it in Germplasm and Seedlot tables ."""
                                            location_obj = Location.objects.get(short_name=row[ID_SEED_LOT_SOWN].split('_')[1])
                                        except Location.DoesNotExist:
                                            error_messages["person_error"].append(_('line') + ' {line}: {seedlot} <br>'.format(line=line, seedlot=row[ID_SEED_LOT_SOWN].split('_')[1]))
                                        except IndexError :
                                            error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot').format(line=line, name=row[ID_SEED_LOT_SOWN]) + '<br>')
                                            continue
                                        else:  
                                            # Cas harvested_year nulle et SL SANS digit
                                            if len(row[ID_SEED_LOT_SOWN].split('_')) == 3:
                                                
                                                
                                                if row[ID_SEED_LOT_SOWN] in build_dict :
                                                    father_digit = build_dict[row[ID_SEED_LOT_SOWN]]
                                                else:
                    
                                                    father_digit = create_digit(row[ID_SEED_LOT_SOWN])
                                                    build_dict[row[ID_SEED_LOT_SOWN]] = father_digit
                                                
                                               
                                                
                                                """ la création de seed lot ne se fait que si ce seed lot n'existe pas déjà dans le fichier (cad dans le dictionnaire qui collecte les seed lot du fichier)"""
                                                
                                                if father_digit in seed_lot_dict.keys():
                                                    """ pas besoin de creer le seed_lot dans la table seed_lot, on le récupère du dictionnaire."""
                                                    father_obj = seed_lot_dict[father_digit]
                                                    
                                               
                                                   
                                                                                                
                                                # son_obj = seed_lot_dict[son_digit]
                                                else:   
                                                    """ Germplasm creation in germplasm table  """
                                                    
                                                    
                                                    try :
                                                        (germplasm_obj, create) = create_germplasm(row[ID_SEED_LOT_SOWN], None, species)
                                                    except NameErrorException as ne:
                                                        error_messages["idgermplasm_error"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ne)))
                                                        continue
                                                    
                                                    if create == True:
                                                        warning_messages["warnings_germ"].append(germplasm_warnings(germplasm_obj, _('father/son'), line, menu_list, species))
                                                        """ we have to create the seed lot in Seedlot table (include germplasm_id and person_id)"""
                                                        
                                                    weight, msg = get_quantity(row[QUANTITY_HARVESTED])
                                                    if msg :
                                                        error_messages["error_message"].append(msg)
                                                        continue
                                                    
                                                    try :
                                                        date = find_date(row[ID_SEED_LOT_SOWN])
                                                    except :
                                                        error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot').format(line=line, name=row[ID_SEED_LOT_SOWN]) + '<br>')
                                                        continue
                                                    
                                                    """ si la date de creation est inconnue """
                                                    """if date == "XXXX":
                                                        date = None
                                                    else:
                                                        date = date"""
                                                    
                                                
                                                    seed_lot_fields_dict = {'date': date, 'quantity_ini': None}   
                                                    
                                                    """ father seed_lot creation """
                                                    try :
                                                        (father_obj, create) = create_seed_lot(seed_lot_fields_dict, location_obj, germplasm_obj, father_digit)
                                                    except Exception as e :
                                                        error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                                    # father_obj.project.add(project)
                                                    
                                                    if create == True:
                                                        warning_messages["warnings_seed"].append(seed_lot_warnings(father_obj, _('father'), father_obj.quantity_ini, line))
                                                        warning_messages["warnings_not_quantity"].append(_('line {line}: (son) {seedlot}').format(line=line, seedlot=father_obj) + '.<br>')
                                                    seed_lot_dict[father_digit] = father_obj
                                                    
                                                """ cas des répétitions """
                                                """ Update quantity_ini du Seed lot  """
    #                                             if duplication == True:
    #                                                      
    #                                                 oldVal = seed_lot_fields_dict['quantity_ini']
    #                                                 newVal = int(oldVal)+int(row[QUANTITY_SOWN])                                                                
    #                                                 seed_lot_fields_dict['quantity_ini'] = newVal
    #                                                 father_obj.quantity_ini = newVal
    #                                                 father_obj.save()
           
                                                # son_obj = seed_lot_dict[son_digit]
                                                existing_relations = None
                                                if row[INTRA_SELECTION_NAME] == '' :
                                                    if blockX and blockY :
                                                        existing_relations = Relation.objects.filter(seed_lot_father__location=location_obj, seed_lot_father__germplasm__species=species, reproduction__start_date=int(row[SOWN_YEAR]), block=row[BLOCK], X=blockX, Y=blockY, selection=None)
                                                    if existing_relations:
                                                        error_messages['sowing_block_error'].append(_("line {line}: A reproduction already exist in {year} on {location} farm at block={block}, X={X} and Y={Y}".format(line=line,year=row[SOWN_YEAR],location=location_obj,block=row[BLOCK],X=blockX,Y=blockY))+"<br />")
                                                        continue
                                                    else :
                                                        """ relation creation """  # a tester
                                                        #reproduction_date = row[SOWN_YEAR] + '-' + 'Unknown harvested_year'  
                                                        quantity, msg = get_quantity(row[QUANTITY])
                                                        if msg :
                                                            error_messages["error_message"].append(msg)
                                                            continue
                                                        reproduction_dict = {'block': row[BLOCK], 'start_date' : row[SOWN_YEAR], 'end_date' : None, 'X' : blockX, 'Y' : blockY, 'quantity' : quantity, 'split':row[SPLIT]}
                                                        try :
                                                            reproduction_obj = create_reproduction(father_obj, None, reproduction_dict)
                                                        except ValidationError as ve :
                                                            error_messages["error_message"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ve)))
                                                            continue
                                                        reproduction_obj.project.add(project)
                                                else :
                                                    error_messages['sowing_block_error'].append(_("line {line}: The intra seed lot selection you try to use for this reproduction doesn't exists. You should create it before".format(line=line)+"<br />"))
                        
                                            
                                            # Cas harvested_year nulle et SL AVEC digit        
                                            elif len(row[ID_SEED_LOT_SOWN].split('_')) == 4:
                                                """We want to verify for seeds lots with # and digit if the same seed lot without # exists (its relation and reproduction too."""
                                                try:
                                                    father_obj = Seedlot.objects.get(name=row[ID_SEED_LOT_SOWN])
                                                except Seedlot.DoesNotExist:
                                                    error_messages["seed_lot_error"].append(_("line {line}: {seedlot} doesn't exist").format(line=line, seedlot=row[ID_SEED_LOT_SOWN]) + "<br>")
                                                    continue
                                            else :
                                                error_messages["seed_lot_error"].append(_('line {line}: {seedlot} is not a valid name for a seed lot').format(line=line, seedlot=row[ID_SEED_LOT_SOWN]) + '<br>')
                                                continue

                                                    
                                            try :
                                                if row[INTRA_SELECTION_NAME] == '' :
                                                    reproduction_obj = Relation.objects.get(seed_lot_father=father_obj, reproduction__start_date=int(row[SOWN_YEAR]), block=row[BLOCK], X=blockX, Y=blockY, selection=None)
                                                else :
                                                    reproduction_obj = Relation.objects.get(seed_lot_father=father_obj, reproduction__start_date=int(row[SOWN_YEAR]), block=row[BLOCK], X=blockX, Y=blockY, selection__selection_name=row[INTRA_SELECTION_NAME])
                                                son_obj = reproduction_obj.seed_lot_son
                                                if row[QUANTITY_HARVESTED] != '' and (son_obj.quantity_ini == None or son_obj.quantity_ini == '') :
                                                    quantity, msg = get_quantity(row[QUANTITY])
                                                    if msg :
                                                        error_messages["error_message"].append(msg)
                                                        continue
                                                    son_obj.quantity_ini = quantity
                                                    son_obj.save()
                                            # b. la relation n'existe pas
                                            except Relation.DoesNotExist :
                                                """ Reproduction creation """        
                                                existing_relations = None
                                                if row[INTRA_SELECTION_NAME] == '' :
                                                    if blockX and blockY :
                                                        existing_relations = Relation.objects.filter(seed_lot_father__location=location_obj, seed_lot_father__germplasm__species=species, reproduction__start_date=int(row[SOWN_YEAR]), block=row[BLOCK], X=blockX, Y=blockY, selection=None)
                                                    if existing_relations:
                                                        error_messages['sowing_block_error'].append(_("line {line}: A reproduction already exist in {year} on {location} farm at block={block}, X={X} and Y={Y}".format(line=line,year=row[SOWN_YEAR],location=location_obj,block=row[BLOCK],X=blockX,Y=blockY))+"<br />")
                                                        continue
                                                    else :
                                                        #reproduction_date = row[SOWN_YEAR] + '-' + 'Unknown harvested_year'  
                                                        quantity, msg = get_quantity(row[QUANTITY])
                                                        if msg :
                                                            error_messages["error_message"].append(msg)
                                                            continue
                                                        reproduction_dict = {'block': row[BLOCK], 'start_date' : row[SOWN_YEAR], 'X' : blockX, 'Y' : blockY, 'quantity' : quantity, 'split': row[SPLIT]}
                                                        try :
                                                            reproduction_obj = create_reproduction(father_obj, None, reproduction_dict)
                                                        except ValidationError as ve:
                                                            error_messages["error_message"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ve)))
                                                            continue
                                                        reproduction_obj.project.add(project)
                                                else :
                                                    error_messages['sowing_block_error'].append(_("line {line}: The intra seed lot selection you try to use for this reproduction doesn't exists. You should create it before".format(line=line)+"<br />"))
                                            else:
                                                son_obj = reproduction_obj.seed_lot_son
                                                   
                                            # warning_messages["warnings_relation"].append('line {line}: %s.<br>' % (line, str(reproduction_obj)))  
                            elif row[HARVESTED_YEAR] != '' :
                                """ HARVESTED_YEAR est non NULLE """
                                """ SOWN_YEAR et HARVESTED_YEAR comprises entre 1900 et 3000 ? """ 
         
                                try:
                                    int(row[SOWN_YEAR])
                                    int(row[HARVESTED_YEAR])
                                    if int(row[SOWN_YEAR]) > int(row[HARVESTED_YEAR]):
                                        error_messages["error_sown_year"].append(_('line {line}: sown year is higher than harvested year').format(line=line) + '<br>')
                                except ValueError:
                                    dict_year_sh = {row[SOWN_YEAR]:SOWN_YEAR, row[HARVESTED_YEAR]:HARVESTED_YEAR}    
                                    for i in dict_year_sh.keys():
                                        if i not in list_year:
                                            error_messages["error_sown_year"].append(_("line {line}: column '{col}', '{value}' is not a valid value").format(line=line, col=dict_year_sh[i], value=i) + "<br>")
                                else:                           
                                    try:                        
                                        """ Sometimes the following queries don't return objects that's why we put them in try statement."""                       
                                        """ we want to get the person_id in order to create it in Germplasm and Seedlot tables ."""
                                        #print(row[ID_SEED_LOT_SOWN].split('_')[1])
                                        location_obj = Location.objects.get(short_name=row[ID_SEED_LOT_SOWN].split('_')[1])
                                    except Location.DoesNotExist:
                                        error_messages["person_error"].append(_('line') + ' {line}: {seedlot} <br>'.format(line=line, seedlot=row[ID_SEED_LOT_SOWN].split('_')[1]))
                                    except IndexError :
                                        error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot').format(line=line, name=row[ID_SEED_LOT_SOWN]) + '<br>')
                                    else:
                                        # Cas harvested_year non nulle et SL SANS digit
                                        if len(row[ID_SEED_LOT_SOWN].split('_')) == 3:
                                            
                                            try :
                                                date = find_date(row[ID_SEED_LOT_SOWN])
                                            except :
                                                error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot').format(line=line, name=row[ID_SEED_LOT_SOWN]) + '<br>')
                                                continue
                                            
                                            """date = find_date(row[ID_SEED_LOT_SOWN])
                                            si la date de creation est inconnue
                                            if date == "XXXX":
                                                date = None"""
                                            
                                            quantity, msg = get_quantity(row[QUANTITY_SOWN])
                                            if msg :
                                                error_messages["error_message"].append(msg)
                                                continue
                                            seed_lot_fields_dict = {'date':date, 'quantity_ini':None}
                                            if row[ID_SEED_LOT_SOWN] in build_dict :
                                                father_digit = build_dict[row[ID_SEED_LOT_SOWN]]
                                            else:
                                                father_digit = create_digit(row[ID_SEED_LOT_SOWN])
                                                build_dict[row[ID_SEED_LOT_SOWN]] = father_digit
                                                seed_lot_fields_dict = {'date':date, 'quantity_ini':None}        
                                            
                                            #reproduction_date = row[SOWN_YEAR] + '-' + row[HARVESTED_YEAR]
                                            
        
                                            """ Germplasm creation in germplasm table  """
                                            location_obj = Location.objects.get(short_name=row[ID_SEED_LOT_SOWN].split('_')[1])
                                            
                                            try : 
                                                (germplasm_obj, created) = create_germplasm(row[ID_SEED_LOT_SOWN].split('_')[0], None, species)
                                            except NameErrorException as ne:
                                                error_messages["idgermplasm_error"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ne)))
                                                continue
                                            if created == True:
                                                warning_messages["warnings_germ"].append(germplasm_warnings(germplasm_obj, _('Father'), line, menu_list, species))
        
                                            """ father_obj creation """
                                            
                                            try :
                                                father_obj = create_seed_lot(seed_lot_fields_dict, location_obj, germplasm_obj, father_digit)[0]
                                            except Exception as e :
                                                error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                            
                                            
                                            # if father_obj:
                                                # warning_messages["warnings_species"].append(species_warnings(line,father_obj,germplasm_obj, species))
                                            """ son seed_lot creation """
                                            harvested_qty = get_quantity(row[QUANTITY_HARVESTED])
                                            if msg :
                                                error_messages["error_message"].append(msg)
                                                continue

                                            if harvested_qty == 0:
                                                son = row[ID_SEED_LOT_SOWN].split('_')[0] + "_" + row[ID_SEED_LOT_SOWN].split('_')[1] + "_" + row[HARVESTED_YEAR]
                                                son_digit = create_digit(son)
                                                son_digit = 'NOT_HARVESTED_' + son_digit
                                            else :
                                                son = row[ID_SEED_LOT_SOWN].split('_')[0] + "_" + row[ID_SEED_LOT_SOWN].split('_')[1] + "_" + row[HARVESTED_YEAR]
                                                son_digit = create_digit(son) 
                                                
                                            
                                            date = row[HARVESTED_YEAR]
                                            seed_lot_fields_dict = {'date':date, 'quantity_ini':harvested_qty}
                                            try :
                                                (son_obj, create) = create_seed_lot(seed_lot_fields_dict, location_obj, germplasm_obj, son_digit)
                                            except Exception as e :
                                                error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                            # son_obj.project.add(project)
                                            
                                            if create == True :
                                                warning_messages["warnings_seed"].append(seed_lot_warnings(son_obj, _('son'), son_obj.quantity_ini, line))
                                                # warning_messages["warnings_species"].append(species_warnings(line,son_obj,germplasm_obj, species))
                                                # print(warning_messages["warnings_species"])
                                            
                                            
                                            """ reproduction_obj creation """
                                            existing_relations = None
                                            if blockX and blockY :
                                                existing_relations = Relation.objects.filter(seed_lot_father__location=location_obj, seed_lot_father__germplasm__species=species, reproduction__start_date=int(row[SOWN_YEAR]), block=row[BLOCK], X=blockX, Y=blockY, selection=None)
                                            if existing_relations:
                                                error_messages['sowing_block_error'].append(_("line {line}: A reproduction already exist in {year} on {location} farm at block={block}, X={X} and Y={Y}".format(line=line,year=row[SOWN_YEAR],location=location_obj,block=row[BLOCK],X=blockX,Y=blockY))+"<br />")
                                                continue
                                            else :
                                                reproduction_dict = {'block': row[BLOCK], 'start_date' : row[SOWN_YEAR], 'end_date': row[HARVESTED_YEAR], 'X' : blockX, 'Y' : blockY, 'quantity':quantity, 'split': row[SPLIT]} 
                                                try :
                                                    reproduction_obj = create_reproduction(father_obj, son_obj, reproduction_dict)
                                                except ValidationError as ve:
                                                    error_messages["error_message"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ve)))
                                                    continue
                                                reproduction_obj.project.add(project)
                                                if create == True:
                                                    warning_messages["warnings_seed"].append(seed_lot_warnings(father_obj, _('father'), father_obj.quantity_ini, line))
                                                    warning_messages["warnings_not_quantity"].append(_('line {line}: (son) {seedlot}').format(line=line, seedlot=father_obj) + '.<br>')
                                                warning_messages["warnings_relation"].append(_('line') + " {line}: {reproduction}.<br />&nbsp;&nbsp;&nbsp;&nbsp;".format(line=line, reproduction=reproduction_obj) + _("Choose the reproduction type") + " : &nbsp;&nbsp;<select name='reproduction_{id}'>{methods}</select><br /><center>***</center><br />".format(id=reproduction_obj.reproduction.id, methods=reproduction_method_list))     
                                                if father_obj.name not in rep_manager.keys():
                                                    rep_manager[father_obj.name] = {'replist':[],
                                                                                    'project':project}
                                                if (row[BLOCK], blockX, blockY, son_obj) not in rep_manager[father_obj.name]['replist']:
                                                    rep_manager[father_obj.name]['replist'].append((row[BLOCK], blockX, blockY, son_obj))
                                                # else :
                                                    # error_messages["error_block"].append('line {line}: information concerning relation %s --> %s is duplicated in this file<br />'%(line,father_obj,son_obj))
                                               
                                                """ on vérifie que la sélection existe """ 
                                                #relation_date = row[HARVESTED_YEAR]
                                                if row[INTRA_SELECTION_NAME] != '':
                                                    try:
                                                        selection_obj = Relation.objects.get(seed_lot_son=son_obj, reproduction__start_date=int(row[SOWN_YEAR]), reproduction__end_date=int(row[HARVESTED_YEAR]), block=row[BLOCK], X=blockX, Y=blockY, selection__selection_name=row[INTRA_SELECTION_NAME])
                                                    except Relation.DoesNotExist:
                                                        error_messages["error_message"].append(_('line {line}: The selection {selection} does not exist (block {block}) for this seed lot').format(line=line, selection=row[INTRA_SELECTION_NAME], block=row[BLOCK]) + '.<br>')  
                                                
                                                    
              
                                            
                                        # Cas harvested_year non nulle et SL AVEC digit
             
                                        elif len(row[ID_SEED_LOT_SOWN].split('_')) == 4:
                                            
                                            """ le seed lot existe t-il dans la base ? """  
                                            father_obj = None
                                            try:
                                                father_obj = Seedlot.objects.get(name=row[ID_SEED_LOT_SOWN]) 
                                            except:    
                                                error_messages["error_message"].append(_('line {line} : The seed lot {seedlot} does not exist').format(line=line, seedlot=row[ID_SEED_LOT_SOWN]))
                                                continue
                                            
                                            """ Does Relation exist ? """
                                            if row[ID_SEED_LOT_SOWN] in build_dict :
                                                father_digit = build_dict[row[ID_SEED_LOT_SOWN]]
                                            else:
                                                try:
                                                    father_digit = create_digit(row[ID_SEED_LOT_SOWN])
                                                except:
                                                    error_messages["error_message"].append(_('line {line} : The seed lot {seedlot} does not exist').format(line=line, seedlot=row[ID_SEED_LOT_SOWN]) + '<br>')
                                                else:
                                                    build_dict[row[ID_SEED_LOT_SOWN]] = father_digit
                                                    
                                            """ la création de seed lot ne se fait que si ce seed lot n'existe pas déjà dans le fichier (cad dans le dictionnaire qui collecte les seed lot du fichier)"""
                                            if father_digit in seed_lot_dict.keys():
                                                """ pas besoin de creer le seed_lot dans la table seed_lot, on le récupère du dictionnaire."""
                                                father_obj = seed_lot_dict[father_digit]
                                            
        
                                            #relation_date = reproduction_date = row[SOWN_YEAR] + '-' + _('Unknown harvested_year')
                                            
                                            try:
                                                reproduction_obj = Relation.objects.get(seed_lot_father__name=row[ID_SEED_LOT_SOWN], block=row[BLOCK], reproduction__start_date=int(row[SOWN_YEAR]), reproduction__end_date=int(row[HARVESTED_YEAR]), X=blockX, Y=blockY, selection=None)
                                                father_obj = reproduction_obj.seed_lot_father
                                                son_obj = reproduction_obj.seed_lot_son
                                                harvested_qty, msg = get_quantity(row[QUANTITY_HARVESTED])
                                                if msg:
                                                    error_messages["error_message"].append(msg)
                                                    continue
                                                if harvested_qty != None and (son_obj.quantity_ini == None or son_obj.quantity_ini == '') :
                                                    son_obj.quantity_ini = harvested_qty
                                                    son_obj.save()
                                            # a. la relation n'existe pas 
                                            except Relation.DoesNotExist:
                                                existing_relations = None
                                                if blockX and blockY :
                                                    existing_relations = Relation.objects.filter(seed_lot_father__location=location_obj, seed_lot_father__germplasm__species=species, reproduction__start_date=int(row[SOWN_YEAR]), block=row[BLOCK], X=blockX, Y=blockY, selection=None)
                                                if existing_relations:
                                                    error_messages['sowing_block_error'].append(_("line {line}: A reproduction already exist in {year} on {location} farm at block={block}, X={X} and Y={Y}".format(line=line,year=row[SOWN_YEAR],location=location_obj,block=row[BLOCK],X=blockX,Y=blockY))+"<br />")
                                                    continue
                                                else :
                                                    """son seed lot creation"""
                                                    date = row[HARVESTED_YEAR]
                                                    harvested_qty, msg = get_quantity(row[QUANTITY_HARVESTED])
                                                    if msg:
                                                        error_messages["error_message"].append(msg)
                                                        continue
                                                    seed_lot_fields_dict = {'date':date,
                                                                            'quantity_ini':harvested_qty,
                                                                            }
                                                    # son = row[ID_SEED_LOT_SOWN].replace(row[ID_SEED_LOT_SOWN].split('_')[2], row[HARVESTED_YEAR])
                                                    son = row[ID_SEED_LOT_SOWN].split('_')[0] + "_" + row[ID_SEED_LOT_SOWN].split('_')[1] + "_" + row[HARVESTED_YEAR]
                                                    son_digit = create_digit(son)
                                                    try :
                                                        (germplasm_obj, create) = create_germplasm(row[ID_SEED_LOT_SOWN], None, species)
                                                    except NameError as ne:
                                                        error_messages["idgermplasm_error"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ne)))
                                                        continue
                                                    if create == True:
                                                        warning_messages["warnings_germ"].append(germplasm_warnings(germplasm_obj, _('Son'), line, menu_list, species))
                                                    
                                                    try :
                                                        (son_obj, create) = create_seed_lot(seed_lot_fields_dict,
                                                                                              location_obj,
                                                                                              germplasm_obj,
                                                                                              son_digit,
                                                                                              )
                                                    except Exception as e :
                                                        error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                                    # son_obj.project.add(project)
                                                    if create == True:
                                                        warning_messages["warnings_seed"].append(seed_lot_warnings(son_obj, _('son'), son_obj.quantity_ini, line))
                                                        warning_messages["warnings_not_quantity"].append(_("line {line}: (son) {seedlot}").format(line=line, seedlot=son_obj) + ".<br>")
                                                        seed_lot_dict[son_digit] = son_obj
                                                    """ Reproduction creation"""
                                                    blockX = None
                                                    blockY = None
                                                    if row[X] != "" and row[X] != None:
                                                        blockX = row[X]
                                                    if row[Y] != "" and row[Y] != None:
                                                        blockY = row[Y]
                                                    quantity, msg = get_quantity(row[QUANTITY_SOWN])
                                                    if msg :
                                                        error_messages["error_message"].append(msg)
                                                        continue
                                                    reproduction_dict = {'block': row[BLOCK],
                                                                         'start_date' : row[SOWN_YEAR],
                                                                         'end_date' : row[HARVESTED_YEAR],
                                                                         'X' : blockX,
                                                                         'Y' : blockY,
                                                                         'quantity':quantity,
                                                                         'split': row[SPLIT]
                                                                      }
                                                    try :  
                                                        reproduction_obj = create_reproduction(father_obj, son_obj, reproduction_dict)
                                                    except ValidationError as ve:
                                                        error_messages["error_message"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ve)))
                                                        continue
                                                    reproduction_obj.project.add(project)
                                                    warning_messages["warnings_relation"].append("{transline}  {line}: {reproduction}.<br />&nbsp;&nbsp;&nbsp;&nbsp;{transchoose} : &nbsp;&nbsp;<select name='reproduction_{id}'>{type}</select><br /><center>***</center><br />".format(
                                                                                    transline=_("line"), line=line, reproduction=reproduction_obj, transchoose=_("Choose the reproduction type"),
                                                                                    id=reproduction_obj.reproduction.id, type=reproduction_method_list))
                                                # warning_messages["reproduction_"].append('<tr><td>%s --> %s has been created</td></tr>')
                            if father_obj.name not in rep_manager.keys():
                                rep_manager[father_obj.name] = {'replist':[],
                                                                'project':project}
                            if (row[BLOCK], blockX, blockY, son_obj) not in rep_manager[father_obj.name]['replist']:
                                rep_manager[father_obj.name]['replist'].append((row[BLOCK], blockX, blockY, son_obj))
                            # warning_messages["warnings_relation"].append('line {line}: %s.<br>' % (line, str(reproduction_obj)))
                            
  
                            for i in variable_method_dict.items() :
                                """ we  will create the raw_data from Rawdata table"""
                                if str(list(variable_method_dict.items())[e][0]) == '' :
                                   
                                    error_messages["variable_error"].append('%s. %s<br>' % (len(error_messages["variable_error"]) + 1, _("A column containing data has an empty name")))
                                elif str(list(variable_method_dict.items())[e][0]) not in csvfile.fieldnames:
                                    
                                    error_messages["variable_error"].append(_('{line}. {variable}  is present in the method file but is not present in the data file').format(line=len(error_messages["variable_error"]) + 1, variable=str(list(variable_method_dict.items())[e][0])) + '<br>') 
                                else:
                                  
                                    if row[str(list(variable_method_dict.items())[e][0])] != "" :
                                        data = None
                                        """str(list(variable_method_dict.items())[e][0]) correspond à la variable"""
                                        if list(variable_method_dict.items())[e][0] + '$date' in file_list and row[str(list(variable_method_dict.items())[e][0]) + '$date'] != '':
                                            try :
                                                with transaction.atomic() :
                                                    data = create_raw_data(list(variable_method_dict.items())[e][1][0],
                                                                                list(variable_method_dict.items())[e][1][1],
                                                                                row[list(variable_method_dict.items())[e][0]],
                                                                                row[str(list(variable_method_dict.items())[e][0]) + '$date'],
                                                                                request.user   
                                                                                )
                                            except ValidationError as ve:
#                                                 print(list(variable_method_dict.items())[e][1][0])
#                                                 print("-->{0}<--".format(row[str(list(variable_method_dict.items())[e][0]) + '$date']))
                                                error_messages["error_pheno_variable"].append("line {line} : {message}".format(line=line, message=str(ve) % {'value': row[str(list(variable_method_dict.items())[e][0])+ '$date']}) + "<br />")
                                        else:
                                            """str(list(variable_method_dict.items())[e][0]) correspond à la variable"""
                                            try :
                                                with transaction.atomic() :
                                                    data = create_raw_data(list(variable_method_dict.items())[e][1][0],
                                                                                list(variable_method_dict.items())[e][1][1],
                                                                                row[list(variable_method_dict.items())[e][0]],
                                                                                None,
                                                                                request.user)
                                
                                            except ValidationError as ve:
                                                #print(list(variable_method_dict.items())[e][1][0])
                                                error_messages["error_pheno_variable"].append("line {line} : {message}".format(line=line, message=str(ve) % {'value': row[str(list(variable_method_dict.items())[e][0])+ '$date']}) + "<br />")
                                       
                                        if data :
                                            if str(data.variable.type) == 'T1':
                                                data.relation.add(reproduction_obj) 
                                            elif str(data.variable.type) == 'T7':
                                                data.seedlot.add(father_obj)
                                            elif str(data.variable.type) == 'T8':
                                                data.seedlot.add(son_obj)
                                            else :
                                                error_messages["error_pheno_variable"].append(_("line {line} : Type {type} can't be used with {event} event".format(line=line, type=data.variable.type, event=REPRODUCTION))+"<br />")
                                e = e + 1
                            e = 0
                           
                            if error_messages["variable_error"]:
                                transaction.rollback()
                                return render(request, 'myuser/error.html', {'variable_error':error_messages["variable_error"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                            
                                        
                    elif radio == INTRA_VARIETAL:

                        fields = [PROJECT, SOWN_YEAR, HARVESTED_YEAR, ID_SEED_LOT_SOWN, ETIQUETTE, BLOCK, X, Y, SELECTION_PERSON, SELECTION_QUANTITY_INI, SELECTION_NAME]           
                        if SELECTION_NAME not in csvfile.fieldnames :
                            error_messages["error_message"].append(_("You selected the {type} button but the file you submitted doesn't correspond to a {type} file format").format(type=radio))
                            return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name}) 
                    
                        else:
                            if row[PROJECT] == '' : 
                                error_messages["error_project"].append(_("line {line}: No project found").format(line=line) + "<br />")
                                continue
                            if not project or project.project_name != row[PROJECT] :
                                project, project_created = Project.objects.get_or_create(project_name=row[PROJECT], defaults={'start_date':datetime.date.today()})
                            
                            if row[X] == '' or row[Y] == '' :
                                warning_messages['xy_missing'].append(_("line {line}: X and/or Y is missing for the reproduction concerning").format(line=line) + " {seedlot}<br />".format(seedlot=row[ID_SEED_LOT_SOWN]))
                            
                            """on initalise l, copy permet de creer deux espaces différents dans le disque dure pour csvfile.fieldnames et file_list.
                            à la fin de cette boucle for file list represente l'entete - colones obligatoire = variables"""
                            file_list = get_variable_list(csvfile.fieldnames, fields)
                            
                            """ On vérifie que les éléments de file_list correspondent à ce qui se trouve dans le fichier methode """
                
                            var_list = []  # liste des variables devant figurer dans le fichier méthodes
                            var_date_list = []  # liste des variables du type 'variable$date'
                            
                            for var in file_list:
                                if var.find('$') != -1:
                                    var_date_list.append(var) 
                                else:
                                    var_list.append(var)
            
                            error_file_list = check_variable_methods(var_list, variable_method_dict)
                            
                            if error_file_list != [] :
                                return render(request, 'myuser/error.html', {'error_file_list': error_file_list, 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                            
                            """ On vérifie que la date 'variable$date' est bien liée à une variable 'variable' existante """ 
                            error_var_date_list = check_variable_date(var_date_list, var_list)
                            
                            if error_var_date_list != [] :
                                return render(request, 'myuser/error.html', {'error_var_date_list': error_var_date_list, 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                      
                            if (row[SELECTION_PERSON] != '' and row[SELECTION_NAME] == '') or (row[SELECTION_PERSON] == '' and row[SELECTION_NAME] != ''):
                                print("'selection_person' and 'selection_name' should be both filled or both empty")
                                error_messages["error_message"].append(_("line {line}: the columns 'selection_person' and 'selection_name' should be both filled or both empty").format(line=line) + ".<br>")
                                return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                           
                        
                            """vérification que SOWN_YEAR et HARVESTED_YEAR sont bien une date comprise entre 1900 et 3000"""
                            try:
                                int(row[SOWN_YEAR])
                                int(row[HARVESTED_YEAR])
                                if int(row[SOWN_YEAR]) > int(row[HARVESTED_YEAR]) :
                                    error_messages["error_sown_year"].append(_('line {line}: sown year is higher than harvested year').format(line=line) + '<br>')
                            except ValueError:
                                dict_year_sh = {row[SOWN_YEAR]:SOWN_YEAR, row[HARVESTED_YEAR]:HARVESTED_YEAR}    
                                for i in dict_year_sh.keys():
                                    if i not in list_year:
                                        error_messages["error_sown_year"].append(_("line {line}: column '{col}', '{value}' is not a valid value").format(line=line, col=dict_year_sh[i], value=i) + "<br>")
                            else:  
#                                 try:
#                                     int(row[BLOCK])
#                                 except ValueError:   
#                                     error_messages["error_block"].append(_('line {line}: block information is missing or is not an integer value').format(line=line) + '<br />')
                                if row[BLOCK] == '' :
                                    error_messages["error_block"].append(_('line {line}: block information is missing or is not an integer value').format(line=line) + '<br />')
                                else:                     
                                    try:                        
                                        """Some times the following queries don't return objects that's why we put them in try statement.                      
                                         we want to get the person_id in order to create it in Germplasm and Seedlot tables ."""
                                        
                                        location_obj = Location.objects.get(short_name=row[ID_SEED_LOT_SOWN].split('_')[1])
                                    except Location.DoesNotExist:
                                        
                                        error_messages["person_error"].append(_('line') + ' {line}: {seedlot} <br>'.format(line=line, seedlot=row[ID_SEED_LOT_SOWN].split('_')[1]))
                                    except IndexError :
                                        
                                        error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot').format(line=line, seedlot=row[ID_SEED_LOT_SOWN]) + '<br>')
                                    else:
                                    
                                        if len(row[ID_SEED_LOT_SOWN].split('_')) == 3:
                                           
                                            """ Germplasm creation in germplasm table  """
                                            try :
                                                (germplasm_obj, create) = create_germplasm(row[ID_SEED_LOT_SOWN], None, species)
                                            except NameErrorException as ne:
                                                error_messages["idgermplasm_error"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ne)))
                                                continue
                                            if create == True:
                                                warning_messages["warnings_germ"].append(germplasm_warnings(germplasm_obj, _('father'), line, menu_list, species))                                    
                                            if row[ID_SEED_LOT_SOWN] in build_dict :
                                                father_obj = build_dict[row[ID_SEED_LOT_SOWN]]
                                                sonm_obj = build_dict1[row[ID_SEED_LOT_SOWN]]
                                            else:
                                                father_digit = create_digit(row[ID_SEED_LOT_SOWN])                        
                                                try :
                                                    date = find_date(row[ID_SEED_LOT_SOWN])
                                                except :
                                                    error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot').format(line=line, name=row[ID_SEED_LOT_SOWN]) + '<br>')
                                                    continue
                                                seed_lot_fields_dict = {'date':date,
                                                                        'quantity_ini':None,
                                                                        }
                                                try :
                                                    (father_obj, create) = create_seed_lot(seed_lot_fields_dict,
                                                                                  location_obj,
                                                                                  germplasm_obj,
                                                                                  father_digit,
                                                                                  )
                                                except Exception as e :
                                                    error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                                # father_obj.project.add(project)
                                                if create == True:
                                                    warning_messages["warnings_seed"].append(seed_lot_warnings(father_obj, _('father'), father_obj.quantity_ini, line))
                                                    warning_messages["warnings_not_quantity"].append(_("line {line}: (father) {seedlot}").format(line=line, seedlot=father_obj) + ".<br>")
                                                    # if row[SELECTION_PERSON] =='' and row[SELECTION_NAME] =='':
                                                son = row[ID_SEED_LOT_SOWN].replace(row[ID_SEED_LOT_SOWN].split('_')[2], row[HARVESTED_YEAR])
                                                son_digit = create_digit(son)
                                                
                                                """multiplication son seed lot creation"""
                                                
                                                date = row[HARVESTED_YEAR]
                                                seed_lot_fields_dict = {'date':date,
                                                                       'quantity_ini':None,
                                                                       }
                                                try :
                                                    (sonm_obj, create) = create_seed_lot(seed_lot_fields_dict,
                                                                                      location_obj,
                                                                                      germplasm_obj,
                                                                                      son_digit,
                                                                                      )
                                                except Exception as e :
                                                    error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                                # sonm_obj.project.add(project)
                                                build_dict1[row[ID_SEED_LOT_SOWN]] = sonm_obj
                                                if create == True:                                            
                                                        warning_messages["warnings_seed"].append(seed_lot_warnings(sonm_obj, _('son'), sonm_obj.quantity_ini, line))
                                                        warning_messages["warnings_not_quantity"].append(_('line {line}: (son) {seedlot}'.format(line=line,seedlot=sonm_obj)) + '.<br>')
                                                build_dict[row[ID_SEED_LOT_SOWN]] = father_obj
                                            if row[SELECTION_PERSON] != '' and row[SELECTION_NAME] != '':                   
                                                if '#' in row[ID_SEED_LOT_SOWN]: 
                                                    select_test = row[ID_SEED_LOT_SOWN].split('_')[0]
                                                    select_test = select_test.split('#')[1]
                                                    if select_test == row[SELECTION_NAME]:
                                                        error_messages["error_message"].append(_('line {line}: Selection name {selection} is already used for germplasm').format(line=line, selection=row[SELECTION_NAME]) + ' {germplasm}.<br>'.format(germplasm=germplasm_obj))
                                                    son_name3 = row[ID_SEED_LOT_SOWN].split('#')[0] + '#' + row[SELECTION_NAME]
                                                else:
                                                    son_name3 = row[ID_SEED_LOT_SOWN].split('_')[0] + '#' + row[SELECTION_NAME]
                                            
                                                check = Seedlot.objects.filter(name__startswith=son_name3 + "_").values_list('name', flat=True)
                                                if len(check) != 0:
                                                    error_messages["error_message"].append(_('line {line}: Selection name {selection} is already used for germplasm').format(line=line, selection=row[SELECTION_NAME]) + ' {germplasm}.<br>'.format(germplasm=germplasm_obj))
                                                elif len(check) == 0:
                                                    son_name3 = son_name3 + '_' + row[ID_SEED_LOT_SOWN].split('_')[1] + '_' + row[HARVESTED_YEAR]
                                                    
                                            son_digit = create_digit(son_name3)
                                           
                                            
                                            date = row[HARVESTED_YEAR]
                                            seed_lot_fields_dict = {'date':date,
                                                                    'quantity_ini':None,
                                                                    }
                                            try :
                                                (son_obj, create) = create_seed_lot(seed_lot_fields_dict,
                                                                              location_obj,
                                                                              germplasm_obj,
                                                                              son_digit,
                                                                              )
                                            except Exception as e :
                                                error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                            # son_obj.project.add(project)
                                            
                                            
                                            if create == True:  # not here
                                                    warning_messages["warnings_seed"].append(seed_lot_warnings(son_obj, _('test fils -> son'), son_obj.quantity_ini, line))
                                                    warning_messages["warnings_not_quantity"].append(_('line {line}: (son) {seedlot}').format(line=line, seedlot=son_obj) + '.<br>')
                                                
                                                    
                                            """ Reproduction creation"""   
                                                 
                                            #reproduction_date = row[SOWN_YEAR] + '-' + row[HARVESTED_YEAR]
                                            reproduction_dict = {'block': row[BLOCK],
                                                                 'X':row[X],
                                                                 'Y':row[Y],
                                                                 'start_date' : row[SOWN_YEAR],
                                                                 'end_date' : row[HARVESTED_YEAR],
                                                                 'quantity' : None,
                                                                 'split': None                                                  
                                                              }
                                            reproduction_query = Relation.objects.filter(seed_lot_father__name=father_obj, reproduction__start_date=int(row[SOWN_YEAR]), reproduction__end_date=int(row[HARVESTED_YEAR]), block=row[BLOCK])
                                            
                                           
                                            if reproduction_query.exists() == False:
                                                try :
                                                    reproduction_obj = create_reproduction(father_obj, sonm_obj, reproduction_dict)
                                                except ValidationError as ve:
                                                    error_messages["error_message"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ve)))
                                                    continue
                                                reproduction_obj.project.add(project) 
                                                warning_messages["warnings_relation"].append(_('line {line}: (reproduction) {reproduction}').format(line=line, reproduction=reproduction_obj) + '.<br>')
                                            else:
                                                reproduction_obj = reproduction_query[0]
                                            # print(line,reproduction_query,reproduction_query.exists(),reproduction_obj) 
                                            try:
                                                person_selection = Person.objects.get(short_name=row[SELECTION_PERSON])
                                            except Person.DoesNotExist:
                                                error_messages["person_error"].append(_('line {line}: (Intra varietal person) {selection}').format(line=line, selection=row[SELECTION_PERSON]) + ' <br>')
                                            except IndexError :
                                                error_messages["seed_lot_error"].append(_('line {line}: {seedlot} is not a valid name for a seed lot').format(line=line, seedlot=row[ID_SEED_LOT_SOWN]) + '<br>')
                                            else:
                                                try :
                                                    selection_date = int(row[HARVESTED_YEAR])
                                                except :
                                                    selection_date = None
                                                
                                                quantity, msg = get_quantity(row[SELECTION_QUANTITY_INI])
                                                if msg :
                                                    error_messages["error_message"].append(msg)
                                                    continue
                                                selection_dict = {'block': row[BLOCK],
                                                                'X':row[X],
                                                                'Y':row[Y],
                                                                'date' : selection_date,
                                                                'quantity' : quantity
                                                          }
                                                (selection_obj, create) = create_selection(father_obj, son_obj, selection_dict, row[SELECTION_NAME], person_selection, reproduction_obj.reproduction) 
                                                selection_obj.project.add(project)
                                                if create == True:
                                                    warning_messages["warnings_relation"].append(_('line {line}: (Intra varietal) {selection} done by {person}').format(line=line, selection=selection_obj, person=selection_obj.selection.person) + '.<br>')
        
                #                           """ we  will create the raw_data from Rawdata table"""
                                            for i in variable_method_dict.items():
                                                if str(list(variable_method_dict.items())[e][0]) not in csvfile.fieldnames:
                                                    error_messages["variable_error"].append('%s <br>' % str(list(variable_method_dict.items())[e][0]))
                                                else:
                                                    data = None
                                                    if row[list(variable_method_dict.items())[e][0]].strip() != "":
                                                        """str(list(variable_method_dict.items())[e][0]) correspond à la variable"""
                                                        if list(variable_method_dict.items())[e][0] + '$date' in file_list and row[str(list(variable_method_dict.items())[e][0]) + '$date'] != '':
                                                            try :
                                                                with transaction.atomic() :
                                                                    data = create_raw_data(list(variable_method_dict.items())[e][1][0],
                                                                                list(variable_method_dict.items())[e][1][1],
                                                                                row[list(variable_method_dict.items())[e][0]],
                                                                                row[str(list(variable_method_dict.items())[e][0]) + '$date'],
                                                                                request.user    
                                                                                )
                                                            except ValidationError as ve:
                                                                error_messages["error_pheno_variable"].append("line {line} : {message}".format(line=line, message=str(ve) % {'value': row[str(list(variable_method_dict.items())[e][0])+ '$date']}) + "<br />")
                                                        else:
                                                            """str(list(variable_method_dict.items())[e][0]) correspond à la variable"""
                                                            try :
                                                                with transaction.atomic() :
                                                                    data = create_raw_data(list(variable_method_dict.items())[e][1][0],
                                                                                list(variable_method_dict.items())[e][1][1],
                                                                                row[list(variable_method_dict.items())[e][0]],
                                                                                None,
                                                                                request.user)
                                                            except ValidationError as ve:
                                                                error_messages["error_pheno_variable"].append("line {line} : {message}".format(line=line, message=str(ve) % {'value': row[str(list(variable_method_dict.items())[e][0])+ '$date']}) + "<br />")
                                                        if data :
                                                            if str(data.variable.type) == 'T1':
                                                                data.relation.add(reproduction_obj)
                                                                data.relation.add(selection_obj)
                                                            elif str(data.variable.type) == 'T4':
                                                                data.relation.add(selection_obj)
                                                            elif str(data.variable.type) == 'T7':
                                                                data.seedlot.add(father_obj)
                                                            elif str(data.variable.type) == 'T8':
                                                                data.seedlot.add(son_obj)
                                                            else :
                                                                error_messages["error_pheno_variable"].append(_("line {line} : Type {type} can't be used with {event} event".format(line=line, type=data.variable.type, event=INTRA_VARIETAL))+"<br />")
                                                e = e + 1
                                            e = 0
#                                                 if error_messages["variable_error"]:
#                                                     transaction.rollback()
#                                                     return render(request, 'myuser/error.html', {'variable_error':error_messages["variable_error"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                                        if len(row[ID_SEED_LOT_SOWN].split('_')) == 4:
                                            
                                            try:
                                                father_obj = Seedlot.objects.get(name=row[ID_SEED_LOT_SOWN])
                                            except Seedlot.DoesNotExist:
                                                error_messages["seed_lot_error"].append(_("line {line}: {seedlot} doesn't exist").format(line=line, seedlot=row[ID_SEED_LOT_SOWN]) + "<br>")
                                                continue
                                            else:
                                                
                                                query_set = Relation.objects.filter(seed_lot_father__name=father_obj.name, block=row[BLOCK])  # ,reproduction__date=relation_date,
                                                
                                                """ problème avec la reproduction_date!!!!!! à plusieurs endroits du code :( penser à le corriger """
                                                
                                                if query_set.exists() == False:
                                                    error_messages["error_message"].append(_('line {line}: The reproduction {seedlot} does not exist (block {block})').format(line=line, seedlot=str(father_obj), block=row[BLOCK]) + '.<br>')
                                                else:
                                                    if row[SELECTION_PERSON] != '' and row[SELECTION_NAME] != '':
                                                        try :
                                                            (germplasm_obj, create) = create_germplasm(row[ID_SEED_LOT_SOWN], None, species)
                                                        except NameErrorException as ne:
                                                            error_messages["idgermplasm_error"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ne)))
                                                            continue
                                                        if create == True:
                                                            warning_messages["warnings_germ"].append(germplasm_warnings(germplasm_obj, _('Son'), line, menu_list, species))
                                                        if '#' in row[ID_SEED_LOT_SOWN]: 
                                                            new_germ = row[ID_SEED_LOT_SOWN].split('#')[0] + '#' + row[SELECTION_NAME]
                                                            select_test = row[ID_SEED_LOT_SOWN].split('_')[0]
                                                            if select_test.split('#')[1] == row[SELECTION_NAME]:
                                                                error_messages["error_message"].append(_('line {line}: Selection name {selection} is already used for germplasm {germplasm}').format(line=line, selection=row[SELECTION_NAME], germplasm=germplasm_obj) + '.<br>')
                                                                
                                                        else:
                                                            new_germ = row[ID_SEED_LOT_SOWN].split('_')[0] + '#' + row[SELECTION_NAME]
                                                        
                                                        
                                                        
                                                        check = Seedlot.objects.filter(name__startswith=new_germ + "_").values_list('name', flat=True)
                                                        if len(check) != 0:
                                                            error_messages["error_message"].append(_('line {line}: Selection name {selection} is already used for germplasm {germplasm}').format(line=line, selection=row[SELECTION_NAME], germplasm=germplasm_obj) + '.<br>')
                                                        elif len(check) == 0:
                                                            try:
                                                                location_obj = Location.objects.get(short_name=row[ID_SEED_LOT_SOWN].split('_')[1])
                                                            except Location.DoesNotExist:
                                                                error_messages["person_error"].append(_('line') + ' {line}: {seedlot} <br>'.format(line=line, seedlot=row[ID_SEED_LOT_SOWN].split('_')[1]))
                                                            except IndexError :
                                                                error_messages["seed_lot_error"].append(_('line {line}: {seedlot} is not a valid name for a seed lot').format(line=line, seedlot=row[ID_SEED_LOT_SOWN]) + '<br>')
                                                            else:
                                                                new_germ = new_germ + '_' + row[ID_SEED_LOT_SOWN].split('_')[1] + '_' + row[HARVESTED_YEAR]
                                                                son_digit = create_digit(new_germ)
                                                                date = row[HARVESTED_YEAR]
                                                                quantity, msg = get_quantity(row[SELECTION_QUANTITY_INI])
                                                                if msg :
                                                                    error_messages["error_message"].append(msg)
                                                                    continue
                                                                seed_lot_fields_dict = {'date':date,
                                                                                   'quantity_ini':quantity,  # À conserver : permet de faire l'affichage de la quantité initiale sélectionnée 
                                                                                   }
                                                                try :
                                                                    (son_obj, create) = create_seed_lot(seed_lot_fields_dict,
                                                                                                  location_obj,
                                                                                                  germplasm_obj,
                                                                                                  son_digit,
                                                                                                  )
                                                                except Exception as e :
                                                                    error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                                                # son_obj.project.add(project)
                                                                if create == True:
                                                                    if son_obj.quantity_ini == None:
                                                                        warning_messages["warnings_seed"].append(seed_lot_warnings(son_obj, _('son'), row[SELECTION_QUANTITY_INI], line))
                                                                    else:
                                                                        warning_messages["warnings_seed"].append(seed_lot_warnings(son_obj, _('son'), son_obj.quantity_ini, line))
                                                                        warning_messages["warnings_not_quantity"].append(_('line {line}: (son) {seedlot}').format(line=line, seedlot=son_obj) + '.<br>')
                                                            try:
                                                                person_selection = Person.objects.get(short_name=row[SELECTION_PERSON])
                                                            except Person.DoesNotExist:
                                                                error_messages["person_error"].append(_('line {line}: (Intra varietal person) {person}').format(line=line, person=row[SELECTION_PERSON]) + ' <br>')
                                                            except IndexError :
                                                                error_messages["seed_lot_error"].append(_('line {line}: {seedlot} is not a valid name for a seed lot').format(line=line, seedlot=row[ID_SEED_LOT_SOWN]) + '<br>')
                                                            else:
                                                                selection_date = int(row[HARVESTED_YEAR])
                                                                quantity, msg = get_quantity(row[SELECTION_QUANTITY_INI])
                                                                if msg :
                                                                    error_messages["error_message"].append(msg)
                                                                    continue
                                                                selection_dict = {'block': row[BLOCK],
                                                                                'X':row[X],
                                                                                'Y':row[Y],
                                                                                'date' : selection_date,
                                                                                'quantity' : quantity
                                                                          }
        
                                                                (selection_obj, create) = create_selection(father_obj, son_obj, selection_dict, row[SELECTION_NAME], person_selection, query_set[0].reproduction) 
                                                                selection_obj.project.add(project)
                                                                if create == True:
                                                                    warning_messages["warnings_relation"].append(_('line {line}: (Intra varietal) {selection} done by {person}').format(line=line, selection=selection_obj, person=selection_obj.selection.person) + '.<br>')
                                                                for i in variable_method_dict.items():
                                                                    if str(list(variable_method_dict.items())[e][0]) == '' :
                                                                        error_messages["variable_error"].append('%s. %s<br>' % (len(error_messages["variable_error"]) + 1, _("A column containing data has an empty name")))
                                                                    elif str(list(variable_method_dict.items())[e][0]) not in csvfile.fieldnames:
                                                                        error_messages["variable_error"].append(_('{line}. {variable}  is present in the method file but is not present in the data file').format(len(error_messages["variable_error"]) + 1, str(list(variable_method_dict.items())[e][0])) + '<br>') 
                                                                    else:
                                                                        data = None
                                                                        if row[list(variable_method_dict.items())[e][0]].strip() != "":
                                                                            """str(list(variable_method_dict.items())[e][0]) correspond à la variable"""
                                                                            
                                                                            if list(variable_method_dict.items())[e][0] + '$date' in file_list and row[str(list(variable_method_dict.items())[e][0]) + '$date'] != '':
                                                                                try :
                                                                                    with transaction.atomic() :
                                                                                        data = create_raw_data(list(variable_method_dict.items())[e][1][0],
                                                                                        list(variable_method_dict.items())[e][1][1],
                                                                                        row[list(variable_method_dict.items())[e][0]],
                                                                                        row[str(list(variable_method_dict.items())[e][0]) + '$date'],
                                                                                        request.user
                                                                                            )
                                                                                except ValidationError as ve:
                                                                                    error_messages["error_pheno_variable"].append("line {line} : {message}".format(line=line, message=str(ve) % {'value': row[str(list(variable_method_dict.items())[e][0])+ '$date']}) + "<br />")
                                                                            else:
                                                                                """str(list(variable_method_dict.items())[e][0]) correspond à la variable"""
                                                                                try :
                                                                                    with transaction.atomic() :
                                                                                        data = create_raw_data(list(variable_method_dict.items())[e][1][0],
                                                                                                                    list(variable_method_dict.items())[e][1][1],
                                                                                                                    row[list(variable_method_dict.items())[e][0]],
                                                                                                                    None,
                                                                                                                    request.user)
                                                                                except ValidationError as ve:
                                                                                    error_messages["error_pheno_variable"].append("line {line} : {message}".format(line=line, message=str(ve) % {'value': row[str(list(variable_method_dict.items())[e][0])+ '$date']}) + "<br />")
                                                                            if data :
                                                                                if str(data.variable.type) == 'T1':
                                                                                    data.relation.add(query_set[0])
                                                                                    data.relation.add(selection_obj)
                                                                                elif str(data.variable.type) == 'T4':
                                                                                    data.relation.add(selection_obj)
                                                                                elif str(data.variable.type) == 'T7':
                                                                                    data.relation.add(father_obj)
                                                                                elif str(data.variable.type) == 'T8':
                                                                                    data.relation.add(son_obj)
                                                                                else :
                                                                                    error_messages["error_pheno_variable"].append(_("line {line} : Type {type} can't be used with {event} event".format(line=line, type=data.variable.type, event=INTRA_VARIETAL))+"<br />")
                                                                    e = e + 1
                                                                e = 0
#                                                                     if error_messages["variable_error"]:
#                                                                         transaction.rollback()
#                                                                         return render(request, 'myuser/error.html', {'variable_error':error_messages["variable_error"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
#                                                                     if error_messages["method_error"]:
#                                                                         transaction.rollback()
#                                                                         return render(request, 'myuser/error.html', {'method_error':error_messages["method_error"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
             
                    elif radio == PHENOTYPIC:
                        print("line : ",line)
                        unknown_corr_gp = []
                        if INDIVIDUALS not in csvfile.fieldnames :
                            error_messages["error_message"].append(_("You selected the {type} button but the file you submitted doesn't correspond to a {type} file format").format(type=radio))
                            return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name}) 
                    
                        
                        # fields = [PROJECT, SOWN_YEAR, HARVESTED_YEAR, ID_SEED_LOT_SOWN, SOWN_ETIQUETTE, ID_SEED_LOT_HARVESTED, HARVESTED_ETIQUETTE, BLOCK, X, Y, INDIVIDUALS,CORRELATION_GROUP]
         
                        fields = [PROJECT, SOWN_YEAR, HARVESTED_YEAR, ID_SEED_LOT_SOWN, ETIQUETTE, INTRA_SELECTION_NAME, BLOCK, X, Y, INDIVIDUALS, CORRELATION_GROUP]
                        for l in range(len(fields)) :
                            if fields[l] not in csvfile.fieldnames:
                                error_messages["error_message"].append(_('{field} is missing among file fieldnames').format(field=fields[l]) + '.<br>')
                                transaction.rollback()
                                return render(request, 'myuser/error.html', {'error_message': error_messages["error_message"], 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})   
                        
                        if row[PROJECT] == '' : 
                            error_messages["error_project"].append(_("line {line}: No project found").format(line=line) + "<br />")
                            continue
                        if not project or project.project_name != row[PROJECT] :
                            project, created = Project.objects.get_or_create(project_name=row[PROJECT], defaults={'start_date':datetime.date.today()})
                        
                        if row[X] == '' or row[Y] == '' :
                                warning_messages['xy_missing'].append(_("line {line}: X and/or Y is missing for the reproduction concerning").format(line=line) + " {seedlot}<br />".format(seedlot=row[ID_SEED_LOT_SOWN]))
                                
                        """on initalise l, copy permet de creer deux espaces différents dans le disque dur pour csvfile.fieldnames et file_list.
                        à la fin de cette boucle for file list represente l'entete - colones obligatoire = variables"""
                        file_list = get_variable_list(csvfile.fieldnames, fields)
        
                        """ On vérifie que les éléments de file_list correspondent à ce qui se trouve dans le fichier methode """
                
                        var_list = []  # liste des variables devant figurer dans le fichier méthodes
                        var_date_list = []  # liste des variables du type 'variable$date'
                        
                        for var in file_list:
                            if var.find('$') != -1:
                                var_date_list.append(var) 
                            else:
                                var_list.append(var)
        
                        error_file_list = check_variable_methods(var_list, variable_method_dict)
                        
                        if error_file_list != [] :
                            return render(request, 'myuser/error.html', {'error_file_list': error_file_list, 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
                        
                        """ On vérifie que la date 'variable$date' est bien liée à une variable 'variable' existante """ 
                        error_var_date_list = check_variable_date(var_date_list, var_list)
                        
                        if error_var_date_list != [] :
                            return render(request, 'myuser/error.html', {'error_var_date_list': error_var_date_list, 'form':form, 'radio':radio, 'species':species, 'file_name':file_name})
        
                        """vérification que SOWN_YEAR et HARVESTED_YEAR sont bien une date comprise entre 1900 et 3000"""
                        try:
                            if row[HARVESTED_YEAR] != "":
                                int(row[SOWN_YEAR])
                                int(row[HARVESTED_YEAR])
                                if int(row[SOWN_YEAR]) > int(row[HARVESTED_YEAR]) :
                                    error_messages["error_sown_year"].append(_('line {line}: sown year is higher than harvested year').format(line=line) + '<br>')
                        except ValueError:
                            dict_year_sh = {row[SOWN_YEAR]:SOWN_YEAR, row[HARVESTED_YEAR]:HARVESTED_YEAR}
                            if row[HARVESTED_YEAR] != "":
                                for i in dict_year_sh.keys():
                                    if i not in list_year:
                                        error_messages["error_sown_year"].append(_("line {line}: column '{col}', '{value}' is not a valid value").format(line=line, col=dict_year_sh[i], value=i) + "<br>")
                        else:                    
#                             try:
#                                 int(row[BLOCK])
#                             except ValueError:   
#                                 error_messages["error_block"].append(_('line {line}: block information is missing or is not an integer value').format(line=line) + '<br />')
                            if row[BLOCK] == '' :
                                error_messages["error_block"].append(_('line {line}: block information is missing or is not an integer value').format(line=line) + '<br />')
                            else:
                                try:
                                    location_obj = Location.objects.get(short_name=row[ID_SEED_LOT_SOWN].split('_')[1])
                                except Location.DoesNotExist:
                                    error_messages["person_error"].append(_('line') + ' {line}: {seedlot}<br>'.format(line=line, seedlot=row[ID_SEED_LOT_SOWN].split('_')[1]))
                                except IndexError :
                                    error_messages["seed_lot_error"].append(_('line {line}: {seedlot} is not a valid name for a seed lot').format(line=line, seedlot=row[ID_SEED_LOT_SOWN]) + '<br>')
                                else:
                                    try :
                                        (germplasm_obj, create) = create_germplasm(row[ID_SEED_LOT_SOWN], None, species)
                                    except NameErrorException as ne:
                                        error_messages["idgermplasm_error"].append("line {line}: %s<br />" % (line, str(ne)))
                                        continue
                                    if create == True:
                                        warning_messages["warnings_germ"].append(germplasm_warnings(germplasm_obj, _('father & son'), line, menu_list, species))
                                    
                                    """ Coordonnées du SL """
                                    blockX = None
                                    blockY = None
                                    
                                    if row[X] != "":
                                        blockX = row[X]
                                        
                                    if row[Y] != "":
                                        blockY = row[Y]
                                        
                                    # Si le seed lot semé est AVEC digit    
                                    if len(row[ID_SEED_LOT_SOWN].split('_')) == 4:
                                        if row[INTRA_SELECTION_NAME] == "":
                                            try:
                                                father_obj = Seedlot.objects.get(name=row[ID_SEED_LOT_SOWN])
                                                # father_obj.project.add(project)
                                            except Seedlot.DoesNotExist:
                                                error_messages["seed_lot_error"].append(_("line {line}: sown seed lot {seedlot} doesn't exist").format(line=line, seedlot=row[ID_SEED_LOT_SOWN]) + "<br>")
                                                continue
                                            try:
                                                relation = Relation.objects.get(seed_lot_father=father_obj,
                                                                         reproduction__start_date=int(row[SOWN_YEAR]), reproduction__end_date=int(row[HARVESTED_YEAR]),
                                                                         block=row[BLOCK],
                                                                         X=blockX,
                                                                         Y=blockY,
                                                                         selection = None)
                                            except MultipleObjectsReturned:
                                                error_messages["error_message"].append(_("line {line} : Several relations exists for a same seed lot at the same place".format(line=line)))
                                            except ObjectDoesNotExist :
                                                """ Reproduction creation """        
                                                existing_relations = None
                                                if blockX and blockY :
                                                    existing_relations = Relation.objects.filter(seed_lot_father__location=location_obj, seed_lot_father__germplasm__species=species, reproduction__start_date=int(row[SOWN_YEAR]), block=row[BLOCK], X=blockX, Y=blockY, selection=None)
                                                if existing_relations:
                                                    error_messages['sowing_block_error'].append(_("line {line}: A reproduction already exist in {year} on {location} farm at block={block}, X={X} and Y={Y}".format(line=line,year=row[SOWN_YEAR],location=location_obj,block=row[BLOCK],X=blockX,Y=blockY))+"<br />")
                                                    continue
                                                else :
                                                    if row[HARVESTED_YEAR] != "" :
                                                        date = row[HARVESTED_YEAR]
                                                        seed_lot_fields_dict = {'date':date,
                                                                                'quantity_ini':None,
                                                                                }
                                                        son = row[ID_SEED_LOT_SOWN].split('_')[0] + "_" + row[ID_SEED_LOT_SOWN].split('_')[1] + "_" + row[HARVESTED_YEAR]
                                                        son_digit = create_digit(son)
                                                        try :
                                                            (germplasm_obj, create) = create_germplasm(row[ID_SEED_LOT_SOWN], None, species)
                                                        except NameError as ne:
                                                            error_messages["idgermplasm_error"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ne)))
                                                            continue
                                                        if create == True:
                                                            warning_messages["warnings_germ"].append(germplasm_warnings(germplasm_obj, _('Son'), line, menu_list, species))
                                                        
                                                        try :
                                                            (son_obj, create) = create_seed_lot(seed_lot_fields_dict,
                                                                                                  location_obj,
                                                                                                  germplasm_obj,
                                                                                                  son_digit,
                                                                                                  )
                                                        except Exception as e :
                                                            error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                                        if create == True:
                                                            warning_messages["warnings_seed"].append(seed_lot_warnings(son_obj, _('son'), son_obj.quantity_ini, line))
                                                            warning_messages["warnings_not_quantity"].append(_("line {line}: (son) {seedlot}").format(line=line, seedlot=son_obj) + ".<br>")
                                                            seed_lot_dict[son_digit] = son_obj
                                                    
                                                        reproduction_dict = {'block': row[BLOCK], 'start_date' : row[SOWN_YEAR], 'end_date' : row[HARVESTED_YEAR],'X' : blockX, 'Y' : blockY, 'quantity' : None, 'split': None}
                                                        try :
                                                            relation = create_reproduction(father_obj, son_obj, reproduction_dict)
                                                        except ValidationError as ve:
                                                            error_messages["error_message"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ve)))
                                                            continue
                                                        else :
                                                            relation.project.add(project)
                                                    else :
                                                        reproduction_dict = {'block': row[BLOCK], 'start_date' : row[SOWN_YEAR], 'end_date' : row[HARVESTED_YEAR],'X' : blockX, 'Y' : blockY, 'quantity' : None, 'split': None}
                                                        try :
                                                            relation = create_reproduction(father_obj, None, reproduction_dict)
                                                        except ValidationError as ve:
                                                            error_messages["error_message"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ve)))
                                                            continue
                                                        else :
                                                            relation.project.add(project)
                                                    warning_messages["warnings_relation"].append(_('line {line}: (reproduction) {reproduction}').format(line=line, reproduction=relation) + '.<br>')
                                            son_obj = relation.seed_lot_son 
                                        elif row[INTRA_SELECTION_NAME] != "":
                                            # regarde si la relation de selection existe
                                            try:
                                                father_obj = Seedlot.objects.get(name=row[ID_SEED_LOT_SOWN])
                                            except Seedlot.DoesNotExist:
                                                error_messages["seed_lot_error"].append(_("line {line}: sown seed lot {seedlot} doesn't exist").format(line=line, seedlot=row[ID_SEED_LOT_SOWN]) + "<br>")
                                                continue
                                            
                                            else:
                                                # si father_obj existe
                                                try:
                                                    print(father_obj)
                                                    print("%{0}%".format(row[INTRA_SELECTION_NAME]))
                                                    print(row[BLOCK])
                                                    print(blockX)
                                                    print(blockY)
                                                    relation = Relation.objects.get(seed_lot_father=father_obj,
                                                                         selection__selection_name=row[INTRA_SELECTION_NAME],
                                                                         # reproduction__date = relation_date,
                                                                         #block=int(row[BLOCK]),
                                                                         block=row[BLOCK],
                                                                         X=blockX,
                                                                         Y=blockY)
                                                    son_obj = relation.seed_lot_son 
                                                except Relation.DoesNotExist:
                                                    error_messages["error_message"].append(_("line {line}: relation with father {seedlot} doesn't exist for selection name {selection}").format(line=line, seedlot=row[ID_SEED_LOT_SOWN], selection=row[INTRA_SELECTION_NAME]) + "<br>")
                                                    continue
                                                    
                                                
                                    # Si le seed lot semé est SANS digit  
                                    if len(row[ID_SEED_LOT_SOWN].split('_')) == 3:
                                        # cas 1 : si le selection_name est vide -> il faut créer le SL semé, le SL récolté, et la relation de reproduction
                                        
                                        if row[INTRA_SELECTION_NAME] == "":   
                                            """ Germplasm creation in germplasm table """    
                                            try :
                                                (germplasm_obj, create) = create_germplasm(row[ID_SEED_LOT_SOWN], None, species)
                                            except NameErrorException as ne:
                                                error_messages["idgermplasm_error"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ne)))
                                                continue
                                            
                                            if create == True:
                                                warning_messages["warnings_germ"].append(germplasm_warnings(germplasm_obj, _('father/son'), line, menu_list, species))
                                                """ we have to create the seed lot in Seedlot table (include germplasm_id and person_id)"""
                                    
                                            
                                            """ father creation """
                                            father_digit = create_digit(row[ID_SEED_LOT_SOWN])
                                            try :
                                                date = find_date(row[ID_SEED_LOT_SOWN])
                                            except :
                                                error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot').format(line=line, name=row[ID_SEED_LOT_SOWN]) + '<br>')
                                                continue
                                            
                                            #date = find_date(row[ID_SEED_LOT_SOWN])
                                            fields_dict = {'date':date,
                                                           'quantity_ini':None}
                                            try :
                                                (father_obj, create) = create_seed_lot(fields_dict,
                                                                              location_obj,
                                                                              germplasm_obj,
                                                                              father_digit,
                                                                              )
                                            except Exception as e :
                                                error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                            
                                            
                                            if create == True:
                                                warning_messages["warnings_seed"].append(seed_lot_warnings(father_obj, _('father'), father_obj.quantity_ini, line))
                                                
                                            """ son creation """
                                            son_name = "{0}_{1}_{2}".format(father_obj.germplasm.idgermplasm, 
                                                                            father_obj.location.short_name,
                                                                            row[HARVESTED_YEAR])
                                            son_digit = create_digit(son_name)
                                            date = row[HARVESTED_YEAR]
                                            son_seed_lot_fields_dict = {'date':date,
                                                                        'quantity_ini':None}
                                            
                                            try :
                                                (son_obj, create) = create_seed_lot(son_seed_lot_fields_dict,
                                                                               location_obj,
                                                                               germplasm_obj,
                                                                               son_digit)
                                            except Exception as e :
                                                error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                            # son_obj.project.add(project)
                                            # son_indiv = Individual.objects.create(seed_lot=son_obj)
                                            if create == True:
                                                warning_messages["warnings_seed"].append(seed_lot_warnings(son_obj, _('son'), son_obj.quantity_ini, line))
                                                warning_messages["warnings_not_quantity"].append(_('line {line}: (son) {seedlot}').format(line=line, seedlot=son_obj) + '.<br>')
                                       
                                       
                                            
                                            
                                            """ reproduction creation """
                                            #reproduction_date = row[SOWN_YEAR] + '-' + row[HARVESTED_YEAR]
                                            reproduction_dict = {
                                                                    'block': row[BLOCK],
                                                                    'start_date' : row[SOWN_YEAR],
                                                                    'end_date': row[HARVESTED_YEAR],
                                                                    'X' : blockX,
                                                                    'Y' : blockY,
                                                                    'quantity':None,
                                                                    'split': None
                                                                    }
                                            existing_relations = None
                                            if blockX and blockY :
                                                existing_relations = Relation.objects.filter(seed_lot_father__location=location_obj, seed_lot_father__germplasm__species=species, reproduction__start_date=int(row[SOWN_YEAR]), block=row[BLOCK], X=blockX, Y=blockY, selection=None)
                                            if existing_relations:
                                                error_messages['sowing_block_error'].append(_("line {line}: A reproduction already exist in {year} on {location} farm at block={block}, X={X} and Y={Y}".format(line=line,year=row[SOWN_YEAR],location=location_obj,block=row[BLOCK],X=blockX,Y=blockY))+"<br />")
                                                continue
                                            else :
                                                try :
                                                    reproduction_obj = create_reproduction(father_obj, son_obj, reproduction_dict)
                                                    reproduction_obj.project.add(project)
                                                except ValidationError as ve :
                                                    error_messages["error_message"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ve)))
                                                warning_messages["warnings_relation"].append(_('line {line}: (reproduction) {reproduction}').format(line=line, reproduction=reproduction_obj) + '.<br>')
                                                    
                                        elif row[INTRA_SELECTION_NAME] != "":    
                                            # cas 2 : si le selection_name n'est pas vide -> il faut créer le SL semé, le SL sélectionné, la relation de sélection
                                            
                                            
                                            # il faut créer une relation en amont sans seed_lot father (SL unknown) et un seed_lot son avec le sélection name                                     
                                            """ selection relation for grandfather """
                                            
                                            grandfather = 'UNKNOWN_'
                                            grandfather_digit = create_digit(grandfather)
                                            try :
                                                date = find_date(row[ID_SEED_LOT_SOWN])
                                            except :
                                                error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot').format(line=line, name=row[ID_SEED_LOT_SOWN]) + '<br>')
                                                continue
                                            #date = find_date(row[ID_SEED_LOT_SOWN])
                                            fields_dict = {'date':date,
                                                           'quantity_ini':None}
                                            
        
                                            try :
                                                (gdfather_obj, create) = create_seed_lot(fields_dict,
                                                                              location_obj,
                                                                              germplasm_obj,
                                                                              grandfather_digit,
                                                                              )
                                            except Exception as e :
                                                error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                            
                                            """ father creation """
                                            father = row[ID_SEED_LOT_SOWN].split('#')[0] + row[ID_SEED_LOT_SOWN].split('_')[1] + row[ID_SEED_LOT_SOWN].split('_')[2]
                                            
                                            father_digit = create_digit(father)
                                            #date = find_date(row[ID_SEED_LOT_SOWN])
                                            try :
                                                date = find_date(row[ID_SEED_LOT])
                                            except :
                                                error_messages["seed_lot_error"].append(_('line {line}: {name} is not a valid name for a seed lot').format(line=line, name=row[ID_SEED_LOT_SOWN]) + '<br>')
                                                continue
                                            fields_dict = {'date':date,
                                                           'quantity_ini':None}
                                            
                                            try :
                                                (father_obj, create) = create_seed_lot(fields_dict,
                                                                              location_obj,
                                                                              germplasm_obj,
                                                                              father_digit,
                                                                              )
                                            except Exception as e :
                                                error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                            if create == True:
                                                warning_messages["warnings_seed"].append(seed_lot_warnings(father_obj, _('father'), father_obj.quantity_ini, line))
        
                                            """ reproduction creation """
                                            try :
                                                reproduction_obj = create_reproduction(gdfather_obj, father_obj, reproduction_dict)
                                            except ValidationError as ve :
                                                error_messages["error_message"].append(_("line") + " {line}: {message}<br />".format(line=line, message=str(ve)))
                                                continue 
                                            reproduction_obj.project.add(project)
                                            """ son creation """
                                            son_digit = create_digit(row[ID_SEED_LOT_SOWN])
                                            date = row[HARVESTED_YEAR]
                                            son_seed_lot_fields_dict = {'date':date,
                                                                        'quantity_ini':None}
                                            
                                            try :
                                                (son_obj, create) = create_seed_lot(son_seed_lot_fields_dict,
                                                                               location_obj,
                                                                               germplasm_obj,
                                                                               son_digit)
                                            except Exception as e :
                                                error_messages["seed_lot_error"].append(_("line")+" {line}: {message}<br />".format(line=line, message=str(e)))
                                            # son_obj.project.add(project)
                                            # son_indiv = Individual.objects.create(seed_lot=son_obj)
                                            if create == True:
                                                warning_messages["warnings_seed"].append(seed_lot_warnings(son_obj, _('son'), son_obj.quantity_ini, line))
                                                warning_messages["warnings_not_quantity"].append(_('line {line}: (son) {seedlot}').format(line=line, seedlot=son_obj) + '.<br>')
        
                                            """ selection creation THIS CODE SHOULD NOT BE ACTIVE"""                                                
                                            selection_date = None
                                            person_selection = None  
                                            
                                            selection_dict = {'block': row[BLOCK],
                                                            'X':row[X],
                                                            'Y':row[Y],
                                                            'date' : selection_date,
                                                            'quantity' : None
                                                      }
                                             
                                            if 'X' in selection_dict.keys() and selection_dict['X'] != '' and selection_dict['X'] != None :
                                                block_x = selection_dict['X']
                                            if 'Y' in selection_dict.keys() and selection_dict['Y'] != '' and selection_dict['Y'] != None :
                                                block_y = selection_dict['Y']
                                                    
        
                                            (selection_obj, create) = create_selection(father_obj,
                                                                                      son_obj,
                                                                                      selection_dict,
                                                                                      row[INTRA_SELECTION_NAME],
                                                                                      person_selection,
                                                                                      reproduction_obj.reproduction) 
                                            
                                            if create == True:
                                                warning_messages["warnings_relation"].append(_('line {line}: (Intra varietal) {selection} done by {person})').format(line=line, selection=selection_obj, person=selection_obj.selection.person) + '.<br>')
                                                            
        
                                            """ get relation """
                                            relation = Relation.objects.get(seed_lot_father=father_obj,
                                                                         selection__selection_name=row[INTRA_SELECTION_NAME],
                                                                         #block=int(row[BLOCK]),
                                                                         block=row[BLOCK],
                                                                         X=blockX,
                                                                         Y=blockY)

                                   
                                        if (father_obj, son_obj, row[BLOCK], blockX, blockY, row[INDIVIDUALS]) in phenotypic_list:
                                            error_messages["error_block"].append(_('line {line}: information concerning relation {father} --> {son} for individual {ind} is duplicated in this file').format(line=line, father=father_obj, son=son_obj, ind=row[INDIVIDUALS]) + '<br />')
                                        try :
                                            relation = Relation.objects.get(seed_lot_father=father_obj,
                                                                            seed_lot_son=son_obj,
                                                                            # reproduction__date = relation_date,
                                                                            #block=int(row[BLOCK]),
                                                                            block=row[BLOCK],
                                                                            X=blockX,
                                                                            Y=blockY)
                                            phenotypic_list.append((father_obj, son_obj, row[BLOCK], blockX, blockY, row[INDIVIDUALS]))
                                        except :
                                            error_messages["sowing_block_error"].append(_("line {line}. It seems that the relation {father} --> {son} doesn't exists in block {block} at X = {x}, Y = {y}").format(line=line, father=father_obj, son=son_obj, block=row[BLOCK], x=row[X], y=row[Y]) + ' <br>')
                                            continue
                                        seed_lot_dict[row[ID_SEED_LOT_SOWN]] = father_obj
                                    
                                    """ we  will create the raw_data from Rawdata table"""
                                    for i in variable_method_dict.items():
                                        if str(list(variable_method_dict.items())[e][0]) == '' :
                                            error_messages["variable_error"].append('%s. %s<br>' % (len(error_messages["variable_error"]) + 1, _("A column containing data has an empty name")))
                                        elif str(list(variable_method_dict.items())[e][0]) not in csvfile.fieldnames:
                                            error_messages["variable_error"].append(_('{line}. {variable}  is present in the method file but is not present in the data file').format(line=len(error_messages["variable_error"]) + 1, variable=str(list(variable_method_dict.items())[e][0])) + '<br>') 
                                        elif str(list(variable_method_dict.items())[e][0]) in csvfile.fieldnames:
                                            """str(list(variable_method_dict.items())[e][0]) correspond à la variable"""
                                            if row[list(variable_method_dict.items())[e][0]].strip() != "":
                                                data = None
                                                if list(variable_method_dict.items())[e][0] + '$date' in file_list:
                                                    data_date = row[str(list(variable_method_dict.items())[e][0]) + '$date']
                                                else :
                                                    data_date = None
                                                try :
                                                    data = create_raw_data(list(variable_method_dict.items())[e][1][0],
                                                                list(variable_method_dict.items())[e][1][1],
                                                                row[list(variable_method_dict.items())[e][0]],
                                                                data_date,
                                                                request.user   
                                                                )
                                                except ValidationError as ve:
                                                    error_messages["error_pheno_variable"].append("line {line} : {message}".format(line=line, message=str(ve) % {'value': row[str(list(variable_method_dict.items())[e][0])+ '$date']} ) + "<br />")
                                                if data and str(data.variable.type) == 'T11':
                                                    data.relation.add(relation)
                                                    try :
                                                        data.individual = int(row[INDIVIDUALS])
                                                    except :
                                                        error_messages["error_pheno_variable"].append(_('line {line}: Individual number must be an integer, not "{value}"')
                                                                                                                      .format(line=line, value=row[INDIVIDUALS]) + '<br>')
                                                        continue
                                                    grp_list_pheno = map(lambda x: str(x).strip(), row[CORRELATION_GROUP].split(','))
                                                    if grp_list_pheno and row[CORRELATION_GROUP] != '' :
                                                        for grp in grp_list_pheno:
                                                            if grp in dict_grp_pheno.keys() :
                                                                if data.variable.name in dict_grp_pheno[grp] :
                                                                    if data.group == None or data.group == "":
                                                                        data.group = grp
                                                                    else :
                                                                        error_messages["error_pheno_variable"].append(_('line {line}: raw data for variable {variable} could not belong to several groups {groups} {other}')
                                                                                                                      .format(line=line, variable=data.variable.name, groups=grp, other=data.group) + '<br>')
                                                            elif grp not in unknown_corr_gp :
                                                                unknown_corr_gp.append(grp)
                                                                error_messages["error_pheno_variable"].append(_("line {line}: This correlation group ({grp}) is not referenced in method file")
                                                                                                              .format(line=line, grp=grp) +"<br />")
                                                    data.save() 
                                                else :
                                                    error_messages["error_pheno_variable"].append(_("line {line} : Type {type} can't be used with {event} event"
                                                                                                    .format(line=line, type=data.variable.type, event=PHENOTYPIC))+"<br />")
                                        e = e + 1
                                    e = 0
    #                                 if error_messages["variable_error"]:
    #                                     transaction.rollback()
    #                                     return render(request,'myuser/error.html', {'variable_error':error_messages["variable_error"], 'form':form,'radio':radio,'species':species, 'file_name':file_name})                            
                
                    copy_rep_manager = copy.copy(rep_manager)
                    for rep in copy_rep_manager.keys():
                        if len(rep_manager[rep]['replist']) == 1:
                            rep_manager.pop(rep)
                        elif is_already_merged(rep_manager[rep]['replist']) :
                            rep_manager.pop(rep)
                    
                check_errors(error_messages)
                message_report = "Submission successfull"
                return render(request, 'myuser/upload.html', {'form':form, 'file':file_name, 'warnings_var':warning_messages["warnings_var"],
                                                         'warnings_germ':warning_messages["warnings_germ"], 'warning_diffusion_quantity':warning_messages["warning_diffusion_quantity"],
                                                         'warnings_seed':warning_messages["warnings_seed"], 'female_warnings_seed':warning_messages["female_warnings_seed"],
                                                         'male_warnings_seed':warning_messages["male_warnings_seed"], 'son_warnings_seed':warning_messages["son_warnings_seed"],
                                                         'warnings_relation':warning_messages["warnings_relation"], 'warnings_not_quantity':warning_messages["warnings_not_quantity"],
                                                         'created_person':warning_messages["created_person"], 'male_relation':warning_messages["male_relation"], 'xy_missing':warning_messages['xy_missing'],
                                                         'female_relation':warning_messages["female_relation"], 'germplasm_type':germplasm_type, 'species':species, 'radio':radio, 'species':species, 'file_name':file_name, 'reps':rep_manager,
                                                         'message_report':message_report})      
        except Exception as e :
            traceback.print_exc()
            return render(request, 'myuser/error.html', {'form':form, 'error_message': error_messages["error_message"], 'person_error':error_messages["person_error"], 'sowing_block_error': error_messages["sowing_block_error"],
                                                                'error_son_germplasm':error_messages["error_son_germplasm"], 'idgermplasm_error':error_messages["idgermplasm_error"], 'select_error':error_messages["select_error"],
                                                                'seed_lot_error':error_messages["seed_lot_error"], 'error_split':error_messages["error_split"],
                                                                'error_nomenclature':error_messages["error_nomenclature"], 'error_empty':error_messages["error_empty"],
                                                                'mixture_empty':error_messages["mixture_empty"], 'method_error':error_messages["method_error"], 'variable_error':error_messages["variable_error"], 'error_project':error_messages["error_project"],
                                                                'pheno_grp_error':error_messages["pheno_grp_error"], 'error_pheno_variable':error_messages["error_pheno_variable"],
                                                                'error_row':error_messages["error_row"], 'split_string':error_messages["split_string"], 'error_sown_year':error_messages["error_sown_year"],
                                                                'error_block':error_messages["error_block"], 'error_quantity':error_messages["error_quantity"],'diffusion_error':error_messages['diffusion_error'],
                                                                'radio':radio, 'species':species, 'file_name':file_name})
            

    else :
        return render(request, 'myuser/upload.html', {'form':form})
    
def my_view(request):
    if not request.user.is_authenticated:
        message = 'login error'
        return render(request, 'login_error.html', {'message':message})
    else:
        message = 'logged!!'
        return render(request, 'login_error.html', {'message':message})
    
            
