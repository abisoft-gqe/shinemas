# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.db import models
from django.contrib.auth.models import User  

from .managers import AuditFileManager

class AuditFile(models.Model):
    user =  models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    file_name=models.URLField(null=False)
    file_url = models.URLField(null=False)
    date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="Update date")
    hash=models.TextField(max_length=400,null=False,unique=True)
    
    objects = AuditFileManager()

    