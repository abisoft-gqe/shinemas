# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""


import csv
import io
import chardet
import hashlib

from chardet.universaldetector import UniversalDetector


class IOFactory():
    """
      This class is used to read CSV files.
    """   
    csv_file = None
    file_dialect = None
    stream = None
    f = None
    file = None
    headers = []
    
    def __init__(self, csvf):
        self.csv_file = csvf
    
    def find_dialect(self):
        """
          This method use the Sniffer function of the csv module 
          to deduce the format of the file.
        """
        pass

    def recode(self, string):
        """
          This method change the encoding of the file to be read by the csv module
          (for temporary files only).
        """
        result = chardet.detect(string)
        print(result)
        return string.decode(result["encoding"])
    
    def read_file(self):
        """
          Convert the file in a DictReader file format to be processed.
        """
        self.file = csv.DictReader(self.f, dialect = self.file_dialect)
        self.headers = self.file.fieldnames
    
    def get_file(self):
        """
          Return the file.
        """
        return self.file
    
    def get_headers(self):
        """
          Return the headers of the file in a list.
        """
        return self.headers

    def check_headers_conformity(self, headers_list):
        """
          To check if file headers contains the headers 
          of the list.
        """
        missing = []
        
        for h in headers_list:
            if h not in self.headers:
                missing.append(h)
        
        return missing
    
    @staticmethod
    def detect_file_encoding(file_name):
        """
          This function is used to detect and return file encoding to 
          open files prior to use the CSVFactoryForStoredFiles.
          It is not needed for temporary files as the CSVFactoryForTempFiles 
          decode and read files at once.
        """
        rawdata = open(file_name, 'rb')
        
        detector = UniversalDetector()
        for line in rawdata:
            detector.feed(line)
            if detector.done: break
            
        detector.close()
        rawdata.close()
        
        result = detector.result
        return result['encoding']



class CSVFactoryForTempFiles(IOFactory):
    
    def __init__(self, csvf):
        IOFactory.__init__(self, csvf)
        self.csv_file = csvf
    
    def find_dialect(self):
        """
          Extends the find_dialect method of IOFactory with 
          the use of recode for the temporary files.
        """
        stream_init = self.csv_file.read()
        crypt = hashlib.sha1()
        crypt.update(stream_init)
        self.hash = crypt.hexdigest()
        stream = self.recode(stream_init)
        self.f = io.StringIO(stream)
        self.file_dialect = csv.Sniffer().sniff(self.f.read())
        self.f = io.StringIO(stream)
        
    def recode(self, string):
        return IOFactory.recode(self, string)
    
    def getHash(self):
        return self.hash
    
class CSVFactoryForStoredFiles(IOFactory):
    
    def __init__(self, csvf):
        IOFactory.__init__(self, csvf)
        self.csv_file = csvf
    
    def find_dialect(self):
        """
          Extends the find_dialect method of IOFactory
        """
        stream_init = self.csv_file.read()
        
        if isinstance(stream_init, str):
            stream = stream_init
        else:
            stream = self.recode(stream_init)
            
        self.f = io.StringIO(stream)
        self.file_dialect = csv.Sniffer().sniff(self.f.read())
        self.f = io.StringIO(stream)
        
    def recode(self, string):
        return IOFactory.recode(self, string)