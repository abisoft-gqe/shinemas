# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Yannick De Oliveira, Laura Burlot, Marie Lefebvre, Darkawi Madi, Eva Dechaux

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.db import models
from django.apps import apps
from django.conf import settings
from django.core.files.storage import FileSystemStorage


from .exceptions import AlreadySubmitedFile

class AuditFileManager(models.Manager):
    
    def audit_file(self, audited_file, user, hash):
        AuditFile = apps.get_model('csv_io','auditfile')   
        path=settings.MEDIA_ROOT
        fs = FileSystemStorage(location=path)  
        filename = fs.save(audited_file.name, audited_file)
        file_url = fs.url(filename)
        #u=User.objects.get(username=user)
        AuditFile.objects.create(user=user,file_url=file_url ,file_name=filename,hash=hash)
        
    def check_submited_files(self, hashprint, username):
        AuditFile = apps.get_model('csv_io','auditfile')   
        audited_file = None
        try :
            audited_file = AuditFile.objects.get(hash=hashprint)
        except AuditFile.DoesNotExist :
            print("le fichier n'a pas été trouvé")
            return hashprint
        else :
            print("le fichier a bien été trouvé")
            if audited_file.user==username :
                raise AlreadySubmitedFile("You have already submited this file")
            else :
                raise AlreadySubmitedFile("This file has been already submited by another user")

