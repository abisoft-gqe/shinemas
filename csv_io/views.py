from django.contrib.auth.decorators import user_passes_test, login_required
from django.shortcuts import render

from csv_io.models import AuditFile

@user_passes_test(lambda u: u.is_superuser)
@login_required 
def stored_files(request):
    username=request.user
    files=list(AuditFile.objects.filter(user__username=username) ) 
    return render(request, 'actors/auditFile.html', {'files':files})