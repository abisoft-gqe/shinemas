# # -*- coding: utf-8 -*-
# """ LICENCE:
#     SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
#     with the close collaboration of the RSP (Réseau Semence Paysanne)
#     
#     Copyright (C) 2014, Marie Lefebvre
# 
#     This file is part of SHiNeMaS
# 
#     SHiNeMaS is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
# 
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#     LICENCE
#     
#     Define administration interface configuration\n
#     For each ModelAdmin class :\n
#     1. fieldsets enable form configuration design\n
#     2. list_display is the list of field to display in the whole table\n
#     3. search_fields is a list of field you can use to search data\n
#     4. list_filter is a list of fields you can use to filter data\n
#     
#     For more information, read the `django admin documentation <https://docs.djangoproject.com/en/1.6/ref/contrib/admin/>`_\n
# """
# from django.utils.translation import gettext_lazy as _
# from django.contrib import admin
# 
# from analysis.models import Analysis, Group_Analysis, Analysis_File
# 
# 
# 
# class AnalysisAdmin(admin.ModelAdmin):
#     """
#         The ModelAdmin class for
#         :py:class:`analysis.models.Analysis`
#         model
#     """
#     list_display = ('user', 'analysis_name', 'comment', 'parameters')
#     Fieldset = [
#         
#         (_('User'), {'fields': ['user']}),
#         (_('Analysis name'), {'fields': ['analysis_name']}),
#         (_('Comment'), {'fields': ['comment']}),
# 		(_('Parameters'), {'fields': ['parameters']}),
# 
#         ]
# 
# 
# 
# 
# class Group_AnalysisAdmin(admin.ModelAdmin):
#     """
#         The ModelAdmin class for
#         :py:class:`analysis.models.Group_Analysis`
#         model
#     """
#     Fieldset = [
# 
#         (_('Group name'), {'fields': ['group_name']}),
#                 
#         ]
# 
# 
# class Analysis_FileAdmin(admin.ModelAdmin):
#     """
#         The ModelAdmin class for
#         :py:class:`analysis.models.Analysis_File`
#         model
#     """
#     list_display = ('analyse', 'RData_path','content_file')
#     Fieldset = [
#         
#         (_('Analyse'), {'fields': ['analyse']}),
#         (_('Path to de RData file'), {'fields': ['RData_path']}),
# 		(_('Content file sha1'), {'fields': ['content_file']}),
#                       
#         ]
# 
# admin.site.register(Analysis,AnalysisAdmin)
# admin.site.register(Group_Analysis,Group_AnalysisAdmin)
# admin.site.register(Analysis_File,Analysis_FileAdmin)

