# # -*- coding: utf-8 -*-
# """ LICENCE:
#     SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
#     with the close collaboration of the RSP (Réseau Semence Paysanne)
#     
#     Copyright (C) 2014, Marie Lefebvre, Alice Beaugrand
# 
#     This file is part of SHiNeMaS
# 
#     SHiNeMaS is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
# 
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#     LICENCE
#     
#     :data `STATUT_CHOICES`: A tuple related to :py:class:Group_Analysis class. Give a choice for the status of the analysis Private or Public
# """
# from jsonfield import JSONField
# 
# from django.db import models
# from django.contrib.auth.models import User
# 
# STATUT_CHOICES = (
#                   (u'Private', u'Private'),
#                   (u'Public', u'Public'),
#                   )  
# 
# class Analysis (models.Model):
#     """
#         The Analysis model contains description and path to file of analysis
#     
#         :var User user : The user that run the analysis
#         :var CharField analysis_name : analysis' name choosed by the user
#         :var CharField comment : comment on this analysis
#         :var JSONField parameters : a json dict that list all the parameters for the analysis
#         :var Analysis analysis : The previous step in the analyses pipeline
#     """
#     user = models.ForeignKey(User)
#     analysis_name = models.CharField(max_length=100)
#     comment = models.CharField(max_length=200,blank=True,null=True)     
#     parameters = JSONField()
#     analysis_ref =  models.ForeignKey("self",null=True,blank=True)
# 
#     def __unicode__(self):
#         return u"%s"%self.analysis_name
#     def __str__(self):
#         return "%s"%self.analysis_name
# 
#     class Meta :
#         verbose_name_plural = 'analyses'
# 
# class Group_Analysis(models.Model):
#     """
#         The Group_Analysis model is folder like structure that makes possible to group the analyses
#     
#         :var ManyToMany analyse : The analyses that are part of this group
#         :var CharField group_name : the name of this group
#         :var CharField statut : private or public. If private, only the user can see it
#     """
#     analyse = models.ManyToManyField(Analysis)
#     group_name = models.CharField(max_length=100)
#     statut = models.CharField(max_length=10,choices=STATUT_CHOICES)
# 
#     def __unicode__(self):
#         return u"%s"%self.group_name
#     def __str__(self):
#         return "%s"%self.group_name
#     class Meta :
#         verbose_name_plural = 'group_analyses'
# 
# class Analysis_File (models.Model):
#     """
#         The Analysis_File model contain the files related to the Analysis
#         
#         :var Analysis analyse: the py:class:Analysis related to this file
#         :var CharField RData_path: the path to the RData file
#         :var CharField PDF_path: the path to the PDF file
#         :var CharField content_file: the name of the file ?
#     """
#     analyse = models.ForeignKey(Analysis)
#     RData_path = models.CharField(max_length=500) 
#     PDF_path = models.CharField(max_length=500, blank=True, null=True)
#     content_file = models.CharField(unique=True, max_length=500) 
# 
#     def __unicode__(self):
#         return u"%s"%self.content_file
#     def __str__(self):
#         return "%s"%self.RData_path
# 

