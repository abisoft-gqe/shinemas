# # -*- coding: utf-8 -*-
# """ LICENCE:
#     SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
#     with the close collaboration of the RSP (Réseau Semence Paysanne)
#     
#     Copyright (C) 2014, Marie Lefebvre
# 
#     This file is part of SHiNeMaS
# 
#     SHiNeMaS is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
# 
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#     LICENCE
# """
#     
# from django.conf.urls import url
# from analysis.views import historic, automatisation, save_analysis, file_link2, file_link, start_analysis, change_template, template, figuresDrag, test_template, save_template, third_run_temp, readFileTemp, checkBoxFigures, formVecVar, formCorrGps, first_run, readFile, second_run, readFile2, third_run, readFile3, ajax_save, ajax_save2
# 
# 
# urlpatterns = [
#     url(r'^$', start_analysis ,name='startAnalysis'),
# 	url(r'^historic/', historic, name='historic'),
# 	url(r'^automatisation/', automatisation, name='automatisation'),
# 	url(r'^template/', template, name='template'),
# 	url(r'^save_analysis/',save_analysis,name='save_analysis'),
# 	url(r'^change_template/',change_template,name='change_template'),	
# 	url(r'^file_link/',file_link,name='file_link'),
# 	url(r'^file_link2/',file_link2,name='file_link2'),
# 	url(r'^test_template/',test_template,name='test_template'),
# 	url(r'^save_template/', save_template, name='save_template'),
# 	url(r'^third_run_temp/', third_run_temp, name='third_run_temp'),
# 	url(r'^first_run/', first_run, name='first_run'),
# 	url(r'^readFile/', readFile, name='readFile'),
# 	url(r'^readFile2/', readFile2, name='readFile2'),
# 	url(r'^readFile3/', readFile3, name='readFile3'),
# 	url(r'^readFileTemp/', readFileTemp, name='readFileTemp'),
# 	url(r'^formCorrGps/', formCorrGps, name='formCorrGps'),
# 	url(r'^formVecVar/', formVecVar, name='formVecVar'),
# 	url(r'^checkBoxFigures/',checkBoxFigures,name='checkBoxFigures'),
# 	url(r'^figuresDrag/',figuresDrag,name='figuresDrag'),
# 	url(r'^second_run/', second_run, name='second_run'),
# 	url(r'^third_run/', third_run, name='third_run'),
# 	url(r'^ajax_save/', ajax_save, name='ajax_save'),
# 	url(r'^ajax_save2/', ajax_save2, name='ajax_save2'),
# 
#     ]
