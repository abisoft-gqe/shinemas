# # -*- coding: utf-8 -*-
# """ LICENCE:
#     SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
#     with the close collaboration of the RSP (Réseau Semence Paysanne)
#     
#     Copyright (C) 2014, Marie Lefebvre, Alice Beaugrand
# 
#     This file is part of SHiNeMaS
# 
#     SHiNeMaS is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
# 
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#     LICENCE
#     
#     The workflow of the R package `shinemas2R <https://github.com/priviere/shinemas2R>`_ is organized in 4 views :\n
#     start_analysis --> first_run --> second_run --> third_run
# """
# import pickle
# import os, subprocess,threading,os.path
# import simplejson
# import hashlib
# import shutil
# import rpy2.robjects as robjects
# import rpy2.rinterface as rinterface
# from django.conf import settings
# 
# from django.shortcuts import render
# from django.http import HttpResponse,HttpResponseRedirect
# from django.contrib.auth.decorators import login_required
# 
# <<<<<<< .working
# from analysis.forms import GgplotNetworkForm, DataForm, GgplotDataTable, GetPdfFormTemp, saveForm
# from entities.models import Germplasm, Seed_lot, Germplasm_type
# ||||||| .merge-left.r674
# from analysis.forms import NetworkForm, GetDataForm, MeanComparisonForm, GxEform, netRelationForm, RelationForm, PlotDataForm,SaveForm
# from entities.models import Germplasm, Seed_lot
# =======
# from analysis.forms import NetworkForm, GetDataForm, MeanComparisonForm, GxEform, netRelationForm, RelationForm, PlotDataForm,SaveForm
# from entities.models import Germplasm, Seedlot
# >>>>>>> .merge-right.r679
# from actors.models import Person, Project
# from eppdata.models import Variable
# from network.models import Relation, Reproduction_method
# from analysis.models import Analysis, Group_Analysis, Analysis_File
# from django.contrib.auth.models import User
# 
# 
# #------------------------------------------------------------------------------------------------------------------------
# PROJECT_ROOT = os.path.dirname(os.path.dirname(__file__))
# username="alice/" ## <----- A changer selon le nom de l'utilisateur!!
# 
# 
# #------------------------------------------------------------------------------------------------------------------------
# #--------------------------------------------------- HOMEPAGE ----------------------------------------------------
# #------------------------------------------------------------------------------------------------------------------------
# @login_required
# 
# def start_analysis(request):
#     """
#         view that starts the analysis
#         
#         TODO: change usage of request object. request.GET is not used at all
#     """
#     #if get.POST["family_analysis"]=="Get the pdfTemp"
#         #from analysis.forms import GetPdfForm
#         #liste_elements=request.POST.getlist("elements")
#         #if liste_elements=="":
#             #liste_elements="NULL"
#         #else:
#             #if(len(liste_elements)==1):
#                 #liste_elements=request.POST["elements"]        
#                 #liste_elements=liste_elements.encode('utf-8')
#             #else:
#                 #liste_elements=[x.encode('utf-8') for x in (request.POST.getlist("elements"))]
#                 #liste_elements=tuple(liste_elements)
#         ##liste_elements="{0}".format(str(liste_elements)) 
#         
#         #template_name=request.POST["template_name"]
#         #formPdfTemp=GetPdfFormTemp()
#         #return render(request, 'analysis/proceed_analysis.html',{'formData':formData,'formPdfTemp':formPdfTemp,'template_name':template_name,'liste_elements':liste_elements})
#     #else:
#     liste_fichiers_templates=os.listdir(settings.MEDIA_ROOT_TEMPLATES+username)
#     liste_templates=[]
#     for i in liste_fichiers_templates:
#         if i.endswith(".p"):
#             liste_templates.append(pickle.load(open(settings.MEDIA_ROOT_TEMPLATES+username+i,"rb")))
#     
#     template_dico_keys=[]
#     template_dico_values=[]
#     for v in range(0,len(liste_templates),1):
#         for key, value in liste_templates[v].items():
#             template_dico_keys.append(key)
#             template_dico_values.append(value)
#     template_dico_keys=[x.encode('utf-8') for x in template_dico_keys]
#     v=0
#     liste_template_dico=[]
#     for i in template_dico_keys:
#         liste_template_dico.append([i,liste_templates[v][i]])
#         v=v+1
#     formData=DataForm()
#     return render(request, 'analysis/proceed_analysis.html',{'formData':formData,'liste_template_dico':liste_template_dico})
#     
# #-------------------------------------------------------------------------------------------------------------------------------------
# #------------------------------------------------------ FIRST --------------------------------------------------------------
# #-------------------------------------------------------------------------------------------------------------------------------------
# 
# def template(request):
#     liste_fichiers_templates=os.listdir(settings.MEDIA_ROOT_TEMPLATES+username)
#     liste_templates=[]
#     for i in liste_fichiers_templates:
#         if i.endswith(".p"):
#             liste_templates.append(pickle.load(open(settings.MEDIA_ROOT_TEMPLATES+username+i,"rb")))
# 
#     template_dico_keys=[]
#     template_dico_values=[]
#     for v in range(0,len(liste_templates),1):
#         for key, value in liste_templates[v].items():
#             template_dico_keys.append(key)
#             template_dico_values.append(value)
#     template_dico_keys=[x.encode('utf-8') for x in template_dico_keys]
#     v=0
#     liste_template_dico=[]
#     for i in template_dico_keys:
#         liste_template_dico.append([i,liste_templates[v][i]])
#         v=v+1
# 
#     return render(request,'analysis/template.html',{"liste_template_dico":liste_template_dico,"template_dico_values":template_dico_values,"template_dico_keys":template_dico_keys})
#  
# def save_template(request):
#     from analysis.forms import GetPdfForm
#     template_dico={}
#     
#     if request.POST.get("template1")=="SAVE":
#         liste_elements=request.POST.getlist("elements")
#         if liste_elements=="":
#             liste_elements="NULL"
#         else:
#             if(len(liste_elements)==1):
#                 liste_elements=request.POST["elements"]        
#                 liste_elements=liste_elements.encode('utf-8')
#             else:
#                 liste_elements=[x.encode('utf-8') for x in (request.POST.getlist("elements"))]
#                 liste_elements=tuple(liste_elements)
#         
#         template_name=request.POST["template_name"]
#         template_dico[template_name]=liste_elements
#         pickle.dump(template_dico,open((settings.MEDIA_ROOT_TEMPLATES+username+template_name+".p"),"wb"))
#         
#     liste_fichiers_templates=os.listdir(settings.MEDIA_ROOT_TEMPLATES+username)
#     liste_templates=[]
#     for i in liste_fichiers_templates:
#         if i.endswith(".p"):
#             liste_templates.append(pickle.load(open(settings.MEDIA_ROOT_TEMPLATES+username+i,"rb")))
#     
#     template_dico_keys=[]
#     template_dico_values=[]
#     for v in range(0,len(liste_templates),1):
#         for key, value in liste_templates[v].items():
#             template_dico_keys.append(key)
#             template_dico_values.append(value)
#     template_dico_keys=[x.encode('utf-8') for x in template_dico_keys]
#     v=0
#     liste_template_dico=[]
#     for i in template_dico_keys:
#         liste_template_dico.append([i,liste_templates[v][i]])
#         v=v+1
#     formPdf=GetPdfFormTemp()
#     return render(request,"analysis/save_template.html",{"liste_template_dico":liste_template_dico})
# 
# def change_template(request):
#     val=request.POST['val']
#     os.remove(settings.MEDIA_ROOT_TEMPLATES+username+val+'.p')
#     liste_fichiers_templates=os.listdir(settings.MEDIA_ROOT_TEMPLATES+username)
#     liste_templates=[]
#     for i in liste_fichiers_templates:
#         if i.endswith(".p"):
#             liste_templates.append(pickle.load(open(settings.MEDIA_ROOT_TEMPLATES+username+i,"rb")))
# 
#     template_dico_keys=[]
#     template_dico_values=[]
#     for v in range(0,len(liste_templates),1):
#         for key, value in liste_templates[v].items():
#             template_dico_keys.append(key)
#             template_dico_values.append(value)
#     template_dico_keys=[x.encode('utf-8') for x in template_dico_keys]
#     v=0
#     liste_template_dico=[]
#     for i in template_dico_keys:
#         liste_template_dico.append([i,liste_templates[v][i]])
#         v=v+1
#     formPdf=GetPdfFormTemp()
#     return render(request,"analysis/save_template.html",{"liste_template_dico":liste_template_dico,"template_dico_values":template_dico_values,"template_dico_keys":template_dico_keys,"formPdf":formPdf})
#     #return HttpResponse("{0}".format(val))
# 
# def test_template(request):
#     template_name=request.POST["template"]
#     template_dico=pickle.load(open(settings.MEDIA_ROOT_TEMPLATES+username+template_name+'.p',"rb"))
#     liste_elements=template_dico[template_name]
#     nombre_figures=liste_elements.count('TitleFigure')
#     
#     liste_figures_historic=request.POST.getlist("figures_historic")
#     if len(liste_figures_historic)==1:
#         liste_figures_historic=request.POST["figures_historic"].encode('utf-8')
#     else:
#         liste_figures_historic=[x.encode('utf-8') for x in (request.POST.getlist("figures_historic"))]
#         
#     formPdf=GetPdfFormTemp()
#     return render(request,"analysis/test_template.html",{"liste_figures_historic":liste_figures_historic,"template_name":template_name,"liste_elements":liste_elements,"formPdf":formPdf,"nombre_figures":nombre_figures}) 
#   
# class third_run_tempThread(threading.Thread):
#     def __init__(self,listArgs):
#         self.stdout = None
#         self.stderr = None
#         self.listArgs=listArgs
#         threading.Thread.__init__(self)
#         
#     def run(self):
#         listArgs=self.listArgs
#         filename='templateRun.R'
#         # Path complet du fichier .r
#         full_filename=settings.MEDIA_ROOT_ANALYSIS+username+'scriptR/'+filename
#         name=settings.MEDIA_ROOT_ANALYSIS+username+'templateRun.log'
#         log = open(name, "w")
#         
#         family_analysis=listArgs["family_analysis"]
#         if family_analysis=="Get the pdfTemp":
#             liste_elements_pdf=[x.encode('utf-8') for x in (listArgs.getlist("element_pdf"))]
#             if " " in liste_elements_pdf[1]:
#                 liste_elements_pdf[1]=liste_elements_pdf[1].replace(" ","_")
#             for i in range(0,len(liste_elements_pdf)):
#                 if liste_elements_pdf[i]=="":
#                     liste_elements_pdf[i]="NULL"
#             liste_elements_pdf=tuple(liste_elements_pdf)
#             liste_elements_pdf="{0}".format(str(liste_elements_pdf))
#             
#             proc = subprocess.Popen(["Rscript", full_filename, family_analysis, liste_elements_pdf],shell=False, stdout=log,stderr=log,universal_newlines=True)        
#             log.close()
# 
# def third_run_temp(request):
#     liste_elements_pdf=[x.encode('utf-8') for x in (request.POST.getlist("element_pdf"))]
#     for i in range(0,len(liste_elements_pdf)):
#         if liste_elements_pdf[i]=="":
#             liste_elements_pdf[i]="NULL"
#     form_name=liste_elements_pdf[1]
#     if " " in form_name:
#         form_name=form_name.replace(" ","_")
#     liste_elements_pdf=tuple(liste_elements_pdf)
# 
#     picture = settings.MEDIA_ROOT_ANALYSIS+username+'templateRun.pdf'
#     Rdata = settings.MEDIA_ROOT_ANALYSIS+username+'templateRun.RData'
#     if os.path.exists(picture) :
#         os.unlink(picture)
#     if os.path.exists(Rdata) :
#         os.unlink(Rdata)
#     formData=DataForm
#     runThread = third_run_tempThread(request.POST)
#     runThread.run()
#     return render(request,"analysis/third_run_temp.html",{"liste_elements_pdf":liste_elements_pdf,"form_name":form_name,"formData":formData})
# 
# class firstRunThread(threading.Thread):
#     def __init__(self,listArgs):
#         self.stdout = None
#         self.stderr = None
#         self.listArgs=listArgs
#         threading.Thread.__init__(self)
# 
#     def run(self):
#         listArgs=self.listArgs
#         filename='firstRun.R'
#         # Path complet du fichier .r
#         full_filename=settings.MEDIA_ROOT_ANALYSIS+username+'scriptR/'+filename
#         family_analysis=listArgs["family_analysis"]
# 
#         if family_analysis== "Get the data" :              
#             query_type= listArgs["query_type"]
#             
#             id_filter_on=listArgs["filter_on"]
#             if id_filter_on=="":
#                 filter_on="NULL"
#             else:
#                 filter_on=listArgs["filter_on"]
#             
#             fill_diff_gap=listArgs["fill_diff_gap"]
#             network_info=listArgs["network_info"]
#             mdist=listArgs["mdist"]
#             
#             id_germplasm_in=listArgs["#liste_germ_in"]
#             id2_germplasm_in=listArgs.getlist("#liste_germ_in")
#             if id_germplasm_in=='':
#                 germplasm_in="NULL"
#             else:
#                 if(len(id2_germplasm_in)==2):
#                     germ_in=listArgs["#liste_germ_in"]        
#                     germ_in=germ_in.encode('utf-8')
#                 else:
#                     germ_in=[x.encode('utf-8') for x in (listArgs.getlist("#liste_germ_in"))]
#                     germ_in.remove('')
#                     germ_in=tuple(germ_in)
#                 germplasm_in="{0}".format(str(germ_in))
#                 
#             id_germplasm_out=listArgs["#liste_germ_out"]
#             id2_germplasm_out=listArgs.getlist("#liste_germ_out")
#             if id_germplasm_out=='':
#                 germplasm_out="NULL"
#             else:
# <<<<<<< .working
#                 if(len(id2_germplasm_out)==2):
#                     germ_out=listArgs["#liste_germ_out"]        
#                     germ_out=germ_out.encode('utf-8')
#                 else:
#                     germ_out=[x.encode('utf-8') for x in (listArgs.getlist("#liste_germ_out"))]
#                     germ_out.remove('')
#                     germ_out=tuple(germ_out)
#                 germplasm_out="{0}".format(str(germ_out))
#                 
#             id_germplasm_type_in=listArgs["germplasm_type_in"]
#             id2_germplasm_type_in=listArgs.getlist("germplasm_type_in")
#             if id_germplasm_type_in=='':
#                 germplasm_type_in="NULL"
#             else:
#                 if(len(id2_germplasm_type_in)==1):
#                     germ_type_in=str(Germplasm_type.objects.get(id=id_germplasm_type_in))
#                 else:
#                     for x in id2_germplasm_type_in:
#                         if (x==""):
#                             id2_germplasm_type_in.remove("")
#                     germ_type_in=[str(Germplasm_type.objects.get(id=x)) for x in id2_germplasm_type_in]
#                     germ_type_in=tuple(germ_type_in)
#                 germplasm_type_in="{0}".format(str(germ_type_in))  
#                 
#             id_germplasm_type_out=listArgs["germplasm_type_out"]
#             id2_germplasm_type_out=listArgs.getlist("germplasm_type_out")
#             if id_germplasm_type_out=='':
#                 germplasm_type_out="NULL"
#             else:
#                 if(len(id2_germplasm_type_out)==1):
#                     germ_type_out=str(Germplasm_type.objects.get(id=id_germplasm_type_out))
#                 else:
#                     for x in id2_germplasm_type_out:
#                         if (x==""):
#                             id2_germplasm_type_out.remove("")
#                     germ_type_out=[str(Germplasm_type.objects.get(id=x)) for x in id2_germplasm_type_out]                    
#                     germ_type_out=tuple(germ_type_out)
#                 germplasm_type_out="{0}".format(str(germ_type_out))
#                 
#             id_year_in=listArgs["year_in"]
#             id2_year_in=listArgs.getlist("year_in")
#             if id_year_in=='':
#                 year_in="NULL"
#             else:
#                 if(len(id2_year_in)==1):
#                     ye_in=listArgs["year_in"]        
#                 else:
#                     ye_in=[x.encode() for x in (listArgs.getlist("year_in"))]
#                     for x in ye_in:
#                         if (x==""):
#                             ye_in.remove("")
#                     ye_in=tuple(ye_in)
#                 year_in="'{0}'".format(str(ye_in))
#                 
#             id_year_out=listArgs["year_out"]
#             id2_year_out=listArgs.getlist("year_out")
#             if id_year_out=='':
#                 year_out="NULL"
#             else:
#                 if(len(id2_year_out)==1):
#                     ye_out=listArgs["year_out"]        
#                 else:
#                     ye_out=[x.encode() for x in (listArgs.getlist("year_out"))]
#                     for x in ye_out:
#                         if (x==""):
#                             ye_out.remove("")
#                     ye_out=tuple(ye_out)
#                 year_out="{0}".format(str(ye_out))
#                 
#             id_project_in=listArgs["project_in"]
#             id2_project_in=listArgs.getlist("project_in")
#             if id_project_in=='':
#                 project_in="NULL"
#             else:
#                 if(len(id2_project_in)==1):
#                     proj_in=str(Project.objects.get(id=id_project_in))
#                 else:
#                     proj_in=[str(Project.objects.get(id=x)) for x in id2_project_in]
#                     for x in proj_in:
#                         if (x==""):
#                             proj_in.remove("")
#                     proj_in=tuple(proj_in)
#                 project_in="{0}".format(str(proj_in))
#               
#             id_project_out=listArgs["project_out"]
#             id2_project_out=listArgs.getlist("project_out")
#             if id_project_out=='':
#                 project_out="NULL"
#             else:
#                 if(len(id2_project_out)==1):
#                     proj_out=str(Project.objects.get(id=id_project_out))
#                 else:
#                     proj_out=[str(Project.objects.get(id=x)) for x in id2_project_out]
#                     for x in proj_out:
#                         if (x==""):
#                             proj_out.remove("")
#                     proj_out=tuple(proj_out)
#                 project_out="{0}".format(str(proj_out))
#                 
#             id_person_in=listArgs["#liste_person_in"]
#             id2_person_in=listArgs.getlist("#liste_person_in")
#             if id_person_in=='':
#                 person_in="NULL"
#             else:
#                 if(len(id2_person_in)==2):
#                     pers_in=listArgs["#liste_person_in"]        
#                     pers_in=pers_in.encode('utf-8')        
#                 else:
#                     pers_in=[x.encode('utf-8') for x in (listArgs.getlist("#liste_person_in"))]
#                     pers_in.remove('')
#                     pers_in=tuple(pers_in)
#                 person_in="{0}".format(str(pers_in)) 
#                 
#             id_person_out=listArgs["#liste_person_out"]
#             id2_person_out=listArgs.getlist("#liste_person_out")
#             if id_person_out=='':
#                 person_out="NULL"
#             else:
#                 if(len(id2_person_out)==2):
#                     pers_out=listArgs["#liste_person_out"]        
#                     pers_out=pers_out.encode('utf-8')        
#                 else:
#                     pers_out=[x.encode('utf-8') for x in (listArgs.getlist("#liste_person_out"))]
#                     pers_out.remove('')
#                     pers_out=tuple(pers_out)
#                 person_out="{0}".format(str(pers_out))  
#             
#             id_seed_lots_in=listArgs["#liste_sl_in"]
#             id2_seed_lots_in=listArgs.getlist("#liste_sl_in")
#             if id_seed_lots_in=='':
#                 seed_lots_in="NULL"
#             else:
#                 if(len(id2_seed_lots_in)==2):
#                     sl_in=listArgs["#liste_sl_in"]        
#                     sl_in=sl_in.encode('utf-8')        
#                 else:
#                     sl_in=[x.encode('utf-8') for x in (listArgs.getlist("#liste_sl_in"))]
#                     sl_in.remove('')
#                     sl_in=tuple(sl_in)
#                 seed_lots_in="{0}".format(str(sl_in))
#             
#             id_seed_lots_out=listArgs["#liste_sl_out"]
#             id2_seed_lots_out=listArgs.getlist("#liste_sl_out")
#             if id_seed_lots_out=='':
#                 seed_lots_out="NULL"
#             else:
#                 if(len(id2_seed_lots_out)==2):
#                     sl_out=listArgs["#liste_sl_out"]        
#                     sl_out=sl_out.encode('utf-8')        
#                 else:
#                     sl_out=[x.encode('utf-8') for x in (listArgs.getlist("#liste_sl_out"))]
#                     sl_out.remove('')
#                     sl_out=tuple(sl_out)
#                 seed_lots_out="{0}".format(str(sl_out))
#                 
#             id_relation_in=listArgs["relation_in"]
#             if id_relation_in=="":
#                 relation_in="NULL"
#             else:
#                 relation_in=listArgs["relation_in"]
#                 
#             id_repro_type_in=listArgs["repro_type_in"]
#             if id_repro_type_in=="":
#                 repro_type_in="NULL"
#             else:
#                 repro_type_in=[str(Reproduction_method.objects.get(id=x)) for x in id_repro_type_in]
#                 repro_type_in="{0}".format(str(repro_type_in[0]))
#         
#             id_variables_in=listArgs["#liste_var_in"]
#             id2_variables_in=listArgs.getlist("#liste_var_in")
#             if id_variables_in=='':
#                 variables_in="NULL"
#             else:
#                 if(len(id2_variables_in)==2):
#                     var_in=listArgs["#liste_var_in"]        
#                 else:
#                     var_in=[x.encode('utf-8') for x in (listArgs.getlist("#liste_var_in"))]
#                     var_in.remove('')
#                     var_in=tuple(var_in)
#                 variables_in="{0}".format(str(var_in))
#                         
#                             
#             id_data_type=listArgs["data_type"]
#             if id_data_type=="":
#                 data_type="NULL"
#             else:
#                 data_type=listArgs["data_type"]
#                   
#             name=settings.MEDIA_ROOT_ANALYSIS+username+'firstRun.log'
#             log = open(name, "w")
#             proc = subprocess.Popen(["Rscript", full_filename, family_analysis, query_type, filter_on, fill_diff_gap, network_info, mdist, germplasm_in, germplasm_out, germplasm_type_in, germplasm_type_out, year_in, year_out, project_in, project_out, person_in, person_out, seed_lots_in, seed_lots_out, relation_in, repro_type_in, variables_in, data_type],shell=False, stdout=log,stderr=log,universal_newlines=True)        
# ||||||| .merge-left.r674
#                 SL_name = unicode(Seed_lot.objects.get(id=id_SL_name))
#             family_analysis = listArgs["family_analysis"]      
#             log = open(name, "w")      
#             proc = subprocess.Popen(["Rscript", full_filename, family_analysis, graph, legend_another_plot, vertex_legend, edge_legend, interactive, gender, hide_name, fill_diffusion_gap, display_label, text_size, title, col_type, diffusion_color, color_reproduction, color_mixture, color_selection, display_diffusion, display_reproduction, display_mixture, display_selection, display_reproduction_type, display_generation, germplasm_in, germplasm_out, person_in,person_out, year_in, year_out, project_in, project_out, SL_name],shell=False, stdout=log,stderr=log,universal_newlines=True)        
# =======
#                 SL_name = unicode(Seedlot.objects.get(id=id_SL_name))
#             family_analysis = listArgs["family_analysis"]      
#             log = open(name, "w")      
#             proc = subprocess.Popen(["Rscript", full_filename, family_analysis, graph, legend_another_plot, vertex_legend, edge_legend, interactive, gender, hide_name, fill_diffusion_gap, display_label, text_size, title, col_type, diffusion_color, color_reproduction, color_mixture, color_selection, display_diffusion, display_reproduction, display_mixture, display_selection, display_reproduction_type, display_generation, germplasm_in, germplasm_out, person_in,person_out, year_in, year_out, project_in, project_out, SL_name],shell=False, stdout=log,stderr=log,universal_newlines=True)        
# >>>>>>> .merge-right.r679
#             log.close()
#                 
#         elif family_analysis == "Get the network" : 
#                 query_type=listArgs["query_type"]
#                 filter_on=listArgs["filter_on"]
#                 fill_diff_gap=listArgs["fill_diff_gap"]
#                 network_info=listArgs["network_info"]
#                 mdist=listArgs["mdist"]
#                 germplasm_in=listArgs["germplasm_in"]
#                 germplasm_out=listArgs["germplasm_out"]
#                 germplasm_type_in=listArgs["germplasm_type_in"]
#                 germplasm_type_out=listArgs["germplasm_type_out"]
#                 year_in=listArgs["year_in"]
#                 year_out=listArgs["year_out"]
#                 project_in=listArgs["project_in"]
#                 project_out=listArgs["project_out"]
#                 person_in=listArgs["person_in"]
#                 person_out=listArgs["person_out"]
#                 seed_lots_in=listArgs["seed_lots_in"]
#                 seed_lots_out=listArgs["seed_lots_out"]
#                 relation_in=listArgs["relation_in"]
#                 repro_type_in=listArgs["repro_type_in"]
#                 variables_in=listArgs["variables_in"]
#                 data_type=listArgs["data_type"]
#                 
#                 name=settings.MEDIA_ROOT_ANALYSIS+username+'firstRun.log'
#                 log = open(name, "w")
#                 proc = subprocess.Popen(["Rscript", full_filename, family_analysis, query_type, filter_on, fill_diff_gap, network_info, mdist, germplasm_in, germplasm_out, germplasm_type_in, germplasm_type_out, year_in, year_out, project_in, project_out, person_in, person_out, seed_lots_in, seed_lots_out, relation_in, repro_type_in, variables_in, data_type],shell=False, stdout=log,stderr=log,universal_newlines=True)        
#                 log.close()
#                 
#         elif family_analysis == "historic" :
#                 query_type= listArgs["query_type"].encode("utf-8")        
#                 filter_on=listArgs["filter_on"].encode("utf-8")
#                 fill_diff_gap=listArgs["fill_diff_gap"].encode("utf-8")
#                 network_info=listArgs["network_info"].encode("utf-8")
#                 mdist=listArgs["mdist"].encode("utf-8")
#                 
#                 id_germplasm_in=listArgs["germplasm_in"]
#                 if(id_germplasm_in=="NULL" or len(id_germplasm_in)==1):
#                     germ_in=listArgs["germplasm_in"]        
#                     germ_in=germ_in.encode('utf-8')
#                 else:
#                     id_germplasm_in=list(id_germplasm_in)
#                     longueur=len(id_germplasm_in)
#                     j=0
#                     for i in range(0,longueur):
#                         if id_germplasm_in[j]=="(" or id_germplasm_in[j]==")" or id_germplasm_in[j]=="'":
#                             del(id_germplasm_in[j])
#                             longueur=longueur-1
#                         else:
#                             j=j+1
#                     id_germplasm_out=''.join(id_germplasm_in)
#                     id_germplasm_in=id_germplasm_out.split(',')
#                             
#                     for x in id_germplasm_in:
#                             if (x==""):
#                                 id_germplasm_in.remove("")
#                     germ_in=tuple(id_germplasm_in)
#                 germplasm_in="{0}".format(str(germ_in))  
#                                               
#                 id_germplasm_out=listArgs["germplasm_out"]
#                 if(id_germplasm_out=="NULL" or len(id_germplasm_out)==1):
#                     germ_out=listArgs["germplasm_out"]        
#                     germ_out=germ_out.encode('utf-8')                
#                 else:
#                     id_germplasm_out=list(id_germplasm_out)
#                     longueur=len(id_germplasm_out)
#                     j=0
#                     for i in range(0,longueur):
#                         if id_germplasm_out[j]=="(" or id_germplasm_out[j]==")" or id_germplasm_out[j]=="'":
#                             del(id_germplasm_out[j])
#                             longueur=longueur-1
#                         else:
#                             j=j+1
#                     id_germplasm_out=''.join(id_germplasm_out)
#                     id_germplasm_out=id_germplasm_out.split(',')
#                             
#                     for x in id_germplasm_out:
#                             if (x==""):
#                                 id_germplasm_out.remove("")
#                     germ_out=tuple(id_germplasm_out)
#                 germplasm_out="{0}".format(str(germ_out))  
#                     
#                 id_germplasm_type_in=listArgs["germplasm_type_in"]
#                 if(id_germplasm_type_in=="NULL" or len(id_germplasm_type_in)==1):
#                     germ_type_in=listArgs["germplasm_type_in"]
#                     if len(id_germplasm_type_in)==1:
#                       germ_type_in=str(Project.objects.get(id=germ_type_in))
#                 else:
#                     id_germplasm_type_in=list(id_germplasm_type_in)
#                     longueur=len(id_germplasm_type_in)
#                     j=0
#                     for i in range(0,longueur):
#                         if id_germplasm_type_in[j]=="(" or id_germplasm_type_in[j]==")" or id_germplasm_type_in[j]=="'":
#                             del(id_germplasm_type_in[j])
#                             longueur=longueur-1
#                         else:
#                             j=j+1
#                     id_germplasm_type_in=''.join(id_germplasm_type_in)
#                     id_germplasm_type_in=id_germplasm_type_in.split(',')
#                             
#                     for x in id_germplasm_type_in:
#                             if (x==""):
#                                 id_germplasm_type_in.remove("")
#                     germ_type_in=tuple(id_germplasm_type_in)
#                 germplasm_type_in="{0}".format(str(germ_type_in))  
#                     
#                 id_germplasm_type_out=listArgs["germplasm_type_out"]
#                 if(id_germplasm_type_out=="NULL" or len(id_germplasm_type_out)==1):
#                     germ_type_out=listArgs["germplasm_type_out"]
#                     if len(id_germplasm_type_out)==1:
#                       germ_type_out=str(Project.objects.get(id=germ_type_out))
#                 else:
#                     id_germplasm_type_out=list(id_germplasm_type_out)
#                     longueur=len(id_germplasm_type_out)
#                     j=0
#                     for i in range(0,longueur):
#                         if id_germplasm_type_out[j]=="(" or id_germplasm_type_out[j]==")" or id_germplasm_type_out[j]=="'":
#                             del(id_germplasm_type_out[j])
#                             longueur=longueur-1
#                         else:
#                             j=j+1
#                     id_germplasm_type_out=''.join(id_germplasm_type_out)
#                     id_germplasm_type_out=id_germplasm_type_out.split(',')
#                             
#                     for x in id_germplasm_type_out:
#                             if (x==""):
#                                 id_germplasm_type_out.remove("")
#                     germ_type_out=tuple(id_germplasm_type_out)
#                 germplasm_type_out="{0}".format(str(germ_type_out))
#                     
#                 id_year_in=listArgs["year_in"]
#                 if(id_year_in=="NULL" or len(id_year_in)==1):
#                     ye_in=listArgs["year_in"]        
#                 else:
#                     id_year_in=list(id_year_in)
#                     longueur=len(id_year_in)
#                     j=0
#                     for i in range(0,longueur):
#                         if id_year_in[j]=="(" or id_year_in[j]==")" or id_year_in[j]=="'":
#                             del(id_year_in[j])
#                             longueur=longueur-1
#                         else:
#                             j=j+1
#                     id_year_in=''.join(id_year_in)
#                     id_year_in=id_year_in.split(',')
#                             
#                     for x in id_year_in:
#                             if (x==""):
#                                 id_year_in.remove("")
#                     ye_in=tuple(id_year_in)
#                 year_in="{0}".format(str(ye_in))
#                     
#                 id_year_out=listArgs["year_out"]
#                 if(id_year_out=="NULL" or len(id_year_out)==1):
#                     ye_out=listArgs["year_out"]        
#                 else:
#                     id_year_out=list(id_year_out)
#                     longueur=len(id_year_out)
#                     j=0
#                     for i in range(0,longueur):
#                         if id_year_out[j]=="(" or id_year_out[j]==")" or id_year_out[j]=="'":
#                             del(id_year_out[j])
#                             longueur=longueur-1
#                         else:
#                             j=j+1
#                     id_year_out=''.join(id_year_out)
#                     id_year_out=id_year_out.split(',')
#                             
#                     for x in id_year_out:
#                             if (x==""):
#                                 id_year_out.remove("")
#                     ye_out=tuple(id_year_out)
#                 year_out="{0}".format(str(ye_out))
#                     
#                 id_project_in=listArgs["project_in"]
#                 if(id_project_in=="NULL" or len(id_project_in)==1):
#                     proj_in=listArgs["project_in"]        
#                     if len(id_project_in)==1:
#                       proj_in=str(Project.objects.get(id=proj_in))
#                 else:
#                     id_project_in=list(id_project_in)
#                     longueur=len(id_project_in)
#                     j=0
#                     for i in range(0,longueur):
#                         if id_project_in[j]=="(" or id_project_in[j]==")" or id_project_in[j]=="'":
#                             del(id_project_in[j])
#                             longueur=longueur-1
#                         else:
#                             j=j+1
#                     id_project_in2=''.join(id_project_in)
#                     id_project_in=id_project_in2.split(',')
#                             
#                     for x in id_project_in:
#                             if (x==""):
#                                 id_project_in.remove("")
#                     proj_in=tuple(id_project_in)
#                 project_in="{0}".format(str(proj_in))
#                   
#                 id_project_out=listArgs["project_out"]
#                 if(id_project_out=="NULL" or len(id_project_out)==1):
#                     proj_out=listArgs["project_out"]        
#                     if len(id_project_out)==1:
#                       proj_in=str(Project.objects.get(id=proj_out))
#                 else:
#                     id_project_out=list(id_project_out)
#                     longueur=len(id_project_out)
#                     j=0
#                     for i in range(0,longueur):
#                         if id_project_out[j]=="(" or id_project_out[j]==")" or id_project_out[j]=="'":
#                             del(id_project_out[j])
#                             longueur=longueur-1
#                         else:
#                             j=j+1
#                     id_project_out=''.join(id_project_out)
#                     id_project_out=id_project_out.split(',')
#                             
#                     for x in id_project_out:
#                             if (x==""):
#                                 id_project_out.remove("")
#                     proj_out=tuple(id_project_out)
#                 project_out="{0}".format(str(proj_out))
#                        
#                 id_person_in=listArgs["person_in"]
#                 if(id_person_in=="NULL" or len(id_person_in)==1):
#                     pers_in=listArgs["person_in"]        
#                 else:
#                     id_person_in=list(id_person_in)
#                     longueur=len(id_person_in)
#                     j=0
#                     for i in range(0,longueur):
#                         if id_person_in[j]=="(" or id_person_in[j]==")" or id_person_in[j]=="'":
#                             del(id_person_in[j])
#                             longueur=longueur-1
#                         else:
#                             j=j+1
#                     id_person_in=''.join(id_person_in)
#                     id_person_in=id_person_in.split(',')
#                             
#                     for x in id_person_in:
#                             if (x==""):
#                                 id_person_in.remove("")
#                     pers_in=tuple(id_person_in)
#                 person_in="{0}".format(str(pers_in))
#                 
#                 id_person_out=listArgs["person_out"]
#                 if(id_person_out=="NULL" or len(id_person_out)==1):
#                     pers_out=listArgs["person_out"]        
#                 else:
#                     id_person_out=list(id_person_out)
#                     longueur=len(id_person_out)
#                     j=0
#                     for i in range(0,longueur):
#                         if id_person_out[j]=="(" or id_person_out[j]==")" or id_person_out[j]=="'":
#                             del(id_person_out[j])
#                             longueur=longueur-1
#                         else:
#                             j=j+1
#                     id_person_out=''.join(id_person_out)
#                     id_person_out=id_person_out.split(',')
#                             
#                     for x in id_person_out:
#                             if (x==""):
#                                 id_person_out.remove("")
#                     pers_out=tuple(id_person_out)
#                 person_out="{0}".format(str(pers_out))
#                             
#                 id_seed_lots_in=listArgs["seed_lots_in"]
#                 if(id_seed_lots_in=="NULL" or len(id_seed_lots_in)==1):
#                     sl_in=listArgs["seed_lots_in"]        
#                     sl_in=sl_in.encode('utf-8')                
#                 else:
#                     id_seed_lots_in=list(id_seed_lots_in)
#                     longueur=len(id_seed_lots_in)
#                     j=0
#                     for i in range(0,longueur):
#                         if id_seed_lots_in[j]=="(" or id_seed_lots_in[j]==")" or id_seed_lots_in[j]=="'":
#                             del(id_seed_lots_in[j])
#                             longueur=longueur-1
#                         else:
#                             j=j+1
#                     id_seed_lots_in=''.join(id_seed_lots_in)
#                     id_seed_lots_in=id_seed_lots_in.split(',')
#                             
#                     for x in id_seed_lots_in:
#                             if (x==""):
#                                 id_seed_lots_in.remove("")
#                     sl_in=tuple(id_seed_lots_in)
#                 seed_lots_in="{0}".format(str(sl_in))
#                 
#                 id_seed_lots_out=listArgs["seed_lots_out"]
#                 if(id_seed_lots_out=="NULL" or len(id_seed_lots_out)==1):
#                     sl_out=listArgs["seed_lots_out"]        
#                     sl_out=sl_out.encode('utf-8')                
#                 else:
#                     id_seed_lots_out=list(id_seed_lots_out)
#                     longueur=len(id_seed_lots_out)
#                     j=0
#                     for i in range(0,longueur):
#                         if id_seed_lots_out[j]=="(" or id_seed_lots_out[j]==")" or id_seed_lots_out[j]=="'":
#                             del(id_seed_lots_out[j])
#                             longueur=longueur-1
#                         else:
#                             j=j+1
#                     id_seed_lots_out=''.join(id_seed_lots_out)
#                     id_seed_lots_out=id_seed_lots_out.split(',')
#                             
#                     for x in id_seed_lots_out:
#                             if (x==""):
#                                 id_seed_lots_out.remove("")
#                     sl_out=tuple(id_seed_lots_out)
#                 seed_lots_out="{0}".format(str(sl_out))
#                     
#                 relation_in=listArgs["relation_in"]
#                     
#                 id_repro_type_in=listArgs["repro_type_in"]
#                 if(id_repro_type_in=="NULL" or len(id_repro_type_in)==1):
#                     repro_type_in=listArgs["seed_lots_in"]        
#                     repro_type_in=repro_type_in.encode('utf-8')        
#                     if len(id_repro_type_in)==1:
#                       repro_type_in=str(Project.objects.get(id=repro_type_in))
#                 else:
#                     repro_type_in=[str(Reproduction_method.objects.get(id=x)) for x in id_repro_type_in]
#                     repro_type_in="{0}".format(repro_type_in[0])
#             
#                 id_variables_in=listArgs["variables_in"]
#                 if(id_variables_in=="NULL" or len(id_variables_in)==1):
#                     var_in=listArgs["variables_in"]        
#                 else:
#                     id_variables_in=list(id_variables_in)
#                     longueur=len(id_variables_in)
#                     j=0
#                     for i in range(0,longueur):
#                         if id_variables_in[j]=="(" or id_variables_in[j]==")" or id_variables_in[j]=="'":
#                             del(id_variables_in[j])
#                             longueur=longueur-1
#                         else:
#                             j=j+1
#                     id_variables_in=''.join(id_variables_in)
#                     id_variables_in=id_variables_in.split(',')
#                             
#                     for x in id_variables_in:
#                             if (x==""):
#                                 id_variables_in.remove("")
#                     var_in=tuple(id_variables_in)
#                 variables_in="{0}".format(str(var_in))
#                 
#                 data_type=listArgs["data_type"]
# 
#                 origine=listArgs["origine"]
#                 analysis_name=listArgs["analysis_name"]
#                 name=settings.MEDIA_ROOT_ANALYSIS+username+'firstRun.log'
#                 log = open(name, "w")
#                 proc = subprocess.Popen(["Rscript", full_filename, family_analysis, query_type, filter_on, fill_diff_gap, network_info, mdist, germplasm_in, germplasm_out, germplasm_type_in, germplasm_type_out, year_in, year_out, project_in, project_out, person_in, person_out, seed_lots_in, seed_lots_out, relation_in, repro_type_in, variables_in, data_type, analysis_name, origine],shell=False, stdout=log,stderr=log,universal_newlines=True)        
#                 log.close()
#         
# #-------------------------------------------------------------------------------------------------------------------------------------
# #--------------------------------------------------- FIRST ANALYSE VIEW ---------------------------------------------
# @login_required
# def first_run(request):
#     from django.utils.datastructures import MultiValueDictKeyError
#     from analysis.forms import GgplotDataForm
#     result=request.POST
#      
#     family_analysis=result["family_analysis"]
#     
#     if family_analysis == "Get the data" :      
#         query_type= result["query_type"]
#         
#         id_filter_on=result["filter_on"]
#         if id_filter_on=="":
#             filter_on="NULL"
#         else:
#             filter_on=result["filter_on"]
#         
#         fill_diff_gap=result["fill_diff_gap"]
#         network_info=result["network_info"]
#         mdist=result["mdist"]
#         
#         
#         id_germplasm_in=result["#liste_germ_in"]
#         id2_germplasm_in=result.getlist("#liste_germ_in")
#         if id_germplasm_in=='':
#             germplasm_in="NULL"
#         else:
#             if(len(id2_germplasm_in)==2):
#                 germ_in=result["#liste_germ_in"]        
#                 germ_in=germ_in.encode('utf-8')
#             else:
#                 germ_in=[x.encode('utf-8') for x in (result.getlist("#liste_germ_in"))]
#                 if '' in germ_in:
#                   germ_in.remove('')
#                 germ_in=tuple(germ_in)
#             germplasm_in="{0}".format(str(germ_in))  
#                                        
#         id_germplasm_out=result["#liste_germ_out"]
#         id2_germplasm_out=result.getlist("#liste_germ_out")
#         if id_germplasm_out=='':
#             germplasm_out="NULL"
#         else:
#             if(len(id2_germplasm_out)==2):
#                 germ_out=result["#liste_germ_out"]        
#                 germ_out=germ_out.encode('utf-8')                
#             else:
#                 germ_out=[x.encode('utf-8') for x in (result.getlist("#liste_germ_out"))]
#                 if '' in germ_out:
#                   germ_out.remove('')
#                 germ_out=tuple(germ_out)
#             germplasm_out="{0}".format(str(germ_out))  
#             
#         id_germplasm_type_in=result["germplasm_type_in"]
#         id2_germplasm_type_in=result.getlist("germplasm_type_in")
#         if id_germplasm_type_in=='':
#             germplasm_type_in="NULL"
#         else:
#             if(len(id2_germplasm_type_in)==1):
#                 germ_type_in=result["germplasm_type_in"]        
#             else:
#                 for x in id2_germplasm_type_in:
#                         if (x==""):
#                             id2_germplasm_type_in.remove("")
#                 germ_type_in=[str(Germplasm_type.objects.get(id=x)) for x in id2_germplasm_type_in]            
#                 germ_type_in=tuple(germ_type_in)
#             germplasm_type_in="{0}".format(str(germ_type_in))  
#             
#         id_germplasm_type_out=result["germplasm_type_out"]
#         id2_germplasm_type_out=result.getlist("germplasm_type_out")
#         if id_germplasm_type_out=='':
#             germplasm_type_out="NULL"
#         else:
#             if(len(id2_germplasm_type_out)==1):
#                 germ_type_out=result["germplasm_type_out"]        
#             else:
#                 for x in id2_germplasm_type_out:
#                           if (x==""):
#                               id2_germplasm_type_out.remove("")
#                 germ_type_out=[str(Germplasm_type.objects.get(id=x)) for x in id2_germplasm_type_out]
#                 germ_type_out=tuple(germ_type_out)
#             germplasm_type_out="{0}".format(str(germ_type_out))
#             
#         id_year_in=result["year_in"]
#         id2_year_in=result.getlist("year_in")
#         if id_year_in=='':
#             year_in="NULL"
#         else:
#             if(len(id2_year_in)==1):
#                 ye_in=result["year_in"]        
#             else:
#                 ye_in=[x.encode() for x in (result.getlist("year_in"))]
#                 for x in ye_in:
#                         if (x==""):
#                             ye_in.remove("")
#                 ye_in=tuple(ye_in)
#             year_in="{0}".format(str(ye_in))
#             
#         id_year_out=result["year_out"]
#         id2_year_out=result.getlist("year_out")
#         if id_year_out=='':
#             year_out="NULL"
#         else:
#             if(len(id2_year_out)==1):
#                 ye_out=result["year_out"]        
#             else:
#                 ye_out=[x.encode() for x in (result.getlist("year_out"))]
#                 for x in ye_out:
#                         if (x==""):
#                             ye_out.remove("")
#                 ye_out=tuple(ye_out)
#             year_out="{0}".format(str(ye_out))
#             
#         id_project_in=result["project_in"]
#         id2_project_in=result.getlist("project_in")
#         if id_project_in=='':
#             project_in="NULL"
#         else:
#             if(len(id2_project_in)==1):
#                 proj_in=result["project_in"]        
#             else:
#                 for x in id2_project_in:
#                         if (x==""):
#                             id2_project_in.remove("")
#                 proj_in=[str(Project.objects.get(id=x)) for x in id2_project_in] 
#                 proj_in=tuple(proj_in)
#             project_in="{0}".format(str(proj_in))
#           
#         id_project_out=result["project_out"]
#         id2_project_out=result.getlist("project_out")
#         if id_project_out=='':
#             project_out="NULL"
#         else:
#             if(len(id2_project_out)==1):
#                 proj_out=result["project_out"]        
#             else:
#                 for x in id2_project_out:
#                         if (x==""):
#                             id2_project_out.remove("")
#                 proj_out=[str(Project.objects.get(id=x)) for x in id2_project_out]
#                 proj_out=tuple(proj_out)
#             project_out="{0}".format(str(proj_out))
#             
#             
#         id_person_in=result["#liste_person_in"]
#         id2_person_in=result.getlist("#liste_person_in")
#         if id_person_in=='':
#             person_in="NULL"
#         else:
#             if(len(id2_person_in)==2):
#                 pers_in=result["#liste_person_in"]        
#                 pers_in=pers_in.encode('utf-8')                
#             else:
#                 pers_in=[x.encode('utf-8') for x in (result.getlist("#liste_person_in"))]
#                 if '' in pers_in:
#                   pers_in.remove('')
#                 pers_in=tuple(pers_in)
#             person_in="{0}".format(str(pers_in)) 
#             
#         id_person_out=result["#liste_person_out"]
#         id2_person_out=result.getlist("#liste_person_out")
#         if id_person_out=='':
#             person_out="NULL"
#         else:
#             if(len(id2_person_out)==2):
#                 pers_out=result["#liste_person_out"]        
#                 pers_out=pers_out.encode('utf-8')                
#             else:
#                 pers_out=[x.encode('utf-8') for x in (result.getlist("#liste_person_out"))]
#                 if '' in pers_out:
#                   pers_out.remove('')
#                 pers_out=tuple(pers_out)
#             person_out="{0}".format(str(pers_out))  
#             
#         
#         id_seed_lots_in=result["#liste_sl_in"]
#         id2_seed_lots_in=result.getlist("#liste_sl_in")
#         if id_seed_lots_in=='':
#             seed_lots_in="NULL"
#         else:
#             if(len(id2_seed_lots_in)==2):
#                 sl_in=result["#liste_sl_in"]        
#                 sl_in=sl_in.encode('utf-8')                
#             else:
#                 sl_in=[x.encode('utf-8') for x in (result.getlist("#liste_sl_in"))]
#                 if '' in sl_in:
#                   sl_in.remove('')
#                 sl_in=tuple(sl_in)
#             seed_lots_in="{0}".format(str(sl_in))
#         
#         id_seed_lots_out=result["#liste_sl_out"]
#         id2_seed_lots_out=result.getlist("#liste_sl_out")
#         if id_seed_lots_out=='':
#             seed_lots_out="NULL"
#         else:
#             if(len(id2_seed_lots_out)==2):
#                 sl_out=result["#liste_sl_out"]        
#                 sl_out=sl_out.encode('utf-8')                
#             else:
#                 sl_out=[x.encode('utf-8') for x in (result.getlist("#liste_sl_out"))]
#                 if '' in sl_out:
#                   sl_out.remove('')
#                 sl_out=tuple(sl_out)
#             seed_lots_out="{0}".format(str(sl_out))
#             
#         id_relation_in=result["relation_in"]
#         if id_relation_in=="":
#             relation_in="NULL"
#         else:
#             relation_in=result["relation_in"]
#             
#         id_repro_type_in=result["repro_type_in"]
#         if id_repro_type_in=="":
#             repro_type_in="NULL"
#         else:
#             repro_type_in=[str(Reproduction_method.objects.get(id=x)) for x in id_repro_type_in]
#             repro_type_in="{0}".format(repro_type_in[0])
#     
#         id_variables_in=result["#liste_var_in"]
#         id2_variables_in=result.getlist("#liste_var_in")
#         if id_variables_in=='':
#             variables_in="NULL"
#         else:
#             if(len(id2_variables_in)==2):
#                 var_in=result["#liste_var_in"]        
#             else:
#                 var_in=[x.encode('utf-8') for x in (result.getlist("#liste_var_in"))]
#                 if '' in var_in:
#                   var_in.remove('')
#                 var_in=tuple(var_in)
#             variables_in="{0}".format(str(var_in))
#             
#                           
#         id_data_type=result["data_type"]
#         if id_data_type=="":
#             data_type="NULL"
#         else:
#             data_type=result["data_type"]
#         
#         family='use_historic'
#              
#     elif family_analysis == "Get the network" : 
#         query_type=result["query_type"]
#         filter_on=result["filter_on"]
#         fill_diff_gap=result["fill_diff_gap"]
#         network_info=result["network_info"]
#         mdist=result["mdist"]
#         germplasm_in=result["germplasm_in"]
#         germplasm_out=result["germplasm_out"]
#         germplasm_type_in=result["germplasm_type_in"]
#         germplasm_type_out=result["germplasm_type_out"]
#         year_in=result["year_in"]
#         year_out=result["year_out"]
#         project_in=result["project_in"]
#         project_out=result["project_out"]
#         person_in=result["person_in"]
#         person_out=result["person_out"]
#         seed_lots_in=result["seed_lots_in"]
#         seed_lots_out=result["seed_lots_out"]
#         relation_in=result["relation_in"]
#         repro_type_in=result["repro_type_in"]
#         variables_in=result["variables_in"]
#         data_type=result["data_type"]
#         family='use_historic'
#         
#     elif family_analysis == "historic":
#         import UserDict
#         analysis_name=result["get_data"]
#         dico_analysis=pickle.load(open(settings.MEDIA_ROOT_ANALYSIS+username+analysis_name+"/"+analysis_name+".p"))
#         origine=dico_analysis["origine"]
#         result_var2=dico_analysis['result_var']
#         result_var2=UserDict.UserDict(eval(result_var2))
#         
#         query_type= result_var2["query_type"].encode("utf-8")        
#         filter_on=result_var2["filter_on"].encode("utf-8")
#         fill_diff_gap=result_var2["fill_diff_gap"].encode("utf-8")
#         network_info=result_var2["network_info"].encode("utf-8")
#         mdist=result_var2["mdist"].encode("utf-8")
#         
#         id_germplasm_in=result_var2["germplasm_in"]
#         if(id_germplasm_in=="NULL" or len(id_germplasm_in)==1):
#             germ_in=result_var2["germplasm_in"]        
#             germ_in=germ_in.encode('utf-8')
#         else:
#             id_germplasm_in=list(id_germplasm_in)
#             longueur=len(id_germplasm_in)
#             j=0
#             for i in range(0,longueur):
#                 if id_germplasm_in[j]=="(" or id_germplasm_in[j]==")" or id_germplasm_in[j]=="'":
#                     del(id_germplasm_in[j])
#                     longueur=longueur-1
#                 else:
#                     j=j+1
#             id_germplasm_out=''.join(id_germplasm_in)
#             id_germplasm_in=id_germplasm_out.split(',')
#                     
#             for x in id_germplasm_in:
#                     if (x==""):
#                         id_germplasm_in.remove("")
#             germ_in=tuple(id_germplasm_in)
#         germplasm_in="{0}".format(str(germ_in))  
#                                       
#         id_germplasm_out=result_var2["germplasm_out"]
#         if(id_germplasm_out=="NULL" or len(id_germplasm_out)==1):
#             germ_out=result_var2["germplasm_out"]        
#             germ_out=germ_out.encode('utf-8')                
#         else:
#             id_germplasm_out=list(id_germplasm_out)
#             longueur=len(id_germplasm_out)
#             j=0
#             for i in range(0,longueur):
#                 if id_germplasm_out[j]=="(" or id_germplasm_out[j]==")" or id_germplasm_out[j]=="'":
#                     del(id_germplasm_out[j])
#                     longueur=longueur-1
#                 else:
#                     j=j+1
#             id_germplasm_out=''.join(id_germplasm_out)
#             id_germplasm_out=id_germplasm_out.split(',')
#                     
#             for x in id_germplasm_out:
#                     if (x==""):
#                         id_germplasm_out.remove("")
#             germ_out=tuple(id_germplasm_out)
#         germplasm_out="{0}".format(str(germ_out))  
#             
#         id_germplasm_type_in=result_var2["germplasm_type_in"]
#         if(id_germplasm_type_in=="NULL" or len(id_germplasm_type_in)==1):
#             germ_type_in=result_var2["germplasm_type_in"]
#             if len(id_germplasm_type_in)==1:
#               germ_type_in=str(Project.objects.get(id=germ_type_in))
#         else:
#             id_germplasm_type_in=list(id_germplasm_type_in)
#             longueur=len(id_germplasm_type_in)
#             j=0
#             for i in range(0,longueur):
#                 if id_germplasm_type_in[j]=="(" or id_germplasm_type_in[j]==")" or id_germplasm_type_in[j]=="'":
#                     del(id_germplasm_type_in[j])
#                     longueur=longueur-1
#                 else:
#                     j=j+1
#             id_germplasm_type_in=''.join(id_germplasm_type_in)
#             id_germplasm_type_in=id_germplasm_type_in.split(',')
#                     
#             for x in id_germplasm_type_in:
#                     if (x==""):
#                         id_germplasm_type_in.remove("")
#             germ_type_in=tuple(id_germplasm_type_in)
#         germplasm_type_in="{0}".format(str(germ_type_in))  
#             
#         id_germplasm_type_out=result_var2["germplasm_type_out"]
#         if(id_germplasm_type_out=="NULL" or len(id_germplasm_type_out)==1):
#             germ_type_out=result_var2["germplasm_type_out"]
#             if len(id_germplasm_type_out)==1:
#               germ_type_out=str(Project.objects.get(id=germ_type_out))
#         else:
#             id_germplasm_type_out=list(id_germplasm_type_out)
#             longueur=len(id_germplasm_type_out)
#             j=0
#             for i in range(0,longueur):
#                 if id_germplasm_type_out[j]=="(" or id_germplasm_type_out[j]==")" or id_germplasm_type_out[j]=="'":
#                     del(id_germplasm_type_out[j])
#                     longueur=longueur-1
#                 else:
#                     j=j+1
#             id_germplasm_type_out=''.join(id_germplasm_type_out)
#             id_germplasm_type_out=id_germplasm_type_out.split(',')
#                     
#             for x in id_germplasm_type_out:
#                     if (x==""):
#                         id_germplasm_type_out.remove("")
#             germ_type_out=tuple(id_germplasm_type_out)
#         germplasm_type_out="{0}".format(str(germ_type_out))
#             
#         id_year_in=result_var2["year_in"]
#         if(id_year_in=="NULL" or len(id_year_in)==1):
#             ye_in=result_var2["year_in"]        
#         else:
#             id_year_in=list(id_year_in)
#             longueur=len(id_year_in)
#             j=0
#             for i in range(0,longueur):
#                 if id_year_in[j]=="(" or id_year_in[j]==")" or id_year_in[j]=="'":
#                     del(id_year_in[j])
#                     longueur=longueur-1
#                 else:
#                     j=j+1
#             id_year_in=''.join(id_year_in)
#             id_year_in=id_year_in.split(',')
#                     
#             for x in id_year_in:
#                     if (x==""):
#                         id_year_in.remove("")
#             ye_in=tuple(id_year_in)
#         year_in="{0}".format(str(ye_in))
#             
#         id_year_out=result_var2["year_out"]
#         if(id_year_out=="NULL" or len(id_year_out)==1):
#             ye_out=result_var2["year_out"]        
#         else:
#             id_year_out=list(id_year_out)
#             longueur=len(id_year_out)
#             j=0
#             for i in range(0,longueur):
#                 if id_year_out[j]=="(" or id_year_out[j]==")" or id_year_out[j]=="'":
#                     del(id_year_out[j])
#                     longueur=longueur-1
#                 else:
#                     j=j+1
#             id_year_out=''.join(id_year_out)
#             id_year_out=id_year_out.split(',')
#                     
#             for x in id_year_out:
#                     if (x==""):
#                         id_year_out.remove("")
#             ye_out=tuple(id_year_out)
#         year_out="{0}".format(str(ye_out))
#             
#         id_project_in=result_var2["project_in"]
#         if(id_project_in=="NULL" or len(id_project_in)==1):
#             proj_in=result_var2["project_in"]        
#             if len(id_project_in)==1:
#               proj_in=str(Project.objects.get(id=proj_in))
#         else:
#             id_project_in=list(id_project_in)
#             longueur=len(id_project_in)
#             j=0
#             for i in range(0,longueur):
#                 if id_project_in[j]=="(" or id_project_in[j]==")" or id_project_in[j]=="'":
#                     del(id_project_in[j])
#                     longueur=longueur-1
#                 else:
#                     j=j+1
#             id_project_in2=''.join(id_project_in)
#             id_project_in=id_project_in2.split(',')
#                     
#             for x in id_project_in:
#                     if (x==""):
#                         id_project_in.remove("")
#             proj_in=tuple(id_project_in)
#         project_in="{0}".format(str(proj_in))
#           
#         id_project_out=result_var2["project_out"]
#         if(id_project_out=="NULL" or len(id_project_out)==1):
#             proj_out=result_var2["project_out"]        
#             if len(id_project_out)==1:
#               proj_in=str(Project.objects.get(id=proj_out))
#         else:
#             id_project_out=list(id_project_out)
#             longueur=len(id_project_out)
#             j=0
#             for i in range(0,longueur):
#                 if id_project_out[j]=="(" or id_project_out[j]==")" or id_project_out[j]=="'":
#                     del(id_project_out[j])
#                     longueur=longueur-1
#                 else:
#                     j=j+1
#             id_project_out=''.join(id_project_out)
#             id_project_out=id_project_out.split(',')
#                     
#             for x in id_project_out:
#                     if (x==""):
#                         id_project_out.remove("")
#             proj_out=tuple(id_project_out)
#         project_out="{0}".format(str(proj_out))
#                 
#         id_person_in=result_var2["person_in"]
#         if(id_person_in=="NULL" or len(id_person_in)==1):
#             pers_in=result_var2["person_in"]        
#         else:
#             id_person_in=list(id_person_in)
#             longueur=len(id_person_in)
#             j=0
#             for i in range(0,longueur):
#                 if id_person_in[j]=="(" or id_person_in[j]==")" or id_person_in[j]=="'":
#                     del(id_person_in[j])
#                     longueur=longueur-1
#                 else:
#                     j=j+1
#             id_person_in=''.join(id_person_in)
#             id_person_in=id_person_in.split(',')
#                     
#             for x in id_person_in:
#                     if (x==""):
#                         id_person_in.remove("")
#             pers_in=tuple(id_person_in)
#         person_in="{0}".format(str(pers_in))
#         
#         id_person_out=result_var2["person_out"]
#         if(id_person_out=="NULL" or len(id_person_out)==1):
#             pers_out=result_var2["person_out"]        
#         else:
# <<<<<<< .working
#             id_person_out=list(id_person_out)
#             longueur=len(id_person_out)
#             j=0
#             for i in range(0,longueur):
#                 if id_person_out[j]=="(" or id_person_out[j]==")" or id_person_out[j]=="'":
#                     del(id_person_out[j])
#                     longueur=longueur-1
#                 else:
#                     j=j+1
#             id_person_out=''.join(id_person_out)
#             id_person_out=id_person_out.split(',')
#                     
#             for x in id_person_out:
#                     if (x==""):
#                         id_person_out.remove("")
#             pers_out=tuple(id_person_out)
#         person_out="{0}".format(str(pers_out))
#                 
#         id_seed_lots_in=result_var2["seed_lots_in"]
#         if(id_seed_lots_in=="NULL" or len(id_seed_lots_in)==1):
#             sl_in=result_var2["seed_lots_in"]        
#             sl_in=sl_in.encode('utf-8')                
# ||||||| .merge-left.r674
#             SL_name = str(Seed_lot.objects.get(id=id_SL_name))
# 
#     if result["family_analysis"] == "Get the data" :
#         id_germplasm_in_a = result["germplasm_in_a"]
#         if id_germplasm_in_a == "":
#             germplasm_in_a = "NULL"
# =======
#             SL_name = str(Seedlot.objects.get(id=id_SL_name))
# 
#     if result["family_analysis"] == "Get the data" :
#         id_germplasm_in_a = result["germplasm_in_a"]
#         if id_germplasm_in_a == "":
#             germplasm_in_a = "NULL"
# >>>>>>> .merge-right.r679
#         else:
#             id_seed_lots_in=list(id_seed_lots_in)
#             longueur=len(id_seed_lots_in)
#             j=0
#             for i in range(0,longueur):
#                 if id_seed_lots_in[j]=="(" or id_seed_lots_in[j]==")" or id_seed_lots_in[j]=="'":
#                     del(id_seed_lots_in[j])
#                     longueur=longueur-1
#                 else:
#                     j=j+1
#             id_seed_lots_in=''.join(id_seed_lots_in)
#             id_seed_lots_in=id_seed_lots_in.split(',')
#                     
#             for x in id_seed_lots_in:
#                     if (x==""):
#                         id_seed_lots_in.remove("")
#             sl_in=tuple(id_seed_lots_in)
#         seed_lots_in="{0}".format(str(sl_in))
#         
#         id_seed_lots_out=result_var2["seed_lots_out"]
#         if(id_seed_lots_out=="NULL" or len(id_seed_lots_out)==1):
#             sl_out=result_var2["seed_lots_out"]        
#             sl_out=sl_out.encode('utf-8')                
#         else:
#             id_seed_lots_out=list(id_seed_lots_out)
#             longueur=len(id_seed_lots_out)
#             j=0
#             for i in range(0,longueur):
#                 if id_seed_lots_out[j]=="(" or id_seed_lots_out[j]==")" or id_seed_lots_out[j]=="'":
#                     del(id_seed_lots_out[j])
#                     longueur=longueur-1
#                 else:
#                     j=j+1
#             id_seed_lots_out=''.join(id_seed_lots_out)
#             id_seed_lots_out=id_seed_lots_out.split(',')
#                     
#             for x in id_seed_lots_out:
#                     if (x==""):
#                         id_seed_lots_out.remove("")
#             sl_out=tuple(id_seed_lots_out)
#         seed_lots_out="{0}".format(str(sl_out))
#             
#         relation_in=result_var2["relation_in"]
#             
#         id_repro_type_in=result_var2["repro_type_in"]
#         if(id_repro_type_in=="NULL" or len(id_repro_type_in)==1):
#             repro_type_in=result_var2["seed_lots_in"]        
#             repro_type_in=repro_type_in.encode('utf-8')        
#             if len(id_repro_type_in)==1:
#               repro_type_in=str(Project.objects.get(id=repro_type_in))
#         else:
#             repro_type_in=[str(Reproduction_method.objects.get(id=x)) for x in id_repro_type_in]
#             repro_type_in="{0}".format(repro_type_in[0])
#     
#         id_variables_in=result_var2["variables_in"]
#         if(id_variables_in=="NULL" or len(id_variables_in)==1):
#             var_in=result_var2["variables_in"]        
#         else:
#             id_variables_in=list(id_variables_in)
#             longueur=len(id_variables_in)
#             j=0
#             for i in range(0,longueur):
#                 if id_variables_in[j]=="(" or id_variables_in[j]==")" or id_variables_in[j]=="'":
#                     del(id_variables_in[j])
#                     longueur=longueur-1
#                 else:
#                     j=j+1
#             id_variables_in=''.join(id_variables_in)
#             id_variables_in=id_variables_in.split(',')
#                     
#             for x in id_variables_in:
#                     if (x==""):
#                         id_variables_in.remove("")
#             var_in=tuple(id_variables_in)
#         variables_in="{0}".format(str(var_in))
#         
#         data_type=result_var2["data_type"]
#         result_var2["family_analysis"]=family_analysis
#         result_var2["analysis_name"]=analysis_name
#         result_var2["origine"]=origine
#         result=result_var2
#         family='historic_saved'
# 
#     picture = settings.MEDIA_ROOT_ANALYSIS+username+'firstRun.pdf'
#     Rdata = settings.MEDIA_ROOT_ANALYSIS+username+'firstRun1.RData'
#     if os.path.exists(picture) :
#         os.unlink(picture)
#     if os.path.exists(Rdata) :
#         os.unlink(Rdata)
#     
#     runThread = firstRunThread(result)
#     runThread.run()
#     formNetwork= GgplotNetworkForm()
#     formDataPlot=GgplotDataForm()
#     formDataTable=GgplotDataTable()
#     result={}
#     result["query_type"]=query_type
#     result["filter_on"]=filter_on
#     result["fill_diff_gap"]=fill_diff_gap
#     result["network_info"]=network_info
#     result["mdist"]=mdist
#     result["germplasm_in"]=germplasm_in
#     result["germplasm_out"]=germplasm_out
#     result["germplasm_type_in"]=germplasm_type_in
#     result["germplasm_type_out"]=germplasm_type_out
#     result["year_in"]=year_in
#     result["year_out"]=year_out
#     result["project_in"]=project_in
#     result["project_out"]=project_out
#     result["person_in"]=person_in
#     result["person_out"]=person_out
#     result["seed_lots_in"]=seed_lots_in
#     result["seed_lots_out"]=seed_lots_out
#     result["relation_in"]=relation_in
#     result["repro_type_in"]=repro_type_in
#     result["variables_in"]=variables_in
#     result["data_type"]=data_type
#     result_var=result.items()
#     result_var.sort()
#     SaveForm=saveForm()
#     
#     compt=0
#     if query_type == "network":
#         compt=1       
#     return render(request, 'analysis/first_run.html',{'year_in':year_in,'family':family,'formDataTable':formDataTable,'formNetwork': formNetwork,"saveForm":SaveForm,"query_type":query_type,
#                                                       "filter_on":filter_on,"fill_diff_gap":fill_diff_gap,"network_info":network_info,"mdist":mdist,"germplasm_in":germplasm_in,"germplasm_out":germplasm_out,
#                                                       "germplasm_type_in":germplasm_type_in,"germplasm_type_out":germplasm_type_out,"year_in":year_in,"year_out":year_out,"project_in":project_in,"project_out":project_out,
#                                                       "person_in":person_in,"person_out":person_out,"seed_lots_in":seed_lots_in,"seed_lots_out":seed_lots_out,"relation_in":relation_in,"repro_type_in":repro_type_in,"variables_in":variables_in,
#                                                       "data_type":data_type,"result_var":result_var,"compt":compt,"formDataPlot":formDataPlot,"family_analysis":family_analysis})
#           
#         #return render(request, 'analysis/first_run.html',{'dico_analysis':dico_analysis,"result_var2":result_var2})  
#             
# def save_analysis(request):
#     import UserDict
#     status=request.POST['statut']
#     analysis_name=request.POST['analysis_name']
#     comment=request.POST["comment"]
#     result_var=request.POST["result_var"]
#     analysis_type=request.POST['analysis_type']
#     result={"status":status,"analysis_name":analysis_name,"comment":comment,"result_var":result_var,'analysis_type':analysis_type}  
#     path=settings.MEDIA_ROOT_ANALYSIS+username+analysis_name+"/"
#     if os.path.isdir(path)==False:
#       os.mkdir(path)
#     if analysis_type=="Get the data":
#       analysis_run="firstRun"
#       robjects.r("origine='{0}'".format(analysis_name))
#     elif analysis_type=="Get the network" or analysis_type=="Get the plot" or analysis_type=="Get the table":
#       analysis_run="secondRun"
#     
#     fichier=open(settings.MEDIA_ROOT_ANALYSIS+username+analysis_run+'.log','r')
#     read=fichier.read()
#     fichier.close()
#     fichier_recp=open(path+analysis_name+'.log',"w")
#     fichier_recp.write(read)
#     fichier_recp.close()
#    
#     
#     robjects.r("MEDIA_ROOT_ANALYSIS='{0}'".format(settings.MEDIA_ROOT_ANALYSIS))
#     robjects.r("username='{0}'".format(username))
#     
#     robjects.r("analysis_run='{0}'".format(analysis_run))   
#     robjects.r('''load(paste(MEDIA_ROOT_ANALYSIS,username,analysis_run,".RData",sep=""))''')
#     robjects.r("analysis_name='{0}'".format(analysis_name))
#     robjects.r("analysis_run='{0}'".format(analysis_run))      
#     robjects.r("saveFile=paste(MEDIA_ROOT_ANALYSIS,username,analysis_name,'/',analysis_name,'.RData',sep='')")
#     robjects.r('''
#             library(shinemas2R) 
#             resave=paste(MEDIA_ROOT_ANALYSIS,username,analysis_run,".RData",sep="")
#             if (analysis_run=="firstRun"){
#               if(exists("liste_elements_pdf")){
#                 rm(liste_elements_pdf)
#               }
#               save(MEDIA_ROOT_ANALYSIS,RdataFile,data_type,fill_diff_gap,filter_on,
#               germplasm_in,germplasm_out,germplasm_type_in,germplasm_type_out,get.data,i,
#               j,mdist,network_info,person_in,person_out,project_in,project_out,query_type,
#               relation_in,repro_type_in,saveFile,seed_lots_in,seed_lots_out,toto,username,
#               variables_in,vecteur,year_in,year_out,file=saveFile)
#               save(MEDIA_ROOT_ANALYSIS,RdataFile,data_type,fill_diff_gap,filter_on,
#               germplasm_in,germplasm_out,germplasm_type_in,germplasm_type_out,get.data,i,
#               j,mdist,network_info,person_in,person_out,project_in,project_out,query_type,
#               relation_in,repro_type_in,saveFile,seed_lots_in,seed_lots_out,toto,username,
#               variables_in,vecteur,year_in,year_out,file=resave)
#             }else if (analysis_run=="secondRun"){
#               if(!exists("origine")){
#                   origine="group_without_name"
#               }
#               if(exists("liste_elements_pdf")){
#                 rm(liste_elements_pdf)
#               }
#               if(exists("figures")){
#                 rm(figures)
#               }
#               if(exists("pers")){
#                 rm(pers)
#               }
#               if (exists("Network")){
#                 assign(analysis_name,Network)
#                 rm(Network)
#                 save(list=ls(), file=saveFile)
#               }else{
#                 assign(analysis_name,DataPlot)
#                 save(list=ls(), file=saveFile)
#               }
#             }    
#     ''')
#     origine=robjects.r["origine"]
#     origine=list(origine)
#     result["origine"]=origine[0]
#     
#     if analysis_type=="Get the network" or analysis_type=="Get the plot" or analysis_type=="Get the table":
#         path_origine=settings.MEDIA_ROOT_ANALYSIS+username+result["origine"]+"/"+result["origine"]+".p"
#         if os.path.isfile(path_origine)==False:
#           group_name_origine=str(origine)[2:-2]
#         else:
#           origine_data=pickle.load(open(path_origine))
#           group_name_origine=origine_data["group_name"]
#         result["group_name"]=group_name_origine
#     else:
#         result["group_name"]=request.POST['group_name']
#         
#     group_name=result["group_name"]  
#     pickle.dump(result,open((path+analysis_name+".p"),"wb"))
#     #dico_saved=pickle.load(open(settings.MEDIA_ROOT_ANALYSIS+username+analysis_name+".p","rb")))
#     #result_var=dico_saved['result_var']
#     
#     #result_var=UserDict.UserDict(eval(result_var)) pour sortir la "value" de la "key" result_var sous forme de dictionnaire
#     return HttpResponse("<p style='font-size:14pt;'>ANALYSIS SAVED </p><br><br><u>status:</u> {0} <br> <u>analysis_name:</u> {1} <br><u>group_name:</u> {2} <br><u>comment:</u> {3}".format(status,analysis_name,group_name,origine))
# 
# #------------------------------------------------------------------------------------------------------------------------
# #------------------------------------------------ SECOND --------------------------------------------------------------------------
# #------------------------------------------------------------------------------------------------------------------------
# class secondRunThread(threading.Thread):
#     def __init__(self,listArgs):
#         self.stdout = None
#         self.stderr = None
#         self.listArgs=listArgs
#         threading.Thread.__init__(self)
# 
#     def run(self):
#         listArgs=self.listArgs
#         filename='secondRun.R'
#         # Path complet du fichier .r
#         full_filename=settings.MEDIA_ROOT_ANALYSIS+username+'scriptR/'+filename
#         name=settings.MEDIA_ROOT_ANALYSIS+username+'secondRun.log'
#         log = open(name, "w")
#         
#         #---------------------------------- Liste des arguments pour la fonction GxE --------------------------
#         family_analysis2=listArgs["family_analysis2"]
#                 
#         if family_analysis2=="Get the network":          
#             id_ggplot_type=listArgs["ggplot_type"]
#             if id_ggplot_type=="":
#                 ggplot_type="NULL"
#             else:
#                 ggplot_type=listArgs["ggplot_type"]
#             
#             correlated_group="NULL"
#             ggplot_on="son"
#             vec_variables="NULL"
#             labels_on="son"
#             merge_g_and_s=listArgs["merge_g_and_s"]
#             organise_sl=listArgs["organise_sl"]
#             
#             vertex_size=listArgs["vertex_size"]
#             vertex_color=listArgs["vertex_color"]
#             ggplot_display=listArgs["ggplot_display"]
#             
#             id_x_axis=listArgs["x_axis"]
#             if id_x_axis=="":
#                 x_axis="NULL"
#             else:
#                 x_axis=listArgs["x_axis"]
#                 
#             id_in_col=listArgs["in_col"]
#             if id_in_col=="":
#                 in_col="NULL"
#             else:
#                 in_col=listArgs["in_col"]
#                     
#             id_param_x_axis=listArgs["param_x_axis"]
#             if id_param_x_axis=="":
#                 param_x_axis="NULL"
#             else:
#                 param_x_axis=listArgs["param_x_axis"]
#             
#             id_param_in_col=listArgs["param_in_col"]
#             if id_param_in_col=="":
#                 param_in_col="NULL"
#             else:
#                 param_in_col=listArgs["param_in_col"]
#             
#             location_map=listArgs["location_map"]
#             pie_size=listArgs["pie_size"]
#             
#             id_hide_labels_parts=listArgs["hide_labels_parts"]
#             if id_hide_labels_parts=="":
#                 hide_labels_parts="NULL"
#             else:
#                 hide_labels_parts=listArgs["hide_labels_parts"]
#                             
#             labels_size=listArgs["labels_size"]
#             #display_labels_sex=listArgs["display_labels_sex"]
#             labels_generation=listArgs["labels_generation"]
#             
#             log = open(name, "w")      
#             proc = subprocess.Popen(["Rscript", full_filename, family_analysis2, correlated_group, merge_g_and_s,ggplot_type, ggplot_display, ggplot_on, x_axis, in_col, vec_variables, param_x_axis, param_in_col, vertex_size, vertex_color,organise_sl, labels_on, hide_labels_parts,labels_generation,labels_size,location_map, pie_size],shell=False, stdout=log,stderr=log,universal_newlines=True)        
#             log.close()
#             
#         else:
#             if family_analysis2=="Get the plot":
#                 id_ggplot_type=listArgs["data_ggplot_type"]
#                 if id_ggplot_type=="":
#                     ggplot_type="NULL"
#                 else:
#                     ggplot_type=listArgs["data_ggplot_type"]
#                 
#                 merge_g_and_s="FALSE"
#                 organise_sl=listArgs["data_organise_sl"]
#                 
#                 id_correlated_group=listArgs["data_correlated_group"]
#                 if id_correlated_group=="":
#                     correlated_group="NULL"
#                 else:
#                     correlated_group=listArgs["data_correlated_group"]
#                 
#                 vec_var=[x.encode('utf-8') for x in (listArgs.getlist("data_vec_variables"))]
#                 vec_var=tuple(vec_var)
#                 vec_variables="{0}".format(str(vec_var))
#                 
#                 ggplot_on=listArgs["data_ggplot_on"]
#                 labels_on=listArgs["data_labels_on"]
#                 
#                 vertex_size=listArgs["data_vertex_size"]
#                 vertex_color=listArgs["data_vertex_color"]
#                 ggplot_display="NULL"
#                 
#                 id_x_axis=listArgs["data_x_axis"]
#                 if id_x_axis=="":
#                     x_axis="NULL"
#                 else:
#                     x_axis=listArgs["data_x_axis"]
#                     
#                 id_in_col=listArgs["data_in_col"]
#                 if id_in_col=="":
#                     in_col="NULL"
#                 else:
#                     in_col=listArgs["data_in_col"]
#                         
#                 id_param_x_axis=listArgs["data_param_x_axis"]
#                 if id_param_x_axis=="":
#                     param_x_axis="NULL"
#                 else:
#                     param_x_axis=listArgs["data_param_x_axis"]
#                 
#                 id_param_in_col=listArgs["data_param_in_col"]
#                 if id_param_in_col=="":
#                     param_in_col="NULL"
#                 else:
#                     param_in_col=listArgs["data_param_in_col"]
#                 
#                 location_map=listArgs["data_location_map"]
#                 pie_size=listArgs["data_pie_size"]
#                 
#                 id_hide_labels_parts=listArgs["data_hide_labels_parts"]
#                 if id_hide_labels_parts=="":
#                     hide_labels_parts="NULL"
#                 else:
#                     hide_labels_parts=listArgs["data_hide_labels_parts"]
#                                 
#                 labels_size=listArgs["data_labels_size"]
#                 #display_labels_sex="TRUE"
#                 labels_generation="TRUE"
#                    
#                 log = open(name, "w")      
#                 proc = subprocess.Popen(["Rscript", full_filename, family_analysis2, correlated_group, merge_g_and_s,ggplot_type, ggplot_display, ggplot_on, x_axis, in_col, vec_variables, param_x_axis, param_in_col, vertex_size, vertex_color,organise_sl, labels_on, hide_labels_parts,labels_generation,labels_size,location_map, pie_size],shell=False, stdout=log,stderr=log,universal_newlines=True)        
#                 log.close()
#                 
#             else:
#                 if family_analysis2=="Get the table":
#                     id_correlated_group=listArgs["data_correlated_group"]
#                     if id_correlated_group=="":
#                         correlated_group="NULL"
#                     else:
#                         correlated_group=listArgs["data_correlated_group"]
#                     
#                     vec_var=[x.encode('utf-8') for x in (listArgs.getlist("data_vec_variables"))]
#                     vec_var=tuple(vec_var)
#                     vec_variables="{0}".format(str(vec_var))
#                         
#                     merge_g_and_s=listArgs["merge_g_and_s"]
#                     
#                     id_data_table_type=listArgs["data_table_type"]
#                     if id_data_table_type=="":
#                         data_table_type="NULL"
#                     else:
#                         data_table_type=listArgs["data_table_type"]
# 
#                     id_data_table_on=listArgs["data_table_on"]
#                     if id_data_table_on=="":
#                         data_table_on="NULL"
#                     else:
#                         data_table_on=listArgs["data_table_on"]
#                 
#                     table_nb_row=listArgs["table_nb_row"]
#                     table_nb_col=listArgs["table_nb_col"]
#                     table_nb_duplicated_rows=listArgs["table_nb_duplicated_rows"]
#                     
#                     id_table_col_to_display=listArgs["table_col_to_display"]
#                     id2_table_col_to_display=listArgs.getlist("table_col_to_display")
#                     if id_table_col_to_display=="":
#                         table_col_to_display="NULL"
#                     else:
#                         if(len(id2_table_col_to_display)==1):
#                             table_col_to_display=listArgs["table_col_to_display"]
#                         else:
#                             table_col_to_display=listArgs.getlist("table_col_to_display")
#                             table_col_to_display=tuple(table_col_to_display)
#                         table_col_to_display="{0}".format(str(table_col_to_display))
#                     
#                     table_invert_row_col=listArgs["table_invert_row_col"]
#                       
#                     log = open(name, "w")      
#                     proc = subprocess.Popen(["Rscript", full_filename, family_analysis2, correlated_group, vec_variables, merge_g_and_s, data_table_type, data_table_on, table_nb_row, table_nb_col, table_nb_duplicated_rows,
#                                table_col_to_display, table_invert_row_col],shell=False, stdout=log,stderr=log,universal_newlines=True)        
#                     log.close()  
# 
# #-------------------------------------------------------------------------------------------------------------------------------------        
# #---------------------------- SECOND ANALYSE VIEW ----------------------------------------------------------------------------------
# @login_required
# def second_run(request):
#     from analysis.forms import GetPdfForm
#     ##recuperation des variables de getData pour les reutiliser
# 
#     liste_fichiers_templates=os.listdir(settings.MEDIA_ROOT_TEMPLATES+username)
#     liste_templates=[]
#     for i in liste_fichiers_templates:
#         if i.endswith(".p"):
#             liste_templates.append(pickle.load(open(settings.MEDIA_ROOT_TEMPLATES+username+i,"rb")))
# 
#     template_dico_keys=[]
#     template_dico_values=[]
#     for v in range(0,len(liste_templates),1):
#         for key, value in liste_templates[v].items():
#             template_dico_keys.append(key)
#             template_dico_values.append(value)
#     template_dico_keys=[x.encode('utf-8') for x in template_dico_keys]
#     v=0
#     liste_template_dico=[]
#     for i in template_dico_keys:
#         liste_template_dico.append([i,liste_templates[v][i]])
#         v=v+1
#         
#       
#     family_analysis2=request.POST["family_analysis2"]
#     if family_analysis2 == "Get the network" or family_analysis2=="Get the plot":
#         query_type=request.POST["query_type"]
#         filter_on=request.POST["filter_on"]
#         fill_diff_gap=request.POST["fill_diff_gap"]
#         network_info=request.POST["network_info"]
#         mdist=request.POST["mdist"]
#         germplasm_in=request.POST["germplasm_in"]
#         germplasm_out=request.POST["germplasm_out"]
#         germplasm_type_in=request.POST["germplasm_type_in"]
#         germplasm_type_out=request.POST["germplasm_type_out"]
#         year_in=request.POST["year_in"]
#         year_out=request.POST["year_out"]
#         project_in=request.POST["project_in"]
#         project_out=request.POST["project_out"]
#         person_in=request.POST["person_in"]
#         person_out=request.POST["person_out"]
#         seed_lots_in=request.POST["seed_lots_in"]
#         seed_lots_out=request.POST["seed_lots_out"]
#         relation_in=request.POST["relation_in"]
#         repro_type_in=request.POST["repro_type_in"]
#         variables_in=request.POST["variables_in"]
#         data_type=request.POST["data_type"]
#         
#         if family_analysis2=="Get the network":      
#             id_ggplot_type=request.POST["ggplot_type"]
#             if id_ggplot_type=="":
#                 ggplot_type="NULL"
#             else:
#                 ggplot_type=request.POST["ggplot_type"]
#             
#             correlated_group="NULL"
#             ggplot_on="son"
#             vec_variables="NULL"
#             labels_on="son"
#             merge_g_and_s=request.POST["merge_g_and_s"]
#             organise_sl=request.POST["organise_sl"]
#             
#             vertex_size=request.POST["vertex_size"]
#             vertex_color=request.POST["vertex_color"]
#             ggplot_display=request.POST["ggplot_display"]
#             
#             id_x_axis=request.POST["x_axis"]
#             if id_x_axis=="":
#                 x_axis="NULL"
#             else:
#                 x_axis=request.POST["x_axis"]
#                 
#             id_in_col=request.POST["in_col"]
#             if id_in_col=="":
#                 in_col="NULL"
#             else:
#                 in_col=request.POST["in_col"]
#                     
#             id_param_x_axis=request.POST["param_x_axis"]
#             if id_param_x_axis=="":
#                 param_x_axis="NULL"
#             else:
#                 param_x_axis=request.POST["param_x_axis"]
#             
#             id_param_in_col=request.POST["param_in_col"]
#             if id_param_in_col=="":
#                 param_in_col="NULL"
#             else:
#                 param_in_col=request.POST["param_in_col"]
#             
#             location_map=request.POST["location_map"]
#             pie_size=request.POST["pie_size"]
#             
#             id_hide_labels_parts=request.POST["hide_labels_parts"]
#             if id_hide_labels_parts=="":
#                 hide_labels_parts="NULL"
#             else:
#                 hide_labels_parts=request.POST["hide_labels_parts"]
#                             
#             labels_size=request.POST["labels_size"]
#             #display_labels_sex=request.POST["display_labels_sex"]
#             labels_generation=request.POST["labels_generation"]
#         
#         elif family_analysis2=="Get the plot":        
#                 id_ggplot_type=request.POST["data_ggplot_type"]
#                 if id_ggplot_type=="":
#                     ggplot_type="NULL"
#                 else:
#                     ggplot_type=request.POST["data_ggplot_type"]
#                 
#                 merge_g_and_s="FALSE"
#                 organise_sl=request.POST["data_organise_sl"]
#                 
#                 id_correlated_group=request.POST["data_correlated_group"]
#                 if id_correlated_group=="":
#                     correlated_group="NULL"
#                 else:
#                     correlated_group=request.POST["data_correlated_group"]
#                 
#                 vec_var=[x.encode('utf-8') for x in (request.POST.getlist("data_vec_variables"))]
#                 vec_var=tuple(vec_var)
#                 vec_variables="{0}".format(str(vec_var))
#                 
#                 ggplot_on=request.POST["data_ggplot_on"]
#                 labels_on=request.POST["data_labels_on"]
#                 
#                 vertex_size=request.POST["data_vertex_size"]
#                 vertex_color=request.POST["data_vertex_color"]
#                 ggplot_display="NULL"
#                 
#                 id_x_axis=request.POST["data_x_axis"]
#                 if id_x_axis=="":
#                     x_axis="NULL"
#                 else:
#                     x_axis=request.POST["data_x_axis"]
#                     
#                 id_in_col=request.POST["data_in_col"]
#                 if id_in_col=="":
#                     in_col="NULL"
#                 else:
#                     in_col=request.POST["data_in_col"]
#                         
#                 id_param_x_axis=request.POST["data_param_x_axis"]
#                 if id_param_x_axis=="":
#                     param_x_axis="NULL"
#                 else:
#                     param_x_axis=request.POST["data_param_x_axis"]
#                 
#                 id_param_in_col=request.POST["data_param_in_col"]
#                 if id_param_in_col=="":
#                     param_in_col="NULL"
#                 else:
#                     param_in_col=request.POST["data_param_in_col"]
#                 
#                 location_map=request.POST["data_location_map"]
#                 pie_size=request.POST["data_pie_size"]
#                 
#                 id_hide_labels_parts=request.POST["data_hide_labels_parts"]
#                 if id_hide_labels_parts=="":
#                     hide_labels_parts="NULL"
#                 else:
#                     hide_labels_parts=request.POST["data_hide_labels_parts"]
#                                 
#                 labels_size=request.POST["data_labels_size"]
#                 #display_labels_sex="TRUE"
#                 labels_generation="TRUE"
#                 
#         elif family_analysis2=="Get the table":
#             id_correlated_group=request.POST["data_correlated_group"]
#             if id_correlated_group=="":
#                 correlated_group="NULL"
#             else:
#                 correlated_group=request.POST["data_correlated_group"]
#             
#             vec_var=[x.encode('utf-8') for x in (request.POST.getlist("data_vec_variables"))]
#             vec_var=tuple(vec_var)
#             vec_variables="{0}".format(str(vec_var))
#                 
#             merge_g_and_s=request.POST["merge_g_and_s"]
#             
#             id_data_table_type=request.POST["data_table_type"]
#             if id_data_table_type=="":
#                 data_table_type="NULL"
#             else:
#                 data_table_type=request.POST["data_table_type"]
# 
#             id_data_table_on=request.POST["data_table_on"]
#             if id_data_table_on=="":
#                 data_table_on="NULL"
#             else:
#                 data_table_on=request.POST["data_table_on"]
#         
#             table_nb_row=request.POST["table_nb_row"]
#             table_nb_col=request.POST["table_nb_col"]
#             table_nb_duplicated_rows=request.POST["table_nb_duplicated_rows"]
#             id_table_col_to_display=request.POST["table_col_to_display"]
#             id2_table_col_to_display=request.POST.getlist("table_col_to_display")
#             if id_table_col_to_display=='':
#                 table_col_to_display="NULL"
#             else:
#                 if(len(id2_table_col_to_display)==1):
#                     table_col_to_display=request.POST["table_col_to_display"]
#                 else:
#                     table_col_to_display=request.POST.getlist("table_col_to_display")
#                     table_col_to_display=tuple(table_col_to_display)
#                 table_col_to_display="{0}".format(str(table_col_to_display))
#             
#             table_invert_row_col=request.POST["table_invert_row_col"]
#           
#         result={}
#         result['correlated_group']=correlated_group
#         result['vec_variables']=vec_variables
#         result['ggplot_on']=ggplot_on
#         result['labels_on']=labels_on
#         result['ggplot_type']=ggplot_type
#         result['merge_g_and_s']=merge_g_and_s
#         result['organise_sl']=organise_sl
#         result['vertex_size']=vertex_size
#         result['vertex_color']=vertex_color
#         result['ggplot_display']=ggplot_display
#         result['x_axis']=x_axis
#         result['in_col']=in_col
#         result['param_x_axis']=param_x_axis
#         result['param_in_col']=param_in_col
#         result['location_map']=location_map
#         result['pie_size']=pie_size
#         result['hide_labels_parts']=hide_labels_parts
#         result['labels_size']=labels_size
#         #result['display_labels_sex']=display_labels_sex
#         result['labels_generation']=labels_generation
#         result_var=result.items()
#         result_var.sort()
#         
#         SaveForm=saveForm()
#         formPdf=GetPdfForm()
#       
#         #------ Remove the existing pdf ==> it will not show up if the R function does not generate it afterward 
#         picture = settings.MEDIA_ROOT_ANALYSIS+username+'secondRun.pdf'
#         if os.path.exists(picture) :
#             os.unlink(picture)
#         #------ Start running the second thread
#         runThread = secondRunThread(request.POST)
#         runThread.run()
#         family="use_historic"
#         return render(request, 'analysis/second_run.html',{"result_var":result_var,"liste_template_dico":liste_template_dico,'formPdf':formPdf,"query_type":query_type,"filter_on":filter_on,"fill_diff_gap":fill_diff_gap,"network_info":network_info,"mdist":mdist,"germplasm_in":germplasm_in,"germplasm_out":germplasm_out,"germplasm_type_in":germplasm_type_in,"germplasm_type_out":germplasm_type_out,
#                                                           "year_in":year_in,"year_out":year_out,"project_in":project_in,"project_out":project_out,"person_in":person_in,"person_out":person_out,"seed_lots_in":seed_lots_in,"seed_lots_out":seed_lots_out,"relation_in":relation_in,"repro_type_in":repro_type_in,"variables_in":variables_in,"data_type":data_type,
#                                                           'family_analysis2':family_analysis2,"saveForm":SaveForm,"family":family,
#                                                           'correlated_group':correlated_group,'vec_variables':vec_variables,'ggplot_on':ggplot_on,'labels_on':labels_on,
#                                                           'ggplot_type':ggplot_type,'merge_g_and_s':merge_g_and_s,'organise_sl':organise_sl,'vertex_size':vertex_size,'vertex_color':vertex_color,'ggplot_display':ggplot_display,'x_axis':x_axis,
#                                                           'in_col':in_col,'param_x_axis':param_x_axis,'param_in_col':param_in_col,'location_map':location_map,'pie_size':pie_size,'hide_labels_parts':hide_labels_parts,'labels_size':labels_size,'labels_generation':labels_generation})
#     
#     #elif family_analysis2 == "Get the table":
#         
#     
#     elif family_analysis2 == "get figure":
#         liste_figures_historic=request.POST.getlist("get_figure")
#         if len(liste_figures_historic)==1:
#             liste_figures_historic=request.POST["get_figure"].encode('utf-8')
#         else:
#             liste_figures_historic=[x.encode('utf-8') for x in (request.POST.getlist("get_figure"))]
#         SaveForm=saveForm()
#         formPdf=GetPdfForm()
#         family="historic_saved"
#         return render(request, 'analysis/second_run.html',{"liste_figures_historic":liste_figures_historic,"liste_template_dico":liste_template_dico,'formPdf':formPdf,"saveForm":SaveForm, "family":family})
#                                                          
# #------------------------------------------------------------------------------------------------------------------------
# #------------------------------------------------------------------------------------------------------------------------
# #------------------------------------------------------------------------------------------------------------------------
# class thirdRunThread(threading.Thread):
#     def __init__(self,listArgs):
#         self.stdout = None
#         self.stderr = None
#         self.listArgs=listArgs
#         threading.Thread.__init__(self)
# 
#     def run(self):
#         listArgs=self.listArgs
#         filename='thirdRun.R'
#         # Path complet du fichier .r
#         full_filename=settings.MEDIA_ROOT_ANALYSIS+username+'scriptR/'+filename
#         name=settings.MEDIA_ROOT_ANALYSIS+username+'thirdRun.log'
#         log = open(name, "w")
#         family_analysis = listArgs["family_analysis"]
#         
#         #liste_elements_pdf=[x.encode('utf-8') for x in (listArgs.getlist("element_pdf"))]
#         #for i in range(0,len(liste_elements_pdf)):
#             #if liste_elements_pdf[i]=="":
#                 #liste_elements_pdf[i]="NULL"
#         #liste_elements_pdf=tuple(liste_elements_pdf)
#         #liste_elements_pdf="{0}".format(str(liste_elements_pdf))
#         if family_analysis=="Get the pdf":
#             form_name=listArgs["form_name"]
#             if " " in form_name:
#                 form_name=form_name.replace(" ","_")
#             
#             id_title=listArgs["title"]
#             if id_title=="":
#                 title="NULL"
#             else:
#                 title=listArgs["title"]
#             
#             id_authors=listArgs["authors"]
#             if id_authors=="":
#                 authors="NULL"
#             else:
#                 authors=listArgs["authors"]
#             
#             id_email=listArgs["email"]
#             if id_email=="":
#                 email="NULL"
#             else:
#                 email=listArgs["email"]
#             
#             id_date=listArgs["date"]
#             if id_date=="":
#                 date="NULL"
#             else:
#                 date=listArgs["date"]
#             
#             table_of_contents=listArgs["table_of_contents"]
#                     
#             id_name_chapter=listArgs["name_chapter"]
#             if id_name_chapter=="":
#                 name_chapter="NULL"
#             else:
#                 name_chapter=listArgs["name_chapter"]
#             
#             id_name_section=listArgs["name_section"]
#             if id_name_section=="":
#                 name_section="NULL"
#             else:
#                 name_section=listArgs["name_section"]
#             
#             id_name_subsection=listArgs["name_subsection"]
#             if id_name_subsection=="":
#                 name_subsection="NULL"
#             else:
#                 name_subsection=listArgs["name_subsection"]
#             
#             id_text_to_add=listArgs["text_to_add"]
#             if id_text_to_add=="":
#                 text_to_add="NULL"
#             else:
#                 text_to_add=listArgs["text_to_add"]
#             
#             id_include_pdf=listArgs["include_pdf"]
#             if id_include_pdf=="":
#                 include_pdf="NULL"
#             else:
#                 include_pdf=listArgs["include_pdf"]
#                     
#             id_input_tex=listArgs["input_tex"]
#             if id_input_tex=="":
#                 input_tex="NULL"
#             else:
#                 input_tex=listArgs["input_tex"]
#                 
#             id_figures_to_display=listArgs.getlist("figures_to_display")
#             if len(id_figures_to_display)==1:
#                 figures_to_display=listArgs["figures_to_display"]
#             else:
#                 fig_to_display=[x.encode('utf-8') for x in (listArgs.getlist("figures_to_display"))]
#                 fig_to_display=tuple(fig_to_display)
#                 figures_to_display="{0}".format(str(fig_to_display))
#             
#             id_caption_figure=listArgs["caption_figure"]
#             if id_caption_figure=="":
#                 caption_figure="NULL"
#             else:
#                 caption_figure=listArgs["caption_figure"]
#                     
#             nb_col_matrix=listArgs["nb_col_matrix"]
#             nb_row_matrix=listArgs["nb_row_matrix"]
#             compile_tex=listArgs["compile_tex"]
#             
#         log = open(name, "w")      
#         proc = subprocess.Popen(["Rscript",full_filename,family_analysis,form_name,title,authors,email,date,table_of_contents,name_chapter,name_section,name_subsection,text_to_add,include_pdf,input_tex,caption_figure,nb_col_matrix,nb_row_matrix,compile_tex,figures_to_display],shell=False, stdout=log,stderr=log,universal_newlines=True)        
#         log.close()
# 
# #---------------------------- THIRD ANALYSE VIEW ----------------------------------------------------------------------------------
# @login_required
# def third_run(request):    
#     family_analysis = request.POST["family_analysis"]
#     if family_analysis=="Get the pdf":
#         form_name=request.POST["form_name"]
#         if " " in form_name:
#             form_name=form_name.replace(" ","_")
#         
#         id_title=request.POST["title"]
#         if id_title=="":
#             title="NULL"
#         else:
#             title=request.POST["title"]
#         
#         id_authors=request.POST["authors"]
#         if id_authors=="":
#             authors="NULL"
#         else:
#             authors=request.POST["authors"]
#         
#         id_email=request.POST["email"]
#         if id_email=="":
#             email="NULL"
#         else:
#             email=request.POST["email"]
#         
#         id_date=request.POST["date"]
#         if id_date=="":
#             date="NULL"
#         else:
#             date=request.POST["date"]
#         
#         table_of_contents=request.POST["table_of_contents"]
#                 
#         id_name_chapter=request.POST["name_chapter"]
#         if id_name_chapter=="":
#             name_chapter="NULL"
#         else:
#             name_chapter=request.POST["name_chapter"]
#         
#         id_name_section=request.POST["name_section"]
#         if id_name_section=="":
#             name_section="NULL"
#         else:
#             name_section=request.POST["name_section"]
#         
#         id_name_subsection=request.POST["name_subsection"]
#         if id_name_subsection=="":
#             name_subsection="NULL"
#         else:
#             name_subsection=request.POST["name_subsection"]
#         
#         id_text_to_add=request.POST["text_to_add"]
#         if id_text_to_add=="":
#             text_to_add="NULL"
#         else:
#             text_to_add=request.POST["text_to_add"]
#         
#         id_include_pdf=request.POST["include_pdf"]
#         if id_include_pdf=="":
#             include_pdf="NULL"
#         else:
#             include_pdf=request.POST["include_pdf"]
#                 
#         id_input_tex=request.POST["input_tex"]
#         if id_input_tex=="":
#             input_tex="NULL"
#         else:
#             input_tex=request.POST["input_tex"]
#             
#         id_figures_to_display=request.POST.getlist("figures_to_display")
#         if len(id_figures_to_display)==1:
#             figures_to_display=request.POST["figures_to_display"]
#         else:
#             fig_to_display=[x.encode('utf-8') for x in (request.POST.getlist("figures_to_display"))]
#             fig_to_display=tuple(fig_to_display)
#             figures_to_display="{0}".format(str(fig_to_display))
#         
#         id_caption_figure=request.POST["caption_figure"]
#         if id_caption_figure=="":
#             caption_figure="NULL"
#         else:
#             caption_figure=request.POST["caption_figure"]
#                 
#         nb_col_matrix=request.POST["nb_col_matrix"]
#         nb_row_matrix=request.POST["nb_row_matrix"]
#         compile_tex=request.POST["compile_tex"]
#         
#         result = request.POST
#         runThread = thirdRunThread(result)
#         runThread.run()
#         return render(request, 'analysis/third_run.html',{'family_analysis':family_analysis,'result':result,"form_name":form_name,"authors":authors,"email":email,"date":date,"table_of_contents":table_of_contents,"name_chapter":name_chapter,
#                                                           "name_section":name_section,"name_subsection":name_subsection,"text_to_add":text_to_add,"include_pdf":include_pdf,"input_tex":input_tex,"figures_to_display":figures_to_display,
#                                                           "caption_figure":caption_figure,"nb_col_matrix":nb_col_matrix,"nb_row_matrix":nb_row_matrix,"compile_tex":compile_tex})
# 
# def historic(request):
#     import collections
#     liste_fichiers=os.listdir(settings.MEDIA_ROOT_ANALYSIS+username)
#     liste_fichiers2=[]
#     for i in liste_fichiers:
#         if os.path.isdir(settings.MEDIA_ROOT_ANALYSIS+username+i) and i!="scriptR":
#             liste_fichiers2.append(os.listdir(settings.MEDIA_ROOT_ANALYSIS+username+i))
#             
#     dicos_saved=[]
#     for i in range(0,len(liste_fichiers2)):
#         for j in range(0,len(liste_fichiers2[i])):
#             if liste_fichiers2[i][j].endswith('.p'):
#               dicos_saved.append(pickle.load(open(settings.MEDIA_ROOT_ANALYSIS+username+liste_fichiers2[i][j].replace('.p','/')+liste_fichiers2[i][j])))
#               
#     groups=[]
#     analysis_name=[]
#     for i in range(0,len(dicos_saved)):
#         analysis_name.append(dicos_saved[i]["analysis_name"])
#         groups.append(dicos_saved[i]["group_name"])
#         
# 
#     counter=collections.Counter(groups)
#     counter=counter.items()
#     dico_group={}
#     dico_analysis_name={}
#     dico_origine_saved={}
#     for i in range(0,len(dicos_saved)):
#         for j in analysis_name:
#             if dicos_saved[i]["analysis_name"]==j:
#                 dico_analysis_name[j]=dicos_saved[i]["group_name"]+" // "+dicos_saved[i]["analysis_type"]+" // "
#                 if "origine" in dicos_saved[i].keys():
#                   dico_origine_saved[j]=dicos_saved[i]["origine"]
#                 else:
#                   dico_origine_saved[j]="group_without_name"
# 
#     for key,value in counter:
#         dico_group[key]={}
#         for j in analysis_name:
#             if key+" " in dico_analysis_name[j]:
#                 dico_group[key][j]=dico_analysis_name[j]
#     
#     dico={}
#     for key, value in counter:
#         names=[]
#         for cle, valeur in dico_group[key].items():
#             names.append(cle)
#             dico[key]=names
# 
#     liste=[]
#     for i in range(0,len(dicos_saved)):
#       liste.append(dicos_saved[i]["analysis_name"]+' '+dicos_saved[i]["group_name"]+' '+dicos_saved[i]["analysis_type"])    
#     
#     origine=[]
#     for i in range(0,len(dicos_saved)):
#       origine.append(dicos_saved[i]["analysis_name"]+' <- '+dicos_saved[i]["origine"])
#     
#     return render(request,'analysis/historic.html',{"dico":dico,"dico_group":dico_group,"dico_origine":dico_origine_saved,"liste":liste,"counter":counter,"origine":origine})
# 
# class automatisationThread(threading.Thread):
#     def __init__(self,listArgs):
#         self.stdout = None
#         self.stderr = None
#         self.listArgs=listArgs
#         threading.Thread.__init__(self)
#         
#     def run(self):
#         listArgs=self.listArgs
#         filename='automatisation.R'
#         # Path complet du fichier .r
#         full_filename=settings.MEDIA_ROOT_ANALYSIS+username+'scriptR/'+filename
#         name=settings.MEDIA_ROOT_ANALYSIS+username+'automatisation.log'
#         log = open(name, "w")
#         
#         submit=listArgs["submit"]
#         
#         id_person_in=listArgs["#liste_person_in"]
#         id2_person_in=listArgs.getlist("#liste_person_in")
#         if id_person_in=='':
#             person_in="NULL"
#         else:
#             if(len(id2_person_in)==2):
#                 pers_in=listArgs["#liste_person_in"]        
#                 pers_in=pers_in.encode('utf-8')                
#             else:
#                 pers_in=[x.encode('utf-8') for x in (listArgs.getlist("#liste_person_in"))]
#                 if '' in pers_in:
#                   pers_in.remove('')
#                 pers_in=tuple(pers_in)
#             person_in="{0}".format(str(pers_in)) 
#         
#         robjects.r("MEDIA_ROOT_ANALYSIS='{0}'".format(settings.MEDIA_ROOT_ANALYSIS))
#         robjects.r("username='{0}'".format(username))
#         robjects.r('''            
#               library(shinemas2R)
#               load(paste(MEDIA_ROOT_ANALYSIS,username,"templateRun.RData",sep=""))
#               ''')
#         figures=list(robjects.r["figures"])
#         dicos_saved=[]
#         for i in figures:
#             dicos_saved.append(pickle.load(open(settings.MEDIA_ROOT_ANALYSIS+username+i+"/"+i+".p")))
#         origine=[]
#         for i in dicos_saved:
#             origine.append(i["analysis_name"])
#             origine.append(i["origine"])
#         if submit=="person":
#             form_name=listArgs["person_form"]
#             liste_person_ou_germ=Person.objects.values_list('short_name',flat=True)
#             origine=[x.encode('utf-8') for x in origine]
#             liste_person_ou_germ=[x.encode('utf-8') for x in liste_person_ou_germ]
#             
#             id_person_in=listArgs["#liste_person_in"]
#             id2_person_in=listArgs.getlist("#liste_person_in")
#             if id_person_in=='':
#                 liste_person_germ="NULL"
#             else:
#                 if(len(id2_person_in)==2):
#                     pers_in=listArgs["#liste_person_in"]        
#                     pers_in=pers_in.encode('utf-8')                
#                 else:
#                     pers_in=[x.encode('utf-8') for x in (listArgs.getlist("#liste_person_in"))]
#                     if '' in pers_in:
#                       pers_in.remove('')
#                     pers_in=tuple(pers_in)
#                 liste_person_germ="{0}".format(str(pers_in)) 
#         else:
#             form_name=listArgs["germplasm_form"]
#             liste_person_ou_germ=Germplasm.objects.values_list('germplasm_name',flat=True)
#             origine=[x.encode('utf-8') for x in origine]
#             liste_person_ou_germ=[x.encode('utf-8') for x in liste_person_ou_germ]
#             id_germplasm_in=listArgs["#liste_germ_in"]
#             id2_germplasm_in=listArgs.getlist("#liste_germ_in")
#             if id_germplasm_in=='':
#                 liste_person_germ="NULL"
#             else:
#                 if(len(id2_germplasm_in)==2):
#                     germ_in=listArgs["#liste_germ_in"]        
#                     germ_in=germ_in.encode('utf-8')
#                 else:
#                     germ_in=[x.encode('utf-8') for x in (listArgs.getlist("#liste_germ_in"))]
#                     if '' in germ_in:
#                       germ_in.remove('')
#                     germ_in=tuple(germ_in)
#                 liste_person_germ="{0}".format(str(germ_in))  
#         
#         figures=tuple(figures)
#         figures="{0}".format(str(figures))
#         
#         origine=tuple(origine)
#         origine="{0}".format(str(origine))
#         
#         liste_person_ou_germ=tuple(liste_person_ou_germ)
#         liste_person_ou_germ="{0}".format(str(liste_person_ou_germ))
# 
#         log = open(name, "w")      
#         proc = subprocess.Popen(["Rscript",full_filename,form_name,submit,figures,origine,liste_person_ou_germ,liste_person_germ],shell=False, stdout=log,stderr=log,universal_newlines=True)        
#         log.close()
#         
# def automatisation(request):
#     path=settings.MEDIA_ROOT_ANALYSIS+username+"zip/"
#     if os.path.isdir(path)==False:
#       os.mkdir(path)
#       
#     result=request.POST
#     submit=result["submit"]
#     
#     
#     robjects.r("MEDIA_ROOT_ANALYSIS='{0}'".format(settings.MEDIA_ROOT_ANALYSIS))
#     robjects.r("username='{0}'".format(username))
#     robjects.r('''            
#           library(shinemas2R)
#           load(paste(MEDIA_ROOT_ANALYSIS,username,"templateRun.RData",sep=""))
#           ''')
#     figures=list(robjects.r["figures"])
#     dicos_saved=[]
#     for i in figures:
#         dicos_saved.append(pickle.load(open(settings.MEDIA_ROOT_ANALYSIS+username+i+"/"+i+".p")))
#     origine=[]
#     for i in dicos_saved:
#         origine.append(i["analysis_name"])
#         origine.append(i["origine"])
#     if submit=="person":
#         form_name=result["person_form"]
#         liste_person_ou_germ=Person.objects.values_list('short_name',flat=True)
#         origine=[x.encode('utf-8') for x in origine]
#         liste_person_ou_germ=[x.encode('utf-8') for x in liste_person_ou_germ]
#         id_person_in=result["#liste_person_in"]
#         id2_person_in=result.getlist("#liste_person_in")
#         if id_person_in=='':
#             liste_person_germ="NULL"
#         else:
#             if(len(id2_person_in)==2):
#                 pers_in=result["#liste_person_in"]        
#                 pers_in=pers_in.encode('utf-8')                
#             else:
#                 pers_in=[x.encode('utf-8') for x in (result.getlist("#liste_person_in"))]
#                 if '' in pers_in:
#                   pers_in.remove('')
#                 pers_in=tuple(pers_in)
#             liste_person_germ="{0}".format(str(pers_in)) 
#     else:
#         form_name=result["germplasm_form"]
#         liste_person_ou_germ=Germplasm.objects.values_list('germplasm_name',flat=True)
#         origine=[x.encode('utf-8') for x in origine]
#         liste_person_ou_germ=[x.encode('utf-8') for x in liste_person_ou_germ]
#         id_germplasm_in=result["#liste_germ_in"]
#         id2_germplasm_in=result.getlist("#liste_germ_in")
#         if id_germplasm_in=='':
#             liste_person_germ="NULL"
#         else:
#             if(len(id2_germplasm_in)==2):
#                 germ_in=result["#liste_germ_in"]        
#                 germ_in=germ_in.encode('utf-8')
#             else:
#                 germ_in=[x.encode('utf-8') for x in (result.getlist("#liste_germ_in"))]
#                 if '' in germ_in:
#                   germ_in.remove('')
#                 germ_in=tuple(germ_in)
#             liste_person_germ="{0}".format(str(germ_in))  
# 
#     #return render(request, 'analysis/automatisation.html',{"form_name":form_name,"submit":submit,"figures":figures,"origine":origine,"liste_person_ou_germ":liste_person_ou_germ,"person_in":person_in})
#     runThread=automatisationThread(result)
#     runThread.run()
#     
#     
#     result={}
#     result["form_name"]=form_name
#     result["figures"]=figures
#     result["dicos_saved"]=dicos_saved
#     result["origine"]=origine
#     result["liste_person_ou_germ"]=liste_person_ou_germ
#     result["submit"]=submit
#     result["person_in"]=pers_in
# 
# 
#     return render(request, 'analysis/automatisation.html',{"result":liste_person_ou_germ})
#         
#     
#     
# #------------------------------------------------------------------------------------------------------------
# #-------------------------Read file.log------------------------
# @login_required
# def readFile(line):
#     name=settings.MEDIA_ROOT_ANALYSIS+username+'firstRun.log'
#     f = open(name, 'r')
#     return HttpResponse(f.readlines()[76:])
# 
# @login_required
# def readFile2(line):
#     name=settings.MEDIA_ROOT_ANALYSIS+username+'secondRun.log'
#     f = open(name, 'r')
#     return HttpResponse(f.readlines()[60:])
# 
# @login_required
# def readFile3(line):
#     name=settings.MEDIA_ROOT_ANALYSIS+username+'thirdRun.log'
#     f = open(name, 'r')
#     return HttpResponse(f.readlines()[60:])
#   
# @login_required  
# def readFileTemp(line):
#     name=settings.MEDIA_ROOT_ANALYSIS+username+'templateRun.log'
#     f = open(name, 'r')
#     return HttpResponse(f.readlines()[77:])
#     
# def file_link(request):
#     if request.GET.get('form_name'):
#         nom_pdf=request.GET.get('form_name')
#         response= HttpResponse(content_type="application/pdf") 
#         response['Content-Disposition']='attachment; filename={0}.pdf'.format(nom_pdf)
#         docPdf=settings.MEDIA_ROOT_ANALYSIS+username+nom_pdf+".pdf"
#         handler = open(docPdf,'r')
#         response.write(handler.read())
#         return response
#     elif request.GET.get('RData'):
#         response= HttpResponse(content_type="application/hdf") 
#         response['Content-Disposition']='attachment; filename=templateRun.RData'
#         RData=settings.MEDIA_ROOT_ANALYSIS+username+"templateRun.RData"
#         handler = open(RData,'r')
#         response.write(handler.read())
#         return response
#     elif request.GET.get('Log'):
#         response= HttpResponse(content_type="application/hdf") 
#         response['Content-Disposition']='attachment; filename=templateRun.log'
#         Log=settings.MEDIA_ROOT_ANALYSIS+username+"templateRun.log"
#         handler = open(Log,'r')
#         response.write(handler.read())
#         return response
#       
# def file_link2(request):
#     if request.GET.get('form_name'):
#         nom_pdf=request.GET.get('form_name')
#         response= HttpResponse(content_type="application/pdf") 
#         response['Content-Disposition']='attachment; filename={0}.pdf'.format(nom_pdf)
#         docPdf=settings.MEDIA_ROOT_ANALYSIS+username+nom_pdf+".pdf"
#         handler = open(docPdf,'r')
#         response.write(handler.read())
#         return response
#     elif request.GET.get('RData'):
#         response= HttpResponse(content_type="application/hdf") 
#         response['Content-Disposition']='attachment; filename=thirdRun.RData'
#         RData=settings.MEDIA_ROOT_ANALYSIS+username+"thirdRun.RData"
#         handler = open(RData,'r')
#         response.write(handler.read())
#         return response
#     elif request.GET.get('Log'):
#         response= HttpResponse(content_type="application/hdf") 
#         response['Content-Disposition']='attachment; filename=thirdRun.log'
#         Log=settings.MEDIA_ROOT_ANALYSIS+username+"thirdRun.log"
#         handler = open(Log,'r')
#         response.write(handler.read())
#         return response 
#   
# def formCorrGps(request):
#     if request.is_ajax(): 
#         robjects.r("MEDIA_ROOT_ANALYSIS='{0}'".format(settings.MEDIA_ROOT_ANALYSIS))
#         robjects.r("username='{0}'".format(username))
#         robjects.r('''            
#               library(shinemas2R)
#               load(paste(MEDIA_ROOT_ANALYSIS,username,"firstRun.RData",sep=""))
#               m=toto$data$data.with.correlated.variables
#               if (is.null(m)){
#                 names_corr_gps="none"
#               }else{
#                 names_corr_gps=names(m)
#               }
#               ''')
#         corr_gps=robjects.r["names_corr_gps"]
#         return HttpResponse("<option value={0}>{0}</option>".format(i) for i in corr_gps)           
#   
# def formVecVar(request):
#     if request.is_ajax():
#         robjects.r("MEDIA_ROOT_ANALYSIS='{0}'".format(settings.MEDIA_ROOT_ANALYSIS))
#         robjects.r("username='{0}'".format(username))
#         robjects.r('''
#                 library(shinemas2R)
#                 load(paste(MEDIA_ROOT_ANALYSIS,username,"firstRun.RData",sep=""))
#                 m=toto$data$data
#                 names_vec_var=names(m)
#                 ''')
#         vec_var=robjects.r["names_vec_var"]
#         return HttpResponse("<option value={0}>{0}</option>".format(i) for i in vec_var)
# 
# def checkBoxFigures(request):
#     import rpy2.robjects as robjects
#     robjects.r("MEDIA_ROOT_ANALYSIS='{0}'".format(settings.MEDIA_ROOT_ANALYSIS))
#     robjects.r("username='{0}'".format(username))
#     #robjects.r('''
#       #b=""
#       #c=""
#       #if (file.exists(paste(MEDIA_ROOT_ANALYSIS,username,"secondRun.RData",sep=""))){
#       #load(paste(MEDIA_ROOT_ANALYSIS,username,"secondRun.RData",sep=""))
#       #if (i>0 && j==0){
#         #b="Network1"
#         #c=""
#         #if (i>1){
#           #for (v in 2:i){
#               #b[v]=paste("Network",v,sep="")
#           #}
#         #}
#       #}else if(i==0 && j==0){
#         #b=""
#         #c=""
#       #}else if(i>0 && j>0){
#         #b="Network1"
#         #c="DataPlot1"
#         #if (i>1){
#           #for (v in 2:i){
#               #c[v]=paste("Network",v,sep="")
#           #}
#         #}  
#         #if (j>1){
#           #for (v in 2:j){
#               #b[v]=paste("DataPlot",v,sep="")
#           #}
#         #}
#       #}else if(i==0 && j>0){
#         #b=""
#         #c="DataPlot1"
#         #if (j>1){
#           #for (v in 2:j){
#               #c[v]=paste("DataPlot",v,sep="")
#           #}
#         #}
#       #}
#     #}''')
#     #b=robjects.r["b"]
#     #c=robjects.r["c"]
#     #b=tuple([e for e in b])
#     #c=tuple([e for e in c])
#     #choices=b+c
#     #a=('')
#     #choices2=list(e for e in choices if e!=a)
#     robjects.r('''
#       if (file.exists(paste(MEDIA_ROOT_ANALYSIS,username,"secondRun.RData",sep=""))){
#         load(paste(MEDIA_ROOT_ANALYSIS,username,"secondRun.RData",sep=""))
#         if (exists("Network")){
#             i="Network"
#         }else if (exists("DataPlot")){
#             i="DataPlot"
#         }
#         }
#       ''')
#     i=robjects.r["i"]
#     i=list(i)
#     return HttpResponse("<input type='hidden' value={0} name='figures_to_display'></input>{0}".format(i) for i in i)
#   
# def figuresDrag(request):
#     liste_figures_historic=request.POST['liste_figures_historic'].encode('utf-8')
#     liste_figures_historic=liste_figures_historic.replace("[",'').replace("]",'').replace(" ",'')
#     liste_figures_historic=liste_figures_historic.split(",")
#     #liste_figures_historic=liste_figures_historic.split("'")
#     #a=""
#     #for a in liste_figures_historic:
#         #liste_figures_historic.remove(a)    
#     
#     return HttpResponse("<div class='fig_drag' value='{0}' name='element_pdf'>{0}<input type='hidden' name='element_pdf' value='figure'></input><input type='hidden' name='element_pdf' value='{0}'></input></div>".format(i) for i in liste_figures_historic)
# #<div class='fig_drag' value='{0}' name='element_pdf'>{0}<input type='hidden' name='element_pdf' value='{0}'></input></div>
# 
# #////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
# #\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# #############################################         BACKUP         #################################################################
# #////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
# #\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# """
# Si on a un groupe de selectionne
# __try : verifie analyse n'est pas deja sauvegardee
# _________recupere id analyse si sauvegardee
# _________try : trouver le groupe
# ______________recupere id group
# ______________try : verifie analyse rattachee au groupe
# ______________________pass
# ______________except : analyse pas rattachee 
# ______________________ajoute
# 
# 
# 
# 
# __except not exist
# _________creer analyse, recupere id
# _________creer file
# _________mouv file
# _________ajoute dans le groupe
# Si utilisateur saisie clavier groupe
# __verifie si existe
# _________si oui
# ______________message
# _________si non
# ______________try recuperation content File
# _____________________
# 
# """
# 
# def ajax_save(request):
#     print("COMMENCE")
#     data_dict = request.POST.copy() # list of parameters' copy for manipulating data
#     print(data_dict)
#     first_runFile = settings.MEDIA_ROOT_ANALYSIS+username+'firstRun.RData'    
#     hashF = hashlib.sha1(open(first_runFile).read()).hexdigest()
#     gr_name = unicode(str(data_dict["group_name"]))
#     del data_dict["group_name"] #removes the group name from list of parameters
#     print(type(request.user))
#     usr = User.objects.get(username=request.user)
#     analyse_name = str(data_dict["family_analysis"])    
#     commentary = unicode(str(data_dict["comment"]))
#     statut = str(data_dict.get("statut"))   
#     del data_dict["comment"] #removes comment from list of parameters
#     del data_dict["statut"]
#     
#     if gr_name == '':
#         ch_name = unicode(str(data_dict["choose_group"]))
#         del data_dict["choose_group"] #removes the name chosen from list of parameters
#         try :
#             id_analyse = Analysis.objects.get(user=usr, analysis_name=analyse_name, comment=commentary ,parameters=data_dict) # look for analysis
#             try :
#                 id_group = Group_Analysis.objects.get(id=ch_name) # look for the group  
#                 try :
#                     id_group = Group_Analysis.objects.filter(analyse=id_analyse, id=ch_name) # the group exist and already has the analysis
#                     return HttpResponse(simplejson.dumps({'success':True, 'message_res':'This analysis and group already exist'}), mimetype='application/json')
#                 except Group_Analysis.DoesNotExist:
#                     id_group.analyse.add(id_analyse) # the group exist but does not contain the analysis so add it
#                     return HttpResponse(simplejson.dumps({'success':True, 'message_res':'This analysis has been add to the group and save.'}), mimetype='application/json')
#             except Group_Analysis.DoesNotExist:
#                 # the group does not exist so :
#                 g = Group_Analysis(statut=statut,group_name=ch_name) # group creation
#                 g.save()
#                 g.analyse.add(id_analyse) # add the reference of the analysis
#                 return HttpResponse(simplejson.dumps({'success':True, 'message_res':u'The backup procedure was successfully completed'}), mimetype='application/json')                
#         except Analysis.DoesNotExist:
#             print("ANALYSIS DOES NOT EXIST")
#             # the analysis does not exist so :
#             a = Analysis(user=usr, analysis_name=analyse_name, comment=commentary ,parameters=data_dict)
#             a.save()
#             analyse_ref = a.pk
#             user_path = settings.MEDIA_ROOT_ANALYSIS+username+str(usr)
#             if os.path.isdir(user_path) :
# 
#                 # if user's folder exists        
#                 a_path = user_path+'/'+str(analyse_ref)###
#                 os.mkdir(a_path)
#             else :
# 
#                 # user's folder does not exist
#                 os.mkdir(user_path)
#                 a_path = user_path+'/'+str(analyse_ref)
#                 os.mkdir(a_path)
#             # move files
#             destination = a_path
#             source_r = first_runFile
#             shutil.copy(source_r,destination)
#             r_path = a_path+'/firstRun.RData'
# 
#             if analyse_name == "Get the network":
#                 source_pdf = settings.MEDIA_ROOT_ANALYSIS+username+'firstRun.pdf'                
#                 shutil.copy(source_pdf, destination)            
#                 pdf_path = a_path+'/firstRun.pdf'###
#                 # save in database
# 
#                 af = Analysis_File(analyse=a, RData_path=r_path , PDF_path=pdf_path , content_file=hashF)
#                 af.save()
#             else:
#                 af = Analysis_File(analyse=a, RData_path=r_path, content_file=hashF)
#                 af.save()
#             #print 'g'
#             g = Group_Analysis.objects.get(id=ch_name)
#             g.analyse.add(a)
# 
#             return HttpResponse(simplejson.dumps({'success':True, 'message_res': u"Analysis saved"}), mimetype='application/json')
#     else :
# 
#         try :
# 
#             id_group = Group_Analysis.objects.get(group_name=gr_name) # the group exists                                                        
#             return HttpResponse(simplejson.dumps({'success':True, 'message_res': u"This group already exists"}), mimetype='application/json')
#         except Group_Analysis.DoesNotExist:
# 
#             try :
# 
#                 id_analyse =  Analysis.objects.get(user=usr, analysis_name=analyse_name, comment=commentary ,parameters=data_dict) # look for analysis
# 
# 
#                 g = Group_Analysis(statut=statut,group_name=gr_name) # group creation
#                 g.save()
#                 g.analyse.add(id_analyse) # add the reference of the analysis    
# 
#                 return HttpResponse(simplejson.dumps({'success':True, 'message_res': u"The backup procedure was successfully completed"}), mimetype='application/json')
#             except Analysis.DoesNotExist:
# 
#                 # the analysis does not exist so :
#                 a = Analysis(user=usr, analysis_name=analyse_name, comment=commentary ,parameters=data_dict)
#                 a.save()
# 
#                 analyse_ref = a.pk
#                 user_path = settings.MEDIA_ROOT_ANALYSIS+username+str(usr)
#                 if os.path.isdir(user_path) :
#                     # if user's folder exists        
#                     a_path = user_path+'/'+str(analyse_ref)
#                     os.mkdir(a_path)
#                 else :
#                     # user's folder does not exist
#                     os.mkdir(user_path)
#                     a_path = user_path+'/'+str(analyse_ref)
#                     os.mkdir(a_path)
#                 # move files
#                 destination = a_path
#                 source_r = first_runFile
#                 source_pdf = settings.MEDIA_ROOT_ANALYSIS+username+'firstRun.pdf'    
#                 shutil.copy(source_r,destination)
#                 shutil.copy(source_pdf, destination)
#                 r_path = a_path+'/firstRun.RData'
#                 pdf_path = a_path+'/firstRun.pdf'
#                 # save in database
#                 af = Analysis_File(analyse=a, RData_path=r_path , PDF_path=pdf_path , content_file=hashF)
#                 af.save()
#                 g = Group_Analysis(statut=statut,group_name=gr_name) # group creation
#                 g.save()
#                 g.analyse.add(a)                                                             
#                 return HttpResponse(simplejson.dumps({'success' : True, 'message_res' : u"Analysis saved"}), mimetype='application/json')
# 
# #-----------------------------------DEPLACEMENT FILE FUNCTION ----------------------------------
# 
# def file_deplacement(analyse_ref, usr, number_run, analysis_name):
#     if number_run == "first_run":
#         first_runFile = settings.MEDIA_ROOT_ANALYSIS+username+'firstRun.RData'    
#         hashF = hashlib.sha1(open(first_runFile).read()).hexdigest()
#         user_path = settings.MEDIA_ROOT_ANALYSIS+username+str(usr)
#         if os.path.isdir(user_path) :
#             # if user's folder exists        
#             a_path = user_path+'/'+str(analyse_ref)
#             os.mkdir(a_path)
#         else :
#             # user's folder does not exist
#             os.mkdir(user_path)
#             a_path = user_path+'/'+str(analyse_ref)
#             os.mkdir(a_path)
#         destination = a_path
#         source_r = first_runFile
#         shutil.copy(source_r,destination)
#         r_path = a_path+'/firstRun.RData'
# 
#         if analysis_name == "Get the network":
#             source_pdf = settings.MEDIA_ROOT_ANALYSIS+username+'firstRun.pdf'                
#             shutil.copy(source_pdf, destination)            
#             pdf_path = a_path+'/firstRun.pdf'
#             return(r_path,pdf_path,hashF)
#         else:
#             return(r_path,hashF)
#     elif number_run == "second_run":
#         second_runFile = settings.MEDIA_ROOT_ANALYSIS+username+'secondRun.RData'    
#         hashF = hashlib.sha1(open(second_runFile).read()).hexdigest()
#         user_path = settings.MEDIA_ROOT_ANALYSIS+username+str(usr)
#         if os.path.isdir(user_path) :
#             # if user's folder exists        
#             a_path = user_path+'/'+str(analyse_ref)
#             os.mkdir(a_path)
#         else :
#             # user's folder does not exist
#             os.mkdir(user_path)
#             a_path = user_path+'/'+str(analyse_ref)
#             os.mkdir(a_path)
#         destination = a_path
#         source_r = second_runFile
#         shutil.copy(source_r,destination)
#         r_path = a_path+'/secondRun.RData'
# 
#         source_pdf = settings.MEDIA_ROOT_ANALYSIS+username+'secondRun.pdf'                
#         shutil.copy(source_pdf, destination)            
#         pdf_path = a_path+'/secondRun.pdf'
#         return(r_path,pdf_path,hashF)
#     elif number_run == "third_run":
#         third_runFile = settings.MEDIA_ROOT_ANALYSIS+username+'thirdRun.RData'    
#         hashF = hashlib.sha1(open(third_runFile).read()).hexdigest()
#         user_path = settings.MEDIA_ROOT_ANALYSIS+username+str(usr)
#         if os.path.isdir(user_path) :
#             # if user's folder exists        
#             a_path = user_path+'/'+str(analyse_ref)
#             os.mkdir(a_path)
#         else :
#             # user's folder does not exist
#             os.mkdir(user_path)
#             a_path = user_path+'/'+str(analyse_ref)
#             os.mkdir(a_path)
#         destination = a_path
#         source_r = third_runFile
#         shutil.copy(source_r,destination)
#         r_path = a_path+'/thirdRun.RData'
# 
#         source_pdf = settings.MEDIA_ROOT_ANALYSIS+username+'thirdRun.pdf'                
#         shutil.copy(source_pdf, destination)            
#         pdf_path = a_path+'/thirdRun.pdf'
#         return(r_path,pdf_path,hashF)
# 
# 
# 
# #----------------------------------- SECOND VIEW FOR BACKUP ----------------------------------
# def ajax_save2(request):
#     """
#     data_dict : copy of request.POST for manipulation
#     dict_total : idem, for backup of secondRun
#     dict_param : reconstitution of request.POST for the backup of firstRun
#     """
#     data_dict = request.POST.copy() # list of parameters' copy for manipulating data
#     gr_name = unicode(str(data_dict["group_name"]))
#     del data_dict["group_name"] #removes the group name from list of parameters
#     usr = User.objects.get(username=request.user)
#     analyse_name = str(data_dict["family_analysis1"])
#     analyse_name2 = str(data_dict["family_analysis"])    
#     commentary = unicode(str(data_dict["comment"]))
#     statut = str(data_dict.get("statut"))   
#     del data_dict["comment"] #removes comment from list of parameters
#     del data_dict["statut"]
#     dict_total = data_dict
#     # reconsitution of the request.POST of firstRun
#     dict_param = {}
#     dict_param['family_analysis'] = analyse_name
#     dict_param['start1'] = [u'Submit']
#     if analyse_name == "Get the data":
#         list_param = ("requete_type", "relation_on", "generation", "germplasm_in_a")
#     else :
#         list_param = ("legend_another_plot", "vertex_legend", "edge_legend", "gender", "hide_name", "fill_diffusion_gap", "display_label", "text_size", "title", "col_type", "diffusion_color", "color_reproduction", "color_mixture", "color_selection", "display_diffusion", "display_reproduction", "display_mixture", "display_selection", "display_reproduction_type", "display_generation", "germplasm_in", "germplasm_out", "person_in", "person_out", "year_in", "year_out", "project_in", "project_out", "SL_name")
#     for key,value in data_dict.items():
#         for motif in list_param:
#             if key == motif:
#                 dict_param[motif] = data_dict.pop(key)
# 
#     
# 
#     ####################################################################### GROUP SELECTED ###################################################
#     if gr_name == '':
#         ch_name = unicode(str(dict_total["choose_group"]))
#         del dict_total["choose_group"] #removes the name chosen from list of parameters
#         try :
#             id_group = Group_Analysis.objects.get(id=ch_name) # look for the group  
#             try :
# 
#                 # look for the first analysis
#                 id_analyse1 = Analysis.objects.get(user=usr, analysis_name=analyse_name, comment=commentary ,parameters=dict_param) 
#                 try :
#                     id_groupANDanalysis = Group_Analysis.objects.filter(analyse=id_analyse1, id=ch_name)
#                 except Group_Analysis.DoesNotExist:
#                     id_group.analyse.add(id_analyse1) # the group exist but does not contain the analysis so add it
#             except Analysis.DoesNotExist:
# 
#                 # the analysis does not exist so :
#                 a = Analysis(user=usr, analysis_name=analyse_name, comment=commentary ,parameters=dict_param)
#                 a.save()
#                 analyse_ref = a.pk
#                 id_analyse1 = a
#                 number_run = "first_run"
#                 if analyse_name == "Get the network":
#                     (r_path,pdf_path,hashF) = file_deplacement(analyse_ref, usr, number_run, analyse_name)
#                     af = Analysis_File(analyse=a, RData_path=r_path , PDF_path=pdf_path , content_file=hashF)
#                     af.save()
#                 else:
#                     (r_path,hashF) = file_deplacement(analyse_ref, usr, number_run, analyse_name)
#                     af = Analysis_File(analyse=a, RData_path=r_path, content_file=hashF)
#                     af.save()
#                 g = Group_Analysis.objects.get(id=ch_name)
#                 g.analyse.add(a)
#             finally:
#                 try :
#                     # look for the second analysis
#                     id_analyse2 = Analysis.objects.get(user=usr, analysis_name=analyse_name2, comment=commentary ,parameters=dict_total) 
#                     try :
#                         id_groupANDanalysis2 = Group_Analysis.objects.filter(analyse=id_analyse2, id=ch_name)
#                         return HttpResponse(simplejson.dumps({'success':True, 'message_res':u"This backup already exists"}),mimetype='application/json')
#                     except Group_Analysis.DoesNotExist:
#                         id_group.analyse.add(id_analyse2) # the group exist but does not contain the analysis so add it
#                         return HttpResponse(simplejson.dumps({'success':True, 'message_res':u"The backup procedure was successfully completed"}),mimetype='application/json')
#                 except Analysis.DoesNotExist:
#                     # the analysis does not exist so :
# 
#                     a2 = Analysis(user=usr, analysis_name=analyse_name2, comment=commentary ,parameters=dict_total, analysis_ref=id_analyse1)
#                     a2.save()
#                     analyse_ref2 = a2.pk
#                     number_run = "second_run"
#                     (r_path2,pdf_path2,hashF2) = file_deplacement(analyse_ref2, usr, number_run, analyse_name2)
#                     af2 = Analysis_File(analyse=a2, RData_path=r_path2 , PDF_path=pdf_path2 , content_file=hashF2)
#                     af2.save()
#                     g = Group_Analysis.objects.get(id=ch_name)
#                     g.analyse.add(a2)
# 
#                     return HttpResponse(simplejson.dumps({'success':True, 'message_res':u"The backup procedure was successfully completed"}),mimetype='application/json')
#         except Group_Analysis.DoesNotExist:
#             return HttpResponse(simplejson.dumps({'success':False, 'message_res':u'An error occurred. Please select a valid group.'}), mimetype='application/json')
#     else :
# 
#         try :
#             id_group = Group_Analysis.objects.get(group_name=gr_name) # the group exists                                                        
#             return HttpResponse(simplejson.dumps({'success':True, 'message_res': u"This group already exists"}), mimetype='application/json')
#         except Group_Analysis.DoesNotExist:
#             g = Group_Analysis(statut=statut,group_name=gr_name) # group creation
#             g.save()
#             try :
#                 # look for the first analysis
#                 id_analyse1 = Analysis.objects.get(user=usr, analysis_name=analyse_name, comment=commentary ,parameters=dict_param) 
#                 g.analyse.add(id_analyse1)
#             except Analysis.DoesNotExist:
#                 # the first analysis does not exist so :
#                 a = Analysis(user=usr, analysis_name=analyse_name, comment=commentary ,parameters=dict_param)
#                 a.save()
#                 analyse_ref = a.pk
#                 id_analyse1 = a
#                 number_run = "first_run"
#                 if analyse_name == "Get the network":
#                     (r_path,pdf_path,hashF) = file_deplacement(analyse_ref, usr, number_run, analyse_name)
#                     af = Analysis_File(analyse=a, RData_path=r_path , PDF_path=pdf_path , content_file=hashF)
#                     af.save()
#                 else:
#                     (r_path,hashF) = file_deplacement(analyse_ref, usr, number_run, analyse_name)
#                     af = Analysis_File(analyse=a, RData_path=r_path, content_file=hashF)
#                     af.save()
#                 g.analyse.add(a)
#             finally:
#                 try :
#                     # look for the second analysis
#                     id_analyse2 = Analysis.objects.get(user=usr, analysis_name=analyse_name2, comment=commentary ,parameters=dict_total) 
#                     g.analyse.add(id_analyse2)
#                     return HttpResponse(simplejson.dumps({'success':True, 'message_res':u"The backup procedure was successfully completed"}),mimetype='application/json')
#                 except Analysis.DoesNotExist:
#                     # the analysis does not exist so :
#                     a2 = Analysis(user=usr, analysis_name=analyse_name2, comment=commentary ,parameters=dict_total, analysis_ref=id_analyse1)
#                     a2.save()
#                     analyse_ref2 = a2.pk
#                     number_run = "second_run"
#                     (r_path2,pdf_path2,hashF2) = file_deplacement(analyse_ref2, usr, number_run, analyse_name2)
#                     af2 = Analysis_File(analyse=a2, RData_path=r_path2 , PDF_path=pdf_path2 , content_file=hashF2)
#                     af2.save()
#                     g.analyse.add(a2)
#                     return HttpResponse(simplejson.dumps({'success':True, 'message_res':u"The backup procedure was successfully completed"}),mimetype='application/json')
