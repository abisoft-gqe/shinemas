# ## -*- coding: utf-8 -*-
# #""" LICENCE:
#     #SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
#     #with the close collaboration of the RSP (Réseau Semence Paysanne)
#     
#     #Copyright (C) 2014, Marie Lefebvre
# 
#     #This file is part of SHiNeMaS
# 
#     #SHiNeMaS is free software: you can redistribute it and/or modify
#     #it under the terms of the GNU Affero General Public License as
#     #published by the Free Software Foundation, either version 3 of the
#     #License, or (at your option) any later version.
# 
#     #This program is distributed in the hope that it will be useful,
#     #but WITHOUT ANY WARRANTY; without even the implied warranty of
#     #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     #GNU Affero General Public License for more details.
# 
#     #You should have received a copy of the GNU Affero General Public License
#     #along with this program.  If not, see <http://www.gnu.org/licenses/>.
#     #LICENCE
#     
#     #:data `tuples_vals_TF`: A True/False tuple for Analysis forms
#     #:data `text_size_vals`: Allow to define the size of the text for R graphs
#     #:data `color_type_vals`: A tuple for colors used in R graphs
#     #:data `color_dif_vals`: A tuple for colors used in R graphs
#     #:data `color_repro_vals`: A tuple for colors used in R graphs
#     #:data `color_mix_vals`: A tuple for colors used in R graphs
#     #:data `color_selec_vals`: A tuple for colors used in R graphs
# #"""
# from django import forms
# from django.utils.translation import gettext_lazy as _
# from django.apps import apps
# 
# import rpy2.robjects as robjects
# import rpy2.rinterface as rinterface
# from entities.models import Germplasm, Seedlot
# from actors.models import Person, Project
# from eppdata.models import Variable
# from analysis.models import Group_Analysis
# 
# 
# ## GET THE DATA FUNCTION
# 
# #List of possible values for parameters
# query_type_vals=[('person','person'),('year','year'),('variable','variable'),('project','project'),('seed lots','seed lots'),('selection person','selection person'),('reproduction type','reproduction type'),
#          ('germplasm type','germplasm type'),('germplasm','germplasm'),('cross','cross'),('methods','methods'),('network','network'),('data-classic','data-classic'),
#          ('data-S','data-S'),('data-SR','data-SR'),('data-mixture-1','data-mixture-1'),('species','species'),('person.info','person.info'),('grand-father','grand-father')]
# filter_on_vals=[('','---------'),('son','son'),('father','father'),('father-son','father-son')]
# TF_vals=[('TRUE','TRUE'),('FALSE','FALSE')]
# data_type_vals=[('','---------'),('relation','relation'),('seed-lots','seed-lots')]
# relation_in_vals=[('','---------'),('reproduction','reproduction'),('mixture','mixture'),('diffusion','diffusion'),('selection','selection')]
# 
# 
# def variables_in():
#     Variable=apps.get_model('eppdata', 'Variable')
#     variables_in=Variable.objects.all().order_by('name')
#     return variables_in
# # Definition of the parameters' form
# class NetworkForm(forms.Form):
#     """
#         A form that fit the parameters of the R package `shinemas2R <https://github.com/priviere/shinemas2R>`_ for the Network analysis
#         
#         **TODO: documentation need to be updated with the new version of shinemas2R**
#     """
#     legend_another_plot = forms.ChoiceField(label=_("legend_on_another_plot"),widget=forms.RadioSelect(),required=True,initial='TRUE',choices=tuples_vals_TF)
#     vertex_legend = forms.ChoiceField(label=_("vertex_legend"),widget=forms.RadioSelect(),required=True,initial='TRUE',choices=tuples_vals_TF)
#     edge_legend = forms.ChoiceField(label=_("edge_legend"),widget=forms.RadioSelect(),required=True,initial='TRUE',choices=tuples_vals_TF)
#     gender = forms.ChoiceField(label=_("gender"),widget=forms.RadioSelect(),required=True,initial='FALSE',choices=tuples_vals_TF)
#     hide_name = forms.ChoiceField(label=_("hide_name"),widget=forms.Select(),required=True,initial='ALL',choices=tuples_vals_AN)
#     fill_diffusion_gap=forms.ChoiceField(label=_("fill_diffusion_gap"),widget=forms.RadioSelect(),required=True,initial='FALSE',choices=tuples_vals_TF)
#     display_label=forms.ChoiceField(label=_("display_label"),widget=forms.RadioSelect(),required=True,initial='TRUE',choices=tuples_vals_TF)
#     text_size=forms.ChoiceField(label=_("text_size"),widget=forms.Select(),required=True,initial='0.8',choices=text_size_vals)
#     title=forms.CharField(label=_("title"), required=True,initial="")
#     col_type=forms.ChoiceField(label=_("col_type"),widget=forms.Select(),required=True,initial='black',choices=color_type_vals)
#     diffusion_color=forms.ChoiceField(label=_("diffusion_color"),widget=forms.Select(),required=True,initial='red',choices=color_dif_vals)
#     color_reproduction=forms.ChoiceField(label=_("reproduction_color"),widget=forms.Select(),required=True,initial='darkgreen',choices=color_repro_vals)
#     color_mixture=forms.ChoiceField(label=_("mixture_color"),widget=forms.Select(),required=True,initial='purple',choices=color_mix_vals)
#     color_selection=forms.ChoiceField(label=_("selection_color"),widget=forms.Select(),required=True,initial='black',choices=color_selec_vals)
#     display_diffusion=forms.ChoiceField(label=_("display_diffusion"),widget=forms.RadioSelect(),required=True,initial='TRUE',choices=tuples_vals_TF)
#     display_reproduction=forms.ChoiceField(label=_("display_reproduction"),widget=forms.RadioSelect(),required=True,initial='TRUE',choices=tuples_vals_TF)
#     display_mixture=forms.ChoiceField(label=_("display_mixture"),widget=forms.RadioSelect(),required=True,initial='TRUE',choices=tuples_vals_TF)
#     display_selection=forms.ChoiceField(label=_("display _selection"),widget=forms.RadioSelect(),required=True,initial='TRUE',choices=tuples_vals_TF)
#     display_reproduction_type = forms.ChoiceField(label=_("display_reproduction_type"),widget=forms.Select(), required=True,initial='ALL', choices=tuples_vals_AN)
#     display_generation=forms.ChoiceField(label=_("display _generation"),widget=forms.RadioSelect(),required=True,initial='FALSE',choices=tuples_vals_TF)
#     germplasm_in = forms.ModelChoiceField(queryset=Germplasm.objects.all().order_by('germplasm_name'),required=True, widget=forms.Select(attrs={'style':'WIDTH: 200px;'}))
#     germplasm_out = forms.ModelChoiceField(queryset=Germplasm.objects.all().order_by('germplasm_name'),required=True, widget=forms.Select(attrs={'style':'WIDTH: 200px;'}))
#     person_in = forms.ModelChoiceField(queryset=Person.objects.all().order_by('short_name'),required=True)
#     person_out = forms.ModelChoiceField(queryset=Person.objects.all().order_by('short_name'),required=True)
#     year_in = forms.ModelChoiceField(queryset=Seed_lot.objects.values_list('date', flat=True).order_by('date').distinct(),required=True)
#     year_out = forms.ModelChoiceField(queryset=Seed_lot.objects.values_list('date', flat=True).order_by('date').distinct(),required=True)
#     project_in = forms.ModelChoiceField(queryset=Project.objects.all().order_by('project_name'),required=True)
#     project_out = forms.ModelChoiceField(queryset=Project.objects.all().order_by('project_name'),required=True)
#     SL_name = forms.ModelChoiceField(queryset=Seed_lot.objects.all().order_by('name'),required=True, widget=forms.Select(attrs={'style':'WIDTH: 200px;'}))
# =======
# # Definition of the parameters' form
# class NetworkForm(forms.Form):
#     """
#         A form that fit the parameters of the R package `shinemas2R <https://github.com/priviere/shinemas2R>`_ for the Network analysis
#         
#         **TODO: documentation need to be updated with the new version of shinemas2R**
#     """
#     legend_another_plot = forms.ChoiceField(label=_("legend_on_another_plot"),widget=forms.RadioSelect(),required=True,initial='TRUE',choices=tuples_vals_TF)
#     vertex_legend = forms.ChoiceField(label=_("vertex_legend"),widget=forms.RadioSelect(),required=True,initial='TRUE',choices=tuples_vals_TF)
#     edge_legend = forms.ChoiceField(label=_("edge_legend"),widget=forms.RadioSelect(),required=True,initial='TRUE',choices=tuples_vals_TF)
#     gender = forms.ChoiceField(label=_("gender"),widget=forms.RadioSelect(),required=True,initial='FALSE',choices=tuples_vals_TF)
#     hide_name = forms.ChoiceField(label=_("hide_name"),widget=forms.Select(),required=True,initial='ALL',choices=tuples_vals_AN)
#     fill_diffusion_gap=forms.ChoiceField(label=_("fill_diffusion_gap"),widget=forms.RadioSelect(),required=True,initial='FALSE',choices=tuples_vals_TF)
#     display_label=forms.ChoiceField(label=_("display_label"),widget=forms.RadioSelect(),required=True,initial='TRUE',choices=tuples_vals_TF)
#     text_size=forms.ChoiceField(label=_("text_size"),widget=forms.Select(),required=True,initial='0.8',choices=text_size_vals)
#     title=forms.CharField(label=_("title"), required=True,initial="")
#     col_type=forms.ChoiceField(label=_("col_type"),widget=forms.Select(),required=True,initial='black',choices=color_type_vals)
#     diffusion_color=forms.ChoiceField(label=_("diffusion_color"),widget=forms.Select(),required=True,initial='red',choices=color_dif_vals)
#     color_reproduction=forms.ChoiceField(label=_("reproduction_color"),widget=forms.Select(),required=True,initial='darkgreen',choices=color_repro_vals)
#     color_mixture=forms.ChoiceField(label=_("mixture_color"),widget=forms.Select(),required=True,initial='purple',choices=color_mix_vals)
#     color_selection=forms.ChoiceField(label=_("selection_color"),widget=forms.Select(),required=True,initial='black',choices=color_selec_vals)
#     display_diffusion=forms.ChoiceField(label=_("display_diffusion"),widget=forms.RadioSelect(),required=True,initial='TRUE',choices=tuples_vals_TF)
#     display_reproduction=forms.ChoiceField(label=_("display_reproduction"),widget=forms.RadioSelect(),required=True,initial='TRUE',choices=tuples_vals_TF)
#     display_mixture=forms.ChoiceField(label=_("display_mixture"),widget=forms.RadioSelect(),required=True,initial='TRUE',choices=tuples_vals_TF)
#     display_selection=forms.ChoiceField(label=_("display _selection"),widget=forms.RadioSelect(),required=True,initial='TRUE',choices=tuples_vals_TF)
#     display_reproduction_type = forms.ChoiceField(label=_("display_reproduction_type"),widget=forms.Select(), required=True,initial='ALL', choices=tuples_vals_AN)
#     display_generation=forms.ChoiceField(label=_("display _generation"),widget=forms.RadioSelect(),required=True,initial='FALSE',choices=tuples_vals_TF)
#     germplasm_in = forms.ModelChoiceField(queryset=Germplasm.objects.all().order_by('germplasm_name'),required=True, widget=forms.Select(attrs={'style':'WIDTH: 200px;'}))
#     germplasm_out = forms.ModelChoiceField(queryset=Germplasm.objects.all().order_by('germplasm_name'),required=True, widget=forms.Select(attrs={'style':'WIDTH: 200px;'}))
#     person_in = forms.ModelChoiceField(queryset=Person.objects.all().order_by('short_name'),required=True)
#     person_out = forms.ModelChoiceField(queryset=Person.objects.all().order_by('short_name'),required=True)
#     year_in = forms.ModelChoiceField(queryset=Seedlot.objects.values_list('date', flat=True).order_by('date').distinct(),required=True)
#     year_out = forms.ModelChoiceField(queryset=Seedlot.objects.values_list('date', flat=True).order_by('date').distinct(),required=True)
#     project_in = forms.ModelChoiceField(queryset=Project.objects.all().order_by('project_name'),required=True)
#     project_out = forms.ModelChoiceField(queryset=Project.objects.all().order_by('project_name'),required=True)
#     SL_name = forms.ModelChoiceField(queryset=Seedlot.objects.all().order_by('name'),required=True, widget=forms.Select(attrs={'style':'WIDTH: 200px;'}))
# >>>>>>> .merge-right.r679
# 
# def project_in_vals():
#     Project=apps.get_model('actors', 'Project')
#     project_in_vals=list(Project.objects.values_list('id','project_name'))
#     project_in_vals.insert(0,('','---------'))
#     return project_in_vals
# 
# def person_vals():
#     Person=apps.get_model('actors', 'Person')
#     person_vals=list(Person.objects.values_list('id','short_name'))
#     person_vals.insert(0,('','---------'))
#     return person_vals  
# 
# def repro_type_vals():
#     Reproduction_method=apps.get_model('network', 'Reproduction_method')
#     repro_type_vals=list(Reproduction_method.objects.values_list('id','reproduction_methode_name'))
#     repro_type_vals.insert(0,('','---------'))
#     return repro_type_vals
# 
# def year_in_vals():
#     Seed_lot = apps.get_model('entities', 'Seed_lot')
#     year_in_vals=list(Seed_lot.objects.values_list('date','date',flat=False).order_by('date').distinct())
#     year_in_vals.insert(0,('','---------'))
#     return year_in_vals
#     
# 
# def germplasm_type_vals():
#     Germplasm_type = apps.get_model('entities', 'Germplasm_type')
#     germplasm_type_vals=list(Germplasm_type.objects.values_list('id','germplasm_type'))
#     germplasm_type_vals.insert(0,('','---------'))
#     return germplasm_type_vals
# 
# def seed_lot_in():
#     Seed_lot = apps.get_model('entities', 'Seed_lot')
#     seed_lot_in=Seed_lot.objects.all().order_by('name')
#     return seed_lot_in
#   
# def germplasms_in():
#     Germplasm= apps.get_model('entities','Germplasm')
#     germplasms_in=Germplasm.objects.all().order_by('germplasm_name')
#     return germplasms_in 
# 
# #Class pour get.data:
# class DataForm(forms.Form):
#     query_type=forms.ChoiceField(label=_("query type :"),widget=forms.Select(attrs={'onchange':'getdata_query_lies(this,document.getElementsByName("#liste_germ_in"),document.getElementsByName("#liste_germ_out"),document.getElementById("id_year_in"),document.getElementById("id_year_out"),document.getElementById("id_project_in"),document.getElementById("id_project_out"),document.getElementById("id_relation_in"),document.getElementById("id_repro_type_in"),document.getElementById("id_germplasm_type_in"),document.getElementById("id_germplasm_type_out"),document.getElementById("liste_germ_in"),document.getElementById("liste_germ_out"),document.getElementById("liste_person_in"),document.getElementById("liste_person_out"),document.getElementById("liste_sl_in"),document.getElementById("liste_sl_out"),document.getElementById("liste_var_in"),document.getElementById("germ_in_liste"),document.getElementById("germ_out_liste"),document.getElementById("sl_in_liste"),document.getElementById("sl_out_liste"),document.getElementById("person_in_liste"),document.getElementById("person_out_liste"),document.getElementById("var_in_liste"),document.getElementById("id_filter_on"),document.getElementById("data_type_txt"),document.getElementById("data_type_menu"),document.getElementById("filter_on_txt"),document.getElementById("filter_on_menu"),document.getElementById("fill_gap_txt"),document.getElementById("fill_gap_menu"),document.getElementById("net_info_txt"),document.getElementById("net_info_menu"),document.getElementById("mdist_txt"),document.getElementById("mdist_menu"),document.getElementById("germ_in_txt"),document.getElementById("germ_in_menu"),document.getElementById("germ_out_txt"),document.getElementById("germ_out_menu"),document.getElementById("germ_type_in_txt"),document.getElementById("germ_type_in_menu"),document.getElementById("germ_type_out_txt"),document.getElementById("germ_type_out_menu"),document.getElementById("year_in_txt"),document.getElementById("year_in_menu"),document.getElementById("year_out_txt"),document.getElementById("year_out_menu"),document.getElementById("project_in_txt"),document.getElementById("project_in_menu"),document.getElementById("project_out_txt"),document.getElementById("project_out_menu"),document.getElementById("person_in_txt"),document.getElementById("person_in_menu"),document.getElementById("person_out_txt"),document.getElementById("person_out_menu"),document.getElementById("sl_in_txt"),document.getElementById("sl_in_menu"),document.getElementById("sl_out_txt"),document.getElementById("sl_out_menu"),document.getElementById("rel_in_txt"),document.getElementById("rel_in_menu"),document.getElementById("repro_in_txt"),document.getElementById("repro_in_menu"),document.getElementById("var_in_txt"),document.getElementById("var_in_menu"));'}),required=True,initial="person",choices=query_type_vals)
#     filter_on=forms.ChoiceField(label=_("filter on :"),widget=forms.Select(attrs={'onchange':"getdata_filter_lies(this,document.getElementById('liste_germ_in'),document.getElementById('liste_germ_out'),document.getElementById('liste_person_in'),document.getElementById('liste_person_out'),document.getElementById('liste_sl_in'),document.getElementById('liste_sl_out'),document.getElementById('liste_var_in'),document.getElementById('germ_in_liste'),document.getElementById('germ_out_liste'),document.getElementById('sl_in_liste'),document.getElementById('sl_out_liste'),document.getElementById('person_in_liste'),document.getElementById('person_out_liste'),document.getElementById('var_in_liste'),document.getElementById('query_type_menu'),document.getElementById('germ_in_txt'),document.getElementById('germ_in_menu'),document.getElementById('germ_out_txt'),document.getElementById('germ_out_menu'),document.getElementById('germ_type_in_txt'),document.getElementById('germ_type_in_menu'),document.getElementById('germ_type_out_txt'),document.getElementById('germ_type_out_menu'),document.getElementById('year_in_txt'),document.getElementById('year_in_menu'),document.getElementById('year_out_txt'),document.getElementById('year_out_menu'),document.getElementById('project_in_txt'),document.getElementById('project_in_menu'),document.getElementById('project_out_txt'),document.getElementById('project_out_menu'),document.getElementById('person_in_txt'),document.getElementById('person_in_menu'),document.getElementById('person_out_txt'),document.getElementById('person_out_menu'),document.getElementById('sl_in_txt'),document.getElementById('sl_in_menu'),document.getElementById('sl_out_txt'),document.getElementById('sl_out_menu'),document.getElementById('rel_in_txt'),document.getElementById('rel_in_menu'),document.getElementById('repro_in_txt'),document.getElementById('repro_in_menu'),document.getElementById('var_in_txt'),document.getElementById('var_in_menu'))"}),required=True,initial={""},choices=filter_on_vals)
#     fill_diff_gap=forms.ChoiceField(label=_("fill diffusion gap :"),widget=forms.RadioSelect(),required=True,initial="FALSE",choices=TF_vals)
#     network_info=forms.ChoiceField(label=_("network info :"),widget=forms.RadioSelect(),required=True,initial="TRUE",choices=TF_vals)
#     mdist=forms.ChoiceField(label=_("Mdist :"),widget=forms.RadioSelect(),required=True,initial="FALSE",choices=TF_vals)
#     germplasm_in=forms.ModelChoiceField(label=_("germplasm in:"),queryset=germplasms_in)
#     germplasm_out=forms.ModelChoiceField(label=_("germplasm out:"),queryset=germplasms_in)
#     germplasm_type_in=forms.ChoiceField(label=_("germplasm type in :"),widget=forms.SelectMultiple(attrs={'style':'WIDTH: 200px;HEIGHT:100px'}),initial={''},required=True,choices=germplasm_type_vals)
#     germplasm_type_out=forms.ChoiceField(label=_("germplasm type out :"),widget=forms.SelectMultiple(attrs={'style':'WIDTH: 200px;HEIGHT:100px'}),initial={''},required=True,choices=germplasm_type_vals)
#     person_in=forms.ChoiceField(label=_("person in :"),widget=forms.SelectMultiple(attrs={'style':'WIDTH: 200px;HEIGHT:100px'}),initial={''},required=True,choices=person_vals)
#     person_out=forms.ChoiceField(label=_("person out :"),widget=forms.SelectMultiple(attrs={'style':'WIDTH: 200px;HEIGHT:100px'}),initial={''},required=True,choices=person_vals)
#     year_in=forms.ChoiceField(label=_("year in :"),widget=forms.SelectMultiple(attrs={'style':'WIDTH: 200px;HEIGHT:100px'}),initial={''},required=True,choices=year_in_vals)
#     year_out=forms.ChoiceField(label=_("year out :"),widget=forms.SelectMultiple(attrs={'style':'WIDTH: 200px;HEIGHT:100px'}),initial={''},required=True,choices=year_in_vals)
#     project_in=forms.ChoiceField(label=_("project in :"),widget=forms.SelectMultiple(attrs={'style':'WIDTH: 200px;HEIGHT:100px'}),initial={''},required=True,choices=project_in_vals)
#     project_out=forms.ChoiceField(label=_("project out :"),widget=forms.SelectMultiple(attrs={'style':'WIDTH: 200px;HEIGHT:100px'}),initial={''},required=True,choices=project_in_vals)
#     seed_lots_in=forms.ModelChoiceField(label=_("seed lots in:"),queryset=seed_lot_in)
#     seed_lots_out=forms.ModelChoiceField(label=_("seed lots out:"),queryset=seed_lot_in)
#     relation_in=forms.ChoiceField(label=_("relation in :"),widget=forms.Select(),initial={''},required=True,choices=relation_in_vals)
#     repro_type_in=forms.ChoiceField(label=_("reproduction type in :"),widget=forms.Select(),initial={''},required=True,choices=repro_type_vals)
#     variables_in=forms.ModelChoiceField(label=_("variables in:"),queryset=variables_in)
#     data_type=forms.ChoiceField(label=_("data type :"),widget=forms.Select(attrs={'onchange':"getdata_data_lies(this,document.getElementById('liste_germ_in'),document.getElementById('liste_germ_out'),document.getElementById('liste_person_in'),document.getElementById('liste_person_out'),document.getElementById('liste_sl_in'),document.getElementById('liste_sl_out'),document.getElementById('liste_var_in'),document.getElementById('germ_in_liste'),document.getElementById('germ_out_liste'),document.getElementById('sl_in_liste'),document.getElementById('sl_out_liste'),document.getElementById('person_in_liste'),document.getElementById('person_out_liste'),document.getElementById('var_in_liste'),document.getElementById('id_filter_on'),document.getElementById('query_type_menu'),document.getElementById('filter_on_txt'),document.getElementById('filter_on_menu'),document.getElementById('germ_in_txt'),document.getElementById('germ_in_menu'),document.getElementById('germ_out_txt'),document.getElementById('germ_out_menu'),document.getElementById('germ_type_in_txt'),document.getElementById('germ_type_in_menu'),document.getElementById('germ_type_out_txt'),document.getElementById('germ_type_out_menu'),document.getElementById('year_in_txt'),document.getElementById('year_in_menu'),document.getElementById('year_out_txt'),document.getElementById('year_out_menu'),document.getElementById('project_in_txt'),document.getElementById('project_in_menu'),document.getElementById('project_out_txt'),document.getElementById('project_out_menu'),document.getElementById('person_in_txt'),document.getElementById('person_in_menu'),document.getElementById('person_out_txt'),document.getElementById('person_out_menu'),document.getElementById('sl_in_txt'),document.getElementById('sl_in_menu'),document.getElementById('sl_out_txt'),document.getElementById('sl_out_menu'),document.getElementById('rel_in_txt'),document.getElementById('rel_in_menu'),document.getElementById('repro_in_txt'),document.getElementById('repro_in_menu'),document.getElementById('var_in_txt'),document.getElementById('var_in_menu'))"}),required=True,initial={""},choices=data_type_vals)
# 
# ## GGPLOT GET THE NETWORK FUNCTION (QUAND L'UTILISATEUR A CHOISI DES DONNEES DE TYPE NETWORK)
# 
# #List of possible values for parameters
# 
# 
# 
# #Class pour get.ggplot quand query_type="network"
# class GgplotNetworkForm(forms.Form):
#     ggplot_type_vals=[('','---------'),('network-network','network-network'),('network-reproduction-sown','network-reproduction-sown'),('network-reproduction-harvested','network-reproduction-harvested'),
#               ('network-reproduction-positive-inter-selected','network-reproduction-positive-inter-selected'),('network-reproduction-negative-inter-selected','network-reproduction-negative-inter-selected'),
#               ('network-reproduction-crossed','network-reproduction-crossed'),('network-diffusion-sent','network-diffusion-sent'),('network-diffusion-received','network-diffusion-received'),
#               ('network-diffusion-relation','network-diffusion-relation'),('network-mixture','network-mixture'),('network-positive-intra-selected','network-positive-intra-selected')]
#     vertex_size_vals=[('1','1'),('3','3'),('5','5'),('8','8'),('10','10')]
#     vertex_color_vals=[('year','year'),('person','person'),('germplasm','germplasm')]
#     ggplot_display_vals=[('both','both'),('map','map'),('barplot','barplot')]
#     axis_vals=[('','---------'),('year','year'),('person','person'),('germplasm','germplasm')]
#     param_axis_vals=[('','---------'),('1','1'),('2','2'),('3','3'),('4','4'),('5','5')]
#     location_map_vals=[('france','france'),('world','world')]
#     pie_size_vals=[('0.2','0.2'),('0.4','0.4'),('0.5','0.5'),('0.7','0.7'),('1','1')]
#     hide_labels_parts_vals=[('all','all'),('year','year'),('person','person'),('germplasm','germplasm'),('year:germplasm','year:germplasm'),('person:germplasm','person:germplasm'),('person:year','person:year'),('','---------')]
#     labels_size_vals=[('1','1'),('3','3'),('5','5'),('8','8'),('10','10')]
#     #data_name=forms.CharField(label=_("name of your network:"),widget=forms.TextInput(attrs={'style':'width:150px;','required':"required"}),min_length=1,max_length=40)
#     ggplot_type=forms.ChoiceField(label=_("ggplot type :"), widget=forms.Select(attrs={'style':'margin-left: 50px',"onchange":"ggplotNetwork_type_lies(this,document.getElementById('id_x_axis'),document.getElementById('id_in_col'),document.getElementById('id_hide_labels_parts'),document.getElementById('id_ggplot_display'),document.getElementById('organise_sl_txt'),document.getElementById('organise_sl_menu'),document.getElementById('vertex_size_txt'),document.getElementById('vertex_size_menu'),document.getElementById('vertex_color_txt'),document.getElementById('vertex_color_menu'),document.getElementById('ggplot_display_txt'),document.getElementById('ggplot_display_menu'),document.getElementById('x_axis_txt'),document.getElementById('x_axis_menu'),document.getElementById('in_col_txt'),document.getElementById('in_col_menu'),document.getElementById('param_x_axis_txt'),document.getElementById('param_x_axis_menu'),document.getElementById('param_in_col_txt'),document.getElementById('param_in_col_menu'),document.getElementById('location_map_txt'),document.getElementById('location_map_menu'),document.getElementById('pie_size_txt'),document.getElementById('pie_size_menu'),document.getElementById('hide_labels_parts_txt'),document.getElementById('hide_labels_parts_menu'),document.getElementById('labels_size_txt'),document.getElementById('labels_size_menu'),document.getElementById('labels_generation_txt'),document.getElementById('labels_generation_menu'))"}),required=True,initial="---------",choices=ggplot_type_vals)
#     merge_g_and_s=forms.ChoiceField(label=_("merge g and s :"), widget=forms.RadioSelect(),required=True,initial="FALSE",choices=TF_vals)
#     organise_sl=forms.ChoiceField(label=_("organise sl :"), widget=forms.RadioSelect(),required=True,initial="FALSE",choices=TF_vals)
#     vertex_size=forms.ChoiceField(label=_("vertex size :"),widget=forms.Select(attrs={'style':'margin-left: 50px'}),required=True,initial="3",choices=vertex_size_vals)
#     vertex_color=forms.ChoiceField(label=_("vertex color :"),widget=forms.Select(attrs={'style':'margin-left: 50px'}),required=True,initial="year",choices=vertex_color_vals)
#     ggplot_display=forms.ChoiceField(label=_("ggplot display :"),widget=forms.Select(attrs={'style':'margin-left: 50px','onchange':"ggplotNetwork_display_lies(this,document.getElementById('x_axis_txt'),document.getElementById('x_axis_menu'),document.getElementById('in_col_txt'),document.getElementById('in_col_menu'),document.getElementById('param_x_axis_txt'),document.getElementById('param_x_axis_menu'),document.getElementById('param_in_col_txt'),document.getElementById('param_in_col_menu'),document.getElementById('location_map_txt'),document.getElementById('location_map_menu'),document.getElementById('pie_size_txt'),document.getElementById('pie_size_menu'))"}),required=True,initial="both",choices=ggplot_display_vals)
#     x_axis=forms.ChoiceField(label=_("x axis :"),widget=forms.Select(attrs={'style':'margin-left: 50px','onchange':"ggplot_x_axis_lies(this,document.getElementById('id_in_col'))"}),required=True,initial="",choices=axis_vals)
#     in_col=forms.ChoiceField(label=_("in col :"),widget=forms.Select(attrs={'style':'margin-left: 50px','onchange':"ggplot_in_col_lies(this,document.getElementById('id_x_axis'))"}),required=True,initial="",choices=axis_vals)
#     param_x_axis=forms.ChoiceField(label=_("nb parameter x axis :"),widget=forms.Select(attrs={'style':'margin-left: 50px'}),required=True,initial="",choices=param_axis_vals)
#     param_in_col=forms.ChoiceField(label=_("nb parameter in col :"),widget=forms.Select(attrs={'style':'margin-left: 50px'}),required=True,initial="",choices=param_axis_vals)
#     location_map=forms.ChoiceField(label=_("location map :"),widget=forms.Select(attrs={'style':'margin-left: 50px'}),required=True,initial="france",choices=location_map_vals)
#     pie_size=forms.ChoiceField(label=_("pie size :"),widget=forms.Select(attrs={'style':'margin-left: 50px'}),required=True,initial="0.5",choices=pie_size_vals)
#     hide_labels_parts=forms.ChoiceField(label=_("hide labels parts :"),widget=forms.Select(attrs={'style':'margin-left: 50px','onchange':"ggplotNetwork_hide_labels_lies(this,document.getElementById('id_ggplot_type'),document.getElementById('labels_size_txt'),document.getElementById('labels_size_menu'),document.getElementById('labels_generation_txt'),document.getElementById('labels_generation_menu'))"}),required=True,initial="all",choices=hide_labels_parts_vals)
#     labels_size=forms.ChoiceField(label=_("labels size :"),widget=forms.Select(attrs={'style':'margin-left: 50px'}),required=True,initial="3",choices=labels_size_vals)
#     #display_labels_sex=forms.ChoiceField(label=_("display labels sex :"),widget=forms.RadioSelect(),required=True,initial="TRUE",choices=TF_vals)
#     labels_generation=forms.ChoiceField(label=_("labels generation :"),widget=forms.RadioSelect(),required=True,initial="TRUE",choices=TF_vals)
#   
# ### GGPLOT GET THE PLOT FUNCTION (QUAND L'UTILISATEUR A CHOISI DES DONNEES DE TYPE DATA)
# 
# 
# #Class pour get.ggplot quand query_type="data-"
# class GgplotDataForm(forms.Form):
#     data_ggplot_type_vals=[('','---------'),('data-barplot','data-barplot'),('data-boxplot','data-boxplot'),('data-interaction','data-interaction'),('data-radar','data-radar'),('data-biplot','data-biplot'),('data-pie.on.network','data-pie.on.network'),('data-pie.on.map','data-pie.on.map')]
#     data_ggplot_on_vals=[('son','son'),('father','father')]
#     vertex_size_vals=[('1','1'),('3','3'),('5','5'),('8','8'),('10','10')]
#     vertex_color_vals=[('year','year'),('person','person'),('germplasm','germplasm')]
#     axis_vals=[('','---------'),('year','year'),('person','person'),('germplasm','germplasm')]
#     param_axis_vals=[('','---------'),('1','1'),('2','2'),('3','3'),('4','4'),('5','5')]
#     location_map_vals=[('france','france'),('world','world')]
#     pie_size_vals=[('0.2','0.2'),('0.4','0.4'),('0.5','0.5'),('0.7','0.7'),('1','1')]
#     hide_labels_parts_vals=[('all','all'),('year','year'),('person','person'),('germplasm','germplasm'),('year:germplasm','year:germplasm'),('person:germplasm','person:germplasm'),('person:year','person:year'),('','---------')]
#     labels_size_vals=[('1','1'),('3','3'),('5','5'),('8','8'),('10','10')]
#     data_correlated_group_vals=[]
#     data_vec_variables_vals=[]
# 
#     data_ggplot_type=forms.ChoiceField(label=_("ggplot type :"),widget=forms.Select(attrs={'style':'width:150px; margin-left:50px',"onchange":"ggplotData_type_lies(this,document.getElementById('data_labels_on_menu'),document.getElementById('data_labels_on_txt'),document.getElementById('data_hide_labels_parts_menu'),document.getElementById('data_hide_labels_parts_txt'),document.getElementById('data_location_map_menu'),document.getElementById('data_location_map_txt'),document.getElementById('data_pie_size_menu'),document.getElementById('data_pie_size_txt'),document.getElementById('data_vertex_size_menu'),document.getElementById('data_vertex_size_txt'),document.getElementById('data_vertex_color_menu'),document.getElementById('data_vertex_color_txt'),document.getElementById('data_organise_sl_menu'),document.getElementById('data_organise_sl_txt'),document.getElementById('data_x_axis_menu'),document.getElementById('data_x_axis_txt'),document.getElementById('data_in_col_menu'),document.getElementById('data_in_col_txt'),document.getElementById('data_param_x_axis_menu'),document.getElementById('data_param_x_axis_txt'),document.getElementById('data_param_in_col_menu'),document.getElementById('data_param_in_col_txt'),document.getElementById('id_data_hide_labels_parts'),document.getElementById('id_data_x_axis'),document.getElementById('id_data_in_col'))"}),required=True,initial="---------",choices=data_ggplot_type_vals)
#     data_correlated_group=forms.ChoiceField(label=_("correlated groups :"),widget=forms.Select(attrs={'style':'width:150px; margin-left:50px'}),required=True,initial="---------",choices=data_correlated_group_vals)
#     data_vec_variables=forms.ChoiceField(label=_("vec variables :"),widget=forms.Select(attrs={'style':'width:150px; margin-left:50px'}),required=True,initial="Variable(s) to display",choices=data_vec_variables_vals)
#     data_ggplot_on=forms.ChoiceField(label=_("ggplot on :"),widget=forms.Select(attrs={'style':'width:100px; margin-left:50px'}),required=True,initial="son",choices=data_ggplot_on_vals)
#     data_labels_on=forms.ChoiceField(label=_("labels on :"),widget=forms.Select(attrs={'style':'width:100px; margin-left:50px'}),required=True,initial="son",choices=data_ggplot_on_vals)
#     data_hide_labels_parts=forms.ChoiceField(label=_("hide labels parts :"),widget=forms.Select(attrs={'style':'width:100px; margin-left:50px',"onchange":"ggplotData_hide_labels_lies(this,getElementById('data_labels_size_menu'),getElementById('data_labels_size_txt'))"}),required=True,initial="all",choices=hide_labels_parts_vals)
#     data_labels_size=forms.ChoiceField(label=_("labels size :"),widget=forms.Select(attrs={"style":'width:100px; margin-left:50px'}),required=True,initial="3",choices=labels_size_vals)
#     data_location_map=forms.ChoiceField(label=_("location map :"),widget=forms.Select(attrs={"style":'width:100px; margin-left:50px;'}),required=True,initial="france",choices=location_map_vals)
#     data_pie_size=forms.ChoiceField(label=_("pie size :"),widget=forms.Select(attrs={"style":"width:100px; margin-left:50px"}),required=True,initial="0.5",choices=pie_size_vals)
#     data_vertex_size=forms.ChoiceField(label=_("vertex size :"),widget=forms.Select(attrs={'style':'width:100px; margin-left:50px'}),required=True,initial="3",choices=vertex_size_vals)
#     data_vertex_color=forms.ChoiceField(label=_("vertex color :"),widget=forms.Select(attrs={'style':'width:100px; margin-left:50px'}),required=True,initial="year",choices=vertex_color_vals)
#     data_organise_sl=forms.ChoiceField(label=_("organise sl :"), widget=forms.RadioSelect(),required=True,initial="FALSE",choices=TF_vals)
#     data_x_axis=forms.ChoiceField(label=_("x axis :"),widget=forms.Select(attrs={'style':'width:100px; margin-left:50px',"onchange":"ggplot_x_axis_lies(this,document.getElementById('id_data_in_col'))"}),required=True,initial="",choices=axis_vals)
#     data_in_col=forms.ChoiceField(label=_("in col :"),widget=forms.Select(attrs={'style':'width:100px; margin-left:50px','onchange':"ggplot_in_col_lies(this,document.getElementById('id_data_x_axis'))"}),required=True,initial="",choices=axis_vals)
#     data_param_x_axis=forms.ChoiceField(label=_("nb parameters x axis :"),widget=forms.Select(attrs={'style':'width:100px; margin-left:50px'}),required=True,initial="",choices=param_axis_vals)
#     data_param_in_col=forms.ChoiceField(label=_("nb parameters in col :"),widget=forms.Select(attrs={'style':'width:100px; margin-left:50px'}),required=True,initial="",choices=param_axis_vals)
#     
#     
# choices_col_to_display=[('person','person'),('germplasm','germplasm'),('year','year'),('block','block'),('X','X'),('Y','Y')]
# choices_table_type=[('','---------'),('mean','mean'),('mean.sd','mean.sd'),('mean.sd.cv','mean.sd.cv'),('summary','summary'),('raw','raw')]
# choices_table_on=[('son','son'),('father','father')]
# data_correlated_group_vals=[]
# data_vec_variables_vals=[]
#     
# class GgplotDataTable(forms.Form):
#     data_correlated_group=forms.ChoiceField(label=_("correlated groups :"),widget=forms.Select(attrs={'style':'width:150px; margin-left:50px'}),required=True,initial={''},choices=data_correlated_group_vals)
#     data_vec_variables=forms.ChoiceField(label=_("vec variables :"),widget=forms.Select(attrs={'style':'width:150px; margin-left:50px'}),required=True,initial="Variable(s) to display",choices=data_vec_variables_vals)
#     merge_g_and_s=forms.ChoiceField(label=_("merge g and s :"), widget=forms.RadioSelect(),required=True,initial="FALSE",choices=TF_vals)
#     data_table_type=forms.ChoiceField(label=_('table type:'),widget=forms.Select(attrs={'style':'width:150px; margin-left:50px'}),required=True,initial={''},choices=choices_table_type)
#     data_table_on=forms.ChoiceField(label=_('table on:'),widget=forms.Select(attrs={'style':'width:150px; margin-left:50px'}),required=True,initial={''},choices=choices_table_on)
#     table_nb_row=forms.IntegerField(label=_("number of rows:"),initial=0)
#     table_nb_col=forms.IntegerField(label=_("number of columns:"),initial=0)
#     table_nb_duplicated_rows=forms.IntegerField(label=_("number of duplicated rows:"),initial=10)
#     table_col_to_display=forms.ChoiceField(label=_("columns to display :"),widget=forms.SelectMultiple(attrs={'style':'width:150px; margin-left:50px'}),required=True,initial={''},choices=choices_col_to_display)
#     table_invert_row_col=forms.ChoiceField(label=_("invert row and col :"), widget=forms.RadioSelect(),required=True,initial="FALSE",choices=TF_vals)
#     
# #layout_figure_vals=[('matrix(1x1)','matrix(1x1)'),('matrix(2x2)','matrix(2x2)'),('matrix(2x3)','matrix(2x3)')]   
# layout_figure_vals=[('1','1'),('2','2'),('3','3'),('4','4')]
# colors_vals=[('white','white'),('deepskyblue','blue'),('mediumpurple1','purple'),('yellow','yellow'),('gray75','gray'),('chartreuse','green'),('hotpink1','pink'),('red','red')]
# width_figure_vals=[('0.5','0.5'),('0.8','0.8'),('1','1'),('1.3','1.3'),('1.5','1.5'),('2','2')]
# 
# 
# def set_field_html_name(cls, new_name):
#     """
#     This creates wrapper around the normal widget rendering, 
#     allowing for a custom field name (new_name).
#     """
#     old_render = cls.widget.render
#     def _widget_render_wrapper(name, value, attrs=None):
#         return old_render(new_name, value, attrs)
# 
#     cls.widget.render = _widget_render_wrapper
#     
# class GetPdfForm(forms.Form):
#     form_name=forms.CharField(label=_("form name:"),widget=forms.TextInput(attrs={'style':'width:150px;','required':"required"}),min_length=1,max_length=30)
#     title=forms.CharField(label=_("title:"),widget=forms.TextInput(attrs={'style':'width:150px;','required':"required"}),min_length=1,max_length=200)
#     authors=forms.CharField(label=_("authors:"),widget=forms.TextInput(attrs={'style':'width:150px;'}),min_length=1,max_length=500)
#     email=forms.CharField(label=_("email:"),widget=forms.TextInput(attrs={'style':'width:150px;'}),min_length=1,max_length=70)
#     date=forms.CharField(label=_("date:"),widget=forms.TextInput(attrs={'style':'width:150px;'}),min_length=1,max_length=50)
#     table_of_contents=forms.ChoiceField(label=_("table of contents :"),widget=forms.RadioSelect(),required=True,initial="FALSE",choices=TF_vals)
#     name_chapter=forms.CharField(label=_("name chapter:"),widget=forms.TextInput(attrs={'style':'width:150px;'}),min_length=1,max_length=50)
#     name_section=forms.CharField(label=_("name section:"),widget=forms.TextInput(attrs={'style':'width:150px;'}),min_length=1,max_length=50)
#     name_subsection=forms.CharField(label=_("name subsection:"),widget=forms.TextInput(attrs={'style':'width:150px;'}),min_length=1,max_length=50)
#     text_to_add=forms.CharField(label=_("text to add:"),widget=forms.TextInput(attrs={'style':'width:150px;'}),min_length=1,max_length=500)
#     include_pdf=forms.CharField(label=_("path of the pdf to include:"),widget=forms.TextInput(attrs={'style':'width:150px;'}),min_length=1,max_length=30)
#     input_tex=forms.CharField(label=_("path of the tex to insert:"),widget=forms.TextInput(attrs={'style':'width:150px;'}),min_length=1,max_length=30)
#     caption_table=forms.CharField(label=_("name of the table:"),widget=forms.TextInput(attrs={'style':'width:150px;'}),min_length=1,max_length=30)
#     landscape_table=forms.ChoiceField(label=_("landscape table :"),widget=forms.RadioSelect(),required=True,initial="TRUE",choices=TF_vals)
#     display_rownames=forms.ChoiceField(label=_("display_rownames :"),widget=forms.RadioSelect(),required=True,initial="TRUE",choices=TF_vals)
#     caption_figure=forms.CharField(label=_("name of the figure:"),widget=forms.TextInput(attrs={'style':'width:150px;'}),min_length=1,max_length=30)
#     nb_col_matrix=forms.ChoiceField(label=_("nb columns for the layout of the plots:"),widget=forms.RadioSelect(),required=True,initial="1",choices=layout_figure_vals)
#     nb_row_matrix=forms.ChoiceField(label=_("nb rows for the layout of the plots:"),widget=forms.RadioSelect(),required=True,initial="1",choices=layout_figure_vals)
#     width_figure=forms.ChoiceField(label=_("width of the figure(s):"),widget=forms.Select(attrs={'style':'width:150px;'}),initial="1",choices=width_figure_vals)
#     landscape_figure=forms.ChoiceField(label=_("landscape:"),widget=forms.RadioSelect(),required=True,initial="TRUE",choices=TF_vals)
#     compile_tex=forms.ChoiceField(label=_("compile the pdf :"),widget=forms.RadioSelect(),required=True,initial="TRUE",choices=TF_vals)
#     color_head_table=forms.ChoiceField(label=_("head color :"),widget=forms.Select(attrs={'style':'margin-left: 50px'}),required=True,initial="white",choices=colors_vals)
#     color_row_table=forms.ChoiceField(label=_("row color :"),widget=forms.Select(attrs={'style':'margin-left: 50px'}),required=True,initial="white",choices=colors_vals)
#     
# class GetPdfFormTemp(forms.Form):
#     form_name=forms.CharField(label=_("form name:"),widget=forms.TextInput(attrs={'style':'width:150px;','required':"required"}),min_length=1,max_length=30)
#     set_field_html_name(form_name, 'element_pdf')
#     title=forms.CharField(label=_("title:"),widget=forms.TextInput(attrs={'style':'width:150px;','required':"required"}),min_length=1,max_length=200)
#     set_field_html_name(title, 'element_pdf')
#     authors=forms.CharField(label=_("authors:"),widget=forms.TextInput(attrs={'style':'width:150px;','required':"required"}),min_length=1,max_length=500)
#     set_field_html_name(authors, 'element_pdf')
#     email=forms.CharField(label=_("email:"),widget=forms.TextInput(attrs={'style':'width:150px;','required':"required"}),min_length=1,max_length=70)
#     set_field_html_name(email, 'element_pdf')
#     date=forms.CharField(label=_("date:"),widget=forms.TextInput(attrs={'style':'width:150px;','required':"required"}),min_length=1,max_length=50)
#     set_field_html_name(date, 'element_pdf')    
#     table_of_contents=forms.ChoiceField(label=_("table of contents:"),widget=forms.Select(),required=True,initial="FALSE",choices=TF_vals)
#     set_field_html_name(table_of_contents, 'element_pdf')
#     name_chapter=forms.CharField(label=_("name chapter:"),widget=forms.TextInput(attrs={'style':'width:150px;','required':"required"}),min_length=1,max_length=50)
#     set_field_html_name(name_chapter, 'element_pdf')
#     name_section=forms.CharField(label=_("name section:"),widget=forms.TextInput(attrs={'style':'width:150px;','required':"required"}),min_length=1,max_length=50)
#     set_field_html_name(name_section, 'element_pdf')
#     name_subsection=forms.CharField(label=_("name subsection:"),widget=forms.TextInput(attrs={'style':'width:150px;','required':"required"}),min_length=1,max_length=50)
#     set_field_html_name(name_subsection, 'element_pdf')
#     text_to_add=forms.CharField(label=_("text to add:"),widget=forms.TextInput(attrs={'style':'width:150px;','required':"required"}),min_length=1,max_length=500)
#     set_field_html_name(text_to_add, 'element_pdf')
#     include_pdf=forms.CharField(label=_("path of the pdf to include:"),widget=forms.TextInput(attrs={'style':'width:150px;','required':"required"}),min_length=1,max_length=30)
#     set_field_html_name(include_pdf, 'element_pdf')
#     input_tex=forms.CharField(label=_("path of the tex to insert:"),widget=forms.TextInput(attrs={'style':'width:150px;','required':"required"}),min_length=1,max_length=30)
#     set_field_html_name(input_tex, 'element_pdf')
#     caption_table=forms.CharField(label=_("title of the table:"),widget=forms.TextInput(attrs={'style':'width:150px;','required':"required"}),min_length=1,max_length=30)
#     set_field_html_name(caption_table, 'element_pdf')
#     landscape_table=forms.ChoiceField(label=_("landscape table :"),widget=forms.RadioSelect(),required=True,initial="TRUE",choices=TF_vals)
#     display_rownames=forms.ChoiceField(label=_("display_rownames :"),widget=forms.RadioSelect(),required=True,initial="TRUE",choices=TF_vals)
#     caption_figure=forms.CharField(label=_("title of the figure:"),widget=forms.TextInput(attrs={'style':'width:150px;','required':"required"}),min_length=1,max_length=30)
#     set_field_html_name(caption_figure, 'element_pdf')
#     nb_col_matrix=forms.ChoiceField(label=_("nb columns for the layout of the figures:"),widget=forms.Select(),required=True,initial="1",choices=layout_figure_vals)
#     set_field_html_name(nb_col_matrix, 'element_pdf')
#     nb_row_matrix=forms.ChoiceField(label=_("nb rows for the layout of the figures:"),widget=forms.Select(),required=True,initial="1",choices=layout_figure_vals)
#     set_field_html_name(nb_row_matrix, 'element_pdf')
#     width_figure=forms.ChoiceField(label=_("width of the figures:"),widget=forms.Select(attrs={'style':'width:150px;'}),initial="1",choices=width_figure_vals)
#     set_field_html_name(width_figure, 'element_pdf')
#     landscape_figure=forms.ChoiceField(label=_("landscape:"),widget=forms.Select(),required=True,initial="FALSE",choices=TF_vals)
#     set_field_html_name(landscape_figure, 'element_pdf')
#     compile_tex=forms.ChoiceField(label=_("compile the pdf:"),widget=forms.RadioSelect(),required=True,initial="TRUE",choices=TF_vals)
#     color_head_table=forms.ChoiceField(label=_("head color:"),widget=forms.Select(attrs={'style':'margin-left: 50px'}),required=True,initial="white",choices=colors_vals)
#     set_field_html_name(color_head_table, 'element_pdf')
#     color_row_table=forms.ChoiceField(label=_("row color:"),widget=forms.Select(attrs={'style':'margin-left: 50px'}),required=True,initial="white",choices=colors_vals)
#     set_field_html_name(color_row_table, 'element_pdf')
#     
#     
#     
# tuples_vals_PP = [('Private','Private'),('Public','Public')]
# 
# class saveForm(forms.Form):
#     analysis_name = forms.CharField(widget=forms.TextInput(attrs={'size': '25','required':"required"}))
#     group_name=forms.CharField(widget=forms.TextInput(attrs={'size':'25', 'required':'required'}))
#     #choose_group = forms.ModelChoiceField(queryset=Group_Analysis.objects.all().order_by('group_name'),required=True, widget=forms.Select(attrs={'style':'WIDTH: 220px;'}))
#     comment = forms.CharField(widget=forms.Textarea(attrs={'cols': 30, 'rows': 7}))
#     statut = forms.ChoiceField(widget=forms.RadioSelect(), choices=tuples_vals_PP, initial='Private')

