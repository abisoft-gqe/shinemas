ALTER TABLE eppdata_env_pra_phe_raw_data ALTER COLUMN variable_id SET NOT NULL;
ALTER TABLE eppdata_env_pra_phe_raw_data ALTER COLUMN method_id SET NOT NULL;

ALTER TABLE eppdata_env_pra_phe_variable RENAME TO eppdata_variable;
ALTER TABLE eppdata_env_pra_phe_method RENAME TO eppdata_method;
ALTER TABLE eppdata_env_pra_phe_raw_data RENAME TO eppdata_raw_data;
ALTER TABLE eppdata_env_pra_phe_raw_data_relation RENAME TO eppdata_raw_data_relation;
ALTER TABLE eppdata_env_pra_phe_raw_data_seed_lot RENAME TO eppdata_raw_data_seed_lot;
ALTER TABLE eppdata_env_pra_phe_value RENAME TO eppdata_value;

ALTER TABLE eppdata_raw_data_relation RENAME COLUMN env_pra_phe_raw_data_id TO raw_data_id;
ALTER TABLE eppdata_raw_data_seed_lot RENAME COLUMN env_pra_phe_raw_data_id TO raw_data_id;

ALTER INDEX eppdata_env_pra_phe_method_classe_id RENAME TO eppdata_method_classe_id;
ALTER SEQUENCE eppdata_env_pra_phe_raw_data_relation_id_seq RENAME TO eppdata_raw_data_relation_id_seq;
ALTER INDEX eppdata_env_pra_phe_value_rawdata_id RENAME TO eppdata_value_rawdata_id;
ALTER INDEX eppdata_env_pra_phe_raw_data_relation_pkey RENAME TO eppdata_raw_data_relation_pkey;
ALTER SEQUENCE eppdata_env_pra_phe_variable_id_seq RENAME TO eppdata_variable_id_seq;
ALTER INDEX eppdata_env_pra_phe_raw_data_relation_relation_id RENAME TO eppdata_raw_data_relation_relation_id;
ALTER INDEX eppdata_env_pra_phe_variable_pkey RENAME TO eppdata_variable_pkey;
ALTER SEQUENCE eppdata_env_pra_phe_method_id_seq RENAME TO eppdata_method_id_seq;
ALTER INDEX eppdata_env_pra_phe_raw_data_seed_l_env_pra_phe_raw_data_id_key RENAME TO eppdata_raw_data_seed_l_raw_data_id_key;
ALTER INDEX eppdata_env_pra_phe_method_method_name_key RENAME TO eppdata_method_method_name_key;
ALTER INDEX eppdata_env_pra_phe_raw_data_seed_lot_env_pra_phe_raw_data_id RENAME TO eppdata_raw_data_seed_lot_raw_data_id;
ALTER INDEX eppdata_env_pra_phe_method_person_id RENAME TO eppdata_method_person_id;
ALTER SEQUENCE eppdata_env_pra_phe_raw_data_seed_lot_id_seq RENAME TO eppdata_raw_data_seed_lot_id_seq;
ALTER INDEX eppdata_env_pra_phe_method_pkey RENAME TO eppdata_method_pkey;
ALTER INDEX eppdata_env_pra_phe_raw_data_seed_lot_pkey RENAME TO eppdata_raw_data_seed_lot_pkey;
ALTER SEQUENCE eppdata_env_pra_phe_raw_data_id_seq RENAME TO eppdata_raw_data_id_seq;
ALTER INDEX eppdata_env_pra_phe_raw_data_seed_lot_seed_lot_id RENAME TO eppdata_raw_data_seed_lot_seed_lot_id;
ALTER INDEX eppdata_env_pra_phe_raw_data_method_id RENAME TO eppdata_raw_data_method_id;
ALTER INDEX eppdata_env_pra_phe_raw_data_variable_id RENAME TO eppdata_raw_data_variable_id;
ALTER INDEX eppdata_env_pra_phe_raw_data_pkey RENAME TO eppdata_raw_data_pkey;
ALTER INDEX eppdata_env_pra_phe_value_envpraphemethod_id RENAME TO eppdata_value_envpraphemethod_id;
ALTER INDEX eppdata_env_pra_phe_raw_data_relati_env_pra_phe_raw_data_id_key RENAME TO eppdata_raw_data_relati_raw_data_id_key;
ALTER SEQUENCE eppdata_env_pra_phe_value_id_seq RENAME TO eppdata_value_id_seq;
ALTER INDEX eppdata_env_pra_phe_raw_data_relation_env_pra_phe_raw_data_id RENAME TO eppdata_raw_data_relation_raw_data_id;
ALTER INDEX eppdata_env_pra_phe_value_pkey RENAME TO eppdata_value_pkey;

DROP TABLE actors_organization_person;
DROP TABLE actors_organization;

DROP TABLE actors_activity_person;
DROP TABLE actors_activity;

DROP TABLE myuser_monmodele;
DROP TABLE myuser_userprofile;
