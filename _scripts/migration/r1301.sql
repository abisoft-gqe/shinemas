CREATE TABLE "entities_species" (
    "id" serial NOT NULL PRIMARY KEY,
    "species" varchar(100) NOT NULL UNIQUE,
    "latin_name" varchar(100),
    "description" text
);
ALTER TABLE entities_germplasm ADD species_id integer REFERENCES "entities_species" ("id");
ALTER TABLE entities_germplasm ADD CONSTRAINT entities_germplasm_species_id_germplasm_name UNIQUE ("species_id","germplasm_name");

ALTER TABLE actors_person DROP COLUMN short_nickname;
ALTER TABLE actors_person DROP COLUMN nick_name;
ALTER TABLE actors_person DROP COLUMN born_year;
ALTER TABLE actors_person ADD birth_date date;
ALTER TABLE entities_germplasm DROP COLUMN germplasm_short_name;

ALTER TABLE eppdata_env_pra_phe_raw_data ADD "date" date;