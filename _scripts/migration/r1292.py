
"""
This script migrate database Project are not linked anymore to SeedLot but to a relation
we need to change all data

Before to run the script you need to create the new table between network_relation and actors_project table :
CREATE TABLE "network_relation_project" (
    "id" serial NOT NULL PRIMARY KEY,
    "relation_id" integer NOT NULL,
    "project_id" integer NOT NULL REFERENCES "actors_project" ("id") DEFERRABLE INITIALLY DEFERRED,
    UNIQUE ("relation_id", "project_id")
)
;


After runing the script you need to delete the many to many table between entities_seed_lot and actors_project table :
DROP TABLE entities_seed_lot_project ;
"""

from network.models import Relation
from actors.models import Project

relationset = Relation.objects.all()

for relation in relationset:
    father_projects = set(Project.objects.raw('SELECT actors_project.id, actors_project.project_name, actors_project.start_date, actors_project.end_date FROM actors_project INNER JOIN entities_seed_lot_project ON actors_project.id = entities_seed_lot_project.project_id WHERE entities_seed_lot_project.seed_lot_id = {0}'.format(relation.seed_lot_father.id)))
    son_projects = set(Project.objects.raw('SELECT actors_project.id, actors_project.project_name, actors_project.start_date, actors_project.end_date FROM actors_project INNER JOIN entities_seed_lot_project ON actors_project.id = entities_seed_lot_project.project_id WHERE entities_seed_lot_project.seed_lot_id = {0}'.format(relation.seed_lot_son.id)))
    common = father_projects.intersection(son_projects)
    for p in common :
        relation.project.add(p)