from entities.models import Germplasm
import networkx as nx
import re
from Levenshtein import distance as lev

qs1 = Germplasm.objects.all()
qs2 = Germplasm.objects.all()

G = nx.Graph()

for gp1 in qs1 :
    if not gp1.name.startswith('inconnu') and not gp1.name.startswith('Inconnu') and not gp1.name.startswith('Blank') and not gp1.name.startswith('NOT CROSSED') and not gp1.name.startswith('Mélange') and not gp1.name.startswith('mélange') and not re.match(r'^C\d+', gp1.name)  and not re.match(r'^A\d+', gp1.name)  and not re.match(r'^M[\d\.]+', gp1.name):
        for gp2 in qs2 :
            if lev(gp1.name, gp2.name) <= 2  and gp1.id != gp2.id :
                G.add_node(gp1.name)
                G.add_node(gp2.name)
                G.add_edge(gp1.name, gp2.name)
                #print("there is similarity between {0} and {1}".format(gp1.name, gp2.name))
                
for subgraph in list(nx.connected_components(G)) :
    print(', '.join(subgraph))