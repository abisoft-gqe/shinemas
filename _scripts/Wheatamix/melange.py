import sys
import csv
def main():
 csvinput= open(sys.argv[1],'r')
 header = []
 h0 = csv.reader(csvinput, delimiter = '\t').next()
 if 'type' in h0:
  index_var_type = h0.index( 'type' )

 h =['project','id_seed_lot','etiquette','split','quantity','germplasm','event_year','variete'	,'bloc'	,'type'	,'rdtnet',	'h2o'	,'ps'	,'pmg'	,'proteines',	'pieds.m2'	,'date_epi',	'epis.m2',	'coeff_tall',	'hauteur'
,'maladies.global',	'psce_septo'	,'psce_rj',	'psce_rb',	'psce_oidium'	,'freq_septo_f1',	'freq_septo_f2'	,'freq_septo_f3',	'freq_rj_f1',	'freq_rj_f2',	'freq_rj_f3'	,'freq_rb_f1',	'freq_rb_f2',	'freq_rb_f3'	
,'freq_oidium_f1'	,'freq_oidium_f2',	'freq_oidium_f3'] #entete du fichier


 writer = csv.writer(open(sys.argv[2], 'w'),delimiter = '\t')
 writer.writerow(h)
 prec1 = ''
 prec2 = ''
 for row in csv.reader(csvinput, delimiter = '\t'):
     i=0
     if row[index_var_type] == 'Melange': #tester si le type (de la variété) est bien 'mélange'
    #récupérer les valeurs précédentes, les comparer aux courantes
      if (prec1 == row[0] and prec2 == row[2]):
        i+=1
      prec1 = row[0]
      prec2 = row[2]
      sl = row[2].split('+')
      row[2] = row[2] + "-" + row[0][0:3] + "-" + str(i) #incrémenter les noms des mélanges qui se répètent
      for term in sl:
             term1 =   term + "_"  + row[0][0:3] + "_" + sys.argv[4]
             val = [sys.argv[3], term1,'',0,'',row[2],sys.argv[4]]
             [val.append(x) for x in row[2:len(row)]]
             writer.writerow(val)
        

if __name__ == '__main__':
  if len(sys.argv) == 5:
    main()   
  else:
    print('Nombre d"arguments systeme incorrect')
       
