from eppdata.models import Variable
import networkx as nx
import re
from Levenshtein import distance as lev

qs1 = Variable.objects.all()
qs2 = Variable.objects.all()

G = nx.Graph()

for v1 in qs1 :
    if not v1.name.startswith('DEAP_MOULON') :
        for v2 in qs2 :
            if lev(v1.name, v2.name) <= 2  and v1.id != v2.id :
                G.add_node(v1.name)
                G.add_node(v2.name)
                G.add_edge(v1.name, v2.name)
                #print("there is similarity between {0} and {1}".format(v1.name, v2.name))
                
for subgraph in list(nx.connected_components(G)) :
    print(', '.join(subgraph))