#!/bin/bash

# Usage: ./<scriptfilename> <software> <version>

SOFTWARE=$1
VERSION=$2
IP=$(ip addr | grep 'scope global mngtmpaddr dynamic' |sed -e's/^.*inet6 \([^ ]*\)\/.*$/\1/;t;d')

apt-get update
apt-get install -y python3-pip postgresql-client libpq-dev libapache2-mod-wsgi-py3
a2enmod proxy
pip3 --proxy=http://moulon-6.moulon.inra.fr:3128/ install virtualenv
cd /usr/local/
mkdir python-virtualenv
cd python-virtualenv/
virtualenv -p python3 /usr/local/python-virtualenv/$SOFTWARE$VERSION/
/usr/local/python-virtualenv/$SOFTWARE$VERSION/bin/pip install -r /var/www/requirements.txt
mkdir /var/store
mkdir /var/store/$SOFTWARE
cd /etc/apache2/sites-available
echo '<VirtualHost ['$IP']:80>
        ServerName '$HOSTNAME'.moulon.inra.fr
        ServerAlias '$HOSTNAME'.moulon.inra.fr '$HOSTNAME'

        ProxyRequests On

WSGIDaemonProcess '$SOFTWARE' python-home=/usr/local/python-virtualenv/'$SOFTWARE$VERSION'/ python-path=/var/www/
WSGIProcessGroup '$SOFTWARE'

WSGIScriptAlias / /usr/local/wsgi-apache/wsgi.py process-group='$SOFTWARE'


<Directory /usr/local/wsgi-apache/>
	<Files "wsgi.py">
	Require all granted
	</Files>
</Directory>

</VirtualHost>' > $SOFTWARE.conf

ln -s /etc/apache2/sites-available/$SOFTWARE.conf /etc/apache2/sites-enabled/$SOFTWARE.conf

cd /usr/local/
mkdir wsgi-apache
cd wsgi-apache
echo 'import os
import sys

path = '\''/var/www/'\''
if path not in sys.path:
    sys.path.append(path)

os.environ["DJANGO_SETTINGS_MODULE"] = '\''settings'\''
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()' > wsgi.py
chmod +x wsgi.py

chown -R www-data.www-data /var/www/
chown -R www-data.www-data  /usr/local/wsgi-apache
chown -R www-data.www-data /var/store

echo 'You can now edit config.py file'
