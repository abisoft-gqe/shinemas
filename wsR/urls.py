# -*- coding: utf-8 -*-

from django.urls import path
from wsR import views


urlpatterns = [
            path('locations/', views.Locations.as_view()),
            path('variables/', views.Variables.as_view()),
            path('germplasm/', views.Germplasms.as_view()),
            path('germplasm_types/', views.GermplasmTypes.as_view()),
            path('projects/', views.Projects.as_view()),
            path('species/', views.SpeciesAPI.as_view()),
            path('data_agro/', views.data_agro), 
            path('data_network_unipart_seed_lots/', views.data_network_unipart_seed_lots), 
            path('data_agro_sr/', views.data_agro_sr), 
            path('data_agro_ha/', views.data_agro_ha), 
            path('data_agro_mixture/', views.data_agro_mixture), 

]
