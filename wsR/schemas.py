from rest_framework.schemas import openapi
import yaml

class AutoDocstringSchema(openapi.AutoSchema):
    @property
    def documentation(self):
        if not hasattr(self, '_documentation'):
            try:
                self._documentation = yaml.safe_load(self.view.__doc__)
            except yaml.scanner.ScannerError:
                self._documentation = {}
        return self._documentation

    def get_components(self, path, method):
        components = super().get_components(path, method)
        doc_components = self.documentation.get('components', {})
        components.update(doc_components)
        return components

    def get_operation(self, path, method):
        operation = super().get_operation( path, method)
        doc_operation = self.documentation.get(method.lower(), {})
        operation.update(doc_operation)
        return operation


class SpecificSchema(openapi.AutoSchema):
    tags=['SHiNemaS\' API',]
    parameters = [{
                "name": "token",
                "in": "query",
                "required": True,
                "description": "User authentication token",
                'schema': {
                    'type': 'string',
                }},
                {
                "name": "apply_on",
                "in": "query",
                "required": False,
                "description": "A value child or parent. This value means the filters germplasm, germplasm_type, location, species will apply to the parent seed lot or to the child seed lot",
                'schema': {
                    'type': 'string',
                }},
                {
                "name": "germplasm",
                "in": "query",
                "required": False,
                "description": "A list of germplasm defined in SHiNeMaS, example of value : ['blé-du-lot', 'rouge-de-bordeaux']",
                'schema': {
                    'type': 'string',
                }},
                {
                "name": "germplasm_type",
                "in": "query",
                "required": False,
                "description": "A list of germplasm type defined in SHiNeMaS, example of value : ['Population','Landrace']",
                'schema': {
                    'type': 'string',
                }},
                {
                "name": "location",
                "in": "query",
                "required": False,
                "description": "A list of location defined in SHiNeMaS and described by the short_name field, example of value : ['MLN', 'CLM', 'SMH']",
                'schema': {
                    'type': 'string',
                }},
                
                {
                "name": "project",
                "in": "query",
                "required": False,
                "description": "A list of projects defined in SHiNeMaS, example of value : ['PPB','PPB-Mixtures']",
                'schema': {
                    'type': 'string',
                }},
                {
                "name": "species",
                "in": "query",
                "required": False,
                "description": "A list of species defined in SHiNeMaS, example of value : ['wheat', 'maize']",
                'schema': {
                    'type': 'string',
                }},
                ]
             
    def __init__(self):
        super().__init__(tags=self.tags)
        
    def get_path_parameters(self, path, method):
        return self.parameters
    # def get_responses(self, path, method):
    #     return self.responses

class GlobalSchema(openapi.AutoSchema):
    tags=['SHiNemaS\' API',]
    parameters = [{
                "name": "token",
                "in": "query",
                "required": True,
                "description": "User authentication token",
                'schema': {
                    'type': 'string',
                }},
                {
                "name": "apply_on",
                "in": "query",
                "required": False,
                "description": "A value child or parent. This value means the filters germplasm, germplasm_type, location, species will apply to the parent seed lot or to the child seed lot",
                'schema': {
                    'type': 'string',
                }},
                {
                "name": "germplasm",
                "in": "query",
                "required": False,
                "description": "A list of germplasm defined in SHiNeMaS, example of value : ['blé-du-lot', 'rouge-de-bordeaux']",
                'schema': {
                    'type': 'string',
                }},
                {
                "name": "germplasm_type",
                "in": "query",
                "required": False,
                "description": "A list of germplasm type defined in SHiNeMaS, example of value : ['Population','Landrace']",
                'schema': {
                    'type': 'string',
                }},
                {
                "name": "location",
                "in": "query",
                "required": False,
                "description": "A list of location defined in SHiNeMaS and described by the short_name field, example of value : ['MLN', 'CLM', 'SMH']",
                'schema': {
                    'type': 'string',
                }},
                
                {
                "name": "project",
                "in": "query",
                "required": False,
                "description": "A list of projects defined in SHiNeMaS, example of value : ['PPB','PPB-Mixtures']",
                'schema': {
                    'type': 'string',
                }},
                {
                "name": "species",
                "in": "query",
                "required": False,
                "description": "A list of species defined in SHiNeMaS, example of value : ['wheat', 'maize']",
                'schema': {
                    'type': 'string',
                }},
                {
                "name": "relation_type",
                "in": "query",
                "required": False,
                "description": "A list of value regarding the relation types defined in SHiNeMaS. Accepted values are 'cross', 'diffusion', 'merge', 'mixture', 'reproduction', 'selection'",
                'schema': {
                    'type': 'string',
                }},
                {
                "name": "variables",
                "in": "query",
                "required": False,
                "description": "A list of variables to fetch data",
                'schema': {
                    'type': 'string',
                }},
                {
                "name": "year",
                "in": "query",
                "required": False,
                "description": "A list of years. The year refers to the event year of a mixture, a diffusion, a merge, a cross, or a selection. \
                                    In a reproduction relation the year refers to the sowing year if apply_on is set to parent, the harvesting year if apply_on is set to child. Example of value : [2011, 2012, 2013]",
                'schema': {
                    'type': 'string',
                }}
    ]
    
    def __init__(self):
        super().__init__(tags=self.tags)
        
    def get_path_parameters(self, path, method):
        return self.parameters
