import datetime

from django.db.models import Prefetch
from django.db.models import Q, F
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from rest_framework.authtoken.models import Token
from rest_framework import authentication
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes, schema
from rest_framework.permissions import AllowAny
from rest_framework.status import (
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_401_UNAUTHORIZED
)
from rest_framework.response import Response

from network.models import Relation
from eppdata.models import Variable
from eppdata.serializers import VariableSerializer
from entities.models import Germplasm, GermplasmType, Species
from entities.serializers import GermplasmSerializer, GermplasmTypeSerializer, SpeciesSerializer
from actors.models import Project, Location
from actors.serializers import LocationSerializer, ProjectSerializer

from .schemas import GlobalSchema, SpecificSchema, AutoDocstringSchema


class APIAuthentication():
    
    success = False
    message = None
    token = None
    status = None
    
    def __init__(self, token):
        self.token = token
       
    def authenticate(self):
        if self.token == None :
            self.message = "Missing token"
            self.status = HTTP_401_UNAUTHORIZED
            return
        try:
            db_token = Token.objects.get(key=self.token)
        except Token.DoesNotExist:
            self.message = "Invalid token"
            self.status = HTTP_401_UNAUTHORIZED
            return
        else :
            expiration_date = db_token.created + datetime.timedelta(days=settings.AUTH_TOKEN_LIFE)
            has_expired = datetime.datetime.now() > expiration_date
            if has_expired :
                self.message = "Token has expired, please create a new one"
                self.status = HTTP_401_UNAUTHORIZED
            else :
                self.success = True
                self.status = HTTP_200_OK
            return

        

def get_filters(request):
    
    """
    Pour chaque filtre, crée une liste des objets correspondants aux valeurs saisies dans l'url 
    les listes de chacun des filtres sont ensuite ajouter a un dictionnaire qui associera la liste au nom du filtre
    si aucun objet ne correspond on ajoute simplement la chaine de caractere 
    on peut filtrer sur sown_year, harvested_year, event_year, species, project, relation_type, location, germplasm, germplasm_type 
    pour filtrer dans l'url, la valeur associee au filtre doit etre une liste meme s'il n'y a qu'une seule valeur
    """
    FILTERS = ('apply_on', 'species', 'germplasm', 'location', 'year', 'project', 'relation_type', 'germplasm_type', 'variables')
    OTHER_FIELDS = ('page', 'page_size', 'token')
    final_dico = {}
    print('get_filters')
    warning_messages = []
    for field in request.GET.keys() :
        if field not in FILTERS and field not in OTHER_FIELDS :
            warning_messages.append("You have a problem with filter {0}. The filters that you can use are apply_on, species, germplasm, location, event_year, project, relation_type, germplasm_type, variables, sown_year and harvested_year. To use them you need to add &filtername=[values,values] to your url for each filter that you want. Harvested_year and sown_year need to be used with the filter &relation_type=[reproduction].".format(field))
        elif field in FILTERS :
            if field == 'apply_on':
                final_dico[field] = request.GET[field]
            else :
                if request.GET[field][0] == '[' and request.GET[field][-1] == ']' :
                    final_dico[field] = build_filter_list(field, request.GET[field])
                else :
                    warning_messages.append("Your filter {0} for {1} must be in this format : &filtername=[values,values]".format(request.GET[field],field))
                
    
    final_dico["warnings"] = warning_messages
    return final_dico
    
    
def build_filter_list(field, s):
    if field == 'variables':
        liste_v =  []
        #liste_m =  []
        for var in s[1:-1].split(',') :
            try :
                var_name, var_type = var.split('$')
                v = Variable.objects.get(name=var_name, type=var_type)
                liste_v.append(v)
            except :
                print("Variable {0} not found".format(var))
        return liste_v
    else :
        filters = list(map(str.strip, s[1:len(s)-1].split(',')))
        if field in ('year', 'relation_type'):
            return  filters
        elif field == 'germplasm':
            return  list(Germplasm.objects.filter(idgermplasm__in=filters))
        elif field == 'species':
            return list(Species.objects.filter(species__in=filters))
        elif field == 'location':
            return  list(Location.objects.filter(short_name__in=filters))
        elif field == 'project':
            return  list(Project.objects.filter(project_name__in=filters))
        elif field == 'germplasm_type':
            return  list(GermplasmType.objects.filter(germplasm_type__in=filters))
        return None

def entities_filters(relations, dico_filtres):
    if 'apply_on' in dico_filtres .keys() and dico_filtres['apply_on'] == 'child' :
        apply_on = 'seed_lot_son'
    else :
        apply_on = 'seed_lot_father'
    
    filters_dict = {}
    if 'location' in dico_filtres.keys() :
        filters_dict[apply_on+'__location__short_name__in'] = dico_filtres['location']
    if 'germplasm' in dico_filtres.keys() :
        filters_dict[apply_on+'__germplasm__idgermplasm__in'] = dico_filtres['germplasm']
    if 'germplasm_type' in dico_filtres.keys() :
        filters_dict[apply_on+'__germplasm__germplasm_type__germplasm_type__in'] = dico_filtres['germplasm_type']
    if 'species' in dico_filtres.keys() :
        filters_dict[apply_on+'__germplasm__species__species__in'] = dico_filtres['germplasm_type']
    
    relations = relations.filter(**filters_dict)
    return relations

def relations_filters(relations, dico_filtres):
    qrel = Q()
    if 'relation_type' in dico_filtres.keys():
        print("filtering regarding relations type")
        if "selection" in dico_filtres['relation_type'] :
            if "year" in dico_filtres.keys():
                qrel = qrel | Q(selection__isnull=False, selection__date__in=dico_filtres['year'])
            else :
                qrel = qrel | Q(selection__isnull=False)
        if "reproduction" in dico_filtres['relation_type'] :
            if "year" in dico_filtres.keys():
                if dico_filtres['apply_on'] == 'child' :
                    qrel = qrel | Q(reproduction__isnull=False, reproduction__end_date__in=dico_filtres['year'])
                elif dico_filtres['apply_on'] == 'parent' :
                    qrel = qrel | Q(reproduction__isnull=False, reproduction__start_date__in=dico_filtres['year'])
            else :
                qrel = qrel | Q(reproduction__isnull=False)
        if "mixture" in dico_filtres['relation_type'] :
            if "year" in dico_filtres.keys():
                qrel = qrel | (Q(mixture__isnull=False, mixture__date__in=dico_filtres['year']) & ~Q(seed_lot_father__germplasm=F('seed_lot_son__germplasm')))
            else :
                qrel = qrel | (Q(mixture__isnull=False) & ~Q(seed_lot_father__germplasm=F('seed_lot_son__germplasm')))
        if "merge" in dico_filtres['relation_type'] :
            if "year" in dico_filtres.keys():
                qrel = qrel | (Q(mixture__isnull=False, mixture__date__in=dico_filtres['year']) & Q(seed_lot_father__germplasm=F('seed_lot_son__germplasm')))
            else :
                qrel = qrel | (Q(mixture__isnull=False) & Q(seed_lot_father__germplasm=F('seed_lot_son__germplasm')))
        if "diffusion"  in dico_filtres['relation_type'] :
            if "year" in dico_filtres.keys():
                qrel = qrel | Q(diffusion__isnull=False, diffusion__date__in=dico_filtres['year'])
            else :
                qrel = qrel | Q(diffusion__isnull=False)
        if "cross" in dico_filtres['relation_type']:
            if "year" in dico_filtres.keys():
                if dico_filtres['apply_on'] == 'child' :
                    qrel = qrel | Q(reproduction__isnull=False, reproduction__end_date__in=dico_filtres['year'])
                elif dico_filtres['apply_on'] == 'parent' :
                    qrel = qrel | Q(reproduction__isnull=False, reproduction__start_date__in=dico_filtres['year'])
            else :
                qrel = qrel | (Q(reproduction__isnull=False) & ~Q(is_male='X') )
    relations = relations.filter(qrel)
    return relations

def variables_filters(relations, dico_filtres):
    if 'variables' in dico_filtres.keys() :
        relations = relations.filter(rawdatas__variable__in=dico_filtres['variables'])
    return relations

def api_filters(relations, dico_filtres):
    print(dico_filtres)
    if 'apply_on' in dico_filtres.keys() :
        relations = entities_filters(relations, dico_filtres)
        if 'project' in dico_filtres.keys() :
            relations = relations.filter(project__project_name__in=dico_filtres['project'])
        relations = relations_filters(relations, dico_filtres)
        relations = variables_filters(relations, dico_filtres)
    else :
        pass
        #raise an error
    return relations

class Variables(APIView):
    '''
        get:
          description: return the list of Variables defined in SHiNeMaS
          parameters:
            - name: token
              in: query
              schema:
                type: string
              description: authentication token
              required: true
          responses:
            '200':
              description: array of Variables objects
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      name:
                        type: string
                        description: the name of this Variable
                      type:
                        type: string
                        description: the type of this variable
    '''
    authentication_classes = [authentication.TokenAuthentication,]
    permission_classes = [AllowAny,]
    schema = AutoDocstringSchema()
    @csrf_exempt
    def get(self, request, token=None):
        auth = APIAuthentication(request.GET.get("token"))
        auth.authenticate()
        if auth.success == False:
            return Response({'error': auth.message},status=auth.status) 
        else :
            variables = Variable.objects.all()
            variables_serialized = VariableSerializer(variables, many=True)
            return Response(variables_serialized.data)
    

class Projects(APIView):
    '''
        get:
          description: return the list of Projects defined in SHiNeMaS
          parameters:
            - name: token
              in: query
              schema:
                type: string
              description: authentication token
              required: true
          responses:
            '200':
              description: array of Projects objects
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      project_name:
                        type: string
                        description: the name of this Project
                      start_date:
                        type: date
                        description: the start date of this project
                      end_date:
                        type: date
                        description: the end date of this Project
    '''
    authentication_classes = [authentication.TokenAuthentication,]
    permission_classes = [AllowAny,]
    schema = AutoDocstringSchema()
    @csrf_exempt
    def get(self, request, token=None):
        auth = APIAuthentication(request.GET.get("token"))
        auth.authenticate()
        if auth.success == False:
            return Response({'error': auth.message},status=auth.status) 
        else :
            projects = Project.objects.all()
            projects_serialized = ProjectSerializer(projects, many=True)
            return Response(projects_serialized.data)
    
class SpeciesAPI(APIView):
    '''
        get:
          description: return the list of Species defined in SHiNeMaS
          parameters:
            - name: token
              in: query
              schema:
                type: string
              description: authentication token
              required: true
          responses:
            '200':
              description: array of Species Type objects
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      species:
                        type: string
                        description: the name of this Species
                      latin_name:
                        type: string
                        description: the Latin name of this Species
                      description:
                        type: string
                        description: the description of this Species
    '''
    authentication_classes = [authentication.TokenAuthentication,]
    permission_classes = [AllowAny,]
    schema = AutoDocstringSchema()
    @csrf_exempt
    def get(self, request, token=None):
        auth = APIAuthentication(request.GET.get("token"))
        auth.authenticate()
        if auth.success == False:
            return Response({'error': auth.message},status=auth.status) 
        else :
            species = Species.objects.all()
            species_serialized = SpeciesSerializer(species, many=True)
            return Response(species_serialized.data)

class GermplasmTypes(APIView):
    '''
        get:
          description: return the list of GermplasmType defined in SHiNeMaS
          parameters:
            - name: token
              in: query
              schema:
                type: string
              description: authentication token
              required: true
          responses:
            '200':
              description: array of Germplasm Type objects
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      germpalsm_type:
                        type: string
                        description: the name of this GermplasmType
                      description:
                        type: string
                        description: the description of this GermplasmType
    '''
    authentication_classes = [authentication.TokenAuthentication,]
    permission_classes = [AllowAny,]
    schema = AutoDocstringSchema()
    @csrf_exempt
    def get(self, request, token=None):
        auth = APIAuthentication(request.GET.get("token"))
        auth.authenticate()
        if auth.success == False:
            return Response({'error': auth.message},status=auth.status) 
        else :
            germplasm_types = GermplasmType.objects.all()
            germplasm_types_serialized = GermplasmTypeSerializer(germplasm_types, many=True)
            return Response(germplasm_types_serialized.data)

class Germplasms(APIView):
    '''
        get:
          description: return the list of Germplasm defined in SHiNeMaS
          parameters:
            - name: token
              in: query
              schema:
                type: string
              description: authentication token
              required: true
          responses:
            '200':
              description: array of Germplasm objects
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      germpalsm_name:
                        type: string
                        description: the name of this Germplasm
                      germplasm_type:
                        type: string
                        description: the type of this Germplasm
                      species:
                        type: string
                        description: the species of this Germplasm
    '''
    authentication_classes = [authentication.TokenAuthentication,]
    permission_classes = [AllowAny,]
    schema = AutoDocstringSchema()
    @csrf_exempt
    def get(self, request, token=None):
        auth = APIAuthentication(request.GET.get("token"))
        auth.authenticate()
        if auth.success == False:
            return Response({'error': auth.message},status=auth.status) 
        else :
            germplasms = Germplasm.objects.all()
            germplasm_serialized = GermplasmSerializer(germplasms, many=True)
            return Response(germplasm_serialized.data)

class Locations(APIView):
    """
    get:
      description: return the list of Location defined in SHiNeMaS
      parameters:
        - name: token
          in: query
          schema:
            type: string
          description: authentication token
          required: true
      responses:
        '200':
          description: array of Location objects
          content:
            application/json:
              schema:
                type: object
                properties:
                  short_name:
                    type: string
                    description: the short name of the Location
                  location_name:
                    type: string
                    description: the full name of this Location
                  country:
                    type: string
                    description: the country of this location
    """
    authentication_classes = [authentication.TokenAuthentication,]
    permission_classes = [AllowAny,]
    schema = AutoDocstringSchema()
    @csrf_exempt
    def get(self, request, token=None):
        auth = APIAuthentication(request.GET.get("token"))
        auth.authenticate()
        if auth.success == False:
            return Response({'error': auth.message},status=auth.status) 
        else :
            locations = Location.objects.all()
            locations_serialized = LocationSerializer(locations, many=True)
            return Response(locations_serialized.data)
    
@csrf_exempt
@api_view(['GET',])
@permission_classes((AllowAny,))
@schema(GlobalSchema())
def data_agro(request, token=""):
    print("data_agro")
    
    """
    Fourni des informations sur des relations (reproduction et selection), 
    id, child, parent, block, X, Y, location du parent, annee (celle du parent), type, 
    valeurs des variables, dates des variables  
    """
    
    auth = APIAuthentication(request.GET.get("token"))
    auth.authenticate()
    if auth.success == False:
        return Response({'error': auth.message},status=auth.status) 
    else :
        start = datetime.datetime.now()
        dico_filtres = get_filters(request)
    
        #relations = Relation.objects.select_related('selection', 'reproduction')
        response = {}
        response['data'] = []
        #on recupere les relations et on applique les filtres
        #relations = Relation.objects.prefetch_related('rawdatas', 'rawdatas__variable', 'project').select_related('selection', 'reproduction', 'diffusion', 'mixture', 'seed_lot_son', 'seed_lot_father','seed_lot_father__location','seed_lot_father__germplasm','seed_lot_father__germplasm__species') 
        relations = Relation.objects.prefetch_related('rawdatas','rawdatas__variable', 'rawdatas__method', 'project').select_related('selection', 'reproduction', 'diffusion', 'mixture', 'seed_lot_son', 'seed_lot_father','seed_lot_son__location','seed_lot_son__germplasm','seed_lot_son__germplasm__germplasm_type','seed_lot_son__germplasm__species','seed_lot_father__location','seed_lot_father__germplasm','seed_lot_father__germplasm__germplasm_type','seed_lot_father__germplasm__species') 
        relations = api_filters(relations, dico_filtres)
    
        after_filters = datetime.datetime.now()
        
        #on recupere le numero de la page et la taille d'une page s'ils sont specifies
        try : 
            page = int(request.GET['page'])
        except : 
            page = 1
            
    
        try : 
            page_size = int(request.GET['page_size'])
        except : 
            page_size = 1000
        
        nb_relations = relations.count()
        print("Relations count : {0}".format(nb_relations))
        print("Relations count page: {0}".format((page*page_size-1)-(page-1)*page_size))
        
        dico_infos = {}
        dico_infos['current_page'] = page
        dico_infos['page_size'] = page_size
        dico_infos['nb_pages'] = int(nb_relations/page_size)+1
        dico_infos['nb_result'] = nb_relations
        dico_infos['warnings'] = dico_filtres["warnings"]
        response['information'] = dico_infos
        
        for r in relations[(page-1)*page_size:page*page_size-1] :
        #for r in relations:
            #loop_start = datetime.datetime.now()
            dico = {}
                
            if r.selection != None :
                dico['type'] = 'selection'
            elif r.reproduction != None and r.is_male == 'X':
                dico['type'] = 'reproduction'
            elif r.reproduction != None and not r.is_male == 'X':
                dico['type'] = 'cross'
            elif r.diffusion != None :
                dico['type'] = 'diffusion'
            elif r.mixture != None and r.seed_lot_father.germplasm != r.seed_lot_son.germplasm:
                dico['type'] = 'mixture'
            elif r.mixture != None and r.seed_lot_father.germplasm == r.seed_lot_son.germplasm:
                dico['type'] = 'merge'
    
            dico['seed_lot_parent'] = r.seed_lot_father.name
            dico['seed_lot_child'] = r.seed_lot_son.name
            dico['block'] = r.block
            dico['X'] = r.X
            dico['Y'] = r.Y
            dico['location'] = r.seed_lot_son.location.short_name
            dico['year'] = r.seed_lot_son.date
    
            
            dico['germplasm'] = r.seed_lot_son.germplasm.idgermplasm
            #dico['germplasm_type'] = r.seed_lot_father.germplasm.germplasm_type.germplasm_type
            dico['projects'] = ', '.join([project.project_name for project in r.project.all()])
            dico['species'] = r.seed_lot_son.germplasm.species.species
    
            ### VARIABLES ###
            #dico_vars = []
            #for rd in r.rawdatas.all() :
            """if 'variables' in dico_filtres.keys():
                rawdatas = r.rawdatas.filter(variable__in=dico_filtres['variables']).distinct()
            else :
                rawdatas = r.rawdatas.all()"""
            #dico['raw_data'] = {}
            for rd in r.rawdatas.all():
                if 'variables' in dico_filtres.keys() and rd.variable in dico_filtres['variables'] :
                    _append_rawdata(rd, dico)
                elif 'variables' not in dico_filtres.keys():
                    _append_rawdata(rd, dico)
                    
                """datadico[rd.variable.name] = rd.raw_data 
                datadico[rd.variable.name+"$date"] = rd.date """
                
                """dico_var = {}
                col1 = str(rd.variable.name)
                col2 = col1 +"$date"
                dico_var[str(col1)] = rd.raw_data 
                dico_var[str(col2)] = rd.date 
                dico_vars.append(dico_var)"""
            #if dico_vars != {} : 
            #    dico['variables'] = dico_vars
            
            response['data'].append(dico) 
            #print("time for one relation : {0}".format(datetime.datetime.now()-loop_start))
        print("Query time : {0}".format(datetime.datetime.now()-after_filters))
        print("Total time : {0}".format(datetime.datetime.now()-start))
        return Response(response,status=HTTP_200_OK)    
    


def _append_rawdata(rd, dico):
    variable_dfid = 'raw_data.{0}'.format(rd.variable.name)
    if variable_dfid not in dico.keys() :
        dico[variable_dfid] = []
    datadico = {}
    datadico['individual'] = rd.individual
    datadico['date'] = rd.date
    datadico['value'] = rd.raw_data
    datadico['method'] = rd.method.method_name
    datadico['group'] = rd.group
    dico[variable_dfid].append(datadico)

@csrf_exempt
@api_view(['GET',])
@permission_classes((AllowAny,))
@schema(GlobalSchema())
def data_network_unipart_seed_lots(request,token=""):
    """
    get:
    description: return a list of relations
    parameters:
      - name: token
        type: string
        required: true
        location: form       
    """
    auth = APIAuthentication(request.GET.get("token"))
    auth.authenticate()
    if auth.success == False:
        return Response({'error': auth.message},status=auth.status) 
    else :
        start = datetime.datetime.now()
        response = {}
        response['data'] = []
        #FILTRES
        dico_filtres = get_filters(request)
        
        relations = Relation.objects.prefetch_related('seed_lot_father__relations_as_parent', 'seed_lot_father__relations_as_child__project','seed_lot_father__relations_as_parent__project', 'seed_lot_father__relations_as_child').select_related('seed_lot_father__location', 'seed_lot_son__location', 'diffusion', 'selection', 'mixture', 'reproduction', 'seed_lot_son__germplasm', 'seed_lot_father__germplasm', 'seed_lot_father__germplasm__species').all() 
        relations = relations = api_filters(relations, dico_filtres)
        if str(type(relations)) == "<class 'rest_framework.response.Response'>" : 
            return relations
        after_filters = datetime.datetime.now()
        
        try : 
            page = int(request.GET['page'])
        except : 
            page = 1
            
        try : 
            page_size = int(request.GET['page_size'])
        except : 
            page_size = 1000
        
        print("Relations count : {0}".format(relations.count()))
        print("Relations count page: {0}".format((page*page_size-1)-(page-1)*page_size))
        
        dico_infos = {}
        dico_infos['current_page'] = page
        dico_infos['page_size'] = page_size
        dico_infos['nb_pages'] = int(relations.count()/page_size)+1
        dico_infos['nb_result'] = relations.count()
        response['information']= dico_infos 
        
        for r in relations[(page-1)*page_size:page*page_size-1] :
            #loop_start = datetime.datetime.now()
            dico = {}
            
            dico['specie'] = r.seed_lot_father.germplasm.species.latin_name
            dico['projects'] = ', '.join([project.project_name for project in r.project.all()])
            dico['seed_lot_parent'] = r.seed_lot_father.name
            dico['seed_lot_child'] = r.seed_lot_son.name
            
            ### TYPE ###
            if(r.diffusion is not None) : 
                dico['relation_type'] = 'diffusion'
                dico['relation_year_start'] = r.diffusion.date
                dico['relation_year_end'] = r.diffusion.date
                
            if(r.mixture is not None) : 
                dico['relation_type'] = 'mixture'
                dico['relation_year_start'] = r.mixture.date
                dico['relation_year_end'] = r.mixture.date
            
            if(r.reproduction is not None) : 
                dico['relation_type'] = 'reproduction'
                dico['relation_year_start'] = r.reproduction.start_date
                dico['relation_year_end'] = r.reproduction.end_date
                
            if(r.selection is not None) : 
                dico['relation_type'] = 'selection'
                dico['relation_year_start'] = r.selection.date
                dico['relation_year_end'] = r.selection.date
                
            ### CHILD ###
            dico['germplasm_parent'] = r.seed_lot_father.germplasm.id
            dico['location_parent'] = r.seed_lot_father.location.short_name
            dico['long_parent'] = r.seed_lot_father.location.longitude
            dico['lat_parent'] = r.seed_lot_father.location.latitude
            dico['year_parent'] = r.seed_lot_father.date
            
            ### PARENT ###
            dico['germplasm_child'] = r.seed_lot_son.germplasm.id
            dico['location_child'] = r.seed_lot_son.location.short_name
            dico['long_child'] = r.seed_lot_son.location.longitude
            dico['lat_child'] = r.seed_lot_son.location.latitude
            dico['year_child'] = r.seed_lot_son.date
    
            response['data'].append(dico) 
            #print("time for one relation : {0}".format(datetime.datetime.now()-loop_start))
        print("Query time : {0}".format(datetime.datetime.now()-after_filters))
        print("Total time : {0}".format(datetime.datetime.now()-start))
        return Response(response,status=HTTP_200_OK)
    

@csrf_exempt
@api_view(['GET',])
@permission_classes((AllowAny,))
@schema(SpecificSchema())
def data_agro_sr(request,token=""):
    print("data_agro_sr")
    
    """
    On cherche quatres relations telles que on est eu une selection et reproduction 
    sur un seedlots puis reproduction de la selection et du vrac l'annee suivante 
    On affiche ensuite les informations de ces quatres relations 
    """
    response = {}
    response['data'] = [] 
    start = datetime.datetime.now()
    auth = APIAuthentication(request.GET.get("token"))
    auth.authenticate()
    if auth.success == False:
        return Response({'error': auth.message},status=auth.status) 
    else :
        dico_filtres = get_filters(request)
        if str(type(dico_filtres)) == "<class 'rest_framework.response.Response'>" : 
            return dico_filtres
    
        """Query with django ORM to test"""
        """
        available filters : all except relation type
        """
        relations = Relation.objects.prefetch_related(
                    Prefetch('seed_lot_son__relations_as_parent',queryset=Relation.objects.prefetch_related('project').select_related('reproduction','seed_lot_son','seed_lot_father','seed_lot_son__germplasm','seed_lot_son__germplasm__species','seed_lot_son__location').filter(reproduction_id__isnull=False, selection_id__isnull=True, is_male = 'X'), to_attr='relations_selresponse'), 
                    Prefetch('seed_lot_father__relations_as_parent',queryset=Relation.objects.prefetch_related('project').select_related('reproduction','seed_lot_son','seed_lot_father','seed_lot_son__germplasm','seed_lot_son__germplasm__species','seed_lot_son__location').filter(reproduction_id__isnull=False, selection_id__isnull=True, is_male = 'X'), to_attr='relations_vrac'),
                    Prefetch('seed_lot_father__relations_as_parent__seed_lot_son__relations_as_parent',queryset=Relation.objects.prefetch_related('project').select_related('reproduction','seed_lot_son','seed_lot_father','seed_lot_son__germplasm','seed_lot_son__germplasm__species','seed_lot_son__location').filter(reproduction_id__isnull=False, selection_id__isnull=True, is_male = 'X'), to_attr='relations_vracresponse')
    
                ).select_related(
                    'seed_lot_father','seed_lot_son','seed_lot_son__location','seed_lot_son__germplasm','seed_lot_son__germplasm__species'
                ).filter(selection_id__isnull=False, 
                
                         seed_lot_son__relations_as_parent__reproduction_id__isnull=False, 
                         seed_lot_son__relations_as_parent__selection_id__isnull=True, 
                         seed_lot_son__relations_as_parent__is_male='X',
                         
                         seed_lot_father__relations_as_parent__reproduction_id__isnull=False, 
                         seed_lot_father__relations_as_parent__selection_id__isnull=True, 
                         seed_lot_father__relations_as_parent__is_male='X',
                         
                         seed_lot_father__relations_as_parent__seed_lot_son__relations_as_parent__reproduction_id__isnull=False, 
                         seed_lot_father__relations_as_parent__seed_lot_son__relations_as_parent__selection_id__isnull=True, 
                         seed_lot_father__relations_as_parent__seed_lot_son__relations_as_parent__is_male='X',
                ).distinct()
        
        relations = entities_filters(relations, dico_filtres)
        
        if str(type(relations)) == "<class 'rest_framework.response.Response'>" : 
            return relations
        after_filters = datetime.datetime.now()
        
    
        try : 
            page = int(request.GET['page'])
        except : 
            page = 1
            
        try : 
            page_size = int(request.GET['page_size'])
        except : 
            page_size = 1000
        
        print("Expe count : {0}".format(len(list(relations))))
        print("Expe count page: {0}".format((page*page_size-1)-(page-1)*page_size))
        expe_id = 0
        
        for r in relations[(page-1)*page_size:page*page_size-1] :
            #loop_start = datetime.datetime.now()
            expe_id += 1
            """
                review dict building with sury using ORM
            """
            dict_selection = {}
            dict_selection['projects'] = ', '.join([project.project_name for project in r.project.all()])
            dict_selection['species'] = r.seed_lot_son.germplasm.species.species
            dict_selection['seed_lot_child'] = r.seed_lot_son.name
            dict_selection['seed_lot_parent'] = r.seed_lot_father.name
            dict_selection['type'] = 'selection'
            dict_selection['block'] = r.block
            dict_selection['X'] = r.X
            dict_selection['Y'] = r.Y
            dict_selection['location'] = r.seed_lot_son.location.short_name
            dict_selection['year'] = r.seed_lot_son.date
            dict_selection['group'] = "S"
            dict_selection['version'] = "selection"
            dict_selection['expe_id'] = expe_id
            
            response['data'].append(dict_selection)
            
            for rvrac in r.seed_lot_father.relations_vrac :
                dict_vrac = {}
                dict_vrac['projects'] = ', '.join([project.project_name for project in rvrac.project.all()])
                dict_vrac['species'] = rvrac.seed_lot_son.germplasm.species.species
                dict_vrac['seed_lot_child'] = rvrac.seed_lot_son.name
                dict_vrac['seed_lot_parent'] = rvrac.seed_lot_father.name
                dict_vrac['type'] = 'reproduction'
                dict_vrac['block'] = rvrac.block
                dict_vrac['X'] = rvrac.X
                dict_vrac['Y'] = rvrac.Y
                dict_vrac['location'] = rvrac.seed_lot_son.location.short_name
                dict_vrac['year'] = rvrac.seed_lot_son.date
                dict_vrac['group'] = "S"
                dict_vrac['version'] = "vrac"
                dict_vrac['expe_id'] = expe_id
                response['data'].append(dict_vrac)
            
            responses_ids = []
            for respsel in r.seed_lot_son.relations_selresponse:
                dict_sel_resp = {}
                responses_ids.append(respsel.id)
                dict_sel_resp['projects'] = ', '.join([project.project_name for project in respsel.project.all()])
                dict_sel_resp['species'] = respsel.seed_lot_son.germplasm.species.species
                dict_sel_resp['seed_lot_child'] = respsel.seed_lot_son.name
                dict_sel_resp['seed_lot_parent'] = respsel.seed_lot_father.name
                dict_sel_resp['type'] = 'reproduction'
                dict_sel_resp['block'] = respsel.block
                dict_sel_resp['X'] = respsel.X
                dict_sel_resp['Y'] = respsel.Y
                dict_sel_resp['location'] = respsel.seed_lot_son.location.short_name
                dict_sel_resp['year'] = respsel.seed_lot_son.date
                dict_sel_resp['group'] = "R"
                dict_sel_resp['version'] = "selection"
                dict_sel_resp['expe_id'] = expe_id
                response['data'].append(dict_sel_resp)
    
            for rel_respsel in r.seed_lot_father.relations_as_parent.all():
                for respsel in rel_respsel.seed_lot_son.relations_vracresponse:
                    if respsel.id not in responses_ids :
                        dict_vrac_resp = {}
                        dict_vrac_resp['projects'] = ', '.join([project.project_name for project in respsel.project.all()])
                        dict_vrac_resp['species'] = respsel.seed_lot_son.germplasm.species.species
                        dict_vrac_resp['seed_lot_child'] = respsel.seed_lot_son.name
                        dict_vrac_resp['seed_lot_parent'] = respsel.seed_lot_father.name
                        dict_vrac_resp['type'] = 'reproduction'
                        dict_vrac_resp['block'] = respsel.block
                        dict_vrac_resp['X'] = respsel.X
                        dict_vrac_resp['Y'] = respsel.Y
                        dict_vrac_resp['location'] = respsel.seed_lot_son.location.short_name
                        dict_vrac_resp['year'] = respsel.seed_lot_son.date
                        dict_vrac_resp['group'] = "R"
                        dict_vrac_resp['version'] = "vrac"
                        dict_vrac_resp['expe_id'] = expe_id
                        response['data'].append(dict_vrac_resp)
            
        dico_infos = {}
        dico_infos['current_page'] = page
        dico_infos['page_size'] = page_size
        dico_infos['nb_pages'] = int(expe_id/page_size)+1
        dico_infos['nb_expe'] = expe_id
        dico_infos['nb_result'] = len(response['data'])
        response['information'] = dico_infos 
        
        print("Query time : {0}".format(datetime.datetime.now()-after_filters))
        print("Total time : {0}".format(datetime.datetime.now()-start))
                
        return Response(response,status=HTTP_200_OK)


@csrf_exempt
@api_view(['GET',])
@permission_classes((AllowAny,))
@schema(SpecificSchema())
def data_agro_ha(request,token=""):
    print("data_agro_ha")
    
    """
    On cherche deux reproductions : une apres diffusion et l'autre avec le meme seedlot mais sur le lieu d'origine 
    On affiche ensuite les informations de ces relations 
    """
    
    start = datetime.datetime.now()
    
    auth = APIAuthentication(request.GET.get("token"))
    auth.authenticate()
    if auth.success == False:
        return Response({'error': auth.message},status=auth.status) 
    else :
        response = {}
        response['data'] = []
        
        dico_filtres = get_filters(request)
        if str(type(dico_filtres)) == "<class 'rest_framework.response.Response'>" :
            return dico_filtres
        
        """
        available filters : all except relation type
        """
        relations = Relation.objects.prefetch_related(
                    Prefetch('seed_lot_son__relations_as_parent',queryset=Relation.objects.prefetch_related('project').select_related('reproduction','seed_lot_son','seed_lot_father','seed_lot_son__germplasm__species','seed_lot_son__location').filter(reproduction_id__isnull=False, selection_id__isnull=True, is_male = 'X'), to_attr='relations_away'), 
                    Prefetch('seed_lot_father__relations_as_parent',queryset=Relation.objects.prefetch_related('project').select_related('reproduction','seed_lot_son','seed_lot_father','seed_lot_son__germplasm__species','seed_lot_son__location').filter(reproduction_id__isnull=False, selection_id__isnull=True, is_male = 'X'), to_attr='relations_home')
                ).select_related(
                    'seed_lot_father','seed_lot_son','seed_lot_father__location'
                ).filter(diffusion_id__isnull=False, 
                         seed_lot_son__relations_as_parent__reproduction_id__isnull=False, 
                         seed_lot_son__relations_as_parent__selection_id__isnull=True, 
                         seed_lot_son__relations_as_parent__is_male='X',
                         seed_lot_father__relations_as_parent__reproduction_id__isnull=False, 
                         seed_lot_father__relations_as_parent__selection_id__isnull=True, 
                         seed_lot_father__relations_as_parent__is_male='X',
                ).distinct()
        relations = entities_filters(relations, dico_filtres)
        if str(type(relations)) == "<class 'rest_framework.response.Response'>" : 
            return relations
        after_filters = datetime.datetime.now()
        
        try : 
            page = int(request.GET['page'])
        except : 
            page = 1
            
        try : 
            page_size = int(request.GET['page_size'])
        except : 
            page_size = 1000
        
        print("Relations count : {0}".format(relations.count()))
        print("Relations count page: {0}".format((page*page_size-1)-(page-1)*page_size))
        expe_id = 0
        for r in relations[(page-1)*page_size:page*page_size-1] : 
            #loop_start = datetime.datetime.now()
            expe_id += 1
            
            
    
            #all_home_relations = Relation.objects.prefetch_related('project').select_related('seed_lot_son','seed_lot_son__location', 'reproduction', 'seed_lot_son__germplasm__species').filter(reproduction_id__isnull=False, selection_id__isnull=True, is_male = 'X', seed_lot_father=r.seed_lot_father)
            #all_home_relations = r.relations_home
            for home_relation in r.seed_lot_father.relations_home:
                home={}
                home['projects'] = ', '.join([project.project_name for project in home_relation.project.all()])
                home['species'] = home_relation.seed_lot_son.germplasm.species.species
                home['seed_lot_child'] = home_relation.seed_lot_son.name
                home['seed_lot_parent'] = home_relation.seed_lot_father.name
                home['date'] = str(home_relation.reproduction.start_date) + " - " + str(home_relation.reproduction.end_date)
                home['relation_type'] = "reproduction"
                home['origin'] = r.seed_lot_father.location.short_name
                home['expe_id'] = expe_id
                home['group'] = "H"
                response['data'].append(home)
            
            
            #all_away_relations = Relation.objects.prefetch_related('project').select_related('seed_lot_son','seed_lot_son__location', 'reproduction', 'seed_lot_son__germplasm__species').filter(reproduction_id__isnull=False, selection_id__isnull=True, is_male = 'X', seed_lot_father=r.seed_lot_son)
            #all_away_relations = r.seed_lot_father.away
            for away_relation in r.seed_lot_son.relations_away :
                away={}
                away['projects'] = ', '.join([project.project_name for project in away_relation.project.all()])
                away['species'] = away_relation.seed_lot_son.germplasm.species.species
                away['seed_lot_child'] = away_relation.seed_lot_son.name
                away['seed_lot_parent'] = away_relation.seed_lot_father.name
                away['date'] = str(away_relation.reproduction.start_date) + " - " + str(away_relation.reproduction.end_date)
                away['relation_type'] = "reproduction"
                away['origin'] = r.seed_lot_father.location.short_name
                away['expe_id'] = expe_id
                away['group'] = "A"
                response['data'].append(away)
        
        dico_infos = {}
        dico_infos['current_page'] = page
        dico_infos['page_size'] = page_size
        dico_infos['nb_pages'] = int(relations.count()/page_size)+1
        dico_infos['nb_expe'] = expe_id
        dico_infos['nb_result'] = len(response['data'])
        response['information'] = dico_infos 
        
        print("Query time : {0}".format(datetime.datetime.now()-after_filters))
        print("Total time : {0}".format(datetime.datetime.now()-start))
            
        return Response(response,status=HTTP_200_OK)



@csrf_exempt
@api_view(['GET',])
@permission_classes((AllowAny,))
@schema(SpecificSchema())
def data_agro_mixture(request,token=""):
    print("data_agro_mix")
    
    """
     
    """
    start = datetime.datetime.now()

    
    auth = APIAuthentication(request.GET.get("token"))
    auth.authenticate()
    if auth.success == False:
        return Response({'error': auth.message},status=auth.status) 
    else :
    
        response = {}
        response['data'] = []
        
        dico_filtres = get_filters(request)
            
        relations = Relation.objects.prefetch_related('project',
                        Prefetch('seed_lot_son__relations_as_child',queryset=Relation.objects.prefetch_related('project').select_related(
                    'seed_lot_father','seed_lot_son','seed_lot_father__location','seed_lot_son__location','seed_lot_father__germplasm','seed_lot_son__germplasm','seed_lot_father__germplasm__species','seed_lot_son__germplasm__species','mixture'
                ).filter(mixture_id__isnull=False).exclude(seed_lot_father__germplasm=F('seed_lot_son__germplasm')), to_attr='other_mixtures'), 
                
                ).select_related(
                    'seed_lot_father','seed_lot_son','seed_lot_father__location','seed_lot_son__location','seed_lot_father__germplasm','seed_lot_son__germplasm','seed_lot_father__germplasm__species','seed_lot_son__germplasm__species','mixture').filter(mixture_id__isnull=False
                ).exclude(seed_lot_father__germplasm=F('seed_lot_son__germplasm')
                )
        
        relations = entities_filters(relations, dico_filtres)
        relations = relations.order_by('-mixture__date','seed_lot_son')
        after_filters = datetime.datetime.now()
        
        try : 
            page = int(request.GET['page'])
        except : 
            page = 1
            
        try : 
            page_size = int(request.GET['page_size'])
        except : 
            page_size = 1000
            
        id_mixture = 1
        mixtures_ok = []
        for relation in relations[(page-1)*page_size:page*page_size-1] : 
            if relation.seed_lot_son not in mixtures_ok :
                for component in relation.seed_lot_son.other_mixtures:
                    mixture = {}
                    mixture['id_mixture'] = id_mixture
                    mixture['project'] = ', '.join([project.project_name for project in component.project.all()])
                    mixture['component_seed_lot'] = component.seed_lot_father.name
                    mixture['component_germplasm'] = component.seed_lot_father.germplasm.idgermplasm
                    mixture['component_species'] =  component.seed_lot_father.germplasm.species.species
                    mixture['mixture_seed_lot'] = component.seed_lot_son.name
                    mixture['mixture_germplasm'] = component.seed_lot_son.germplasm.idgermplasm
                    mixture['mixture_species'] =  component.seed_lot_son.germplasm.species.species
                    mixture['quantity'] = component.quantity
                    mixture['year'] = component.mixture.date
                    mixture['location'] = component.seed_lot_son.location.short_name
                    response['data'].append(mixture)
                id_mixture += 1
                mixtures_ok.append(relation.seed_lot_son)
        
        
        dico_infos = {}
        dico_infos['current_page'] = page
        dico_infos['page_size'] = page_size
        dico_infos['nb_pages'] = int(id_mixture/page_size)+1
        #dico_infos['nb_expe'] = expe_id
        dico_infos['nb_result'] = id_mixture-1
        response['information'] = dico_infos 
        
        print("Query time : {0}".format(datetime.datetime.now()-after_filters))
        print("Total time : {0}".format(datetime.datetime.now()-start))
            
        return Response(response,status=HTTP_200_OK)
    
    
    
    
    
    