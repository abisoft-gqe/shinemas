function getdata_query_lies(id_query_type,
			    id_germ_in,id_germ_out,
			    id_year_in,id_year_out,id_project_in,id_project_out,id_relation_in,id_repro_type_in,id_germplasm_type_in,id_germplasm_type_out,
			    liste_germ_in,liste_germ_out,liste_person_in,liste_person_out,
			    liste_sl_in,liste_sl_out,liste_var_in,
			    germ_in_liste,germ_out_liste,sl_in_liste,sl_out_liste,person_in_liste,person_out_liste,var_in_liste,
			    id_filter_on,data_type_txt,data_type_menu,
			    filter_on_txt,filter_on_menu,
			    fill_gap_txt,fill_gap_menu,
			    net_info_txt,net_info_menu,
			    mdist_txt,mdist_menu,germ_in_txt,germ_in_menu,
			    germ_out_txt,germ_out_menu,
			    germ_type_in_txt,germ_type_in_menu,germ_type_out_txt,germ_type_out_menu,
			    year_in_txt,year_in_menu,year_out_txt,year_out_menu,
			    project_in_txt,project_in_menu,project_out_txt,project_out_menu,
			    person_in_txt,person_in_menu,person_out_txt,person_out_menu,
			    sl_in_txt,sl_in_menu,sl_out_txt,sl_out_menu,
			    rel_in_txt,rel_in_menu,
			    repro_in_txt,repro_in_menu,
			    var_in_txt,var_in_menu){

  if (id_query_type.value=="network"){
    var p_list = document.getElementsByTagName("p");
    for(var i=p_list.length-1; i>=0; i--){
        var p = p_list[i];
	if (p.className==="liste"){
	  p.parentNode.removeChild(p);
	}
    }
    id_filter_on.value=''; //pour eviter que quand on revient sur filter_on on ait autre chose selectionné et pas filtres autres proposés 
    data_type_txt.style.display='none';
    data_type_menu.style.display='none';
    filter_on_txt.style.display='block';
    filter_on_menu.style.display='block';
    fill_gap_txt.style.display='block';
    fill_gap_menu.style.display='block';
    net_info_txt.style.display='block';
    net_info_menu.style.display='block';
    mdist_txt.style.display='block';
    mdist_menu.style.display='block';
    germ_in_txt.style.display='none';
    germ_in_menu.style.display='none';
    germ_in_liste.style.display='none'; 
    liste_germ_in.style.display='none';
    
    germ_out_txt.style.display='none';
    germ_out_menu.style.display='none';
    germ_out_liste.style.display='none';
    liste_germ_out.style.display='none';
    
    germ_type_in_txt.style.display='none';
    germ_type_in_menu.style.display='none';
    id_germplasm_type_in.value="";
    germ_type_out_txt.style.display='none';
    germ_type_out_menu.style.display='none';
    id_germplasm_type_out.value="";
    
    year_in_txt.style.display='none';
    year_in_menu.style.display='none';
    id_year_in.value=""; 
    year_out_txt.style.display='none';
    year_out_menu.style.display='none';
    id_year_out.value="";
    
    project_in_txt.style.display='none';
    project_in_menu.style.display='none';
    id_project_in.value="";
    project_out_txt.style.display='none';
    project_out_menu.style.display='none';
    id_project_out.value='';
    
    person_in_txt.style.display='none';
    person_in_menu.style.display='none';
    person_in_liste.style.display='none';
    liste_person_in.style.display='none';
 
    person_out_txt.style.display='none';
    person_out_menu.style.display='none';
    person_out_liste.style.display='none';
    liste_person_out.style.display='none';
    
    sl_in_txt.style.display='none';
    sl_in_menu.style.display='none';
    sl_in_liste.style.display='none';
    liste_sl_in.style.display='none';
    
    sl_out_txt.style.display='none';
    sl_out_menu.style.display='none';
    sl_out_liste.style.display='none';
    liste_sl_out.style.display='none';
    
    rel_in_txt.style.display='none';
    rel_in_menu.style.display='none';
    id_relation_in.value='';
    
    repro_in_txt.style.display='none';
    repro_in_menu.style.display='none';
    id_repro_type_in.value="";
    var_in_txt.style.display='none';
    var_in_menu.style.display='none';
    var_in_liste.style.display='none';
    liste_var_in.style.display='none';
  }
  else if (id_query_type.value=="methods"){   
    var p_list = document.getElementsByTagName("p");
    for(var i=p_list.length-1; i>=0; i--){
        var p = p_list[i];
	if (p.className==="liste"){
	  p.parentNode.removeChild(p);
	}
    }
    data_type_txt.style.display='none';
    data_type_menu.style.display='none';
    filter_on_txt.style.display='none';
    filter_on_menu.style.display='none';
    fill_gap_txt.style.display='none';
    fill_gap_menu.style.display='none';
    net_info_txt.style.display='none';
    net_info_menu.style.display='none';
    mdist_txt.style.display='none';
    mdist_menu.style.display='none';
    
    germ_in_txt.style.display='none';
    germ_in_menu.style.display='none';
    germ_in_liste.style.display='none';
    liste_germ_in.style.display='none';
    
    germ_out_txt.style.display='none';
    germ_out_menu.style.display='none';
    germ_out_liste.style.display='none';
    liste_germ_out.style.display='none';
    
    germ_type_in_txt.style.display='none';
    germ_type_in_menu.style.display='none';
    id_germplasm_type_in.value="";
    
    germ_type_out_txt.style.display='none';
    germ_type_out_menu.style.display='none';
    id_germplasm_type_out.value="";
    
    year_in_txt.style.display='none';
    year_in_menu.style.display='none';
    id_year_in.value="";
    
    year_out_txt.style.display='none';
    year_out_menu.style.display='none';
    id_year_out.value='';
    
    project_in_txt.style.display='none';
    project_in_menu.style.display='none';
    id_project_in.value='';
    
    project_out_txt.style.display='none';
    project_out_menu.style.display='none';
    id_project_out.value="";
    
    person_in_txt.style.display='none';
    person_in_menu.style.display='none';
    person_in_liste.style.display='none';
    liste_person_in.style.display='none';
    
    person_out_txt.style.display='none';
    person_out_menu.style.display='none';
    person_out_liste.style.display='none';
    liste_person_out.style.display='none';
    
    sl_in_txt.style.display='none';
    sl_in_menu.style.display='none';
    sl_in_liste.style.display='none';
    liste_sl_in.style.display='none';
    
    sl_out_txt.style.display='none';
    sl_out_menu.style.display='none';    
    sl_out_liste.style.display='none';
    liste_sl_out.style.display='none';
    
    rel_in_txt.style.display='none';
    rel_in_menu.style.display='none';
    id_relation_in.value="";
    
    repro_in_txt.style.display='none';
    repro_in_menu.style.display='none';
    id_repro_type_in.value="";
    
    var_in_txt.style.display='block';
    var_in_menu.style.display='block';
    var_in_liste.style.display='block';
    liste_var_in.style.display='block';
  }
  else if (id_query_type.value=="cross"){
    var p_list = document.getElementsByTagName("p");
    for(var i=p_list.length-1; i>=0; i--){
        var p = p_list[i];
	if (p.className==="liste"){
	  p.parentNode.removeChild(p);
	}
    }
    data_type_txt.style.display='none';
    data_type_menu.style.display='none';
    id_filter_on.value='';
    filter_on_txt.style.display='block';
    filter_on_menu.style.display='block';
    fill_gap_txt.style.display='none';
    fill_gap_menu.style.display='none';
    net_info_txt.style.display='none';
    net_info_menu.style.display='none';
    mdist_txt.style.display='none';
    mdist_menu.style.display='none';
    
    germ_in_txt.style.display='none';
    germ_in_menu.style.display='none';
    germ_in_liste.style.display='none';
    liste_germ_in.style.display='none';
    
    germ_out_txt.style.display='none';
    germ_out_menu.style.display='none';
    germ_out_liste.style.display='none';
    liste_germ_out.style.display='none';
    
    germ_type_in_txt.style.display='none';
    germ_type_in_menu.style.display='none';
    id_germplasm_type_in.value="";
    germ_type_out_txt.style.display='none';
    germ_type_out_menu.style.display='none';
    id_germplasm_type_out.value="";
    
    year_in_txt.style.display='none';
    year_in_menu.style.display='none';
    id_year_in.value='';
    
    year_out_txt.style.display='none';
    year_out_menu.style.display='none';
    id_year_out.value='';
    
    project_in_txt.style.display='none';
    project_in_menu.style.display='none';
    id_project_in.value="";
    project_out_txt.style.display='none';
    project_out_menu.style.display='none';
    id_project_out.value='';
    
    person_in_txt.style.display='none';
    person_in_menu.style.display='none';
    person_in_liste.style.display='none';
    liste_person_in.style.display='none';
    
    person_out_txt.style.display='none';
    person_out_menu.style.display='none';
    person_out_liste.style.display='none';
    liste_person_out.style.display='none';
    
    sl_in_txt.style.display='none';
    sl_in_menu.style.display='none';
    sl_in_liste.style.display='none';
    liste_sl_in.style.display='none';
    
    sl_out_txt.style.display='none';
    sl_out_menu.style.display='none';    
    sl_out_liste.style.display='none';
    liste_sl_out.style.display='none';
    
    rel_in_txt.style.display='none';
    rel_in_menu.style.display='none';
    id_relation_in.value="";
    
    repro_in_txt.style.display='none';
    repro_in_menu.style.display='none';
    id_repro_type_in.value="";
    
    var_in_txt.style.display='none';
    var_in_menu.style.display='none';
    var_in_liste.style.display='none';
    liste_var_in.style.display='none';
  }
  else if (id_query_type.value=="data-classic" || id_query_type.value=="data-S" || id_query_type.value=="data-SR"){
    var p_list = document.getElementsByTagName("p");
    for(var i=p_list.length-1; i>=0; i--){
        var p = p_list[i];
	if (p.className==="liste"){
	  p.parentNode.removeChild(p);
	}
    }
    data_type_txt.style.display='block';
    data_type_menu.style.display='block';    
    id_data_type.options[1].selected="selected";
    id_data_type.options[0].style.display="none";    
    filter_on_txt.style.display='block';
    filter_on_menu.style.display='block';    
    fill_gap_txt.style.display='none';
    fill_gap_menu.style.display='none';
    net_info_txt.style.display='none';
    net_info_menu.style.display='none';    
    mdist_txt.style.display='none';
    mdist_menu.style.display='none';
    
    germ_in_txt.style.display='none';
    germ_in_menu.style.display='none';
    germ_in_liste.style.display='none';
    liste_germ_in.style.display='none';
    
    germ_out_txt.style.display='none';
    germ_out_menu.style.display='none';
    germ_out_liste.style.display='none';
    liste_germ_out.style.display='none';
    
    germ_type_in_txt.style.display='none';
    germ_type_in_menu.style.display='none';
    id_germplasm_type_in.value="";
    
    germ_type_out_txt.style.display='none';
    germ_type_out_menu.style.display='none';
    id_germplasm_type_out.value="";
    
    year_in_txt.style.display='none';
    year_in_menu.style.display='none';
    id_year_in.value="";
    
    year_out_txt.style.display='none';
    year_out_menu.style.display='none';
    id_year_out.value="";
    
    project_in_txt.style.display='none';
    project_in_menu.style.display='none';
    id_project_in.value="";
    
    project_out_txt.style.display='none';
    project_out_menu.style.display='none';
    id_project_out.value="";
    
    person_in_txt.style.display='none';
    person_in_menu.style.display='none';
    person_in_liste.style.display='none';
    liste_person_in.style.display='none';
    
    person_out_txt.style.display='none';
    person_out_menu.style.display='none';
    person_out_liste.style.display='none';
    liste_person_out.style.display='none';
    
    sl_in_txt.style.display='none';
    sl_in_menu.style.display='none';
    sl_in_liste.style.display='none';
    liste_sl_in.style.display='none';
    
    sl_out_txt.style.display='none';
    sl_out_menu.style.display='none';    
    sl_out_liste.style.display='none';
    liste_sl_out.style.display='none';
    
    rel_in_txt.style.display='none';
    rel_in_menu.style.display='none';
    id_relation_in.value="";
    
    repro_in_txt.style.display='none';
    repro_in_menu.style.display='none';
    id_repro_type_in.value="";
    
    var_in_txt.style.display='none';
    var_in_menu.style.display='none';
    var_in_liste.style.display='none';
    liste_var_in.style.display='none';
    
    id_filter_on.value='';
  }
  else{
    var p_list = document.getElementsByTagName("p");
    for(var i=p_list.length-1; i>=0; i--){
        var p = p_list[i];
	if (p.className==="liste"){
	  p.parentNode.removeChild(p);
	}
    }
    data_type_txt.style.display='none';
    data_type_menu.style.display='none';
    
    filter_on_txt.style.display='none';
    filter_on_menu.style.display='none';
    
    fill_gap_txt.style.display='none';
    fill_gap_menu.style.display='none';
    
    net_info_txt.style.display='none';
    net_info_menu.style.display='none';
    
    mdist_txt.style.display='none';
    mdist_menu.style.display='none';
    
    germ_in_txt.style.display='none';
    germ_in_menu.style.display='none';
    germ_in_liste.style.display='none';
    liste_germ_in.style.display='none';
    
    germ_out_txt.style.display='none';
    germ_out_menu.style.display='none';
    germ_out_liste.style.display='none';
    liste_germ_out.style.display='none';
    
    germ_type_in_txt.style.display='none';
    germ_type_in_menu.style.display='none';
    id_germplasm_type_in.value="";
    
    germ_type_out_txt.style.display='none';
    germ_type_out_menu.style.display='none';
    id_germplasm_type_out.value="";
    
    year_in_txt.style.display='none';
    year_in_menu.style.display='none';    
    id_year_in.value=""; 
    
    year_out_txt.style.display='none';
    year_out_menu.style.display='none';
    id_year_out.value="";
    
    project_in_txt.style.display='none';
    project_in_menu.style.display='none';
    id_project_in.value="";
    
    project_out_txt.style.display='none';
    project_out_menu.style.display='none';
    id_project_out.value="";
    
    person_in_txt.style.display='none';
    person_in_menu.style.display='none';
    person_in_liste.style.display='none';
    liste_person_in.style.display='none';
    
    person_out_txt.style.display='none';
    person_out_menu.style.display='none';
    person_out_liste.style.display='none';
    liste_person_out.style.display='none';
    
    sl_in_txt.style.display='none';
    sl_in_menu.style.display='none';
    sl_in_liste.style.display='none';
    liste_sl_in.style.display='none';
    
    sl_out_txt.style.display='none';
    sl_out_menu.style.display='none';    
    sl_out_liste.style.display='none';
    liste_sl_out.style.display='none';
    
    rel_in_txt.style.display='none';
    rel_in_menu.style.display='none';
    id_relation_in.value="";
    
    repro_in_txt.style.display='none';
    repro_in_menu.style.display='none';
    id_repro_type_in.value="";
    
    var_in_txt.style.display='none';
    var_in_menu.style.display='none';
    var_in_liste.style.display='none';
    liste_var_in.style.display='none';
    
    id_filter_on.value='';
  }
  
}

function getdata_data_lies(id_data_type,liste_germ_in,liste_germ_out,liste_person_in,liste_person_out,
			  liste_sl_in,liste_sl_out,liste_var_in,germ_in_liste,germ_out_liste,sl_in_liste,sl_out_liste,person_in_liste,person_out_liste,var_in_liste,
			  id_filter_on,query_type_menu,
			  filter_on_txt,filter_on_menu,
			  germ_in_txt,germ_in_menu,
			  germ_out_txt,germ_out_menu,
			  germ_type_in_txt,germ_type_in_menu,
			  germ_type_out_txt,germ_type_out_menu,
			  year_in_txt,year_in_menu,
			  year_out_txt,year_out_menu,
			  project_in_txt,project_in_menu,
			  project_out_txt,project_out_menu,
			  person_in_txt,person_in_menu,person_out_txt,person_out_menu,
			  sl_in_txt,sl_in_menu,
			  sl_out_txt,sl_out_menu,
			  rel_in_txt,rel_in_menu,
			  repro_in_txt,repro_in_menu,
			  var_in_txt,var_in_menu){

  if (id_data_type.value=="seed lots"){
    filter_on_txt.style.display='none';
    filter_on_menu.style.display='none';
    germ_in_txt.style.display='block';
    germ_in_menu.style.display='block';
    germ_in_liste.style.display='block';
    liste_germ_in.style.display='block';
    germ_out_txt.style.display='block';
    germ_out_menu.style.display='block';
    germ_out_liste.style.display='block';
    liste_germ_out.style.display='block';
    germ_type_in_txt.style.display='block';
    germ_type_in_menu.style.display='block';
    germ_type_out_txt.style.display='block';
    germ_type_out_menu.style.display='block';
    year_in_txt.style.display='block';
    year_in_menu.style.display='block';
    year_out_txt.style.display='block';
    year_out_menu.style.display='block';
    project_in_txt.style.display='block';
    project_in_menu.style.display='block';
    project_out_txt.style.display='block';
    project_out_menu.style.display='block';
    person_in_txt.style.display='block';
    person_in_menu.style.display='block';
    person_in_liste.style.display='block';
    liste_person_in.style.display='block';
    person_out_txt.style.display='block';
    person_out_menu.style.display='block';
    person_out_liste.style.display='block';
    liste_person_out.style.display='block';
    sl_in_txt.style.display='block';
    sl_in_menu.style.display='block';
    sl_in_liste.style.display='block';
    liste_sl_in.style.display='block';
    sl_out_txt.style.display='block';
    sl_out_menu.style.display='block';
    sl_out_liste.style.display='block';
    liste_sl_out.style.display='block';
    rel_in_txt.style.display='none';
    rel_in_menu.style.display='none';
    repro_in_txt.style.display='block';
    repro_in_menu.style.display='block';
    var_in_txt.style.display='block';
    var_in_menu.style.display='block';
    var_in_liste.style.display='block';
    liste_var_in.style.display='block';
  }
  else{
    filter_on_txt.style.display='block';
    filter_on_menu.style.display='block';
    germ_in_txt.style.display='none';
    germ_in_menu.style.display='none';
    germ_in_liste.style.display='none';
    liste_germ_in.style.display='none';
    germ_out_txt.style.display='none';
    germ_out_menu.style.display='none';
    germ_out_liste.style.display='none';
    liste_germ_out.style.display='none';
    germ_type_in_txt.style.display='none';
    germ_type_in_menu.style.display='none';
    germ_type_out_txt.style.display='none';
    germ_type_out_menu.style.display='none';
    year_in_txt.style.display='none';
    year_in_menu.style.display='none';
    year_out_txt.style.display='none';
    year_out_menu.style.display='none';
    project_in_txt.style.display='none';
    project_in_menu.style.display='none';
    project_out_txt.style.display='none';
    project_out_menu.style.display='none';
    person_in_txt.style.display='none';
    person_in_menu.style.display='none';
    person_in_liste.style.display='none';
    liste_person_in.style.display='none';
    person_out_txt.style.display='none';
    person_out_menu.style.display='none';
    person_out_liste.style.display='none';
    liste_person_out.style.display='none';
    sl_in_txt.style.display='none';
    sl_in_menu.style.display='none';
    sl_in_liste.style.display='none';
    liste_sl_in.style.display='none';
    sl_out_txt.style.display='none';
    sl_out_menu.style.display='none';
    sl_out_liste.style.display='none';
    liste_sl_out.style.display='none';
    rel_in_txt.style.display='none';
    rel_in_menu.style.display='none';
    repro_in_txt.style.display='none';
    repro_in_menu.style.display='none';
    var_in_txt.style.display='none';
    var_in_menu.style.display='none';
    var_in_liste.style.display='none';
    liste_var_in.style.display='none';
    id_filter_on.value='';
  }
}

function getdata_filter_lies(id_filter_on,liste_germ_in,liste_germ_out,liste_person_in,liste_person_out,
			    liste_sl_in,liste_sl_out,liste_var_in,germ_in_liste,germ_out_liste,sl_in_liste,sl_out_liste,person_in_liste,person_out_liste,var_in_liste,query_type_menu,germ_in_txt,germ_in_menu,
			    germ_out_txt,germ_out_menu,
			    germ_type_in_txt,germ_type_in_menu,
			    germ_type_out_txt,germ_type_out_menu,
			    year_in_txt,year_in_menu,
			    year_out_txt,year_out_menu,
			    project_in_txt,project_in_menu,
			    project_out_txt,project_out_menu,
			    person_in_txt,person_in_menu,person_out_txt,person_out_menu,
			    sl_in_txt,sl_in_menu,
			    sl_out_txt,sl_out_menu,
			    rel_in_txt,rel_in_menu,
			    repro_in_txt,repro_in_menu,
			    var_in_txt,var_in_menu){

  if (id_filter_on.value==''){
    germ_in_txt.style.display='none';
    germ_in_menu.style.display='none';
    germ_in_liste.style.display='none';
    liste_germ_in.style.display='none';
    germ_out_txt.style.display='none';
    germ_out_menu.style.display='none';
    germ_out_liste.style.display='none';
    liste_germ_out.style.display='none';
    germ_type_in_txt.style.display='none';
    germ_type_in_menu.style.display='none';
    germ_type_out_txt.style.display='none';
    germ_type_out_menu.style.display='none';
    year_in_txt.style.display='none';
    year_in_menu.style.display='none';
    year_out_txt.style.display='none';
    year_out_menu.style.display='none';
    project_in_txt.style.display='none';
    project_in_menu.style.display='none';
    project_out_txt.style.display='none';
    project_out_menu.style.display='none';
    person_in_txt.style.display='none';
    person_in_menu.style.display='none';
    person_in_liste.style.display='none';
    liste_person_in.style.display='none';
    person_out_txt.style.display='none';
    person_out_menu.style.display='none';
    person_out_liste.style.display='none';
    liste_person_out.style.display='none';
    sl_in_txt.style.display='none';
    sl_in_menu.style.display='none';
    sl_in_liste.style.display='none';
    liste_sl_in.style.display='none';
    sl_out_txt.style.display='none';
    sl_out_menu.style.display='none';
    sl_out_liste.style.display='none';
    liste_sl_out.style.display='none';
    rel_in_txt.style.display='none';
    rel_in_menu.style.display='none';
    repro_in_txt.style.display='none';
    repro_in_menu.style.display='none';
    var_in_txt.style.display='none';
    var_in_menu.style.display='none';
    var_in_liste.style.display='none';
    liste_var_in.style.display='none';
  }
  else if (id_filter_on.value!='' && id_query_type.value=="cross"){
    germ_in_txt.style.display='block';
    germ_in_menu.style.display='block';
    germ_in_liste.style.display='block';
    liste_germ_in.style.display='block';
    germ_out_txt.style.display='block';
    germ_out_menu.style.display='block';
    germ_out_liste.style.display='block';
    liste_germ_out.style.display='block';
    germ_type_in_txt.style.display='block';
    germ_type_in_menu.style.display='block';
    germ_type_out_txt.style.display='block';
    germ_type_out_menu.style.display='block';
    year_in_txt.style.display='block';
    year_in_menu.style.display='block';
    year_out_txt.style.display='block';
    year_out_menu.style.display='block';
    project_in_txt.style.display='block';
    project_in_menu.style.display='block';
    project_out_txt.style.display='block';
    project_out_menu.style.display='block';
    person_in_txt.style.display='block';
    person_in_menu.style.display='block';
    person_in_liste.style.display='block';
    liste_person_in.style.display='block';
    person_out_txt.style.display='block';
    person_out_menu.style.display='block';
    person_out_liste.style.display='block';
    liste_person_out.style.display='block';
    sl_in_txt.style.display='none';
    sl_in_menu.style.display='none';
    sl_in_liste.style.display='none';
    liste_sl_in.style.display='none';
    sl_out_txt.style.display='none';
    sl_out_menu.style.display='none';
    sl_out_liste.style.display='none';
    liste_sl_out.style.display='none';
    rel_in_txt.style.display='none';
    rel_in_menu.style.display='none';
    repro_in_txt.style.display='none';
    repro_in_menu.style.display='none';
    var_in_txt.style.display='none';
    var_in_menu.style.display='none';
    var_in_liste.style.display='none';
    liste_var_in.style.display='none';
  }
  else{
    germ_in_txt.style.display='block';
    germ_in_menu.style.display='block';
    germ_in_liste.style.display='block';
    liste_germ_in.style.display='block';
    germ_out_txt.style.display='block';
    germ_out_menu.style.display='block';
    germ_out_liste.style.display='block';
    liste_germ_out.style.display='block';
    germ_type_in_txt.style.display='block';
    germ_type_in_menu.style.display='block';
    germ_type_out_txt.style.display='block';
    germ_type_out_menu.style.display='block';
    year_in_txt.style.display='block';
    year_in_menu.style.display='block';
    year_out_txt.style.display='block';
    year_out_menu.style.display='block';
    project_in_txt.style.display='block';
    project_in_menu.style.display='block';
    project_out_txt.style.display='block';
    project_out_menu.style.display='block';
    person_in_txt.style.display='block';
    person_in_menu.style.display='block';
    person_in_liste.style.display='block';
    liste_person_in.style.display='block';
    person_out_txt.style.display='block';
    person_out_menu.style.display='block';
    person_out_liste.style.display='block';
    liste_person_out.style.display='block';
    sl_in_txt.style.display='block';
    sl_in_menu.style.display='block';
    sl_in_liste.style.display='block';
    liste_sl_in.style.display='block';
    sl_out_txt.style.display='block';
    sl_out_menu.style.display='block';
    sl_out_liste.style.display='block';
    liste_sl_out.style.display='block';
    rel_in_txt.style.display='block';
    rel_in_menu.style.display='block';
    repro_in_txt.style.display='block';
    repro_in_menu.style.display='block';
    var_in_txt.style.display='block';
    var_in_menu.style.display='block';
    var_in_liste.style.display='block';
    liste_var_in.style.display='block';
  }
}

 