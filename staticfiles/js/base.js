$(document).ready(
		function() {
				
			// Fonction js pour merger les reps
			$('[name="mergeform"]').on('submit', function() {
 
		        // je récupère les valeurs
		        var father = $(this).attr('id');
		        //var mail = $(this ).val();
		 
	            // appel Ajax
	            $.ajax({
	                url: $(this).attr('action'), // le nom du fichier indiqué dans le formulaire
	                type: $(this).attr('method'), // la méthode indiquée dans le formulaire (get ou post)
	                data: $(this).serialize(), // je sérialise les données (voir plus loin), ici les $_POST
	                success: function(html) { // je récupère la réponse du fichier PHP
	                    //alert(html); // j'affiche cette réponse
	                    $('input[name="'+father+'"]').replaceWith(html);
	                    //return false;
	                },
	                error: function(jqXHR, textStatus, errorThrown) {
	                	alert(jqXHR.responseText);
	                }
	            });
		        return false; // j'empêche le navigateur de soumettre lui-même le formulaire
	    });
			
		// fonction js pour soumettre les germplasm_type
		$('[name="gpform"]').on('submit', function() {
			$.ajax({
                url: $(this).attr('action'), // le nom du fichier indiqué dans le formulaire
                type: $(this).attr('method'), // la méthode indiquée dans le formulaire (get ou post)
                data: $(this).serialize(), // je sérialise les données (voir plus loin), ici les $_POST
                success: function(html) { // je récupère la réponse du fichier PHP
                    $(':input[name="gptype_submit"]').replaceWith(html);
                	$.each($("select[name^='germplasm']"),function() {
                		$('select[name="'+$(this).attr("name")+'"]').replaceWith($(this).val());
                	});
                	//$.each($("select[name^='specie']"),function() {
                		//$('select[name="'+$(this).attr("name")+'"]').replaceWith($(this).val());
                	//});
                    //return false;
                },
                error: function(jqXHR, textStatus, errorThrown) {
                	alert(jqXHR.responseText);
                }
            });
			return false; // j'empêche le navigateur de soumettre lui-même le formulaire
		});

		// fonction js pour soumettre les reproduction_method
		$('[name="reproductionform"]').on('submit',function() {
			
			$.ajax({
                url: $(this).attr('action'), // le nom du fichier indiqué dans le formulaire
                type: $(this).attr('method'), // la méthode indiquée dans le formulaire (get ou post)
                data: $(this).serialize(), // je sérialise les données (voir plus loin), ici les $_POST
                success: function(html) { // je récupère la réponse du fichier PHP
                    $(':input[name="submitreprochoice"]').replaceWith(html);
                	$.each($("select[name^='reproduction']"),function() {
                		$('select[name="'+$(this).attr("name")+'"]').replaceWith($('select[name="'+$(this).attr("name")+'"] option[value="'+$(this).val()+'"]').text());
                	});
                	
                    //return false;
                },
                error: function(jqXHR, textStatus, errorThrown) {
                	alert(jqXHR.responseText);
                }
            });
			return false; // j'empêche le navigateur de soumettre lui-même le formulaire
		});
		
		$('.hideshow').on('click',function() {
			if ($(this).next('div').is(":visible")) {
				$(this).next('div').hide('slow');
                img_url = ROOT_URL+'/static/images/plus.png';
				$(this).css( 'background-image', "url("+img_url+")" )
			} else {
				$(this).next('div').show('slow');
				img_url = ROOT_URL+'/static/images/minus.png';
				$(this).css( 'background-image', "url("+img_url+")" )
			}
		})
		$('.hideshow_save').on('click',function() {
			if ($(this).next('div').is(":visible")) {
				$(this).next('div').hide('slow');
				img_url = ROOT_URL+'/static/images/save.gif';
				$(this).css( 'background-image', "url('"+img_url+"')" )
			} else {
				$(this).next('div').show('slow');
				img_url = ROOT_URL+'/static/images/save.gif';
				$(this).css( 'background-image', "url('"+img_url+"')" )
			}
		})
		
		var availableTags = [];
		$.ajax({
            url: ROOT_URL.concat("/entities/getgermplasmlist/"),
            type: "POST", // la méthode indiquée dans le formulaire (get ou post)
            success: function(html) { // je récupère la réponse du fichier PHP
            	availableTags = html;
            	
            },
            async: false,
            error: function(jqXHR, textStatus, errorThrown) {
            	alert(jqXHR.responseText);
            }
        });

/*		$("#autocomplete").autocomplete({
		      source: availableTags,
		      minLength: 0
		});*/
		
		$('[name="testselection"]').on('submit',function() {
			$.ajax({
                url: ROOT_URL.concat('/network/testselection/'),
                type: 'POST',
                data: {'selection_name':$('#id_selection_name').val(), 'germplasm':$('#autocomplete').val()},
                success: function(html) {
                    $('#selectionresult').html(html);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                	alert(jqXHR.responseText);
                }
            });
			return false; // j'empêche le navigateur de soumettre lui-même le formulaire
		});
		
		$('#selectall').on('change',function() {
				$('[name="raw_data"]').each(function( index ) {
					if ($('#selectall').prop('checked') == true) {
						$(this).prop('checked', true);
					} else {
						$(this).prop('checked', false);
					}
					
				})
		});
	
		
});

function submit_save(){
// 			      alert("test");
      return false;
	  
}

function test(){
  if ($('#id_group_name').val()!="" && $('#id_analysis_name').val()!=""){
    $.ajax({
	url: "../save_analysis/",
	type: "POST",
	data: $('#myform').serialize(),
	    success: function(html){
		  $('#save_analysis').html(html); //$('#ici').append(html) pour ajouter au texte deja présent dans la div alors que là, ça remplace toute la div 
	    },
	    error: function(XMLHttpRequest, textStatus, errorThrown) { 
		  $('#save_analysis').html("<p style='color:red; font-size:25px;'>NONE</p>"); 
	    } 
    });
  }
}