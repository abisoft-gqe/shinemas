function getCSVData(id){
	var csv_value;
	if ($('#'+id).parent().is(':visible')) {
		csv_value=$('#'+id).table2CSV({delivery:'value'}); 
	}
	else {
		$('#'+id).parent().show();
		csv_value=$('#'+id).table2CSV({delivery:'value'}); 
		$('#'+id).parent().hide();
	}
  	$.ajax({
        url: ROOT_URL.concat("/data/getcsvfile/"),
        type: "POST", 
        data: {'csvstring':csv_value,},
        dataType: "json",
        success: function(result){
        	     var url = result['hash'];
        	     window.location = ROOT_URL.concat("/data/getcsvfile/"+url);
        	  },
        async: false,
        error: function(jqXHR, textStatus, errorThrown) {
        	alert(jqXHR.responseText);
        }
    });
};