$(document).ready(
		function() {
			
			$(function() {
			    $(".elementlist").draggable();
			 });
			
			$('[name="view_list"]').on('click',function() {
				if ($('#listdiv')) {
					$('#listdiv').show();
				}
				
				if ($('#crossdiv')) {
					$('#crossdiv').show();
				}
				
				if ($('#relationdiv')) {
					$('#relationdiv').show();
				}
				
			});
			
			$('[name="close_list"]').on('click',function() {
				if ($('#listdiv')) {
					$('#listdiv').hide();
				}
				
				if ($('#crossdiv')) {
					$('#crossdiv').hide();
				}
				
				if ($('#relationdiv')) {
					$('#relationdiv').hide();
				}
			});
			
			$('[name=femaleicon]').on('click',function(){
				var seedlot_id = $(this).attr('value');
				if ($('#crosstable tr').length == 0) {
					var inittable = '<tr><th></th><th width="240px">Male seed lot</th><th width="240px">Female  seed lot</th></tr>';
					$('#crosstable').append(inittable);
					$('#crosstable').append('<tr><td><img src="'+ROOT_URL+'/static/images/delete.png" name="delete" style="cursor: pointer;"/></td><td align="left"></td><td align="left">'+$(this).attr('value')+'</td></tr>');
					$('[name=export]').show();
					$('.hideshow').show();
				} else {
					var val = "no"
					$.each($('#crosstable tr'),function() {
						if ($('td:eq(2)',this).text() == "" && $('th:eq(2)',this).text() == "") {
							$('td:eq(2)',this).append(seedlot_id);
							val = "yes";
							return false;
						}
					});
					if (val == "no") {
						$('#crosstable tr:last').after('<tr><td><img src="'+ROOT_URL+'/static/images/delete.png" name="delete" style="cursor: pointer;"/></td><td align="left"></td><td align="left">'+$(this).attr('value')+'</td></tr>');
					}	
				}
				$('#crossdiv').scrollTop(450);
				$('[name=delete]').on('click',function(){
					deleteElement($(this));
				});
				editCrossElements();
			});
			
			$('[name=maleicon]').on('click',function(){
				var seedlot_id = $(this).attr('value');
				if ($('#crosstable tr').length == 0) {
					var inittable = '<tr><th></th><th width="240px">Male  seed lot</th><th width="240px">Female  seed lot</th></tr>';
					$('#crosstable').append(inittable);
					$('#crosstable').append('<tr><td><img src="'+ROOT_URL+'/static/images/delete.png" name="delete" style="cursor: pointer;"/></td><td align="left">'+$(this).attr('value')+'</td><td align="left"></td></tr>');
					$('[name=export]').show();
					$('.hideshow').show();
				} else {
					var val = "no"
						$.each($('#crosstable tr'),function() {
							if ($('td:eq(1)',this).text() == "" && $('th:eq(1)',this).text() == "") {
								$('td:eq(1)', this).append(seedlot_id);
								val = "yes";
								return false;
							}
						});
						if (val == "no") {
							$('#crosstable tr:last').after('<tr><td><img src="'+ROOT_URL+'/static/images/delete.png" name="delete" style="cursor: pointer;"/></td><td align="left">'+$(this).attr('value')+'</td><td align="left"></td></tr>');
						}
				}
				$('#crossdiv').scrollTop(450);
				$('[name=delete]').on('click',function(){
					deleteElement($(this));
				});
				editCrossElements();
			});
			
			$('[name="add-all"]').on('click',function() {
				$( '[name="add-diff"]' ).each(function( index ) {
					var seedlot_id = $(this).attr('value');
					var receiver = $('[name=sendto] option:selected').text();
					texttoshow = seedlot_id+"-->"+receiver;
					addElementToList(texttoshow, "current_list");
					});
				
				$('[name=add-mixt]').each(function( index ) {
					var seedlot_id = $(this).attr('value');
					var receiver = $('[name=mixture_name]').val();
					texttoshow = seedlot_id+"-->"+receiver;
					addElementToList(texttoshow, "current_list");
				});
				
				$('[name=addicon]').each(function( index ) {
					var seedlot_id = $(this).attr('value');
					addElementToList(seedlot_id, "current_list");
				});
				
			});
			
			$('[name=addicon]').on('click',function(){
				var seedlot_id = $(this).attr('value');
				addElementToList(seedlot_id, "current_list");
			});
			
			$('[name=add-diff]').on('click',function(){
				var seedlot_id = $(this).attr('value');
				var receiver = $('[name=sendto] option:selected').text();
				texttoshow = seedlot_id+"-->"+receiver;
				addElementToList(texttoshow, "current_list");
			});
			
			
			$('[name=add-mixt]').on('click',function(){
				var seedlot_id = $(this).attr('value');
				var receiver = $('[name=mixture_name]').val();
				texttoshow = seedlot_id+"-->"+receiver;
				addElementToList(texttoshow, "current_list");
			});
			
			$('[name=delete]').on('click',function(){
				deleteElement($(this));
			});
			
			var deleteElement = function ($delicon) {
				$delicon.closest("tr").remove();
				if ($delicon.attr('value')) {
					var id = $delicon.attr('value').replace('#','\\#').replace('>','\\>');
					id = id.replace('&','\\&').replace(')','\\)').replace('(','\\(').replace(/\s/g,'\\ ');
					$("#list_"+id).remove();
				} else {
					editCrossElements();
				}
			}
			
			var addElementToList = function($texttoshow, $listname) {
				if ($('#listtable tr').length == 0) {
					var inittable = '<th width="240px"><u><b>Selected seed lot :</b></u></th>';
					$('#listtable').append(inittable);
					$('#listtable').append('<tr><td align="left"><img src="'+ROOT_URL+'/static/images/delete.png" name="delete" style="cursor: pointer;" value="'+$texttoshow+'"/>'+$texttoshow+'<input type="hidden" name="finallist" value="'+$texttoshow+'" /></td></tr>');
					$('[name=export]').show();
					$('.hideshow').show();
				} else {
					$('#listtable tr:last').after('<tr><td align="left"><img src="'+ROOT_URL+'/static/images/delete.png" name="delete" style="cursor: pointer;" value="'+$texttoshow+'"/>'+$texttoshow+'<input type="hidden" name="finallist" value="'+$texttoshow+'" /></td></tr>');
				}
				$('#listdiv').scrollTop(450);
				$('.formfield').after('<input type="hidden" name="'+$listname+'" id="list_'+$texttoshow+'" value="'+$texttoshow+'"/>');
				$('[name=delete]').on('click',function(){
					deleteElement($(this));
				});
			}
			
			var editCrossElements = function() {
				$("[name=current_list]").remove();
				$("[name=finallist]").remove();
				var finallist = "";
				$.each($('#crosstable tr'),function() {
					if ($('td:eq(1)',this).text() || $('td:eq(2)',this).text()) {
						$('.formfield').after('<input type="hidden" name="current_list" value="'+$('td:eq(1)',this).text()+';'+$('td:eq(2)',this).text()+'"/>');
						finallist = finallist+$('td:eq(1)',this).text()+";"+$('td:eq(2)',this).text()+"&";
					}
				});
				$('#crosstable').after('<input type="hidden" name="finallist" value="'+finallist+'"/>');
			}
		});
