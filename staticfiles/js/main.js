jQuery(function($){

	$.datepicker.setDefaults($.datepicker.regional['fr']);

	//$('.datepicker').datepicker('desable')
	//$('.datepicker').datepicker('enable')
	
	
	var datepickers = $('.datepicker').datepicker({
		dateFormat: 'yy-mm-dd',
		changeYear: true
	});

	$(document).ready(function(){		
		if ($('input[type=radio][name=mode]:checked').val() == 'measures') {
					$('#period').show();
		}
		else {
			$('#period').hide();
		}
			
		$(':input[type=radio][name=mode]').on('change', function() {
			if ($('input[type=radio][name=mode]:checked').val() == 'measures') {
					$('#period').show();
			}
			else {
				$('#period').hide();
			}
		});
		
		$(':input[name$=period]').on('change', function() {
			
			
	        // Get the field prefix, ie. if this comes from a formset form
	        var prefix = $(this).getFormPrefix();

	        // Clear the autocomplete with the same prefix
	        $(':input[name=' + prefix + 'var]').val(null).trigger('change');
	        $(':input[name=' + prefix + 'station1]').val(null).trigger('change');
	        $(':input[name=' + prefix + 'station2]').val(null).trigger('change');
	        $(':input[name=' + prefix + 'station3]').val(null).trigger('change');
	    });
		
		$(':input[name$=location]').on('change', function() {
	        // Get the field prefix, ie. if this comes from a formset form
	        var prefix = $(this).getFormPrefix();

	        // Clear the autocomplete with the same prefix
	        $(':input[name=' + prefix + 'station1]').val(null).trigger('change');
	        $(':input[name=' + prefix + 'station2]').val(null).trigger('change');
	        $(':input[name=' + prefix + 'station3]').val(null).trigger('change');
	    });
	});

	
//je bloque les input date et variable si le boutton mesures n'est pas selectionné

/*$('input[type=radio][name=mode]').change(function() {
        if (this.value == 'measures') {
        	$('#period').show();
        }
        else {
        	$('#period').hide();
        }
        if (this.value == 'relation') {
        	$('#id_relation').val(0);
        	$('#id_relation_exclude').prop('checked', false);
        	$('#id_relation').attr("disabled", "disabled");
        	$('#id_relation_exclude').attr("disabled", "disabled");
        }
        else {
        	$('#id_relation').removeAttr("disabled");
        	$('#id_relation_exclude').removeAttr("disabled", "disabled");
        }
			
    }); */


// je cree un autocomplete contenant les variables et je stocke les variables selectionnees dans un tableau <table name="liste_var">

	var objVar, arrayVar, iter ;  
	var arrayVerif = new Array();
	
	
	
	try{
		objVar = document.getElementById("var_in").textContent;
	}
	catch (typeError){
		objVar="";
	} 
	
	arrayVar = objVar.split("\n");	
	iter = 0;


	$( "#var" ).autocomplete({
	source: arrayVar,
	minLength : 1,
	select: function( event, ui ) {
		            event.preventDefault(); //j'empêche le navigateur de soumettre le formulaire (attention plusieurs methodes selon navigateur: a valider!!) 

		            $( "#var" ).val(ui.item.label);
					
					iter = iter + 1
 
		           if ($.inArray(ui.item.value, arrayVerif) === -1){
		            	arrayVerif.push(ui.item.value);
					 	$( "#liste_var" ).append('<tr><td align= ' + '"' + 'center'+ '"' + ' ><img src= ' + '"' + '/static/static/admin/img/admin/icon_deletelink.gif'+ '"' + 
					 	' name=' + '"' + 'delete' + '"' + ' style='+ '"'  + 'cursor: pointer;'+ '"' + ' value='+ '"' + ui.item.value + '"' + '/><input type=' + '"' + 'hidden' +
					 	'"' + 'name=' + '"' +'liste_var' + '"' + ' value='+ '"' + ui.item.value + '"' + '/></td><td value='+'\"'+iter+'\"'+'>'+ui.item.value+' '+'</td></tr>');
					 	}
				   else{
					   	alert('Cette variable est déjà dans la liste');   
				   }
				 	

				 	$('[name=delete]').on('click',function(){
							deleteElement($(this));
					});
				 	

					var deleteElement = function ($delicon) {
						$delicon.closest("tr").remove();
						var id = $delicon.attr('value').replace('#','\\#').replace('>','\\>');
						id = id.replace('&','\\&').replace(')','\\)').replace('(','\\(');
						$(id).remove();
						arrayVerif.splice(arrayVerif.indexOf(id),1);
					}
		}
	})  
	
	/*$('#submit').on('click', function () {
		$('.selector').autocomplete('search' , '');
}); */

$('.hideshow').on('click',function() {
		if ($(this).next('div').is(":visible")) {
			$(this).next('div').hide('slow');
			$(this).css( 'background-image', "url('/static/images/plus.png')" )
		} else {
			$(this).next('div').show('slow');
			$(this).css( 'background-image', "url('/static/images/minus.png')" )
		}
})
$('.hideshow_save').on('click',function() {
			if ($(this).next('div').is(":visible")) {
				$(this).next('div').hide('slow');
				$(this).css( 'background-image', "url('/static/images/save.gif')" )
			} else {
				$(this).next('div').show('slow');
				$(this).css( 'background-image', "url('/static/images/save.gif')" )
			}
		})
// callback Ajax pour datepicker

$.ajax({
    
    url: $(this).attr('action'), 
    type: $(this).attr('method'), 
    data: $(this).serialize(),     
    success: function(html) { 

       start = $(':input[name="start"]').val();
       end = $(':input[name="end"]').val();
       
    },
    error: function(jqXHR, textStatus, errorThrown) {
    	alert(jqXHR.responseText);
    }
});


return false; // j'empêche le navigateur de soumettre lui-même le formulaire
});






/*

//je cree un autocomplete contenant les germplasm et je les selectionnees dans un tableau <table name="liste_germ">

	var objVar, arrayVar, iter ;  
	var arrayVerif = new Array();
	
	
	
	try{
		objVar = document.getElementById("germ_in").textContent;
	}
	catch (typeError){
		objVar="";
	} 
	
	arrayVar = objVar.split("\n");	
	iter = 0;


	$( "#varg" ).autocomplete({
	source: arrayVar,
	minLength : 1,
	select: function( event, ui ) {
		            event.preventDefault(); //j'empêche le navigateur de soumettre le formulaire (attention plusieurs methodes selon navigateur: a valider!!) 

		            $( "#varg" ).val(ui.item.label);
					
					iter = iter + 1

		           if ($.inArray(ui.item.value, arrayVerif) === -1){
		            	arrayVerif.push(ui.item.value);
					 	$( "#liste_germ" ).append('<tr><td align= ' + '"' + 'center'+ '"' + ' ><img src= ' + '"' + '/media/static/admin/img/admin/icon_deletelink.gif'+ '"' + 
					 	' name=' + '"' + 'delete' + '"' + ' style='+ '"'  + 'cursor: pointer;'+ '"' + ' value='+ '"' + ui.item.value + '"' + '/><input type=' + '"' + 'hidden' +
					 	'"' + 'name=' + '"' +'liste_var' + '"' + ' value='+ '"' + ui.item.value + '"' + '/></td><td value='+'\"'+iter+'\"'+'>'+ui.item.value+' '+'</td></tr>');
					 	}
				   else{
					   	alert('Cette variable est déjà dans la liste');   
				   }
				 	

				 	$('[name=delete]').on('click',function(){
							deleteElement($(this));
					});
			

					var deleteElement = function ($delicon) {
						$delicon.closest("tr").remove();
						var id = $delicon.attr('value').replace('#','\\#').replace('>','\\>');
						id = id.replace('&','\\&').replace(')','\\)').replace('(','\\(');
						$(id).remove();
						arrayVerif.splice(arrayVerif.indexOf(id),1);
					}
		}
	})  

$('.hideshow').on('click',function() {
		if ($(this).next('div').is(":visible")) {
			$(this).next('div').hide('slow');
			$(this).css( 'background-image', "url('/media/images/plus.png')" )
		} else {
			$(this).next('div').show('slow');
			$(this).css( 'background-image', "url('/media/images/minus.png')" )
		}
})
$('.hideshow_save').on('click',function() {
			if ($(this).next('div').is(":visible")) {
				$(this).next('div').hide('slow');
				$(this).css( 'background-image', "url('/media/images/save.gif')" )
			} else {
				$(this).next('div').show('slow');
				$(this).css( 'background-image', "url('/media/images/save.gif')" )
			}
		})
//callback Ajax pour datepicker

$.ajax({

url: $(this).attr('action'), 
type: $(this).attr('method'), 
data: $(this).serialize(),     
success: function(html) { 

  start = $(':input[name="start"]').val();
  end = $(':input[name="end"]').val();
  
},
error: function(jqXHR, textStatus, errorThrown) {
	alert(jqXHR.responseText);
}
});


return false; // j'empêche le navigateur de soumettre lui-même le formulaire
});


	*/
	
	






