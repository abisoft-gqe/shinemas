
for (var x=1;x<7;x++){
      document.getElementById('id_hide_labels_parts').options[x].style.display='none';
}


function ggplotNetwork_type_lies(id_ggplot_type,id_x_axis,id_in_col,id_hide_labels_parts,id_ggplot_display,organise_sl_txt,organise_sl_menu,
				 vertex_size_txt,vertex_size_menu,vertex_color_txt,vertex_color_menu,
				 ggplot_display_txt,ggplot_display_menu,x_axis_txt,x_axis_menu,in_col_txt,in_col_menu,
				 param_x_axis_txt,param_x_axis_menu,param_in_col_txt,param_in_col_menu,location_map_txt,location_map_menu,
				 pie_size_txt,pie_size_menu,hide_labels_parts_txt,hide_labels_parts_menu,
				  labels_size_txt,labels_size_menu,
				 display_labels_sex_txt,display_labels_sex_menu,labels_generation_txt,labels_generation_menu)			 
{
  var x = 1;
  for (x=1;x<4;x++){
    id_x_axis.options[x].style.display='block';
    id_in_col.options[x].style.display='block';
  }
  
  for (var x=1;x<7;x++){
    if(id_ggplot_type.value=="network-network"){
      id_hide_labels_parts.options[x].style.display='block';
    }
    else{
      id_hide_labels_parts.options[x].style.display='none';
    }
  }
  
  if (id_ggplot_type.value=="network-network"){
    organise_sl_txt.style.display='block';
    organise_sl_menu.style.display='block';
    vertex_size_txt.style.display='block';
    vertex_size_menu.style.display='block';
    vertex_color_txt.style.display='block';
    vertex_color_menu.style.display='block';
    ggplot_display_txt.style.display='none';
    ggplot_display_menu.style.display='none';
    x_axis_txt.style.display='none';
    x_axis_menu.style.display='none';
    in_col_txt.style.display='none';
    in_col_menu.style.display='none';
    param_x_axis_txt.style.display='none';
    param_x_axis_menu.style.display='none';
    param_in_col_txt.style.display='none';
    param_in_col_menu.style.display='none';
    location_map_txt.style.display='none';
    location_map_menu.style.display='none';
    pie_size_txt.style.display='none';
    pie_size_menu.style.display='none';
    hide_labels_parts_txt.style.display='block';
    hide_labels_parts_menu.style.display='block';
    id_hide_labels_parts.value='all';
    labels_size_txt.style.display='none';
    labels_size_menu.style.display='none';
    display_labels_sex_txt.style.display='none';
    display_labels_sex_menu.style.display='none';
    labels_generation_txt.style.display='none';
    labels_generation_menu.style.display='none';
    id_x_axis.value="---------";
    id_in_col.value="---------";
    id_ggplot_display.value="both";
  }
  else if(id_ggplot_type.value=="network-diffusion-relation"){
    organise_sl_txt.style.display='block';
    organise_sl_menu.style.display='block';
    vertex_size_txt.style.display='none';
    vertex_size_menu.style.display='none';
    vertex_color_txt.style.display='none';
    vertex_color_menu.style.display='none';
    ggplot_display_txt.style.display='none';
    ggplot_display_menu.style.display='none';
    x_axis_txt.style.display='none';
    x_axis_menu.style.display='none';
    in_col_txt.style.display='none';
    in_col_menu.style.display='none';
    param_x_axis_txt.style.display='none';
    param_x_axis_menu.style.display='none';
    param_in_col_txt.style.display='none';
    param_in_col_menu.style.display='none';
    location_map_txt.style.display='block';
    location_map_menu.style.display='block';
    pie_size_txt.style.display='block';
    pie_size_menu.style.display='block';
    hide_labels_parts_txt.style.display='block';
    hide_labels_parts_menu.style.display='block';
    id_hide_labels_parts.value='all';
    labels_size_txt.style.display='none';
    labels_size_menu.style.display='none';
    display_labels_sex_txt.style.display='none';
    display_labels_sex_menu.style.display='none';
    labels_generation_txt.style.display='none';
    labels_generation_menu.style.display='none';  
    id_x_axis.value="---------";
    id_in_col.value="---------";
    id_ggplot_display.value="map";
  }
  else if(id_ggplot_type.value=="---------"){
    organise_sl_txt.style.display='block';
    organise_sl_menu.style.display='block';
    vertex_size_txt.style.display='block';
    vertex_size_menu.style.display='block';
    vertex_color_txt.style.display='block';
    vertex_color_menu.style.display='block';
    ggplot_display_txt.style.display='block';
    ggplot_display_menu.style.display='block';
    x_axis_txt.style.display='block';
    x_axis_menu.style.display='block';
    in_col_txt.style.display='block';
    in_col_menu.style.display='block';
    param_x_axis_txt.style.display='block';
    param_x_axis_menu.style.display='block';
    param_in_col_txt.style.display='block';
    param_in_col_menu.style.display='block';
    location_map_txt.style.display='block';
    location_map_menu.style.display='block';
    pie_size_txt.style.display='block';
    pie_size_menu.style.display='block';
    hide_labels_parts_txt.style.display='block';
    hide_labels_parts_menu.style.display='block';
    id_hide_labels_parts.value='all';
    labels_size_txt.style.display='none';
    labels_size_menu.style.display='none';
    display_labels_sex_txt.style.display='none';
    display_labels_sex_menu.style.display='none';
    labels_generation_txt.style.display='none';
    labels_generation_menu.style.display='none';
    id_x_axis.value="---------";
    id_in_col.value="---------";
    id_ggplot_display.value="both";
  }
  else{
    organise_sl_txt.style.display='block';
    organise_sl_menu.style.display='block';
    vertex_size_txt.style.display='none';
    vertex_size_menu.style.display='none';
    vertex_color_txt.style.display='none';
    vertex_color_menu.style.display='none';
    ggplot_display_txt.style.display='block';
    ggplot_display_menu.style.display='block';
    x_axis_txt.style.display='block';
    x_axis_menu.style.display='block';
    in_col_txt.style.display='block';
    in_col_menu.style.display='block';
    param_x_axis_txt.style.display='block';
    param_x_axis_menu.style.display='block';
    param_in_col_txt.style.display='block';
    param_in_col_menu.style.display='block';
    location_map_txt.style.display='block';
    location_map_menu.style.display='block';
    pie_size_txt.style.display='block';
    pie_size_menu.style.display='block';
    hide_labels_parts_txt.style.display='block';
    hide_labels_parts_menu.style.display='block';
    id_hide_labels_parts.value='all';
    labels_size_txt.style.display='none';
    labels_size_menu.style.display='none';
    display_labels_sex_txt.style.display='none';
    display_labels_sex_menu.style.display='none';
    labels_generation_txt.style.display='none';
    labels_generation_menu.style.display='none';
    id_x_axis.value="---------";
    id_in_col.value="---------";
    id_ggplot_display.value="both";
  }
}

function ggplotNetwork_hide_labels_lies(id_hide_labels_parts,id_ggplot_type,labels_size_txt,labels_size_menu,display_labels_sex_txt,display_labels_sex_menu,labels_generation_txt,labels_generation_menu)
{
  if (id_hide_labels_parts.value!="all"){
    labels_size_txt.style.display='block';
    labels_size_menu.style.display='block';
    display_labels_sex_txt.style.display='block';
    display_labels_sex_menu.style.display='block';
    labels_generation_txt.style.display='block';
    labels_generation_menu.style.display='block';
  }

  else{
    labels_size_txt.style.display='none';
    labels_size_menu.style.display='none';
    display_labels_sex_txt.style.display='none';
    display_labels_sex_menu.style.display='none';
    labels_generation_txt.style.display='none';
    labels_generation_menu.style.display='none';
  }
}


function ggplotNetwork_display_lies(id_ggplot_display,x_axis_txt,x_axis_menu,in_col_txt,in_col_menu,param_x_axis_txt,param_x_axis_menu,param_in_col_txt,param_in_col_menu,location_map_txt,location_map_menu,pie_size_txt,pie_size_menu)
{
  if (id_ggplot_display.value=="map"){
    x_axis_txt.style.display='none';
    x_axis_menu.style.display='none';
    in_col_txt.style.display='none';
    in_col_menu.style.display='none';
    param_x_axis_txt.style.display='none';
    param_x_axis_menu.style.display='none';
    param_in_col_txt.style.display='none';
    param_in_col_menu.style.display='none';
    location_map_txt.style.display='block';
    location_map_menu.style.display='block';
    pie_size_txt.style.display='block';
    pie_size_menu.style.display='block';
  }
  
  else if(id_ggplot_display.value=="barplot"){
    x_axis_txt.style.display='block';
    x_axis_menu.style.display='block';
    in_col_txt.style.display='block';
    in_col_menu.style.display='block';
    param_x_axis_txt.style.display='block';
    param_x_axis_menu.style.display='block';
    param_in_col_txt.style.display='block';
    param_in_col_menu.style.display='block';
    location_map_txt.style.display='none';
    location_map_menu.style.display='none';
    pie_size_txt.style.display='none';
    pie_size_menu.style.display='none';
  }
  
  else{
    x_axis_txt.style.display='block';
    x_axis_menu.style.display='block';
    in_col_txt.style.display='block';
    in_col_menu.style.display='block';
    param_x_axis_txt.style.display='block';
    param_x_axis_menu.style.display='block';
    param_in_col_txt.style.display='block';
    param_in_col_menu.style.display='block';
    location_map_txt.style.display='block';
    location_map_menu.style.display='block';
    pie_size_txt.style.display='block';
    pie_size_menu.style.display='block';
  }
}

function ggplot_x_axis_lies(id_x_axis,id_in_col)
{
  var x = 1;
  for (x=1;x<4;x++){
    if (id_x_axis.value==id_x_axis.options[x].text){
      id_in_col.options[x].style.display='none';
    }
    else{
      id_in_col.options[x].style.display='block';
    }
  }
}
  
function ggplot_in_col_lies(id_in_col,id_x_axis)
{
  var x = 1;
  for (x=1;x<4;x++){
    if (id_in_col.value==id_in_col.options[x].text){
      id_x_axis.options[x].style.display='none';
    }
    else{
      id_x_axis.options[x].style.display='block';
    }
  }
}
  
  
  
function ggplotData_type_lies(id_data_ggplot_type,data_labels_on_menu,data_labels_on_txt,data_hide_labels_parts_menu,data_hide_labels_parts_txt,data_location_map_menu,data_location_map_txt,
  data_pie_size_menu,data_pie_size_txt,data_vertex_size_menu,data_vertex_size_txt,data_vertex_color_menu,data_vertex_color_txt,data_organise_sl_menu,data_organise_sl_txt,
  data_x_axis_menu,data_x_axis_txt,data_in_col_menu,data_in_col_txt,data_param_x_axis_menu,data_param_x_axis_txt,data_param_in_col_menu,data_param_in_col_txt,id_data_hide_labels_parts,id_data_x_axis,id_data_in_col){
//   if (id_query_type.value=="data-classic"){
//     id_data_ggplot_type.options[6].style.display="block";
//     id_data_ggplot_type.options[7].style.display="block";
//   }
//   else{
//     id_data_ggplot_type.options[6].style.display="none";
//     id_data_ggplot_type.options[7].style.display="none";      
//   }
//   
  var x = 1;
  for (x=1;x<4;x++){
    id_data_x_axis.options[x].style.display='block';
    id_data_in_col.options[x].style.display='block';
  }
  
  if (id_data_ggplot_type.value==""){
    data_labels_on_menu.style.display="block";
    data_labels_on_txt.style.display="block";
    data_hide_labels_parts_menu.style.display="block";
    data_hide_labels_parts_txt.style.display="block";
    data_location_map_menu.style.display="block";
    data_location_map_txt.style.display="block";
    data_pie_size_menu.style.display="block";
    data_pie_size_txt.style.display="block";
    data_vertex_size_menu.style.display="block";
    data_vertex_size_txt.style.display="block";
    data_vertex_color_menu.style.display="block";
    data_vertex_color_txt.style.display="block";
    data_organise_sl_menu.style.display="block";
    data_organise_sl_txt.style.display="block";
    data_x_axis_menu.style.display="none";
    data_x_axis_txt.style.display="none";
    data_in_col_menu.style.display="none";
    data_in_col_txt.style.display="none";
    data_param_x_axis_menu.style.display="none";
    data_param_x_axis_txt.style.display="none";
    data_param_in_col_menu.style.display="none";
    data_param_in_col_txt.style.display="none";
    id_data_hide_labels_parts.value="all";
  }
    
  else if (id_data_ggplot_type.value=="data-barplot" || id_data_ggplot_type.value=="data-interaction" || id_data_ggplot_type.value=="data-boxplot"){
    data_x_axis_menu.style.display="block";
    data_x_axis_txt.style.display="block";
    data_in_col_menu.style.display="block";
    data_in_col_txt.style.display="block";
    data_param_x_axis_menu.style.display="block";
    data_param_x_axis_txt.style.display="block";
    data_param_in_col_menu.style.display="block";
    data_param_in_col_txt.style.display="block";
    data_labels_on_menu.style.display="none";
    data_labels_on_txt.style.display="none";
    data_hide_labels_parts_menu.style.display="none";
    data_hide_labels_parts_txt.style.display="none";
    data_location_map_menu.style.display="none";
    data_location_map_txt.style.display="none";
    data_pie_size_menu.style.display="none";
    data_pie_size_txt.style.display="none";
    data_vertex_size_menu.style.display="none";
    data_vertex_size_txt.style.display="none";
    data_vertex_color_menu.style.display="none";
    data_vertex_color_txt.style.display="none";
    data_organise_sl_menu.style.display="none";
    data_organise_sl_txt.style.display="none"; 
    id_data_hide_labels_parts.value="all";
  }
  
  else if (id_data_ggplot_type.value=="data-biplot" || id_data_ggplot_type.value=="data-radar"){
    data_x_axis_menu.style.display="none";
    data_x_axis_txt.style.display="none";
    data_in_col_menu.style.display="block";
    data_in_col_txt.style.display="block";
    data_param_x_axis_menu.style.display="none";
    data_param_x_axis_txt.style.display="none";
    data_param_in_col_menu.style.display="block";
    data_param_in_col_txt.style.display="block";
    data_labels_on_menu.style.display="block";
    data_labels_on_txt.style.display="block";
    data_hide_labels_parts_menu.style.display="block";
    data_hide_labels_parts_txt.style.display="block";
    data_location_map_menu.style.display="none";
    data_location_map_txt.style.display="none";
    data_pie_size_menu.style.display="none";
    data_pie_size_txt.style.display="none";
    data_vertex_size_menu.style.display="none";
    data_vertex_size_txt.style.display="none";
    data_vertex_color_menu.style.display="none";
    data_vertex_color_txt.style.display="none";
    data_organise_sl_menu.style.display="none";
    data_organise_sl_txt.style.display="none";
    id_data_hide_labels_parts.value="all";
    
  }
    
  else if (id_data_ggplot_type.value=="data-pie.on.network"){
    data_x_axis_menu.style.display="none";
    data_x_axis_txt.style.display="none";
    data_in_col_menu.style.display="block";
    data_in_col_txt.style.display="block";
    data_param_x_axis_menu.style.display="none";
    data_param_x_axis_txt.style.display="none";
    data_param_in_col_menu.style.display="block";
    data_param_in_col_txt.style.display="block";
    data_labels_on_menu.style.display="block";
    data_labels_on_txt.style.display="block";
    data_hide_labels_parts_menu.style.display="block";
    data_hide_labels_parts_txt.style.display="block";
    data_location_map_menu.style.display="none";
    data_location_map_txt.style.display="none";
    data_pie_size_menu.style.display="block";
    data_pie_size_txt.style.display="block";
    data_vertex_size_menu.style.display="block";
    data_vertex_size_txt.style.display="block";
    data_vertex_color_menu.style.display="block";
    data_vertex_color_txt.style.display="block";
    data_organise_sl_menu.style.display="block";
    data_organise_sl_txt.style.display="block";
    id_data_hide_labels_parts.value="all";
  }
  
  else if (id_data_ggplot_type.value=="data-pie.on.map"){
    data_x_axis_menu.style.display="none";
    data_x_axis_txt.style.display="none";
    data_in_col_menu.style.display="none";
    data_in_col_txt.style.display="none";
    data_param_x_axis_menu.style.display="none";
    data_param_x_axis_txt.style.display="none";
    data_param_in_col_menu.style.display="none";
    data_param_in_col_txt.style.display="none";
    data_labels_on_menu.style.display="block";
    data_labels_on_txt.style.display="block";
    data_hide_labels_parts_menu.style.display="block";
    data_hide_labels_parts_txt.style.display="block";
    data_location_map_menu.style.display="block";
    data_location_map_txt.style.display="block";
    data_pie_size_menu.style.display="block";
    data_pie_size_txt.style.display="block";
    data_vertex_size_menu.style.display="none";
    data_vertex_size_txt.style.display="none";
    data_vertex_color_menu.style.display="none";
    data_vertex_color_txt.style.display="none";
    data_organise_sl_menu.style.display="none";
    data_organise_sl_txt.style.display="none";
    id_data_hide_labels_parts.value="all";
  }
} 

function ggplotData_hide_labels_lies(id_data_hide_labels_parts,data_labels_size_menu,data_labels_size_txt){
  if (id_data_hide_labels_parts.value=="all"){
    data_labels_size_menu.style.display="none";
    data_labels_size_txt.style.display="none";
  }
  else{
    data_labels_size_menu.style.display="block";
    data_labels_size_txt.style.display="block";
  }
}