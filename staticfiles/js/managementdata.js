
$(document).ready(
	
	function(){
		var i = 1;
		$(document).on('click','[name=addicon]',function(){
			var line = $(this).closest('tr');			
			$('#var_table tr:last').after("<tr>"+line.html()+"</tr>");
			$('#var_table tr:last').find('#id_form-0-variable').attr('id','id_form-'+i+'-variable');
			$('#var_table tr:last').find('#id_form-0-raw_data').attr('id','id_form-'+i+'-raw_data');
			$('#var_table tr:last').find('#id_form-0-method').attr('id','id_form-'+i+'-method');
			$('#var_table tr:last').find('#id_form-0-date').attr('id','id_form-'+i+'-date');
			$('#var_table tr:last').find('[name=form-0-variable]').attr('name','form-'+i+'-variable');
			$('#var_table tr:last').find('[name=form-0-raw_data]').attr('name','form-'+i+'-raw_data');
			$('#var_table tr:last').find('[name=form-0-method]').attr('name','form-'+i+'-method');
			$('#var_table tr:last').find('[name=form-0-date]').attr('name','form-'+i+'-date');
			$('#var_table tr:last td:last input').attr('id','id_date_'+i);
			$('#var_table tr:last').find('td:first').html('<img src="'+ROOT_URL+'/static/images/delete.png" name="delete" style="cursor: pointer;"/>');
			$('#var_table tr:last td:last .datepicker').removeClass('hasDatepicker').datepicker({ dateFormat: 'dd/mm/yy' });
			i = i+1;
			var total = parseInt($('#id_form-TOTAL_FORMS').val());
			total = total+1;
			var total = $('#id_form-TOTAL_FORMS').val(total);
		});
		
		$(document).on('click','[name=delete]',function(){
			$(this).closest("tr").remove();
			var total = parseInt($('#id_form-TOTAL_FORMS').val());
			total = total-1;
			var total = $('#id_form-TOTAL_FORMS').val(total);
			var k = -1;
			$('#var_table > tbody  > tr').each(function() {
				$(this).find('td').eq(1).find("select").attr('id','id_form-'+k.toString()+'-variable');
				$(this).find('td').eq(1).find("select").attr('name','form-'+k.toString()+'-variable');

				$(this).find('td').eq(2).find("input").attr('id','id_form-'+k+'-raw_data');
				$(this).find('td').eq(2).find("input").attr('name','form-'+k+'-raw_data');
				
				$(this).find('td').eq(3).find("select").attr('id','id_form-'+k+'-method');
				$(this).find('td').eq(3).find("select").attr('name','form-'+k+'-method');
				
				$(this).find('td').eq(4).find("input").attr('id','id_form-'+k+'-date');
				$(this).find('td').eq(4).find("input").attr('name','form-'+k+'-date');
				k=k+1;
			});
			$(document).each('.datepicker',function(){
				$(this).datepicker({ dateFormat: 'dd/mm/yy' });
			});
			
		});
		
		
		$('.datepicker').each(function(){
			$(this).datepicker({ dateFormat: 'dd/mm/yy' });
		});
	}
);
