
function autocomp(liste_source,test,liste_var,nul){
    var objGerm, arrayGerm, iter;
    var arrayVerif=new Array();
    
    try{
	obj=document.getElementById(liste_source).textContent;
    }
    catch(typeError){
	obj="";
    }
   
    arrayObj=obj.split("\n");
    iter=0;

    $(test).autocomplete({
    source: arrayObj,
    minLength: 1,
    select: function ( event, ui ) {
			
			event.defaultPrevented; //j'empêche le navigateur de soumettre le formulaire (attention plusieurs methodes selon navigateur: a valider!!) 

			$(test).val(ui.item.value);
				    
				    iter = iter + 1;
			
			if ($.inArray(ui.item.value, arrayVerif) === -1){
			    $(nul).remove();
			    arrayVerif.push(ui.item.value);
			    $(liste_var).append('<p style='+'"'+'margin-top:2px;margin-bottom:0px;margin-left:20px'+'"'+'><img src= ' + '"' + '/media/images/cross.png'+ '"' + 
			    ' name=' + '"' + 'delete' + '"' + ' style='+ '"'  + 'height:10px;cursor: pointer; margin-left:10px; margin-right:10px;'+ '"' + ' value='+ '"' + ui.item.value + '"' + '/><input type=' + '"' + 'hidden' +
			    '"' + 'name=' +'\"' +liste_var + '\"' + ' value='+ '"' + ui.item.value + '"' + '/><td value='+'\"'+iter+'\"'+'>'+ui.item.value+' '+'</td></p>');
			}
			
			else{
				    alert('Cet élément est déjà dans la liste');   
			}
				    
			$('[name=delete]').on('click',function(){
					deleteElement($(this));
			});
			
			
			var deleteElement = function ($delicon) {
				$delicon.closest("p").remove();
				var id = $delicon.attr('value').replace('#','\\#').replace('>','\\>');
				id = id.replace('&','\\&').replace(')','\\)').replace('(','\\(');
				$(id).remove();
				arrayVerif.splice(arrayVerif.indexOf(id),1);
			}
			
	  }
    })

} 