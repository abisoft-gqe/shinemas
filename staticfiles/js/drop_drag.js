$(document).ready(function() {
	$(".fs").sortable({
		connectWith: ".fs,#trash",
		tolerance: "pointer"
	}).disableSelection();
	
	$("#content2 .item_drag").draggable({
		connectToSortable: ".fs",
		revert: "invalid",
		helper: "clone"
	});
	
	$("#trash").droppable({
	  accept: ".fs div",
	  hoverClass: "ui-state-hover",
	  drop: function(ev, ui) {
	      ui.draggable.remove();
	  }
	});
	

	$(".figure").sortable({
		connectWith: ".figure",
		tolerance: "pointer"
	}).disableSelection();

	$("#content2 .fig_drag").draggable({
		connectToSortable: ".figure",
		revert: "invalid",
		helper: "clone"
	});
});

function supprimer(suppr){
    if (confirm("Voulez-vous vraiment supprimer ce template?")){
	suppr.remove();
	val=suppr.getAttribute('value');
	$.ajax({
	    type:'POST',
	    url:'../change_template/',
	    data:{'val':val}
	});
    }
}