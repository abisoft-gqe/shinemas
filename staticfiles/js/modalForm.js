$(document).ready(function() {
	
	$('[name="close_list"]').on('click',function() {
		$('#rawdata_div').hide();
	});
	
	
	$(document).on("click", "span.handclick", function() {
		var id = $(this).attr("value");
		$.ajax({
	        url: ROOT_URL.concat("/data/getrawdataform/"+id+"/"),
	        type: "GET", 
	        //data: {},
	        dataType: "json",
	        success: function(result){
	        	     var form = jQuery.parseJSON(result['form']);
	        	     $('#rawdata_form').text('');
	        	     $('#rawdata_form').attr('action',ROOT_URL.concat("/data/getrawdataform/"+id+"/"));
	        	     showFormInDiv(form);
	        	     $('#rawdata_div').show();
	        	     $('.datepicker').each(function(){
	        				$(this).removeClass('hasDatepicker').datepicker({ dateFormat: 'dd/mm/yy' });
	        		});
	        	  },
	        async: false,
	        error: function(jqXHR, textStatus, errorThrown) {
	        	alert(jqXHR.responseText);
	        }
	    });
	});
	
	function showFormInDiv(form) {
		var formString = ''
		$.each(form['fields'], function(field_name, field) {
			required_string = '';
			if (field['widget']['is_required'] == true) {
				required_string = 'required';
			}
			formString += '<b>'+field['label']+'</b> :<br />';
			if (form['errors'][field_name]) {
				formString += '<span class="errorlist">';
				formString += '<ul>';
				$.each(form['errors'][field_name], function(error) {
					formString += '<li>'+form['errors'][field_name][error]+'</li>';
				});
				formString += '</ul>';
			}
			if (field['widget']['attrs']['disabled'] == true) {
				if (field['title'] == 'ModelChoiceField' || field['title'] == 'ChoiceField') {
					$.each(field['choices'], function(choice){

						if (field['choices'][choice]['value'] == field['initial']) {
							formString += field['choices'][choice]['display']+'<br />';
							formString += '<input type="hidden" id="'+field_name+'" name="'+field_name+'" value="'+field['choices'][choice]['value']+'" />';
						}
					});
				} else {
					formString += field['initial']+'<br />';
				}
			}
			else {
				if (field['widget']['title'] == 'Select' ) {
					formString += '<select width="250px" name="'+field_name+'" '+required_string+' id="id_'+field_name+'" >\n';
					$.each(field['choices'], function(choice){
						if (field['choices'][choice]['value'] == field['initial']) {
							formString += '<option selected value="'+field['choices'][choice]['value']+'">'+field['choices'][choice]['display']+'</option>\n';
						} else {
							formString += '<option value="'+field['choices'][choice]['value']+'">'+field['choices'][choice]['display']+'</option>\n';
						}
					});
					formString += '</select><br />\n';
				}
				else if ((field['widget']['title'] == 'Textarea' ) ) {
					formString += '<textarea rows="'+field['widget']['attrs']['rows']+'" cols="'+field['widget']['attrs']['cols']+'" name="'+field_name+'" id="id_'+field_name+'" '+required_string+'>'+field['initial']+'</textarea><br />';
				} else if ((field['widget']['title'] == 'DateInput' ) ) {
					initial_date = '';
					if (field['initial'] != null) {
						initial_date = field['initial'];
					}
					formString += '<input type="text" id="'+field_name+'" name="'+field_name+'" class="datepicker hasDatepicker" value="'+initial_date+'"/><br />';
				}
			}
			formString += '<br />';
		});
		formString += '<input type="submit" value="Update" />';
		$('#rawdata_form').append(formString);
	};
	
	$('#rawdata_form').submit(function(e){
	    $.post($(this).attr('action'), $(this).serialize(), function(data){
	    	results = jQuery.parseJSON(data);
	    	if (results['form']) {
	    		var form = jQuery.parseJSON(results['form']);
	    		$('#rawdata_form').text('');
       	     	showFormInDiv(form);
	       	    $('.datepicker').each(function(){
	 				$(this).removeClass('hasDatepicker').datepicker({ dateFormat: 'dd/mm/yy' });
	       	    });
	    	} else if (results['data']) {
	    		var data = jQuery.parseJSON(results['data']);
	    		var date = '-';
    			if (data[0]['fields']['date'] != null) {
    				date = data[0]['fields']['date'];
    			}
	    		if (data[0]['fields']['individual'] == null) {
	    			$('#rawdata_'+data[0]['pk']).text(data[0]['fields']['raw_data']);
	    			$('#rawdata_'+data[0]['pk']).parent('td').parent('tr').children('td').eq(2).text(date);
	    			$('#rawdata_'+data[0]['pk']).parent('td').parent('tr').children('td').eq(3).text(data[1]['fields']['method_name']);
	    		} else {
		    		$('#rawdata_'+data[0]['pk']).text(data[0]['fields']['raw_data']);
		    		$('#rawdata_'+data[0]['pk']+' + span').text('('+date+', '+data[1]['fields']['method_name']+')');
	    		}
	    		$('#rawdata_div').hide();
	    	}
	    });
	    e.preventDefault();
	});
	
});