plot.data = function(
out,
plot.type = "pie.on.map",
data.display = "y",
to.plot = "sd",
classes = "auto",
pie.color = NULL,
region = "France",
nb_per_plot = 10,

# option du network
only.pie.label = TRUE,
hide_name = "auto",
with_sex = FALSE,
fill_diffusion_gap = FALSE,
graph = TRUE,
displaylabels = TRUE,
CEX = 0.8,
main = "",
col.type = "black",
network.legend.on.another.plot = TRUE,
pie.legend.on.another.plot = FALSE,
vertex.legend = TRUE,
edge.legend = TRUE,
diffusion_color = "red",
reproduction_color = "darkgreen",
mixture_color = "purple",
selection_color = "gold",
interactive = FALSE,
display_diffusion = TRUE,
display_reproduction = TRUE,
display_mixture = TRUE,
display_selection = TRUE,
display_reproduction_type = "ALL",
display.generation = FALSE,
...
)

{
cat("--------------------------------------------------- \n")
cat("A faire : \n")
cat("Faire les messages d'erreur \n")
cat("Ajouter les sorties de Sand R: valeurs de R, S etc ?!?")
cat("Choisir si pie, boxplot, hist \n")
cat("Gérer la taille des pie et des points ?!? Ce n'est pas la même chose entre map et network !!! A creuser ... \n")
size = 0.03
cat("Attention avec l'année qui est l'année du SL father et pas celle dela relation : à faire évoluer !!!")
cat("Gérer le titre selon les différents cas pour l'instant, c'est crado t\n")
titre = ""
cat("Afficher un tableau avec un sommaire par graph ? Afficher C ?!? Important je pense pour avoir le nombre d'individu ?!? ou l' afficher sur les camemberts ? \n")
cat("Ajouter l'argument interactive pour le plot de network ? mais comment gérer ça ... faire une pause avant de plote les pie ?!? \n")
cat("Gérer si l'on veut plusieurs graphiques par page \n")
cat("Gérer les biplot \n")
cat("--------------------------------------------------- \n")

########################################################################
########################################################################
########################################################################
# Fonction utilisée dans la suite
########################################################################
########################################################################
########################################################################

########################################################################
# from.out.to.data : pour mettre les données au format qui va bien
########################################################################
from.out.to.data = function(V, to.plot, data.display)
{

if(to.plot == "ind") { to.plot = "ind" }
if(plot.type == "boxplot") { to.plot = "ind" ; cat("Avec plot.type = \"boxplot\", on travaille sur toutes les données, on ne représente pas \"moy\" ni \"sd\".\n")}

V = unlist(V)

to.map = paste(".",to.plot,".", sep="")

y = V[grep(to.map, names(V))]

SL = sapply(names(y), function(x){unlist(strsplit(x,to.map))[2]})
SL = sapply(SL, function(x){unlist(strsplit(x,"\\."))[1]}) # pour le cas particulier de ind
G = sapply(SL, function(x){unlist(strsplit(x,"_"))[1]})
G = sapply(G, function(x){unlist(strsplit(x,"\\."))[1]})
P = sapply(SL, function(x){unlist(strsplit(x,"_"))[2]})
Y = sapply(SL, function(x){unlist(strsplit(x,"_"))[3]})

lat = V[grep(".lat", names(V))]
names(lat) = sub(".lat", "", names(lat))

long = V[grep(".long", names(V))]
names(long) = sub(".long", "", names(long))


donnees = cbind.data.frame("label" = SL, "germplasm" = G, "person" = P, "year" = Y, "variable" = y, "lat" = lat[P], "long" = long[P])
donnees$yg = paste(donnees$year, donnees$germplasm, sep=":"); colnames(donnees)[ncol(donnees)] = "year:germplasm"
donnees$yp = paste(donnees$year, donnees$person, sep=":"); colnames(donnees)[ncol(donnees)] = "year:person"
donnees$pg = paste(donnees$person, donnees$germplasm, sep=":"); colnames(donnees)[ncol(donnees)] = "person:germplasm"


# faire des sous jeux de données selon ce que l'on veut afficher ou non
liste_donnees = list()
liste_y = as.character(unique(donnees[,"year"]))
liste_g = as.character(unique(donnees[,"germplasm"]))
liste_p = as.character(unique(donnees[,"person"]))
liste_yg = as.character(unique(donnees[,"year:germplasm"]))
liste_yp = as.character(unique(donnees[,"year:person"]))
liste_pg = as.character(unique(donnees[,"person:germplasm"]))


getdata = function(x.axis, liste_facteur, donnees, liste_donnees)
{
for(i in liste_facteur)
	{
	d = donnees[which(donnees[,x.axis]==i),]
	if( length(na.omit(d[,"variable"])) > 0 )
		{
		d = list(d)
		names(d) = i
		} else { d = NULL ; cat("Stocker cette info qql part ?!? \n")}
	liste_donnees = c(liste_donnees, d)
	}
return(liste_donnees)
}

if(data.display == "y") { x.axis = "year"; liste_donnees = getdata(x.axis, liste_y, donnees, liste_donnees) }
if(data.display == "g") { x.axis = "germplasm"; liste_donnees = getdata(x.axis, liste_g, donnees, liste_donnees) }
if(data.display == "p") { x.axis = "person"; liste_donnees = getdata(x.axis, liste_p, donnees, liste_donnees) }
if(data.display == "yp" | data.display == "py") { x.axis = "year:person"; liste_donnees = getdata(x.axis, liste_yp, donnees, liste_donnees) }
if(data.display == "yg" | data.display == "gy") { x.axis = "year:germplasm"; liste_donnees = getdata(x.axis, liste_yg, donnees, liste_donnees) }
if(data.display == "pg" | data.display == "gp") { x.axis = "person:germplasm"; liste_donnees = getdata(x.axis, liste_pg, donnees, liste_donnees) }

out = list("liste_donnees" = liste_donnees, "x.axis" = x.axis)
return(out)
}

########################################################################
# create.data.for.pie : pour mettre les données au format qui va bien
########################################################################
create.data.for.pie = function(donnees, classes, pie.color, nom_variable)
{
# Mise en forme des données dans les classes
if(is.numeric(classes[1])) { num.class = TRUE; charac.class = FALSE } # si ce sont des classes faites de chiffres
if(is.character(classes[1])) { num.class = FALSE; charac.class = TRUE } # si ce sont des classes faites de caractères

# gérer les classes et la légende
classes.name <- NULL
if(num.class){ goto = length(classes)-1 }
if(charac.class){ goto = length(classes) }

for (i in 1:goto)
	{
	cn <- paste("class.",i,sep="")
	classes.name <- c(classes.name, cn)
	}
liste_label <- as.character(unique(donnees[,"label"]))

# Afficher le nom dans les colonnes pour la légende
leg <- NULL
for (k in 1:length(classes.name))
	{
	if(num.class){ P <- paste(classes[k],"<",nom_variable,"<",classes[k+1],sep=" ") }
	if(charac.class){ P <- paste(classes[k]) }
	leg <- c(leg,P)
	}
classes.name <- leg

# Gérer les couleurs
if(is.null(pie.color))
	{
	pie.color = c(1:length(leg))
	names(pie.color) = leg
	}


# Assigner le nombre d'occurence pour chaque classe
Cok <- NULL

for (i in 1:length(liste_label))
	{
	a = as.character(liste_label[i])
	D <- subset(donnees, label == a)
	
	if(!is.na(D[,"variable"]))
		{
		C <- matrix(0,ncol=goto,nrow=1)
		C_ <- matrix(0,ncol=4,nrow=1)
		C <- cbind.data.frame(C_,C)
		colnames(C) = c("label","Y","X","size",classes.name) # Latitude = Y, Longitude = X
		C[1,"label"] <- a
		C[1,"Y"] <- as.numeric(as.character(D[1,"lat"]))
		C[1,"X"] <- as.numeric(as.character(D[1,"long"]))
		# C[,"size"] <- size
		C <- as.data.frame(C)

		for (k in 1:length(classes.name))
			{
			c <- classes.name [k]
			for (m in 1:nrow(D))
				{
				if(num.class){ if (classes[k] < as.numeric(as.character(D[m,"variable"])) & as.numeric(as.character(D[m,"variable"])) <= classes[k+1]) { C[1,c] <- sum(C[1,c],1)} }
				if(charac.class){ if (classes[k] == as.numeric(as.character(D[m,"variable"])) ) { C[1,c] <- sum(C[1,c],1)} }
				}
			}
		Cok <- rbind(Cok,C)
		}
	}

if(!is.null(Cok))
	{ 
	C = Cok[which(apply(Cok[,4:ncol(Cok)],1,sum)>0),] # On vire les lignes où il n'y a que des 0
	} else { C = NULL }

out = list("C" = C, "leg" = leg, "color" = pie.color)
return(out)
}

########################################################################
# put.pie : pour afficher les pie sur un réseau ou sur une carte
########################################################################
put.pie = function(C, only.pie.label, data.display, pie.legend.on.another.plot,...)
{
if(!is.null(C)) {
for(i in 1:nrow(C))
	{
	x = as.numeric(C[i, 5:ncol(C)])
		
	if(length(which(x==0)) != (length(x) - 1))
		{				
		pie.on.plot(
					x = x,
					X = C[i, "X"], 
					Y  = C[i, "Y"], 
					XLIM = XLIM, 
					YLIM = YLIM, 
					radius = size, 
					labels = C[i, "label"],
					col = color,
					...
					)
		} else 
				{
				points(C[i, "X"], C[i, "Y"], pch = 19, col = color[as.character(leg[which(x==1)])], cex=4)
				}

	if(only.pie.label)
		{
		lab = which(donnees[, "label"]==C[i, "label"])
		text(
			C[i,"Y"] ~ C[i,"X"],
			labels = donnees[lab, data.display],
			cex = CEX,
			pos = 3, # position au dessus
			offset = size/0.03
			)
		}
						
	if(pie.legend.on.another.plot) { plot(0,col="white", xaxt="n", yaxt="n", xlab="", ylab= "", bty="n"); legend.place = "center" } else { legend.place = "bottomright" }

	legend  (
			legend.place,
			legend = leg,
			col = color,
			pch = 15,
			cex = CEX
			)		
	}
}
}

########################################################################
# go.and.SEQ : 
# go : pour vérifier si il y a des données
# SEQ : la séquence pour afficher les données en plusieurs graph, pour plus de lisibilité
########################################################################
go.and.SEQ = function(todo, nb_per_plot)
{
test = which(is.na(todo))
if( length(test) == length(todo) ) 
	{
	go = FALSE 
	} else { 
			go = TRUE 
			SEQ = c(seq(0,length(todo), nb_per_plot), length(todo))
			SEQ = unique(SEQ)
			}
return(list("go" = go, "SEQ" = SEQ))
}

########################################################################
# plot.barplot : affiche un barplot à partir de todo et SEQ + renvoie le nom des SL qui n'ont pas été affichés
########################################################################
plot.barplot = function(todo, SEQ)
{
NOT_DONE_because_of_NA = NULL
for(s in 1:(length(SEQ)-1))
	{
	do = todo[(SEQ[s]+1):SEQ[s+1]]
	na = which(is.na(do))
	NOT_DONE_because_of_NA = names(do)[na]
	do = do[-na]
	if(length(do) > 0) { barplot(do, las = 3) }
	}
return(NOT_DONE_because_of_NA)
}


########################################################################
# plot.boxplot : affiche un boxplot à partir de todo et SEQ + renvoie le nom des SL qui n'ont pas été affichés
########################################################################
plot.boxplot = function(todo, SEQ)
{
NOT_DONE_because_of_NA = NULL
N = sapply(names(todo), function(x){unlist(strsplit(x,"\\."))[1]} )
for(s in 1:(length(SEQ)-1))
	{
	do = todo[(SEQ[s]+1):SEQ[s+1]]
	n = N[(SEQ[s]+1):SEQ[s+1]]
	na = which(is.na(do))
	NOT_DONE_because_of_NA = names(do)[na]
	do = do[-na]
	n = n[-na]
	if(length(do) > 0) { boxplot(do ~ n, las = 3) }
	}
return(NOT_DONE_because_of_NA)
}

########################################################################
########################################################################
########################################################################
# En amont
########################################################################
########################################################################
########################################################################
if(plot.type == "boxplot") { to.plot = "ind" }

if(classes[1] == "auto")
	{
	test = unlist(out)
	test = as.numeric(as.character(test[grep(paste(".",to.plot,".", sep=""), names(test))]))
	
	min = min(test, na.rm = TRUE); min = min - min/10
	max = max(test, na.rm = TRUE); max = max + max/10

	classes = round(seq(min, max, (max - min)/5 ), 1)
	}

color_g = get.col.pch.lty(col.type = "germplasm")$col

########################################################################
########################################################################
########################################################################
# Messages d'erreur
########################################################################
########################################################################
########################################################################
if(plot.type != "pie.on.network") {only.pie.label = FALSE}
if(hide_name == "auto") { hide_name = data.display }

# les classes : important pour que le C dans put.pie n'ait pas 0 ligne
test.classes = function(out, classes, to.plot)
	{
	test = unlist(out)
	test = as.numeric(as.character(test[grep(paste(".",to.plot,".", sep=""), names(test))]))
	if( min(test, na.rm = TRUE) < min(as.numeric(classes), na.rm = TRUE) | max(test, na.rm = TRUE) > max(as.numeric(classes), na.rm = TRUE) ) { stop("Les classes ne contiennent pas toutes les valeurs des données \n") }
	}

if(plot.type == "pie.on.map" | plot.type == "pie.on.network") { test.classes(out, classes, to.plot) }

test = function(x, liste_x) { if(!is.null(x)) { t = sort(unique(is.element(x, liste_x))); out = length(t) == 2 | !t[1] } else { out = FALSE }}

if(test(plot.type, c("barplot", "beanpot", "boxplot", "distribution", "biplot", "interaction.plot", "pie.on.map", "pie.on.network"))) { stop(paste("plot.type", person_in, "n'existe pas. plot.type doit être \"barplot\", \"beanpot\", \"boxplot\", \"distribution\", \"biplot\", \"interaction.plot\", \"pie.on.map\" ou \"pie.on.network\"")) }
if(test(data.display, c("y", "g", "p", "yg", "gy", "yp", "py", "pg", "gp"))) { stop(paste("data.display", data.display, "n'existe pas. data.display doit être \"y\", \"g\", \"p\", \"yg\", \"gy\", \"yp\", \"py\", \"pg\", \"gp\"")) }
if(test(to.plot, c("sd", "moy", "ind", "sd_g", "moy_g", "ind_g", "sd_e", "moy_e", "ind_e", "sd_gxe", "moy_gxe", "ind_gxe", "s"))) { stop(paste("to.plot", to.plot, "n'existe pas. to.plot doit être \"sd\", \"moy\", \"ind\", \"sd_g\", \"moy_g\", \"ind_g\", \"sd_e\", \"moy_e\", \"ind_e\", \"sd_gxe\", \"moy_gxe\", \"ind_gxe\" ou \"s\"")) }


# Vérifier que l'on plot bien qqch en rapport avec les données de out
analysis = out$analyse

if(analysis == "mean.comparison.on.farm")
	{
	if(	to.plot == "sd_g" | to.plot == "moy_g" | to.plot == "ind_g" | 
		to.plot == "sd_e" | to.plot == "moy_e" | to.plot == "ind_e" | 
		to.plot == "sd_gxe" | to.plot == "moy_gxe" | to.plot == "ind_gxe" |
		to.plot == "s"
		){ stop("Avec out venant de la fonction mean.comparison.on.farm, to.plot doit être \"sd\", \"moy\" ou \"ind\". \n") }

	if(plot.type == "interaction.plot" ) { stop("plot.type = \"interaction.plot\" n'est possible qu'avec out venant de la fonction gxe. \n") }
	}

if(analysis == "gxe")
	{
	cat("Avec out venant de la fonction gxe, data.display n'est pas pris en compte. \n")
	if(
		to.plot == "sd" |
		to.plot == "moy" |
		to.plot == "gxe"
		) { stop("Avec out venant de la fonction gxe, to.plot doit être \"sd_g\", \"moy_g\", \"ind_g\", \"sd_e\", \"moy_e\", \"ind_e\", \"sd_gxe\", \"moy_gxe\", \"ind_gxe\" ou \"s\". \n") }
	}

########################################################################
########################################################################
########################################################################
# Fonction : représentation des graphiques
########################################################################
########################################################################
########################################################################
out = out [2:length(out)] # On enlève l'information sur l'analyse pour ne conserver que les données

liste_nom_variable = names(out)

# Voir comment gérer les biplot
if(plot.type == "biplot")
	{
	var_x = ""
	var_y = ""
	}	
			
OUT = NULL

for(nom_variable in liste_nom_variable)
	{
	# nom_variable = "plant_height"
	V = out[[nom_variable]] 

	NOT_DONE_because_of_NA = NULL

	####################################################################
	# Pour les sorties de gxe
	####################################################################
	if(analysis == "gxe")
		{		
		if(plot.type == "interaction.plot")
			{
			cat("Avec plot.type = \"interaction.plot\", to.plot n'est pas pris en compte \n")

			env_order = names(sort(V$e$moy_e))
			coord_env = c(1:length(env_order)); names(coord_env) = env_order
			gxe = V$gxe$moy_gxe
			d = cbind.data.frame(
								gxe,
								"g" = sapply(names(gxe), function(x){unlist(strsplit(x,":"))[1]}),
								"e" = sapply(names(gxe), function(x){unlist(strsplit(x,":"))[2]})
								)
								
			d = d[which(!is.na(d[,"gxe"])),]
			
			liste_g = as.character(unique(d[,"g"]))
						
			xmin = 1
			xmax = max(coord_env, na.rm = TRUE)
			ymin = 0
			ymax = max(d[,"gxe"], na.rm = TRUE)
									
			SEQ = c(seq(0,length(liste_g), nb_per_plot), length(liste_g))
			SEQ = unique(SEQ)

			for(s in 1:(length(SEQ)-1))
				{
				l_g = liste_g[(SEQ[s]+1):SEQ[s+1]]
				
				plot(0, col = "white", xlim = c(xmin, xmax), ylim = c(ymin, ymax), xaxt = "n", xlab="Environnement", ylab = nom_variable )
				for(i in 1: length(env_order)){ mtext(env_order[i], 1, line=1, at = i, las=3) }
				legend("bottomright", legend = l_g, col = color_g[l_g], pch = 21)
				
				for(i in l_g)
					{
					dd = subset(d, g == i)
					y = dd[,"gxe"]
					x = coord_env[as.character(dd[,"e"])]

					# tri pour que les lignes se fassent bien
					z = cbind.data.frame(x, y)
					z = z[order(z$x),]
					y = z$y
					x = z$x
					points(x, y, pch=21, col = color_g[i])
					lines(x, y, col = color_g[i])
					}
				}
			}
		
		if(to.plot == "sd_g" | to.plot == "moy_g" | to.plot == "ind_g") { todo = V$g[[as.character(to.plot)]] }
		if(to.plot == "sd_e" | to.plot == "moy_e" | to.plot == "ind_e") { todo = V$e[[as.character(to.plot)]] }
		if(to.plot == "sd_gxe" | to.plot == "moy_gxe" | to.plot == "ind_gxe") { todo = V$gxe[[as.character(to.plot)]] }
		if(to.plot == "s") { todo = V$s }
						
		toto = go.and.SEQ(todo, nb_per_plot)
		go = toto$go
		SEQ = toto$SEQ
					
		if( plot.type == "barplot" & go ) { NOT_DONE_because_of_NA = plot.barplot(todo, SEQ) }
	
		if(plot.type == "beanplot" & go ) { cat("A faire avec le package beanplot. Equivalent à barplot + group \n") }
		
		if(plot.type == "boxplot" & go ) { NOT_DONE_because_of_NA = plot.boxplot(todo, SEQ) }
		
		if(plot.type == "distribution" & go ) # comme beanplot en fait et proche de boxplot
			{
			
			}
		if(plot.type == "pie.on.map" | plot.type == "pie.on.network") { stop("Avec out venant de la fonction gxe, plot.type ne peut pas être \"pie.on.map\" ou \"pie.on.network\". \n") }		
		}
	
	####################################################################
	# Pour les sorties de mean.comparison.on.farm
	####################################################################
	if(analysis == "mean.comparison.on.farm")
		{
		yo = from.out.to.data(V, to.plot, data.display)

		liste_donnees = yo$liste_donnees
		x.axis = yo$x.axis
		
		for(d in 1:length(liste_donnees))
			{
			donnees = liste_donnees[[d]]
			
			cat("Ajouter les sorties du bayésien pour faire les groupes \n")
			cat("Ajouter aussi les beans plots \n")
			# plus ajouter les sorties du bayésien pour modifier les graphs ! ajouter graph pour faire les moyennes ave cles lettres etc
			# Mettre ici la fonction qui crée les groupes ...
						
			if(data.display == "y") { name.to.show = "person:germplasm" }
			if(data.display == "g") { name.to.show = "year:person" }
			if(data.display == "p") { name.to.show = "year:germplasm" }
			if(data.display == "yg" |  data.display == "gy") { name.to.show = "person" }
			if(data.display == "yp" | data.display == "py") { name.to.show = "germplasm" }
			if(data.display == "pg" | data.display == "gp") { name.to.show = "year" }
			
			todo = donnees[,"variable"] ; names(todo) = donnees[,name.to.show]
			
			toto = go.and.SEQ(todo, nb_per_plot)
			go = toto$go
			SEQ = toto$SEQ

			if( plot.type == "barplot" & go ) { NOT_DONE_because_of_NA = plot.barplot(todo, SEQ) }
		
			if(plot.type == "beanplot" & go ) { cat("A faire avec le package beanplot. Equivalent à barplot + group \n") }
			
			if(plot.type == "boxplot" & go ) { NOT_DONE_because_of_NA = plot.boxplot(todo, SEQ) }


			if(plot.type == "distribution")
				{
				# pour SandR
				}
			
			if(plot.type == "pie.on.map" | plot.type == "pie.on.network") # "pie.on.map" et "pie.on.network" n'ont de sens qu'avec les sorties de mean.comparison.on.farm. En effet, dans ce cas, on travaille bien sur le G + GxE
				{
				a = create.data.for.pie(donnees, classes, pie.color, nom_variable)
				C = a$C
				leg = a$leg
				color = a$color
				} 

			if(is.null(C)) { cat(paste("Il n'y a pas de données pour",variable,"pour",names(liste_donnees)[d],"\n")) }
			
			if(plot.type == "pie.on.map" & !is.null(C))
				{
				map = get.map(region, titre)
				XLIM = map$XLIM
				YLIM = map$YLIM
				}
			
			if(plot.type == "pie.on.network" & !is.null(C))
				{
				SL = as.character(unique(donnees$label))
				n = get.network(
								SL_name = SL, 
								hide_name = hide_name,
								with_sex = with_sex,
								fill_diffusion_gap = fill_diffusion_gap,
								graph = graph,
								displaylabels = displaylabels,
								CEX = CEX,
								main = main,
								col.type = col.type,
								legend.on.another.plot = network.legend.on.another.plot,
								vertex.legend = vertex.legend,
								edge.legend = edge.legend,
								diffusion_color = diffusion_color,
								reproduction_color = reproduction_color,
								mixture_color = mixture_color,
								selection_color = selection_color,
								display_diffusion = display_diffusion,
								display_reproduction = display_reproduction,
								display_mixture = display_mixture,
								display_selection = display_selection,
								display_reproduction_type = display_reproduction_type,
								display.generation = display.generation
								)
				
				XLIM = c(0, 1)
				YLIM = c(0, 1)

				coord = n$graph.coordinates
				colnames(coord)[1] = "label"
				
				C = merge(C, coord, by = "label")
				a = C[,c("label", "X.y", "Y.y")]
				colnames(a) = c("label", "X", "Y")
				b = C[,c(4:(ncol(C)-2))]
				C = cbind(a, b)
				}
			
			# afficher les points ou les pie sur la carte ou le réseau
			if(plot.type == "pie.on.map" | plot.type == "pie.on.network") { put.pie(C, only.pie.label, data.display, pie.legend.on.another.plot) }
			
			title(paste("Données pour",names(liste_donnees)[d]))
			}
		}
		
	NOT_DONE_because_of_NA = list(NOT_DONE_because_of_NA)
	names(NOT_DONE_because_of_NA) = paste(nom_variable,plot.type,sep=":")
	OUT = c(OUT, NOT_DONE_because_of_NA)
	}
return(OUT)
}

