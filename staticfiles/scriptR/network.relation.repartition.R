network.relation.repartition = function(
N,
nbr_ferme_par_graph=3,
example=FALSE
)
{
        
etape = c(
"cahier des charges",
"création variabilité",
"sélection inter negative",
"sélection inter positive",
"sélection intra",
"melange",
"évaluation",
"multiplication",
"diffusion envoi",
"diffusion recoi"
)

if(example)
{
TAB = list(

conv = list(
c("y1"=1997,"y2"=1998,"cahier des charges"=1),
c("y1"=1998,"y2"=1999,"création variabilité"=90),
c("y1"=1999,"y2"=2000,"sélection inter positive"=50),
c("y1"=2000,"y2"=2001,"sélection inter positive"=40),
c("y1"=2001,"y2"=2002,"sélection inter positive"=30),
c("y1"=2002,"y2"=2003,"sélection inter positive"=20),
c("y1"=2003,"y2"=2004,"sélection inter positive"=10),
c("y1"=2004,"y2"=2005,"sélection inter positive"=5),
c("y1"=2005,"y2"=2006,"évaluation"=90),
c("y1"=2006,"y2"=2007,"évaluation"=90),
c("y1"=2007,"y2"=2008,"évaluation"=90),
c("y1"=2008,"y2"=2009,"multiplication"=90),
c("y1"=2009,"y2"=2010,"multiplication"=90),
c("y1"=2010,"y2"=2011,"diffusion envoi"=90))
)
nbr_ferme_par_graph=1
}

r = N[["data.reseau"]]
year = sort(unique(c(r$father_year,r$son_year)))
year = paste(year,year+1,sep="-")
year = year[-length(year)]

liste_nom_ferme = unique(c(as.character(r$father_person),as.character(r$son_person)))

# créer le patron pour les fermes
F = NULL
for (i in 1:length(year))
	{
	y1=unlist(strsplit(year[i],"-"))[1]
	y2=unlist(strsplit(year[i],"-"))[2]
	a=c(y1,y2,rep(0,length(etape)))
	names(a) = c("y1","y2",as.character(etape))
	a=list(a)
	names(a)=year[i]
	F=c(F,a)
	}
FF=F


w_cross = analyse.network.relation(N=N, x.axis = "year", in.col = "person", relation = "reproduction", repro.type = "cross", hist=FALSE, map=FALSE)
w_recolte = analyse.network.relation(N=N, x.axis = "year", in.col = "person", relation = "reproduction", repro.type = "recolte", hist=FALSE, map=FALSE)
w_inter_neg = analyse.network.relation(N=N, x.axis = "year", in.col = "person", relation = "reproduction", repro.type = "inter-negative", hist=FALSE, map=FALSE)
w_inter_pos = analyse.network.relation(N=N, x.axis = "year", in.col = "person", relation = "reproduction", repro.type = "inter-positive", hist=FALSE, map=FALSE)
w_mix = analyse.network.relation(N=N, x.axis = "year", in.col = "person", relation = "mixture", hist=FALSE, map=FALSE)
w_sel = analyse.network.relation(N=N, x.axis = "year", in.col = "person", relation = "selection", hist=FALSE, map=FALSE)
w_recoi = analyse.network.relation(N=N, x.axis = "year", in.col = "person", relation = "diffusion", diff.type = "recoi", hist=FALSE, map=FALSE)
w_envoi = analyse.network.relation(N=N, x.axis = "year", in.col = "person", relation = "diffusion", diff.type = "envoi", hist=FALSE, map=FALSE)

	
TAB = NULL
for (yo in 1:length(liste_nom_ferme))
	{
	nom_ferme = liste_nom_ferme[yo]
	
	F=FF
	
	w = w_cross	# cas particulier des cross
	if(!is.null(w))
		{
		if(is.element(nom_ferme,colnames(w$M)))
			{
			a = w$M[nom_ferme,]
			n = as.numeric(names(a))
			names(a) = paste(n-1,n,sep="-") # on met le bon format, l'année de récolte correspond à y2
			for ( i in 1:length(a)) {F[[names(a)[i]]]["création variabilité"] = a[i]}
			}
		}

	w = w_recolte
	if(!is.null(w))
		{
		a = w$M[nom_ferme,]
		n = as.numeric(names(a))
		names(a) = paste(n-1,n,sep="-") # on met le bon format, l'année de récolte correspond à y2
		for ( i in 1:length(a)) {F[[names(a)[i]]]["évaluation"] = sum(a[i])}
		}
		
	w=w_inter_neg
	if(!is.null(w))
		{
		a = w$M[nom_ferme,]
		n = as.numeric(names(a))
		names(a) = paste(n-1,n,sep="-") # on met le bon format, l'année de récolte correspond à y2
		for ( i in 1:length(a)) {F[[names(a)[i]]]["sélection inter negative"] = sum(a[i])}
		}
		
	w=w_inter_pos
	if(!is.null(w))
		{
		a = w$M[nom_ferme,]
		n = as.numeric(names(a))
		names(a) = paste(n-1,n,sep="-") # on met le bon format, l'année de récolte correspond à y2
		for ( i in 1:length(a)) {F[[names(a)[i]]]["sélection inter positive"] = sum(a[i])}
		}

	w=w_mix
	if(!is.null(w))
		{
		a = w$M[nom_ferme,]
		n = as.numeric(names(a))
		names(a) = paste(n-1,n,sep="-") # on met le bon format, l'année de récolte correspond à y2
		for ( i in 1:length(a)) {F[[names(a)[i]]]["melange"] = sum(a[i])}
		}

	w=w_sel
	if(!is.null(w))
		{
		a = w$M[nom_ferme,]
		n = as.numeric(names(a))
		names(a) = paste(n-1,n,sep="-") # on met le bon format, l'anéne de récolte correspond à y2
		for ( i in 1:length(a)) {F[[names(a)[i]]]["sélection intra"] = sum(a[i])}
		}

	w=w_recoi
	if(!is.null(w))
		{
		a = w$M[nom_ferme,]
		n = as.numeric(names(a))
		names(a) = paste(n-1,n,sep="-") # on met le bon format, l'année de récolte correspond à y2
		for ( i in 1:length(a)) {F[[names(a)[i]]]["diffusion recoi"] = sum(a[i])}
		}

	w=w_envoi
	if(!is.null(w))
		{
		a = w$M[nom_ferme,]
		n = as.numeric(names(a))
		names(a) = paste(n-1,n,sep="-") # on met le bon format, l'année de récolte correspond à y2
		for ( i in 1:length(a)) {F[[names(a)[i]]]["diffusion envoi"] = sum(a[i])}
		}
	
	F=list(F)
	names(F)=nom_ferme

	TAB = c(TAB,F)		
	}


# mise en place du graphique

# par(xpd=TRUE,mar=c(5,3,5,13)) # pour avoir la légende à droite, mais ça déconne ave le pdf, je sais pas pourquoi + c'est moins lisible le graph

classes = c(0,5,10,20)

etape = as.factor(etape)

liste_ferme = names(TAB)

temps = sort(unique(na.omit(as.numeric(unlist(TAB)[c(grep("y1",names(unlist(TAB))),grep("y2",names(unlist(TAB))))]))))
a = rep(1,length(temps))

toto = sort(seq(0.1,0.9,0.8/length(etape)),decreasing=TRUE)

change_plot = seq(0,(length(liste_ferme)+100),nbr_ferme_par_graph)


plot.new()

leg <- NULL
for (k in 1:length(classes))
	{
	if (k != length(classes)) {P = paste(classes[k],"< ... <",classes[k+1],sep=" ")} else {P = paste(classes[k],"< ...",sep=" ")}
	leg <- c(leg,P)
	}

# les couleurs sont prises dans plot.PCA du package FactoMineR
palette = c("black", "red", "green3", "blue", "cyan", 
                "magenta", "darkgray", "darkgoldenrod", "darkgreen", 
                "violet", "turquoise", "orange", "lightpink", 
                "lavender", "yellow", "lightgreen", "lightgrey", 
                "lightblue", "darkkhaki", "darkmagenta", "darkolivegreen", 
                "lightcyan", "darkorange", "darkorchid", "darkred", 
                "darksalmon", "darkseagreen", "darkslateblue", 
                "darkslategray", "darkslategrey", "darkturquoise", 
                "darkviolet", "lightgray", "lightsalmon", "lightyellow", 
                "maroon")
		
legend(
"center",
legend=c(as.character(etape),"","Nombre de populations par étape",leg),
col=c(palette[sort(as.numeric(etape))],"white","white",rep("black",length(leg))),
lty=c(rep(1,length(etape)),-1,-1,rep(1,length(leg),1)),
lwd=c(rep(1,length(etape)),-1,-1,seq(1,length(leg),1)),
cex=1.5)


par(xpd=TRUE,mar=c(5,3,5,5))

for(f in 1:length(liste_ferme))
	{
	w=which(change_plot==(f-1))
	if(length(w)==1)
		{
		W=w
		ww=0
		
		yminplot = change_plot[W]
		ymaxplot = change_plot[W+1]
		
		plot(temps,a,col="white",ylim=c(yminplot,ymaxplot),yaxt="n",ylab="",xlab="",las=3)
		abline(h=seq(change_plot[W],(change_plot[W+1]),1),xpd=FALSE)
		} else {ww=ww+1}

	ymin = change_plot[W] + ww + 1
	ymax = change_plot[W] + ww 
	text((temps[1]-(length(temps)/10)),c(ymax-((ymax-ymin)/2)),liste_ferme[f])

	TT = TAB[[liste_ferme[f]]]

	for(y in 1:length(TT))
		{
		T = TT[[y]]
			
		for(e in 1:length(etape))
			{		
			lwd = T[as.character(etape[e])]
			
			if(!is.na(lwd))
				{
				if(lwd!=0)
					{
					LWD=NULL
					for (k in 1:length(classes))
						{
						if (k != length(classes) & classes[k] < lwd & lwd <= classes[k+1]) {LWD=k}
						if (k == length(classes) & is.null(LWD)) {LWD=k}
						}	
					ylim = ymax + toto[e]
					lines(c(T["y1"],T["y2"]),c(ylim,ylim),col=palette[e],lwd=LWD)
					}
				}
			}
		}
	
	# legende nbr de pop par étapes
	# on le met après sinon le pdf() ne marche pas

	w=which(change_plot==(f))
	
	if(length(w)==1)
		{	
		leg <- NULL
		for (k in 1:length(classes))
			{
			if (k != length(classes)) {P = paste(classes[k],"< ... <",classes[k+1],sep=" ")} else {P = paste(classes[k],"< ...",sep=" ")}
			leg <- c(leg,P)
			}	
		#legend((temps[length(temps)]+(length(temps)/15)),ymaxplot,legend=c(as.character(etape),"","Nombre de populations", "par étape",leg),col=c(palette[sort(as.numeric(etape))],"white","white","white",rep("black",length(leg))),lty=1,lwd=c(rep(1,length(etape)),-1,-1,-1,seq(1,length(leg),1)))
		}	

		
	}

par(xpd=FALSE,mar=c(5,5,5,5)) # remise à jour de la fenetre graphique

} 


