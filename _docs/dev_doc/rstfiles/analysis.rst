========================
Analysis App Description
========================

Analysis models
===============
.. automodule:: analysis.models
    :members:

Analysis admin
==============
.. automodule:: analysis.admin
    :members:

Analysis views
==============
.. automodule:: analysis.views
    :members:

Analysis forms
==============
.. automodule:: analysis.forms
    :members: