======================
Actors App Description
======================

Actors models
=============
.. automodule:: actors.models
    :members:
    
Actors admin
============
.. automodule:: actors.admin
    :members:

Actors views
============
.. automodule:: actors.views
    :members:
    
    