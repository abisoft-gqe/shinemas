.. SHiNeMaS documentation master file, created by
   sphinx-quickstart on Wed Feb  3 16:06:39 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SHiNeMaS' developer documentation
=================================

Contents:

.. toctree::
   :maxdepth: 2

   rstfiles/actors
   rstfiles/analysis
   rstfiles/common
   rstfiles/entities
   rstfiles/eppdata
   rstfiles/myuser
   rstfiles/network