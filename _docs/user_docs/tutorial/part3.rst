Part III: Submit data with forms
--------------------------------

Create a selection event with the form
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

During the 2014-2015 multiplications, Peter made a selection of the RxB population. Before to share this selection with John and Steve he wants to evaluate it in his farm in 2015-2016.

.. figure:: /_img/tuto_img/NewSelection.png
   :align: center
   
   *Create selection with form*

We want to declare this selection in SHiNeMaS but do we really want to prepare and export a file just for one row? Not really. To do this you can use a simple form to declare this selection. To do it go in the menu Admin data > Seed lot relation > Intra seed lot selection > New. From the form select the reproduction RxB_PEM_2014_0001 --> RxB_PEM_2015_0001 and fill the other fields as described in the picture below (PPB project, selection in 2015, breeder is Peter, name is S1 and a quantity of 100). You have to give a name to your selection, this name will be used in the final name of the seeds lot (RxB#S1_PEM_2015_0001) resulting in a different naming you used to see until now. In the ‘Additional information’ panel you can add data as you would do in a file. Select the variable, the method and a date, and then fill a value. Here for example we give information on tkw which could our criteria of selection. Then click on the ‘Create’ button, a green message should appear to indicate that the selection has been recorded.

Create a past diffusion event with a form
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Creating a single event is not the only advantage to use these kind of forms. Do you remember in the introduction about the origin of the seed lot of Green population owned by Peter? We said that we don’t know the origin of this seed lot yet. 

Now we know this origin and as you could expect at the beginning, the lot of Green population was given by John.

Until now when we submitted a file we created chronological data, meaning that we filled a parent seed lot in a file and SHiNeMaS created automatically the child seed lot of the relation. That the way it works, but in this particular case we want to create a relation between two seed lot already recorded in the database. 
You can do that with the forms. 

From the menu Admin data > Seed lot relation > Diffusion > New. In the ‘Seed lot diffused’ frame use the autocomplete list to select the ‘Green_JOD_2012_0001’ seed lot and in the ‘Diffusion information’ frame select ‘Green_PEM_2012_0001’ as received seed lot. You can choose the PPB project and 2012 for the event year (you should have an error message if you select another year) and then submit the form.
It’s important here to say that you can create this diffusion because ‘Green_PEM_2012_0001’ hasn’t been created by any relation before. In the other hand it would have result in a conflict, that is, two different events creating a same seed lot.

Let's go for :ref:`part IV of this tutorial <Part IV: Advanced Features>` !
