Part V: Edit/delete data
------------------------

Delete an event from administration interface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Unfortunately, when you merged all the lines in the merge reps tool interface, you also merged the selection done by Peter on the RxB population with the bulk of the population. 

You want to cancel it, thus, go in the menu Admin data > Seed lot relation > Mixture > Update/Delete. One of the line should appear with the following title: ‘2015 [RxB#S1_PEM_2015_0001 --> RxB_PEM_2015_0002, RxB_PEM_2015_0001 --> RxB_PEM_2015_0002]’, click on the title and press the ‘Delete’ button. The admin interface displays an overview of data that will be deleted. You can confirm the deletion by clicking on ‘Yes I’m sure’. 

Delete data from a query
~~~~~~~~~~~~~~~~~~~~~~~~

Also, you realized that there was a mistake on Steve’s farm. The scale to measure the awns was not used the good way and you need to delete and submit again the range of data. 

Go to the Admin data menu > seed lot relation > reproduction > Delete raw data. From the form filter on ‘STS’ location, awns variable, sowing data is 2015 and fill a range of date between 2015, July the 1st and 2016 July, the 1st, then click on the ‘Delete’ button.

A list of raw data is displayed grouped by relation; you can check/uncheck each raw data before final deletion. Confirm deletion by clicking on ‘Delete selected data’.

.. figure:: /_img/tuto_img/Deletedata.png
   :align: center
   
   *Raw data list to delete*

A report is displayed showing the number of raw data that have been deleted for each reproduction. After the report you can download two files, for reproductions and for individual data. These files are filled with the concerned reproductions, the good number of individual and the variables concerned by the deletion. You “just” need to fill the data that need to be reloaded and submit the file with the good method file.

.. figure:: /_img/tuto_img/DownloadFile.png
   :align: center
   
   *Download prefilled files to submit data again*
   
Delete reproduction events from a file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ultimately, all information that was recorded on Steve’s farm is wrong. We need to reset all data and the relations. To do this we will prepare an existing reproduction file but this time we won’t use it to add new data in the database, but to delete the reproductions filled in the file.

Go in the menu Prepare & Upload files > Prepare files > Existing reproduction. Filter on ‘STS’ location and search the relations. Add all to the list and then export the file. Move this file in the ‘tutorial data’ folder and rename it ‘09-reproduction_deletion.csv’.

Now, go to the menu Prepare & Upload files > Delete from file > Reproduction. From this form you can select your file. Also check the ‘delete in cascade’ option. Remember, after we submit data concerning the repetitions of Steve we decided to merge them. Deleting in cascade means that this will delete the reproduction filled in the file, but also all the following relations resulting from this relation in the network. Here, that doesn’t really make sense to keep track of this merge if the repetitions are wrong that’s why we delete in cascade. But in some other cases you might want to keep this track if that makes sense. 

.. figure:: /_img/tuto_img/Deletefromfile.png
   :align: center
   
   *Deleting events in cascade or not*

Edit data from a relation card
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To finish this tutorial, we will have a look on how to have a finer management of data in SHiNeMaS. You can delete massively raw data or relations, but sometime, you need to edit only one raw data, just because of an error when you filled the file. You can do it from the relation cards.

From the global search bar open the relation card of ‘RxY_PEM_2015_0001 --> RxY_PEM_2016_0001’ and display the individual data table. Click on the value for individual 7 for the variable ‘height’ (value should be 121).

A form is now displayed, you can edit the value of 121, fill it at 118 and the submit the form. The value is updated in the table.

This is the end!
