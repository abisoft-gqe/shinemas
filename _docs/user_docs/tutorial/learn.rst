Learner guide
=============

In this tutorial you will see how to use SHiNeMaS, load data in the tool, edit or delete them, and explore the information. A scenario, described just below, will give you the opportunity to test the different features of the application. As the aim is not to spend time in filling data in spreadsheet tools, in most of the case you will find data tables that will help you to fill your files. The information give in this tutorial should be enough to achieve it, but you can use at any time the user :ref:`reference documentation <refdoc>` if you need.
You can log in with username = admin & password = admin.

From 2010, 4 populations (Green, Yellow, Blue and Red) have been reproduced in 2 farms (2 populations in John Doe’s farm, JOD and 2 populations in Peter Martin’s farm, PEM). In 2012, the Yellow population have been sent to Peter Martin.
That is the state of the network information you have at the beginning of this tutorial.
 
.. figure:: /_img/tuto_img/Diapositive1.PNG
   :align: center
   
   *Former seed lot network*

Peter Martin will use each population and cross with each other (he also gets a seeds lot of the Green population but currently we don’t know the origin of this seeds lot) resulting in 6 crosses and the creation of 6 new populations that we will call RxB, RxY, RxG, BxY, BxG and YxG.

.. figure:: /_img/tuto_img/Diapositive2.PNG
   :align: center
   
   *Final network*

Then Peter will send his new populations to John Doe and Steve Smith a farmer who joined the project in 2015. Each of them will evaluate the populations, Steve decided to sow in two repetitions. Peter made a selection etc. All the events of this tutorial will results in a network that looks like the figure above.
The real network is in fact much more complicated as since 2013 only the RxB population is depicted but each step applies also to other populations. It’s important to know that the names of the seed lots follow a specific naming: germplasmName_locationShortName_creationYear_digitNumber, the digit number ensure unicity of the name of the seed lot.
Now that you have an overview you can go with the :ref:`next part <Part I: First steps with data management>`.

.. toctree::
   :hidden:
   
   part1
   part2
   part3
   part4
   part5
