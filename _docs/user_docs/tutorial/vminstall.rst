Install tutorial environment
============================

Windows
-------

Install Docker Desktop
~~~~~~~~~~~~~~~~~~~~~~
Download `executable file for Windows <https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe?utm_source=docker&utm_medium=webreferral&utm_campaign=dd-smartbutton&utm_location=header>`_ 
and install Docker Destop following instruction with `Docker documentation <https://docs.docker.com/desktop/windows/install/#install-interactively>`_

Install tutorial container
~~~~~~~~~~~~~~~~~~~~~~~~~~

Download `tutorial folder <https://forgemia.inra.fr/abisoft-gqe/shinemas-tutorial/-/archive/2.0/shinemas-tutorial-2.0.zip>`_ and unzip it. Move in the uncompressed folder and double click on the *install-tutorial.bat* file

Run tutorial
~~~~~~~~~~~~

Open Docker Desktop from windows main menu and click on the *Images* in the left panel. Click 'Run' to start the container.

.. figure:: /_img/tuto_img/Tutorial_Docker1.png
   :align: center
   
   *Docker Images menu*

Configure the container, fill the field "Container name" with "shinemas" and the field "host port" with 8000. Click the 'Run' button.

.. figure:: /_img/tuto_img/Tutorial_Docker2.png
   :align: center
   
   *Container configuration*
   
Click on the *Containers* menu on the left panel, you should see a container running. Start the tutorial by clicking on the |browser| icon
   
.. figure:: /_img/tuto_img/Tutorial_Docker3.png
   :align: center
   
   *Run container*

Now, You can :ref:`start the tutorial <Learner guide>` !

Linux
-----

Install docker for Linux system
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Install docker for Linux regarding `Docker documentation <https://docs.docker.com/engine/install/>`_.

Install tutorial container
~~~~~~~~~~~~~~~~~~~~~~~~~~

Download `tutorial folder <https://forgemia.inra.fr/abisoft-gqe/shinemas-tutorial/-/archive/2.0/shinemas-tutorial-2.0.tar.gz>`_ and uncompress it :

.. code-block::

   tar xvzf shinemas-tutorial-2.0.tar.gz

Move in the uncompressed folder and install the container :

.. code-block::

   ./install-tutorial.sh

Run tutorial
~~~~~~~~~~~~
Then to start/stop shinemas tutorial :

.. code-block::

   ./shinemas start
   ./shinemas stop

As soon as tutorial is started go to `http://127.0.0.1:8000`_

Now, You can :ref:`start the tutorial <Learner guide>` !

..
   First you need to install the virtualbox software and its Extension Pack. VirtualBox is platform dependant but not the Extension Pack.

   VirtualBox Install
   ~~~~~~~~~~~~~~~~~~
   
   Windows
   ^^^^^^^
   You can download the executable file `here <https://download.virtualbox.org/virtualbox/5.2.8/VirtualBox-5.2.8-121009-Win.exe>`_
   
   Click on the downloaded file and then click on the “next” buton several time to install virtualbox manager.
   
   .. figure:: /_img/tuto_img/virtualbox.png
      :align: center
      
   
   You have also to say yes to this screen :
   
   .. figure:: /_img/tuto_img/virtualbox2.png
      :align: center
   
   Linux
   ^^^^^
   
   For Linux you can find a paquage regarding your distribution at `this URL <https://www.virtualbox.org/wiki/Linux_Downloads>`_
   
   If you use any Linux distribution based on Debian you can also install it with apt-get :
   
   .. code-block::
   
      sudo apt-get install virtualbox
   
   Mac OS
   ^^^^^^
   
   You can also find an executable file for Moc OS at `this URL <https://download.virtualbox.org/virtualbox/5.2.8/VirtualBox-5.2.8-121009-OSX.dmg>`_
   
   
   Extension Pack Install
   ~~~~~~~~~~~~~~~~~~~~~~
   
   On any OS you need to install the “Extension pack”, download it `here <https://download.virtualbox.org/virtualbox/5.2.8/Oracle_VM_VirtualBox_Extension_Pack-5.2.8.vbox-extpack>`_
   
   And click on the file to install it
   
   
   Download ova file
   -----------------
   
   Download the virtual machine (OVA file) `here <http://moulon.inra.fr/abisoft/SHiNeMaSTutorial.ova>`_
   
   
   Import ova file
   ---------------
   To import the OVA file, start virtualbox software. For exemple on Windows :
   
   .. figure:: /_img/tuto_img/startvbwindows.png
      :align: center
   
   Go in the file menu and click on import a virtual machine
   
   .. figure:: /_img/tuto_img/import.png
      :align: center
   
   Click on the buton to browse files folder et select the OVA file to import. Then click on “next” at each step.
   
   At this step :
   
   .. figure:: /_img/tuto_img/import3.png
      :align: center
   
   Check the box “Reinitialize MAC address …” and click on “Import”.
   
   Start the virtual machine
   -------------------------
   As soon as import is finished start the virtual machine. Select the one you have imported (it should be named SHiNeMaSTutorial) and click on the start buton.
   
   .. figure:: /_img/tuto_img/startvm.png
      :align: center
   
   When the virtual machine is started, open the tutorial PDF file on the Desktop. Start SHiNeMaS by double clicking on the green button in the lateral menu or on the Desktop. You can start the tutorial !
   
   To log in the virtual machine, username is “shinemas”, password “shinemas”.

.. |browser| image:: /_img/tuto_img/Tutorial_BrowserIcon.png
