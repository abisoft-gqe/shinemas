.. shinemas documentation master file, created by
   sphinx-quickstart on Fri Jun 24 08:55:24 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

**S**\eeds **Hi**\story and **Ne**\twork **Ma**\nagement **S**\ystem
====================================================================

.. include:: introduction/introduction.rst

.. toctree::
   :hidden:
   :caption: Start with SHiNeMaS
   
   introduction/whatsnew
   tutorial/tuto
   installation/installation

.. toctree::
   :hidden:
   :caption: User Reference Guide
   
   admin/datamanagement
   viewers/consultingdata