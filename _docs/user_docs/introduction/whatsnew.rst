What's new in this version ?
============================

SHiNeMaS 2.1 is now distributed with additional new features :

* Web app design have been rebuild to offer a better user experience
* Home page have been changed with list of species
* Germplasm management :
   * Upload germplasm with files and link data to germplasms
   * New forms to create/update germplasm
   * Germplasm name have been spited in two fields : The real name of Germplasm without constraint on this name and idgermplasm : the id used in the SeedLot names (with constraints on the name)
* New species profil page : This page include a map with farmer using this species
* Update Germplasm profil page : Germplasm page include a map of fermers using this germplasm, also a network picture of this germplasm history
* Graph network explorer : An interface representing a network of seedlots


