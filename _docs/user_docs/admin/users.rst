Users
*****

===========================
Create/Modify/Delete a user
===========================
To create users, go to the menu Admin data > Actors & Users > Users.
 
.. figure:: ../_img/Admin-Tools-UserList.png
   :align: center
   
   *User list*

The screen below (*User list*) list all existing users. To create a new user click on ‘Add User’, a first form (*New user form*) ask to fill the login and password of the new user. Click on ‘Save’ and finish to create the user filling useful information (First name, last name, email) with the second form. If a user is destined to be an administrator of the database, check the boxes ‘staff status’ and ‘superuser status’.

To edit a user, from the all users screen, click on the login. All information of a user can be updated with the form. Click on ‘Save’ to validate the changes. To reset the password of a user follow the link ‘this form’ in the ‘Password’ field set. To delete a user click on the ‘Delete’ button in the left down corner of the form and then confirm in the next screen that you really want to delete this user.

.. figure:: ../_img/Admin-Tools-NewUser.png
   :align: center
   
   *New user form*

============
Users status
============

A user can have two status in SHiNeMaS : Administrator or read only. When creating/editing an admin user, it is import to check the two boxes ‘staff status’ and ‘superuser status’ (*User list*) that the user has a full access to administrator features. The ‘superuser’ field enables create/modify/delete features  on any data in the database. The ‘staff’ field gives access to the ‘admin’ screens of the SHiNeMaS web interface (link at the right top corner). The two boxes must be unchecked to create a read only user.