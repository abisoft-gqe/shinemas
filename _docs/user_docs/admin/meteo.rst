Climatic data
*************

Stations management
===================

To manage weather stations in SHiNeMaS go to the menu Admin data > Climatic data > stations.

This screen displays all the existing stations in the database.

To create a new station click on ‘Add weather station’. The form must be filled with a name for this station as well as a number (id), a longitude, a latitude, a start date and a status (open/close). Other information are optional : city, country altitude, end date, last update. Click on ‘Save button to validate its creation.
It's also possible to load a file describing Weather Station objects with the :ref:`admin interface <Admin import/export>`.

To edit a weather station, from the all stations screen, click on its name. All information of a station can be edited with the form. Click on the ‘Save’ button to validate the modification.

To delete a station, click on the ‘Delete’ button at the down left corner and then confirm deletion in the next screen.

Climatic variables
==================

There is two way to manage climatic variables in SHiNeMaS. You can use the :ref:`classic system variables <Variables>` management or the :ref:`CLIMATIK web service <Update variables>`.

Note that the type must be set to 'Weather hourly' or 'Weather daily' for climatic variables, else it won't appear in :ref:`climatic data query interfaces <Climatic data queries>`.

Management with files
=====================

To submit climatic data go to Prepare & upload files > Upload files > Climatic files. 

The data file must be separated text file (comma, semi-comma, tab etc.). Headers must have this structure :

.. code-block::

   variable1, variable1$date, variable2, variable2$date, variable3, variable3$date
   
Where variable1, variable2, variable3 are the names of the variables. The method file must filled as :ref:`network method files <Methods file>`. Note that type must be 'Weather hourly' or 'Weather daily' in that case.

Select your files in the form and choose your weather station that provided those data, then submit.

INRAE CLIMATIK service
======================

CLIMATIK is the climatic data service provided by INRAE for its collaborators. To access this service you need an INRAE account. The service is developed by `AgroClim lab <https://www6.paca.inrae.fr/agroclim/>`_.

Authentication
--------------

The first time you use CLIMATIK service you need to be authenticated. To do this go in User > Web Services > Climatik OAuth and then fill your INRAE authentication information.

Update stations list
--------------------

From Admin data > Climatic data > Web services click on 'Update stations' to update the list of the stations from INRAE Climatik service.

Update variables
----------------

From Admin data > Climatic data > Web services click on 'Update variables' to update the list of the variables from INRAE Climatik service.

