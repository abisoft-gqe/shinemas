Networks
********

.. include:: networkfiles/methods.rst

Network events
==============

.. toctree::
   
   networkfiles/filedesc
   networkfiles/preparefile
   networkfiles/submitfile
   networkfiles/eventforms
   networkfiles/edit_delete

