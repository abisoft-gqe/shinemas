Miscellaneous features
**********************

Audit files
===========
In the User > Submited files menu you have access to the log of the files you have submitted in SHiNeMaS. This feature ensure that a file can't be submitted twice, including files you haven't submitted yourself.
All files can be downloaded, this feature is enabled on the :ref:`seed lot file <Seeds lot management>` and storage file submission features. It aims to be implemented for all file submission interfaces in further versions.

.. figure:: /_img/AuditFiles.png
   :align: center
   
   *Audit files table*

Admin import/export
===================
Location, Person, Germplasm Method, variable Stations

From the admin interface you can load data with a file. On any admin interface of the objects described below, click on the 'Import' button and submit a file with the correct headers. Files can be of various format : csv, xlsx, xls, tsv, yaml, json

Person file headers :

.. code-block::

   location, first_name, last_name, short_name, birth_date, gender, email, phone1, phone2, fax

Location file headers :

.. code-block::

   location_name, location_type, address, post_code, country, longitude, latitude, altitude, short_name
   
Germplasm file headers :

.. code-block::

   person, germplasm_type, species, germplasm_name

Method file headers :

.. code-block::

   person, method_description, method_name, unit, quali_quanti_notes, ind_global

Variable file headers :

.. code-block::

   name, name_descriptor, type, season, unit, source

Stations file headers :

.. code-block::

   station_name, station_number, city, country, longitude, latitude, altitude, start_date, end_date, status, source, update_date

