Projects and actors
*******************

Projects management
===================

To manage projects go to the menu Admin data > Actors & Users > Projects. This screen displays a list of all projects in the database.

To create a new project, click on ‘Add project’, the form must be filled with a name and the starting date of this project. Click on the ‘Save’ button to validate it’s creation.

To edit a project, from the all project list screen, click on the name of a project. All information of a project can be edited with the next form. Click on  ‘Save’ to validate modification.

To delete a project click on the ‘Delete’ button in the down left corner of the form and then confirm deletion in the next screen.

Network actors (Person) management
==================================

The ‘person’ or actors of the network are physical people who produce data to store in the database without having necessarily a user access. To manage ‘people’ of SHiNeMaS go to the menu Admin data > Actors & Users > Persons. This screen displays all people already existing.

.. figure:: ../_img/Admin-Tools-NewPerson.png
   :align: center
   
   *Person creation form*
   
To create a new person click on ‘Add person’, the form (*Person creation form*) must be filled with a first name, a last name and a short name which will be used in many other screen of the app. By clicking on ‘Show’ a second part of the form is displayed in which contact information can be filled. Click on  ‘Save’ to validate its creation.
It's also possible to load a file describing Person objects with the :ref:`admin interface <Admin import/export>`.

To edit a person, from the people list screen, click on the short name. All information of a person can be edited from the form. Click on ‘Save’ to validate the modification.

To delete a person, click on the ‘Delete’ button in the down left corner and the confirm the deletion in the next screen.

Location/farm (Location) management
===================================

To manage locations in the app go in the menu Admin data > Actors & Users > Locations. This screen shows all the existing locations.

.. figure:: /_img/Admin-Tools-NewLocation.png
   :align: center
   
   *Location creation form*

To create a new location click on ‘Add location’. The form (*Location creation form*) must be filled with a name and other optional information as well as a short name (mandatory) for this location. This short name is import as it is used in the :ref:`name of a seed lot<seedlotnames>`. Click on ‘Save’ to validate its creation. It's also possible to load a file describing Location objects with the :ref:`admin interface <Admin import/export>`.

To edit a location, from the all locations screen, click on a location name. All information except the short name can be edited with the form. Click on ‘Save’ to validate the modification.

To delete a location click on the « Delete » button in the down left corner of the form and then confirm the deletion in the next screen.

Storage devices management
==========================

By default in SHiNeMaS, seeds lot are considered stored at the location (a farm, an experimentation site or whatever the location means). It is possible to get an accurate information about the storage of seed lot. You can declare storage devices, devices can be stacked to define a maximum 4 level tree of devices.

Go to Admin data > Actors & Users > Storage devices. Select a location where you want to add devices.

.. figure:: /_img/Admin-StorageDevices.png
   :align: center
   
   *Storage devices management interface*

The first level is the Location itself. To add a device to an existing upper level click on the add button of this upper level. Fill the form and submit to create this device and link it as a sub level. By clicking on the 'view' button you will be able to check the content of the device.

