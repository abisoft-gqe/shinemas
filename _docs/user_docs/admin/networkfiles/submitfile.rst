Network file submission
-----------------------

In Prepare & upload files > Upload files > Network files, choose a data file in the "File" field, a method file in the "Method" field. Select the correct file type from: Cross, Diffusion, Mixture, Multiplication, Intra varietal selection or Individual data. Click on "Submit".

**Special treatments when submitting a file**

* When the project field indicates a project that does not exist this one is automatically created by the application (will be removed in a future version)
* Seed lots that do not exist in the database are treated as described in :ref:`this section<seedlotnames>`  (will be removed in a future version)

**Case of crosses**

* In the case of a number_crosses equal to 0 the application generates a virtual seed lot named according to the nomenclature NOTCROSSED_NUM, NUM ensuring its uniqueness.
* In the case of a kernel_number_F1 equal to 0 the application generates a virtual seed lot named according to the nomenclature NOTCROSSED_NUM, NUM ensuring its uniqueness.

**Case of reproductions**

* When reproduction is in progress at a given place (even location, X, Y) you can not sow a seed lot of the same species.
* However, you can add data to a current reproduction. The reproduction will be identified by the same seed lot sown at the same place.

**Case of selections**

* A selection is always associated with a reproduction (identified by the seed lot sown, and its place). However, if a selection event is filled in a file but no reproduction matches, the application will create a new reproduction. 
* A selection name can only be used once for a germplasm

**Case of individual data**

* Reproductions declared in the individual data file must exist

Submission report and post submission process
---------------------------------------------
Success report
~~~~~~~~~~~~~~
A report is produced after each submission (*Success report*). This submission report summarizes information that was included into the database. This information is of different types and indicates:

* New created variables
* New created varieties
* New created seed lots
* New created relation between seed lots
* On going replicates
* Seed lots without initial quantity
* Relations which quantities are not indicated
* Reproductions which coordinates are not indicated

It is also possible to perform some actions directly from the submission report. These actions are detailed in the :ref:`next section <Post submission process>`

.. figure:: /_img/UploadFile-Report4.png
   :align: center
   
   *Success report*

Error report
~~~~~~~~~~~~
In some cases, file submission can produce errors (*Error reports*) that stop the submission process before the end. In this case, all on going operations are canceled and an error report is produced.

.. figure:: /_img/UploadFile-Errors.png
   :align: center
   
   *Errors report*

These errors can be of different types :

* a variable was found in the method file or in the data file and not within the other.
* one line of the file was not associated to a project
* a method did not exists in the database
* a variety already existed in the database (in the case of crossing or mixture)
* The variety name contained a forbidden character
* One actor of the network no longer exist
* The seed lot name was not filled
* The seed lot name contained a forbidden character '#'
* The seed lot no longer exist in the database
* No-valid value for a ‘split’ field (i.e. value different of 0 or 1)
* Seed lot nomenclature was not respected
* Some mandatory fields were empty
* Some correlation groups were inconsistent
* Some dates were inconsistent (for instance : sowing date after harvesting date)
* Some errors were detected for the locations of the reproductions

Post submission process
~~~~~~~~~~~~~~~~~~~~~~~
**Link a germplasm type to a germplasm**

It is possible to associate a variety type for a variety newly created directly from the submission report. Variety types need to be created previously to the association (*Link a germplasm type*). In the table the column 'Choose a germplasm type' allows to select which type of variety to be associated to the variety name cited in the column  'Germplasm'. Click on the 'Submit' button only after associating all the varieties types to the varieties.

It is always possible to :ref:`modify the type of variety <Germplasm management>` associated to a variety after this step.

.. figure:: /_img/UploadFile-Report1.png
   :align: center
   
   *Link a germplasm type*

**Link a reproduction type to a reproduction**

It is possible to associate a reproduction type to an on going reproduction directly from the submission report. To be able to make such associations, :ref:`reproduction methods <Reproduction methods>` need to be created before the production of the submission report. In the example *Link a reproduction type* the line 'Choose the reproduction type' allows to select which type of reproduction is associated to the reproduction describes on the above line. Click on the 'Submit' button only when all methods are selected.

.. figure:: /_img/UploadFile-Report3.png
   :align: center
   
   *Link a reproduction type*

**Merge seed lots coming from different replicates after a submission**

It is also possible to merge on going seed lots coming from different replicates from the submission report. Each series of seed lots distributed in different replicates is separated by '\*\*\*'. Seed lots that needs to be merged can be selected by checking proper boxes. After selecting the seed lots, clicking on the 'Merge' button. It is necessary to repeat this operation for each series of seed lots distributed in different replicates.

Merging seed lots coming from different replicates leads to create a new mixture event without creating a new variety (which is the case for a :ref:`mixture <Mixture>`). Be careful, this operation is irreversible.

It is always possible to :ref:`perform this operation <Merge replicate>` latter if the user leaves the report submission before merging all the needed seed lots coming from different replicates.
