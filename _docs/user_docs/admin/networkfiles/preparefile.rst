Prepare a file
--------------

General usage
~~~~~~~~~~~~~
To prepare a file, go to the Prepare & Upload File> Prepare Files> eventType. For each event type you will find an interface that makes possible to prepare a file for your network events. This interface provides a “download template file” link to download a file filled with mandatory headers for the event you want to submit.

A simplified search interface (*Prepare file : search form*) suggests to search for seeds or relations already filled in the database according to 4 criteria: year of creation, projects in which they are used, place on which they are stored and the species.


.. figure:: /_img/PrepareFile-Form.png
   :align: center
   
   *Prepare file : search form*

Once the search has been done you can select seed lots or relation according the type of events you are working with. Cross-breeding event propose two icons |male| and |female| (*Prepare cross-breeding file*) that allow by a single click to add the seed lot to a list of lots that will be used as male or female. 

In other cases an add button |add| (*Prepare diffusion file*) will allow you to add the relation or the seed lot to the list you are building.

For a diffusion event the destination can be filled with a select box (*Prepare diffusion file*) while for mixture event the name of the new germplasm can be filled with a text field.

In any case, the list is visible by clicking on the |show| icon on the right of the results list. This action brings up a window that allows you to delete |delete| seed lots or relation from the list or to complete the file preparation information.

Clicking on the 'Export File' button will download the pre-filled cross-breeding file. A user can also modify the search filters and continue to complete his list to export.

.. figure:: /_img/PrepareFile-Cross.png
   :align: center
   
   *Prepare cross-breeding file*

.. figure:: /_img/PrepareFile-Diffusion.png
   :align: center
   
   *Prepare diffusion file*

.. _seedlotnames:

Rules about nomenclature of seeds lots
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Case of already existing seed lots in the database**

A nomenclature has been defined in SHiNeMaS in order to ensure the uniqueness of the data base seeds lots names. It bases on 3 indications : the name of the variety (VARIETY), the seed lot’s creation year (YEAR) and the location of this seed lot (LOCATION). We can imagine that one single variety has given several seed lots during one year on a single location. Therefore, a fourth indication is used : a four digits code (NUM) ensuring the uniqueness of a seeds lot.

Those indications are separated by an underscore (‘_’). The use of this character is then forbidden to define a variety’s name or the short name of a person.

A seeds lots’ name is written as follows :
*VARIETY_YEAR_PERSON_NUM*

**Case of non already existing seeds lots in the database**

This case happens when a seed lot enters the seeds history network although it has never appeared in the network before. In that case, the user has to indicate the following indications in the box corresponding to the seeds lot ID :

*VARIETY_YEAR_PERSON*

The app will calculate automatically the four digits extension necessary to (1) ensure the uniqueness of the seeds lot’s name and (2) respect the nomenclature defined in SHiNeMaS. If the variety VARIETY doesn’t exist in the database, it would be created automatically.

It is important to indicate that it is possible to use the same structure *VARIETY_YEAR_PERSON* several times in a single file. The app will consider that it refers to the same seeds lots used several times.

**Selections’ cases**

When a :ref:`selection<Selection>` is done the seeds lot’s name follows a specific nomenclature including the selection’s name (SEL) :

*VARIETY#SEL_YEAR_PERSON_NUM*

The ‘#’ character (diez) is used to combine the variety’s name to the selection’s name. This character cannot be used in the variety’s name. The expression VARIETY#SEL will be transferred to all the descending seeds lots when it is a reproduction or a spreading. If a new selection (NSEL) is done on a descending seeds lots, the expression SEL is then replaced by NSEL as follows :

*VARIETY#NSEL_YEAR_PERSON_NUM*

As a mixture or a cross-breeding creates a new variety, the selection name’s indication disappears and the seeds lot’s name a classic nomenclature described in the first paragraph.


Selection table
~~~~~~~~~~~~~~~
The selection table (Figure 40) displays a table with all the selection names used for a germplasm. You can find it in the « Admin data > Tools > Selection table » menu. 

When many selection already occurred, a form (germplasm and selection name) is available to test if the name is already used.

.. |male| image:: /_img/male_icon.png
.. |female| image:: /_img/female_icon.png
.. |add| image:: /_img/add.png
.. |delete| image:: /_img/delete.png
.. |show| image:: /_img/shape_move.png

