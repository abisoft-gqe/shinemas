File formats
------------

The submission files in SHiNeMaS are « tabulated » files and must fit with the following rules :

* Text file
* Column separator : tabulation
* Text delimiter : "

For example, a file open with a simple text editor, that will be submitted into SHiNeMaS must look like this :

.. code-block::

   "project" "location" "id_seed_lot" "etiquette" "event_year" "split" "quantity"
   "PPB"  "FLM"  "Rouge-du-Roc_JFB_2004_0001"  2004 1 250
   "PPB"  "FLM"  "Clomone_JFB_2004_0001"  2004 1 122

Each file type is described by mandatory fields (headers). It is possible to add additional columns regarding the method file (section :ref:`Methods file<Methods file>` ). With this feature a user can customize variables definition.

Methods file
------------

The method file contains the description of measures and the methods used to make these measures. Four fields described this file :

* **variable** : the name of the measured variable
* **type** : the type of the variable (described in :ref:`this section<variablestypes>`)
* **method_name** : the name of the :ref:`method used <Data methods>` to measure this variable.
* **correlation_group** : correlation groups used for this variable. Correlation groups usage is described in :ref:`this section <Correlation groups>`.

.. include:: /admin/networkfiles/events.rst


Correlation groups
~~~~~~~~~~~~~~~~~~
Correlation groups are used for individual data. Let’s consider a data series (at a same date for example) on numerated individuals. For the next series (at another date) the user collect data on the same number of individuals. But two scenarios can occur :

* The numerated individuals are not the same (You lost the numeration between the two series of data). In that case the data of the first series and the second series can’t be correlated as it’s haven’t be done on the same individuals. Example : In figure A, on a single line, data in green, blue and orange aren’t correlated and belongs to 3 different groups B, C, and D
* The numerated individuals are the same. In that case all data can belong to the same correlation group. Example : In figure A, the lines in yellow. The data belongs to the group A.

.. figure:: /_img/CorrelationGroups1.png
   :align: center
   
   *A : Reproduction file with correlation groups*

In the method file you should report in the correlation_group column all the correlation groups in which a variable is implied (figure B). For example, the variable ‘plant_height’ is implied in the correlation groups A (yellow) and B (green).

.. figure:: /_img/CorrelationGroups2.png
   :align: center
   
   *B : Method file with correlation groups*

Particular cases
~~~~~~~~~~~~~~~~
It is possible to measure a same variable with two different method. To do the variable must be tagged with a prefix in the data file and in the method file. The following syntax is used (Figure below)  :
*prefix%variable_name*

.. figure:: /_img/MethodFile2methods.png
   :align: center
   
   *Variable using 2 differents methods*
