Events creation with form
-------------------------
Administration interfaces help the user to create on-time event, independent to the file submission. They allow to gather events older than a series of event already documented in the database. 

Diffusion form
~~~~~~~~~~~~~~
Go to the menu: Admin data > Network > Diffusion > New, to create a diffusion event. Administrator interface is divided into three blocks of information (*New diffusion form*).

.. figure:: /_img/Admin-Tools-NewDiffusion.png
   :align: center
   
   *New diffusion form*

In the block ‘Seed lot diffused’, the user has to fill the diffused seed lot (to choose within a list of existing seed lots) or to provide information to create a new one. In the block ‘Diffusion information’, the user has to fill the targeted seed lot involved in the diffusion relation by selecting within the list of the existing seed lot or by creating a new one by filling the fields ‘receiver’ and ‘event_year’. The user has to fill the other fields of the block.

The different rules to create a diffusion event by :ref:`loading a file <Diffusion>` also apply to this interface.

Note that when the user selects an existing seed lot within the block ‘Diffusion information’, this one cannot be created by any other relation. Otherwise, an error message will appear. Finally, in the block ‘Additional information’ the user can add data linked to this diffusion event. By clicking on the button |add| make additional lines appear. Then, to click on the ‘Create’ button to valid the event creation.

Reproduction form
~~~~~~~~~~~~~~~~~
Go to the menu: Admin data > Network > Reproduction > New, to create a reproduction event. The administration interface (*New reproduction form*) is divided into three blocks of information.

.. figure:: /_img/Admin-Tools-NewReproduction.png
   :align: center
   
   *New reproduction form*

In the block ‘Sown seed lot information’, the user has to fill the seed lot that was sown by choosing within a list of existing seed lots or by filling the required information to create a new one. In the block ‘Reproduction information’ the user has to fill the harvested seed lot by selecting the seed lot in the list coming from the reproduction relation or by creating a new one by filling the field ‘harvested_year’. The user has to fill the other fields of the block.

The different rules to create a reproduction event by :ref:`loading a file <Reproduction>` also apply to this interface. Note that when the user select an existing seed lot in the block ‘Reproduction information’, this one cannot be created by any other relation. Otherwise, an error message will appears. Finally, in the block ‘Additional information’ the user can add data linked to this reproduction event. By clicking on the |add| button make additional lines appear. Then, to click on the ‘Create’ button to valid the event creation.

Selection form
~~~~~~~~~~~~~~
Go to the menu : Admin data > Network > Selection > New, to create a selection event. The administration interface (*New selection form*) is divided into two blocks of information.

.. figure:: /_img/Admin-Tools-NewSelection.png
   :align: center
   
   *New selection form*


In the block ‘Selection information’, the user has to fill an existing reproduction event by choosing it within a list. Then, to fill the other field of the block.

The different rules to create a selection event by :ref:`loading a file <Selection>` also apply to this interface. Finally, in the block ‘Additional information’ the user can add data linked to this selection event. By clicking on the button |add| make additional lines appear. Then, to click on the ‘Create’ button to valid the event creation. 

Cross-breeding form
~~~~~~~~~~~~~~~~~~~
Go to the menu : Admin data > Network > Cross > New, to create a cross event. The administration interface (*New cross-breeding form*) is divided into four blocks of information.

.. figure:: /_img/Admin-Tools-NewSelection.png
   :align: center
   
   *New cross-breeding form*

In the block ‘Male seed lot information’, the user has to fill the male seed lot by choosing within a list of existing seed lots, or by providing information to create a new one by filling the fields ‘location’, ‘year’, and ‘germplasm’. Then, one has to fill information relative to the sowing of the male seed lot. In the block ‘Female seed lot information’, the user has to fill the female seed lot by choosing within a list of existing seed lots, or by providing information to create a new one by filling the fields ‘location’, ‘year’, and ‘germplasm’. Then, one has to fill sowing information of the female seed lot. In the block ‘Cross information’, the user has to fill the targeted seed lot of the cross relation by selecting it within a list, or by filling field ‘species’ and ‘germplasm’ to create a new one. Then, the user has to fill the other fields of the block. 

The different rules to create a cross event by :ref:`loading a file <Cross-breeding>` also apply to this interface.

Note that when the user select an existing seed lot in the block ‘Cross information’, this one cannot be created by any other relation. Otherwise, an error message will appears. Finally, in the block ‘Additional information’ the user can add data linked to this cross event. By clicking on the button |add| make additional lines appear. Then, to click on the ‘Create’ button to valid the event creation.

.. |add| image:: /_img/add.png