Merge replicate
---------------
Seed lots coming from different replicates can be merged by the admin user later after the submission step of reproduction file. To process to such change, go to the menu Admin data > Tools > Merge Reps. A screen (*Merge repetitions*) shows all the seed lots coming from different replicates that can be merged.

.. figure:: /_img/Admin-Tools-MergeReps.png
   :align: center
   
   *Merge repetitions*

It is possible to select/unselect seed lots to merge for each line. Select the corresponding project that need to be link to this event and click on the ‘Merge’ button. A message appears with the name of the new created seed lot when operation happens successfully.

Edit raw data
-------------
From the menu Admin data > Seed lot relation > Any relation type > Edit raw data, a user can access to a simple autocompletion list where he can choose the relation containing the data to edit. As soon as the event card appears the user, if he is an administrator of the database, can click on a data and display a form in which the data can be edited (*Edit raw data*).

.. figure:: /_img/Admin-EditRawdata.png
   :align: center
   
   *Edit raw data*

When a data is edited, the user can add a comment to explain why this edition, or keep the track of the previous value. Note that the date of the last update and the username is also track and displayed in the data table in the card when moving the mouse over the value.

Delete information
------------------
Delete events
~~~~~~~~~~~~~

.. _deletecascade:

**Delete an event through the administration interface**

Go to the menu : Admin data > Network > « Event type » > Delete, to remove an event. The user can access to an administration interface which lists all the events of the event type selected by the user. On the right, filters help the user to limit this list. This page allows to select different events and to remove them through the ‘Delete selected « event »’ action. Nevertheless, this method is to avoid because deletion would be partial. To remove an event properly, it is advised to click on its name. The page of the event details appears and propose two kinds of deletion :

* The ‘Delete’ button helps the user to remove the considered event with all the associated data/measurements at the condition that these data are not associated themselves to another event. If the « child » seed lot is not used in any other relation, it is also removed. 
* The ‘Delete in cascade’ button helps the user to remove the considered event with all the posterior events resulting of this event. On the same way, all the associated data/measurements will be removed at the condition that these data are not associated themselves to another event.
In the both cases, the user will be directed to a page that will ask him to confirm the deletion. To confirm the choice, to click on the ‘Yes I’m sure’ button. Otherwise, to click on the ‘No, take me back’ button.

**Delete reproduction events from a file**

From the menu Prepare & upload files > Delete from file > Reproduction, a user can delete a set of events by submitting a file. The first step (*Delete reproductions from a file*) aims to sumbit a reproduction file.

.. figure:: /_img/Admin-DeleteFromFile.png
   :align: center
   
   *Delete reproductions from a file*

This file has the same format that the one described in the section :ref:`Reproduction<Reproduction>`. The deletion can be done in cascade or not as described in the :ref:`previous section<deletecascade>`. The second step read the file and inform the user of the reproductions that will be deleted. If the user accepts the reproductions are deleted and a report is displayed (step 3). The user can also go back to the submission form.

Delete raw data
~~~~~~~~~~~~~~~

From the menu Admin > Seed lot relation > Reproduction > Delete raw data a user can delete a set of raw data without deleting related reproductions. Raw data can be selected with a form (*Delete raw data*) filtering according to locations, germplasms, variables, a range of date (including also data without date), and the sowing year.

.. figure:: /_img/Admin-DeleteRawData.png
   :align: center
   
   *Delete raw data*

The second step list all the raw data found as result of the query. The user can select/unselect each value before validation. As soon as the user has accepted his selection a report is displayed with the number of raw data deleted for each reproduction. Just below this report the user can download two templates files filed with the relations and the variables related to the raw data deleted.
