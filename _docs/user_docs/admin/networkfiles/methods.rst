Reproduction methods
====================
To manage reproduction methods in SHiNeMaS go to the menu Admin data > Data Descriptors > Repro. method. 

This screen displays all the existing reproduction methods in the database. To create a new reproduction method click on ‘Add reproduction_method’. The form must be filled with a name for this method as well as a description (optional) for this method. Click on ‘Save button to validate its creation.

To edit a reproduction method, from the all reproduction methods screen, click on its name. All information of a reproduction method can be edited with the form. Click on the ‘Save’ button to validate the modification.

To delete a reproduction method, click on the ‘Delete’ button at the down left corner and then confirm deletion in the next screen.

Data methods
============
To manage data methods in SHiNeMaS go to the menu Admin data > Data Descriptors > Data method.

This screen displays all the existing data methods in the database.

To create a new data method click on ‘Add method’. The form must be filled with a name for this method as well as a description, a unit and some other information (optional) for this method. Click on ‘Save button to validate its creation.
It's also possible to load a file describing Method objects with the :ref:`admin interface <Admin import/export>`.

To edit a data method, from the all data methods screen, click on its name. All information of a data method can be edited with the form. Click on the ‘Save’ button to validate the modification.

To delete a data method, click on the ‘Delete’ button at the down left corner and then confirm deletion in the next screen.

Variables
=========
To manage data variables in SHiNeMaS go to the menu Admin data > Data Descriptors > Data variables.

This screen displays all the existing data variables in the database.

To create a new data variable click on ‘Add variable’. The form must be filled with a name for this variable as well as a description, a unit, type and some other information (optional) for this variable. Click on 'Save' button to validate its creation.
It's also possible to load a file describing Variable objects with the :ref:`admin interface <Admin import/export>`.

To edit a data variable, from the all data variables screen, click on its name. All information of a data variables can be edited with the form. Click on the ‘Save’ button to validate the modification.

To delete a data variable, click on the ‘Delete’ button at the down left corner and then confirm deletion in the next screen.

Variables types
~~~~~~~~~~~~~~~
Each data can have a different scope regarding the context. The scope affect how data are linked to the relations in a particular event. The table 1 described the variable types regarding their scope.
To create new variables it's possible to load a file describing Variable objects with the :ref:`admin interface <Admin import/export>`.

.. _variablestypes:

.. table:: Variables types table
   :widths: auto

   ======= =================================================================================
   Type    Scope
   ======= =================================================================================
   Type 1  Whatever the event type, the data is linked to all the relations. 
   Type 2  In a breeding-cross the data is linked to the male relation
   Type 3  In a breeding-cross the data is linked to the female relation
   Type 4  In a reproduction, the data is linked only to the selection
   Type 5  In a mixture, the data is linked to only one relation of the mixture
   Type 6  In a reproduction, the data is linked to any relation that is not a selection
   Type 7  The data is linked to the parent seeds lot
   Type 8  The data is linked to the child seeds lot
   Type 9  In a breeding-cross, the data is linked to the male seeds lot
   Type 10 In a breeding-cross, they data is linked to the female seeds lot
   Type 11 The data is  an individual data
   ======= =================================================================================