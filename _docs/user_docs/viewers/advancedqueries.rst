Advanced network queries
========================

Filters
-------

The Advanced query interface makes possible to filter results according to some criteria :

* 'Creation year' : creation year of the seed lot. It can be harvesting year, reception year (in the case of a diffusion), mixture year etc.
* 'Projects' : projects in which the seed lot is used
* 'Person' : person owning this seed lot
* 'Relation type' : Filter on relation type which creates this seed lot (Diffusion, mixture, reproduction etc.)
* 'Germplasm' : Filter on germplasm

.. figure:: ../_img/AdvancedQueryForm.png
   :align: center
   
   *Advanced Query Form*

Each filter offers a box 'Not' which allows to define an exclusion criteria (« All except »...). Click on the « Search » button to apply filters.

Classic mode
------------

The « Classic » query mode shows the list of seed lot from applied filters. Some additional information are available :

* Seed lot name : the name of the seed lot
* Relation : The relation type that produce this seed lot
* Parent names : The names of parents seed lot
* Grandparents Relation : The relation type that produce parents seed lot
* Grandparents : The names of grandparents seed lot
* Selection name : The selection name when the seed lot is produced from a selection
* Selection person : Person who made the selection when a seed lot is produced from a selection
* Origin seed lot : Location where the seed lot comes from
* Diffusion date : Year of the last diffusion of the seed lot

Generation mode
---------------

The « Generation » query mode shows the list of seed lot according to selected filters. It also shows information about generation number (total and on a same location)  :

* Seed lot name : Seed lot name
* First parents : Name of the parents used for the cross from origins (when there was one)
* Total number of generation : Total number of multiplication needful to obtain this seed lot
* Number of generation on the last farm : Number of multiplication that happened on this location to obtain this seed lot
* Confidence : Confidence accorded to the number of generation. For example a seed lot which integrate spontaneously the network will have a default confidence at false
* Confidence on last farm : Confidence accorded to the number of generation on a same location.

.. figure:: ../_img/AdvancedQueryGeneration.png
   :align: center
   
   *Advanced Query Generation table*

Measures mode
-------------

The «Measures» query mode shows collected data on a given time interval. User fill variables name in the form and the dates that define the time interval. The results are shown in 3 tables :

* Seed lot data : the table displays the seed lot name and data related to this seed lot.
* Individual data related to the relations : the table displays the name of the relation as well as coordinates in the field, individual number and data related to this individual.
* Global data related to the relations : the table displays the name of the relation as well as coordinates of the reproduction and data related to the plot.

.. figure:: ../_img/AdvancedQueryMeasures.png
   :align: center
   
   *Advanced Query Measures table*

