Data cards
==========

All these cards can be accessed from the `global search bar <Global search toolbar>` visible anywhere in SHiNeMaS.

Seed lot card
-------------

:ref:`Advanced query <Advanced network queries>` displays a list of seed lot. By clicking on the name of a seed lot, it is possible to view the seed lot card.

.. figure:: ../_img/Profil-SeedLot.png
   :align: center
   
   *Seed lot card*

The card gives information ('seed lot information' frame) about the variety of the seed lot, its owner, its creation date (harvesting, diffusion, mixture etc.) as well as the project in which the seed lot is used.

History of the seed lot is also displayed, especially the event that created the seed lot and the events in which it is used ('seed lot history' frame).

At last, the frame 'stock information', displays data about stock information if the seed lot.

Germplasm card
--------------

From a :ref:`seed lot card <Seed lot card>`  it is possible to click on the name of a variety to displays its card.

.. figure:: ../_img/Profil-Germplasm.png
   :align: center
   
   *Germplasm card*

The 'Germplasm information' frame shows the name of the variety, the person who obtained this variety when information is available, as well as the type of the variety.

The 'Germplasm history' frame displays how the variety has been created especially when it is a cross or a mixture.

At last, the 'stock information' frame shows who owns seed lots of this variety. Contact information can be shown when it is possible.

Relation card
-------------

From a :ref:`seed lot card <Seed lot card>` it is possible to click on the ‘relation’ icon to display its card.

.. figure:: ../_img/Profil-Relation.png
   :align: center
   
   *Relation card*

The ‘Relation information’ frame displays the type of the relation, as well as all information related to this relation (date, coordinates when it is a cross or a reproduction, quantity used etc.)

The ‘Data’ frame displays all data collected in this relation.