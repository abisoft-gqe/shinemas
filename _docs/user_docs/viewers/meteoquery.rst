Climatic data queries
=====================

Go to Search > Climatic query. 

.. figure:: /_img/ClimaticDataSearch.png
   :align: center
   
   *Climatic data search form*
   
Start by filling a range of date for your query, the periodicity (hourly or daily) and a list of variables. Select a Location that will be considered as the center point of your query. You will then have access to a list of stations located at less than 50km from you Location ordered by status closed/open and the distance to your Location.

You will be able to choose 3 stations by order of preference. This order is important as long as when you will run the query, priority will be given to data found at the first station, then at the second station etc.

The result will be display in the 'Climatic data' section. The first part of the result gives information about (1) the ariables with the origin of the data and (2) stations with the distance to the Location. The second part is the climatic data table.

.. figure:: /_img/ClimaticDataResult.png
   :align: center
   
   *Climatic data table*
  