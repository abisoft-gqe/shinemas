Web Services
============

SHiNeMaS propose a REST API to access data through third party tools or programmatically.

Authentication token
--------------------

Using the REST API need an authentication token. To manage your token go in User > API token.

.. figure:: /_img/APIToken.png
   :align: center
   
   *Token information interface*

You can generate a new token by clicking on the dedicated button. As soon as you create a new token this one will be available 60 days. 
If your current token has expired a message will inform you  about it and you willbe able to create a new one.

Use the API
-----------

The API documentation is embedded in SHiNeMaS' web app. You can find it in About > API doc.

.. figure:: /_img/APIMethods.png
   :align: center
   
   *List of the API methods*