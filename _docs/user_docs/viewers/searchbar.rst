Global search toolbar
=====================

From any screen in SHiNeMaS a global search bar at the top of the web interface provides a useful tool to search any kind of information (Germplasm, Seed lot or Relation) in the database.

.. figure:: ../_img/SearchBar.png
   :align: center
   
   *Search bar*

The search is done by auto-completion, meaning that a user starts to write a text in the bar and a restricted list is shown in the bar. As soon as an object is selected a click on the “Go” button forward to the corresponding card (see below the description of the cards).