How to install SHiNeMaS
***********************


Download code source
====================

Download SHiNemaS' source code link to v2.0

.. code-block::

   $ tar xvzf shinemas2.0.tar.gz
   $ cd “shinemas/”

Dependencies
============

Debian packages
---------------

On Debian or any other distribution like Ubuntu or Mint, SHiNeMaS uses Python version 3 and Postgresql (9.3 < ) :

.. code-block::

   $ sudo apt-get install python3
   $ sudo apt-get install postgresql

Python packages
---------------

To facilitate python packages installation it’s better to use python packages manager 'pip' : 

.. code-block::

   $ sudo apt-get install python3-pip

Assuming you are in the 'shinemas/' folder :

.. code-block::

   $ pip3 install -r requirements.txt

.. note::

   There can be some troubles with psycopg2 installation, this one should be installed according to your postgreSQL version. Check `psycopg documentation <https://www.psycopg.org>`_ for more information.

Database creation & initialzation
=================================

Database configuration
----------------------

First, if you installed postgreSQL for the first time, you need to create a PostgreSQL superuser.

.. code-block::

   $ sudo su postgres
   $ createuser -s -P pguser
   $ exit

where pguser is the username you want to use as the PostgreSQL superuser. You'll have to fill a pguser_passsword for this user.
Edit the pg_hba.conf file (postgresql configuration file) :

.. code-block::

   $ sudo pluma /etc/postgresql/9.x/main/pg_hba.conf

.. note::

   pluma may not be your default text editor, in that case you case use gedit, vi or any other text editor

.. code-block::

   $ sudo gedit /etc/postgresql/12/main/pg_hba.conf

12 is the postgresql version. You can have a different version on your server.

.. code-block::

   #For Unix socket change anthentication method to “md5” (“peer” by default)
   # "local" is for Unix domain socket connections only
   local   all             all                                     md5
 
Restart Postgresql :

.. code-block::

   $ sudo /etc/init.d/postgresql restart

Then create the database

.. code-block::

   $ createdb -U pguser shinemasdb

Edit shinemas settings file
   
.. code-block::
  
   $ pluma config.py
   

Edit the parameters, this configuration should work :
   
.. code-block::

   """""""""""""""""""""""""""""""""""""""""""""""""""""
   CUSTOM SETTINGS
   """""""""""""""
   DJANGO_ROOT_SETTING = ''
   ALLOWED_HOSTS = ['127.0.0.1','//.your.domain.here//'] #example : moulon.inra.fr
   DATABASES = {
     'default': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 
   'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
         'NAME': 'shinemasdb',        # Or path to database file if using sqlite3.
         'USER': '//pguser//',         # Not used with sqlite3.
         'PASSWORD': '//pguser_password//',     # Not used with sqlite3.
         'HOST': '',  #Set to empty string for localhost. Not used with sqlite3.
         'PORT': '',  # Set to empty string for default. Not used with sqlite3.
         'ATOMIC_REQUESTS': True     
     }
   }
   #we use sqlite if test are run
   #if 'test' in sys.argv:
   #    DATABASES['default'] = {'ENGINE': 'django.db.backends.sqlite3'}
   # Local time zone for this installation. Choices can be found here:
   # http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
   # although not all choices may be available on all operating systems.
   # If running in a Windows environment this must be set to the same as your
   # system time zone.
   TIME_ZONE = 'Europe/Paris'
   # Absolute path to the directory that holds media.
   # Example: "/home/media/media.lawrence.com/"
   MEDIA_ROOT =  "///your/storing/folder/here//" #example : /var/store/shinemas/
   ROOT_URL = ''

Database initialization:
------------------------

.. code-block::

   $ python3 manage.py migrate
   $ python3 manage.py createsuperuser

Fill information to create a superuser for shinemas. The superuser is the person who will administrate the data.


Deploy and use SHiNeMaS
-----------------------

Test installation. From the repository that contains SHiNeMaS source code :

.. code-block::

   $ python3 manage.py runserver

Keep the terminal alive, open a web browser and write the following URL : `http://127.0.0.1:8000 <http://127.0.0.1:8000>`_
SHiNeMaS home page should now be displayed.

Deploying SHiNeMaS with Apache
------------------------------

The best way is to read `this article <https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/modwsgi/>`_ on Django documentation.
