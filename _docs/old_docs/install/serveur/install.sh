#!/bin/bash

apt-get update
apt-get install python3
apt-get install postgresql
read -p "Please, edit pg_hba.conf file as explained in the documentation, tape 'y' when done\n" -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi
apt-get install python3-pip
apt-get install python3-psycopg2
pip3 install Django==2.0
pip3 install jsonfield==2.0.2
pip3 install django-autocomplete-light==3.3.5
pip3 install bokeh==1.2.0
pip3 install django-querysetsequence==0.11
su postgres -c "createuser -s -P shinemas"
/etc/init.d/postgresql restart
createdb -U shinemas shinemasdb
python3 ../../manage.py migrate
python3 ../../manage.py createsuperuser

