# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Melanie POLART-DONAT

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.contrib import admin
from django.utils.translation import ugettext as _

from images.models import Image, ImageLink
from import_export.admin import ImportExportModelAdmin

class ImageAdmin(admin.ModelAdmin):
     
    list_display = ('image_name',)
    search_fields = ('image_name',)
    readonly_fields = ('image_name','date', 'comment', 'footprint') 
    fieldsets = (
                 (_('Image'),{'fields':['image_path','image_name','date','comment', 'footprint']}),
                 )
    

admin.site.register(Image, ImageAdmin)
admin.site.register(ImageLink)

