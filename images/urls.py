# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Melanie Polart-Donat

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.urls import include, path

from images.views import UploadedImagesView, LinkImagesView, UploadSummaryView, ImagesManagementView, image_render



urlpatterns = [              
                path('upload_images/', UploadedImagesView.as_view(), name='upload_images'),
                path('link_images/', LinkImagesView.as_view(), name='link_images'),
                path('upload_summary/', UploadSummaryView.as_view(), name='upload_summary'),
                path('images_management/<int:image_id>/', ImagesManagementView.as_view(), name='images_management'),
                path('render/<str:footprint>/', image_render, name='imagerender')
    ]