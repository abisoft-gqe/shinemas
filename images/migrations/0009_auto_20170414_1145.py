# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2017-04-14 11:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('images', '0008_auto_20170414_1143'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='image',
            name='germplasm',
        ),
        migrations.RemoveField(
            model_name='image',
            name='relation',
        ),
        migrations.RemoveField(
            model_name='image',
            name='seed_lot',
        ),
        migrations.AddField(
            model_name='image',
            name='content_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType'),
        ),
        migrations.AddField(
            model_name='image',
            name='object_id',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
