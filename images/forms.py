# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014, Melanie Polart-Donat

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""
from dal import autocomplete

from django import forms
from django.forms.formsets import formset_factory

from entities.models import Germplasm, Seedlot
from network.models import Relation


class MultipleImageForm(forms.Form):
    input_file = forms.FileField(widget=forms.FileInput(attrs={'multiple':True}))
    #input_file = MultipleUploadedFileField()
    #input_index_file = UploadedFileField(required=False)
    input_index_file = forms.FileField(required=False)
    
    """def save(self):
        pass          """      

class LinkForm(forms.Form):

    germplasms = forms.ModelMultipleChoiceField(queryset=Germplasm.objects.all().order_by('idgermplasm'),required=False,
                                             widget=autocomplete.ModelSelect2Multiple(url='germplasm-autocomplete', attrs={'style': 'width:350px'})
                                            )

    relations = forms.ModelMultipleChoiceField(queryset=Relation.objects.all().order_by('seed_lot_father_id'),required=False,
                                             widget=autocomplete.ModelSelect2Multiple(url='relation-autocomplete', attrs={'style': 'width:350px'})
                                            )
    
    seed_lots = forms.ModelMultipleChoiceField(queryset=Seedlot.objects.all().order_by('name'),required=False,
                                             widget=autocomplete.ModelSelect2Multiple(url='seedlot-autocomplete', attrs={'style': 'width:350px'})
                                            )
    
    date = forms.CharField(widget=forms.DateInput(attrs = {'class':'datepicker'}),required=False)
    comment = forms.CharField(required=False)
    
    def clean(self):
        cleaned_data = super(LinkForm, self).clean()
        germplasms = self.cleaned_data.get('germplasms')
        relations = self.cleaned_data.get('relations')
        seed_lots = self.cleaned_data.get('seed_lots')
         
        if not germplasms and not relations and not seed_lots:
            raise forms.ValidationError('Please select at least one entity.')
         
        return cleaned_data
    
    def __init__(self, *args, **kwargs):
        super(LinkForm, self).__init__(*args, **kwargs)
        
    """def save(self):
        self.delete_temporary_files()"""
  
LinkFormSet = formset_factory(LinkForm, max_num=1)



class ImagesManagementForm(forms.Form):
    
    germplasms = forms.ModelMultipleChoiceField(queryset=Germplasm.objects.all().order_by('idgermplasm'),required=False,
                                             widget=autocomplete.ModelSelect2Multiple(url='germplasm-autocomplete', attrs={'style': 'width:350px'})
                                            )

    relations = forms.ModelMultipleChoiceField(queryset=Relation.objects.all().order_by('seed_lot_father_id'),required=False,
                                             widget=autocomplete.ModelSelect2Multiple(url='relation-autocomplete', attrs={'style': 'width:350px'})
                                            )
    
    seed_lots = forms.ModelMultipleChoiceField(queryset=Seedlot.objects.all().order_by('name'),required=False,
                                             widget=autocomplete.ModelSelect2Multiple(url='seedlot-autocomplete', attrs={'style': 'width:350px'})
                                            )
    
    date = forms.CharField(widget=forms.DateInput(attrs = {'class':'datepicker'}),required=False)
    comment = forms.CharField(required=False)
    
    def __init__(self, *args, **kwargs):
        super(ImagesManagementForm, self).__init__(*args, **kwargs)
        
    def clean(self):
        cleaned_data = super(ImagesManagementForm, self).clean()
        germplasms = self.cleaned_data.get('germplasms')
        relations = self.cleaned_data.get('relations')
        seed_lots = self.cleaned_data.get('seed_lots')
         
        if not germplasms and not relations and not seed_lots:
            raise forms.ValidationError('Please select at least one entity.')
         
        return cleaned_data
    
    def save(self):
        pass                
   
         
class ExistingFileForm(MultipleImageForm):
    def get_upload_url(self):
        return reverse('example_handle_upload')