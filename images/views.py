# -*- coding: utf-8 -*-
""" LICENCE:
    SHiNeMaS (Seeds History and Network Management System) is developed by INRA 
    with the close collaboration of the RSP (Réseau Semence Paysanne)
    
    Copyright (C) 2014,  Melanie Polart-Donat

    This file is part of SHiNeMaS

    SHiNeMaS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""
from __future__ import unicode_literals

import traceback
import io
import csv
import hashlib
from pathlib import Path
from PIL import Image as ImagePil

from django.db import transaction
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.decorators import user_passes_test, login_required
from django.contrib import messages
from django.utils.decorators import method_decorator
from django.utils.html import format_html
from django.views import generic
from django.views.generic.base import TemplateView
from django.conf import settings
from django.urls import reverse
from django.core.files import File
from django.http.response import HttpResponseRedirect, Http404, HttpResponse


from images import forms
from images.models import Image, ImageLink
from entities.models import Germplasm, Seedlot
from network.models import Relation
from myuser.utils import recode


IMAGE_NAME = 'IMAGE_NAME'
DATE = 'DATE'
COMMENT = 'COMMENT'
GERMPLASM = 'GERMPLASM'
RELATION = 'RELATION'
SEED_LOT = 'SEED_LOT'

@login_required
def image_render(request, footprint):
    try :
        img = Image.objects.get(footprint=footprint)
        fh = img.image_path.open('rb')
        filename = img.image_name
        response = HttpResponse(fh.read(), content_type='image') 
        response['Content-Disposition'] = 'filename={0}'.format(filename)
        return response
    except Exception as e:
        print(e)
        raise Http404()

class BaseFormView(generic.FormView):
    """ This view manage the upload of images and parse index 
        file informations (if an index file is sent) 
    """
    template_name = 'images/upload_images.html'
    
    def get_success_url(self):
        return reverse('link_images')

    def post(self, request):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        images_files = request.FILES.getlist('input_file')
        index_file = request.FILES.get('input_index_file')
        if form.is_valid():
            self.file_processing(images_files, index_file, form)
            return self.form_valid(form)
        else :
            return self.form_invalid(form)
            

    def file_processing(self, images_files, index_stream, form):
        infos_upload = {}
        size = (250, 250)        
        dict_img = {}
        # Creation of thumbnails, and in the same time verification that it's image files
        for f in images_files:
            
                
            dict_img[str(f)] = f
            #outfile = f.file_id + ".thumbnail"
            outfile = f.name + ".thumbnail"
            if f != outfile:
                try:
                    im = ImagePil.open(f)
                    im.thumbnail(size)
                    im.save(settings.TEMP_DIR + outfile, "JPEG")
                except IOError:
                    messages.add_message(self.request, messages.ERROR, 'The file named : '+ str(f)+ ' is not an image.')  
                    return self.form_invalid(form)
            
            img = open(settings.TEMP_DIR + f.name, 'wb')
            f.seek(0)
            img.write(f.read())
            img.close()
        try:
            # If an index file (input_index_file) is submitted, it's parsed and infos are stored in a dict with images' details
            # Use of Sniffer function to deduce the format of the file
            # https://docs.python.org/3.4/library/csv.html#csv.Sniffer
            if index_stream:
                csv_file = index_stream
                stream_init = csv_file.read()
                stream = recode(stream_init) #we call the recode function to avoid encoding errors             
                f = io.StringIO(stream)
                dialect = csv.Sniffer().sniff(f.read())
                f = io.StringIO(stream)
                index_file = csv.DictReader(f, dialect = dialect) 
                   
                list_files = dict_img.keys()
                
                # All the informations about the image are stored in list detail_img 
                # and next, the list is saved in dict upload_infos
                
                for line in index_file: 
                    
                    if line[IMAGE_NAME] in list_files:
                        
                        details_img = []
                        #details_img.append(dict_img[line[IMAGE_NAME]].file_id)
                        details_img.append(dict_img[line[IMAGE_NAME]].name)
                        details_img.append(line[IMAGE_NAME])
                        details_img.append(line[DATE])
                        details_img.append(line[COMMENT])
                                
                        list_g = []
                        list_r = []
                        list_sl = []
             
                        if line[GERMPLASM]:
                            list_g = list(map(lambda x:x.strip(), line[GERMPLASM].split(';')))
                            """germplasm_list = line[GERMPLASM].split(';')
                            for germplasm in germplasm_list:
                                g = germplasm.strip()
                                list_g.append(g)"""
                                     
                        if line[RELATION]:
                            #à optimiser
                            relation_list = line[RELATION].split(';')
                            #all_relations = Relation.objects.all()
                            for relation in relation_list:
                                r = relation.split("-->")
                                try :
                                    relation_obj = Relation.objects.get(seed_lot_son__name = r[1].strip(),seed_lot_father__name = r[0].strip())
                                except :
                                    #do something here
                                    print("relation non trouvée")
                                else :
                                    list_r.append(relation_obj.id)
                                #print(list_r)          
     
                        if line[SEED_LOT]:
                            list_sl = list(map(lambda x:x.strip(), line[SEED_LOT].split(';')))
                            """seed_lot_list = line[SEED_LOT].split(';')
                            for seed_lot in seed_lot_list:
                                sl = seed_lot.strip(' ')             
                                list_sl.append(sl)"""
                                            
                        details_img.append(list_g)
                        details_img.append(list_r)
                        details_img.append(list_sl)
                                
                        if line[IMAGE_NAME] not in infos_upload.keys():
                            infos_upload[line[IMAGE_NAME]] = {}
                            infos_upload[line[IMAGE_NAME]] = details_img
                        else:
                            messages.add_message(self.request, messages.WARNING, 'There is more than one line for the image '+ line[IMAGE_NAME]+ ' in the index file.')  
                                
                    else:
                        messages.add_message(self.request, messages.WARNING, 'Image '+ line[IMAGE_NAME] + ' is in the index file but has not been submitted.')  

                    
                for file in list_files: 
                    if file not in infos_upload.keys():
                        messages.add_message(self.request, messages.WARNING, 'The image '+ file + ' is not in the index file.')
                        details_img = []
                        #details_img.append(dict_img[file].file_id)
                        details_img.append(dict_img[file].name)
                        details_img.append(file)
                        for i in range(5):
                            details_img.append('')
                        infos_upload[file] = details_img
            
            # If there's no index file, only images infos are stored    
            else:
                for f in images_files:   
                    details_img = []
                    #details_img.append(f.file_id)
                    details_img.append(f.name)
                    details_img.append(str(f))
                    for i in range(5):
                        details_img.append('')
                    infos_upload[str(f)] = details_img
                    
        except Exception as e:
            print(traceback.format_exc())
            messages.add_message(self.request, messages.ERROR, 'There is an error in the index file.')  
            return self.form_invalid(form)

        #form.save()
        print(infos_upload)
        # Dict infos_upload is save in the session's variable
        # https://docs.djangoproject.com/fr/1.11/topics/http/sessions/
        self.request.session['infos_upload'] = infos_upload
        
        return super(BaseFormView, self).form_valid(form)
    
    

@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
@method_decorator(transaction.non_atomic_requests, name='dispatch')
@method_decorator(login_required, name='dispatch')
class UploadedImagesView(BaseFormView):
    form_class = forms.MultipleImageForm
    
    
class ExistingFileView(BaseFormView):   
    """ This view adds to the mecanism of BaseFormView, 
        the form class needed by Django File Form package. 
    """  
    form_class = forms.ExistingFileForm
    
    def get_form_kwargs(self):
        form_kwargs = super(ExistingFileView, self).get_form_kwargs()
        
        example = Image.objects.get(id=self.kwargs['id'])

        if example.image_path:
            name = Path(example.image_path.name).name
            form_kwargs['initial'] = dict(
                image_path=File(name)
            )
 
        return form_kwargs

         
#handle_upload = FileFormUploader()
    
    
    
@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
@method_decorator(transaction.non_atomic_requests, name='dispatch')
@method_decorator(login_required, name='dispatch')
class LinkImagesView(generic.FormView):
    """ This view retrieve data of the BasicFormView, 
        pass data to the form and collect info in order to 
        create Image and ImageLink objects.  
    """  
    template_name = 'images/link_images.html'
    form_class = forms.LinkForm
    
    def get_success_url(self):
        return reverse('upload_summary')

    def get(self, request, *args, **kwargs):
        """ Handles GET requests and instantiates blank versions of the form
            and its formsets or if an index file was submitted, pre-filled 
            fields with data collected in the previous view.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        
        infos_upload = self.request.session['infos_upload'].copy()

        liste_n = []
        liste_d = []
        liste_c = []
        liste_g = []
        liste_r = []
        liste_sl = []
        
        liste_img = []
            
        for clef in infos_upload.keys():
            val = infos_upload.get(clef)
            liste_img.append(val[0] + '.thumbnail')
            liste_n.append(val[1])
            liste_d.append(val[2])
            liste_c.append(val[3])
            
            
            get_g = []
            get_g = val[4]            
            get_g2 = []                      
            for g in get_g:
                try:
                    get_g2.append(Germplasm.objects.get(idgermplasm=g))
                except:
                    get_g2.append(None)
            liste_g.append(get_g2)
            
#             get_r = []
#             for i in range(int(len(val[5])/2)):
#                 get_r.append(val[5][i*2])

            get_r2 = []
            #for i in range(len(get_r)):
            for i in range(len(val[5])):
                #get_r2.append(Relation.objects.get(pk=get_r[i]))
                get_r2.append(Relation.objects.get(pk=val[5][i]))
            liste_r.append(get_r2)
            
            get_sl = []
            get_sl = val[6] 
            get_sl2 = []
            for sl in get_sl:
                try:
                    get_sl2.append(Seedlot.objects.get(name=sl))
                except:
                    get_sl2.append(None)    
            liste_sl.append(get_sl2)
        
        # Formsets are pre-filled with info of those lists  
        link_form = forms.LinkFormSet(initial=[{'germplasms': liste_g[i],
                                                'relations': liste_r[i],
                                                'seed_lots': liste_sl[i],
                                                'date': liste_d[i],
                                                'comment': liste_c[i]} for i in range(len(liste_d))])
        
        formset = zip(link_form, liste_n, liste_img)
        
        self.request.session['liste_n'] = liste_n
        self.request.session['liste_img'] = liste_img

        return self.render_to_response(
            self.get_context_data(form=form,
                                  formset=formset,
                                  link_form=link_form))
        
    def post(self, request, *args, **kwargs):
        """ Handles POST requests, instantiating a form instance and its
            formsets with the passed POST variables and then checking them for
            validity.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
       
        link_form = forms.LinkFormSet(self.request.POST)
        
        liste_n = self.request.session['liste_n']
        liste_img = self.request.session['liste_img']
        formset = zip(link_form, liste_n, liste_img)
        
        if link_form.is_valid():
            return self.form_valid(form, link_form)
        else:
            return self.form_invalid(form, link_form, formset)
        
    def form_valid(self, form, link_form):
        """ Called if all forms are valid. Create Image and ImageLink 
            objects with infos of the formsets. Then redirects to the UploadSummary view.
        """
        """self.object = form.save()
        link_form.instance = self.object"""
        
        infos_upload = self.request.session['infos_upload'].copy() 
        summary_upload = {}
        
        liste_n = self.request.session['liste_n']
        liste_img = self.request.session['liste_img']
        formset = zip(link_form, liste_n, liste_img)
        
        liste_n = []
        liste_d = []
        liste_c = []
        liste_g = []
        liste_r = []
        liste_sl = []

        liste_img = []
               
        if link_form.is_valid():     
            
            for form, clef in zip(link_form, infos_upload):
                val = infos_upload.get(clef)
                print(val)
                #up_file = UploadedFile.objects.get(file_id=val[0])
                #file_w_id = up_file.get_uploaded_file()
                
                date = form.cleaned_data.get('date') or None  
                comment = form.cleaned_data.get('comment')
                
                # Image's footprint is created.
                file_stream = open(settings.TEMP_DIR+val[1], "rb")
                stream = file_stream.read()
                crypt = hashlib.sha1()
                crypt.update(stream)
                footprint = crypt.hexdigest()
                
                # We check that image is not already in database, 
                # if it is not, Image object is create.
                try:
                    img = Image.objects.get(footprint=footprint)
                    in_db = True
                except:
                    print("object not found create a new one")
                    in_db = False
                    
                if not in_db:    
                    try:
                        file_object = open(settings.TEMP_DIR+val[1], "rb")
                        img = Image.objects.create(
                            image_path=File(file_object, name=settings.IMAGE_PATH+val[1]),
                            image_name= val[1],
                            date = date,
                            comment = comment,
                            footprint = footprint
                        )
                        
                        liste_d.append(date)
                        liste_c.append(comment)
                    
                    except Exception as e:
                        print(traceback.format_exc())
                        msg = 'There is an error in the submitted informations for the image named {0} : <br />{1}'.format(val[1],str(e))
                        messages.add_message(self.request, messages.ERROR, format_html(msg))
                        return self.form_invalid(form, link_form, formset)
                else:
                    messages.add_message(self.request, messages.WARNING, 'Image '+ val[1] + ' is already in the database. New link has been added but other informations has not been modified.')
                    
                    liste_d.append(None)
                    liste_c.append(None)
                    
            # New ImageLink objects are created if they didn't already exist
                
                get_g = []
                for g in form.cleaned_data.get("germplasms"):
                    #germplasm_obj = Germplasm.objects.get(idgermplasm=g)
                    
                    #g_qs = ImageLink.objects.filter(image=img, entity_name=g)
                    g_qs = ImageLink.objects.filter(image=img, object_id=g.id ,content_type=ContentType.objects.get_for_model(g).id)
                    if not g_qs:
                        new_link = ImageLink(image=img, entity=g, entity_name=g.idgermplasm)
                        new_link.save()
                        get_g.append(str(g))
                    
                        
                liste_g.append(get_g)
                
                get_r = []  
                #all_relations = Relation.objects.all()      
                for r in form.cleaned_data.get("relations"):
                    r_qs = ImageLink.objects.filter(image=img, object_id=r.id ,content_type=ContentType.objects.get_for_model(r).id)
                    if not r_qs:
                        new_link = ImageLink(image=img, entity=r, entity_name=str(r))
                        new_link.save()
                        get_r.append(str(r))                            
                                
                liste_r.append(get_r)
                
                get_sl = []
                for sl in form.cleaned_data.get("seed_lots"):
                    sl_qs = ImageLink.objects.filter(image=img, object_id=sl.id ,content_type=ContentType.objects.get_for_model(sl).id)
                    if not sl_qs:
                        new_link = ImageLink(image=img, entity=sl, entity_name=sl.name)
                        new_link.save()
                        get_g.append(str(g))
                    
                        
                liste_sl.append(get_sl)
                
                if get_g or get_r or get_sl :
                    liste_img.append(val[0] + '.thumbnail')
                    liste_n.append(val[1])
                else:
                    liste_img.append(None)
                    liste_n.append(None)

                #form.save()
                
            summary_upload["liste_n"] = liste_n
            summary_upload["liste_d"] = liste_d
            summary_upload["liste_c"] = liste_c
            summary_upload["liste_g"] = liste_g
            summary_upload["liste_r"] = liste_r
            summary_upload["liste_sl"] = liste_sl
            summary_upload["liste_img"] = liste_img
                    
            self.request.session['summary_upload'] = summary_upload

            messages.add_message(self.request, messages.SUCCESS, 'The following images have been saved with those informations :')

            return HttpResponseRedirect(self.get_success_url())
            #return render('images/upload_summary.html', request, summary_upload)
        
        else:
            return self.form_invalid(form, link_form, formset)
    
    
    def form_invalid(self, form, link_form, formset):
        """
        Called if a form is invalid. Re-renders the context data with the
        data-filled forms and errors.
        """
        return self.render_to_response(
            self.get_context_data(form=form,
                                  formset=formset,
                                  link_form=link_form))
        

@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
@method_decorator(transaction.non_atomic_requests, name='dispatch')
@method_decorator(login_required, name='dispatch')
class UploadSummaryView(TemplateView):
    """ This view display all infos which were selected in the formsets.
    """
    template_name = 'images/upload_summary.html'
    
    def get_context_data(self, **kwargs):
        context = super(UploadSummaryView, self).get_context_data(**kwargs)
        
        summary_upload = self.request.session['summary_upload'].copy()
        
        formset = zip(summary_upload["liste_n"], summary_upload["liste_d"],\
                      summary_upload["liste_c"], summary_upload["liste_g"],\
                      summary_upload["liste_r"], summary_upload["liste_sl"],\
                      summary_upload["liste_img"])
        
        context['formset'] = formset
        
        return context
    
    
@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
@method_decorator(transaction.non_atomic_requests, name='dispatch')
@method_decorator(login_required, name='dispatch')
class ImagesManagementView(generic.FormView):
    """ This view manage image's informations, it creates or delete
        ImageLink objects and edit database data. 
    """
    template_name = 'images/images_management.html'  
    form_class = forms.ImagesManagementForm
    
    def get_success_url(self):
        return reverse('images_management', kwargs={'image_id':self.kwargs['image_id']})
    
    
    def get(self, request, *args, **kwargs): 
        """ Retrieval of ImageLink objects linked to the 
            image and image's informations.
        """  
        image = Image.objects.get(id=self.kwargs.get('image_id'))
        entities = ImageLink.objects.filter(image=image)
        
        germplasms = []
        relations = []
        seed_lots = []
        
        for entity in entities: 
            if str(entity.content_type) == 'germplasm':
                germplasms.append(Germplasm.objects.get(id=entity.object_id))
            elif str(entity.content_type) == 'relation':
                relations.append(Relation.objects.get(id=entity.object_id))
            else:
                seed_lots.append(Seedlot.objects.get(id=entity.object_id))

        form = self.form_class(initial={'germplasms': germplasms,
                                        'relations': relations,
                                        'seed_lots': seed_lots,
                                        'date': image.date,
                                        'comment': image.comment })
          
        return self.render_to_response(
            self.get_context_data(form=form,
                                  image=image))
        
    def form_valid(self, form):
        """ Infos of the form are compared with infos in the database and saved 
        """ 
        if form.is_valid(): 
            image = Image.objects.get(id=self.kwargs.get('image_id'))
            
            date = form.cleaned_data.get('date') or None  
            comment = form.cleaned_data.get('comment')
            
            queryset = []
            
            # Saving new date if it was modified.
            if str(date) != image.date:
                try:
                    image.date = date
                    image.save()
                except:
                    messages.add_message(self.request, messages.ERROR, 'There is an error with the date submitted')
                    return self.form_invalid(form)
                
            # Saving new comment if it was modified.    
            if str(comment) != image.comment:
                try:
                    image.comment = str(comment)   
                    image.save()
                except:
                    messages.add_message(self.request, messages.ERROR, 'There is an error with the comment submitted')
                    return self.form_invalid(form)
            
        # If new links have been added, creation of ImageLink objects, 
               
            for germplasm in form.cleaned_data.get("germplasms"):
                queryset.append(ImageLink.objects.filter(image=image, entity_name=str(germplasm)))
                if not ImageLink.objects.filter(image=image, entity_name=str(germplasm)):
                    germplasm_obj = Germplasm.objects.get(idgermplasm=str(germplasm))
                    new_link = ImageLink(image=image, entity=germplasm_obj, entity_name=str(germplasm))
                    new_link.save()
                    
            for relation in form.cleaned_data.get("relations"):
                queryset.append(ImageLink.objects.filter(image=image, entity_name=str(relation)))
                if not ImageLink.objects.filter(image=image, entity_name=str(relation)):
                    all_relations = Relation.objects.all() 
                    for rel in all_relations:
                        if str(relation) == str(rel):
                            relation_obj = Relation.objects.get(pk=rel.id)
                    new_link = ImageLink(image=image, entity=relation_obj, entity_name=str(relation))
                    new_link.save()
             
            for seed_lot in form.cleaned_data.get("seed_lots"):
                queryset.append(ImageLink.objects.filter(image=image, entity_name=str(seed_lot))) 
                if not ImageLink.objects.filter(image=image, entity_name=str(seed_lot)):
                    seed_lot_obj = Seedlot.objects.get(name=str(seed_lot))
                    new_link = ImageLink(image=image, entity=seed_lot_obj, entity_name=str(seed_lot))
                    new_link.save()
            
            # Check between infos retrieved form the form, and infos in database.               
            qs = []
            for q in queryset:
                for l in q:
                    qs.append(l)
            
            links = ImageLink.objects.filter(image=image)
            
            # If an ImageLink object is in the database and not in the 
            # infos of the form, it is deleted.
            
            for link in links:
                if link not in qs:
                    link.delete()
                
            
            form.save
            messages.add_message(self.request, messages.SUCCESS, 'The image has been successfully updated')
            return HttpResponseRedirect(self.get_success_url()) 
            
        else:
            return self.form_invalid(form)
    